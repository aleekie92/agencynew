# Add project specific ProGuard rules here.
# By default, the flags in this file are appended to flags specified
# in C:\Users\Jeremy\AppData\Local\Android\Sdk/tools/proguard/proguard-android.txt
# You can edit the include path and order by changing the proguardFiles
# directive in build.gradle.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

# Add any project specific keep options here:

# If your project uses WebView with JS, uncomment the following
# and specify the fully qualified class name to the JavaScript interface
# class:
#-keepclassmembers class fqcn.of.javascript.interface.for.webview {
#   public *;
#}

# Uncomment this to preserve the line number information for
# debugging stack traces.
#-keepattributes SourceFile,LineNumberTable

# If you keep the line number information, uncomment this to
# hide the original source file name.
#-renamesourcefileattribute SourceFile


#these rules.....
    -dontskipnonpubliclibraryclasses
    -forceprocessing
    -optimizationpasses 5
    -dontusemixedcaseclassnames
    -dontskipnonpubliclibraryclasses
    -dontskipnonpubliclibraryclassmembers
    -dontpreverify
     -verbose
    -dump class_files.txt
    -printusage unused.txt
    -optimizations !code/simplification/arithmetic,!field/*,!class/merging/*

    -allowaccessmodification
    -keepattributes *Annotation*
    -renamesourcefileattribute SourceFile
    -keepattributes SourceFile,LineNumberTable

   # -dontwarn com.yalantis.ucrop**
   # -keep class com.yalantis.ucrop** {*;}
   # -keep interface com.yalantis.ucrop** {*;}

    -assumenosideeffects class android.util.Log {
      public static *** v(...);
      public static *** d(...);
      public static *** i(...);
      public static *** w(...);
      public static *** e(...);
    }
    -flattenpackagehierarchy 'ex'
    -repackageclasses 'ex'
    -keep class !com.coretec.agencyApplication** { *; }
    -keep class com.coretec.agencyApplication.api** { *; }
    -keep class com.coretec.agencyApplication.api.requests{ *; }
    -keep class com.coretec.agencyApplication.api.responses{ *; }
    -keepclassmembers class ** {
     @com.google.common.eventbus.Subscribe <methods>;
   }