package com.coretec.agencyApplication.fingerprint.bfp.demo;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.util.Log;

import com.coretec.agencyApplication.fingerprint.bfp.mgr.PtHelper;
import com.digitalpersona.uareu.Engine;
import com.digitalpersona.uareu.Fmd;
import com.digitalpersona.uareu.Fmd.Format;
import com.digitalpersona.uareu.Importer;
import com.digitalpersona.uareu.UareUException;
import com.digitalpersona.uareu.UareUGlobal;
import com.upek.android.ptapi.PtConnectionI;
import com.upek.android.ptapi.PtConstants;
import com.upek.android.ptapi.PtException;
import com.upek.android.ptapi.PtGlobal;
import com.upek.android.ptapi.callback.PtGuiStateCallback;
import com.upek.android.ptapi.callback.PtIdleCallback;
import com.upek.android.ptapi.resultarg.ByteArrayArg;
import com.upek.android.ptapi.resultarg.ByteArrayArgI;
import com.upek.android.ptapi.resultarg.IntegerArg;
import com.upek.android.ptapi.struct.PtGuiSampleImage;


public abstract class OpGrab extends Thread implements PtGuiStateCallback, PtIdleCallback
{
    private PtConnectionI mConn;
    private PtGlobal mPtGlobal;
    private Activity mActivity;
    private byte mbyGrabType;

    public OpGrab(PtConnectionI conn, byte byGrabType, Activity aActivity)
    {
        super("GrabThread");
        mConn = conn;
        mActivity = aActivity;
        mbyGrabType = byGrabType;
    }
    public byte idleCallbackInvoke() throws PtException {

        return isInterrupted() ? PtConstants.PT_CANCEL : PtConstants.PT_CONTINUE;
    }

    /**
     * Callback function called by PTAPI.
     */
    public byte guiStateCallbackInvoke(int guiState, int message, byte progress,
                                       PtGuiSampleImage sampleBuffer, byte[] data)
    {
    	String s = PtHelper.GetGuiStateCallbackMessage(guiState,message,progress);
            
        if(s != null)
        {
            onDisplayMessage(s);
        }

        return isInterrupted() ? PtConstants.PT_CANCEL : PtConstants.PT_CONTINUE;
    }
    
    /**
     * Cancel running operation
     */
    @Override
    public void interrupt() 
    {
        super.interrupt();
        
        try 
        {
            mConn.cancel(0);
        }
        catch (PtException e)
        {
        	/* Ignore errors */
        }
    }
    
    
    public static Bitmap CreateBitmap(byte [] aImageData, int iWidth)
    {
    	int[] data = new int[aImageData.length];
    	int iLength = aImageData.length;
    	int iHeight = iLength / iWidth;
		for(int i=0; i<iLength; i++)
        {
        	int color = (int)aImageData[i];
        	if(color < 0)
        	{
        		color = 256 + color;
        	}
        	data[i] = Color.rgb(color,color,color);
        }
        return Bitmap.createBitmap(data, iWidth, iHeight, Bitmap.Config.ARGB_8888);
    }
    
    
//    private void ShowImage(byte [] aImage,int iWidth)
//    {
//    	FPDisplay.mImage = CreateBitmap(aImage,iWidth);
//    	FPDisplay.msTitle = "Fingerprint Image"; 
//        Intent aIntent = new Intent(mActivity, FPDisplay.class);
//        mActivity.startActivityForResult(aIntent,0);
//    }

    private void ConvertImgToIsoTemplate(byte [] aImage, int iWidth)
    {
        int iLength = aImage.length;
        int iHeight = aImage.length/iWidth;

        Importer importer = UareUGlobal.GetImporter();
        try {
            //Fid fid = importer.ImportRaw(aImage, iWidth, iHeight, 500, 0, 0, Fid.Format.ISO_19794_4_2005, 500, false);
            Engine engine = UareUGlobal.GetEngine();
            Fmd fmd = engine.CreateFmd(aImage,iWidth,iHeight,500,0,0, Format.ISO_19794_2_2005);
            int score = engine.Compare(fmd,0,fmd,0);
            Log.i("BasicSample","Import a Fmd from a raw image OK");
            Log.i("BasicSample", String.format("score %d" , score ));
        }
        catch (UareUException e)
        {
            Log.d("BasicSample", "Import Raw Image Fail",e);

        }




    }

    
    /** 
     * Grab execution code. 
     */
    @Override
    public void run()
    {
        try
        {
            // Optional: Set session configuration 
        	ModifyConfig();
            onDisplayMessage("Place your finger on the sensor.");
            // Obtain finger template
            byte [] image = GrabImage(mbyGrabType);
            int iWidth = (mConn.info().imageWidth);
            //if(image == null)
                //onDisplayMessage("Image grabbed badly");
            //else
                //onDisplayMessage("Image grabbed ");
            switch(mbyGrabType)
            {
            case PtConstants.PT_GRAB_TYPE_THREE_QUARTERS_SUBSAMPLE:
                iWidth = (iWidth * 3)>>2;
        	case PtConstants.PT_GRAB_TYPE_508_508_8_SCAN508_508_8:
        	    if (image != null)
//                    ShowImage(image,iWidth);
        		break;
        		default:
        			// unsupported image type for displaying
            }
            Log.i("BasicSample","Calling ConvertImgToIsoTemplate......");
            ConvertImgToIsoTemplate(image,iWidth);
        }
        catch (PtException e)
        {
            onDisplayMessage("exception "+e.getMessage());
        }

        // Un-cancel session
        try
        {
            mConn.cancel(1);
        } 
        catch (PtException e1) {
            onDisplayMessage("exception in cancel() "+e1.getMessage());
        }
        
        onFinished();
    }
    
    /**
     * Modify session configuration if needed
     */
    private void ModifyConfig()  throws PtException
    {
        /*try 
         * {
           final short SESSION_CFG_VERSION = 5;
            PtSessionCfgV5 sessionCfg = (PtSessionCfgV5) mConn.getSessionCfgEx(SESSION_CFG_VERSION);
        
            mConn.setSessionCfgEx(SESSION_CFG_VERSION, sessionCfg);
        }
         catch (PtException e) 
        {
            onDisplayMessage("Unable to set session cfg - " + e.getMessage());
            throw e;
        }*/
    }
    
    /**
     * Obtain finger template.
     */
    private byte[] GrabImage(byte byType) throws PtException
    {
            // Register notification callback of operation state
            // Valid for entire PTAPI session lifetime
            //-------------------------------------------------------------------------------------
            //mConn.setGUICallbacks(null, this);

            //return mConn.grab(byType,PtConstants.PT_BIO_INFINITE_TIMEOUT,true,null,null);

            // for sleepThenGrab
            IntegerArg wakeUpCause = new IntegerArg();
            wakeUpCause.setValue(PtConstants.PT_WAKEUP_CAUSE_FINGER);//PtConstants.PT_WAKEUP_CAUSE_FINGER=1;PT_WAKEUP_CAUSE_HOST
            IntegerArg grabGuiMessage = new IntegerArg();
            ByteArrayArgI grabData = new ByteArrayArg();
         try {
            mConn.sleepThenGrab(this,
                    byType,
                    -1,
                    true,
                    wakeUpCause,
                    grabGuiMessage,
                    grabData,
                    null,
                    null
            );
             onDisplayMessage("wakeupCause "+wakeUpCause.getValue()+" gui "+grabGuiMessage.getValue());

            if ((wakeUpCause.getValue()== PtConstants.PT_WAKEUP_CAUSE_FINGER)
                    && (grabGuiMessage.getValue()== PtConstants.PT_GUIMSG_GOOD_IMAGE))
                return grabData.getValue();
            else
                return null;


        	}
        	catch (PtException e)
        	{
            	onDisplayMessage("Grab failed - " + e.getMessage());
            	throw e;
        	}
    }
    
    
    /** 
     * Display message. To be overridden by sample activity.
     * @param message Message text.
     */
    abstract protected void onDisplayMessage(String message);
    
    /** 
     * Called, if operation is finished.
     * @param message Message text.
     */
    abstract protected void onFinished();

}
