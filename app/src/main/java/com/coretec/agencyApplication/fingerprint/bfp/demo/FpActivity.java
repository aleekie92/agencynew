
package com.coretec.agencyApplication.fingerprint.bfp.demo;

import android.app.Activity;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.coretec.agencyApplication.R;
import com.coretec.agencyApplication.fingerprint.bfp.mgr.FPMgrImpl;
import com.coretec.agencyApplication.fingerprint.bfp.mgr.IFPMgr;
import com.coretec.agencyApplication.fingerprint.util.ByteConvert;
import com.coretec.agencyApplication.fingerprint.util.TextViewUtil;
import com.digitalpersona.uareu.Engine;
import com.digitalpersona.uareu.Engine.Candidate;
import com.digitalpersona.uareu.Fmd;
import com.digitalpersona.uareu.Fmd.Format;
import com.digitalpersona.uareu.Importer;
import com.digitalpersona.uareu.UareUException;
import com.digitalpersona.uareu.UareUGlobal;
import com.upek.android.ptapi.PtConstants;
import com.upek.android.ptapi.PtException;
import com.upek.android.ptapi.struct.PtInputBir;

import java.util.ArrayList;
import java.util.List;

public class FpActivity extends Activity implements View.OnClickListener {

    protected TextView log_text;
    protected Handler mHandler = null;
    protected Runnable runnable = null;
    public Candidate[] results;
    Importer im = null;
    Engine m_engine = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fp_layout);

        Button btn_test1 = (Button) this.findViewById(R.id.btn_test1);

        Button btn_log_clean = (Button) this.findViewById(R.id.btn_log_clean);
        btn_test1.setOnClickListener(this);

        btn_log_clean.setOnClickListener(this);

        log_text = this.findViewById(R.id.text_result);
        log_text.setMovementMethod(ScrollingMovementMethod.getInstance());
        try {
            im = UareUGlobal.GetImporter();
            m_engine = UareUGlobal.GetEngine();
        } catch (Exception e) {
            e.printStackTrace();
        }
        mHandler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                String str = msg.obj + "\n";
                if (msg.what == R.id.log_default) {
                    log_text.setText(str);
                } else if (msg.what == R.id.log_success) {
                    // String str = msg.obj + "\n";
                    TextViewUtil.infoBlueTextView(log_text, str);
                } else if (msg.what == R.id.log_failed) {
                    // String str = msg.obj + "\n";
                    TextViewUtil.infoRedTextView(log_text, str);
                } else if (msg.what == R.id.log_clean) {
                    log_text.setText("");
                }
            }
        };
    }

    private void writerLogInTextview(String log, int id) {
        Message msg = new Message();
        msg.what = id;
        msg.obj = log;
        mHandler.sendMessage(msg);
    }

    @Override
    public void onClick(View view) {
        int index = view.getId();
        if (index == R.id.btn_test1) {
            test();
        } else if (index == R.id.btn_log_clean) {
            log_text.setText("");
        }
    }

    Thread th = null;

    public void test() {
        if (th == null || th.getState() == Thread.State.TERMINATED) {
            th = new Thread() {
                @SuppressWarnings("unused")
                public void run() {
                    IFPMgr fpMgr = FPMgrImpl.getInstance();
                    try {

                        fpMgr.open(FpActivity.this);

                        fpMgr.deleteAll(FpActivity.this);

                        List<PtInputBir> birList = new ArrayList<PtInputBir>();
                        // fpMgr.test(FpActivity.this, FingerId.NONE);
                        writerLogInTextview("start enroll ", R.id.log_success);
                        // PtInputBir template = null;

                        // template = fpMgr.enrollFp(FpActivity.this,
                        // FingerId.NONE);
                        try
                        {
                            byte[] image = fpMgr
                                    .GrabImage(PtConstants.PT_GRAB_TYPE_508_508_8_SCAN508_508_8);
                            int iWidth = fpMgr.getImagewidth();
                            Fmd fm = ConvertImgToIsoTemplate(image, iWidth);
                            Fmd fm1 = im.ImportFmd(ByteConvert.parseHexStr2Byte(TestConstant.FP1),
                                    Fmd.Format.ISO_19794_2_2005, Fmd.Format.ISO_19794_2_2005);
                            Fmd fm2 = im.ImportFmd(ByteConvert.parseHexStr2Byte(TestConstant.FP2),
                                    Fmd.Format.ISO_19794_2_2005, Fmd.Format.ISO_19794_2_2005);
                            Fmd fm3 = im.ImportFmd(ByteConvert.parseHexStr2Byte(TestConstant.FP3),
                                    Fmd.Format.ISO_19794_2_2005, Fmd.Format.ISO_19794_2_2005);
                            Fmd fm4 = im.ImportFmd(ByteConvert.parseHexStr2Byte(TestConstant.FP4),
                                    Fmd.Format.ISO_19794_2_2005, Fmd.Format.ISO_19794_2_2005);
                            Fmd fm5 = im.ImportFmd(ByteConvert.parseHexStr2Byte(TestConstant.FP5),
                                    Fmd.Format.ISO_19794_2_2005, Fmd.Format.ISO_19794_2_2005);
                            Fmd fm6 = im.ImportFmd(ByteConvert.parseHexStr2Byte(TestConstant.FP6),
                                    Fmd.Format.ISO_19794_2_2005, Fmd.Format.ISO_19794_2_2005);
                            Fmd fm7 = im.ImportFmd(ByteConvert.parseHexStr2Byte(TestConstant.FP7),
                                    Fmd.Format.ISO_19794_2_2005, Fmd.Format.ISO_19794_2_2005);
                            Fmd fm8 = im.ImportFmd(ByteConvert.parseHexStr2Byte(TestConstant.FP8),
                                    Fmd.Format.ISO_19794_2_2005, Fmd.Format.ISO_19794_2_2005);
                            Fmd fm9 = im.ImportFmd(ByteConvert.parseHexStr2Byte(TestConstant.FP9),
                                    Fmd.Format.ISO_19794_2_2005, Fmd.Format.ISO_19794_2_2005);
                            Fmd fm10 = im.ImportFmd(
                                    ByteConvert.parseHexStr2Byte(TestConstant.FP10),
                                    Fmd.Format.ISO_19794_2_2005, Fmd.Format.ISO_19794_2_2005);

                            Fmd[] m_fmds_temp = new Fmd[] {
                                    fm1, fm2, fm3, fm4, fm5, fm6, fm7, fm8, fm9, fm10
                            };
                            String data = HexString.bufferToHex(fm.getData());
                            Log.e("fp", "" + data);
                            
                            //compare myself, score: 0=match 
                            int score = m_engine.Compare(fm,0,fm,0);
                            Log.e("BasicSample", String.format("score %d" , score ));
                            
                            //compare with others
                            results = m_engine.Identify(fm, 0, m_fmds_temp, 200000, 3);
                            int m_score = 0;
                           
                            if (results.length != 0)
                            {
                                for (int i = 0; i < 10; i++) {
                                    m_score = m_engine.Compare(m_fmds_temp[i], 0, fm, 0);
                                    writerLogInTextview("match result !" + m_score,
                                            R.id.log_success);
                                }

                            }
                            else
                            {
                                m_score = -1;
                                writerLogInTextview("match failed !" + m_score, R.id.log_failed);
                            }
                        }
                        catch (PtException e)
                        {
                            writerLogInTextview("exception " + e.getMessage(), R.id.log_success);
                        }
                       
                    } catch (UareUException e) {
                        e.printStackTrace();
                        writerLogInTextview("exception occured !" + e.getMessage(), R.id.log_failed);
                    } finally {
                        fpMgr.close();
                    }
                }
            };
            th.start();
        }
    }

    private Fmd ConvertImgToIsoTemplate(byte[] aImage, int iWidth)
    {
        int iLength = aImage.length;
        int iHeight = aImage.length / iWidth;

        Importer importer = UareUGlobal.GetImporter();
        try {
            // Fid fid = importer.ImportRaw(aImage, iWidth, iHeight, 500, 0, 0,
            // Fid.Format.ISO_19794_4_2005, 500, false);
            Engine engine = UareUGlobal.GetEngine();
            Fmd fmd = engine.CreateFmd(aImage, iWidth, iHeight, 500, 0, 0, Format.ISO_19794_2_2005);
            // int score = engine.Compare(fmd,0,fmd,0);

            Log.i("BasicSample", "Import a Fmd from a raw image OK");
            // Log.i("BasicSample", String.format("score %d" , score ));
            writerLogInTextview("compare ok", R.id.log_success);
            return fmd;
        } catch (UareUException e)
        {
            Log.d("BasicSample", "Import Raw Image Fail", e);
            writerLogInTextview("compare fail", R.id.log_success);
            return null;
        }

    }

}
