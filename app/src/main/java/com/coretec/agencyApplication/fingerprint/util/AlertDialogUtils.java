package com.coretec.agencyApplication.fingerprint.util;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.PowerManager;

import com.coretec.agencyApplication.R;


/**
 * Created by pengli on 17-2-17.
 */

public class AlertDialogUtils {

	public static void showWarningDialog(Activity mActivity, String title, String content, DialogInterface.OnClickListener listener) {
		showDialog(mActivity, android.R.drawable.stat_notify_error, title, content, listener);
	}

	public static void showDialog(Activity mActivity, int iconID, String title, String content, DialogInterface.OnClickListener listener) {
		// 创建构建器
		AlertDialog.Builder builder = new AlertDialog.Builder(mActivity);
		// 设置参数
		builder.setTitle(title).setIcon(iconID)
				.setMessage(content)
				.setPositiveButton(R.string.confirm, listener);
		builder.create().show();
	}

	public static void showSuccessDialog(final Activity mActivity, String title, String content) {
		showDialog(mActivity, android.R.drawable.btn_star, title, content, new DialogInterface.OnClickListener() {// 积极

			@Override
			public void onClick(DialogInterface dialog, int which) {
				PowerManager pManager=(PowerManager) mActivity.getSystemService(Context.POWER_SERVICE);
				pManager.reboot("reboot");
			}
		});
	}


	public static void showWarningDialog(Activity mActivity, String title, String content) {
		showWarningDialog(mActivity, title, content, new DialogInterface.OnClickListener() {// 积极

			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.dismiss();
			}
		});
	}

	public static void showWarningDialog(Activity mActivity, String warningMsg) {
		String title = mActivity.getString(R.string.warning_title);
		showWarningDialog(mActivity, title, warningMsg);

	}

	public static void showWarningDialog(Activity mActivity, String warningMsg, DialogInterface.OnClickListener listener) {
		String title = mActivity.getString(R.string.warning_title);
		showWarningDialog(mActivity, title, warningMsg, listener);

	}


}
