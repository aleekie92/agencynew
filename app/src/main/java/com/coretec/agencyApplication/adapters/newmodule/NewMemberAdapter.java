package com.coretec.agencyApplication.adapters.newmodule;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.coretec.agencyApplication.R;
import com.coretec.agencyApplication.activities.newmodules.MicroGroupRepayment;
import com.coretec.agencyApplication.fragments.newmodule.GroupMembersFragment;
import com.coretec.agencyApplication.model.newmodulemodels.GroupObject;
import com.coretec.agencyApplication.utils.SharedPrefs;

import java.util.ArrayList;
import java.util.List;

public class NewMemberAdapter extends RecyclerView.Adapter<NewMemberAdapter.ViewHolder> {

    private Context mContext ;
    private List<GroupObject> mData ;

    private testClick mOnClickListener;

    public NewMemberAdapter(Context mContext, List<GroupObject> mData,testClick mOnClickListener) {
        this.mContext = mContext;
        this.mData = mData;
        this.mOnClickListener=mOnClickListener;

    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView mnumber, accname, expectedamount;
        LinearLayout layoutHolder;
        public ViewHolder(View itemView) {
            super(itemView);
             mnumber = itemView.findViewById(R.id.acc_no_id);
             accname = itemView.findViewById(R.id.tv_name);
             expectedamount = itemView.findViewById(R.id.tv_exp_amt);
             layoutHolder=itemView.findViewById(R.id.linear_container);

        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view ;
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        view = inflater.inflate(R.layout.item_group_members,parent,false) ;
        //final ViewHolder viewHolder = new ViewHolder(view) ;

      //  view.setOnClickListener(mOnClickListener.);
        return new ViewHolder(view);
        //return viewHolder;
    }

    @Override
    public void onBindViewHolder(NewMemberAdapter.ViewHolder holder, final int position) {

        final GroupObject list = mData.get(position);

        holder.mnumber.setText(list.getMemberno());
        holder.accname.setText(list.getAccountname());
        holder.expectedamount.setText(list.getExpectedamount());




        holder.layoutHolder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mOnClickListener.testClicking(mData.get(position).getMemberno());
            }
        });

    }

    @Override
    public int getItemCount() {
        return mData.size();
    }


    public  interface testClick{
        void testClicking(String dataTest);
    }
}
