package com.coretec.agencyApplication.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.coretec.agencyApplication.R;
import com.coretec.agencyApplication.model.Transactions;

import java.util.List;


public class MiniStatementAdapter extends RecyclerView.Adapter<MiniStatementAdapter.MyViewHolder> {

    private Context mContext;
    private List<Transactions> transactionsList;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private TextView receipt_amount;
        private TextView receipt_posting_date;
        private TextView receipt_description;

        public MyViewHolder(View view) {
            super(view);
            receipt_amount = (TextView) view.findViewById(R.id.ms_amount);
            receipt_posting_date = (TextView) view.findViewById(R.id.ms_posting_date);
            receipt_description = (TextView) view.findViewById(R.id.ms_description);
        }
    }

    public MiniStatementAdapter(Context mContext, List<Transactions> transactionsList) {
        this.mContext = mContext;
        this.transactionsList = transactionsList;
    }

    @Override
    public MiniStatementAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.receipt_image, parent, false);

        return new MiniStatementAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MiniStatementAdapter.MyViewHolder holder, final int position) {
        Transactions transactions = transactionsList.get(position);
        holder.receipt_posting_date.setText(transactions.getPosting_date());
        holder.receipt_description.setText(transactions.getDescription());
        holder.receipt_amount.setText(String.valueOf(transactions.getAmount()));


    }

    @Override
    public int getItemCount() {
        return transactionsList.size();
    }
}
