package com.coretec.agencyApplication.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.coretec.agencyApplication.R;
import com.coretec.agencyApplication.api.responses.LastFiveList;

import java.util.List;

public class LastFiveAdapter extends RecyclerView.Adapter<LastFiveAdapter.MyViewHolder> {

    private Context mContext;
    private List<LastFiveList> lastFiveLists;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private TextView last_amount;
        private TextView last_transaction_type;

        public MyViewHolder(View view) {
            super(view);

            last_transaction_type = view.findViewById(R.id.last_transaction_type);
            last_amount =  view.findViewById(R.id.last_amount);
        }
    }

    public LastFiveAdapter(Context mContext, List<LastFiveList> lastFiveLists) {
        this.mContext = mContext;
        this.lastFiveLists = lastFiveLists;
    }

    @Override
    public LastFiveAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.last_five_adapter, parent, false);

        return new LastFiveAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final LastFiveAdapter.MyViewHolder holder, final int position) {
        LastFiveList lastfive = lastFiveLists.get(position);
        if (lastfive != null) {
            holder.last_amount.setText(String.valueOf(lastfive.getAmount()));
            holder.last_transaction_type.setText(lastfive.getTransactiontype());
        }

    }

    @Override
    public int getItemCount() {
        return lastFiveLists.size();
    }
}