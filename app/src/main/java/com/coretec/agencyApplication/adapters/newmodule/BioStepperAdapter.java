package com.coretec.agencyApplication.adapters.newmodule;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.IntRange;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentManager;

import com.coretec.agencyApplication.fragments.newmodule.UpdateBiometrics;
import com.coretec.agencyApplication.fragments.newmodule.UpdateCrossmatchBio;
import com.coretec.agencyApplication.fragments.newmodule.UpdateTunzhengbigBio;
import com.coretec.agencyApplication.utils.SharedPrefs;
import com.stepstone.stepper.Step;
import com.stepstone.stepper.adapter.AbstractFragmentStepAdapter;
import com.stepstone.stepper.viewmodel.StepViewModel;

public class BioStepperAdapter extends AbstractFragmentStepAdapter {

    private static final String CURRENT_STEP_POSITION_KEY = "messageResourceId";
    public BioStepperAdapter(FragmentManager fm, Context context) {
        super(fm, context);
    }
    @Override
    public Step createStep(int position) {
        String module = SharedPrefs.read(SharedPrefs.FINGERMODULE, null);

        switch (position){
            case 0:
                final UpdateBiometrics step1 = new UpdateBiometrics();
                //final FinalFingerprint step1 = new FinalFingerprint();
                //final DetailsFragment step1 = new DetailsFragment();
                Bundle b1 = new Bundle();
                b1.putInt(CURRENT_STEP_POSITION_KEY, position);
                step1.setArguments(b1);
                return step1;

            case 1:

                if (module.equals("1")){
                final UpdateTunzhengbigBio step2 = new UpdateTunzhengbigBio();
                Bundle b2 = new Bundle();
                b2.putInt(CURRENT_STEP_POSITION_KEY, position);
                step2.setArguments(b2);
                return step2;
            }else {
                final UpdateCrossmatchBio step2 = new UpdateCrossmatchBio();
                Bundle b2 = new Bundle();
                b2.putInt(CURRENT_STEP_POSITION_KEY, position);
                step2.setArguments(b2);
                return step2;
            }
        }
        return null;
    }
    @Override
    public int getCount() {
        return 2;
    }
    @NonNull
    @Override
    public StepViewModel getViewModel(@IntRange(from = 0) int position) {
        //Override this method to set Step title for the Tabs, not necessary for other stepper types
        switch (position){
            case 0:
                return new StepViewModel.Builder(context)
                        .setTitle("Customer Details") //can be a CharSequence instead
                        .create();
            case 1:
                return new StepViewModel.Builder(context)
                        .setTitle("Update Fingerprints")
                        .create();
        }
        return null;
    }

}
