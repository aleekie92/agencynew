package com.coretec.agencyApplication.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.coretec.agencyApplication.R;
import com.coretec.agencyApplication.api.responses.AccBalance;

import java.util.List;

/**
 * Created by ofula on 18/06/24.
 */

public class BalanceInquiryAdapter extends RecyclerView.Adapter<BalanceInquiryAdapter.MyViewHolder> {

    private Context mContext;
    private List<AccBalance> balanceList;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private TextView accountName;
        private TextView amount;
        private TextView accountno;

        public MyViewHolder(View view) {
            super(view);
            accountName = (TextView) view.findViewById(R.id.account_name);
            amount = (TextView) view.findViewById(R.id.balance_amount);
            accountno = (TextView) view.findViewById(R.id.account_number);
        }
    }

    public BalanceInquiryAdapter(Context mContext, List<AccBalance> balanceList) {
        this.mContext = mContext;
        this.balanceList = balanceList;
    }

    @Override
    public BalanceInquiryAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.balance_receipt_adapter, parent, false);

        return new BalanceInquiryAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final BalanceInquiryAdapter.MyViewHolder holder, final int position) {
//        BalanceResponse balanceResponse = new BalanceResponse();
        AccBalance accBalance = balanceList.get(position);
        holder.amount.setText(String.valueOf(accBalance.getAmount()));
        holder.accountName.setText(String.valueOf(accBalance.getAccount()));
        holder.accountno.setText(String.valueOf(accBalance.getAccountno()));
    }

    @Override
    public int getItemCount() {
        return balanceList.size();
    }
}
