package com.coretec.agencyApplication.adapters.newmodule;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.IntRange;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentManager;
import android.util.Log;

import com.coretec.agencyApplication.fragments.AgencyBackIdFragment;
import com.coretec.agencyApplication.fragments.AgencyCustomerFragment;
import com.coretec.agencyApplication.fragments.AgencyFrontIdFragment;
import com.coretec.agencyApplication.fragments.AgencyMoreFragment;
import com.coretec.agencyApplication.fragments.AgencyPhotoFragment;
import com.coretec.agencyApplication.fragments.newmodule.ApplicationFormBack;
import com.coretec.agencyApplication.fragments.newmodule.ApplicationFormFront;
import com.coretec.agencyApplication.fragments.newmodule.CrossmatchBioEnrollment;
import com.coretec.agencyApplication.fragments.newmodule.NextOfKinFragment;
import com.coretec.agencyApplication.fragments.newmodule.TunzhengbigBioEnrollment;
import com.coretec.agencyApplication.fragments.newmodule.SignaturePhoto;
import com.coretec.agencyApplication.utils.SharedPrefs;
import com.stepstone.stepper.Step;
import com.stepstone.stepper.adapter.AbstractFragmentStepAdapter;
import com.stepstone.stepper.viewmodel.StepViewModel;

public class NewmoduleRegistrationAdapter extends AbstractFragmentStepAdapter {

    private static final String CURRENT_STEP_POSITION_KEY = "messageResourceId";
    public NewmoduleRegistrationAdapter(FragmentManager fm, Context context) {
        super(fm, context);
    }
    @Override
    public Step createStep(int position) {
        String module = SharedPrefs.read(SharedPrefs.FINGERMODULE, null);

            switch (position){
                case 0:
                    final AgencyCustomerFragment step1 = new AgencyCustomerFragment();
                    //final NextOfKinFragment step1 = new NextOfKinFragment();
                    //final TunzhengbigBioEnrollment step1 = new TunzhengbigBioEnrollment();
                    Bundle b1 = new Bundle();
                    b1.putInt(CURRENT_STEP_POSITION_KEY, position);
                    step1.setArguments(b1);
                    return step1;
                case 1:
                    final AgencyMoreFragment step2 = new AgencyMoreFragment();
                    Bundle b2 = new Bundle();
                    b2.putInt(CURRENT_STEP_POSITION_KEY, position);
                    step2.setArguments(b2);
                    return step2;

                case 2:
                    final AgencyPhotoFragment step3 = new AgencyPhotoFragment();
                    Bundle b3 = new Bundle();
                    b3.putInt(CURRENT_STEP_POSITION_KEY, position);
                    step3.setArguments(b3);
                    return step3;
                case 3:
                    final AgencyFrontIdFragment step4 = new AgencyFrontIdFragment();
                    Bundle b4 = new Bundle();
                    b4.putInt(CURRENT_STEP_POSITION_KEY, position);
                    step4.setArguments(b4);
                    return step4;
                case 4:
                    final AgencyBackIdFragment step5 = new AgencyBackIdFragment();
                    Bundle b5 = new Bundle();
                    b5.putInt(CURRENT_STEP_POSITION_KEY, position);
                    step5.setArguments(b5);
                    return step5;
                case 5:
                    final SignaturePhoto step6 = new SignaturePhoto();
                    Bundle b6 = new Bundle();
                    b6.putInt(CURRENT_STEP_POSITION_KEY, position);
                    step6.setArguments(b6);
                    return step6;

                case 6:
                    final ApplicationFormFront step7 = new ApplicationFormFront();
                    Bundle b7 = new Bundle();
                    b7.putInt(CURRENT_STEP_POSITION_KEY, position);
                    step7.setArguments(b7);
                    return step7;

                case 7:
                    final ApplicationFormBack step8 = new ApplicationFormBack();
                    Bundle b8 = new Bundle();
                    b8.putInt(CURRENT_STEP_POSITION_KEY, position);
                    step8.setArguments(b8);
                    return step8;


                case 8:
                    final NextOfKinFragment step9 = new NextOfKinFragment();
                    Bundle b9 = new Bundle();
                    b9.putInt(CURRENT_STEP_POSITION_KEY, position);
                    step9.setArguments(b9);
                    return step9;

                    case 9:
                    if (module.equals("1")){

                            final TunzhengbigBioEnrollment step10 = new TunzhengbigBioEnrollment();
                            Bundle b10 = new Bundle();
                            b10.putInt(CURRENT_STEP_POSITION_KEY, position);
                            step10.setArguments(b10);
                            return step10;
                    }else {
                        final CrossmatchBioEnrollment step10 = new CrossmatchBioEnrollment();
                        Bundle b10 = new Bundle();
                        b10.putInt(CURRENT_STEP_POSITION_KEY, position);
                        step10.setArguments(b10);
                        return step10;
                    }


            }
          return null;
    }
    @Override
    public int getCount() {
        return 10;
    }
    @NonNull
    @Override
    public StepViewModel getViewModel(@IntRange(from = 0) int position) {
        switch (position){
            case 0:
                return new StepViewModel.Builder(context)
                        .setTitle("Customer Details")
                        .create();
            case 1:
                return new StepViewModel.Builder(context)
                        .setTitle("More Details")
                        .create();
            case 2:
                return new StepViewModel.Builder(context)
                        .setTitle("Passport Photo")
                        .create();
            case 3:
                return new StepViewModel.Builder(context)
                        .setTitle("Front ID")
                        .create();
            case 4:
                return new StepViewModel.Builder(context)
                        .setTitle("Back ID")
                        .create();

            case 5:
                return new StepViewModel.Builder(context)
                        .setTitle("Signature")
                        .create();

            case 6:
                return new StepViewModel.Builder(context)
                        .setTitle("Application Form Front")
                        .create();

            case 7:
                return new StepViewModel.Builder(context)
                        .setTitle("Application Form Back")
                        .create();

            case 8:
                return new StepViewModel.Builder(context)
                        .setTitle("Next of kin")
                        .create();

            case 9:
                return new StepViewModel.Builder(context)
                        .setTitle("Finger Prints")
                        .create();
        }
        return null;
    }

}
