package com.coretec.agencyApplication.adapters;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentManager;

import com.coretec.agencyApplication.fragments.CustomerUpdateDetailsFragment;
import com.coretec.agencyApplication.fragments.IdentitySearchFragment;
import com.stepstone.stepper.Step;
import com.stepstone.stepper.adapter.AbstractFragmentStepAdapter;
import com.stepstone.stepper.viewmodel.StepViewModel;

public class CustomerDetailsUpdatingAdapter extends AbstractFragmentStepAdapter{

    private static final String CURRENT_STEP_POSITION_KEY = "messageResourceId";

    public CustomerDetailsUpdatingAdapter(FragmentManager fm, Context context) {
        super(fm, context);
    }

    @Override
    public Step createStep(int position) {
        switch (position){
            case 0:
                final IdentitySearchFragment step1 = new IdentitySearchFragment();
                Bundle b1 = new Bundle();
                b1.putInt(CURRENT_STEP_POSITION_KEY, position);
                step1.setArguments(b1);
                return step1;
            case 1:
                final CustomerUpdateDetailsFragment step2 = new CustomerUpdateDetailsFragment();
                Bundle b2 = new Bundle();
                b2.putInt(CURRENT_STEP_POSITION_KEY, position);
                step2.setArguments(b2);
                return step2;
        }
        return null;
    }

    @Override
    public int getCount() {
        return 2;
    }

    @NonNull
    @Override
    public StepViewModel getViewModel(int position) {

        switch (position) {
            case 0:
                return new StepViewModel.Builder(context)
                        .setTitle("Customer Information Update") //can be a CharSequence instead
                        .create();
            case 1:
                return new StepViewModel.Builder(context)
                        .setTitle("Select Option") //can be a CharSequence instead
                        .create();
        }
        return super.getViewModel(position);
    }
}
