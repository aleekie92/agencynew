package com.coretec.agencyApplication.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.coretec.agencyApplication.R;
import com.coretec.agencyApplication.activities.ShareDeposit;
import com.coretec.agencyApplication.activities.CashTransfer;
import com.coretec.agencyApplication.activities.CashWithdrawal;
import com.coretec.agencyApplication.activities.LoanRepayment;
import com.coretec.agencyApplication.model.Transact;

import java.util.List;

/**
 * Created by ofula on 18/06/24.
 */

public class TransactAdapter extends RecyclerView.Adapter<TransactAdapter.MyViewHolder> {

    private Context mContext;
    private List<Transact> transactList;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView title;
        public ImageView thumbnail;

        public MyViewHolder(View view) {
            super(view);
            title = (TextView) view.findViewById(R.id.title_transact);
            thumbnail = (ImageView) view.findViewById(R.id.transact_card);
        }
    }

    public TransactAdapter(Context mContext, List<Transact> transactList) {
        this.mContext = mContext;
        this.transactList = transactList;
    }

    @Override
    public TransactAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_transact, parent, false);

        return new TransactAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final TransactAdapter.MyViewHolder holder, final int position) {
        Transact transact = transactList.get(position);
        holder.title.setText(transact.getName());
        holder.thumbnail.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (position == 0) {
                    Intent intent = new Intent(mContext, CashWithdrawal.class);
                    mContext.startActivity(intent);
                } else if (position == 1) {
                    Intent intent = new Intent(mContext, ShareDeposit.class);
                    mContext.startActivity(intent);
                } else if (position == 2) {
                    Intent intent = new Intent(mContext, CashTransfer.class);
                    mContext.startActivity(intent);
                } else if (position == 3) {
                    Intent intent = new Intent(mContext, LoanRepayment.class);
                    mContext.startActivity(intent);
                }
            }
        });

        // loading album cover using Glide library
        Glide.with(mContext).load(transact.getImage()).into(holder.thumbnail);

    }

    @Override
    public int getItemCount() {
        return transactList.size();
    }
}
