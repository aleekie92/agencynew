package com.coretec.agencyApplication.adapters;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.IntRange;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentManager;

import com.coretec.agencyApplication.fragments.AgentBioRegistrationFragment;
import com.coretec.agencyApplication.fragments.DetailsFragment;
import com.coretec.agencyApplication.fragments.FinalFingerprint;
import com.coretec.agencyApplication.fragments.BackIdFragment;
import com.coretec.agencyApplication.fragments.CustomerDetailsFragment;
import com.coretec.agencyApplication.fragments.FrontIdFragment;
import com.coretec.agencyApplication.fragments.UploadPassportFragment;
import com.stepstone.stepper.Step;
import com.stepstone.stepper.adapter.AbstractFragmentStepAdapter;
import com.stepstone.stepper.viewmodel.StepViewModel;
public class StepperAdapter extends AbstractFragmentStepAdapter {

    private static final String CURRENT_STEP_POSITION_KEY = "messageResourceId";
    public StepperAdapter(FragmentManager fm, Context context) {
        super(fm, context);
    }
    @Override
    public Step createStep(int position) {
        switch (position){
            case 0:
                final CustomerDetailsFragment step1 = new CustomerDetailsFragment();
                //final DetailsFragment step1 = new DetailsFragment();
                //final FinalFingerprint step1 = new FinalFingerprint();
                Bundle b1 = new Bundle();
                b1.putInt(CURRENT_STEP_POSITION_KEY, position);
                step1.setArguments(b1);
                return step1;
            case 1:
                final UploadPassportFragment step2 = new UploadPassportFragment();
                Bundle b2 = new Bundle();
                b2.putInt(CURRENT_STEP_POSITION_KEY, position);
                step2.setArguments(b2);
                return step2;
            case 2:
                final FrontIdFragment step3 = new FrontIdFragment();
                Bundle b3 = new Bundle();
                b3.putInt(CURRENT_STEP_POSITION_KEY, position);
                step3.setArguments(b3);
                return step3;
            case 3:
                final BackIdFragment step4 = new BackIdFragment();
                Bundle b4 = new Bundle();
                b4.putInt(CURRENT_STEP_POSITION_KEY, position);
                step4.setArguments(b4);
                return step4;
            case 4:
                final DetailsFragment step5 = new DetailsFragment();
                Bundle b5 = new Bundle();
                b5.putInt(CURRENT_STEP_POSITION_KEY, position);
                step5.setArguments(b5);
                return step5;
            case 5:
                final FinalFingerprint step6 = new FinalFingerprint();
                Bundle b6 = new Bundle();
                b6.putInt(CURRENT_STEP_POSITION_KEY, position);
                step6.setArguments(b6);
                return step6;
        }
        return null;
    }
    @Override
    public int getCount() {
        return 6;
    }
    @NonNull
    @Override
    public StepViewModel getViewModel(@IntRange(from = 0) int position) {
        //Override this method to set Step title for the Tabs, not necessary for other stepper types
        switch (position){
            case 0:
                return new StepViewModel.Builder(context)
                        .setTitle("Customer Details") //can be a CharSequence instead
                        .create();
            case 1:
                return new StepViewModel.Builder(context)
                        .setTitle("Upload Passport") //can be a CharSequence instead
                        .create();
            case 2:
                return new StepViewModel.Builder(context)
                        .setTitle("Upload Front ID") //can be a CharSequence instead
                        .create();
            case 3:
                return new StepViewModel.Builder(context)
                        .setTitle("Upload Back ID") //can be a CharSequence instead
                        .create();
            case 4:
                return new StepViewModel.Builder(context)
                        .setTitle("More Details")
                        .create();
            /*case 5:
                return new StepViewModel.Builder(context)
                        .setTitle("Sacco Details")
                        .create();*/
            case 5:
                return new StepViewModel.Builder(context)
                        .setTitle("Fingerprints")
                        .create();
        }
        return null;
    }

}
