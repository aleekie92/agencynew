package com.coretec.agencyApplication.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.coretec.agencyApplication.R;
import com.coretec.agencyApplication.api.responses.Accounts;

import java.util.List;

public class ReportsAdapter extends RecyclerView.Adapter<ReportsAdapter.MyViewHolder> {

    private Context mContext;
    private List<Accounts> accountsList;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        private TextView eod_ref_no;
        private TextView eod_amount;
        private TextView eod_transaction_type;

        public MyViewHolder(View view) {
            super(view);
           // eod_ref_no = view.findViewById(R.id.eod_ref_no);

            eod_transaction_type = view.findViewById(R.id.eod_transaction_type);
            eod_amount =  view.findViewById(R.id.eod_amount);
        }
    }

    public ReportsAdapter(Context mContext, List<Accounts> accountsList) {
        this.mContext = mContext;
        this.accountsList = accountsList;
    }

    @Override
    public ReportsAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.eod_reports_adapter, parent, false);

        return new ReportsAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final ReportsAdapter.MyViewHolder holder, final int position) {
        Accounts accounts = accountsList.get(position);

        Log.d("accounttttt","accountlist------>"+accounts.getCount());
        Log.d("accounttttt","accountlist------>"+accounts.getTransactiontype());
        if (accounts != null) {
            //holder.eod_ref_no.setText(accounts.getReferenceno());//.getReferenceno());
            //holder.eod_amount.setText(String.valueOf(accounts.getAmount()));
            holder.eod_amount.setText(accounts.getCount());
            holder.eod_transaction_type.setText(accounts.getTransactiontype());
        }

    }

    @Override
    public int getItemCount() {
        return accountsList.size();
    }
}
