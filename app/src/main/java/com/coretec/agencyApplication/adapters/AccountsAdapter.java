package com.coretec.agencyApplication.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.coretec.agencyApplication.R;
import com.coretec.agencyApplication.activities.BalanceInquiry;
import com.coretec.agencyApplication.activities.MiniStatement;
import com.coretec.agencyApplication.model.Accounts;

import java.util.List;

/**
 * Created by ofula on 18/06/24.
 */

public class AccountsAdapter extends RecyclerView.Adapter<AccountsAdapter.MyViewHolder> {

    private Context mContext;
    private List<Accounts> accountsList;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView title;
        public ImageView thumbnail;

        public MyViewHolder(View view) {
            super(view);
            title = (TextView) view.findViewById(R.id.title_accounts);
            thumbnail = (ImageView) view.findViewById(R.id.accounts_card);
        }
    }

    public AccountsAdapter(Context mContext, List<Accounts> accountsList) {
        this.mContext = mContext;
        this.accountsList = accountsList;
    }

    @Override
    public AccountsAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        final View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.adapter_accounts, parent, false);

        return new AccountsAdapter.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final AccountsAdapter.MyViewHolder holder, final int position) {
        Accounts accounts = accountsList.get(position);
        holder.title.setText(accounts.getName());
        holder.thumbnail.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                if (position == 0) {

                    Intent intent = new Intent(mContext, BalanceInquiry.class);
                    mContext.startActivity(intent);
                } else if (position == 1) {
                    Intent intent = new Intent(mContext, MiniStatement.class);
                    mContext.startActivity(intent);
                } else if (position == 2) {
                    Intent intent = new Intent(mContext, MiniStatement.class);
                    mContext.startActivity(intent);
                } else if (position == 3) {
                    Intent intent = new Intent(mContext, MiniStatement.class);
                    mContext.startActivity(intent);
                }
            }
        });

        // loading album cover using Glide library
        Glide.with(mContext).load(accounts.getImage()).into(holder.thumbnail);

    }

    @Override
    public int getItemCount() {
        return accountsList.size();
    }
}
