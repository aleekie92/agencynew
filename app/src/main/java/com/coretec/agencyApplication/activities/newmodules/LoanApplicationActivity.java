package com.coretec.agencyApplication.activities.newmodules;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.Toast;

import com.coretec.agencyApplication.R;
import com.coretec.agencyApplication.activities.BaseActivity;
import com.coretec.agencyApplication.adapters.newmodule.LoanApplicationAdapter;
import com.coretec.agencyApplication.utils.QuickToast;
import com.coretec.agencyApplication.utils.SharedPrefs;
import com.stepstone.stepper.StepperLayout;
import com.stepstone.stepper.VerificationError;

public class LoanApplicationActivity extends BaseActivity implements StepperLayout.StepperListener {

    private QuickToast toast = new QuickToast(this);
    private StepperLayout mStepperLayout;
    private LoanApplicationAdapter mLoanAdapter;
    boolean doubleBackToExitPressedOnce = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        SharedPrefs.init(getApplicationContext());
        setContentView(R.layout.activity_loan_application);
        SharedPrefs.write(SharedPrefs.FP_BOOLEAN, "");

        mStepperLayout = findViewById(R.id.stepperLayout);
        mLoanAdapter = new LoanApplicationAdapter(getSupportFragmentManager(), this);
        mStepperLayout.setAdapter(mLoanAdapter);
        mStepperLayout.setListener(this);

    }

    @Override
    public void onCompleted(View completeButton) {
        Toast.makeText(this, "onCompleted!", Toast.LENGTH_SHORT).show();
    }
    @Override
    public void onError(VerificationError verificationError) {
        Toast.makeText(this, "onError! -> " + verificationError.getErrorMessage(), Toast.LENGTH_SHORT).show();
    }
    @Override
    public void onStepSelected(int newStepPosition) {

    }
    @Override
    public void onReturn() {
        finish();
    }

    @Override
    public void onBackPressed() {

        if (doubleBackToExitPressedOnce) {
            String msg = "Are you sure you want to exit loan application? ";
            AlertDialog.Builder builder1 = new AlertDialog.Builder(LoanApplicationActivity.this);
            builder1.setMessage(msg);
            builder1.setTitle("Alert!");
            builder1.setCancelable(false);

            builder1.setPositiveButton(
                    "yes",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                            finish();
                            Intent i = new Intent(LoanApplicationActivity.this, LoanActivity.class);
                            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                            //i.addFlags(IntentCompat.FLAG_ACTIVITY_CLEAR_TASK);
                            i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(i);
                            //onBackPressed();
                        }
                    });

            builder1.setNegativeButton(
                    "CANCEL",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                            //onBackPressed();
                        }
                    });

            AlertDialog alert11 = builder1.create();
            alert11.show();
            //System.exit(1);
            //Toast.makeText(this, "Are you sure you want to exit Member Registration", Toast.LENGTH_LONG).show();
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        toast.sinfo("Press back again to exit loan application");

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce=false;
            }
        }, 2000);
        //super.onBackPressed();
        //overridePendingTransition(R.anim.move_left_in_activity, R.anim.move_right_out_activity);
    }

}