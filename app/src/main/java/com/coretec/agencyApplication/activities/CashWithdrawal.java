package com.coretec.agencyApplication.activities;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.graphics.Rect;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.animation.DecelerateInterpolator;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.cloudpos.jniinterface.PrinterInterface;
import com.coretec.agencyApplication.R;
import com.coretec.agencyApplication.activities.newmodules.MainDashboardActivity;
import com.coretec.agencyApplication.api.Api;
import com.coretec.agencyApplication.api.requests.AccountDetailsRequest;
import com.coretec.agencyApplication.api.requests.RequestWithdrawal;
import com.coretec.agencyApplication.api.requests.SignaturePhotoRequest;
import com.coretec.agencyApplication.api.responses.AccountDetailsResponse;
import com.coretec.agencyApplication.api.responses.SaccoAccounts;
import com.coretec.agencyApplication.api.responses.WithdrawalResponse;
import com.coretec.agencyApplication.model.AccountDetails;
import com.coretec.agencyApplication.utils.PreferenceFileKeys;
import com.coretec.agencyApplication.utils.QuickToast;
import com.coretec.agencyApplication.utils.SharedPrefs;
import com.coretec.agencyApplication.utils.Utils;
import com.coretec.agencyApplication.wizarpos.function.ActionCallbackImpl;
import com.coretec.agencyApplication.wizarpos.function.printer.PrinterCommand;
import com.coretec.agencyApplication.wizarpos.mvc.base.ActionCallback;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import static com.coretec.agencyApplication.utils.Utils.getFormattedDate;
import static com.coretec.agencyApplication.utils.Utils.getIMEI;
import static com.coretec.agencyApplication.utils.Utils.getLat;
import static com.coretec.agencyApplication.utils.Utils.getLong;
public class CashWithdrawal extends BaseActivity {

    private Button btnConfirmWithdrawal, btn_account_search;
    private ProgressBar withdrawal_progressBar;
    private TextInputLayout input_layout_member_account_number_withdrawal;
    private TextInputLayout layout_amount_to_withdraw;
    private TextView deposit_account_name;
    private EditText amount_to_withdraw;
    private EditText withdrawal_identifier;
    private String withdraw_member_number, withdraw_amount;
    public static Handler handler;
    public static ActionCallback callback;
    //AppCompatRadioButton radio_mpesa, radio_cash;
    public RadioButton rbpin, rbbio;

    private Spinner spinner;
    private RadioGroup radioGroupAutharization;
    private ArrayList<String> arrayList = new ArrayList<>();
    private ArrayAdapter<AccountDetails> adapter;

    String mpesaPhoneNumber = "";
    boolean iscashwithdrawal = true;

    private Spinner spinner2;
    private ArrayList<String> arrayList2 = new ArrayList<>();
    private ArrayAdapter<String> adapter2;

    SharedPreferences sharedPreferences;
    String accountNo = "";
    String accountName = "";
    String identifiercode = "";

    SharedPreferences sharedPref;
    public static String saccomotto = "";
    ProgressBar progressBar, pbphotos;
    ProgressDialog progressDialog;
    boolean find = true;
    private QuickToast toast = new QuickToast(this);
    LinearLayout pic_layout;
    private ImageView picture;
    private ImageView IVsignature;
    private static String autmode;

    private Animator currentAnimator;
    private int shortAnimationDuration;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cash_withdrawal);
        this.overridePendingTransition(R.anim.move_right_in_activity, R.anim.move_left_out_activity);
        sharedPreferences = getSharedPreferences(PreferenceFileKeys.PREFS_FILE, Context.MODE_PRIVATE);
        SharedPrefs.write(SharedPrefs.FP_BOOLEAN, "false");
        rbpin = findViewById(R.id.rbpin);
        rbbio = findViewById(R.id.rbbio);

        handler = new Handler();
        callback = new ActionCallbackImpl(handler);
        progressBar = findViewById(R.id.idPb);
        pbphotos = findViewById(R.id.pbphotos);
        deposit_account_name = findViewById(R.id.deposit_account_name);

        pic_layout = findViewById(R.id.pictures_layout);
        picture = findViewById(R.id.picture_imageview);
        IVsignature = findViewById(R.id.signature_imageview);

        spinner = findViewById(R.id.withdrawal_accounts);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                AccountDetails accountDetails = (AccountDetails) parent.getItemAtPosition(position);
                accountNo = accountDetails.getAccountno();
                accountName = accountDetails.getAccountname();

                if (!accountDetails.equals("")) {
                    deposit_account_name.setText(accountName);
                } else {

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spinner2 = findViewById(R.id.spinner2);
        adapter2 = new ArrayAdapter<String>(getApplicationContext(), R.layout.simple_spinner_dropdown_item, arrayList2);
        adapter2.setDropDownViewResource(R.layout.simple_spinner_dropdown_item);
        spinner2.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                identifiercode = String.valueOf(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        withdrawal_progressBar = findViewById(R.id.withdrawal_progressBar);
        withdrawal_progressBar.setVisibility(View.GONE);
        withdrawal_identifier = findViewById(R.id.withdrawal_identifier);
        amount_to_withdraw = findViewById(R.id.amount_to_withdraw);

        input_layout_member_account_number_withdrawal = findViewById(R.id.input_layout_withdrawal_identifier);
        layout_amount_to_withdraw = findViewById(R.id.layout_amount_to_withdraw);
        btn_account_search = findViewById(R.id.btn_account_search);

        btn_account_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String identifier_number = withdrawal_identifier.getText().toString();

                if (withdrawal_identifier.getText().toString().matches("")) {
                    toast.swarn("Please enter Identity number to proceed!");
                } else {
                    if (MainDashboardActivity.corporateno.equals("CAP002")){
                        progressBar.setVisibility(View.VISIBLE);
                        requestGetAccountDetails(identifier_number);
                        pbphotos.setVisibility(View.VISIBLE);
                        getPicture();
                    }else {
                        progressBar.setVisibility(View.VISIBLE);
                        requestGetAccountDetails(identifier_number);
                    }

                }
            }
        });

        radioGroupAutharization = findViewById(R.id.radioGroupAutharization);
        radioGroupAutharization.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (checkedId == R.id.rbpin) {
                    autmode = "1";
                    btnConfirmWithdrawal.setClickable(true);
                }
                if (checkedId == R.id.rbbio) {
                    autmode = "2";
                    btnConfirmWithdrawal.setClickable(true);
                }
            }
        });

        TextWatcher textWatcher = new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                adapter = new ArrayAdapter<AccountDetails>(getApplicationContext(),
                        R.layout.simple_spinner_dropdown_item, new ArrayList<AccountDetails>());
                adapter.setDropDownViewResource(R.layout.simple_spinner_dropdown_item);
                adapter.clear();

                deposit_account_name.setText("*** ***");
                spinner.setAdapter(adapter);


            }
        };

        withdrawal_identifier.addTextChangedListener(textWatcher);
        btnConfirmWithdrawal = findViewById(R.id.submitWithdrawalRequest);

        String coopnumber = SharedPrefs.read(SharedPrefs.VR_COOPORATE_NO, null);
        if (coopnumber !=null){
            switch (SharedPrefs.read(SharedPrefs.VR_COOPORATE_NO, null)) {
                case "CAP005":
                    btnConfirmWithdrawal.setBackgroundResource(R.drawable.btn_rounded_primary_coloraccentforcoopmis);
                    break;
                case "CAP022":
                    btnConfirmWithdrawal.setBackgroundResource(R.drawable.btn_rounded_primary_coloraccentforcoopmis);
                    break;
                default:
                    btnConfirmWithdrawal.setBackgroundResource(R.drawable.btn_rounded_primary_coloraccent);
                    break;
            }

        }else {
            btnConfirmWithdrawal.setBackgroundResource(R.drawable.btn_rounded_primary_coloraccent);
        }
        btnConfirmWithdrawal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                withdraw_member_number = withdrawal_identifier.getText().toString().trim();
                withdraw_amount = amount_to_withdraw.getText().toString().trim();

                if (withdraw_member_number.isEmpty()) {
                    input_layout_member_account_number_withdrawal.setError("Enter the Member Account Number");
                } else if (withdraw_amount.isEmpty() || withdraw_amount.startsWith("0")) {
                    Toast.makeText(CashWithdrawal.this, "Enter the Amount to Withdraw", Toast.LENGTH_SHORT).show();
                } else if (pbphotos.getVisibility() == View.VISIBLE) {
                    Toast.makeText(CashWithdrawal.this, "Loading photos, Please wait... ", Toast.LENGTH_SHORT).show();
                } else {

                    if (rbpin.isChecked() || rbbio.isChecked()) {
                        withdrawal_progressBar.setVisibility(View.VISIBLE);
                        progressDialog = new ProgressDialog(CashWithdrawal.this);
                        progressDialog.setMessage("Processing your request, please wait...");
                        progressDialog.setCancelable(false);
                        progressDialog.show();
                        String name = null;

                        if (spinner != null && spinner.getSelectedItem() != null) {
                            switch (autmode) {
                                case "1":
                                    requestFundsWithdrawalForPin(withdraw_member_number, withdraw_amount);
                                    break;
                                case "2":
                                    String identity=withdrawal_identifier.getText().toString();
                                    requestFingerPrints(CashWithdrawal.this, identity ,withdrawal_progressBar);
                                    break;
                                default:
                                    requestFundsWithdrawalForPin(withdraw_member_number, withdraw_amount);
                            }

                            btnConfirmWithdrawal.setClickable(false);
                            progressDialog.dismiss();
                        } else {
                            Toast.makeText(CashWithdrawal.this, "Please wait.....", Toast.LENGTH_SHORT).show();
                            progressDialog.dismiss();
                        }
                    } else {
                        Toast.makeText(CashWithdrawal.this, "Please select authentication method", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
    }

    void requestGetAccountDetails(String identifier) {
        final String TAG = "MemberAccounts";
        String URL = Api.MSACCO_AGENT + Api.GetAllMemberSaccoDetails;
        final AccountDetailsRequest accountDetails = new AccountDetailsRequest();
        accountDetails.agentid = sharedPreferences.getString(PreferenceFileKeys.AGENT_ID, "");
        accountDetails.corporateno = MainDashboardActivity.corporateno;
        accountDetails.accountidentifier = identifier;
        accountDetails.accountidentifiercode = identifiercode;
        accountDetails.transactiontype = "W";
        accountDetails.terminalid = MainDashboardActivity.imei;
        accountDetails.longitude = getLong(this);
        accountDetails.latitude = getLat(this);
        accountDetails.date = getFormattedDate();

        Log.e(TAG, accountDetails.getBody().toString());
        Api.instance(this).request(URL, accountDetails, new Api.RequestListener() {
            @Override
            public void onSuccess(String response) {
                withdrawal_progressBar.setVisibility(View.GONE);
                AccountDetailsResponse accountDetailsResponse = Api.instance(CashWithdrawal.this)
                        .mGson.fromJson(response, AccountDetailsResponse.class);
                if (accountDetailsResponse.is_successful) {
                    progressBar.setVisibility(View.GONE);
                    List<AccountDetails> accountDetailsList = new ArrayList<>();
                    adapter = new ArrayAdapter<AccountDetails>(getApplicationContext(),
                            R.layout.simple_spinner_dropdown_item, new ArrayList<AccountDetails>());
                    adapter.setDropDownViewResource(R.layout.simple_spinner_dropdown_item);
                    adapter.clear();
                    spinner.setAdapter(adapter);

                    for (SaccoAccounts saccoAccounts : accountDetailsResponse.getSaccoAccounts()) {
                        accountDetailsList.addAll(saccoAccounts.getAccountDetails());
                    }

                    adapter = new ArrayAdapter<AccountDetails>(getApplicationContext(),
                            R.layout.simple_spinner_dropdown_item, new ArrayList<AccountDetails>());
                    adapter.setDropDownViewResource(R.layout.simple_spinner_dropdown_item);
                    adapter.addAll(accountDetailsList);
                    spinner.setAdapter(adapter);

                } else {
                    toast.swarn("Please confirm identity number and try again!");
                    progressBar.setVisibility(View.GONE);
                }

                try {
                    JSONObject jsonObject = new JSONObject(response);
                    JSONArray jsonArray = jsonObject.getJSONArray("saccoaccounts");
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                        JSONArray jsonArray1 = jsonObject1.getJSONArray("accountdetails");
                        for (int l = 0; l < jsonArray1.length(); l++) {
                            JSONObject jsonObject2 = jsonArray1.getJSONObject(l);
                            arrayList.add(String.valueOf(jsonObject2.getString("accountno") + " " + jsonObject2.getString("accounttype")));
                            mpesaPhoneNumber = jsonObject2.getString("phonenumber");
                        }

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }

            @Override
            public void onTokenExpired() {

            }

            @Override
            public void onError(String error) {

            }
        });
    }



    void requestFundsWithdrawalForPin(String withdraw_member_number, final String withdraw_amount) {

       /* final Timer t = new Timer();
        t.schedule(new TimerTask() {
            @Override
            public void run() {
                progressDialog.dismiss();
                t.cancel();
            }
        }, 6000);*/

        String URL = Api.MSACCO_AGENT + Api.AgentFundsWithdrawal;

        RequestWithdrawal requestWithdrawal = new RequestWithdrawal();
        requestWithdrawal.agentid = sharedPreferences.getString(PreferenceFileKeys.AGENT_ID, "");
        requestWithdrawal.corporate_no = MainDashboardActivity.corporateno;
        requestWithdrawal.accountidentifier = withdraw_member_number;
        requestWithdrawal.accountidentifiercode = identifiercode;
        requestWithdrawal.accountno = accountNo;
        requestWithdrawal.iscashwithdrawal = iscashwithdrawal; //CASH
        requestWithdrawal.amount = Integer.parseInt(withdraw_amount);
        requestWithdrawal.terminalid = getIMEI(this);
        requestWithdrawal.longitude = getLong(this);
        requestWithdrawal.latitude = getLat(this);
        requestWithdrawal.date = getFormattedDate();
        requestWithdrawal.mpesaPhoneNumber = mpesaPhoneNumber;

        Toast.makeText(CashWithdrawal.this, "START OF POSTING", Toast.LENGTH_LONG).show();
        Api.instance(this).request(URL, requestWithdrawal, new Api.RequestListener() {
            @Override
            public void onSuccess(String response) {
                withdrawal_progressBar.setVisibility(View.GONE);

                final WithdrawalResponse withdrawalResponse = Api.instance(CashWithdrawal.this).mGson.fromJson(response, WithdrawalResponse.class);
                if (withdrawalResponse.is_successful) {

                    Toast.makeText(CashWithdrawal.this, "END OF POSTING", Toast.LENGTH_LONG).show();
                    final String agentName = sharedPreferences.getString(PreferenceFileKeys.AGENT_ACCOUNT_NAME, "");
                    final String saccoMotto = sharedPreferences.getString(PreferenceFileKeys.AGENT_SACCO_MOTTO, "");
                        PrinterInterface.open();
                        writetest(accountNo, agentName, withdrawalResponse.getAmount(), withdrawalResponse.getSacconame(), saccoMotto, withdrawalResponse.getCustomername(), withdrawalResponse.getReceiptno(), withdrawalResponse.getTransactiondate(), withdrawalResponse.getTransactiontype());
                        progressDialog.dismiss();

                        Utils.showAlertDialog(CashWithdrawal.this, "Cash withdrawn Successfully", " Press Ok to print another Receipt ", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                PrinterInterface.open();
                                writetest1(accountNo, agentName, withdrawalResponse.getAmount(), withdrawalResponse.getSacconame(), saccoMotto, withdrawalResponse.getCustomername(), withdrawalResponse.getReceiptno(), withdrawalResponse.getTransactiondate(), withdrawalResponse.getTransactiontype());
                                finish();
                                Intent i = new Intent(CashWithdrawal.this, MainDashboardActivity.class);
                                i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                                i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(i);
                            }
                        });
                        PrinterInterface.close();

                } else {
                    progressDialog.dismiss();
                    withdrawal_progressBar.setVisibility(View.GONE);
                    Utils.showAlertDialog(CashWithdrawal.this, "Withdrawal Failed", withdrawalResponse.getError());
                }

            }

            @Override
            public void onTokenExpired() {

            }

            @Override
            public void onError(String error) {

            }
        });
    }


    void requestFundsWithdrawalForBio(String withdraw_member_number, final String withdraw_amount) {
        final String TAG = "withdraw cash ";
        Log.i("withdrawal", "requestFundsWithdrawal");

        Log.e(TAG, withdraw_member_number);
        Log.e(TAG, String.valueOf(withdraw_amount));

        final Timer t = new Timer();
        t.schedule(new TimerTask() {
            @Override
            public void run() {
                progressDialog.dismiss();
                t.cancel();
            }
        }, 6000);

        String URL = Api.MSACCO_AGENT + Api.AgentFundsWithdrawal;

        RequestWithdrawal requestWithdrawal = new RequestWithdrawal();
        requestWithdrawal.agentid = sharedPreferences.getString(PreferenceFileKeys.AGENT_ID, "");
        requestWithdrawal.corporate_no = MainDashboardActivity.corporateno;
        requestWithdrawal.accountidentifier = withdraw_member_number;
        requestWithdrawal.accountidentifiercode = identifiercode;
        requestWithdrawal.accountno = accountNo;
        requestWithdrawal.iscashwithdrawal = iscashwithdrawal; //CASH
        requestWithdrawal.amount = Integer.parseInt(withdraw_amount);
        requestWithdrawal.terminalid = getIMEI(this);
        requestWithdrawal.longitude = getLong(this);
        requestWithdrawal.latitude = getLat(this);
        requestWithdrawal.date = getFormattedDate();
        requestWithdrawal.mpesaPhoneNumber = mpesaPhoneNumber;

        Api.instance(this).bioauthrequest(URL, requestWithdrawal, new Api.RequestListener() {
            @Override
            public void onSuccess(String response) {
                withdrawal_progressBar.setVisibility(View.GONE);

                final WithdrawalResponse withdrawalResponse = Api.instance(CashWithdrawal.this).mGson.fromJson(response, WithdrawalResponse.class);
                if (withdrawalResponse.is_successful) {
                    final String agentName = sharedPreferences.getString(PreferenceFileKeys.AGENT_ACCOUNT_NAME, "");
                    final String saccoMotto = sharedPreferences.getString(PreferenceFileKeys.AGENT_SACCO_MOTTO, "");

                    if (PrinterInterface.queryStatus() == 0) {
                        Utils.showAlertDialog(CashWithdrawal.this, "NO THERMAL PAPER!", "Put thermal paper and click OK to continue.", new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                PrinterInterface.open();
                                writetest(accountNo, agentName, withdrawalResponse.getAmount(), withdrawalResponse.getSacconame(), saccoMotto, withdrawalResponse.getCustomername(), withdrawalResponse.getReceiptno(), withdrawalResponse.getTransactiondate(), withdrawalResponse.getTransactiontype());
                                progressDialog.dismiss();

                                Utils.showAlertDialog(CashWithdrawal.this, "Cash Withdrawn Successfully", " Press Ok to print another Receipt ", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        PrinterInterface.open();
                                        writetest1(accountNo, agentName, withdrawalResponse.getAmount(), withdrawalResponse.getSacconame(), saccoMotto, withdrawalResponse.getCustomername(), withdrawalResponse.getReceiptno(), withdrawalResponse.getTransactiondate(), withdrawalResponse.getTransactiontype());
                                        finish();
                                        Intent i = new Intent(CashWithdrawal.this, MainDashboardActivity.class);
                                        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                                        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                        startActivity(i);
                                    }
                                });

                            }
                        });
                        PrinterInterface.close();
                    } else {
                        PrinterInterface.open();
                        writetest(accountNo, agentName, withdrawalResponse.getAmount(), withdrawalResponse.getSacconame(), saccoMotto, withdrawalResponse.getCustomername(), withdrawalResponse.getReceiptno(), withdrawalResponse.getTransactiondate(), withdrawalResponse.getTransactiontype());
                        progressDialog.dismiss();

                        Utils.showAlertDialog(CashWithdrawal.this, "Cash withdrawn Successfully", " Press Ok to print another Receipt ", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                PrinterInterface.open();
                                writetest1(accountNo, agentName, withdrawalResponse.getAmount(), withdrawalResponse.getSacconame(), saccoMotto, withdrawalResponse.getCustomername(), withdrawalResponse.getReceiptno(), withdrawalResponse.getTransactiondate(), withdrawalResponse.getTransactiontype());
                                finish();
                                Intent i = new Intent(CashWithdrawal.this, MainDashboardActivity.class);
                                i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                                i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(i);
                            }
                        });
                        PrinterInterface.close();
                    }

                } else {
                    progressDialog.dismiss();
                    withdrawal_progressBar.setVisibility(View.GONE);
                    Utils.showAlertDialog(CashWithdrawal.this, "Withdrawal Failed", withdrawalResponse.getError());
                }

            }

            @Override
            public void onTokenExpired() {

            }

            @Override
            public void onError(String error) {

            }
        });
    }


    public void writetest(String accountNumber, String agentName, int amount, String saccoame, String saccoMotto, String customerName, String receiptNumber, String dateed, String transactionType) {

        try {
            byte[] arrySaccoName = null;
            byte[] terminalNo = null;
            byte[] receiptNo = null;
            byte[] date = null;
            byte[] arryWhat = null;

            byte[] arryCustomerName = null;
            byte[] accountNum = null;
            byte[] transactionTYpe = null;
            byte[] arryAmt = null;

            byte[] id = null;
            byte[] signature = null;

            byte[] servedBy = null;

            byte[] arryMotto = null;

            byte[] customerReceipt = null;

            try {
                arrySaccoName = String.valueOf("       " + String.valueOf(saccoame)).getBytes("GB2312");
                terminalNo = String.valueOf("Terminal ID :" + Utils.getIMEI(CashWithdrawal.this)).getBytes("GB2312");
                receiptNo = String.valueOf("Receipt No :" + receiptNumber).getBytes("GB2312");
                date = String.valueOf("Date : " + dateed).getBytes("GB2312");

                arryWhat = String.valueOf("         AGENT WITHDRAWAL").getBytes("GB2312");

                arryCustomerName = String.valueOf("Name :" + customerName).getBytes("GB2312");
                accountNum = String.valueOf(String.valueOf("Acc No :" + accountNumber)).getBytes("GB2312");
                transactionTYpe = String.valueOf("Transaction :" + transactionType).getBytes("GB2312");
                arryAmt = String.valueOf(String.valueOf("Amount :" + amount)).getBytes("GB2312");

                id = String.valueOf("ID :_______________________").getBytes("GB2312");
                signature = String.valueOf("Signature______________").getBytes("GB2312");

                servedBy = String.valueOf("You were Served By " + agentName).getBytes("GB2312");

                customerReceipt = String.valueOf("         Customer Copy         ").getBytes("GB2312");

                arryMotto = String.valueOf("     " + saccoMotto).getBytes("GB2312");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            PrinterInterface.begin();

            byte[] cmd;
            cmd = new byte[]{(byte) 0x1B, (byte) 0x40};

            cmd = new byte[]{
                    (byte) 0x1B, (byte) 0x45, (byte) 0};

            write(cmd);
            writeLineBreak(1);
            write(arrySaccoName);
            writeLineBreak(2);

            write(terminalNo);
            writeLineBreak(1);
            write(receiptNo);
            writeLineBreak(1);
            write(date);
            writeLineBreak(2);
            write(arryWhat);
            writeLineBreak(2);

            write(arryCustomerName);
            writeLineBreak(1);
            write(accountNum);
            writeLineBreak(1);
            write(transactionTYpe);
            writeLineBreak(1);
            write(arryAmt);
            writeLineBreak(2);

            write(id);
            writeLineBreak(1);
            write(signature);
            writeLineBreak(2);

            write(servedBy);
            writeLineBreak(2);

            write(customerReceipt);
            writeLineBreak(2);

            write(arryMotto);
            writeLineBreak(4);
            PrinterInterface.end();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void writetest1(String accountNumber, String agentName, int amount, String saccoame, String saccoMotto, String customerName, String receiptNumber, String dateed, String transactionType) {
        try {

            byte[] arrySaccoName = null;
            byte[] terminalNo = null;
            byte[] receiptNo = null;
            byte[] date = null;
            byte[] arryWhat = null;

            byte[] arryCustomerName = null;
            byte[] accountNum = null;
            byte[] transactionTYpe = null;
            byte[] arryAmt = null;

            byte[] id = null;
            byte[] signature = null;

            byte[] servedBy = null;

            byte[] arryMotto = null;

            byte[] customerReceipt = null;

            try {
                arrySaccoName = String.valueOf("       " + String.valueOf(saccoame)).getBytes("GB2312");
                terminalNo = String.valueOf("Terminal ID :" + Utils.getIMEI(CashWithdrawal.this)).getBytes("GB2312");
                receiptNo = String.valueOf("Receipt No : " + receiptNumber).getBytes("GB2312");
                date = String.valueOf("Date : " + dateed).getBytes("GB2312");

                arryWhat = String.valueOf("         AGENT WITHDRAWAL").getBytes("GB2312");

                arryCustomerName = String.valueOf("Name : " + customerName).getBytes("GB2312");
                accountNum = String.valueOf(String.valueOf("Acc No : " + accountNumber)).getBytes("GB2312");
                transactionTYpe = String.valueOf("Transaction Type : " + transactionType).getBytes("GB2312");
                arryAmt = String.valueOf(String.valueOf("Amount :" + amount)).getBytes("GB2312");

                id = String.valueOf("ID :____________________").getBytes("GB2312");
                signature = String.valueOf("Signature________________").getBytes("GB2312");

                servedBy = String.valueOf("You were Served By " + agentName).getBytes("GB2312");

                customerReceipt = String.valueOf("         Agent Copy         ").getBytes("GB2312");

                arryMotto = String.valueOf("     " + saccoMotto).getBytes("GB2312");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            PrinterInterface.begin();

            byte[] cmd;
            cmd = new byte[]{(byte) 0x1B, (byte) 0x40};

            cmd = new byte[]{
                    (byte) 0x1B, (byte) 0x45, (byte) 0};

            write(cmd);
            writeLineBreak(1);
            write(arrySaccoName);
            writeLineBreak(2);

            write(terminalNo);
            writeLineBreak(1);
            write(receiptNo);
            writeLineBreak(1);
            write(date);
            writeLineBreak(2);

            write(arryWhat);
            writeLineBreak(2);

            write(arryCustomerName);
            writeLineBreak(1);
            write(accountNum);
            writeLineBreak(1);
            write(transactionTYpe);
            writeLineBreak(1);
            write(arryAmt);
            writeLineBreak(2);

            write(id);
            writeLineBreak(1);
            write(signature);
            writeLineBreak(2);

            write(servedBy);
            writeLineBreak(2);

            write(customerReceipt);
            writeLineBreak(2);

            write(arryMotto);
            writeLineBreak(4);
            PrinterInterface.end();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void write(final byte[] arryData) {
        int result = PrinterInterface.write(arryData, arryData.length);
    }

    // print line break
    private void writeLineBreak(int lineNumber) {
        // write(new byte[]{'\n'});
        write(PrinterCommand.getCmdEscDN(lineNumber));
    }



    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.move_left_in_activity, R.anim.move_right_out_activity);
    }


    @Override
    protected void onResume() {
        SharedPrefs.init(getApplicationContext());
        super.onResume();
        String fpBoolean = SharedPrefs.read(SharedPrefs.FP_BOOLEAN, null);

        if (fpBoolean.equals("true")) {
            //withdrawal_progressBar.dismiss();
            requestFundsWithdrawalForBio(withdraw_member_number, withdraw_amount);
        } else if (fpBoolean.equals("false")){

            String coopnumber = SharedPrefs.read(SharedPrefs.VR_COOPORATE_NO, null);
            if (coopnumber !=null){
                switch (SharedPrefs.read(SharedPrefs.VR_COOPORATE_NO, null)) {
                    case "CAP005":
                        toast.swarnblue("For biometrics Authentication, Customer's fingerprints shall be required!");
                        break;
                    case "CAP022":
                        toast.swarnblue("For biometrics Authentication, Customer's fingerprints shall be required!");
                        break;
                    default:
                        toast.swarn("For biometrics Authentication, Customer's fingerprints shall be required!");
                        break;
                }

            }else {
                toast.swarn("For biometrics Authentication, Customer's fingerprints shall be required!");
            }

        }

    }

     void getPicture() {
        String URL = Api.MSACCO_AGENT + Api.PhotoSignature;
        final SignaturePhotoRequest signphotoreq = new SignaturePhotoRequest();
        signphotoreq.identifier = withdrawal_identifier.getText().toString();
        signphotoreq.identifiercode = "1";
        signphotoreq.agentid = SharedPrefs.read(SharedPrefs.AGENT_ID_1, null);
        signphotoreq.terminalid = MainDashboardActivity.imei;
        Api.instance(CashWithdrawal.this).request(URL, signphotoreq, new Api.RequestListener(){
            @SuppressLint("ResourceType")
            @Override
            public void onSuccess(String response) {
                try {
                    pbphotos.setVisibility(View.GONE);
                    JSONObject jsonObject = new JSONObject(response);
                    JSONObject jsonArray = jsonObject.getJSONObject("photosign");
                    String photo = jsonArray.getString("photo");
                    String signature = jsonArray.getString("signature");
                    if (photo.equals("N/A")) {
                        ByteArrayOutputStream baos = new ByteArrayOutputStream();
                        Bitmap bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.user);
                        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
                        byte[] imageBytes = baos.toByteArray();
                        String imageString = Base64.encodeToString(imageBytes, Base64.DEFAULT);
                        imageBytes = Base64.decode(imageString, Base64.DEFAULT);
                        final Bitmap decodedImage = BitmapFactory.decodeByteArray(imageBytes, 0, imageBytes.length);
                        picture.setImageBitmap(decodedImage);
                        pic_layout.setVisibility(View.VISIBLE);
                        final View passportphoto = findViewById(R.id.picture_imageview);
                        passportphoto.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                zoomImageFromThumb(passportphoto,decodedImage);
                            }
                        });
                        shortAnimationDuration = getResources().getInteger(
                                android.R.integer.config_shortAnimTime);
                    }
                    if (signature.equals("N/A")) {

                        ByteArrayOutputStream baos = new ByteArrayOutputStream();
                        Bitmap bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.signature);
                        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
                        byte[] imageBytes = baos.toByteArray();
                        String imageString = Base64.encodeToString(imageBytes, Base64.DEFAULT);

                        imageBytes = Base64.decode(imageString, Base64.DEFAULT);
                        Bitmap decodedImage = BitmapFactory.decodeByteArray(imageBytes, 0, imageBytes.length);
                        IVsignature.setImageBitmap(decodedImage);
                        pic_layout.setVisibility(View.VISIBLE);


                    }

                    if (signature.equals("null")) {
                        ByteArrayOutputStream baos = new ByteArrayOutputStream();
                        Bitmap bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.signature);
                        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
                        byte[] imageBytes = baos.toByteArray();
                        String imageString = Base64.encodeToString(imageBytes, Base64.DEFAULT);

                        //decode base64 string to image
                        imageBytes = Base64.decode(imageString, Base64.DEFAULT);
                        Bitmap decodedImage = BitmapFactory.decodeByteArray(imageBytes, 0, imageBytes.length);
                        IVsignature.setImageBitmap(decodedImage);
                        pic_layout.setVisibility(View.VISIBLE);

                    }
                    if (photo.equals("null")) {
                        ByteArrayOutputStream baos = new ByteArrayOutputStream();
                        Bitmap bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.user);
                        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
                        byte[] imageBytes = baos.toByteArray();
                        String imageString = Base64.encodeToString(imageBytes, Base64.DEFAULT);

                        //decode base64 string to image
                        imageBytes = Base64.decode(imageString, Base64.DEFAULT);
                        final Bitmap decodedImage = BitmapFactory.decodeByteArray(imageBytes, 0, imageBytes.length);
                        picture.setImageBitmap(decodedImage);
                        pic_layout.setVisibility(View.VISIBLE);

                        final View passportphoto = findViewById(R.id.picture_imageview);
                        passportphoto.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                zoomImageFromThumb(passportphoto,decodedImage);
                            }
                        });
                        shortAnimationDuration = getResources().getInteger(
                                android.R.integer.config_shortAnimTime);
                        }
                    if (!signature.equals("null") && !photo.equals("null") && !signature.equals("N/A") && !photo.equals("N/A")) {
                        pic_layout.setVisibility(View.VISIBLE);
                        final Bitmap decodedphoto = StringToBitMap(photo);
                        picture.setImageBitmap(decodedphoto);
                        Bitmap decodedsignature = StringToBitMap(signature);
                        IVsignature.setImageBitmap(decodedsignature);

                        final View passportphoto = findViewById(R.id.picture_imageview);
                        passportphoto.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                zoomImageFromThumb(passportphoto,decodedphoto);
                            }
                        });
                        shortAnimationDuration = getResources().getInteger(
                                android.R.integer.config_shortAnimTime);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onTokenExpired() {

            }

            @Override
            public void onError(String error) {
                Toast.makeText(CashWithdrawal.this, "Failed", Toast.LENGTH_SHORT).show();
            }
        });

    }


    public Bitmap StringToBitMap(String encodedString) {
        try {
            byte[] encodeByte = Base64.decode(encodedString, Base64.DEFAULT);
            Bitmap bitmap = BitmapFactory.decodeByteArray(encodeByte, 0, encodeByte.length);
            return bitmap;
        } catch (Exception e) {
            e.getMessage();
            return null;
        }
    }

    private void zoomImageFromThumb(final View thumbView,Bitmap imageResId) {
        // If there's an animation in progress, cancel it
        // immediately and proceed with this one.
        if (currentAnimator != null) {
            currentAnimator.cancel();
        }

        final ImageView expandedImageView = (ImageView) findViewById(
                R.id.expanded_image);
        expandedImageView.setImageBitmap(imageResId);


        // Calculate the starting and ending bounds for the zoomed-in image.
        // This step involves lots of math. Yay, math.
        final Rect startBounds = new Rect();
        final Rect finalBounds = new Rect();
        final Point globalOffset = new Point();

        // The start bounds are the global visible rectangle of the thumbnail,
        // and the final bounds are the global visible rectangle of the container
        // view. Also set the container view's offset as the origin for the
        // bounds, since that's the origin for the positioning animation
        // properties (X, Y).
        thumbView.getGlobalVisibleRect(startBounds);
        findViewById(R.id.container)
                .getGlobalVisibleRect(finalBounds, globalOffset);
        startBounds.offset(-globalOffset.x, -globalOffset.y);
        finalBounds.offset(-globalOffset.x, -globalOffset.y);

        // Adjust the start bounds to be the same aspect ratio as the final
        // bounds using the "center crop" technique. This prevents undesirable
        // stretching during the animation. Also calculate the start scaling
        // factor (the end scaling factor is always 1.0).
        float startScale;
        if ((float) finalBounds.width() / finalBounds.height()
                > (float) startBounds.width() / startBounds.height()) {
            // Extend start bounds horizontally
            startScale = (float) startBounds.height() / finalBounds.height();
            float startWidth = startScale * finalBounds.width();
            float deltaWidth = (startWidth - startBounds.width()) / 2;
            startBounds.left -= deltaWidth;
            startBounds.right += deltaWidth;
        } else {
            // Extend start bounds vertically
            startScale = (float) startBounds.width() / finalBounds.width();
            float startHeight = startScale * finalBounds.height();
            float deltaHeight = (startHeight - startBounds.height()) / 2;
            startBounds.top -= deltaHeight;
            startBounds.bottom += deltaHeight;
        }

        // Hide the thumbnail and show the zoomed-in view. When the animation
        // begins, it will position the zoomed-in view in the place of the
        // thumbnail.
        thumbView.setAlpha(0f);
        expandedImageView.setVisibility(View.VISIBLE);

        // Set the pivot point for SCALE_X and SCALE_Y transformations
        // to the top-left corner of the zoomed-in view (the default
        // is the center of the view).
        expandedImageView.setPivotX(0f);
        expandedImageView.setPivotY(0f);

        // Construct and run the parallel animation of the four translation and
        // scale properties (X, Y, SCALE_X, and SCALE_Y).
        AnimatorSet set = new AnimatorSet();
        set
                .play(ObjectAnimator.ofFloat(expandedImageView, View.X,
                        startBounds.left, finalBounds.left))
                .with(ObjectAnimator.ofFloat(expandedImageView, View.Y,
                        startBounds.top, finalBounds.top))
                .with(ObjectAnimator.ofFloat(expandedImageView, View.SCALE_X,
                        startScale, 1f))
                .with(ObjectAnimator.ofFloat(expandedImageView,
                        View.SCALE_Y, startScale, 1f));
        set.setDuration(shortAnimationDuration);
        set.setInterpolator(new DecelerateInterpolator());
        set.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                currentAnimator = null;
            }

            @Override
            public void onAnimationCancel(Animator animation) {
                currentAnimator = null;
            }
        });
        set.start();
        currentAnimator = set;

        // Upon clicking the zoomed-in image, it should zoom back down
        // to the original bounds and show the thumbnail instead of
        // the expanded image.
        final float startScaleFinal = startScale;
        expandedImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (currentAnimator != null) {
                    currentAnimator.cancel();
                }

                // Animate the four positioning/sizing properties in parallel,
                // back to their original values.
                AnimatorSet set = new AnimatorSet();
                set.play(ObjectAnimator
                        .ofFloat(expandedImageView, View.X, startBounds.left))
                        .with(ObjectAnimator
                                .ofFloat(expandedImageView,
                                        View.Y,startBounds.top))
                        .with(ObjectAnimator
                                .ofFloat(expandedImageView,
                                        View.SCALE_X, startScaleFinal))
                        .with(ObjectAnimator
                                .ofFloat(expandedImageView,
                                        View.SCALE_Y, startScaleFinal));
                set.setDuration(shortAnimationDuration);
                set.setInterpolator(new DecelerateInterpolator());
                set.addListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        thumbView.setAlpha(1f);
                        expandedImageView.setVisibility(View.GONE);
                        currentAnimator = null;
                    }

                    @Override
                    public void onAnimationCancel(Animator animation) {
                        thumbView.setAlpha(1f);
                        expandedImageView.setVisibility(View.GONE);
                        currentAnimator = null;
                    }
                });
                set.start();
                currentAnimator = set;
            }
        });
    }

}
