package com.coretec.agencyApplication.activities.gfl;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.PorterDuff;
import android.support.design.widget.TextInputLayout;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.cloudpos.jniinterface.PrinterInterface;
import com.coretec.agencyApplication.R;
import com.coretec.agencyApplication.activities.BaseActivity;
import com.coretec.agencyApplication.activities.newmodules.ValidateTunzhengbigBio;
import com.coretec.agencyApplication.api.GFL;
import com.coretec.agencyApplication.api.requests.FingerPrintRequest;
import com.coretec.agencyApplication.api.requests.GrowersDetailsRequest;
import com.coretec.agencyApplication.api.requests.MpesaChangeRequest;
import com.coretec.agencyApplication.api.responses.FingerPrintResponse;
import com.coretec.agencyApplication.api.responses.GrowersDetailsResponse;
import com.coretec.agencyApplication.api.responses.MpesaChangeResponse;
import com.coretec.agencyApplication.model.GrowerDetails;
import com.coretec.agencyApplication.utils.QuickToast;
import com.coretec.agencyApplication.utils.SharedPrefs;
import com.coretec.agencyApplication.wizarpos.function.printer.PrinterCommand;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static com.coretec.agencyApplication.utils.Utils.getFormattedDate;
import static com.coretec.agencyApplication.utils.Utils.getLat;
import static com.coretec.agencyApplication.utils.Utils.getLong;

public class MpesaChange extends AppCompatActivity {

    private QuickToast toast = new QuickToast(this);
    private TextInputLayout input_layout_id_number;
    private EditText identifier_deposit;
    TextView oldPhone;
    private EditText reason;
    private EditText newNumber;
    ImageView backBtn;
    Button submit;
    private Spinner spinner;
    private ArrayList<String> arrayList = new ArrayList<>();
    private ArrayAdapter<GrowerDetails> adapter;

    private Spinner spinner2;
    private ArrayList<String> arrayList2 = new ArrayList<>();
    private ArrayAdapter<String> adapter2;

    private String identifiercode = "";
    private String growerNo = "";
    String text;

    private String item;
    ProgressBar progressBar;
    ProgressDialog progressDialog;
    AlertDialog fingerprintDialog;
    private MpesaChangeResponse mpesaChangeResponse;
    private RadioGroup radioGroupPU;
    private RadioButton rb1,rb2;

    SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.gfl_mpesa_change);
        SharedPrefs.init(getApplicationContext());

        this.overridePendingTransition(R.anim.move_right_in_activity, R.anim.move_left_out_activity);

        SharedPrefs.write(SharedPrefs.FP_BOOLEAN, "");
        SharedPrefs.write(SharedPrefs.MPESA_G_NUMBER, "");

        Toolbar toolbar = findViewById(R.id.toolbar);
        TextView mTitle = toolbar.findViewById(R.id.toolbar_title);
        mTitle.setText(getResources().getText(R.string.mpesa_change));
        setSupportActionBar(toolbar);
        ActionBar actionbar = getSupportActionBar();
        actionbar.setDisplayHomeAsUpEnabled(true);
        actionbar.setHomeAsUpIndicator(R.drawable.ic_arrow_back);

        newNumber = findViewById(R.id.new_number);
        oldPhone = findViewById(R.id.oldPhone);
        newNumber.setSelection(newNumber.getText().length());
        newNumber.setInputType(InputType.TYPE_NULL);
        radioGroupPU = findViewById(R.id.radioGroupPU);
        rb1 = findViewById(R.id.rb1);
        rb2 = findViewById(R.id.rb2);

        newNumber.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // showMyDialog();
            }
        });

        newNumber.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    // showMyDialog();
                }
            }
        });

        TextWatcher textWatcher1 = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                String inputPhone = newNumber.getText().toString();

                if (inputPhone.length() == 1) {
                    switch (inputPhone) {
                        case "0":
                            newNumber.setText("+254");
                            newNumber.setSelection(newNumber.getText().length());
                            toast.sinfo("Correct format auto completed. Please proceed!");
                            break;
                        case "7":
                            newNumber.setText("+2547");
                            newNumber.setSelection(newNumber.getText().length());
                            toast.sinfo("Correct format auto completed. Please proceed!");
                            break;
                        default:
                            newNumber.setText(null);
                            toast.swarn("Please enter correct phone number format!");
                            break;
                    }
                }

            }
        };
        newNumber.addTextChangedListener(textWatcher1);


        radioGroupPU.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (checkedId == R.id.rb1) {
                    SharedPrefs.write(SharedPrefs.UPDATE_PU_REGISTERED, "true");
                }else if (checkedId == R.id.rb2){
                    SharedPrefs.write(SharedPrefs.UPDATE_PU_REGISTERED, "false");
                }
            }
        });

        submit = findViewById(R.id.btnSubmit);
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String new_phone = newNumber.getText().toString();
                String old_phone = oldPhone.getText().toString();
                String fourthChar = new_phone.substring(0, 5);
                String regexStr = "^\\+[0-9]{12,13}$";
                if (identifier_deposit.getText().toString().matches("") || newNumber.getText().toString().matches("") || reason.getText().toString().matches("")) {
                    toast.swarn("Please fill in the missing fields to proceed!");
                } else if (!new_phone.matches(regexStr)) {
                    //newNumber.setText(newNumber.length() - 1);
                    toast.swarn("Please input a valid phone number");
                } else if (old_phone.equals(new_phone)) {
                    toast.swarn("New number matches old phone number!");
                } else if (fourthChar.equals("+2540")) {
                    toast.swarn("Invalid phone number!");
                } else {

                    /* progress dialog */
                    progressDialog = new ProgressDialog(MpesaChange.this);
                    progressDialog.setMessage("Please wait...");
                    progressDialog.setCancelable(false);
                    progressDialog.show();
                    //submitMpesaChange();
                    requestFingerPrints();
                    /*String gNumber = SharedPrefs.read(SharedPrefs.MPESA_G_NUMBER, null);
                    int result = PrinterInterface.open();
                    header(gNumber);
                    PrinterInterface.close();

                    submitMpesaChange();*/

                }
            }
        });

        input_layout_id_number = findViewById(R.id.input_layout_id_number);
        identifier_deposit = findViewById(R.id.id_number);

        identifier_deposit.setInputType(InputType.TYPE_NULL);
        identifier_deposit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // showMyDialog();
            }
        });

        identifier_deposit.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    // showMyDialog();
                }
            }
        });

        identifier_deposit.setText(SharedPrefs.read(SharedPrefs.IDENTIFIER_FOR_UPDATING, null));
        spinner = findViewById(R.id.growerSpinner);
        spinner2 = findViewById(R.id.spinner_deposit);

        progressBar = findViewById(R.id.growersPb);
        reason = findViewById(R.id.reason);

        if (identifier_deposit.getText().toString().length() >= 7) {
            progressBar.setVisibility(View.VISIBLE);
            requestGetAccountDetails(identifier_deposit.getText().toString());
            progressBar.setVisibility(View.GONE);
        }


        adapter = new ArrayAdapter<GrowerDetails>(getApplicationContext(),
                R.layout.simple_spinner_dropdown_item, new ArrayList<GrowerDetails>());
        adapter.setDropDownViewResource(R.layout.simple_spinner_dropdown_item);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                 text = spinner.getSelectedItem().toString();

                SharedPrefs.write(SharedPrefs.MPESA_G_NUMBER, text);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        adapter2 = new ArrayAdapter<String>(getApplicationContext(), R.layout.simple_spinner_dropdown_item, arrayList2);
        adapter2.setDropDownViewResource(R.layout.simple_spinner_dropdown_item);
        spinner2.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                identifiercode = String.valueOf(position);
                // Toast.makeText(getApplicationContext(),String.valueOf(position),Toast.LENGTH_LONG).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }

    void requestFingerPrints() {

        final String TAG = "GET FINGERPRINTS";
        String URL = GFL.MSACCO_AGENT + GFL.GetFourFingerPrint;

        final FingerPrintRequest fingerPrintRequest = new FingerPrintRequest();
        fingerPrintRequest.accountidentifier = identifier_deposit.getText().toString();
        fingerPrintRequest.accountidentifiercode = "1";
        fingerPrintRequest.agentid = SharedPrefs.read(SharedPrefs.AGENT_ID_1, null);
        fingerPrintRequest.terminalid = MainGFLDashboard.imei;//SharedPrefs.read(SharedPrefs.DEVICE_ID, null);
        fingerPrintRequest.longitude = getLong(this);
        fingerPrintRequest.latitude = getLat(this);
        fingerPrintRequest.date = getFormattedDate();

        Log.e(TAG, fingerPrintRequest.getBody().toString());

        GFL.instance(this).request(URL, fingerPrintRequest, new GFL.RequestListener() {
            @Override
            public void onSuccess(String response) {
                /*deposit_progressBar.setVisibility(View.GONE);*/
                //progressDialog.dismiss();
                FingerPrintResponse fingerPrintResponse = GFL.instance(MpesaChange.this)
                        .mGson.fromJson(response, FingerPrintResponse.class);
                if (fingerPrintResponse.is_successful) {



                    try {
                        JSONObject jsonObject = new JSONObject(response);

                        String leftRing = jsonObject.getString("leftring");
                        String leftLittle = jsonObject.getString("leftlittle");
                        String rightRing = jsonObject.getString("rightring");
                        String rightLittle = jsonObject.getString("rightlittle");

                        SharedPrefs.write(SharedPrefs.LEFT_RING, leftRing);
                        SharedPrefs.write(SharedPrefs.LEFT_LITTLE, leftLittle);
                        SharedPrefs.write(SharedPrefs.RIGHT_RING, rightRing);
                        SharedPrefs.write(SharedPrefs.RIGHT_LITTLE, rightLittle);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    SharedPrefs.transactionstypelogs(7, MpesaChange.this);
                    SharedPrefs.growerslogs(text, MpesaChange.this);
                    startActivity(new Intent(MpesaChange.this, ValidateTunzhengbigBio.class));

                } else {
                    progressDialog.dismiss();
                    toast.swarn("Could not get customer finger prints!");
                }

            }

            @Override
            public void onTokenExpired() {

            }

            @Override
            public void onError(String error) {

            }
        });

    }

    private void submitMpesaChange() {

        final String TAG = "MPESA CHANGE";
        String URL = GFL.MSACCO_AGENT + GFL.MpesaChange;

        final MpesaChangeRequest mpesaChange = new MpesaChangeRequest();
        mpesaChange.id = identifier_deposit.getText().toString();
        mpesaChange.growersno = "";
        mpesaChange.accountname = "";
        mpesaChange.phoneno = newNumber.getText().toString();
        mpesaChange.salescode = "";
        mpesaChange.userid = "";
        mpesaChange.reason = reason.getText().toString();
        mpesaChange.puregister = SharedPrefs.read(SharedPrefs.UPDATE_PU_REGISTERED, null);
        mpesaChange.transactiontype = "Mpesa Change";
        mpesaChange.agentid = SharedPrefs.read(SharedPrefs.AGENT_ID_1, null);
        mpesaChange.terminalid = SharedPrefs.read(SharedPrefs.DEVICE_ID, null);
        mpesaChange.longitude = getLong(this);
        mpesaChange.latitude = getLat(this);
        mpesaChange.date = getFormattedDate();
        mpesaChange.corporate_no = "CAP016";


        Log.e(TAG, mpesaChange.getBody().toString());
        GFL.instance(this).request(URL, mpesaChange, new GFL.RequestListener() {
            @Override
            public void onSuccess(String response) {
                /*deposit_progressBar.setVisibility(View.GONE);*/
                progressDialog.dismiss();
                 mpesaChangeResponse = GFL.instance(MpesaChange.this)
                        .mGson.fromJson(response, MpesaChangeResponse.class);
                if (!mpesaChangeResponse.is_successful){
                    AlertDialog.Builder builder1 = new AlertDialog.Builder(MpesaChange.this);
                    builder1.setMessage(mpesaChangeResponse.getError());
                    builder1.setCancelable(true);

                    builder1.setPositiveButton(
                            "Okay",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                   finish();
                                }
                            });
                    AlertDialog alert11 = builder1.create();
                    alert11.show();
                }else{
                    String idno = identifier_deposit.getText().toString();
                    String old_phone = SharedPrefs.read(SharedPrefs.OLD_PHONE, null);
                    String new_phone = newNumber.getText().toString();
                    String sababu = reason.getText().toString();

                    int result = PrinterInterface.open();
                    writetest(idno, old_phone, new_phone, sababu);
                    PrinterInterface.close();

                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        String receiptNo = jsonObject.getString("receiptNo");

                        //button submit disabled after submitting...
                        submit.setEnabled(false);
                        String success = "Mpesa change request received and sent for approval...";
                        android.app.AlertDialog.Builder builder1 = new android.app.AlertDialog.Builder(MpesaChange.this);
                        builder1.setMessage(success);
                        builder1.setTitle("Success!");
                        builder1.setCancelable(true);

                        builder1.setPositiveButton(
                                "DONE",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        Intent intent = new Intent(getApplicationContext(), MainGFLDashboard.class);
                                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                        startActivity(intent);
                                    }
                                });

                        android.app.AlertDialog alert11 = builder1.create();
                        alert11.show();
                        toast.ssuccess(receiptNo);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
               /* else {
                    //
                }*/

            }

            @Override
            public void onTokenExpired() {

            }

            @Override
            public void onError(String error) {

            }
        });

    }

    public void header(String growerNumber) {

        //growerNumber
        String firstG = growerNumber.substring(0, 2);
        String lastG = growerNumber.substring(6, growerNumber.length());
        String maskedG = firstG + "*****" + lastG;

        try {
            byte[] start = null;
            byte[] saccoName = null;
            byte[] type = null;
            byte[] isDefa = null;
            byte[] gno = null;

            try {
                start = String.valueOf("              ").getBytes("GB2312");
                saccoName = String.valueOf("      GREENLAND FEDHA LTD     ").getBytes("GB2312");
                type = String.valueOf("      MPESA CHANGE REQUEST     ").getBytes("GB2312");
                gno = String.valueOf("Grower Number : " + String.valueOf(maskedG)).getBytes("GB2312");
                isDefa = String.valueOf("Defaulter: " + SharedPrefs.read(SharedPrefs.IS_DEFAULTER, null)).getBytes("GB2312");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            PrinterInterface.begin();
            byte[] cmd;
            cmd = new byte[]{(byte) 0x1B, (byte) 0x40};

            cmd = new byte[]{
                    (byte) 0x1B, (byte) 0x45, (byte) 0};

            write(cmd);
            write(start);
            writeLineBreak(1);
            write(saccoName);
            writeLineBreak(2);
            write(type);
            writeLineBreak(1);
            write(gno);
            writeLineBreak(1);
            write(isDefa);
            writeLineBreak(1);
            PrinterInterface.end();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void writetest(String id, String oPhone, String new_phone, String sababu) {
        try {
            byte[] idNumber = null;
            byte[] oldPhone = null;
            byte[] newPhone = null;
            byte[] reas = null;
            byte[] reason = null;
            byte[] time = null;
            byte[] nameAgent = null;
            byte[] motto = null;

            SimpleDateFormat timeStampFormat = new SimpleDateFormat("yyyy/MM/dd - HH:mm");
            Date myDate = new Date();
            String timeCurrent = timeStampFormat.format(myDate);
            String aName = SharedPrefs.read(SharedPrefs.AGENT_NAME, null);

            //String middle = idNos.substring(2, 6);
            String first = id.substring(0, 2);
            String last = id.substring(6, id.length());
            String maskedId = first + "****" + last;

            //Old Phone Number
            String firstOP = oPhone.substring(0, 7);
            String lastOP = oPhone.substring(11, oPhone.length());
            String maskedOP = firstOP + "*****" + lastOP;

            //New phone number
            String firstNP = new_phone.substring(0, 7);
            String lastNP = new_phone.substring(11, new_phone.length());
            String maskedNP = firstNP + "*****" + lastNP;

            try {
                idNumber = String.valueOf("ID Number : " + String.valueOf(maskedId)).getBytes("GB2312");
                oldPhone = String.valueOf("Old Phone Number: " + String.valueOf(maskedOP)).getBytes("GB2312");
                newPhone = String.valueOf("New Phone Number: " + String.valueOf(maskedNP)).getBytes("GB2312");
                reas = String.valueOf("Reason : ").getBytes("GB2312");
                reason = String.valueOf(sababu).getBytes("GB2312");
                time = String.valueOf("   Date : " + String.valueOf(timeCurrent)).getBytes("GB2312");
                nameAgent = String.valueOf("You were served by " + String.valueOf(aName)).getBytes("GB2312");
                motto = String.valueOf("         GFL Motto       ").getBytes("GB2312");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            PrinterInterface.begin();
            byte[] cmd;
            cmd = new byte[]{(byte) 0x1B, (byte) 0x40};

            cmd = new byte[]{
                    (byte) 0x1B, (byte) 0x45, (byte) 0};

            write(cmd);
            writeLineBreak(1);
            write(idNumber);
            writeLineBreak(1);
            write(oldPhone);
            // print line break
            writeLineBreak(1);
            write(newPhone);
            writeLineBreak(1);
            write(reas);
            writeLineBreak(1);
            write(reason);
            writeLineBreak(2);
            write(time);
            writeLineBreak(2);
            write(nameAgent);
            writeLineBreak(1);
            write(motto);
            // print line break
            writeLineBreak(4);
            writeLineBreak(5);
            PrinterInterface.end();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void write(final byte[] arryData) {
        int result = PrinterInterface.write(arryData, arryData.length);
    }

    // print line break
    private void writeLineBreak(int lineNumber) {
        // write(new byte[]{'\n'});
        write(PrinterCommand.getCmdEscDN(lineNumber));
    }

    void requestGetAccountDetails(String identifier) {

        final String TAG = "GrowersAccounts";
//        EasyLocationMod easyLocationMod = new EasyLocationMod(CashWithdrawal.this);

        String URL = GFL.MSACCO_AGENT + GFL.GetGrowersDetails;

        final GrowersDetailsRequest growersDetails = new GrowersDetailsRequest();
        growersDetails.agentid = SharedPrefs.read(SharedPrefs.AGENT_ID_1, null);
        growersDetails.corporateno = "CAP016";
        growersDetails.accountidentifier = identifier;
        growersDetails.accountidentifiercode = "1";
        growersDetails.transactiontype = "1";
        growersDetails.terminalid = SharedPrefs.read(SharedPrefs.DEVICE_ID, null);
        growersDetails.longitude = getLong(this);
        growersDetails.latitude = getLat(this);
        growersDetails.date = getFormattedDate();

        Log.e(TAG, growersDetails.getBody().toString());
        //clear contents of spinner
        arrayList.clear();
        spinner = findViewById(R.id.growerSpinner);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(MpesaChange.this, android.R.layout.simple_spinner_item, arrayList);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(null);

        GFL.instance(this).request(URL, growersDetails, new GFL.RequestListener() {
            @Override
            public void onSuccess(String response) {
                /*deposit_progressBar.setVisibility(View.GONE);*/

                GrowersDetailsResponse growersDetailsResponse = GFL.instance(MpesaChange.this)
                        .mGson.fromJson(response, GrowersDetailsResponse.class);
                if (growersDetailsResponse.is_successful) {
                    List<GrowerDetails> growerDetailsList = new ArrayList<>();

                    try {
                        JSONObject jsonObject = new JSONObject(response);

                        JSONArray jsonArray = jsonObject.getJSONArray("Growers");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                            JSONArray jsonArray1 = jsonObject1.getJSONArray("GrowerDetails");

                            if (jsonArray1.length() < 1) {
                                progressBar.setVisibility(View.GONE);
                                Toast.makeText(MpesaChange.this, "", Toast.LENGTH_SHORT).show();

                            } else {

                                for (int l = 0; l < jsonArray1.length(); l++) {
                                    JSONObject jsonObject2 = jsonArray1.getJSONObject(l);
                                    progressBar.setVisibility(View.GONE);
                                    arrayList.add(String.valueOf(jsonObject2.getString("GrowerNo")));
                                    /*mpesaPhoneNumber = jsonObject2.getString("phonenumber");*/
                                    String gNo = jsonObject2.getString("GrowerNo");
                                    String pNumber = jsonObject2.getString("GrowerMobileNo");
                                    String isDefaulter = jsonObject2.getString("LoanDefaulter");

                                    SharedPrefs.write(SharedPrefs.IS_DEFAULTER, isDefaulter);

                                    SharedPrefs.write(SharedPrefs.OLD_PHONE, pNumber);

                                    oldPhone = findViewById(R.id.oldPhone);
                                    oldPhone.setText(pNumber);
                                }
                            }

                        }

                        //Growers Number
                        spinner = findViewById(R.id.growerSpinner);
                        ArrayAdapter<String> adapter = new ArrayAdapter<String>(MpesaChange.this, android.R.layout.simple_spinner_item, arrayList);
                        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                        spinner.setAdapter(adapter);


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                } else {
                  //
                }
            }

            @Override
            public void onTokenExpired() {

            }

            @Override
            public void onError(String error) {

            }
        });
    }

    @Override
    protected void onResume() {
        SharedPrefs.init(getApplicationContext());
        super.onResume();
        String fpBoolean = SharedPrefs.read(SharedPrefs.FP_BOOLEAN, null);

        if (fpBoolean.equals("true")) {
            progressDialog.dismiss();
            String gNumber = SharedPrefs.read(SharedPrefs.MPESA_G_NUMBER, null);
            int result = PrinterInterface.open();
            header(gNumber);
            PrinterInterface.close();
            submitMpesaChange();

        } else if (fpBoolean.equals("false")){
            progressDialog.dismiss();
            toast.swarn("Please authenticate to complete transaction!");
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.move_left_in_activity, R.anim.move_right_out_activity);
    }

}
