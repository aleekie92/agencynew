package com.coretec.agencyApplication.activities.newmodules;


import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.cloudpos.fingerprint.FingerprintDevice;
import com.coretec.agencyApplication.R;
import com.coretec.agencyApplication.activities.BaseActivity;
import com.coretec.agencyApplication.activities.gfl.MainGFLDashboard;
import com.coretec.agencyApplication.api.GFL;
import com.coretec.agencyApplication.api.requests.GetMatchLogsRequest;
import com.coretec.agencyApplication.api.responses.GetMatchlogsResponse;
import com.coretec.agencyApplication.fingerprint.bfp.demo.HexString;
import com.coretec.agencyApplication.fingerprint.bfp.demo.TestConstant;
import com.coretec.agencyApplication.fingerprint.bfp.mgr.FPMgrImpl;
import com.coretec.agencyApplication.fingerprint.bfp.mgr.IFPMgr;
import com.coretec.agencyApplication.fingerprint.util.ByteConvert;
import com.coretec.agencyApplication.fingerprint.util.TextViewUtil;
import com.coretec.agencyApplication.utils.QuickToast;
import com.coretec.agencyApplication.utils.SharedPrefs;
import com.digitalpersona.uareu.Engine;
import com.digitalpersona.uareu.Fmd;
import com.digitalpersona.uareu.Importer;
import com.digitalpersona.uareu.UareUException;
import com.digitalpersona.uareu.UareUGlobal;
import com.upek.android.ptapi.PtConstants;
import com.upek.android.ptapi.PtException;
import com.upek.android.ptapi.struct.PtInputBir;

import java.util.ArrayList;
import java.util.List;

import static com.coretec.agencyApplication.utils.Utils.getFormattedDate;
import static com.coretec.agencyApplication.utils.Utils.getLat;
import static com.coretec.agencyApplication.utils.Utils.getLong;

public class ValidateCrossmatchBio extends AppCompatActivity {

    private Button identify, dismiss;
    private TextView show;
    //private Context context = this;
    private Handler handler;
    private FingerprintDevice fingerprintDevice;
    private static final int MSGID_SHOW_MESSAGE = 0;
    private SharedPreferences preferences;
    private SharedPreferences.Editor editor;

    //crossmatch fingerprint declarations
    protected TextView log_text;
    protected Handler mHandler = null;
    protected Runnable runnable = null;
    public Engine.Candidate[] results;
    Importer im = null;
    Engine m_engine = null;

    private QuickToast toast = new QuickToast(ValidateCrossmatchBio.this);
    boolean doubleBackToExitPressedOnce = false;
    public static boolean matched;

    @SuppressLint("HandlerLeak")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        SharedPrefs.init(getApplicationContext());
        // Make us non-modal, so that others can receive touch events.
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL, WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL);

        // ...but notify us that it happened.
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_WATCH_OUTSIDE_TOUCH, WindowManager.LayoutParams.FLAG_WATCH_OUTSIDE_TOUCH);

        setContentView(R.layout.finger_print_dialog);
        dismiss = findViewById(R.id.btn_dismiss);
        dismiss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPrefs.write(SharedPrefs.FP_BOOLEAN, "false");
                IFPMgr fpMgr = FPMgrImpl.getInstance();
                fpMgr.close();
                finish();
                Intent i=new Intent();

                if (SharedPrefs.read(SharedPrefs.COPORATE, null).equals("gfl")){
                    i = new Intent(ValidateCrossmatchBio.this, MainGFLDashboard.class);
                }if(SharedPrefs.read(SharedPrefs.COPORATE, null).equals("notgfl")) {
                    i = new Intent(ValidateCrossmatchBio.this, MainDashboardActivity.class);
                }

                i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                //i.addFlags(IntentCompat.FLAG_ACTIVITY_CLEAR_TASK);
                i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(i);
            }
        });
        show = findViewById(R.id.textView);
        //show.setMovementMethod(ScrollingMovementMethod.getInstance());
        show.setMovementMethod(new ScrollingMovementMethod());

        //log_text = this.findViewById(R.id.text_result);
        show.setMovementMethod(ScrollingMovementMethod.getInstance());
        try {
            im = UareUGlobal.GetImporter();
            m_engine = UareUGlobal.GetEngine();
        } catch (Exception e) {
            e.printStackTrace();
        }
        mHandler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                String str = msg.obj + "\n";
                switch (msg.what) {
                    case R.id.log_default:
                        show.setText(str);
                        break;
                    case R.id.log_success:
                        // String str = msg.obj + "\n";
                        TextViewUtil.infoBlueTextView(show, str);
                        break;
                    case R.id.log_failed:
                        // String str = msg.obj + "\n";
                        TextViewUtil.infoRedTextView(show, str);
                        break;
                    case R.id.log_clean:
                        show.setText("");
                        break;
                }
            }
        };

        handler = new Handler(new Handler.Callback() {
            //callback method
            @Override
            public boolean handleMessage(Message msg) {

                switch (msg.what) {
                    case MSGID_SHOW_MESSAGE:
                        show.append(msg.obj.toString() + "\n");//Through the back pass over the information displayed on the TextView
                        break;
                    default:
                        break;
                }
                return false;
            }
        });

        identify = findViewById(R.id.btn_identify);

        identify.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPreferences prefs = getSharedPreferences("userFinger", Context.MODE_PRIVATE);

                String finger1 = SharedPrefs.read(SharedPrefs.LEFT_RING, null);
                String finger2 = SharedPrefs.read(SharedPrefs.LEFT_LITTLE, null);
                String finger3 = SharedPrefs.read(SharedPrefs.RIGHT_RING, null);
                String finger4 = SharedPrefs.read(SharedPrefs.RIGHT_LITTLE, null);


                crossMatch(finger1, finger2, finger3, finger4);

            }
        });

    }

    private void writerLogInTextview(String log, int id) {
        Message msg = new Message();
        msg.what = id;
        msg.obj = log;
        mHandler.sendMessage(msg);
    }

    Thread th = null;

    public void crossMatch(final String finger1, final String finger2, final String finger3, final String finger4) {
        if (th == null || th.getState() == Thread.State.TERMINATED) {
            th = new Thread() {
                @SuppressWarnings("unused")
                public void run() {
                    IFPMgr fpMgr = FPMgrImpl.getInstance();
                    try {

                        fpMgr.open(getApplicationContext());

                        fpMgr.deleteAll(getApplicationContext());

                        List<PtInputBir> birList = new ArrayList<PtInputBir>();
                        // fpMgr.test(FpActivity.this, FingerId.NONE);
                        writerLogInTextview("Put finger to scan ", R.id.log_success);
                        // PtInputBir template = null;

                        // template = fpMgr.enrollFp(FpActivity.this,
                        // FingerId.NONE);
                        try {
                            byte[] image = fpMgr
                                    .GrabImage(PtConstants.PT_GRAB_TYPE_508_508_8_SCAN508_508_8);
                            int iWidth = fpMgr.getImagewidth();
                            Fmd fm = ConvertImgToIsoTemplate(image, iWidth);
                            Fmd fm1 = im.ImportFmd(ByteConvert.parseHexStr2Byte(finger1),
                                    Fmd.Format.ISO_19794_2_2005, Fmd.Format.ISO_19794_2_2005);
                            Fmd fm2 = im.ImportFmd(ByteConvert.parseHexStr2Byte(finger2),
                                    Fmd.Format.ISO_19794_2_2005, Fmd.Format.ISO_19794_2_2005);
                            Fmd fm3 = im.ImportFmd(ByteConvert.parseHexStr2Byte(finger3),
                                    Fmd.Format.ISO_19794_2_2005, Fmd.Format.ISO_19794_2_2005);
                            Fmd fm4 = im.ImportFmd(ByteConvert.parseHexStr2Byte(finger4),
                                    Fmd.Format.ISO_19794_2_2005, Fmd.Format.ISO_19794_2_2005);
                            Fmd fm5 = im.ImportFmd(ByteConvert.parseHexStr2Byte(TestConstant.FP1),
                                    Fmd.Format.ISO_19794_2_2005, Fmd.Format.ISO_19794_2_2005);

                            Fmd[] m_fmds_temp = new Fmd[]{
                                    fm1, fm2, fm3, fm4, fm5
                            };

                            if (fm != null) {

                                String data = HexString.bufferToHex(fm.getData());
                                Log.e("ScannedFinger", "" + finger1);

                                //compare myself, score: 0=match
                            /*int score = m_engine.Compare(fm,0,fm,0);
                            Log.e("BasicSample", String.format("score %d" , score ));*/

                                //compare with others
                                results = m_engine.Identify(fm, 0, m_fmds_temp, 200000, 3);
                                int m_score = 0;
                                int m_score1 = 0;
                                int m_score2 = 0;
                                int m_score3 = 0;
                                int m_score4 = 0;

                                if (results.length != 0) {
                                    m_score = m_engine.Compare(m_fmds_temp[0], 0, fm, 0);
                                    m_score1 = m_engine.Compare(m_fmds_temp[1], 0, fm, 0);
                                    m_score2 = m_engine.Compare(m_fmds_temp[2], 0, fm, 0);
                                    m_score3 = m_engine.Compare(m_fmds_temp[3], 0, fm, 0);
                                    m_score4 = m_engine.Compare(m_fmds_temp[4], 0, fm, 0);
                                    //i should be the number of fingerprints
                                    /*for (int i = 0; i < 5; i++) {
                                     *//*m_score = m_engine.Compare(m_fmds_temp[0], 0, fm, 0);*//*
                                     *//*writerLogInTextview("match result !" + m_score,
                                            R.id.log_success);*//*
                                }*/
                                    if (m_score >= 0 && m_score <= 10) {
                                        matched=true;
                                        requestMatchLogs();
                                        writerLogInTextview("Matched successfully !" + m_score,
                                                R.id.log_success);
                                        SharedPrefs.write(SharedPrefs.FP_BOOLEAN, "true");
                                        finish();
                                    } else if (m_score1 >= 0 && m_score1 <= 10) {
                                        matched=true;
                                        requestMatchLogs();
                                        writerLogInTextview("Matched successfully !" + m_score1,
                                                R.id.log_success);
                                        SharedPrefs.write(SharedPrefs.FP_BOOLEAN, "true");
                                        finish();
                                    } else if (m_score2 >= 0 && m_score2 <= 10) {
                                        matched=true;
                                        requestMatchLogs();
                                        writerLogInTextview("Matched successfully !" + m_score2,
                                                R.id.log_success);
                                        SharedPrefs.write(SharedPrefs.FP_BOOLEAN, "true");
                                        finish();
                                    } else if (m_score3 >= 0 && m_score3 <= 10) {
                                        matched=true;
                                        requestMatchLogs();
                                        writerLogInTextview("Matched successfully !" + m_score3,
                                                R.id.log_success);
                                        SharedPrefs.write(SharedPrefs.FP_BOOLEAN, "true");
                                        finish();
                                    } else if (m_score4 >= 0 && m_score4 <= 10) {
                                        matched=true;
                                        requestMatchLogs();
                                        writerLogInTextview("Matched successfully !" + m_score4,
                                                R.id.log_success);
                                        SharedPrefs.write(SharedPrefs.FP_BOOLEAN, "true");
                                        finish();
                                    } else {
                                        matched=false;
                                        requestMatchLogs();
                                        writerLogInTextview("fingerprint not recognised, please try again later!",
                                                R.id.log_success);
                                        SharedPrefs.write(SharedPrefs.FP_BOOLEAN, "false");
                                        //finish();
                                    }
                                } else {
                                    m_score = -1;
                                    matched=false;
                                    requestMatchLogs();
                                    writerLogInTextview("match failed try again!" + m_score, R.id.log_failed);
                                    SharedPrefs.write(SharedPrefs.FP_BOOLEAN, "false");
                                }

                            } else {
                                Log.e("fm", "fm is null");
                                writerLogInTextview("match failed !", R.id.log_failed);
                            }

                        } catch (PtException e) {
                            writerLogInTextview("Please reposition your finger properly!" + e.getMessage(), R.id.log_success);
                        }

                    } catch (UareUException e) {
                        e.printStackTrace();
                        //writerLogInTextview("exception occurred !" + e.getMessage(), R.id.log_failed);
                        writerLogInTextview("Please make sure your finger margins are within the scanner!", R.id.log_failed);
                    } finally {
                        fpMgr.close();
                    }
                }
            };
            th.start();
        }
    }

    private Fmd ConvertImgToIsoTemplate(byte[] aImage, int iWidth) {

        if (aImage == null) {
            return null;
        }

        int iLength = aImage.length;
        int iHeight = aImage.length / iWidth;

        Importer importer = UareUGlobal.GetImporter();
        try {
            // Fid fid = importer.ImportRaw(aImage, iWidth, iHeight, 500, 0, 0,
            // Fid.Format.ISO_19794_4_2005, 500, false);
            Engine engine = UareUGlobal.GetEngine();
            Fmd fmd = engine.CreateFmd(aImage, iWidth, iHeight, 500, 0, 0, Fmd.Format.ISO_19794_2_2005);
            // int score = engine.Compare(fmd,0,fmd,0);

            Log.i("BasicSample", "Import a Fmd from a raw image OK");
            // Log.i("BasicSample", String.format("score %d" , score ));
            writerLogInTextview("Scanned successfully", R.id.log_success);
            return fmd;
        } catch (UareUException e) {
            Log.d("BasicSample", "Import Raw Image Fail", e);
            writerLogInTextview("Please try again!", R.id.log_success);
            return null;
        }

    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        // If we've received a touch notification that the user has touched
        // outside the app, finish the activity.
        if (MotionEvent.ACTION_OUTSIDE == event.getAction()) {
            //finish();
            return true;
        }

        // Delegate everything else to Activity.
        return super.onTouchEvent(event);
    }

    @Override
    public void onBackPressed() {


        if (doubleBackToExitPressedOnce) {
            String msg = "Are you sure you want to exit loan application? ";
            android.app.AlertDialog.Builder builder1 = new android.app.AlertDialog.Builder(ValidateCrossmatchBio.this);
            builder1.setMessage(msg);
            builder1.setTitle("Alert!");
            builder1.setCancelable(false);

            builder1.setPositiveButton(
                    "yes",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                           /* finish();
                            System.exit(0);*/

                            finish();
                            Intent i=new Intent();

                            if (SharedPrefs.read(SharedPrefs.COPORATE, null).equals("gfl")){
                                i = new Intent(ValidateCrossmatchBio.this, MainGFLDashboard.class);
                            }if(SharedPrefs.read(SharedPrefs.COPORATE, null).equals("notgfl")) {
                                i = new Intent(ValidateCrossmatchBio.this, MainDashboardActivity.class);
                            }
                            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                            //i.addFlags(IntentCompat.FLAG_ACTIVITY_CLEAR_TASK);
                            i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(i);
                            //onBackPressed();
                        }
                    });

            builder1.setNegativeButton(
                    "CANCEL",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                            //onBackPressed();
                        }
                    });

            AlertDialog alert11 = builder1.create();
            alert11.show();
            //System.exit(1);
            //Toast.makeText(this, "Are you sure you want to exit Member Registration", Toast.LENGTH_LONG).show();
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        toast.sinfo("Press back again to exit loan application");
        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce=false;
            }
        }, 2000);
        //super.onBackPressed();
        //overridePendingTransition(R.anim.move_left_in_activity, R.anim.move_right_out_activity);
    }
    @Override
    protected void onResume() {
        super.onResume();
        //Toast.makeText(this, "Returning....", Toast.LENGTH_SHORT).show();
    }
    private void requestMatchLogs() {
        String URL = GFL.MSACCO_AGENT + GFL.userlogs;
        final GetMatchLogsRequest matchlogsrequest = new GetMatchLogsRequest();

        matchlogsrequest.match = matched;
        matchlogsrequest.corporate_no ="CAP016";
        matchlogsrequest.transactiontype = 1;
        matchlogsrequest.agentid = SharedPrefs.read(SharedPrefs.AGENT_ID_1, null);
        matchlogsrequest.terminalid = SharedPrefs.read(SharedPrefs.DEVICE_TERMINAL, null);
        matchlogsrequest.longitude = getLong(this);
        matchlogsrequest.latitude = getLat(this);
        matchlogsrequest.date = getFormattedDate();

        Log.d("match","match---->"+matched);
        Log.d("match","match---->"+SharedPrefs.read(SharedPrefs.AGENT_ID_1, null));
        Log.d("match","match---->"+SharedPrefs.read(SharedPrefs.DEVICE_TERMINAL, null));
        Log.d("match","match---->"+getLong(this));
        Log.d("match","match---->"+getLat(this));
        Log.d("match","match---->"+getFormattedDate());

        GFL.instance(this).request(URL, matchlogsrequest, new GFL.RequestListener() {
            @Override
            public void onSuccess(String response) {
                final GetMatchlogsResponse miniStatementResponse1 = GFL.instance(ValidateCrossmatchBio.this)
                        .mGson.fromJson(response, GetMatchlogsResponse.class);
                if (miniStatementResponse1.is_successful){
                } else {
                    Toast.makeText(ValidateCrossmatchBio.this, "match failed", Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onTokenExpired() {

            }

            @Override
            public void onError(String error) {

            }
        });

    }


}
