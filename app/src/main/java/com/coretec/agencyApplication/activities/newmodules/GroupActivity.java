package com.coretec.agencyApplication.activities.newmodules;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.coretec.agencyApplication.R;
import com.coretec.agencyApplication.activities.BaseActivity;
import com.coretec.agencyApplication.api.Api;
import com.coretec.agencyApplication.api.requests.newmodulesrequests.GetMicrogroupRequest;
import com.coretec.agencyApplication.api.requests.newmodulesrequests.GroupRepaymentRequest;
import com.coretec.agencyApplication.api.responses.newmoduleresponses.GetGroupResponse;
import com.coretec.agencyApplication.api.responses.newmoduleresponses.RepaymentSubmissionResponse;
import com.coretec.agencyApplication.model.newmodulemodels.GroupObject;
import com.coretec.agencyApplication.model.newmodulemodels.PaymentObject;
import com.coretec.agencyApplication.utils.QuickToast;
import com.coretec.agencyApplication.utils.SharedPrefs;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import static com.coretec.agencyApplication.utils.Utils.getFormattedDate;
import static com.coretec.agencyApplication.utils.Utils.getLat;
import static com.coretec.agencyApplication.utils.Utils.getLong;

public class GroupActivity extends BaseActivity {
    private QuickToast toast = new QuickToast(this);
    TextView saccoName;
    //TextView document_no, grower_number, id_number, product_type, amount, req_kgs, disbursement;
    Button btnAdd;
    private Context mContext;
    private List<GroupObject> groupObjectList = new ArrayList<>();
    private List<PaymentObject> paymentObjectList = new ArrayList<>();
    AlertDialog paymentDialog;
    private LinearLayout groupLayout;
    private LinearLayout addPaymentToList;
    private Button saveBtn;
    ProgressDialog progressDialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_group);

        this.overridePendingTransition(R.anim.move_right_in_activity, R.anim.move_left_out_activity);

        SharedPrefs.write(SharedPrefs.FP_BOOLEAN, "");

        Toolbar toolbar = findViewById(R.id.toolbar);
        TextView mTitle = toolbar.findViewById(R.id.toolbar_title);
        mTitle.setText("Micro group Loan Repayment");
        setSupportActionBar(toolbar);
        ActionBar actionbar = getSupportActionBar();
        actionbar.setDisplayHomeAsUpEnabled(true);
        actionbar.setHomeAsUpIndicator(R.drawable.ic_arrow_back);

        progressDialog = new ProgressDialog(GroupActivity.this);
        progressDialog.setMessage("Fetching details, please wait...");
        progressDialog.setCancelable(false);
        progressDialog.show();

        String idd = getIntent().getExtras().getString("group_number");

        Log.d("groupnumber",idd);
        getGroupMembers(idd);
        saccoName = findViewById(R.id.tv_name);

        groupLayout = findViewById(R.id.linear_group);
        addPaymentToList = findViewById(R.id.linear_payment);

        btnAdd = findViewById(R.id.btnAdd);
        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createDialog();
            }
        });

        saveBtn = findViewById(R.id.btnSave);

        saveBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //saveLoanApplication();
                //Toast.makeText(Guarantor.this, "saveddddddd.......", Toast.LENGTH_SHORT).show();\

                String msg = "Are you sure you want to submit group repayment?";
                //final String qualKgs = SharedPrefs.read(SharedPrefs.LOAN_QUALIFIED_KGS, null);
                android.app.AlertDialog.Builder builder1 = new android.app.AlertDialog.Builder(GroupActivity.this);
                builder1.setMessage(msg);
                builder1.setTitle("Alert!");
                builder1.setCancelable(false);

                builder1.setPositiveButton(
                        "Yes",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                                progressDialog = new ProgressDialog(GroupActivity.this);
                                progressDialog.setMessage("Submitting, please wait...");
                                progressDialog.setCancelable(false);
                                progressDialog.show();

                                SubmitGroupPayment();
                                //saveLoanApplication();
                                //Toast.makeText(Guarantor.this, "Loan application submitted successfully!", Toast.LENGTH_SHORT).show();
                            }
                        });

                builder1.setNegativeButton(
                        "Cancel",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });

                android.app.AlertDialog alert11 = builder1.create();
                alert11.show();
            }
        });


    }


    void getGroupMembers(String id){


        final String TAG = "GetMicroGroups";
        String URL = Api.MSACCO_AGENT + Api.GetMicroGroups;

        final GetMicrogroupRequest microgroupRequest = new GetMicrogroupRequest();
        microgroupRequest.agentid = SharedPrefs.read(SharedPrefs.AGENT_ID_1, null);
        microgroupRequest.corporate_no = "CAP002";
        microgroupRequest.groupcode = id;
        microgroupRequest.terminalid = SharedPrefs.read(SharedPrefs.DEVICE_ID, null);
        microgroupRequest.longitude = getLong(this);
        microgroupRequest.latitude = getLat(this);
        microgroupRequest.date = getFormattedDate();

        Api.instance(this).request(URL, microgroupRequest, new Api.RequestListener() {
            @Override
            public void onSuccess(String response) {

                Log.d("responseee",response);

                GetGroupResponse getGroupResponse = Api.instance(GroupActivity.this)
                        .mGson.fromJson(response, GetGroupResponse.class);
                if (getGroupResponse.is_successful) {
                    progressDialog.dismiss();
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        String sacconame = jsonObject.getString("sacconame");
                        saccoName.setText(sacconame);

                        JSONArray grlist = jsonObject.getJSONArray("microgroups");

                        Log.d("myarrayyy",grlist.toString());
                        for (int i = 0; i < grlist.length(); i++) {
                            JSONObject jsonObject2 = grlist.getJSONObject(i);


                            String mnumber = jsonObject2.getString("memberno");
                            String accountname = jsonObject2.getString("accountname");
                            String expectedamt = jsonObject2.getString("expectedamount");
                            String totalexpectedamt = jsonObject2.getString("totalexpected");
                            String count = jsonObject2.getString("count");

                         /*   GroupObject groupObject=new GroupObject();
                            groupObject.setMno(mnumber);
                            groupObject.setAccname(accountname);
                            groupObject.setExpamount(expectedamt);
                            groupObject.setTotalexpe(totalexpectedamt);
                            groupObject.setCount(count);
*/
                            Log.d("nnnn",mnumber);
                            Log.d("oooo",accountname);
                            Log.d("pppp",expectedamt);
                            Log.d("kkkk",totalexpectedamt);
                            Log.d("codeee",microgroupRequest.groupcode);


                            addGroupMemberToList();

                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                } else {
                    progressDialog.dismiss();
                    toast.swarn("Could not find group details!");
                }

            }

            @Override
            public void onTokenExpired() {

            }

            @Override
            public void onError(String error) {

            }
        });
    }

    private  void addGroupMemberToList(){

        // loop to add guarantors
        for (final GroupObject groupObject : groupObjectList) {
            // get g view
            View v = LayoutInflater.from(this).inflate(R.layout.item_group_members, null);

            // get views
            TextView mnumber = v.findViewById(R.id.acc_no_id);
            TextView accname = v.findViewById(R.id.tv_name);
            TextView expectedamount = v.findViewById(R.id.tv_exp_amt);


         /*   mnumber.setText(groupObject.getMno());
            accname.setText(groupObject.getAccname());
            expectedamount.setText(groupObject.getExpamount());*/


            // add view to linear
            groupLayout.addView(v);
        }
    }


    private void createDialog(){

        LayoutInflater myInflater = LayoutInflater.from(this);
        View guarantorView = myInflater.inflate(R.layout.payment_dialog_group, null);

        final ArrayList<String> arrayList = new ArrayList<>();
       // ArrayAdapter<GrowerDetails> adapter;
        String item;

        //define and access view attributes
        final Button submitBtn = guarantorView.findViewById(R.id.btn_submitt);
        //final Button cancelBtn = guarantorView.findViewById(R.id.btnDiagCancel);
        //final EditText identifier_deposit = guarantorView.findViewById(R.id.id_number);

       // final EditText mnumber = guarantorView.findViewById(R.id.mnumber);
        final EditText loanAmount = guarantorView.findViewById(R.id.loan_amt);
        final EditText savingsAmount = guarantorView.findViewById(R.id.savings_amt);



        submitBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String l=loanAmount.getText().toString();
                String s=savingsAmount.getText().toString();

               /* progressDialog = new ProgressDialog(GroupActivity.this);
                progressDialog.setMessage("Please wait...");
                progressDialog.setCancelable(false);
                progressDialog.show();*/
                PaymentObject paymentObject = new PaymentObject();
                //paymentObject.setMemberNo(mnumber.getText().toString());
                paymentObject.setLoanAmount(l);
                paymentObject.setSavingsAmount(savingsAmount.getText().toString());

                paymentObjectList.add(paymentObject);

                addPaymentToList();
                paymentDialog.dismiss();
            }
        });
        /*cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //guarantorList.add(SharedPrefs.read(SharedPrefs.DROP_DOWN, null));
                //addGuarantorToList();
                paymentDialog.dismiss();
            }
        });*/

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setView(guarantorView);
        paymentDialog = builder.create();
        paymentDialog.show();

    }
     void addPaymentToList(){

        if (paymentObjectList.size() >= 5) {
            //Toast.makeText(getActivity(), "Minimum of three guarantors required", Toast.LENGTH_SHORT).show();
            saveBtn.setVisibility(View.VISIBLE);
            //submitLoanApplication.setVisibility(View.GONE);
        }

        // remove previous gs
        addPaymentToList.removeAllViews();

        double totalLoan=0;
        double totalSavings=0;
        // loop to add guarantors
        for (final PaymentObject grObject : paymentObjectList) {
            // get g view
            View v = LayoutInflater.from(this).inflate(R.layout.item_payment, null);

            // get views
            TextView mnumber = v.findViewById(R.id.tv_member_no);
            TextView loanamount = v.findViewById(R.id.tv_loan_amount);
            TextView savingsamount = v.findViewById(R.id.tv_savings_amout);


            mnumber.setText(grObject.getMemberNo());
            loanamount.setText(String.valueOf(grObject.getLoanAmount()));
            savingsamount.setText(String.valueOf(grObject.getSavingsAmount()));


            // add view to linear
            addPaymentToList.addView(v);
        }
    }

     void SubmitGroupPayment(){
       // final String TAG = "GuarantorRegistration";

        //Toast.makeText(mContext, "Loading please wait...", Toast.LENGTH_LONG).show();
        //toast.linfo("Loading please wait...");

        String URL = Api.MSACCO_AGENT + Api.MicroGroupTransaction;

        final GroupRepaymentRequest reprequest = new GroupRepaymentRequest();


        reprequest.corporate_no= "CAP002";
        reprequest.groupcode="1097838";
        reprequest.totalloanpaid="67700";
        reprequest.totalloanunpaid="7000";
        reprequest.totalpaid="700000";
        reprequest.totalsavingspaid="7000000";
        reprequest.verifiedby="Alexander";
        reprequest.verifyphone="0725100851";
        reprequest.initializationtime="98/99/8889";
        reprequest.totalpresentmember="9";
        reprequest.totalabstmember="7";
        reprequest.groupimage="Yctjen1g3BBQ5ooq4/5LBMRlKlEMh5h2Jk\\nhiERuZg6kzPYqocWBBA5UGDJxcfFi0gp6kC1nLPIXFqZ17XPD5dScs5WpBWnaVdUQzRfJkQGy1WI\\nK0dBRIRWBU4UYkor4kEzbCG1bbv/YG/edtPL7uKylFJKbkvOYu3i7/7Ks1sndoLLmIEpo5ghBo+j\\nGgjvfPju8mZ+7uSFdHBzpEfe+kqEimoIZCSGgYKZKqCCorbYyUuCmCICYVDQVJEZGrJTmAIoYBtO\\n7ByLMaL1RS/gEKltmypE53hSVSdRQxURKdISQFsfiGYRbUTF1BV8E3JHlK/ZJxA8ZvfBNwAgIIWu\\nAheJUkAIzFx1noe6fJWRQkhoGggFzMe2/bw1EyQLxAFJQqcIjGYtVsMUr7xzrYVOPtn6y78EIKbC\\n1Xj77PmQehEGYlVFZj+iBUEMzPAne0cvDWajZpcMwUIvNYVGgB0DHoJPM3hegoCAbOS5DjOZMwkV\\nIQ2KZoCmErAscjZELIie0TEgACk97Nc5PxgRgTmoVWLCIVVOoEsGWZN7LwUTKYhjZ3iEtboKIkZy\\nZJBhP8nr7jvGjtzBI9bAifqLmR266PJCQDgcDnPTIlPgxMwUAyIDx43j27ffv17EVtHIei0eQFRK\\nHE1PnbsYUkRkMGJmp38FQwrs7A+CARHr9763XxZDq5MtnRNKPC8HKADEiIRk3SSOmRGDmrPGo6ii\\nZUI2UpGMzCpiZmF7Y+ALwR0Prpn254mBGXopDYgApJgyQnA1dDOOQcQAYEgDcHJ5ULRhp2Hs/Bxe\\noevmErsRJDMARrJuJouid/w7DfWOXBIYEUOKHAYdfVsYQmCiMIwRmcbDcaDIMRARcXzpy6+9c+VT\\nVVrlv6s6Ylc5YMrKpy++xOyceGgdfF36sIuVVIFULU1/98a7P2zv/GK7LMzUDI1Q1cAI/FBTUBWA\\nrg5askFHzqsqaoQucqCqBIRmQBiKghlWIXIHfDHiUEp2mhUmSsSqXXY07KvDvcljWMPHRCLtUiqz\\nnhDZ193Vjdzp+glMKUYkpsq9M4QQmZGZGENMzNFnhzmkECIAcBgABQpsTsSNGGPsAm2zUTX45m/9\\nzp//2V9DT6RLRCklCkSAN27daurMnO7cvTXamiGQT4x5aooGYgpgqoqKAbFYkfGs3XpSdz9t2/sI\\nQkYqaBzVpJdSMESnw+4qIw4fNFUHvYqIy+vk7K+2kNKQmVGd97JLvqPzYJgCsgJkwEhcSiEjQlKV\\nkKIZYXAN70gUvE3LzEwBkZEJ+mVlZiBGg5AiAgd22L+X6YJzcfnLBCEgYYhd5BOYAJGjgSAmIJSu\\njqpkQCGCqHQggPLkkxeeevKkwwJCRzACRHR4eLSzPUNERUiJ89HSAvd1iIfeRqSIiCF4eCem460T\\nePyULT7XrMoEnXoHA";
        reprequest.remarks="done...";
        reprequest.agentid="SAC002";
        reprequest.terminalid="876545678098";
        //reprequest.longitude="0.00";
        //reprequest.latitude= "0.0";
        reprequest.date= "67/9/9866";


        Api.instance(GroupActivity.this).request(URL, reprequest, new Api.RequestListener() {
            @Override
            public void onSuccess(String response) {
                progressDialog.dismiss();

                /* deposit_progressBar.setVisibility(View.GONE); */
                RepaymentSubmissionResponse grResponse = Api.instance(GroupActivity.this)
                        .mGson.fromJson(response, RepaymentSubmissionResponse.class);
                if (grResponse.operationsuccess) {
                    progressDialog.dismiss();
                    try {
                        JSONObject jsonObject = new JSONObject(response);

                        String receiptNo = jsonObject.getString("receiptno");
                        Toast.makeText(GroupActivity.this, receiptNo, Toast.LENGTH_SHORT).show();
                        Log.e("SUCCESSFUL", receiptNo);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    String msg = "Customer registration submitted successfully!";
                    android.app.AlertDialog.Builder builder1 = new android.app.AlertDialog.Builder(GroupActivity.this);
                    builder1.setMessage(msg);
                    builder1.setTitle("Success!");
                    builder1.setCancelable(false);

                    builder1.setPositiveButton(
                            "DONE",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                    startActivity(new Intent(GroupActivity.this, MainDashboardActivity.class));
                                    GroupActivity.this.getSupportFragmentManager().popBackStack();
                                }
                            });

                    android.app.AlertDialog alert11 = builder1.create();
                    alert11.show();
                } else {
                    progressDialog.dismiss();
                    Toast.makeText(GroupActivity.this, "Failed. Try again later!", Toast.LENGTH_LONG).show();
                }

            }

            @Override
            public void onTokenExpired() {

            }

            @Override
            public void onError(String error) {

            }
        });
    }

}
