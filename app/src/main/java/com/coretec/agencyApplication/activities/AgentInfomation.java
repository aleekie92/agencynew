package com.coretec.agencyApplication.activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import com.coretec.agencyApplication.R;
import com.coretec.agencyApplication.api.Api;
import com.coretec.agencyApplication.api.requests.GetAgentAccountInfo;
import com.coretec.agencyApplication.api.responses.AgentAccountResponse;
import com.coretec.agencyApplication.utils.LogOutTimerUtil;
import com.coretec.agencyApplication.utils.PreferenceFileKeys;
import com.coretec.agencyApplication.utils.SharedPrefs;
import com.coretec.agencyApplication.utils.Utils;

import static com.coretec.agencyApplication.utils.Utils.getFormattedDate;
import static com.coretec.agencyApplication.utils.Utils.getIMEI;
import static com.coretec.agencyApplication.utils.Utils.getLat;
import static com.coretec.agencyApplication.utils.Utils.getLong;

public class AgentInfomation extends BaseActivity implements  LogOutTimerUtil.LogOutListener{

  private TextView txt_agent_sacco_name;
  private TextView txt_agent_name;
  private TextView txt_agent_business_name;
  private TextView txt_agent_account_no;
  private TextView txt_agent_location;
  private TextView txt_agent_account_balance;

  SharedPreferences sharedPreferences;
  public static String imei = "", corporateno = "", saccomotto = "";
  private ProgressDialog progressDialog;

  @Override
  protected void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    setContentView(R.layout.agent_account_info);

    this.overridePendingTransition(R.anim.move_right_in_activity, R.anim.move_left_out_activity);
    sharedPreferences = getSharedPreferences(PreferenceFileKeys.PREFS_FILE, Context.MODE_PRIVATE);
    txt_agent_sacco_name = findViewById(R.id.agent_sacco_name);
    txt_agent_name = findViewById(R.id.agent_name);
    txt_agent_business_name = findViewById(R.id.agent_business_name);
    txt_agent_account_no = findViewById(R.id.agent_account_no);
    txt_agent_location = findViewById(R.id.agent_location);
    txt_agent_account_balance = findViewById(R.id.agent_account_balance);

    progressDialog = new ProgressDialog(AgentInfomation.this);
    progressDialog.setMessage("Getting agent information, please wait...");
    progressDialog.setCancelable(false);
    progressDialog.show();
    requestAgentInfo();
  }

  void requestAgentInfo() {
    final String TAG = "request agent info";

    String URL = Api.MSACCO_AGENT + Api.GetAgentAccountInfo;
    GetAgentAccountInfo accountInfo = new GetAgentAccountInfo();
    accountInfo.agentid = sharedPreferences.getString(PreferenceFileKeys.AGENT_ID, "");
    accountInfo.terminalid = getIMEI(this);
    accountInfo.longitude = getLong(this);
    accountInfo.latitude = getLat(this);
    accountInfo.date = getFormattedDate();

    Log.e(TAG, accountInfo.getBody().toString());
    Api.instance(this).request(URL, accountInfo, new Api.RequestListener(){
      @Override
      public void onSuccess(String response){
        Log.e(TAG, response);
        final AgentAccountResponse agentAccountResponse = Api.instance(AgentInfomation.this).mGson.fromJson(response, AgentAccountResponse.class);
        if (agentAccountResponse.is_successful) {

          progressDialog.dismiss();
          txt_agent_sacco_name.setText(agentAccountResponse.getSacconame());
          txt_agent_name.setText(agentAccountResponse.getAgentdetails().getAccountname());
          txt_agent_business_name.setText(agentAccountResponse.getAgentdetails().getProposedname());
          txt_agent_account_no.setText(agentAccountResponse.getAgentdetails().getAgentaccno());
          txt_agent_location.setText(agentAccountResponse.getAgentdetails().getLocation());
          txt_agent_account_balance.setText(SharedPrefs.read(SharedPrefs.CURRENCY, null) + " :" + agentAccountResponse.getAgentdetails().getAccountbalance());

        } else {
          progressDialog.dismiss();
          Toast.makeText(getApplicationContext(), "Could not get agent information, please try again!", Toast.LENGTH_LONG).show();
        }
      }

      @Override
      public void onTokenExpired() {

      }

      @Override
      public void onError(String error) {
        Utils.showAlertDialog(AgentInfomation.this, "Error", error);
      }
    });

  }

  @Override
  public void onBackPressed() {
    super.onBackPressed();
    overridePendingTransition(R.anim.move_left_in_activity, R.anim.move_right_out_activity);
  }

  @Override
  protected void onStart() {
    super.onStart();
    // LogOutTimerUtil.startLogoutTimer(this, this);

  }

  @Override
  public void onUserInteraction() {
    super.onUserInteraction();
    LogOutTimerUtil.startLogoutTimer(this, this);

  }


  @Override
  protected void onPause() {
    super.onPause();
    LogOutTimerUtil.startLogoutTimer(this, this);

  }

  @Override
  protected void onResume() {
    super.onResume();
    LogOutTimerUtil.startLogoutTimer(this, this);

  }

  @Override
  public void doLogout() {
    finish();
  }
}
