package com.coretec.agencyApplication.activities.gfl;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.support.design.widget.TextInputLayout;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.cloudpos.jniinterface.PrinterInterface;
import com.coretec.agencyApplication.R;
import com.coretec.agencyApplication.activities.BaseActivity;
import com.coretec.agencyApplication.activities.newmodules.ValidateTunzhengbigBio;
import com.coretec.agencyApplication.api.GFL;
import com.coretec.agencyApplication.api.requests.FingerPrintRequest;
import com.coretec.agencyApplication.api.requests.GetLoansCalculatorRequest;
import com.coretec.agencyApplication.api.requests.GrowersDetailsRequest;
import com.coretec.agencyApplication.api.responses.FingerPrintResponse;
import com.coretec.agencyApplication.api.responses.GetLoansCalculatorList;
import com.coretec.agencyApplication.api.responses.GetLoansCalculatorResponse;
import com.coretec.agencyApplication.api.responses.GrowersDetailsResponse;
import com.coretec.agencyApplication.model.GrowerDetails;
import com.coretec.agencyApplication.utils.QuickToast;
import com.coretec.agencyApplication.utils.SharedPrefs;
import com.coretec.agencyApplication.wizarpos.function.printer.PrinterCommand;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static com.coretec.agencyApplication.utils.Utils.getFormattedDate;
import static com.coretec.agencyApplication.utils.Utils.getLat;
import static com.coretec.agencyApplication.utils.Utils.getLong;

public class LoanCalculator extends AppCompatActivity {

    private QuickToast toast = new QuickToast(this);
    ImageView backBtn;
    private TextInputLayout input_layout_id_number;
    private EditText identifier_deposit;
    private Spinner spinner;
    private ArrayList<String> arrayList = new ArrayList<>();
    private ArrayAdapter<GrowerDetails> adapter;

    private Spinner spinner2;
    private ArrayList<String> arrayList2 = new ArrayList<>();
    private ArrayAdapter<String> adapter2;

    private String identifiercode = "";
    private String growerNo = "";
    String text;

    private String item;
    ProgressBar progressBar;
    ProgressDialog progressDialog;
    Button submit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.gfl_loan_calculator);
        SharedPrefs.init(getApplicationContext());

        this.overridePendingTransition(R.anim.move_right_in_activity, R.anim.move_left_out_activity);

        SharedPrefs.write(SharedPrefs.FP_BOOLEAN, "");
        SharedPrefs.write(SharedPrefs.LOAN_G_NO, "");
        SharedPrefs.write(SharedPrefs.LOAN_F_N, "");
        SharedPrefs.write(SharedPrefs.LOAN_ID, "");
        SharedPrefs.write(SharedPrefs.LOAN_G_ST, "");
        SharedPrefs.write(SharedPrefs.LOAN_PU_REG, "");
        SharedPrefs.write(SharedPrefs.LOAN_PR_TY, "");
        SharedPrefs.write(SharedPrefs.LOAN_PAY_KEY, "");
        SharedPrefs.write(SharedPrefs.LOAN_PRO_NAME, "");
        SharedPrefs.write(SharedPrefs.LOAN_PREV_YR, "");
        SharedPrefs.write(SharedPrefs.LOAN_CURR_KG, "");
        SharedPrefs.write(SharedPrefs.LOAN_TOT_CURR, "");
        SharedPrefs.write(SharedPrefs.LOAN_RATE_PKG, "");
        SharedPrefs.write(SharedPrefs.LOAN_FACT, "");
        SharedPrefs.write(SharedPrefs.LOAN_BONUS_RT, "");
        SharedPrefs.write(SharedPrefs.LOAN_PER_OF_PRE, "");
        SharedPrefs.write(SharedPrefs.LOAN_PRO_RATE, "");
        SharedPrefs.write(SharedPrefs.LOAN_PROR_RL, "");
        SharedPrefs.write(SharedPrefs.LOAN_QUAL_AMT, "");
        SharedPrefs.write(SharedPrefs.LOAN_HEAD_G_NUMBER, "");
        SharedPrefs.write(SharedPrefs.LOAN_CALC_G_NUMBER, "");

        Toolbar toolbar = findViewById(R.id.toolbar);
        TextView mTitle = toolbar.findViewById(R.id.toolbar_title);
        mTitle.setText(getResources().getText(R.string.loan_calc));
        setSupportActionBar(toolbar);
        ActionBar actionbar = getSupportActionBar();
        actionbar.setDisplayHomeAsUpEnabled(true);
        actionbar.setHomeAsUpIndicator(R.drawable.ic_arrow_back);

        input_layout_id_number = findViewById(R.id.input_layout_id_number);
        identifier_deposit = findViewById(R.id.id_number);

        identifier_deposit.setInputType(InputType.TYPE_NULL);
        identifier_deposit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // showMyDialog();
            }
        });

        identifier_deposit.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    // showMyDialog();
                }
            }
        });

        submit = findViewById(R.id.btnSubmit);
        submit.setVisibility(View.GONE);
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (identifier_deposit.getText().toString().matches("")) {
                    Toast.makeText(getBaseContext(), "Please fill in the missing fields to proceed!", Toast.LENGTH_LONG).show();
                }
                else{
                    progressDialog = new ProgressDialog(LoanCalculator.this);
                    progressDialog.setMessage("Please wait...");
                    progressDialog.setCancelable(false);
                    progressDialog.show();

                    requestFingerPrints();
                    //submitWithoutFP();
                }
            }
        });

        spinner = findViewById(R.id.growerSpinner);
        spinner2 = findViewById(R.id.spinner_deposit);

        progressBar = findViewById(R.id.growersPb);

        TextWatcher textWatcherId = new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                //here, after we introduced something in the EditText we get the string from it
                String identifier_number = identifier_deposit.getText().toString();
                if (identifier_deposit.length() >= 7) {
                    requestGetAccountDetails(identifier_number);
                }
                if (identifier_deposit.length() <= 5) {
                    progressBar.setVisibility(View.GONE);
                    arrayList.clear();
                    submit.setVisibility(View.GONE);
                    spinner = findViewById(R.id.growerSpinner);
                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(LoanCalculator.this, android.R.layout.simple_spinner_item, arrayList);
                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    spinner.setAdapter(null);
                }
                if (identifier_deposit.length() >= 5 && identifier_deposit.length() <= 8) {
                    arrayList.clear();
                    submit.setVisibility(View.GONE);
                    spinner = findViewById(R.id.growerSpinner);
                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(LoanCalculator.this, android.R.layout.simple_spinner_item, arrayList);
                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    spinner.setAdapter(null);
                    progressBar.setVisibility(View.VISIBLE);
                    progressBar.getIndeterminateDrawable().setColorFilter(ContextCompat.getColor(LoanCalculator.this, R.color.btn_green), PorterDuff.Mode.SRC_IN );
                }
//                Toast.makeText(ShareDeposit.this, accountNumber, Toast.LENGTH_SHORT).show();
//                requestAccountName(accountNumber);

            }
        };

        adapter = new ArrayAdapter<GrowerDetails>(getApplicationContext(),
                R.layout.simple_spinner_dropdown_item, new ArrayList<GrowerDetails>());
        adapter.setDropDownViewResource(R.layout.simple_spinner_dropdown_item);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                item = parent.getItemAtPosition(position).toString();
                 text = spinner.getSelectedItem().toString();
                String text1 = spinner.getSelectedItem().toString();

                SharedPrefs.write(SharedPrefs.LOAN_CALC_G_NUMBER, text);
                SharedPrefs.write(SharedPrefs.LOAN_HEAD_G_NUMBER, text1);
                getHeaderDetails();
                Log.e("GROWER NUMBER", text);
                Log.e("GROWER NUMBER", text1);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        identifier_deposit.addTextChangedListener(textWatcherId);

    }

    void requestFingerPrints() {

        final String TAG = "GET FINGERPRINTS";
        String URL = GFL.MSACCO_AGENT + GFL.GetFourFingerPrint;

        final FingerPrintRequest fingerPrintRequest = new FingerPrintRequest();
        fingerPrintRequest.accountidentifier = identifier_deposit.getText().toString();
        fingerPrintRequest.accountidentifiercode = "1";
        fingerPrintRequest.agentid = SharedPrefs.read(SharedPrefs.AGENT_ID_1, null);
        fingerPrintRequest.terminalid = MainGFLDashboard.imei;//SharedPrefs.read(SharedPrefs.DEVICE_ID, null);
        fingerPrintRequest.longitude = getLong(this);
        fingerPrintRequest.latitude = getLat(this);
        fingerPrintRequest.date = getFormattedDate();

        Log.e(TAG, fingerPrintRequest.getBody().toString());

        GFL.instance(this).request(URL, fingerPrintRequest, new GFL.RequestListener() {
            @Override
            public void onSuccess(String response) {
                FingerPrintResponse fingerPrintResponse = GFL.instance(LoanCalculator.this)
                        .mGson.fromJson(response, FingerPrintResponse.class);
                if (fingerPrintResponse.is_successful) {



                    try {
                        JSONObject jsonObject = new JSONObject(response);

                        String leftRing = jsonObject.getString("leftring");
                        String leftLittle = jsonObject.getString("leftlittle");
                        String rightRing = jsonObject.getString("rightring");
                        String rightLittle = jsonObject.getString("rightlittle");

                        SharedPrefs.write(SharedPrefs.LEFT_RING, leftRing);
                        SharedPrefs.write(SharedPrefs.LEFT_LITTLE, leftLittle);
                        SharedPrefs.write(SharedPrefs.RIGHT_RING, rightRing);
                        SharedPrefs.write(SharedPrefs.RIGHT_LITTLE, rightLittle);

                        Log.d("LEFT_RING", leftRing);
                        Log.d("LEFT_LITTLE", leftLittle);
                        Log.d("RIGHT_RING", rightRing);
                        Log.d("RIGHT_LITTLE", rightLittle);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    SharedPrefs.transactionstypelogs(5, LoanCalculator.this);
                    SharedPrefs.growerslogs(text, LoanCalculator.this);
                    startActivity(new Intent(LoanCalculator.this, ValidateTunzhengbigBio.class));

                } else {
                    progressDialog.dismiss();
                    toast.swarn("Could not get customer finger prints!");
                }


            }

            @Override
            public void onTokenExpired() {

            }

            @Override
            public void onError(String error) {

            }
        });

    }

    void getHeaderDetails() {

        final String TAG = "Loan Calculator";
//        EasyLocationMod easyLocationMod = new EasyLocationMod(CashWithdrawal.this);

        String URL = GFL.MSACCO_AGENT + GFL.GetLoansCalculator;
        String gNumber = SharedPrefs.read(SharedPrefs.LOAN_HEAD_G_NUMBER, null);
        final GetLoansCalculatorRequest loansCalculator = new GetLoansCalculatorRequest();
        loansCalculator.corporateno = "CAP016";
        loansCalculator.growerno = gNumber;
        loansCalculator.idno = identifier_deposit.getText().toString();
        loansCalculator.transactiontype = "1";
        loansCalculator.agentid = SharedPrefs.read(SharedPrefs.AGENT_ID_1, null);
        loansCalculator.terminalid = SharedPrefs.read(SharedPrefs.DEVICE_ID, null);
        loansCalculator.longitude = getLong(this);
        loansCalculator.latitude = getLat(this);
        loansCalculator.date = getFormattedDate();

        Log.e(TAG, loansCalculator.getBody().toString());

        GFL.instance(this).request(URL, loansCalculator, new GFL.RequestListener() {
            @Override
            public void onSuccess(String response) {
                /*deposit_progressBar.setVisibility(View.GONE);*/
                //progressDialog.dismiss();
                GetLoansCalculatorResponse loansCalculatorResponse = GFL.instance(LoanCalculator.this)
                        .mGson.fromJson(response, GetLoansCalculatorResponse.class);
                if (loansCalculatorResponse.is_successful) {
                    List<GetLoansCalculatorList> loansCalculatorLists = new ArrayList<>();

                    try {
                        JSONObject jsonObject = new JSONObject(response);

                        JSONArray jsonArray = jsonObject.getJSONArray("GetLoansCalculatorList");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                            JSONArray jsonArray1 = jsonObject1.getJSONArray("LoanCalculator");
                            for (int l = 0; l < jsonArray1.length(); l++) {
                                JSONObject jsonObject2 = jsonArray1.getJSONObject(l);
                                //progressBar.setVisibility(View.GONE);
                                submit.setVisibility(View.VISIBLE);
                                //String gNo = jsonObject2.getString("GrowerNo");
                                //String pNumber = jsonObject2.getString("GrowerMobileNo");
                                //Json responses for the string
                                String gNo = jsonObject2.getString("GrowerNo");
                                String fullNames = jsonObject2.getString("GrowerFullNames");
                                String idNos = jsonObject2.getString("GrowerIDNos");
                                String growerStatus = jsonObject2.getString("GrowerStatus");
                                String puRegistered = jsonObject2.getString("GrowerPURegistered");
                                String previousYearKgs = jsonObject2.getString("PreviousYearKgs");
                                String currentKgs = jsonObject2.getString("CurrentKgs");
                                String factBonusRate = jsonObject2.getString("FactBonusRate");

                                //SharedPrefs.write(SharedPrefs.PHONE_NUMBER, pNumber);
                                SharedPrefs.write(SharedPrefs.LOAN_G_NO, gNo);
                                SharedPrefs.write(SharedPrefs.LOAN_F_N, fullNames);
                                SharedPrefs.write(SharedPrefs.LOAN_ID, idNos);
                                SharedPrefs.write(SharedPrefs.LOAN_G_ST, growerStatus);
                                SharedPrefs.write(SharedPrefs.LOAN_PU_REG, puRegistered);
                                SharedPrefs.write(SharedPrefs.LOAN_PREV_YR, previousYearKgs);
                                SharedPrefs.write(SharedPrefs.LOAN_CURR_KG, currentKgs);
                                SharedPrefs.write(SharedPrefs.LOAN_FACT, factBonusRate);

                                //oldPhone = findViewById(R.id.oldPhone);
                                //oldPhone.setText(pNumber);

                                Log.e("GROWERS NUMBER", gNo);
                                Log.e("GrowerFullNames", fullNames);
                                Log.e("GrowerIDNos", idNos);
                                Log.e("GrowerStatus", growerStatus);
                                Log.e("GrowerPURegistered", puRegistered);
                                Log.e("PreviousYearKgs", previousYearKgs);
                                Log.e("CurrentKgs", currentKgs);
                                Log.e("FactBonusRate", factBonusRate);
                            }

                        }


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
//                    Toast.makeText(ShareDeposit.this, "Accounts Fetching Failed", Toast.LENGTH_LONG).show();
                }

            }

            @Override
            public void onTokenExpired() {

            }

            @Override
            public void onError(String error) {

            }
        });
    }

    void submitLoanCalculator() {

        final String TAG = "Loan Calculator";
        String URL = GFL.MSACCO_AGENT + GFL.GetLoansCalculator;
        String gNumber = SharedPrefs.read(SharedPrefs.LOAN_CALC_G_NUMBER, null);

        final GetLoansCalculatorRequest loansCalculator = new GetLoansCalculatorRequest();
        loansCalculator.corporateno = "CAP016";
        loansCalculator.growerno = gNumber;
        loansCalculator.idno = identifier_deposit.getText().toString();
        loansCalculator.transactiontype = "1";
        loansCalculator.agentid = SharedPrefs.read(SharedPrefs.AGENT_ID_1, null);
        loansCalculator.terminalid = SharedPrefs.read(SharedPrefs.DEVICE_ID, null);
        loansCalculator.longitude = getLong(this);
        loansCalculator.latitude = getLat(this);
        loansCalculator.date = getFormattedDate();

        Log.e(TAG, loansCalculator.getBody().toString());

        GFL.instance(this).request(URL, loansCalculator, new GFL.RequestListener() {
            @Override
            public void onSuccess(String response) {
                /*deposit_progressBar.setVisibility(View.GONE);*/
                progressDialog.dismiss();
                GetLoansCalculatorResponse loansCalculatorResponse = GFL.instance(LoanCalculator.this)
                        .mGson.fromJson(response, GetLoansCalculatorResponse.class);
                if (loansCalculatorResponse.is_successful) {
                    List<GetLoansCalculatorList> loansCalculatorLists = new ArrayList<>();

                    try {
                        JSONObject jsonObject = new JSONObject(response);

                        JSONArray jsonArray = jsonObject.getJSONArray("GetLoansCalculatorList");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                            JSONArray jsonArray1 = jsonObject1.getJSONArray("LoanCalculator");
                            for (int l = 0; l < jsonArray1.length(); l++) {
                                JSONObject jsonObject2 = jsonArray1.getJSONObject(l);
                                progressBar.setVisibility(View.GONE);
                                String productType = jsonObject2.getString("ProductType");
                                String loansPayKey = jsonObject2.getString("LoansPayKey");
                                String productName = jsonObject2.getString("LoanProductName");
                                String totCurrent = jsonObject2.getString("TotCurrent");
                                String ratePerKG = jsonObject2.getString("RatePerKG");
                                String bonusRate = jsonObject2.getString("BonusRate");
                                String percantageOfPrevious = jsonObject2.getString("PercantageOfPrevious");
                                String prorationRate = jsonObject2.getString("ProrationRate");
                                String prorationRateLimit = jsonObject2.getString("ProrationRateLimit");
                                String qualAmount = jsonObject2.getString("QualAmount");
                                String outBal = jsonObject2.getString("OutstandingBalance");
                                String nAmount = jsonObject2.getString("NetAmount");
                                SharedPrefs.write(SharedPrefs.LOAN_PR_TY, productType);
                                SharedPrefs.write(SharedPrefs.LOAN_PAY_KEY, loansPayKey);
                                SharedPrefs.write(SharedPrefs.LOAN_PRO_NAME, productName);
                                SharedPrefs.write(SharedPrefs.LOAN_TOT_CURR, totCurrent);
                                SharedPrefs.write(SharedPrefs.LOAN_RATE_PKG, ratePerKG);
                                SharedPrefs.write(SharedPrefs.LOAN_BONUS_RT, bonusRate);
                                SharedPrefs.write(SharedPrefs.LOAN_PER_OF_PRE, percantageOfPrevious);
                                SharedPrefs.write(SharedPrefs.LOAN_PRO_RATE, prorationRate);
                                SharedPrefs.write(SharedPrefs.LOAN_PROR_RL, prorationRateLimit);
                                SharedPrefs.write(SharedPrefs.LOAN_QUAL_AMT, qualAmount);
                                SharedPrefs.write(SharedPrefs.LOAN_OUT_BAL, outBal);
                                SharedPrefs.write(SharedPrefs.LOAN_NET_AMT, nAmount);
                                String f = SharedPrefs.read(SharedPrefs.LOAN_PR_TY, null);
                                String g = SharedPrefs.read(SharedPrefs.LOAN_PAY_KEY, null);
                                String h = SharedPrefs.read(SharedPrefs.LOAN_PRO_NAME, null);
                                String k = SharedPrefs.read(SharedPrefs.LOAN_TOT_CURR, null);
                                String la = SharedPrefs.read(SharedPrefs.LOAN_RATE_PKG, null);
                                String n = SharedPrefs.read(SharedPrefs.LOAN_BONUS_RT, null);
                                String o = SharedPrefs.read(SharedPrefs.LOAN_PER_OF_PRE, null);
                                String p = SharedPrefs.read(SharedPrefs.LOAN_PRO_RATE, null);
                                String q = SharedPrefs.read(SharedPrefs.LOAN_PROR_RL, null);
                                String r = SharedPrefs.read(SharedPrefs.LOAN_QUAL_AMT, null);
                                String s = SharedPrefs.read(SharedPrefs.LOAN_OUT_BAL, null);
                                String t = SharedPrefs.read(SharedPrefs.LOAN_NET_AMT, null);

                                int result = PrinterInterface.open();
                                writetest(f, g, h, k, la, n, o, p, q, r, s, t);
                                PrinterInterface.close();
                                startActivity(new Intent(LoanCalculator.this, MainGFLDashboard.class));
                            }

                        }


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    String agentName = SharedPrefs.read(SharedPrefs.AGENT_NAME, null);
                    int result = PrinterInterface.open();
                    footer(agentName);
                    PrinterInterface.close();
                } else {
//                    Toast.makeText(ShareDeposit.this, "Accounts Fetching Failed", Toast.LENGTH_LONG).show();
                }

            }

            @Override
            public void onTokenExpired() {

            }

            @Override
            public void onError(String error) {

            }
        });
    }

    public void footer(String aName){
        try {
            byte[] time = null;
            byte[] nameAgent = null;
            byte[] motto = null;

            SimpleDateFormat timeStampFormat = new SimpleDateFormat("yyyy/MM/dd - HH:mm");
            Date myDate = new Date();
            String timeCurrent = timeStampFormat.format(myDate);

            try {
                time = String.valueOf("   Date : " + String.valueOf(timeCurrent)).getBytes("GB2312");
                nameAgent = String.valueOf(" You were served by " + String.valueOf(aName)).getBytes("GB2312");
                motto = String.valueOf("         GFL Motto       ").getBytes("GB2312");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            PrinterInterface.begin();
            byte[] cmd;
            cmd = new byte[]{(byte) 0x1B, (byte) 0x40};

            cmd = new byte[]{
                    (byte) 0x1B, (byte) 0x45, (byte) 0};

            write(cmd);
            writeLineBreak(1);
            write(time);
            writeLineBreak(2);
            write(nameAgent);
            writeLineBreak(1);
            write(motto);
            // print line break
            writeLineBreak(4);
            writeLineBreak(5);
            PrinterInterface.end();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void header(String fullNames, String gNo , String idNos, String growerStatus, String puRegistered, String previousYearKgs,
                       String currentKgs, String factBonusRate){
        String first = idNos.substring(0, 2);
        String last = idNos.substring(6, idNos.length());
        String maskedId = first + "****"+last;
        String firstG = fullNames.substring(0, 2);
        String lastG = fullNames.substring(6, fullNames.length());
        String maskedG = firstG + "*****"+lastG;

        Log.e("MASKED ID", maskedId);
        try {
            byte[] start = null;
            byte[] saccoName = null;
            byte[] type = null;
            byte[] isDefa = null;
            byte[] gno = null;
            byte[] fname = null;
            byte[] idn = null;
            byte[] gstatus = null;
            byte[] pureg = null;
            byte[] prevYear = null;
            byte[] currKgs = null;
            byte[] factBon = null;

            try {
                start = String.valueOf("              ").getBytes("GB2312");
                saccoName = String.valueOf("      GREENLAND FEDHA LTD     ").getBytes("GB2312");
                type = String.valueOf("        LOAN CALCULATOR     ").getBytes("GB2312");
                gno = String.valueOf("Names :" + String.valueOf(gNo)).getBytes("GB2312");
                isDefa = String.valueOf("Defaulter: "+SharedPrefs.read(SharedPrefs.IS_DEFAULTER, null)).getBytes("GB2312");
                fname = String.valueOf("Grower Number :" + String.valueOf(maskedG)).getBytes("GB2312");
                idn = String.valueOf("ID Number : " + String.valueOf(maskedId)).getBytes("GB2312");
                gstatus = String.valueOf("Grower Status : " + String.valueOf(growerStatus)).getBytes("GB2312");
                pureg = String.valueOf("PU Registered : " + String.valueOf(puRegistered)).getBytes("GB2312");
                prevYear = String.valueOf("Prev Year Kgs :" + String.valueOf(previousYearKgs)).getBytes("GB2312");
                currKgs = String.valueOf("Current Kgs : " + String.valueOf(currentKgs)).getBytes("GB2312");
                factBon = String.valueOf("Fact Bonus Rate : " + String.valueOf(factBonusRate)).getBytes("GB2312");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            PrinterInterface.begin();
            byte[] cmd;
            cmd = new byte[]{(byte) 0x1B, (byte) 0x40};

            cmd = new byte[]{
                    (byte) 0x1B, (byte) 0x45, (byte) 0};

            write(cmd);
            write(start);
            writeLineBreak(1);
            write(saccoName);
            writeLineBreak(2);
            write(type);
            writeLineBreak(1);
            write(gno);
            writeLineBreak(1);
            write(isDefa);
            writeLineBreak(1);
            write(fname);
            writeLineBreak(1);
            write(idn);
            writeLineBreak(1);
            write(gstatus);
            writeLineBreak(1);
            write(pureg);
            writeLineBreak(1);
            write(prevYear);
            writeLineBreak(1);
            write(currKgs);
            writeLineBreak(1);
            write(factBon);
            // print line break
            writeLineBreak(2);
            PrinterInterface.end();

        }catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void writetest(String productType, String loansPayKey, String productName,  String totCurrent, String ratePerKG, String bonusRate,
                          String percantageOfPrevious, String prorationRate, String prorationRateLimit, String qualAmount,
                          String outBal, String netAmount) {
        try {

            byte[] gno = null;
            byte[] fname = null;
            byte[] idn = null;
            byte[] gstatus = null;
            byte[] pureg = null;
            byte[] prodTy = null;
            byte[] loanPerk = null;
            byte[] pname = null;
            byte[] prevYear = null;
            byte[] currKgs = null;
            byte[] totCurr = null;
            byte[] ratePer = null;
            byte[] factBon = null;
            byte[] bonRate = null;
            byte[] perOfPrev = null;
            byte[] proRate = null;
            byte[] proRateLim = null;
            byte[] qAmount = null;
            byte[] outBalance = null;
            byte[] nAmount = null;

            byte[] start = null;
            byte[] saccoName = null;
            byte[] type = null;

            String b = SharedPrefs.read(SharedPrefs.LOAN_F_N, null);
            String a = SharedPrefs.read(SharedPrefs.LOAN_G_NO, null);
            String c = SharedPrefs.read(SharedPrefs.LOAN_ID, null);
            String d = SharedPrefs.read(SharedPrefs.LOAN_G_ST, null);
            String e = SharedPrefs.read(SharedPrefs.LOAN_PU_REG, null);
            String ia = SharedPrefs.read(SharedPrefs.LOAN_PREV_YR, null);
            String j = SharedPrefs.read(SharedPrefs.LOAN_CURR_KG, null);
            String m = SharedPrefs.read(SharedPrefs.LOAN_FACT, null);

            try {
                prodTy = String.valueOf("------" + String.valueOf(productType)+"------").getBytes("GB2312");
                loanPerk = String.valueOf("Loans Per Kgs :" + String.valueOf(loansPayKey)).getBytes("GB2312");
                pname = String.valueOf("Product Name :" +String.valueOf(productName)).getBytes("GB2312");
                totCurr = String.valueOf("Total Current : "+ String.valueOf(totCurrent)).getBytes("GB2312");
                ratePer = String.valueOf("Rate Per Kg : " + String.valueOf(ratePerKG)).getBytes("GB2312");
                bonRate = String.valueOf("Bonus Rate :" +String.valueOf(bonusRate)).getBytes("GB2312");
                perOfPrev = String.valueOf("% of Previous :"+String.valueOf(percantageOfPrevious)).getBytes("GB2312");
                proRate = String.valueOf("Proration Rate :"+String.valueOf(prorationRate)).getBytes("GB2312");
                proRateLim = String.valueOf("Proration Rate Limit : " + String.valueOf(prorationRateLimit)).getBytes("GB2312");
                qAmount = String.valueOf("Qualification :"+ String.valueOf(qualAmount)).getBytes("GB2312");
                outBalance = String.valueOf("Outstanding Bal : " + String.valueOf(outBal)).getBytes("GB2312");
                nAmount = String.valueOf("Net Amount :"+ String.valueOf(netAmount)).getBytes("GB2312");
            } catch (UnsupportedEncodingException e1) {
                e1.printStackTrace();
            }
            PrinterInterface.begin();

            byte[] cmd;
            cmd = new byte[]{(byte) 0x1B, (byte) 0x40};

            cmd = new byte[]{
                    (byte) 0x1B, (byte) 0x45, (byte) 0};

            write(cmd);
            writeLineBreak(1);
            write(prodTy);
            writeLineBreak(1);
            write(qAmount);
            writeLineBreak(1);
            write(outBalance);
            writeLineBreak(1);
            write(nAmount);
            writeLineBreak(1);
            PrinterInterface.end();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void write(final byte[] arryData) {
        int result = PrinterInterface.write(arryData, arryData.length);
    }
    private void writeLineBreak(int lineNumber) {
        write(PrinterCommand.getCmdEscDN(lineNumber));
    }

    void requestGetAccountDetails(String identifier) {

        final String TAG = "Loan Calculator";

        String URL = GFL.MSACCO_AGENT + GFL.GetGrowersDetails;
        final GrowersDetailsRequest growersDetails = new GrowersDetailsRequest();
        growersDetails.agentid = SharedPrefs.read(SharedPrefs.AGENT_ID_1, null);
        growersDetails.corporateno = "CAP016";
        growersDetails.accountidentifier = identifier;
        growersDetails.accountidentifiercode = "1";
        growersDetails.transactiontype = "1";
        growersDetails.terminalid = SharedPrefs.read(SharedPrefs.DEVICE_ID, null);
        growersDetails.longitude = getLong(this);
        growersDetails.latitude = getLat(this);
        growersDetails.date = getFormattedDate();

        Log.e(TAG, growersDetails.getBody().toString());
        arrayList.clear();
        spinner = findViewById(R.id.growerSpinner);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(LoanCalculator.this, android.R.layout.simple_spinner_item, arrayList);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(null);

        GFL.instance(this).request(URL, growersDetails, new GFL.RequestListener() {
            @Override
            public void onSuccess(String response) {
                /*deposit_progressBar.setVisibility(View.GONE);*/

                GrowersDetailsResponse growersDetailsResponse = GFL.instance(LoanCalculator.this)
                        .mGson.fromJson(response, GrowersDetailsResponse.class);
                if (growersDetailsResponse.is_successful) {
                    List<GrowerDetails> growerDetailsList = new ArrayList<>();

                    try {
                        JSONObject jsonObject = new JSONObject(response);

                        JSONArray jsonArray = jsonObject.getJSONArray("Growers");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                            JSONArray jsonArray1 = jsonObject1.getJSONArray("GrowerDetails");
                            for (int l = 0; l < jsonArray1.length(); l++) {
                                JSONObject jsonObject2 = jsonArray1.getJSONObject(l);
                                progressBar.setVisibility(View.GONE);
                                arrayList.add(String.valueOf(jsonObject2.getString("GrowerNo")));
                                /*mpesaPhoneNumber = jsonObject2.getString("phonenumber");*/
                                String gNo = jsonObject2.getString("GrowerNo");
                                String isDefaulter = jsonObject2.getString("LoanDefaulter");

                                SharedPrefs.write(SharedPrefs.IS_DEFAULTER, isDefaulter);
                                Log.e("Growers NUMBER:", gNo);
                            }

                        }

                        //Growers Number
                        spinner = findViewById(R.id.growerSpinner);
                        ArrayAdapter<String> adapter = new ArrayAdapter<String>(LoanCalculator.this, android.R.layout.simple_spinner_item, arrayList);
                        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                        spinner.setAdapter(adapter);


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                } else {
//                    Toast.makeText(ShareDeposit.this, "Accounts Fetching Failed", Toast.LENGTH_LONG).show();
                }

            }

            @Override
            public void onTokenExpired() {

            }

            @Override
            public void onError(String error) {

            }
        });
    }

    void submitWithoutFP(){
        progressDialog.dismiss();
        String b = SharedPrefs.read(SharedPrefs.LOAN_F_N, null);
        String a = SharedPrefs.read(SharedPrefs.LOAN_G_NO, null);
        String c = SharedPrefs.read(SharedPrefs.LOAN_ID, null);
        String d = SharedPrefs.read(SharedPrefs.LOAN_G_ST, null);
        String e = SharedPrefs.read(SharedPrefs.LOAN_PU_REG, null);
        String ia = SharedPrefs.read(SharedPrefs.LOAN_PREV_YR, null);
        String j = SharedPrefs.read(SharedPrefs.LOAN_CURR_KG, null);
        String m = SharedPrefs.read(SharedPrefs.LOAN_FACT, null);

        int result = PrinterInterface.open();
        header(a,b,c, d, e, ia, j, m);
        PrinterInterface.close();
        submitLoanCalculator();
    }

    @Override
    protected void onResume() {
        SharedPrefs.init(getApplicationContext());
        super.onResume();
        String fpBoolean = SharedPrefs.read(SharedPrefs.FP_BOOLEAN, null);

        if (fpBoolean.equals("true")) {
            progressDialog.dismiss();
            String b = SharedPrefs.read(SharedPrefs.LOAN_F_N, null);
            String a = SharedPrefs.read(SharedPrefs.LOAN_G_NO, null);
            String c = SharedPrefs.read(SharedPrefs.LOAN_ID, null);
            String d = SharedPrefs.read(SharedPrefs.LOAN_G_ST, null);
            String e = SharedPrefs.read(SharedPrefs.LOAN_PU_REG, null);
            String ia = SharedPrefs.read(SharedPrefs.LOAN_PREV_YR, null);
            String j = SharedPrefs.read(SharedPrefs.LOAN_CURR_KG, null);
            String m = SharedPrefs.read(SharedPrefs.LOAN_FACT, null);

            int result = PrinterInterface.open();
            header(a,b,c, d, e, ia, j, m);
            PrinterInterface.close();
            submitLoanCalculator();

        } else if (fpBoolean.equals("false")){
            progressDialog.dismiss();
            toast.swarn("Please authenticate to complete transaction!");
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.move_left_in_activity, R.anim.move_right_out_activity);
    }

}
