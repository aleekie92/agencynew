package com.coretec.agencyApplication.activities.gfl;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.cloudpos.jniinterface.PrinterInterface;
import com.coretec.agencyApplication.R;
import com.coretec.agencyApplication.activities.BaseActivity;
import com.coretec.agencyApplication.activities.LoginActivity;
import com.coretec.agencyApplication.activities.gfl.MainGFLDashboard;
import com.coretec.agencyApplication.adapters.ReportsAdapter;
import com.coretec.agencyApplication.api.Api;
import com.coretec.agencyApplication.api.requests.EodReportsRequest;
import com.coretec.agencyApplication.api.responses.Accounts;
import com.coretec.agencyApplication.api.responses.EodReportsResponse;
import com.coretec.agencyApplication.utils.LogOutTimerUtil;
import com.coretec.agencyApplication.utils.PreferenceFileKeys;
import com.coretec.agencyApplication.utils.SharedPrefs;
import com.coretec.agencyApplication.utils.Utils;
import com.coretec.agencyApplication.wizarpos.function.printer.PrinterCommand;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.coretec.agencyApplication.utils.Utils.getFormattedDate;
import static com.coretec.agencyApplication.utils.Utils.getLat;
import static com.coretec.agencyApplication.utils.Utils.getLong;

public class Reports extends BaseActivity implements  LogOutTimerUtil.LogOutListener {

    private List<Accounts> accountsList;
    private RecyclerView reports_recyclerView;
    private ReportsAdapter reportsAdapter;
    SharedPreferences sharedPreferences;
    private TextView reports_agent_name;
    private TextView reports_sacco_name;
    private ImageView backBtn;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        SharedPrefs.init(getApplicationContext());
        setContentView(R.layout.gfl_eod_report);

        this.overridePendingTransition(R.anim.move_right_in_activity, R.anim.move_left_out_activity);
        accountsList = new ArrayList<>();
        sharedPreferences = getSharedPreferences(PreferenceFileKeys.PREFS_FILE, Context.MODE_PRIVATE);

        String agentName = SharedPrefs.read(SharedPrefs.GFL_AGENT_NAME, null);
        String saccoName = sharedPreferences.getString(PreferenceFileKeys.AGENT_SACCO_NAME, "");
        reports_agent_name = findViewById(R.id.reports_agent_name);
        reports_agent_name.setText(agentName);

        reports_sacco_name = findViewById(R.id.reports_sacco_name);
        reports_sacco_name.setText("GreenLand Fedha.");

        reports_recyclerView = findViewById(R.id.eodRecyclerView);
        reports_recyclerView.setHasFixedSize(true);
        reportsAdapter = new ReportsAdapter(this, accountsList);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
        reports_recyclerView.setLayoutManager(mLayoutManager);

        final Map<String, Object> params = new HashMap<>();
        params.put("accountsList", accountsList);
        final String TAG = "request reports";

        String URL = Api.MSACCO_AGENT + Api.GetEODReport;
        EodReportsRequest request = new EodReportsRequest();
        request.corporateno = MainGFLDashboard.corporateno;
        request.agentid = sharedPreferences.getString(PreferenceFileKeys.AGENT_ID, "");
        request.terminalid = MainGFLDashboard.imei;
        request.longitude = getLong(this);
        request.latitude = getLat(this);
        request.requestdate = getFormattedDate();
        Log.e(TAG, request.getBody().toString());
        Api.instance(this).request(URL, request, new Api.RequestListener() {
            @Override
            public void onSuccess(String response) {
                Log.e(TAG, response);
                final EodReportsResponse eodReportsResponse = Api.instance(Reports.this).mGson.fromJson(response, EodReportsResponse.class);
                if (eodReportsResponse.is_successful) {
                  /*  if(accountsList.isEmpty()){
                        Utils.showAlertDialog(Reports.this, "Alert!", "You have not done any transaction today");
                    }else {*/
                        accountsList.addAll(eodReportsResponse.getAccounts());
                        reports_recyclerView.setAdapter(reportsAdapter);
                      /*  final String agentName = sharedPreferences.getString(PreferenceFileKeys.AGENT_ACCOUNT_NAME, "");
                        final String agentSaccoName = sharedPreferences.getString(PreferenceFileKeys.AGENT_SACCO_NAME, "");
                        final String agentBusinessName = sharedPreferences.getString(PreferenceFileKeys.AGENT_BUSINESS_NAME, "");
                        int result = PrinterInterface.open();
                        writetest(agentName, eodReportsResponse.getAccounts(), agentSaccoName, eodReportsResponse.getTransactiondate(), "REPORTS");
                        PrinterInterface.close();*/
                   // }
                } else {
                    Utils.showAlertDialog(Reports.this, "Reports Retrieval Failed", eodReportsResponse.getError());
                }
            }

            @Override
            public void onTokenExpired() {

            }

            @Override
            public void onError(String error) {

            }
        });
    }


    public void writetest(String agentName, List<Accounts> accountsList, String saccoame, String dateed, String transactionType) {
        try {

            byte[] arrySaccoName = null;
            byte[] arryDepositAmt = null;
            byte[] arryRefID = null;
            byte[] arryAmount = null;
            byte[] arryTransactionType = null;

            byte[] arryMotto = null;
            byte[] terminalNo = null;
            byte[] date = null;
            byte[] transactionTYpe = null;
            byte[] servedBy = null;
            byte[] signature = null;
            byte[] id = null;


            try {
                arrySaccoName = String.valueOf("       " + String.valueOf(saccoame)).getBytes("GB2312");
                terminalNo = String.valueOf(" Terminal ID :" + Utils.getIMEI(Reports.this)).getBytes("GB2312");

                date = String.valueOf("Date : " + dateed).getBytes("GB2312");

                transactionTYpe = String.valueOf("      " + transactionType).getBytes("GB2312");
                arryDepositAmt = String.valueOf("Ref No     Amount     Type  ").getBytes("GB2312");


                id = String.valueOf("ID :_________________________").getBytes("GB2312");
                signature = String.valueOf("SIGNATURE : _____________________").getBytes("GB2312");
                servedBy = String.valueOf(" You were Served By : " + agentName).getBytes("GB2312");
                arryMotto = "         BASE FOR GROWTH      ".getBytes("GB2312");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            PrinterInterface.begin();

            byte[] cmd;
            cmd = new byte[]{(byte) 0x1B, (byte) 0x40};

            cmd = new byte[]{
                    (byte) 0x1B, (byte) 0x45, (byte) 0};


            write(cmd);
            write(arrySaccoName);
            writeLineBreak(2);
            write(terminalNo);
            writeLineBreak(2);
            write(date);
            writeLineBreak(2);
            write(transactionTYpe);
            writeLineBreak(2);
            write(arryDepositAmt);

            writeLineBreak(4);

            for (int i = 0; i < accountsList.size(); i++) {
               /* arryRefID = String.valueOf(accountsList.get(i).getReferenceno()).getBytes("GB2312");
                write(arryRefID);
*/
                arryAmount = String.valueOf("  " + accountsList.get(i).getCount()).getBytes("GB2312");
                write(arryAmount);

                arryTransactionType = String.valueOf("  " + accountsList.get(i).getTransactiontype()).getBytes("GB2312");
                write(arryTransactionType);
                writeLineBreak(2);

            }


//            for (Accounts accounts : accountsList) {
//
//                write(accounts.getReferenceno().getBytes("GB2312"));
//                writeLineBreak(2);
//                write(String.valueOf(accounts.getAmount()).getBytes("GB2312"));
//                writeLineBreak(2);
//                write(accounts.getTransactiontype().getBytes("GB2312"));
//
//                writeLineBreak(1);
//            }


            writeLineBreak(2);
            write(id);
            writeLineBreak(3);
            write(signature);
            writeLineBreak(2);
            write(servedBy);
            writeLineBreak(4);
            write(arryMotto);
            writeLineBreak(5);
            PrinterInterface.end();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void write(final byte[] arryData) {
        int result = PrinterInterface.write(arryData, arryData.length);
    }

    // print line break
    private void writeLineBreak(int lineNumber) {
        // write(new byte[]{'\n'});
        write(PrinterCommand.getCmdEscDN(lineNumber));
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.move_left_in_activity, R.anim.move_right_out_activity);
    }

    @Override
    protected void onStart() {
        super.onStart();
        //LogOutTimerUtil.startLogoutTimer(this, this);

    }

    @Override
    public void onUserInteraction() {
        super.onUserInteraction();
        LogOutTimerUtil.startLogoutTimer(this, this);

    }


    @Override
    protected void onPause() {
        super.onPause();
        LogOutTimerUtil.startLogoutTimer(this, this);

    }

    @Override
    protected void onResume() {
        super.onResume();
        LogOutTimerUtil.startLogoutTimer(this, this);

    }

    @Override
    public void doLogout() {
        finish();
    }
}
