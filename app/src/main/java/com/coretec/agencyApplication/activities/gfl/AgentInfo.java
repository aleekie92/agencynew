package com.coretec.agencyApplication.activities.gfl;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MenuItem;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.coretec.agencyApplication.R;
import com.coretec.agencyApplication.activities.BaseActivity;
import com.coretec.agencyApplication.adapters.LastFiveAdapter;
import com.coretec.agencyApplication.adapters.ReportsAdapter;
import com.coretec.agencyApplication.api.GFL;
import com.coretec.agencyApplication.api.requests.GetAgentAccountInfo;
import com.coretec.agencyApplication.api.responses.Accounts;
import com.coretec.agencyApplication.api.responses.GflAgentAccountResponse;
import com.coretec.agencyApplication.api.responses.LastFiveList;
import com.coretec.agencyApplication.utils.LogOutTimerUtil;
import com.coretec.agencyApplication.utils.PreferenceFileKeys;
import com.coretec.agencyApplication.utils.SharedPrefs;

import java.util.ArrayList;
import java.util.List;

import static com.coretec.agencyApplication.utils.Utils.getFormattedDate;
import static com.coretec.agencyApplication.utils.Utils.getIMEI;
import static com.coretec.agencyApplication.utils.Utils.getLat;
import static com.coretec.agencyApplication.utils.Utils.getLong;
import static com.coretec.agencyApplication.utils.Utils.writeImageToMedia;

public class AgentInfo extends  BaseActivity implements  LogOutTimerUtil.LogOutListener {

    private TextView agent_sacco_name, agent_name,agent_code,agent_factory,agent_location,
            agent_commision_bal,agent_account_balance,   phone,region,zone,vendername;

    ImageView backBtn;
    SharedPreferences sharedPreferences;
    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        SharedPrefs.init(getApplicationContext());

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.gfl_agent_info);

        this.overridePendingTransition(R.anim.move_right_in_activity, R.anim.move_left_out_activity);
        sharedPreferences = getSharedPreferences(PreferenceFileKeys.PREFS_FILE, Context.MODE_PRIVATE);

        agent_sacco_name= findViewById(R.id.agent_sacco_name);
        agent_name= findViewById(R.id.agent_name);
        agent_code= findViewById(R.id.agent_code);
        agent_factory= findViewById(R.id.agent_factory);
        agent_location= findViewById(R.id.agent_location);
        agent_commision_bal= findViewById(R.id.agent_commision_bal);
        agent_account_balance= findViewById(R.id.agent_account_balance);
        vendername= findViewById(R.id.vendername);

        phone= findViewById(R.id.phone);
        region= findViewById(R.id.region);
        zone= findViewById(R.id.zone);

        progressDialog = new ProgressDialog(AgentInfo.this);
        progressDialog.setMessage("Getting agent information, please wait...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        requestAgentInfo();

    }

    void requestAgentInfo() {
        final String TAG = "request agent Info";
        String URL = GFL.MSACCO_AGENT + GFL.GetAgentAccountInfo;
        GetAgentAccountInfo accountInfo = new GetAgentAccountInfo();

        accountInfo.agentid = SharedPrefs.read(SharedPrefs.AGENT_ID_1, null);
        accountInfo.terminalid = getIMEI(this);
        accountInfo.longitude = getLong(this);
        accountInfo.latitude = getLat(this);
        accountInfo.date = getFormattedDate();

        Log.e(TAG, accountInfo.getBody().toString());

        GFL.instance(this).request(URL, accountInfo, new GFL.RequestListener() {
            @Override
            public void onSuccess(String response) {
                Log.e(TAG, response);
                final GflAgentAccountResponse agentAccountResponse = GFL.instance(AgentInfo.this).mGson.fromJson(response, GflAgentAccountResponse.class);

                if (agentAccountResponse.is_successful) {
                    progressDialog.dismiss();
                    agent_sacco_name.setText("GREENFEDHA MASHINANI");
                    agent_name.setText(agentAccountResponse.getAgentaccountname());
                    agent_code.setText(SharedPrefs.read(SharedPrefs.AGENT_ID_1, null));

                    agent_factory.setText(agentAccountResponse.getFactoryname());
                    agent_location.setText(agentAccountResponse.getAgentlacationcode());
                    agent_commision_bal.setText(agentAccountResponse.getAgentcommisionbal());
                    agent_account_balance.setText(agentAccountResponse.getAgentaccountbal());

                    phone.setText(agentAccountResponse.getPhonenumber());
                    region.setText(agentAccountResponse.getRegion());
                    zone.setText(agentAccountResponse.getZone());
                    vendername.setText(agentAccountResponse.getVendorname());

                } else {
                    progressDialog.dismiss();
                    Toast.makeText(getApplicationContext(), "Could not get agent information, please try again!", Toast.LENGTH_LONG).show();
                }

            }

            @Override
            public void onTokenExpired() {

            }

            @Override
            public void onError(String error) {

            }
        });

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                finish();
                //Toast.makeText(Account.this, "Back has been presses!", Toast.LENGTH_LONG).show();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.move_left_in_activity, R.anim.move_right_out_activity);
    }
    @Override
    protected void onStart() {
        super.onStart();
        //LogOutTimerUtil.startLogoutTimer(this, this);

    }

    @Override
    public void onUserInteraction() {
        super.onUserInteraction();
        LogOutTimerUtil.startLogoutTimer(this, this);

    }


    @Override
    protected void onPause() {
        super.onPause();
        LogOutTimerUtil.startLogoutTimer(this, this);

    }

    @Override
    protected void onResume() {
        super.onResume();
        LogOutTimerUtil.startLogoutTimer(this, this);

    }

    @Override
    public void doLogout() {
        finish();
    }
}
