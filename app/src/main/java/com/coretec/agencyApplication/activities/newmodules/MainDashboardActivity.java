package com.coretec.agencyApplication.activities.newmodules;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.cloudpos.POSTerminal;
import com.cloudpos.fingerprint.FingerprintDevice;
import com.coretec.agencyApplication.R;
import com.coretec.agencyApplication.activities.AgentInfomation;
import com.coretec.agencyApplication.activities.BaseActivity;
import com.coretec.agencyApplication.activities.LoginActivity;
import com.coretec.agencyApplication.activities.PrintLastTransaction;
import com.coretec.agencyApplication.activities.Reports;
import com.coretec.agencyApplication.activities.SplashActivity;
import com.coretec.agencyApplication.activities.gfl.MainGFLDashboard;
import com.coretec.agencyApplication.api.Api;
import com.coretec.agencyApplication.api.requests.FlexibleMenuRequest;
import com.coretec.agencyApplication.api.requests.GetAgentAccountInfo;
import com.coretec.agencyApplication.api.responses.AgentAccountResponse;
import com.coretec.agencyApplication.api.responses.FlexibleMenuResponse;
import com.coretec.agencyApplication.utils.LogOutTimerUtil;
import com.coretec.agencyApplication.utils.PreferenceFileKeys;
import com.coretec.agencyApplication.utils.SharedPrefs;
import com.coretec.agencyApplication.utils.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Calendar;

import static com.coretec.agencyApplication.utils.Utils.getFormattedDate;
import static com.coretec.agencyApplication.utils.Utils.getIMEI;
import static com.coretec.agencyApplication.utils.Utils.getLat;
import static com.coretec.agencyApplication.utils.Utils.getLong;
public class MainDashboardActivity extends BaseActivity implements  LogOutTimerUtil.LogOutListener{

    private Button  btnMainLoanRequest, btnReports,
            btnAgentAccountInfo, btnPrintLastTransaction, btnCustomerTransactions, btnRegistry;


    private CardView mr, cardloanapplication, customertransaction,cardlasttransactionprint;
    private ProgressDialog progressDialog;
    SharedPreferences sharedPref;
    public static String imei = "", corporateno = "", saccomotto = "";
    public String greetings,timeee;
    private ProgressBar main_progressBar;

    FingerprintDevice fingerprintDevice =
            (FingerprintDevice) POSTerminal.getInstance(MainDashboardActivity.this).getDevice("cloudpos.device.fingerprint");


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.newmodule_dash);
        this.overridePendingTransition(R.anim.move_right_in_activity, R.anim.move_left_out_activity);
        imei = Utils.getIMEI(this);
        sharedPref = getSharedPreferences(PreferenceFileKeys.PREFS_FILE, Context.MODE_PRIVATE);


        progressDialog = new ProgressDialog(MainDashboardActivity.this);
        progressDialog.setMessage("Synchronizing Menu, give it a second...");
        progressDialog.setCancelable(false);
        progressDialog.show();

         requestAgentInfo();

        main_progressBar=findViewById(R.id.main_progressBar);
        customertransaction = findViewById(R.id.customertransaction);//
        mr = findViewById(R.id.mr);//
        cardloanapplication = findViewById(R.id.cardloanapplication);//
        cardlasttransactionprint = findViewById(R.id.cardlasttransactionprint);//
        btnCustomerTransactions = findViewById(R.id.btnCustomerTransactions);
        getTime();
        //mr.setVisibility(View.VISIBLE);
        btnCustomerTransactions.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainDashboardActivity.this, CustomerTransaction.class));
            }
        });

        btnRegistry = findViewById(R.id.btnRegistry);
        btnRegistry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainDashboardActivity.this, RegistryActivity.class));
            }
        });

        btnMainLoanRequest = (Button) findViewById(R.id.btnMainLoanRequest);
        btnMainLoanRequest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainDashboardActivity.this, LoanActivity.class));
            }
        });

        btnAgentAccountInfo = findViewById(R.id.btnAgentAccountInfo);
        btnAgentAccountInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainDashboardActivity.this, AgentInfomation.class));
            }
        });

        btnReports = findViewById(R.id.btnReports);
        btnReports.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainDashboardActivity.this, Reports.class));
            }
        });

        btnPrintLastTransaction = findViewById(R.id.btnPrintLastTransaction);
        btnPrintLastTransaction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainDashboardActivity.this, PrintLastTransaction.class));
            }
        });

    }

    public void getTime() {
        Calendar c = Calendar.getInstance();
        int timeOfDay = c.get(Calendar.HOUR_OF_DAY);
        if (timeOfDay >= 0 && timeOfDay < 6) {
            greetings = "Good Morning";
        } else if (timeOfDay >= 6 && timeOfDay < 12) {
            greetings = "Good Morning";
            customertransaction.setVisibility(View.VISIBLE);
            mr.setVisibility(View.VISIBLE);
            cardloanapplication.setVisibility(View.VISIBLE);
            cardlasttransactionprint.setVisibility(View.VISIBLE);
        } else if (timeOfDay >= 12 && timeOfDay < 16) {
            greetings = "Good Afternoon";
            customertransaction.setVisibility(View.VISIBLE);
            mr.setVisibility(View.VISIBLE);
            cardloanapplication.setVisibility(View.VISIBLE);
            cardlasttransactionprint.setVisibility(View.VISIBLE);
        } else if (timeOfDay >= 16 && timeOfDay < 22) {
            greetings = "Good Evening";
            customertransaction.setVisibility(View.VISIBLE);
            mr.setVisibility(View.VISIBLE);
            cardloanapplication.setVisibility(View.VISIBLE);
            cardlasttransactionprint.setVisibility(View.VISIBLE);
        } else if (timeOfDay >= 22 && timeOfDay < 24) {
            greetings = "It's bed time";
        }
    }

    void requestAgentInfo() {
        final String TAG = "request agent Info";
        String URL = Api.MSACCO_AGENT + Api.GetAgentAccountInfo;
        GetAgentAccountInfo accountInfo = new GetAgentAccountInfo();
        accountInfo.agentid = sharedPref.getString(PreferenceFileKeys.AGENT_ID, "");
        accountInfo.terminalid = getIMEI(this);
        accountInfo.longitude = getLong(this);
        accountInfo.latitude = getLat(this);
        accountInfo.date = getFormattedDate();
        Log.e(TAG, accountInfo.getBody().toString());

        Api.instance(this).request(URL, accountInfo, new Api.RequestListener() {
            @Override
            public void onSuccess(String response) {
                Log.e(TAG, response);

                final AgentAccountResponse agentAccountResponse = Api.instance(MainDashboardActivity.this).mGson.fromJson(response, AgentAccountResponse.class);
                if (agentAccountResponse.is_successful) {
                    progressDialog.dismiss();
                    corporateno = agentAccountResponse.getCorporateno();
                    SharedPrefs.write(SharedPrefs.VR_COOPORATE_NO, corporateno);
                    SharedPrefs.cooporateno(agentAccountResponse.getCorporateno(), MainDashboardActivity.this);
                    SharedPreferences.Editor editor = sharedPref.edit();
                    Toast.makeText(MainDashboardActivity.this, getString(R.string.app_time, agentAccountResponse.getAgentdetails().getAccountname(), greetings), Toast.LENGTH_LONG).show();
                    editor.putString(PreferenceFileKeys.AGENT_SACCO_NAME, agentAccountResponse.getSacconame());
                    editor.putString(PreferenceFileKeys.AGENT_SACCO_MOTTO, agentAccountResponse.getSaccomotto());

                    SharedPrefs.write(SharedPrefs.NEW_SACCO_NAME, agentAccountResponse.getSacconame());
                    timeee = agentAccountResponse.getDate();
                    saccomotto = agentAccountResponse.getSaccomotto();
                    editor.putString(PreferenceFileKeys.AGENT_ACCOUNT_NAME, agentAccountResponse.getAgentdetails().getAccountname());
                    editor.putString(PreferenceFileKeys.AGENT_BUSINESS_NAME, agentAccountResponse.getAgentdetails().getProposedname());
                    editor.putString(PreferenceFileKeys.AGENT_ACCOUNT_NO, agentAccountResponse.getAgentdetails().getAgentaccno());
                    editor.putString(PreferenceFileKeys.AGENT_LOCATION, agentAccountResponse.getAgentdetails().getLocation());
                    editor.putString(PreferenceFileKeys.AGENT_ACCOUNT_BALANCE, agentAccountResponse.getAgentdetails().getAccountbalance());
                    editor.putString(PreferenceFileKeys.AGENT_COMMISSION_BALANCE, agentAccountResponse.getAgentdetails().getAccountcommissionbalance());
                    editor.apply();

                    final FlexibleMenuRequest dynamicMenu = new FlexibleMenuRequest();
                    dynamicMenu.agentid = SharedPrefs.read(SharedPrefs.AGENT_ID_1, null);
                    dynamicMenu.terminalid = getIMEI(MainDashboardActivity.this);
                    dynamicMenu.longitude = getLong(MainDashboardActivity.this);
                    dynamicMenu.latitude = getLat(MainDashboardActivity.this);
                    dynamicMenu.date = getFormattedDate();
                    dynamicMenu.corporate_no = agentAccountResponse.getCorporateno();

                    Api.instance(MainDashboardActivity.this).request(Api.MSACCO_AGENT + Api.DynamicMenu, dynamicMenu, new Api.RequestListener() {
                        @Override
                        public void onSuccess(String response) {
                            main_progressBar.setVisibility(View.GONE);
                            FlexibleMenuResponse dynamicmenuresponse = Api.instance(MainDashboardActivity.this).mGson.fromJson(response, FlexibleMenuResponse.class);

                            if (dynamicmenuresponse.operationsuccess) {
                                main_progressBar.setVisibility(View.GONE);
                                try {
                                    JSONObject jsonObject = new JSONObject(response);
                                    String transcurrency = jsonObject.getString("currencycode");
                                    SharedPrefs.currency(transcurrency,MainDashboardActivity.this);
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            } else {

                            }

                        }

                        @Override
                        public void onTokenExpired() {

                        }

                        @Override
                        public void onError(String error) {
                            Utils.showAlertDialog(MainDashboardActivity.this, "Failed", error);
                        }
                    });

                } else {
                    progressDialog.dismiss();
                    Utils.showAlertDialog(MainDashboardActivity.this, "Failed", "Could not get agent details, Please try again ", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            progressDialog.setMessage("Synchronizing Menu, please wait...");
                            progressDialog.setCancelable(false);
                            progressDialog.show();
                            requestAgentInfo();
                        }
                    });
                    //Toast.makeText(MainDashboardActivity.this, "Failed, try again...", Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onTokenExpired() {

            }

            @Override
            public void onError(String error) {
                //progressDialog.dismiss();
                Utils.showAlertDialog(MainDashboardActivity.this, "Failed", error);
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_logout) {
            String msg = "Are you sure you want to Logout? ";
            AlertDialog.Builder builder1 = new AlertDialog.Builder(MainDashboardActivity.this);
            builder1.setMessage(msg);
            builder1.setTitle("Alert!");
            builder1.setCancelable(false);

            builder1.setPositiveButton(
                    "yes",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                            //System.exit(0);
                            finish();
                            Intent i  = new Intent(MainDashboardActivity.this, LoginActivity.class);
                            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                            i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

                            startActivity(i);
                        }

                    });

            builder1.setNegativeButton(
                    "CANCEL",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                            //onBackPressed();
                        }
                    });

            //System.exit(1);
            AlertDialog alert11 = builder1.create();
            alert11.show();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        Toast.makeText(this, "You need some rest? Use Logout button", Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    public void onUserInteraction() {
        super.onUserInteraction();

    }

    @Override
    protected void onPause() {
        super.onPause();
        LogOutTimerUtil.startLogoutTimer(this, this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        LogOutTimerUtil.startLogoutTimer(this, this);
    }

    /*@Override
    protected void onStop() {
        super.onStop();
        Intent i = new Intent(MainDashboardActivity.this, SplashActivity.class);
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        this.startActivity(i);
    }*/

    @Override
    public void doLogout() {
        finish();
        //System.exit(0);
        Intent i  = new Intent(MainDashboardActivity.this, LoginActivity.class);
        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(i);
    }

}
