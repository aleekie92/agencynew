package com.coretec.agencyApplication.activities.gfl;

import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Handler;
import android.support.design.widget.TextInputLayout;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Spinner;

import com.cloudpos.jniinterface.PrinterInterface;
import com.coretec.agencyApplication.R;
import com.coretec.agencyApplication.activities.BaseActivity;
import com.coretec.agencyApplication.api.GFL;
import com.coretec.agencyApplication.api.requests.RequestWithdrawal;
import com.coretec.agencyApplication.api.responses.WithdrawalResponse;
import com.coretec.agencyApplication.utils.PreferenceFileKeys;
import com.coretec.agencyApplication.utils.Utils;
import com.coretec.agencyApplication.wizarpos.function.ActionCallbackImpl;
import com.coretec.agencyApplication.wizarpos.function.printer.PrinterCommand;
import com.coretec.agencyApplication.wizarpos.mvc.base.ActionCallback;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

import static com.coretec.agencyApplication.utils.Utils.getFormattedDate;
import static com.coretec.agencyApplication.utils.Utils.getIMEI;
import static com.coretec.agencyApplication.utils.Utils.getLat;
import static com.coretec.agencyApplication.utils.Utils.getLong;

public class LoanApplication extends BaseActivity {

    private Button btnConfirmApplication;
    private ProgressBar LAprogressBar;
    private TextInputLayout input_layout_id_number;
    private TextInputLayout input_layout_amount;
    private EditText id_number;
    private EditText amount;
    private String loan_app_id_no, loan_app_amount;
    public static Handler handler;
    public static ActionCallback callback;

    private EditText growersNumber;
    /*private ArrayList<String> arrayList = new ArrayList<>();
    private ArrayAdapter<AccountDetails> adapter;*/

    private Spinner spinner2;
    private ArrayList<String> arrayList2 = new ArrayList<>();
    private ArrayAdapter<String> adapter2;

    SharedPreferences sharedPreferences;
    String accountNo = "";
    String identifiercode = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.gfl_loan_application);

        this.overridePendingTransition(R.anim.move_right_in_activity, R.anim.move_left_out_activity);

        sharedPreferences = getSharedPreferences(PreferenceFileKeys.PREFS_FILE, Context.MODE_PRIVATE);

        handler = new Handler();
        callback = new ActionCallbackImpl(handler);

        growersNumber = findViewById(R.id.growersNoSpinner);


        spinner2 = findViewById(R.id.spinner_deposit);
        adapter2 = new ArrayAdapter<String>(getApplicationContext(), R.layout.simple_spinner_dropdown_item, arrayList2);
        adapter2.setDropDownViewResource(R.layout.simple_spinner_dropdown_item);
        spinner2.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                identifiercode = String.valueOf(position);
                // Toast.makeText(getApplicationContext(),String.valueOf(position),Toast.LENGTH_LONG).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        LAprogressBar = findViewById(R.id.loanAppPB);
        LAprogressBar.setVisibility(View.GONE);

        id_number = findViewById(R.id.id_number);
        amount = findViewById(R.id.amount);

        input_layout_id_number = findViewById(R.id.input_layout_id_number);
        input_layout_amount = findViewById(R.id.input_layout_amount);

        TextWatcher textWatcher = new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                //here, after we introduced something in the EditText we get the string from it
                String identifier = id_number.getText().toString();
                if (identifier.length() >= 7) {
//                Toast.makeText(ShareDeposit.this, accountNumber, Toast.LENGTH_SHORT).show();
                    /*requestGetAccountDetails(identifier);*/
                }

            }
        };

        id_number.addTextChangedListener(textWatcher);



        btnConfirmApplication = findViewById(R.id.submitLoanApp);
        btnConfirmApplication.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                loan_app_id_no = id_number.getText().toString().trim();
                loan_app_amount = amount.getText().toString().trim();

                if (loan_app_id_no.isEmpty()) {
                    input_layout_id_number.setError("Enter the Member Account Number");
                } else if (loan_app_amount.isEmpty() || loan_app_amount.startsWith("0")) {
                    input_layout_amount.setError("Enter the Amount to Withdraw");
                } else {
                    Log.e("member_identifier", loan_app_id_no);
                    Log.e("withdrawn amount", loan_app_amount);
                    LAprogressBar.setVisibility(View.VISIBLE);

                    requestLoanApp(loan_app_id_no, loan_app_amount);
                    btnConfirmApplication.setClickable(false);
                }
            }
        });

    }

    void requestLoanApp(String loan_app_id_no, final String loan_app_amount) {

        final String TAG = "withdraw cash ";

        Log.e(TAG, loan_app_id_no);
        Log.e(TAG, String.valueOf(loan_app_amount));

        String URL = GFL.MSACCO_AGENT + GFL.AgentFundsWithdrawal;

        RequestWithdrawal requestWithdrawal = new RequestWithdrawal();
        requestWithdrawal.agentid = sharedPreferences.getString(PreferenceFileKeys.AGENT_ID, "");
        requestWithdrawal.corporate_no = "CAP016";
        requestWithdrawal.accountidentifier = loan_app_id_no;
        requestWithdrawal.accountidentifiercode = identifiercode;
        requestWithdrawal.accountno = accountNo;
        requestWithdrawal.amount = Integer.parseInt(loan_app_amount);
        requestWithdrawal.terminalid = getIMEI(this);
        requestWithdrawal.longitude = getLong(this);
        requestWithdrawal.latitude = getLat(this);
        requestWithdrawal.date = getFormattedDate();

        Log.e(TAG, requestWithdrawal.getBody().toString());

        GFL.instance(this).request(URL, requestWithdrawal, new GFL.RequestListener() {
            @Override
            public void onSuccess(String response) {
                LAprogressBar.setVisibility(View.GONE);
                Log.e(TAG, response);
                final WithdrawalResponse withdrawalResponse = GFL.instance(LoanApplication.this).mGson.fromJson(response, WithdrawalResponse.class);
                if (withdrawalResponse.is_successful) {

                    final String agentName = sharedPreferences.getString(PreferenceFileKeys.AGENT_ACCOUNT_NAME, "");

                    int result = PrinterInterface.open();
                    writetest(accountNo, agentName, withdrawalResponse.getAmount(), withdrawalResponse.getSacconame(), withdrawalResponse.getCustomername(), withdrawalResponse.getReceiptno(), withdrawalResponse.getTransactiondate(), withdrawalResponse.getTransactiontype());
                    PrinterInterface.close();

                    Utils.showAlertDialog(LoanApplication.this, "Loan Application Successful", "Click OK to Print Again", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            int result = PrinterInterface.open();
                            writetest1(accountNo, agentName, withdrawalResponse.getAmount(), withdrawalResponse.getSacconame(), withdrawalResponse.getCustomername(), withdrawalResponse.getReceiptno(), withdrawalResponse.getTransactiondate(), withdrawalResponse.getTransactiontype());
                            PrinterInterface.close();
                        }
                    });

                } else {
                    final String agentName = sharedPreferences.getString(PreferenceFileKeys.AGENT_ACCOUNT_NAME, "");
                    int result = PrinterInterface.open();
                    writetest(accountNo, agentName, withdrawalResponse.getAmount(), withdrawalResponse.getSacconame(), withdrawalResponse.getCustomername(), withdrawalResponse.getReceiptno(), withdrawalResponse.getTransactiondate(), withdrawalResponse.getTransactiontype());
                    PrinterInterface.close();
                    //Utils.showAlertDialog(LoanApplication.this, "Withdrawal Failed", withdrawalResponse.getError());
                }
            }

            @Override
            public void onTokenExpired() {

            }

            @Override
            public void onError(String error) {

            }
        });

    }

    public void writetest(String accountNumber, String agentName, int amount, String saccoame, String customerName, String receiptNumber, String dateed, String transactionType) {
        try {

            byte[] arrySaccoName = null;
            byte[] arryCustomerName = null;
            byte[] arryWithdrawAmt = null;
            byte[] arryAmt = null;

            byte[] arryMotto = null;
            byte[] terminalNo = null;
            byte[] receiptNo = null;
            byte[] date = null;
            byte[] transactionTYpe = null;
            byte[] accountNum = null;
            byte[] servedBy = null;
            byte[] signature = null;
            byte[] customerReceipt = null;


            try {
                arrySaccoName = String.valueOf("       " + String.valueOf(saccoame)).getBytes("GB2312");
                terminalNo = String.valueOf(" Terminal ID :" + Utils.getIMEI(LoanApplication.this)).getBytes("GB2312");
                receiptNo = String.valueOf(" Receipt Number : " + receiptNumber).getBytes("GB2312");
                date = String.valueOf("Date : " + dateed).getBytes("GB2312");
                arryCustomerName = String.valueOf("Name : " + customerName).getBytes("GB2312");
                transactionTYpe = String.valueOf("      " + transactionType).getBytes("GB2312");
                arryWithdrawAmt = String.valueOf(" Total Amount Applied :").getBytes("GB2312");
                arryAmt = String.valueOf(String.valueOf("    KSH " + amount)).getBytes("GB2312");
                accountNum = String.valueOf(String.valueOf("    ACC NO : " + accountNumber)).getBytes("GB2312");
                servedBy = String.valueOf(" You were Served By " + agentName).getBytes("GB2312");
                signature = String.valueOf("Signature__________________________").getBytes("GB2312");
                customerReceipt = String.valueOf("         CUSTOMER COPY         ").getBytes("GB2312");

                arryMotto = "         BASE FOR GROWTH      ".getBytes("GB2312");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            PrinterInterface.begin();

            byte[] cmd;
            cmd = new byte[]{(byte) 0x1B, (byte) 0x40};

            cmd = new byte[]{
                    (byte) 0x1B, (byte) 0x45, (byte) 0};

            write(cmd);
            write(arrySaccoName);
            writeLineBreak(1);
            write(terminalNo);
            writeLineBreak(1);
            write(receiptNo);
            writeLineBreak(1);
            write(date);
            writeLineBreak(1);
            write(arryCustomerName);
            writeLineBreak(1);
            write(transactionTYpe);
            // print line break
            writeLineBreak(2);

            write(arryWithdrawAmt);
            writeLineBreak(1);
            write(arryAmt);
            writeLineBreak(1);
            write(accountNum);
            writeLineBreak(1);
            write(servedBy);
            // print line break
            writeLineBreak(2);
            write(signature);
            writeLineBreak(2);
            write(customerReceipt);
            writeLineBreak(2);
            write(arryMotto);
            writeLineBreak(4);
            PrinterInterface.end();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void writetest1(String accountNumber, String agentName, int amount, String saccoame, String customerName, String receiptNumber, String dateed, String transactionType) {
        try {

            byte[] arrySaccoName = null;
            byte[] arryCustomerName = null;
            byte[] arryWithdrawAmt = null;
            byte[] arryAmt = null;

            byte[] arryMotto = null;
            byte[] terminalNo = null;
            byte[] receiptNo = null;
            byte[] date = null;
            byte[] transactionTYpe = null;
            byte[] accountNum = null;
            byte[] servedBy = null;
            byte[] signature = null;
            byte[] agentReceipt = null;


            try {
                arrySaccoName = String.valueOf("       " + String.valueOf(saccoame)).getBytes("GB2312");
                terminalNo = String.valueOf(" Terminal ID :" + Utils.getIMEI(LoanApplication.this)).getBytes("GB2312");
                receiptNo = String.valueOf(" Receipt Number : " + receiptNumber).getBytes("GB2312");
                date = String.valueOf("Date : " + dateed).getBytes("GB2312");
                arryCustomerName = String.valueOf("Name : " + customerName).getBytes("GB2312");
                transactionTYpe = String.valueOf("      " + transactionType).getBytes("GB2312");
                arryWithdrawAmt = String.valueOf(" Total Amount Applied :").getBytes("GB2312");
                arryAmt = String.valueOf(String.valueOf("    KSH " + amount)).getBytes("GB2312");
                accountNum = String.valueOf(String.valueOf("    ACC NO : " + accountNumber)).getBytes("GB2312");
                servedBy = String.valueOf(" You were Served By " + agentName).getBytes("GB2312");
                signature = String.valueOf("Signature__________________________").getBytes("GB2312");
                agentReceipt = String.valueOf("         AGENT COPY         ").getBytes("GB2312");

                arryMotto = "         BASE FOR GROWTH      ".getBytes("GB2312");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            PrinterInterface.begin();

            byte[] cmd;
            cmd = new byte[]{(byte) 0x1B, (byte) 0x40};

            cmd = new byte[]{
                    (byte) 0x1B, (byte) 0x45, (byte) 0};

            write(cmd);
            write(arrySaccoName);
            writeLineBreak(1);
            write(terminalNo);
            writeLineBreak(1);
            write(receiptNo);
            writeLineBreak(1);
            write(date);
            writeLineBreak(1);
            write(arryCustomerName);
            writeLineBreak(1);
            write(transactionTYpe);
            // print line break
            writeLineBreak(2);

            write(arryWithdrawAmt);
            writeLineBreak(1);
            write(arryAmt);
            writeLineBreak(1);
            write(accountNum);
            writeLineBreak(1);
            write(servedBy);
            // print line break
            writeLineBreak(2);
            write(signature);
            writeLineBreak(2);
            write(agentReceipt);
            writeLineBreak(2);
            write(arryMotto);
            writeLineBreak(4);
            PrinterInterface.end();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void write(final byte[] arryData) {
        int result = PrinterInterface.write(arryData, arryData.length);
    }

    // print line break
    private void writeLineBreak(int lineNumber) {
        // write(new byte[]{'\n'});
        write(PrinterCommand.getCmdEscDN(lineNumber));
    }

}
