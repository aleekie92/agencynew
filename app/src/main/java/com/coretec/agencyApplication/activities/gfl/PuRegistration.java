package com.coretec.agencyApplication.activities.gfl;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.support.design.widget.TextInputLayout;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.Toast;

import com.coretec.agencyApplication.R;
import com.coretec.agencyApplication.activities.BaseActivity;
import com.coretec.agencyApplication.api.GFL;
import com.coretec.agencyApplication.api.requests.GrowersDetailsRequest;
import com.coretec.agencyApplication.api.requests.PuRegistrationRequest;
import com.coretec.agencyApplication.api.responses.GrowersDetailsResponse;
import com.coretec.agencyApplication.api.responses.PuRegistrationResponse;
import com.coretec.agencyApplication.model.GrowerDetails;
import com.coretec.agencyApplication.utils.SharedPrefs;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import static com.coretec.agencyApplication.utils.Utils.getFormattedDate;
import static com.coretec.agencyApplication.utils.Utils.getLat;
import static com.coretec.agencyApplication.utils.Utils.getLong;

public class PuRegistration extends BaseActivity {

    ImageView backBtn;
    private TextInputLayout input_layout_id_number;
    private EditText identifier_deposit;
    private EditText reason;
    Button submit;

    private Spinner spinner;
    private ArrayList<String> arrayList = new ArrayList<>();
    private ArrayAdapter<GrowerDetails> adapter;

    private String item;
    ProgressBar progressBar;
    ProgressDialog progressDialog;

    private EditText phoneNumber, fullName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.gfl_pu_registration);
        SharedPrefs.init(getApplicationContext());

        backBtn = findViewById(R.id.backBtn);
        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(PuRegistration.this, MainGFLDashboard.class));
                finish();
            }
        });

        input_layout_id_number = findViewById(R.id.input_layout_id_number);
        identifier_deposit = findViewById(R.id.id_number);
        spinner = findViewById(R.id.growerSpinner);

        progressBar = findViewById(R.id.growersPb);
        phoneNumber = findViewById(R.id.phone_number);
        fullName = findViewById(R.id.full_names);

        submit = findViewById(R.id.btnSubmit);
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (identifier_deposit.getText().toString().matches("") || phoneNumber.getText().toString().matches("") || fullName.getText().toString().matches("")) {
                    Toast.makeText(getBaseContext(), "Please fill in the missing fields to proceed!", Toast.LENGTH_LONG).show();
                }
                else{
                    /* progress dialog */
                    progressDialog = new ProgressDialog(PuRegistration.this);
                    progressDialog.setMessage("Submitting your request...");
                    progressDialog.setCancelable(false);
                    progressDialog.show();

                    submitPuRegistration();
                }
            }
        });

        TextWatcher textWatcherId = new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                //here, after we introduced something in the EditText we get the string from it
                String identifier_number = identifier_deposit.getText().toString();
                if (identifier_deposit.length() >= 7) {
                    requestGetAccountDetails(identifier_number);
                }
                if (identifier_deposit.length() >= 5 && identifier_deposit.length() <= 8) {
                    progressBar.setVisibility(View.VISIBLE);
                    progressBar.getIndeterminateDrawable().setColorFilter(ContextCompat.getColor(PuRegistration.this, R.color.btn_green), PorterDuff.Mode.SRC_IN );
                }
//                Toast.makeText(ShareDeposit.this, accountNumber, Toast.LENGTH_SHORT).show();
//                requestAccountName(accountNumber);

            }
        };

        adapter = new ArrayAdapter<GrowerDetails>(getApplicationContext(),
                R.layout.simple_spinner_dropdown_item, new ArrayList<GrowerDetails>());
        adapter.setDropDownViewResource(R.layout.simple_spinner_dropdown_item);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                item = parent.getItemAtPosition(position).toString();
                String text = spinner.getSelectedItem().toString();

                SharedPrefs.write(SharedPrefs.PU_REGISTRATION_G_NUMBER, text);
                Log.e("GROWER NUMBER", text);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        identifier_deposit.addTextChangedListener(textWatcherId);

    }

    private void submitPuRegistration(){

        final String TAG = "PU REGISTRATION";
        String URL = GFL.MSACCO_AGENT + GFL.PuRegistration;
        String gNumber = SharedPrefs.read(SharedPrefs.PU_REGISTRATION_G_NUMBER, null);

        final PuRegistrationRequest puRegistration = new PuRegistrationRequest();
        puRegistration.idno = identifier_deposit.getText().toString();
        puRegistration.growersno = gNumber;
        puRegistration.phoneno = phoneNumber.getText().toString();
        puRegistration.fullnames = fullName.getText().toString();
        puRegistration.transactiontype = "Pu Registration";
        puRegistration.agentid = SharedPrefs.read(SharedPrefs.AGENT_ID_1, null);
        puRegistration.terminalid = SharedPrefs.read(SharedPrefs.DEVICE_ID, null);
        puRegistration.longitude = getLong(this);
        puRegistration.latitude = getLat(this);
        puRegistration.date = getFormattedDate();
        puRegistration.corporate_no = "CAP016";

        Log.e(TAG, puRegistration.getBody().toString());

        GFL.instance(this).request(URL, puRegistration, new GFL.RequestListener() {
            @Override
            public void onSuccess(String response) {
                /*deposit_progressBar.setVisibility(View.GONE);*/
                progressDialog.dismiss();
                PuRegistrationResponse puRegistrationResponse = GFL.instance(PuRegistration.this)
                        .mGson.fromJson(response, PuRegistrationResponse.class);
                if (puRegistrationResponse.is_successful) {

                    String idno = identifier_deposit.getText().toString();
                    String pNumber = phoneNumber.getText().toString();
                    String names = fullName.getText().toString();

                    try {
                        JSONObject jsonObject = new JSONObject(response);

                        String receiptNo = jsonObject.getString("receiptNo");
                        String success = "Pu Registration successful. Wait for approval to get a starting PIN";
                        AlertDialog.Builder builder1 = new AlertDialog.Builder(PuRegistration.this);
                        builder1.setMessage(success);
                        builder1.setTitle("Success!");
                        builder1.setCancelable(true);

                        builder1.setPositiveButton(
                                "DONE",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();
                                        startActivity(new Intent(PuRegistration.this, MainGFLDashboard.class));
                                        finish();
                                    }
                                });

                        /*builder1.setNegativeButton(
                                "Cancel",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();
                                    }
                                });*/

                        AlertDialog alert11 = builder1.create();
                        alert11.show();
                        Toast.makeText(PuRegistration.this, receiptNo, Toast.LENGTH_SHORT).show();

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                } else {
//                    Toast.makeText(ShareDeposit.this, "Accounts Fetching Failed", Toast.LENGTH_LONG).show();
                }

            }

            @Override
            public void onTokenExpired() {

            }

            @Override
            public void onError(String error) {

            }
        });

    }

    void requestGetAccountDetails(String identifier) {

        final String TAG = "GrowersAccounts";
//        EasyLocationMod easyLocationMod = new EasyLocationMod(CashWithdrawal.this);

        String URL = GFL.MSACCO_AGENT + GFL.GetGrowersDetails;

        final GrowersDetailsRequest growersDetails = new GrowersDetailsRequest();
        growersDetails.agentid = SharedPrefs.read(SharedPrefs.AGENT_ID_1, null);
        growersDetails.corporateno = "CAP016";
        growersDetails.accountidentifier = identifier;
        growersDetails.accountidentifiercode = "1";
        growersDetails.transactiontype = "1";
        growersDetails.terminalid = SharedPrefs.read(SharedPrefs.DEVICE_ID, null);
        growersDetails.longitude = getLong(this);
        growersDetails.latitude = getLat(this);
        growersDetails.date = getFormattedDate();

        Log.e(TAG, growersDetails.getBody().toString());

        GFL.instance(this).request(URL, growersDetails, new GFL.RequestListener() {
            @Override
            public void onSuccess(String response) {
                /*deposit_progressBar.setVisibility(View.GONE);*/

                GrowersDetailsResponse growersDetailsResponse = GFL.instance(PuRegistration.this)
                        .mGson.fromJson(response, GrowersDetailsResponse.class);
                if (growersDetailsResponse.is_successful) {
                    List<GrowerDetails> growerDetailsList = new ArrayList<>();

                    try {
                        JSONObject jsonObject = new JSONObject(response);

                        JSONArray jsonArray = jsonObject.getJSONArray("Growers");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                            JSONArray jsonArray1 = jsonObject1.getJSONArray("GrowerDetails");
                            for (int l = 0; l < jsonArray1.length(); l++) {
                                JSONObject jsonObject2 = jsonArray1.getJSONObject(l);
                                progressBar.setVisibility(View.GONE);
                                arrayList.add(String.valueOf(jsonObject2.getString("GrowerNo")));
                                /*mpesaPhoneNumber = jsonObject2.getString("phonenumber");*/
                                String gNo = jsonObject2.getString("GrowerNo");
                                String pNumber = jsonObject2.getString("GrowerMobileNo");
                                String gName = jsonObject2.getString("GrowerName");
                                //SharedPrefs.write(SharedPrefs.PHONE_NUMBER, pNumber);

                                fullName.setText(gName);
                                phoneNumber.setText(pNumber);
                            }

                        }

                        //Growers Number
                        spinner = (Spinner) findViewById(R.id.growerSpinner);
                        ArrayAdapter<String> adapter = new ArrayAdapter<String>(PuRegistration.this, android.R.layout.simple_spinner_item, arrayList);
                        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                        spinner.setAdapter(adapter);


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                } else {
//                    Toast.makeText(ShareDeposit.this, "Accounts Fetching Failed", Toast.LENGTH_LONG).show();
                }

            }

            @Override
            public void onTokenExpired() {

            }

            @Override
            public void onError(String error) {

            }
        });
    }

}
