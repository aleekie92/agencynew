package com.coretec.agencyApplication.activities.gfl;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.coretec.agencyApplication.R;
import com.coretec.agencyApplication.activities.BaseActivity;


public class UploadID extends BaseActivity {

    private CardView card_identity_photo;
    private static final int CAMERA_REQUEST = 1888;
    private ImageView identity_imageView;
    private Button submitIdentityPhoto;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.gfl_upload_id);

        this.overridePendingTransition(R.anim.move_right_in_activity, R.anim.move_left_out_activity);

        identity_imageView = findViewById(R.id.imageview_identity);

        card_identity_photo = findViewById(R.id.card_identity_photo);
        card_identity_photo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(cameraIntent, CAMERA_REQUEST);
            }
        });

        submitIdentityPhoto = findViewById(R.id.submitIdentityPhoto);
        submitIdentityPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                //startActivity(new Intent(UploadID.this, FingerPrint.class));
                /*startActivity(new Intent(UploadID.this, Signature.class));*/
            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CAMERA_REQUEST) {
            Bitmap photo = (Bitmap) data.getExtras().get("data");
            identity_imageView.setImageBitmap(photo);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.move_left_in_activity, R.anim.move_right_out_activity);
    }
}
