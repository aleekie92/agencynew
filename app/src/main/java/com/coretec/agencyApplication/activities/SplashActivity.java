package com.coretec.agencyApplication.activities;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.coretec.agencyApplication.R;
import com.coretec.agencyApplication.api.Api;
import com.coretec.agencyApplication.api.Server;
import com.coretec.agencyApplication.api.requests.GenerateTokenRequest;
import com.coretec.agencyApplication.api.responses.GenerateTokenResponse;
import com.coretec.agencyApplication.utils.Const;
import com.coretec.agencyApplication.utils.PreferenceFileKeys;
import com.coretec.agencyApplication.utils.SharedPrefs;
import com.orhanobut.logger.Logger;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.UUID;
public class SplashActivity extends AppCompatActivity implements Server.GenerateTokenListener {
    private View mContentView;
    static Context ctx;
    SharedPreferences mSharedPreferences;
    NetworkInfo wifiInfo;
    NetworkInfo mobInfo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        SharedPrefs.init(getApplicationContext());
        switch (Api.MSACCO_AGENT) {
            case "http://172.16.11.145:3052/api/AgencyBanking/":
                setContentView(R.layout.activity_splash_gfl);//gfl live
                break;
            case "http://172.16.11.145:3051/api/AgencyBanking/":
                setContentView(R.layout.activity_splash_gfl);//gfl test
                break;

            case "http://172.18.6.8:35088/api/AgencyBanking/":

                String coopnumber = SharedPrefs.read(SharedPrefs.VR_COOPORATE_NO, null);
                if (coopnumber !=null){
                    switch (SharedPrefs.read(SharedPrefs.VR_COOPORATE_NO, null)) {

                        case "CAP022":
                            setContentView(R.layout.activity_splash_coopmis);
                            break;

                        case "CAP027":
                            setContentView(R.layout.activity_login_coopmis);
                            break;

                        case "CAP028":
                            setContentView(R.layout.activity_login_coopmis);
                            break;

                        default:
                            setContentView(R.layout.activity_splash);
                            break;
                    }

                }else {
                    setContentView(R.layout.activity_splash);
                }
                break;

            default:
                setContentView(R.layout.activity_splash);
                break;
        }

        getNetworkStatus();
        this.overridePendingTransition(R.anim.move_right_in_activity, R.anim.move_left_out_activity);
        mSharedPreferences = getSharedPreferences(PreferenceFileKeys.PREFS_FILE, Context.MODE_PRIVATE);

        final TelephonyManager tm = (TelephonyManager) getBaseContext().getSystemService(Context.TELEPHONY_SERVICE);
        String serialnum = null;
        try {
            Class<?> c = Class.forName("android.os.SystemProperties");
            Method get = c.getMethod("get", String.class, String.class);
            serialnum = (String) (get.invoke(c, "ro.serialno", "unknown"));
            SharedPrefs.write(SharedPrefs.DEVICE_ID, serialnum);
            Log.e("SERIAL NO", serialnum);
        } catch (Exception ignored) {

        }

        final String tmDevice, tmSerial, androidId;

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(SplashActivity.this,
                    new String[]{Manifest.permission.READ_PHONE_STATE},
                    1);
            return;
        }
        tmDevice = "" + tm.getDeviceId();
        tmSerial = "" + tm.getSimSerialNumber();
        androidId = "" + Settings.Secure.getString(getContentResolver(), Settings.Secure.ANDROID_ID);

        UUID deviceUuid = new UUID(androidId.hashCode(), ((long) tmDevice.hashCode() << 32) | tmSerial.hashCode());
        String deviceId = deviceUuid.toString();
        genarateToken();
        mContentView = findViewById(R.id.fullscreen_content);
        hide();
        mHidePart2Runnable.run();
        fingerprintmodule();
    }

    private void genarateToken(){
        switch (Api.MSACCO_AGENT) {
            case "http://172.16.11.145:3052/api/AgencyBanking/":
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        generateToken();
                    }
                }, 20000);
                break;
            case "http://172.16.11.145:3051/api/AgencyBanking/":
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        generateToken();
                    }
                }, 20000);
                break;

            default:
                Server.generateToken(SplashActivity.this);
        }
    }

    private void fingerprintmodule() {
        String prop = getProperty("wp.fingerprint.model", "");
        if (prop.equalsIgnoreCase("tuzhengbig")) {
            SharedPrefs.fingerprintmodule("1", SplashActivity.this);
        } else {
            SharedPrefs.fingerprintmodule("0", SplashActivity.this);
            //Toast.makeText(SplashActivity.this, "Cross match", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case 1: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                } else {
                    Toast.makeText(SplashActivity.this, "Permission denied to read your External storage", Toast.LENGTH_SHORT).show();
                }
                return;
            }
        }
    }

    public static String getProperty(String key, String defaultValue) {
        String value = defaultValue;
        try {
            Class<?> c = Class.forName("android.os.SystemProperties");
            Method get = c.getMethod("get", String.class, String.class);
            value = (String) (get.invoke(c, key, defaultValue));
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            return value;
        }
    }

    private void hide() {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.hide();
        }
    }

    private final Runnable mHidePart2Runnable = new Runnable() {
        @SuppressLint("InlinedApi")
        @Override
        public void run() {
            mContentView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LOW_PROFILE
                    | View.SYSTEM_UI_FLAG_FULLSCREEN
                    | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                    | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                    | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                    | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
        }
    };

    @Override
    public void onTokenGenerated(GenerateTokenResponse generateTokenResponse) {
        Const.getInstance().setLoginToken(generateTokenResponse.token);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                startActivity(intent);
                finish();

            }
        }, 3000);
    }

    @Override
    public void onError(String error) {
        ConnectivityManager conManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo[] networkInfo = conManager.getAllNetworkInfo();
        for (NetworkInfo netInfo : networkInfo) {

            switch (Api.MSACCO_AGENT) {
                case "http://172.16.11.145:3052/api/AgencyBanking/":

                    break;
                case "http://172.16.11.145:3051/api/AgencyBanking/":

                    break;

                case "http://172.18.6.8:35088/api/AgencyBanking/":
                    if (netInfo.getTypeName().equalsIgnoreCase("WIFI")) {
                        if (netInfo.isConnected()) {
                            String message = "Connect to Cellular Network to continue";
                            android.app.AlertDialog.Builder builder1 = new android.app.AlertDialog.Builder(SplashActivity.this);
                            builder1.setMessage(message);
                            builder1.setTitle("YAAAAK!");
                            builder1.setCancelable(false);
                            builder1.setCancelable(true);

                            builder1.setPositiveButton(
                                    "Ok",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            dialog.dismiss();
                                        }
                                    });

                            android.app.AlertDialog alert11 = builder1.create();
                            alert11.show();
                        }
                    } else if (netInfo.getTypeName().equalsIgnoreCase("MOBILE")) {
                        if (netInfo.isConnected()) {
                            String message = error;
                            android.app.AlertDialog.Builder builder1 = new android.app.AlertDialog.Builder(SplashActivity.this);
                            builder1.setMessage(message);
                            builder1.setTitle("Failed!");
                            builder1.setCancelable(true);

                            builder1.setPositiveButton(
                                    "Okay",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            //finish();
                                            dialog.dismiss();
                                        }
                                    });

                            android.app.AlertDialog alert11 = builder1.create();
                            alert11.show();
                        }
                    }
                    break;
                default:
            }
        }
    }

    @Override
    public void onBackPressed() {
        Toast.makeText(this, "you may need to relaunch the application", Toast.LENGTH_SHORT).show();
    }

    public void getNetworkStatus(){
        ConnectivityManager conManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo[] networkInfo = conManager.getAllNetworkInfo();
        for (NetworkInfo netInfo : networkInfo) {
            switch (Api.MSACCO_AGENT) {
                case "http://172.16.11.145:3052/api/AgencyBanking/":
                    break;
                case "http://172.16.11.145:3051/api/AgencyBanking/":

                    break;

                case "http://172.18.6.8:35088/api/AgencyBanking/":
                    if (netInfo.getTypeName().equalsIgnoreCase("WIFI")) {
                        if (netInfo.isConnected()) {
                            String message = "Connect to Cellular Network to continue";
                            android.app.AlertDialog.Builder builder1 = new android.app.AlertDialog.Builder(SplashActivity.this);
                            builder1.setMessage(message);
                            builder1.setTitle("YAAAAK!");
                            builder1.setCancelable(false);
                            builder1.setCancelable(true);

                            builder1.setPositiveButton(
                                    "Ok",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            dialog.dismiss();
                                        }
                                    });

                            android.app.AlertDialog alert11 = builder1.create();
                            alert11.show();
                        }
                    }
                    break;

                    default:
                    break;
            }
        }
    }

    public  void generateToken() {
        final String TAG = "token generated";
        final Context ctx = null;
        GenerateTokenRequest generateToken = new GenerateTokenRequest();
        String URL = Api.MSACCO_AGENT + Api.GenerateToken;
        Api.instance(getApplicationContext()).request(URL, generateToken, new Api.RequestListener() {
            @Override
            public void onSuccess(String response) {
                Logger.json(response);
                GenerateTokenResponse generateTokenResponse = Api.instance(getApplicationContext()).mGson.fromJson(response, GenerateTokenResponse.class);
                if (generateTokenResponse.is_successful) {
                    Const.getInstance().setLoginToken(generateTokenResponse.token);
                    onTokenGenerated(generateTokenResponse);
                } else {
                    onError("Wrong credentials");
                }
            }

            @Override
            public void onTokenExpired() {

            }

            @Override
            public void onError(String error) {

            }
        });
    }

}
