package com.coretec.agencyApplication.activities.gfl;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.support.design.widget.TextInputLayout;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;

import com.coretec.agencyApplication.R;
import com.coretec.agencyApplication.activities.BaseActivity;
import com.coretec.agencyApplication.activities.newmodules.ValidateTunzhengbigBio;
import com.coretec.agencyApplication.api.GFL;
import com.coretec.agencyApplication.api.requests.FingerPrintRequest;
import com.coretec.agencyApplication.api.requests.GetGrowersPaybillTransactionsRequest;
import com.coretec.agencyApplication.api.requests.GetLoansInfoRequest;
import com.coretec.agencyApplication.api.requests.MpesaPaybillRequest;
import com.coretec.agencyApplication.api.responses.FingerPrintResponse;
import com.coretec.agencyApplication.api.responses.GetGrowersPaybillTransactionsResponse;
import com.coretec.agencyApplication.api.responses.GetLoansInfoResponse;
import com.coretec.agencyApplication.api.responses.MpesaPaybillResponse;
import com.coretec.agencyApplication.model.GrowerInfo;
import com.coretec.agencyApplication.model.LoanDetails;
import com.coretec.agencyApplication.utils.QuickToast;
import com.coretec.agencyApplication.utils.SharedPrefs;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import static com.coretec.agencyApplication.utils.Utils.getFormattedDate;
import static com.coretec.agencyApplication.utils.Utils.getLat;
import static com.coretec.agencyApplication.utils.Utils.getLong;

public class PaybillCorrection extends AppCompatActivity {

    private QuickToast toast = new QuickToast(this);
    private TextInputLayout input_layout_mpesa;
    private EditText mpesa_receipt, growers_number;
    private TextView mpesaStatus, descriptionTv;
    ProgressBar progressBar, loanBar;
    ImageView backBtn;
    Button submit;
    ProgressDialog progressDialog;

    private Spinner spinner;
    private ArrayList<String> arrayList = new ArrayList<>();
    private ArrayAdapter<LoanDetails> adapter;

    private String item;
    String identifier_number;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.gfl_paybill_correction);
        SharedPrefs.init(getApplicationContext());

        this.overridePendingTransition(R.anim.move_right_in_activity, R.anim.move_left_out_activity);
        SharedPrefs.write(SharedPrefs.FP_BOOLEAN, "");

        Toolbar toolbar = findViewById(R.id.toolbar);
        TextView mTitle = toolbar.findViewById(R.id.toolbar_title);
        mTitle.setText(getResources().getText(R.string.paybillcorrection));
        setSupportActionBar(toolbar);
        ActionBar actionbar = getSupportActionBar();
        actionbar.setDisplayHomeAsUpEnabled(true);
        actionbar.setHomeAsUpIndicator(R.drawable.ic_arrow_back);

        input_layout_mpesa = findViewById(R.id.input_layout_mpesa);
        mpesa_receipt = findViewById(R.id.mpesa_receipt);
        mpesaStatus = findViewById(R.id.mpesaStatus);
        descriptionTv = findViewById(R.id.descriptionStatus);
        progressBar = findViewById(R.id.statusPb);

        submit = findViewById(R.id.btnSubmit);
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mpesa_receipt.getText().toString().matches("") || growers_number.getText().toString().matches("")
                   || spinner==null) {
                    toast.swarn("Please fill in the missing fields to proceed!");
                }
                else{
                    /* progress dialog */
                    progressDialog = new ProgressDialog(PaybillCorrection.this);
                    progressDialog.setMessage("Please wait...");
                    progressDialog.setCancelable(false);
                    progressDialog.show();
                    submitPaybillCorrection();
                   // requestFingerPrints();
                }
            }
        });

        loanBar = findViewById(R.id.loanPb);
        growers_number = findViewById(R.id.growers_number);
        spinner = findViewById(R.id.productType);

        TextWatcher textWatcherId = new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                //here, after we introduced something in the EditText we get the string from it
                 identifier_number = mpesa_receipt.getText().toString();
                if (mpesa_receipt.length() >= 9) {
                    requestMpesaReceiptDetails(identifier_number);
                }
                if (mpesa_receipt.length() <= 5) {
                    mpesaStatus.setText(null);
                    descriptionTv.setText(null);
                    progressBar.setVisibility(View.GONE);
                }
                if (mpesa_receipt.length() >= 8 && mpesa_receipt.length() <= 10) {
                    mpesaStatus.setText(null);
                    descriptionTv.setText(null);
                    progressBar.setVisibility(View.VISIBLE);
                    progressBar.getIndeterminateDrawable().setColorFilter(ContextCompat.getColor(PaybillCorrection.this, R.color.btn_green), PorterDuff.Mode.SRC_IN );
                }
//                Toast.makeText(ShareDeposit.this, accountNumber, Toast.LENGTH_SHORT).show();
//                requestAccountName(accountNumber);

            }
        };
        mpesa_receipt.addTextChangedListener(textWatcherId);

        TextWatcher textWatcherLoan = new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                //here, after we introduced something in the EditText we get the string from it
                String loan_number = growers_number.getText().toString();
                if (growers_number.length() >= 7) {
                    requestLoanProducts(loan_number);
                }
                if (growers_number.length() <= 5) {
                    loanBar.setVisibility(View.GONE);
                    arrayList.clear();
                    spinner = findViewById(R.id.productType);
                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(PaybillCorrection.this, android.R.layout.simple_spinner_item, arrayList);
                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    spinner.setAdapter(null);
                }
                if (growers_number.length() >= 5 && growers_number.length() <= 8) {
                    arrayList.clear();
                    spinner = findViewById(R.id.productType);
                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(PaybillCorrection.this, android.R.layout.simple_spinner_item, arrayList);
                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    spinner.setAdapter(null);
                    loanBar.setVisibility(View.VISIBLE);
                    loanBar.getIndeterminateDrawable().setColorFilter(ContextCompat.getColor(PaybillCorrection.this, R.color.btn_green), PorterDuff.Mode.SRC_IN );
                }
//                Toast.makeText(ShareDeposit.this, accountNumber, Toast.LENGTH_SHORT).show();
//                requestAccountName(accountNumber);

            }
        };
        growers_number.addTextChangedListener(textWatcherLoan);

        adapter = new ArrayAdapter<LoanDetails>(getApplicationContext(),
                R.layout.simple_spinner_dropdown_item, new ArrayList<LoanDetails>());
        //adapter.setDropDownViewResource(R.layout.simple_spinner_dropdown_item);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                item = parent.getItemAtPosition(position).toString();
                String text = spinner.getSelectedItem().toString();

                String code = text.substring(0,text.indexOf("-"));
                SharedPrefs.write(SharedPrefs.LOAN_CODE, code);
                Log.e("STRING", code);
                /*if (item != null) {
                    Toast.makeText(parent.getContext(), "Selected" +text, Toast.LENGTH_SHORT).show();

                }*/
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

    }

    void requestFingerPrints() {

        final String TAG = "GET FINGERPRINTS";
        String URL = GFL.MSACCO_AGENT + GFL.GetFourFingerPrint;

        final FingerPrintRequest fingerPrintRequest = new FingerPrintRequest();
        fingerPrintRequest.accountidentifier = growers_number.getText().toString();
        fingerPrintRequest.accountidentifiercode = "2";
        fingerPrintRequest.agentid = SharedPrefs.read(SharedPrefs.AGENT_ID_1, null);
        fingerPrintRequest.terminalid = SharedPrefs.read(SharedPrefs.DEVICE_ID, null);
        fingerPrintRequest.longitude = getLong(this);
        fingerPrintRequest.latitude = getLat(this);
        fingerPrintRequest.date = getFormattedDate();

        Log.e(TAG, fingerPrintRequest.getBody().toString());

        GFL.instance(this).request(URL, fingerPrintRequest, new GFL.RequestListener() {
            @Override
            public void onSuccess(String response) {
                /*deposit_progressBar.setVisibility(View.GONE);*/
                //progressDialog.dismiss();
                FingerPrintResponse fingerPrintResponse = GFL.instance(PaybillCorrection.this)
                        .mGson.fromJson(response, FingerPrintResponse.class);
                if (fingerPrintResponse.is_successful) {

                    startActivity(new Intent(PaybillCorrection.this, ValidateTunzhengbigBio.class));

                    try {
                        JSONObject jsonObject = new JSONObject(response);

                        String leftRing = jsonObject.getString("leftring");
                        String leftLittle = jsonObject.getString("leftlittle");
                        String rightRing = jsonObject.getString("rightring");
                        String rightLittle = jsonObject.getString("rightlittle");

                        SharedPrefs.write(SharedPrefs.LEFT_RING, leftRing);
                        SharedPrefs.write(SharedPrefs.LEFT_LITTLE, leftLittle);
                        SharedPrefs.write(SharedPrefs.RIGHT_RING, rightRing);
                        SharedPrefs.write(SharedPrefs.RIGHT_LITTLE, rightLittle);

                        Log.d("LEFT_RING", leftRing);
                        Log.d("LEFT_LITTLE", leftLittle);
                        Log.d("RIGHT_RING", rightRing);
                        Log.d("RIGHT_LITTLE", rightLittle);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                } else {
                    progressDialog.dismiss();
                    toast.swarn("Could not get customer finger prints!");
                }

            }

            @Override
            public void onTokenExpired() {

            }

            @Override
            public void onError(String error) {

            }
        });

    }

    private void submitPaybillCorrection(){

        final String TAG = "MPESA CHANGE";
        String URL = GFL.MSACCO_AGENT + GFL.MpesaPaybill;
        String loanCode = SharedPrefs.read(SharedPrefs.LOAN_CODE, null);
        String tel = SharedPrefs.read(SharedPrefs.PBC_PHONE, null);
        String amt = SharedPrefs.read(SharedPrefs.PBC_AMOUNT, null);
        String accountNo = SharedPrefs.read(SharedPrefs.REG_ACCOUNT_NO, null);

        final MpesaPaybillRequest mpesaPaybill = new MpesaPaybillRequest();
        mpesaPaybill.mpesareceipt = identifier_number;
        mpesaPaybill.growersno = growers_number.getText().toString();
        mpesaPaybill.loanproduccode = loanCode;
        mpesaPaybill.phoneno = tel;
        mpesaPaybill.amount = amt;
        mpesaPaybill.description = "";
        mpesaPaybill.needschange = true;
        mpesaPaybill.message = "";
        mpesaPaybill.transactiontype = "Paybill Correction";
        mpesaPaybill.agentid = SharedPrefs.read(SharedPrefs.AGENT_ID_1, null);
        mpesaPaybill.terminalid = SharedPrefs.read(SharedPrefs.DEVICE_ID, null);
        mpesaPaybill.longitude = getLong(this);
        mpesaPaybill.latitude = getLat(this);
        mpesaPaybill.date = getFormattedDate();
        mpesaPaybill.corporate_no = "CAP016";
        mpesaPaybill.accNo = accountNo;


        GFL.instance(this).request(URL, mpesaPaybill, new GFL.RequestListener() {
            @Override
            public void onSuccess(String response) {
                /*deposit_progressBar.setVisibility(View.GONE);*/
               // progressDialog.dismiss();
                MpesaPaybillResponse mpesaPaybillResponse = GFL.instance(PaybillCorrection.this)
                        .mGson.fromJson(response, MpesaPaybillResponse.class);
                if (mpesaPaybillResponse.is_successful) {
                    progressDialog.dismiss();
                    try {
                        JSONObject jsonObject = new JSONObject(response);

                        String receiptNo = jsonObject.getString("receiptNo");
                        Log.d("rrrrr",identifier_number);

                        String success = "Paybill correction submitted successfully...";
                        AlertDialog.Builder builder1 = new AlertDialog.Builder(PaybillCorrection.this);
                        builder1.setMessage(success);
                        builder1.setTitle("Success!");
                        builder1.setCancelable(true);

                        builder1.setPositiveButton(
                                "DONE",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        Intent intent = new Intent(getApplicationContext(), MainGFLDashboard.class);
                                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                        startActivity(intent);
                                    }
                                });
                        AlertDialog alert11 = builder1.create();
                        alert11.show();

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                } else {
//                    Toast.makeText(ShareDeposit.this, "Accounts Fetching Failed", Toast.LENGTH_LONG).show();
                }

            }

            @Override
            public void onTokenExpired() {

            }

            @Override
            public void onError(String error) {

            }
        });

    }

    void requestLoanProducts(String identifier){
        final String TAG = "LoanProducts";

        String URL = GFL.MSACCO_AGENT + GFL.GetLoansInfo;

        final GetLoansInfoRequest loansInfo = new GetLoansInfoRequest();
        loansInfo.corporateno = "CAP016";
        loansInfo.growersno = identifier;
        loansInfo.transactiontype = "1";
        loansInfo.agentid = SharedPrefs.read(SharedPrefs.AGENT_ID_1, null);
        loansInfo.terminalid = SharedPrefs.read(SharedPrefs.DEVICE_ID, null);
        loansInfo.longitude = getLong(this);
        loansInfo.latitude = getLat(this);
        loansInfo.date = getFormattedDate();

        Log.e(TAG, loansInfo.getBody().toString());

        arrayList.clear();
        spinner = findViewById(R.id.productType);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(PaybillCorrection.this, android.R.layout.simple_spinner_item, arrayList);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(null);

        GFL.instance(this).request(URL, loansInfo, new GFL.RequestListener() {
            @Override
            public void onSuccess(String response) {
                /*deposit_progressBar.setVisibility(View.GONE);*/

                GetLoansInfoResponse loansInfoResponse = GFL.instance(PaybillCorrection.this)
                        .mGson.fromJson(response, GetLoansInfoResponse.class);
                if (loansInfoResponse.is_successful) {
                    List<LoanDetails> loanDetailsList = new ArrayList<>();

                    try {
                        JSONObject jsonObject = new JSONObject(response);

                        JSONArray jsonArray = jsonObject.getJSONArray("GetLoansInfo");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                            JSONArray jsonArray1 = jsonObject1.getJSONArray("LoanDetails");
                            for (int l = 0; l < jsonArray1.length(); l++) {
                                JSONObject jsonObject2 = jsonArray1.getJSONObject(l);
                                loanBar.setVisibility(View.GONE);
                                String loanType = jsonObject2.getString("ProductType");
                                String loanNo = jsonObject2.getString("LoanNo");
                                String code = jsonObject2.getString("LoansPayKey");
                                String dropDown = code + "-" + loanType + "-" + loanNo;

                                arrayList.add(String.valueOf(dropDown));
                                //SharedPrefs.write(SharedPrefs.PHONE_NUMBER, pNumber);

                                Log.e("LOAN TYPE", loanType);
                                Log.e("LOAN NUMBER:", loanNo);
                            }

                        }

                        //Growers Number
                        spinner = findViewById(R.id.productType);
                        ArrayAdapter<String> adapter = new ArrayAdapter<String>(PaybillCorrection.this, android.R.layout.simple_spinner_item, arrayList);
                        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                        spinner.setAdapter(adapter);


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                } else {
//                    Toast.makeText(ShareDeposit.this, "Accounts Fetching Failed", Toast.LENGTH_LONG).show();
                }

            }

            @Override
            public void onTokenExpired() {

            }

            @Override
            public void onError(String error) {

            }
        });
    }

    void requestMpesaReceiptDetails(String identifier){
        final String TAG = "MpesaReceipts";

        String URL = GFL.MSACCO_AGENT + GFL.GetGrowersPaybillTransactions;

        final GetGrowersPaybillTransactionsRequest paybillTransaction = new GetGrowersPaybillTransactionsRequest();
        paybillTransaction.corporateno = "CAP016";
        paybillTransaction.accountidentifier = identifier;
        paybillTransaction.accountidentifiercode = "4";
        paybillTransaction.transactiontype = "1";
        paybillTransaction.agentid = SharedPrefs.read(SharedPrefs.AGENT_ID_1, null);
        paybillTransaction.terminalid = SharedPrefs.read(SharedPrefs.DEVICE_ID, null);
        paybillTransaction.longitude = getLong(this);
        paybillTransaction.latitude = getLat(this);
        paybillTransaction.date = getFormattedDate();

        Log.e(TAG, paybillTransaction.getBody().toString());

        GFL.instance(this).request(URL, paybillTransaction, new GFL.RequestListener() {
            @Override
            public void onSuccess(String response) {
                /*deposit_progressBar.setVisibility(View.GONE);*/

                GetGrowersPaybillTransactionsResponse paybillTransactionResponse = GFL.instance(PaybillCorrection.this)
                        .mGson.fromJson(response, GetGrowersPaybillTransactionsResponse.class);
                if (paybillTransactionResponse.is_successful) {
                    List<GrowerInfo> growerInfoList = new ArrayList<>();
                    //progressBar.setVisibility(View.GONE);
                    try {
                        JSONObject jsonObject = new JSONObject(response);

                        JSONArray jsonArray = jsonObject.getJSONArray("Growers");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                            JSONArray jsonArray1 = jsonObject1.getJSONArray("GrowerDetails");
                            for (int l = 0; l < jsonArray1.length(); l++) {
                                JSONObject jsonObject2 = jsonArray1.getJSONObject(l);
                                progressBar.setVisibility(View.GONE);
                                //arrayList.add(String.valueOf(jsonObject2.getString("GrowerNo")));
                                /*mpesaPhoneNumber = jsonObject2.getString("phonenumber");*/
                                String description = jsonObject2.getString("Description");
                                String posted = jsonObject2.getString("Posted");
                                String tel = jsonObject2.getString("Telephone");
                                String amount = jsonObject2.getString("Amount");
                                //SharedPrefs.write(SharedPrefs.PHONE_NUMBER, pNumber);

                                SharedPrefs.write(SharedPrefs.PBC_PHONE, tel);
                                SharedPrefs.write(SharedPrefs.PBC_AMOUNT, amount);

                                mpesaStatus.setText(posted);
                                descriptionTv.setText(description);

                                Log.e("POSTED", posted);
                                Log.e("DESCRIPTION", description);
                                Log.e("TELEPHONE", tel);
                                Log.e("AMOUNT", amount);
                            }

                        }


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                } else {
//                    Toast.makeText(ShareDeposit.this, "Accounts Fetching Failed", Toast.LENGTH_LONG).show();
                }

            }

            @Override
            public void onTokenExpired() {

            }

            @Override
            public void onError(String error) {

            }
        });
    }

    @Override
    protected void onResume() {
       // SharedPrefs.init(getApplicationContext());
        super.onResume();
       /* String fpBoolean = SharedPrefs.read(SharedPrefs.FP_BOOLEAN, null);

        if (fpBoolean.equals("true")) {
            progressDialog.dismiss();
            submitPaybillCorrection();
        } else if (fpBoolean.equals("false")){
            progressDialog.dismiss();
            toast.swarn("Please authenticate to complete transaction!");
        }*/

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                finish();
                //Toast.makeText(Account.this, "Back has been presses!", Toast.LENGTH_LONG).show();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.move_left_in_activity, R.anim.move_right_out_activity);
    }

}
