package com.coretec.agencyApplication.activities.gfl;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.graphics.PorterDuff;
import android.graphics.Rect;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.CardView;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.DecelerateInterpolator;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.coretec.agencyApplication.R;
import com.coretec.agencyApplication.activities.BaseActivity;
import com.coretec.agencyApplication.api.Api;
import com.coretec.agencyApplication.api.GFL;
import com.coretec.agencyApplication.api.requests.ChangePin;
import com.coretec.agencyApplication.api.requests.GetAvailableKgsGuarantorsRequest;
import com.coretec.agencyApplication.api.requests.GetLoansInfoRequest;
import com.coretec.agencyApplication.api.requests.GrowersAccountRequest;
import com.coretec.agencyApplication.api.requests.GrowersDetailsRequest;
import com.coretec.agencyApplication.api.responses.GetAvailableKgsGuarantorsResponse;
import com.coretec.agencyApplication.api.responses.GetLoansInfoResponse;
import com.coretec.agencyApplication.api.responses.GrowersAccountResponse;
import com.coretec.agencyApplication.api.responses.GrowersDetailsResponse;
import com.coretec.agencyApplication.api.responses.PinChangeResponse;
import com.coretec.agencyApplication.model.GrowerDetails;
import com.coretec.agencyApplication.model.LoanDetails;
import com.coretec.agencyApplication.utils.LogOutTimerUtil;
import com.coretec.agencyApplication.utils.PreferenceFileKeys;
import com.coretec.agencyApplication.utils.SharedPrefs;
import com.coretec.agencyApplication.utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.List;

import static com.coretec.agencyApplication.utils.Utils.getFormattedDate;
import static com.coretec.agencyApplication.utils.Utils.getLat;
import static com.coretec.agencyApplication.utils.Utils.getLong;

public class GflMemberRegistry extends BaseActivity implements  LogOutTimerUtil.LogOutListener{
    private CardView card_member_registration, cardmemberupdating, cardviewDetails, cardagentbio,cardchangecustomerpin;
    private LinearLayout layoutview, layoutregistration, layoutupdating, layoutdetails,
            layoutidentifier, layoutagentbio,layoutagentchangepin;
    private Button btnconfirm, search;
    private ImageView picture;
    private TextView customername, isdefaulter, currentkgs, previouskgs, availablekgs, totalloan, loanstatus, loanbalance,selfguaranteekgs;
    private ProgressBar details_progressbar, loanPb;
    private EditText id_number;
    private Animator currentAnimator;
    private int shortAnimationDuration;
    String photo;

    private Spinner growerSpinner, spinnerLoan;
    private ArrayList<String> arrayList = new ArrayList<>();
    private ArrayAdapter<GrowerDetails> adapter;
    String item, text;

    private ArrayList<String> arrayListLoan = new ArrayList<>();
    private ArrayAdapter<LoanDetails> adapterLoan;
    private EditText input_agent_phone_number;
    private EditText input_agent_password;

    ProgressDialog progressDialog;
    AlertDialog changePasswordDialog;
    SharedPreferences sharedPref;
    private ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_gfl_member_registry);
        sharedPref = getSharedPreferences(PreferenceFileKeys.PREFS_FILE, Context.MODE_PRIVATE);
        initializevalues();
    }

    private void initializevalues() {
        card_member_registration = findViewById(R.id.card_member_registration);
        cardmemberupdating = findViewById(R.id.cardmemberupdating);
        cardviewDetails = findViewById(R.id.cardviewDetails);
        cardagentbio = findViewById(R.id.cardagentbio);
        cardchangecustomerpin = findViewById(R.id.cardchangecustomerpin);

        layoutview = findViewById(R.id.layoutview);
        layoutregistration = findViewById(R.id.layoutregistration);
        layoutupdating = findViewById(R.id.layoutupdating);
        layoutdetails = findViewById(R.id.layoutdetails);
        layoutidentifier = findViewById(R.id.layoutidentifier);
        layoutagentbio = findViewById(R.id.layoutagentbio);
        layoutagentchangepin = findViewById(R.id.layoutagentchangepin);

        btnconfirm = findViewById(R.id.btnconfirm);
        search = findViewById(R.id.search);
        picture = findViewById(R.id.picture_imageview);
        customername = findViewById(R.id.customername);

        isdefaulter = findViewById(R.id.isdefaulter);
        currentkgs = findViewById(R.id.currentkgs);
        previouskgs = findViewById(R.id.previouskgs);
        availablekgs = findViewById(R.id.availablekgs);
        selfguaranteekgs = findViewById(R.id.selfguaranteekgs);
        totalloan = findViewById(R.id.totalloan);
        loanstatus = findViewById(R.id.loanstatus);
        loanbalance = findViewById(R.id.loanbalance);

        details_progressbar = findViewById(R.id.details_progressbar);
        loanPb = findViewById(R.id.loanPb);

        id_number = findViewById(R.id.id_number);
        growerSpinner = findViewById(R.id.growerSpinner);
        spinnerLoan = findViewById(R.id.spinnerLoan);
        progressBar = findViewById(R.id.progressBar1);
        input_agent_phone_number = findViewById(R.id.input_agent_phone_number);
        input_agent_password = findViewById(R.id.input_agent_password);



        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        Bitmap bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.user);
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] imageBytes = baos.toByteArray();
        String imageString = Base64.encodeToString(imageBytes, Base64.DEFAULT);
        imageBytes = Base64.decode(imageString, Base64.DEFAULT);
        final Bitmap decodedImage = BitmapFactory.decodeByteArray(imageBytes, 0, imageBytes.length);
        picture.setImageBitmap(decodedImage);

        layoutagentchangepin.setVisibility(View.VISIBLE);
        if (SharedPrefs.read(SharedPrefs.MENU_SEPERATOR, null).equals("admin")) {
            layoutagentbio.setVisibility(View.VISIBLE);
            layoutview.setVisibility(View.VISIBLE);
            layoutregistration.setVisibility(View.VISIBLE);
            layoutupdating.setVisibility(View.VISIBLE);
        }else if (SharedPrefs.read(SharedPrefs.MENU_SEPERATOR, null).equals("dsr")){
            layoutregistration.setVisibility(View.VISIBLE);
        }else if(SharedPrefs.read(SharedPrefs.MENU_SEPERATOR, null).equals("commissioned")){
            layoutregistration.setVisibility(View.VISIBLE);
        }else {
            layoutview.setVisibility(View.VISIBLE);
            layoutregistration.setVisibility(View.VISIBLE);
            layoutupdating.setVisibility(View.VISIBLE);
        }

        card_member_registration.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(GflMemberRegistry.this, CustomerRegistration.class));
            }
        });

        cardmemberupdating.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(GflMemberRegistry.this, CustomerDetailsUpdatingActivity.class));
            }
        });

        cardagentbio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(GflMemberRegistry.this, AgentFingerprintParentActivity.class));
            }
        });
        cardchangecustomerpin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createDialogChangePassword();
            }
        });
        cardviewDetails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                layoutregistration.setVisibility(View.GONE);
                layoutupdating.setVisibility(View.GONE);
                layoutview.setVisibility(View.GONE);
                layoutupdating.setVisibility(View.GONE);
                layoutagentbio.setVisibility(View.GONE);
                layoutagentchangepin.setVisibility(View.GONE);
                layoutidentifier.setVisibility(View.VISIBLE);
                btnconfirm.setVisibility(View.VISIBLE);
            }
        });

        search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                details_progressbar.setVisibility(View.VISIBLE);
                requestGetAccountDetails(id_number.getText().toString());
                layoutdetails.setVisibility(View.VISIBLE);
            }
        });

        btnconfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                layoutregistration.setVisibility(View.VISIBLE);
                layoutupdating.setVisibility(View.VISIBLE);
                layoutview.setVisibility(View.VISIBLE);
                layoutupdating.setVisibility(View.VISIBLE);
                layoutagentbio.setVisibility(View.VISIBLE);
                layoutdetails.setVisibility(View.GONE);
                layoutidentifier.setVisibility(View.GONE);
                layoutagentchangepin.setVisibility(View.VISIBLE);

            }
        });

        adapter = new ArrayAdapter<GrowerDetails>(GflMemberRegistry.this,
                R.layout.simple_spinner_dropdown_item, new ArrayList<GrowerDetails>());
        adapter.setDropDownViewResource(R.layout.simple_spinner_dropdown_item);
        growerSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                item = parent.getItemAtPosition(position).toString();
                text = growerSpinner.getSelectedItem().toString();
                loanPb.setVisibility(View.VISIBLE);
                arrayListLoan.clear();
                requestGetAccount(text);
                requestLoanProducts(text);
                availableKgs();
                loanPb.setVisibility(View.GONE);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


    }

    void requestGetAccount(String gn){
        String URL = GFL.MSACCO_AGENT + GFL.GetGrowersAccount;
        final GrowersAccountRequest gnAccountDetails = new GrowersAccountRequest();
        gnAccountDetails.agentid = SharedPrefs.read(SharedPrefs.AGENT_ID_1, null);
        gnAccountDetails.corporateno = "CAP016";
        gnAccountDetails.growerno = gn;
        gnAccountDetails.transactiontype = "0";
        gnAccountDetails.terminalid = SharedPrefs.read(SharedPrefs.DEVICE_ID, null);
        gnAccountDetails.longitude = getLong(GflMemberRegistry.this);
        gnAccountDetails.latitude = getLat(GflMemberRegistry.this);
        gnAccountDetails.date = getFormattedDate();


        GFL.instance(GflMemberRegistry.this).request(URL, gnAccountDetails, new GFL.RequestListener() {
            @Override
            public void onSuccess(String response) {
                GrowersAccountResponse growersAccountResponse = GFL.instance(GflMemberRegistry.this)
                        .mGson.fromJson(response, GrowersAccountResponse.class);
                if (growersAccountResponse.is_successful) {

                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        JSONArray jsonArray = jsonObject.getJSONArray("GetGrowersAccountList");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                            JSONArray jsonArray1 = jsonObject1.getJSONArray("GrowersAccount");

                            for (int l = 0; l < jsonArray1.length(); l++) {
                                JSONObject jsonObject2 = jsonArray1.getJSONObject(l);
                                details_progressbar.setVisibility(View.GONE);
                                currentkgs.setText(jsonObject2.getString("CurrKG"));
                                previouskgs.setText(jsonObject2.getString("PreviousKG"));

                            }

                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                } else {
                    Toast.makeText(GflMemberRegistry.this, "Accounts Fetching Failed", Toast.LENGTH_LONG).show();
                }

            }

            @Override
            public void onTokenExpired() {

            }

            @Override
            public void onError(String error) {

            }
        });
    }

    void availableKgs(){
        progressDialog = new ProgressDialog(GflMemberRegistry.this);
        progressDialog.setMessage("Please wait...");
        progressDialog.setCancelable(false);
        progressDialog.show();

        String URL = GFL.MSACCO_AGENT + GFL.GetAvailableKgsGuarantors;
        final GetAvailableKgsGuarantorsRequest availableKgs = new GetAvailableKgsGuarantorsRequest();
        availableKgs.agentid = SharedPrefs.read(SharedPrefs.AGENT_ID_1, null);
        availableKgs.corporateno = "CAP016";
        availableKgs.growerno = growerSpinner.getSelectedItem().toString();
        availableKgs.transactiontype = "1";
        availableKgs.terminalid = SharedPrefs.read(SharedPrefs.DEVICE_ID, null);
        availableKgs.longitude = getLong(GflMemberRegistry.this);
        availableKgs.latitude = getLat(GflMemberRegistry.this);
        availableKgs.date = getFormattedDate();
        GFL.instance(GflMemberRegistry.this).request(URL, availableKgs, new GFL.RequestListener() {
            @Override
            public void onSuccess(String response) {
                GetAvailableKgsGuarantorsResponse availableKgsResponse = GFL.instance(GflMemberRegistry.this)
                        .mGson.fromJson(response, GetAvailableKgsGuarantorsResponse.class);
                if (availableKgsResponse.is_successful) {
                    progressDialog.dismiss();
                    List<GrowerDetails> growerDetailsList = new ArrayList<>();
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        String availableKgs = jsonObject.getString("availablekgs");
                        double selfg= Double.parseDouble(availableKgs.replace(",",""))/2;

                        availablekgs.setText(availableKgs);
                        selfguaranteekgs.setText(String.valueOf(selfg));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                } else {
                    Toast.makeText(GflMemberRegistry.this, "please wait", Toast.LENGTH_SHORT).show();
                    progressDialog.dismiss();
                }

            }

            @Override
            public void onTokenExpired() {

            }

            @Override
            public void onError(String error) {

            }
        });
    }
    void requestGetAccountDetails(String identifier) {

        String URL = GFL.MSACCO_AGENT + GFL.GetGrowersDetails;
        final GrowersDetailsRequest growersDetails = new GrowersDetailsRequest();
        growersDetails.agentid = SharedPrefs.read(SharedPrefs.AGENT_ID_1, null);
        growersDetails.corporateno = "CAP016";
        growersDetails.accountidentifier = identifier;
        growersDetails.accountidentifiercode = "1";
        growersDetails.transactiontype = "1";
        growersDetails.terminalid = SharedPrefs.read(SharedPrefs.DEVICE_ID, null);
        growersDetails.longitude = getLong(GflMemberRegistry.this);
        growersDetails.latitude = getLat(GflMemberRegistry.this);
        growersDetails.date = getFormattedDate();

        arrayList.clear();
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(GflMemberRegistry.this, android.R.layout.simple_spinner_item, arrayList);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        growerSpinner.setAdapter(null);

        GFL.instance(GflMemberRegistry.this).request(URL, growersDetails, new GFL.RequestListener() {
            @Override
            public void onSuccess(String response) {
                GrowersDetailsResponse growersDetailsResponse = GFL.instance(GflMemberRegistry.this)
                        .mGson.fromJson(response, GrowersDetailsResponse.class);
                if (growersDetailsResponse.is_successful) {
                    List<GrowerDetails> growerDetailsList = new ArrayList<>();

                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        JSONArray jsonArray = jsonObject.getJSONArray("Growers");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                            JSONArray jsonArray1 = jsonObject1.getJSONArray("GrowerDetails");

                            for (int l = 0; l < jsonArray1.length(); l++) {
                                JSONObject jsonObject2 = jsonArray1.getJSONObject(l);
                                details_progressbar.setVisibility(View.GONE);
                                customername.setText(jsonObject2.getString("GrowerName"));
                                isdefaulter.setText(jsonObject2.getString("LoanDefaulter"));
                                arrayList.add(String.valueOf(jsonObject2.getString("GrowerNo")));
                            }

                        }
                        ArrayAdapter<String> adapter = new ArrayAdapter<String>(GflMemberRegistry.this, android.R.layout.simple_spinner_item, arrayList);
                        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        growerSpinner.setAdapter(adapter);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                } else {
                   Toast.makeText(GflMemberRegistry.this, "Accounts Fetching Failed", Toast.LENGTH_LONG).show();
                }

            }

            @Override
            public void onTokenExpired() {

            }

            @Override
            public void onError(String error) {

            }
        });
    }

    public Bitmap StringToBitMap(String encodedString) {
        try {
            byte[] encodeByte = Base64.decode(encodedString, Base64.DEFAULT);
            Bitmap bitmap = BitmapFactory.decodeByteArray(encodeByte, 0, encodeByte.length);
            return bitmap;
        } catch (Exception e) {
            e.getMessage();
            return null;
        }
    }

    private void zoomImageFromThumb(final View thumbView, Bitmap imageResId) {
        // If there's an animation in progress, cancel it
        // immediately and proceed with this one.
        if (currentAnimator != null) {
            currentAnimator.cancel();
        }

        final ImageView expandedImageView = (ImageView) findViewById(
                R.id.expanded_image);
        expandedImageView.setImageBitmap(imageResId);


        // Calculate the starting and ending bounds for the zoomed-in image.
        // This step involves lots of math. Yay, math.
        final Rect startBounds = new Rect();
        final Rect finalBounds = new Rect();
        final Point globalOffset = new Point();

        thumbView.getGlobalVisibleRect(startBounds);
        findViewById(R.id.container)
                .getGlobalVisibleRect(finalBounds, globalOffset);
        startBounds.offset(-globalOffset.x, -globalOffset.y);
        finalBounds.offset(-globalOffset.x, -globalOffset.y);

        float startScale;
        if ((float) finalBounds.width() / finalBounds.height()
                > (float) startBounds.width() / startBounds.height()) {
            // Extend start bounds horizontally
            startScale = (float) startBounds.height() / finalBounds.height();
            float startWidth = startScale * finalBounds.width();
            float deltaWidth = (startWidth - startBounds.width()) / 2;
            startBounds.left -= deltaWidth;
            startBounds.right += deltaWidth;
        } else {
            // Extend start bounds vertically
            startScale = (float) startBounds.width() / finalBounds.width();
            float startHeight = startScale * finalBounds.height();
            float deltaHeight = (startHeight - startBounds.height()) / 2;
            startBounds.top -= deltaHeight;
            startBounds.bottom += deltaHeight;
        }

        thumbView.setAlpha(0f);
        expandedImageView.setVisibility(View.VISIBLE);

        // Set the pivot point for SCALE_X and SCALE_Y transformations
        // to the top-left corner of the zoomed-in view (the default
        // is the center of the view).
        expandedImageView.setPivotX(0f);
        expandedImageView.setPivotY(0f);

        // Construct and run the parallel animation of the four translation and
        // scale properties (X, Y, SCALE_X, and SCALE_Y).
        AnimatorSet set = new AnimatorSet();
        set
                .play(ObjectAnimator.ofFloat(expandedImageView, View.X,
                        startBounds.left, finalBounds.left))
                .with(ObjectAnimator.ofFloat(expandedImageView, View.Y,
                        startBounds.top, finalBounds.top))
                .with(ObjectAnimator.ofFloat(expandedImageView, View.SCALE_X,
                        startScale, 1f))
                .with(ObjectAnimator.ofFloat(expandedImageView,
                        View.SCALE_Y, startScale, 1f));
        set.setDuration(shortAnimationDuration);
        set.setInterpolator(new DecelerateInterpolator());
        set.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {
                currentAnimator = null;
            }

            @Override
            public void onAnimationCancel(Animator animation) {
                currentAnimator = null;
            }
        });
        set.start();
        currentAnimator = set;

        // Upon clicking the zoomed-in image, it should zoom back down
        // to the original bounds and show the thumbnail instead of
        // the expanded image.
        final float startScaleFinal = startScale;
        expandedImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (currentAnimator != null) {
                    currentAnimator.cancel();
                }

                // Animate the four positioning/sizing properties in parallel,
                // back to their original values.
                AnimatorSet set = new AnimatorSet();
                set.play(ObjectAnimator
                        .ofFloat(expandedImageView, View.X, startBounds.left))
                        .with(ObjectAnimator
                                .ofFloat(expandedImageView,
                                        View.Y, startBounds.top))
                        .with(ObjectAnimator
                                .ofFloat(expandedImageView,
                                        View.SCALE_X, startScaleFinal))
                        .with(ObjectAnimator
                                .ofFloat(expandedImageView,
                                        View.SCALE_Y, startScaleFinal));
                set.setDuration(shortAnimationDuration);
                set.setInterpolator(new DecelerateInterpolator());
                set.addListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        thumbView.setAlpha(1f);
                        expandedImageView.setVisibility(View.GONE);
                        currentAnimator = null;
                    }

                    @Override
                    public void onAnimationCancel(Animator animation) {
                        thumbView.setAlpha(1f);
                        expandedImageView.setVisibility(View.GONE);
                        currentAnimator = null;
                    }
                });
                set.start();
                currentAnimator = set;
            }
        });
    }

    void requestLoanProducts(String identifier) {
        final String TAG = "LoansInfo";

        String URL = GFL.MSACCO_AGENT + GFL.GetLoansInfo;

        final GetLoansInfoRequest loansInfoRequest = new GetLoansInfoRequest();
        loansInfoRequest.corporateno = "CAP016";
        loansInfoRequest.growersno = identifier;
        loansInfoRequest.transactiontype = "1";
        loansInfoRequest.agentid = SharedPrefs.read(SharedPrefs.AGENT_ID_1, null);
        loansInfoRequest.terminalid = SharedPrefs.read(SharedPrefs.DEVICE_ID, null);
        loansInfoRequest.longitude = getLong(this);
        loansInfoRequest.latitude = getLat(this);
        loansInfoRequest.date = getFormattedDate();

        Log.e(TAG, loansInfoRequest.getBody().toString());

        GFL.instance(this).request(URL, loansInfoRequest, new GFL.RequestListener() {
            @Override
            public void onSuccess(String response) {
                /*deposit_progressBar.setVisibility(View.GONE);*/

                GetLoansInfoResponse loansInfoResponse = GFL.instance(GflMemberRegistry.this)
                        .mGson.fromJson(response, GetLoansInfoResponse.class);
                if (loansInfoResponse.is_successful) {
                    List<LoanDetails> loanDetailsList = new ArrayList<>();
                    arrayListLoan.clear();
                    try {
                        JSONObject jsonObject = new JSONObject(response);

                        JSONArray jsonArray = jsonObject.getJSONArray("GetLoansInfo");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                            JSONArray jsonArray1 = jsonObject1.getJSONArray("LoanDetails");
                            for (int l = 0; l < jsonArray1.length(); l++) {
                                JSONObject jsonObject2 = jsonArray1.getJSONObject(l);

                                String productType = jsonObject2.getString("ProductType");
                                String loansPayKey = jsonObject2.getString("LoansPayKey");
                                String loansStatus = jsonObject2.getString("LoansStatus");
                                String dropDown1 = loansPayKey + "-" + productType;

                                arrayListLoan.add(String.valueOf(dropDown1));
                            }

                        }

                        //Growers Number
                        //spinner = findViewById(R.id.productType);
                        ArrayAdapter<String> adapter1 = new ArrayAdapter<String>(GflMemberRegistry.this, android.R.layout.simple_spinner_item, arrayListLoan);
                        adapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        spinnerLoan.setAdapter(adapter1);


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                } else {
//                    Toast.makeText(ShareDeposit.this, "Accounts Fetching Failed", Toast.LENGTH_LONG).show();
                }

            }

            @Override
            public void onTokenExpired() {

            }

            @Override
            public void onError(String error) {

            }
        });
    }


    private void createDialogChangePassword() {
        LayoutInflater myInflater = LayoutInflater.from(this);
        View v = myInflater.inflate(R.layout.dialog_change_password, null);
        //define and access view attributes
        final EditText et_agent_id = v.findViewById(R.id.et_agent_id);
        final EditText old_pass = v.findViewById(R.id.et_old_pass);
        final EditText new_pass = v.findViewById(R.id.et_new_pass);
        final EditText confirm_pass = v.findViewById(R.id.et_confirm_pass);
        Button submit = v.findViewById(R.id.button_submit);

        et_agent_id.setText(sharedPref.getString(PreferenceFileKeys.AGENT_ID, ""));
        et_agent_id.setKeyListener(null);

        //on click listeners
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String agentid = et_agent_id.getText().toString();
                //String pass_word = input_agent_password.getText().toString();
                String oldPass = old_pass.getText().toString();
                String newPass = new_pass.getText().toString();
                String confirmPass = confirm_pass.getText().toString();

                Log.d("pinnn","pin------>"+Api.pinn);
                if (!oldPass.equals(Api.pinn)) {
                    old_pass.setError("Pin does not match with the Old Password!");
                } else if (agentid.isEmpty()) {
                    et_agent_id.setError("Please enter your agent id!");
                } else if (oldPass.isEmpty()) {
                    old_pass.setError("Please enter your old Pin!");
                } else if (newPass.isEmpty()) {
                    new_pass.setError("Please enter your new Pin!");
                } else if (confirmPass.isEmpty()) {
                    confirm_pass.setError("Please confirm your new Pin!");
                } else if (!confirmPass.equals(newPass)) {
                    confirm_pass.setError("Pin does not match with the new Pin!");
                } else {
                    progressDialog = new ProgressDialog(GflMemberRegistry.this);
                    progressDialog.setMessage("Processing your request...");
                    progressDialog.setCancelable(false);
                    progressDialog.show();
                    requestPasswordChange(agentid, Api.pinn, confirmPass);
                }

            }
        });

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setView(v);
        changePasswordDialog = builder.create();
        changePasswordDialog.show();
        changePasswordDialog.setCancelable(true);

    }

    private void requestPasswordChange(String agentidd, String userpin, String newpin) {

        String URL = Api.MSACCO_AGENT + Api.AgentPINChange;
        final ChangePin pinchange = new ChangePin();

        pinchange.agentid = agentidd;
        pinchange.terminalid = Utils.getIMEI(this);
        pinchange.old_pin = userpin;
        pinchange.new_pin = newpin;
        pinchange.accountidentifier = agentidd;
        pinchange.accountidentifiercode = "0";
        pinchange.longitude = getLong(this);
        pinchange.latitude = getLat(this);
        pinchange.date = getFormattedDate();


        Api.instance(this).request(URL, pinchange, new Api.RequestListener() {
            @Override
            public void onSuccess(String response) {
                JSONObject jsonObject = null;
                try {
                    jsonObject = new JSONObject(response);

                    PinChangeResponse pinchangeresponseResponse = Api.instance(GflMemberRegistry.this).mGson.fromJson(response, PinChangeResponse.class);
                    if (pinchangeresponseResponse.is_successful) {

                        progressDialog.dismiss();
                        AlertDialog.Builder builder1 = new AlertDialog.Builder(GflMemberRegistry.this);
                        builder1.setMessage("Pin Changed Succesfully");
                        builder1.setTitle("Success");
                        builder1.setCancelable(false);

                        builder1.setPositiveButton(
                                "OK",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();
                                        changePasswordDialog.dismiss();
                                    }
                                });

                        AlertDialog alert11 = builder1.create();
                        alert11.show();

                    } else {
                        Utils.showAlertDialog(GflMemberRegistry.this, "Please try again", pinchangeresponseResponse.getError());
                        progressDialog.dismiss();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }

            @Override
            public void onTokenExpired() {

            }

            @Override
            public void onError(String error) {

            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
       // LogOutTimerUtil.startLogoutTimer(this, this);

    }

    @Override
    public void onUserInteraction() {
        super.onUserInteraction();
        LogOutTimerUtil.startLogoutTimer(this, this);

    }


    @Override
    protected void onPause() {
        super.onPause();
        LogOutTimerUtil.startLogoutTimer(this, this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        LogOutTimerUtil.startLogoutTimer(this, this);

    }

    @Override
    public void doLogout() {
        finish();
    }



}
