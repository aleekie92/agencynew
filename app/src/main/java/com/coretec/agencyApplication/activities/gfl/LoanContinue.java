package com.coretec.agencyApplication.activities.gfl;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.support.annotation.Keep;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.coretec.agencyApplication.R;
import com.coretec.agencyApplication.activities.BaseActivity;
import com.coretec.agencyApplication.api.GFL;
import com.coretec.agencyApplication.api.requests.GetGuarantorsRequest;
import com.coretec.agencyApplication.api.requests.GrowersDetailsRequest;
import com.coretec.agencyApplication.api.responses.GetGuarantorsResponse;
import com.coretec.agencyApplication.api.responses.GrowersDetailsResponse;
import com.coretec.agencyApplication.model.GrowerDetails;
import com.coretec.agencyApplication.utils.QuickToast;
import com.coretec.agencyApplication.utils.SharedPrefs;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import static com.coretec.agencyApplication.utils.Utils.getFormattedDate;
import static com.coretec.agencyApplication.utils.Utils.getLat;
import static com.coretec.agencyApplication.utils.Utils.getLong;

public class LoanContinue extends AppCompatActivity {

    private QuickToast toast = new QuickToast(this);
    private EditText identifier_deposit;
    Button submit;
    private Spinner spinner;
    private ArrayList<String> arrayList = new ArrayList<>();
    private ArrayAdapter<GrowerDetails> adapter;
    ProgressDialog progressDialog;
    ProgressBar progressBar;
    public String spinner_txt = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        SharedPrefs.init(getApplicationContext());
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.gfl_loan_continue);

        this.overridePendingTransition(R.anim.move_right_in_activity, R.anim.move_left_out_activity);

        Toolbar toolbar = findViewById(R.id.toolbar);
        TextView mTitle = toolbar.findViewById(R.id.toolbar_title);
        mTitle.setText(getResources().getText(R.string.continue_app));
        setSupportActionBar(toolbar);
        ActionBar actionbar = getSupportActionBar();
        actionbar.setDisplayHomeAsUpEnabled(true);
        actionbar.setHomeAsUpIndicator(R.drawable.ic_arrow_back);

        identifier_deposit = findViewById(R.id.id_number);
        progressBar = findViewById(R.id.growersPb);
        spinner = findViewById(R.id.growerSpinner);
        identifier_deposit.setInputType(InputType.TYPE_NULL);
        identifier_deposit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // showMyDialog();
            }
        });

        identifier_deposit.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    // showMyDialog();
                }
            }
        });

        adapter = new ArrayAdapter<GrowerDetails>(getApplicationContext(),
                R.layout.simple_spinner_dropdown_item, new ArrayList<GrowerDetails>());
        adapter.setDropDownViewResource(R.layout.simple_spinner_dropdown_item);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String text = spinner.getSelectedItem().toString();
                if (text.isEmpty()) {
                    spinner_txt = "";
                } else {
                    spinner_txt = text;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        submit = findViewById(R.id.btnSubmit);
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //String text = spinner.getSelectedItem().toString();

                if (identifier_deposit.getText().toString().matches("")) {
                    toast.swarn("Please fill in the missing fields to proceed!");
                } else if (spinner_txt.equals("")) {
                    toast.swarn("Please fetch grower number!");
                } else {

                    /* progress dialog */
                    progressDialog = new ProgressDialog(LoanContinue.this);
                    progressDialog.setMessage("Please wait...");
                    progressDialog.setCancelable(false);
                    progressDialog.show();

                    getGuarantors(spinner_txt);

                }
            }
        });

        TextWatcher textWatcherId = new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                //here, after we introduced something in the EditText we get the string from it
                String identifier_number = identifier_deposit.getText().toString();
                if (identifier_deposit.length() >= 7) {
                    requestGetAccountDetails(identifier_number);
                }
                if (identifier_deposit.length() <= 5) {
                    progressBar.setVisibility(View.GONE);
                    arrayList.clear();
                    spinner = findViewById(R.id.growerSpinner);
                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(LoanContinue.this, android.R.layout.simple_spinner_item, arrayList);
                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    spinner.setAdapter(null);
                }
                if (identifier_deposit.length() >= 5 && identifier_deposit.length() <= 8) {
                    arrayList.clear();
                    spinner = findViewById(R.id.growerSpinner);
                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(LoanContinue.this, android.R.layout.simple_spinner_item, arrayList);
                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    spinner.setAdapter(null);
                    progressBar.setVisibility(View.VISIBLE);
                    progressBar.getIndeterminateDrawable().setColorFilter(ContextCompat.getColor(LoanContinue.this, R.color.btn_green), PorterDuff.Mode.SRC_IN);
                }

            }
        };
        identifier_deposit.addTextChangedListener(textWatcherId);

    }

    //@Keep
    void requestGetAccountDetails(String identifier) {

        final String TAG = "GrowersAccounts";
//        EasyLocationMod easyLocationMod = new EasyLocationMod(CashWithdrawal.this);

        String URL = GFL.MSACCO_AGENT + GFL.GetGrowersDetails;

        final GrowersDetailsRequest growersDetails = new GrowersDetailsRequest();
        growersDetails.agentid = SharedPrefs.read(SharedPrefs.AGENT_ID_1, null);
        growersDetails.corporateno = "CAP016";
        growersDetails.accountidentifier = identifier;
        growersDetails.accountidentifiercode = "1";
        growersDetails.transactiontype = "1";
        growersDetails.terminalid = SharedPrefs.read(SharedPrefs.DEVICE_ID, null);
        growersDetails.longitude = getLong(this);
        growersDetails.latitude = getLat(this);
        growersDetails.date = getFormattedDate();

        Log.e(TAG, growersDetails.getBody().toString());
        //clear contents of spinner
        arrayList.clear();
        spinner = findViewById(R.id.growerSpinner);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(LoanContinue.this, android.R.layout.simple_spinner_item, arrayList);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(null);

        GFL.instance(this).request(URL, growersDetails, new GFL.RequestListener() {
            @Override
            public void onSuccess(String response) {
                /*deposit_progressBar.setVisibility(View.GONE);*/

                GrowersDetailsResponse growersDetailsResponse = GFL.instance(LoanContinue.this)
                        .mGson.fromJson(response, GrowersDetailsResponse.class);
                if (growersDetailsResponse.is_successful) {
                    List<GrowerDetails> growerDetailsList = new ArrayList<>();

                    try {
                        JSONObject jsonObject = new JSONObject(response);

                        JSONArray jsonArray = jsonObject.getJSONArray("Growers");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                            JSONArray jsonArray1 = jsonObject1.getJSONArray("GrowerDetails");

                            if (jsonArray1.length() < 1) {
                                progressBar.setVisibility(View.GONE);
                                Toast.makeText(LoanContinue.this, "", Toast.LENGTH_SHORT).show();

                            } else {

                                for (int l = 0; l < jsonArray1.length(); l++) {
                                    JSONObject jsonObject2 = jsonArray1.getJSONObject(l);
                                    progressBar.setVisibility(View.GONE);
                                    arrayList.add(String.valueOf(jsonObject2.getString("GrowerNo")));
                                    /*mpesaPhoneNumber = jsonObject2.getString("phonenumber");*/
                                    String gNo = jsonObject2.getString("GrowerNo");
                                    String pNumber = jsonObject2.getString("GrowerMobileNo");
                                    String isDefaulter = jsonObject2.getString("LoanDefaulter");

                                    SharedPrefs.write(SharedPrefs.IS_DEFAULTER, isDefaulter);

                                }
                            }

                        }

                        //Growers Number
                        spinner = findViewById(R.id.growerSpinner);
                        ArrayAdapter<String> adapter = new ArrayAdapter<String>(LoanContinue.this, android.R.layout.simple_spinner_item, arrayList);
                        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                        spinner.setAdapter(adapter);


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                } else {
//                    Toast.makeText(ShareDeposit.this, "Accounts Fetching Failed", Toast.LENGTH_LONG).show();
                }

            }

            @Override
            public void onTokenExpired() {

            }

            @Override
            public void onError(String error) {

            }
        });
    }

    void getGuarantors(final String growerNumber) {

        final String TAG = "GetGuarantors";
        String URL = GFL.MSACCO_AGENT + GFL.GetGuarantors;

        final GetGuarantorsRequest guarantorsRequest = new GetGuarantorsRequest();
        guarantorsRequest.agentid = SharedPrefs.read(SharedPrefs.AGENT_ID_1, null);
        guarantorsRequest.corporateno = "CAP016";
        guarantorsRequest.growerno = growerNumber;
        guarantorsRequest.terminalid = SharedPrefs.read(SharedPrefs.DEVICE_ID, null);
        guarantorsRequest.longitude = getLong(this);
        guarantorsRequest.latitude = getLat(this);
        guarantorsRequest.date = getFormattedDate();

        Log.e(TAG, guarantorsRequest.getBody().toString());

        GFL.instance(this).request(URL, guarantorsRequest, new GFL.RequestListener() {
            @Override
            public void onSuccess(String response) {

                GetGuarantorsResponse getGuarantorsResponse = GFL.instance(LoanContinue.this)
                        .mGson.fromJson(response, GetGuarantorsResponse.class);
                if (getGuarantorsResponse.is_successful) {
                    progressDialog.dismiss();
                    try {
                        JSONObject jsonObject = new JSONObject(response);

                        JSONArray guarantorlist = jsonObject.getJSONArray("guarantorlist");
                        /*if (guarantorlist.length() == 0) {
                            toast.swarn("Grower has no pending loan application");
                        } else {
                            toast.ssuccess("Yaaaay!");
                        }*/
                        String msg = "Loan details found successfully. Are you sure you want to continue loan application?";
                        //final String qualKgs = SharedPrefs.read(SharedPrefs.LOAN_QUALIFIED_KGS, null);
                        android.app.AlertDialog.Builder builder1 = new android.app.AlertDialog.Builder(LoanContinue.this);
                        builder1.setMessage(msg);
                        builder1.setTitle("Success!");
                        builder1.setCancelable(false);

                        builder1.setPositiveButton(
                                "Yes",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();
                                        Intent i = new Intent(LoanContinue.this, Guarantor.class);

                                        i.putExtra("grower_number", growerNumber);
                                        LoanContinue.this.startActivity(i);

                                    }
                                });

                        builder1.setNegativeButton(
                                "Cancel",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();
                                    }
                                });

                        android.app.AlertDialog alert11 = builder1.create();
                        alert11.show();

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                } else {
                    progressDialog.dismiss();
                    toast.swarn("Could not find loan details for this grower number!");
                }

            }

            @Override
            public void onTokenExpired() {

            }

            @Override
            public void onError(String error) {

            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.move_left_in_activity, R.anim.move_right_out_activity);
    }

}
