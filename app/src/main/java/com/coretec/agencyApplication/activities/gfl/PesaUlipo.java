package com.coretec.agencyApplication.activities.gfl;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.PorterDuff;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.cloudpos.jniinterface.PrinterInterface;
import com.coretec.agencyApplication.R;
import com.coretec.agencyApplication.activities.BaseActivity;
import com.coretec.agencyApplication.activities.newmodules.ValidateTunzhengbigBio;
import com.coretec.agencyApplication.api.GFL;
import com.coretec.agencyApplication.api.requests.FingerPrintRequest;
import com.coretec.agencyApplication.api.requests.GrowersDetailsRequest;
import com.coretec.agencyApplication.api.requests.PuBlockRequest;
import com.coretec.agencyApplication.api.responses.FingerPrintResponse;
import com.coretec.agencyApplication.api.responses.GrowersDetailsResponse;
import com.coretec.agencyApplication.api.responses.PuBlockResponse;
import com.coretec.agencyApplication.model.GrowerDetails;
import com.coretec.agencyApplication.utils.SharedPrefs;
import com.coretec.agencyApplication.wizarpos.function.printer.PrinterCommand;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static com.coretec.agencyApplication.utils.Utils.getFormattedDate;
import static com.coretec.agencyApplication.utils.Utils.getLat;
import static com.coretec.agencyApplication.utils.Utils.getLong;

public class PesaUlipo extends AppCompatActivity /*implements View.OnClickListener*/ {

    private LinearLayout upload;
    private ImageView imageview_reason, backBtn;
    private static final int CAMERA_REQUEST = 1888;

    //keep track of camera capture intent
    final int CAMERA_CAPTURE = 1;
    //captured picture uri
    private Uri picUri;
    //keep track of cropping intent
    final int PIC_CROP = 2;

    private TextInputLayout input_layout_id_number;
    private EditText identifier_deposit;
    private EditText reason;
    private TextView growerName, phoneNumber;
    Button submit;

    private Spinner spinner;
    private ArrayList<String> arrayList = new ArrayList<>();
    private ArrayAdapter<GrowerDetails> adapter;

    private String item;
    ProgressBar progressBar;
    ProgressDialog progressDialog;
    private RadioGroup radioGroupDeactivate;
    private RadioButton rbSelf,rbThirdParty,rbStaff;
    String text;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        SharedPrefs.init(getApplicationContext());

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.gfl_pesa_ulipo);

        this.overridePendingTransition(R.anim.move_right_in_activity, R.anim.move_left_out_activity);

        SharedPrefs.write(SharedPrefs.FP_BOOLEAN, "");

        Toolbar toolbar = findViewById(R.id.toolbar);
        TextView mTitle = toolbar.findViewById(R.id.toolbar_title);
        mTitle.setText(getResources().getText(R.string.block_pu));
        setSupportActionBar(toolbar);
        ActionBar actionbar = getSupportActionBar();
        actionbar.setDisplayHomeAsUpEnabled(true);
        actionbar.setHomeAsUpIndicator(R.drawable.ic_arrow_back);

        imageview_reason = findViewById(R.id.imageview_reason);

        growerName = findViewById(R.id.grower_name);
        phoneNumber = findViewById(R.id.phone_number);

        radioGroupDeactivate = findViewById(R.id.radioGroupAccount);
        rbSelf = findViewById(R.id.rbSelf);
        rbThirdParty = findViewById(R.id.rbThirdParty);
        rbStaff = findViewById(R.id.rbStaff);

        rbSelf.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (rbSelf.isChecked()) {
                    //createDialogBank();
                    //startActivity(new Intent(getContext(), BankDialog.class));
                }
            }
        });

        rbThirdParty.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (rbThirdParty.isChecked()) {
                    //createDialogBank();
                    //startActivity(new Intent(getContext(), BankDialog.class));
                }
            }
        });

        rbStaff.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (rbStaff.isChecked()) {
                    //createDialogBank();
                    //startActivity(new Intent(getContext(), BankDialog.class));
                }
            }
        });

        upload = findViewById(R.id.upload);
        //upload.setOnClickListener(this);
        upload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(cameraIntent, CAMERA_REQUEST);
            }
        });

        input_layout_id_number = findViewById(R.id.input_layout_id_number);
        identifier_deposit = findViewById(R.id.id_number);

        identifier_deposit.setInputType(InputType.TYPE_NULL);
        identifier_deposit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // showMyDialog();
            }
        });

        identifier_deposit.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    // showMyDialog();
                }
            }
        });

        reason = findViewById(R.id.reason);
        submit = findViewById(R.id.btnSubmit);
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (identifier_deposit.getText().toString().matches("") || reason.getText().toString().matches("")) {
                    Toast.makeText(getBaseContext(), "Please fill in the missing fields to proceed!", Toast.LENGTH_LONG).show();
                } else {

                        /* progress dialog */
                        progressDialog = new ProgressDialog(PesaUlipo.this);
                        progressDialog.setMessage("Please wait...");
                        progressDialog.setCancelable(false);
                        progressDialog.show();

                    if (!rbSelf.isChecked() && !rbThirdParty.isChecked() && !rbStaff.isChecked()) {
                        Toast.makeText(PesaUlipo.this, "Please select option", Toast.LENGTH_SHORT).show();
                        progressDialog.dismiss();
                    }else if(rbSelf.isChecked() ){
                        requestFingerPrints();
                    }else {
                        progressDialog.dismiss();
                        int result = PrinterInterface.open();
                        header();
                        PrinterInterface.close();
                        blockPu();
                    }
                        //String gNumber = SharedPrefs.read(SharedPrefs.MPESA_G_NUMBER, null);

                    /*int result = PrinterInterface.open();
                    header();
                    PrinterInterface.close();
                    blockPu();*/
                    }
                }
        });

        spinner = findViewById(R.id.growerSpinner);

        progressBar = findViewById(R.id.growersPb);
        reason = findViewById(R.id.reason);

        TextWatcher textWatcherId = new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                //here, after we introduced something in the EditText we get the string from it
                String identifier_number = identifier_deposit.getText().toString();
                if (identifier_deposit.length() >= 7) {
                    requestGetAccountDetails(identifier_number);
                }
                if (identifier_deposit.length() <= 5) {
                    growerName.setText(null);
                    phoneNumber.setText(null);
                    progressBar.setVisibility(View.GONE);
                    arrayList.clear();
                    spinner = findViewById(R.id.growerSpinner);
                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(PesaUlipo.this, android.R.layout.simple_spinner_item, arrayList);
                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    spinner.setAdapter(null);
                }
                if (identifier_deposit.length() >= 5 && identifier_deposit.length() <= 8) {
                    growerName.setText(null);
                    phoneNumber.setText(null);
                    arrayList.clear();
                    spinner = findViewById(R.id.growerSpinner);
                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(PesaUlipo.this, android.R.layout.simple_spinner_item, arrayList);
                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    spinner.setAdapter(null);
                    progressBar.setVisibility(View.VISIBLE);
                    progressBar.getIndeterminateDrawable().setColorFilter(ContextCompat.getColor(PesaUlipo.this, R.color.btn_green), PorterDuff.Mode.SRC_IN);
                }
//                Toast.makeText(ShareDeposit.this, accountNumber, Toast.LENGTH_SHORT).show();
//                requestAccountName(accountNumber);

            }
        };

        adapter = new ArrayAdapter<GrowerDetails>(getApplicationContext(),
                R.layout.simple_spinner_dropdown_item, new ArrayList<GrowerDetails>());
        adapter.setDropDownViewResource(R.layout.simple_spinner_dropdown_item);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                /*GrowerDetails growerDetails = (GrowerDetails) parent.getItemAtPosition(position);
                growerNo = growerDetails.getGrowerNo();*/
                item = parent.getItemAtPosition(position).toString();
                text = spinner.getSelectedItem().toString();
                if (item != null) {
                    //Toast.makeText(parent.getContext(), "Selected" + position, Toast.LENGTH_SHORT).show();

                    SharedPrefs.write(SharedPrefs.PU_G_NUMBER, text);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        identifier_deposit.addTextChangedListener(textWatcherId);

    }

    void requestFingerPrints() {

        final String TAG = "GET FINGERPRINTS";
        String URL = GFL.MSACCO_AGENT + GFL.GetFourFingerPrint;

        final FingerPrintRequest fingerPrintRequest = new FingerPrintRequest();
        fingerPrintRequest.accountidentifier = identifier_deposit.getText().toString();
        fingerPrintRequest.accountidentifiercode = "1";
        fingerPrintRequest.agentid = SharedPrefs.read(SharedPrefs.AGENT_ID_1, null);
        fingerPrintRequest.terminalid = MainGFLDashboard.imei;//SharedPrefs.read(SharedPrefs.DEVICE_ID, null);
        fingerPrintRequest.longitude = getLong(this);
        fingerPrintRequest.latitude = getLat(this);
        fingerPrintRequest.date = getFormattedDate();

        Log.e(TAG, fingerPrintRequest.getBody().toString());

        GFL.instance(this).request(URL, fingerPrintRequest, new GFL.RequestListener() {
            @Override
            public void onSuccess(String response) {
                /*deposit_progressBar.setVisibility(View.GONE);*/
                //progressDialog.dismiss();
                FingerPrintResponse fingerPrintResponse = GFL.instance(PesaUlipo.this)
                        .mGson.fromJson(response, FingerPrintResponse.class);
                if (fingerPrintResponse.is_successful) {

                    try {
                        JSONObject jsonObject = new JSONObject(response);

                        String leftRing = jsonObject.getString("leftring");
                        String leftLittle = jsonObject.getString("leftlittle");
                        String rightRing = jsonObject.getString("rightring");
                        String rightLittle = jsonObject.getString("rightlittle");

                        SharedPrefs.write(SharedPrefs.LEFT_RING, leftRing);
                        SharedPrefs.write(SharedPrefs.LEFT_LITTLE, leftLittle);
                        SharedPrefs.write(SharedPrefs.RIGHT_RING, rightRing);
                        SharedPrefs.write(SharedPrefs.RIGHT_LITTLE, rightLittle);

                        Log.d("LEFT_RING", leftRing);
                        Log.d("LEFT_LITTLE", leftLittle);
                        Log.d("RIGHT_RING", rightRing);
                        Log.d("RIGHT_LITTLE", rightLittle);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    SharedPrefs.transactionstypelogs(10, PesaUlipo.this);
                    SharedPrefs.growerslogs(text, PesaUlipo.this);
                    startActivity(new Intent(PesaUlipo.this, ValidateTunzhengbigBio.class));
                } else {
//                    Toast.makeText(ShareDeposit.this, "Accounts Fetching Failed", Toast.LENGTH_LONG).show();
                }

            }

            @Override
            public void onTokenExpired() {

            }

            @Override
            public void onError(String error) {

            }
        });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case CAMERA_REQUEST:
                if (resultCode == Activity.RESULT_OK) {
                    try {
                        Bitmap photo = (Bitmap) data.getExtras().get("data");
                        imageview_reason.setImageBitmap(photo);

                        //imageView.setImageBitmap(bitmap);
                        /*Toast.makeText(this, imageview_reason.toString(),
                                Toast.LENGTH_SHORT).show();*/
                    } catch (Exception e) {
                        Toast.makeText(this, "Failed to load", Toast.LENGTH_SHORT)
                                .show();
                        Log.e("Camera", e.toString());
                    }
                }
        }

    }

    private String bitmapToBase64(Bitmap bitmap) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
        byte[] byteArray = byteArrayOutputStream.toByteArray();
        return Base64.encodeToString(byteArray, Base64.DEFAULT);
    }

    private void blockPu() {

        final String TAG = "Block PU";
        String URL = GFL.MSACCO_AGENT + GFL.PuBlock;
            Bitmap img = ((BitmapDrawable) imageview_reason.getDrawable()).getBitmap();
        String encodedImage = bitmapToBase64(img);
        final PuBlockRequest blockPu = new PuBlockRequest();
        blockPu.idno = identifier_deposit.getText().toString();
        blockPu.growersno = SharedPrefs.read(SharedPrefs.PU_G_NUMBER, null);
        blockPu.accountname = "";
        blockPu.message = "";
        blockPu.salescode = "";
        blockPu.userid = "";
        blockPu.description = reason.getText().toString();
        blockPu.transactiontype = "Block PU";
        blockPu.agentid = SharedPrefs.read(SharedPrefs.AGENT_ID_1, null);
        blockPu.terminalid = SharedPrefs.read(SharedPrefs.DEVICE_ID, null);
        blockPu.longitude = getLong(this);
        blockPu.latitude = getLat(this);
        blockPu.date = getFormattedDate();
        blockPu.corporate_no = "CAP016";
        blockPu.phoneno=phoneNumber.getText().toString();
        blockPu.bitmap = encodedImage;

        Log.e(TAG, blockPu.getBody().toString());

        GFL.instance(this).request(URL, blockPu, new GFL.RequestListener() {
            @Override
            public void onSuccess(String response) {
                /*deposit_progressBar.setVisibility(View.GONE);*/
                progressDialog.dismiss();
                PuBlockResponse blockPuResponse = GFL.instance(PesaUlipo.this)
                        .mGson.fromJson(response, PuBlockResponse.class);
                if (blockPuResponse.is_successful) {

                    String idno = identifier_deposit.getText().toString();
                    String pNumber = SharedPrefs.read(SharedPrefs.PU_BLOCK_P_NUMBER, null);

                    int result = PrinterInterface.open();
                    writetest(idno, pNumber);
                    PrinterInterface.close();

                    progressDialog.dismiss();
                    try {
                        JSONObject jsonObject = new JSONObject(response);

                        String receiptNo = jsonObject.getString("receiptNo");

                        String success = "PU Blocked Successfully...";
                        AlertDialog.Builder builder1 = new AlertDialog.Builder(PesaUlipo.this);
                        builder1.setMessage(success);
                        builder1.setTitle("Success!");
                        builder1.setCancelable(true);

                        builder1.setPositiveButton(
                                "DONE",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        Intent intent = new Intent(getApplicationContext(), MainGFLDashboard.class);
                                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                        startActivity(intent);
                                    }
                                });

                        /*builder1.setNegativeButton(
                                "Cancel",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();
                                    }
                                });*/

                        AlertDialog alert11 = builder1.create();
                        alert11.show();
                        Toast.makeText(PesaUlipo.this, receiptNo, Toast.LENGTH_SHORT).show();

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                } else {
//                    Toast.makeText(ShareDeposit.this, "Accounts Fetching Failed", Toast.LENGTH_LONG).show();
                }

            }

            @Override
            public void onTokenExpired() {

            }

            @Override
            public void onError(String error) {

            }
        });

    }

    public void header() {

        try {
            byte[] start = null;
            byte[] saccoName = null;
            byte[] type = null;
            byte[] isDefa = null;

            try {
                start = String.valueOf("              ").getBytes("GB2312");
                saccoName = String.valueOf("      GREENLAND FEDHA LTD     ").getBytes("GB2312");
                type = String.valueOf("        BLOCK PU REQUEST     ").getBytes("GB2312");
                isDefa = String.valueOf("Defaulter: "+SharedPrefs.read(SharedPrefs.IS_DEFAULTER, null)).getBytes("GB2312");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            PrinterInterface.begin();
            byte[] cmd;
            cmd = new byte[]{(byte) 0x1B, (byte) 0x40};

            cmd = new byte[]{
                    (byte) 0x1B, (byte) 0x45, (byte) 0};

            write(cmd);
            write(start);
            writeLineBreak(1);
            write(saccoName);
            writeLineBreak(2);
            write(type);
            writeLineBreak(1);
            write(isDefa);
            writeLineBreak(1);
            PrinterInterface.end();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void writetest(String id, String pNumber) {
        //String middle = idNos.substring(2, 6);
        String first = id.substring(0, 2);
        String last = id.substring(6, id.length());
        String maskedId = first + "****"+last;

        //Old Phone Number
        String firstOP = pNumber.substring(0, 7);
        String lastOP = pNumber.substring(11, pNumber.length());
        String maskedOP = firstOP + "*****"+lastOP;

        try {
            byte[] idNumber = null;
            byte[] phoneNumber = null;
            byte[] message = null;
            byte[] time = null;
            byte[] nameAgent = null;
            byte[] motto = null;
            SimpleDateFormat timeStampFormat = new SimpleDateFormat("yyyy/MM/dd - HH:mm");
            Date myDate = new Date();
            String timeCurrent = timeStampFormat.format(myDate);
            String aName = SharedPrefs.read(SharedPrefs.AGENT_NAME, null);

            try {
                idNumber = String.valueOf("ID Number : " + String.valueOf(maskedId)).getBytes("GB2312");
                phoneNumber = String.valueOf("Number Blocked : " + String.valueOf(maskedOP)).getBytes("GB2312");
                message = String.valueOf("NB: If you need the service to  be re-enabled you need to bring   back your original ID card").getBytes("GB2312");
                time = String.valueOf("   Date : " + String.valueOf(timeCurrent)).getBytes("GB2312");
                //reason = String.valueOf(sababu).getBytes("GB2312");
                nameAgent = String.valueOf("You were served by " + String.valueOf(aName)).getBytes("GB2312");
                motto = String.valueOf("         GFL Motto       ").getBytes("GB2312");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            PrinterInterface.begin();
            byte[] cmd;
            cmd = new byte[]{(byte) 0x1B, (byte) 0x40};

            cmd = new byte[]{
                    (byte) 0x1B, (byte) 0x45, (byte) 0};

            write(cmd);
            write(idNumber);
            writeLineBreak(1);
            write(phoneNumber);
            // print line break
            writeLineBreak(2);
            write(message);
            writeLineBreak(2);
            write(time);
            writeLineBreak(2);
            write(nameAgent);
            writeLineBreak(1);
            write(motto);
            // print line break
            writeLineBreak(4);
            writeLineBreak(5);
            PrinterInterface.end();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void write(final byte[] arryData) {
        int result = PrinterInterface.write(arryData, arryData.length);
    }

    // print line break
    private void writeLineBreak(int lineNumber) {
        // write(new byte[]{'\n'});
        write(PrinterCommand.getCmdEscDN(lineNumber));
    }

    void requestGetAccountDetails(String identifier) {

        final String TAG = "GrowersAccounts";
//        EasyLocationMod easyLocationMod = new EasyLocationMod(CashWithdrawal.this);

        String URL = GFL.MSACCO_AGENT + GFL.GetGrowersDetails;

        final GrowersDetailsRequest growersDetails = new GrowersDetailsRequest();
        growersDetails.agentid = SharedPrefs.read(SharedPrefs.AGENT_ID_1, null);
        growersDetails.corporateno = "CAP016";
        growersDetails.accountidentifier = identifier;
        growersDetails.accountidentifiercode = "1";
        growersDetails.transactiontype = "1";
        growersDetails.terminalid = SharedPrefs.read(SharedPrefs.DEVICE_ID, null);
        growersDetails.longitude = getLong(this);
        growersDetails.latitude = getLat(this);
        growersDetails.date = getFormattedDate();
        growersDetails.phoneno=phoneNumber.getText().toString();

        Log.e(TAG, growersDetails.getBody().toString());
        growerName.setText(null);
        phoneNumber.setText(null);
        arrayList.clear();
        spinner = findViewById(R.id.growerSpinner);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(PesaUlipo.this, android.R.layout.simple_spinner_item, arrayList);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(null);

        GFL.instance(this).request(URL, growersDetails, new GFL.RequestListener() {
            @Override
            public void onSuccess(String response) {
                /*deposit_progressBar.setVisibility(View.GONE);*/

                GrowersDetailsResponse growersDetailsResponse = GFL.instance(PesaUlipo.this)
                        .mGson.fromJson(response, GrowersDetailsResponse.class);
                if (growersDetailsResponse.is_successful) {
                    List<GrowerDetails> growerDetailsList = new ArrayList<>();

                    try {
                        JSONObject jsonObject = new JSONObject(response);

                        JSONArray jsonArray = jsonObject.getJSONArray("Growers");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                            JSONArray jsonArray1 = jsonObject1.getJSONArray("GrowerDetails");
                            for (int l = 0; l < jsonArray1.length(); l++) {
                                JSONObject jsonObject2 = jsonArray1.getJSONObject(l);
                                progressBar.setVisibility(View.GONE);
                                arrayList.add(String.valueOf(jsonObject2.getString("GrowerNo")));
                                /*mpesaPhoneNumber = jsonObject2.getString("phonenumber");*/
                                String gNo = jsonObject2.getString("GrowerNo");
                                String pNumber = jsonObject2.getString("GrowerMobileNo");
                                String gName = jsonObject2.getString("GrowerName");
                                SharedPrefs.write(SharedPrefs.PU_BLOCK_P_NUMBER, pNumber);
                                String isDefaulter = jsonObject2.getString("LoanDefaulter");

                                SharedPrefs.write(SharedPrefs.IS_DEFAULTER, isDefaulter);

                                growerName.setText(gName);
                                phoneNumber.setText(pNumber);
                            }

                        }

                        //Growers Number
                        spinner = (Spinner) findViewById(R.id.growerSpinner);
                        ArrayAdapter<String> adapter = new ArrayAdapter<String>(PesaUlipo.this, android.R.layout.simple_spinner_item, arrayList);
                        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                        spinner.setAdapter(adapter);


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                } else {
//                    Toast.makeText(ShareDeposit.this, "Accounts Fetching Failed", Toast.LENGTH_LONG).show();
                }

            }

            @Override
            public void onTokenExpired() {

            }

            @Override
            public void onError(String error) {

            }
        });
    }

    @Override
    protected void onResume() {
        SharedPrefs.init(getApplicationContext());
        super.onResume();
        String fpBoolean = SharedPrefs.read(SharedPrefs.FP_BOOLEAN, null);

        if (fpBoolean.equals("true")) {
            progressDialog.dismiss();
            int result = PrinterInterface.open();
            header();
            PrinterInterface.close();
            blockPu();
        } else if (fpBoolean.equals("false")){
            progressDialog.dismiss();
            Toast.makeText(this, "Please authenticate to complete transaction!", Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                finish();
                //Toast.makeText(Account.this, "Back has been presses!", Toast.LENGTH_LONG).show();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.move_left_in_activity, R.anim.move_right_out_activity);
    }
}
