package com.coretec.agencyApplication.activities.gfl;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.support.design.widget.TextInputLayout;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.cloudpos.jniinterface.PrinterInterface;
import com.coretec.agencyApplication.R;
import com.coretec.agencyApplication.activities.BaseActivity;
import com.coretec.agencyApplication.activities.newmodules.ValidateTunzhengbigBio;
import com.coretec.agencyApplication.api.GFL;
import com.coretec.agencyApplication.api.requests.FingerPrintRequest;
import com.coretec.agencyApplication.api.requests.GrowersDetailsRequest;
import com.coretec.agencyApplication.api.requests.PuActivationRequest;
import com.coretec.agencyApplication.api.responses.FingerPrintResponse;
import com.coretec.agencyApplication.api.responses.GrowersDetailsResponse;
import com.coretec.agencyApplication.api.responses.PuActivationResponse;
import com.coretec.agencyApplication.model.GrowerDetails;
import com.coretec.agencyApplication.utils.SharedPrefs;
import com.coretec.agencyApplication.wizarpos.function.printer.PrinterCommand;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static com.coretec.agencyApplication.utils.Utils.getFormattedDate;
import static com.coretec.agencyApplication.utils.Utils.getLat;
import static com.coretec.agencyApplication.utils.Utils.getLong;

public class PuActivation extends AppCompatActivity {

    ImageView backBtn;
    private TextInputLayout input_layout_id_number;
    private EditText identifier_deposit;
    Button submit;

    private Spinner spinner;
    private ArrayList<String> arrayList = new ArrayList<>();
    private ArrayAdapter<GrowerDetails> adapter;

    private String item;
    ProgressBar progressBar;
    ProgressDialog progressDialog;
String text;
    private EditText phoneNumber, fullName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.gfl_pu_activation);
        SharedPrefs.init(getApplicationContext());

        this.overridePendingTransition(R.anim.move_right_in_activity, R.anim.move_left_out_activity);

        SharedPrefs.write(SharedPrefs.FP_BOOLEAN, "");

        Toolbar toolbar = findViewById(R.id.toolbar);
        TextView mTitle = toolbar.findViewById(R.id.toolbar_title);
        mTitle.setText(getResources().getText(R.string.pu_activation));
        setSupportActionBar(toolbar);
        ActionBar actionbar = getSupportActionBar();
        actionbar.setDisplayHomeAsUpEnabled(true);
        actionbar.setHomeAsUpIndicator(R.drawable.ic_arrow_back);

        input_layout_id_number = findViewById(R.id.input_layout_id_number);
        identifier_deposit = findViewById(R.id.id_number);
        spinner = findViewById(R.id.growerSpinner);

        identifier_deposit.setInputType(InputType.TYPE_NULL);
        identifier_deposit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // showMyDialog();
            }
        });

        identifier_deposit.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    // showMyDialog();
                }
            }
        });

        progressBar = findViewById(R.id.growersPb);
        phoneNumber = findViewById(R.id.phone_number);
        phoneNumber.setSelection(phoneNumber.getText().length());
        phoneNumber.setInputType(InputType.TYPE_NULL);
        phoneNumber.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // showMyDialog();
            }
        });

        phoneNumber.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    // showMyDialog();
                }
            }
        });

        TextWatcher textWatcher1 = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                String inputPhone = phoneNumber.getText().toString();

                if (inputPhone.length() == 1) {
                    if (inputPhone.equals("0") || inputPhone.equals("7") || inputPhone.equals("+")) {
                        phoneNumber.setText("+254");
                        phoneNumber.setSelection(phoneNumber.getText().length());
                        Toast.makeText(PuActivation.this, "Correct format auto completed. Please proceed!", Toast.LENGTH_SHORT).show();
                    } else {
                        phoneNumber.setText(null);
                        Toast.makeText(PuActivation.this, "Please enter correct phone number format!", Toast.LENGTH_SHORT).show();
                    }
                }

            }
        };
        phoneNumber.addTextChangedListener(textWatcher1);

        fullName = findViewById(R.id.full_names);

        submit = findViewById(R.id.btnSubmit);
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (identifier_deposit.getText().toString().matches("") || phoneNumber.getText().toString().matches("") || fullName.getText().toString().matches("")) {
                    Toast.makeText(getBaseContext(), "Please fill in the missing fields to proceed!", Toast.LENGTH_LONG).show();
                }
                else{
                    /* progress dialog */
                    progressDialog = new ProgressDialog(PuActivation.this);
                    progressDialog.setMessage("Please wait...");
                    progressDialog.setCancelable(false);
                    progressDialog.show();

                    requestFingerPrints();

                }
            }
        });

        TextWatcher textWatcherId = new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                //here, after we introduced something in the EditText we get the string from it
                String identifier_number = identifier_deposit.getText().toString();
                if (identifier_deposit.length() >= 7) {
                    requestGetAccountDetails(identifier_number);
                }
                if (identifier_deposit.length() <= 5) {
                    fullName.setText(null);
                    phoneNumber.setText(null);
                    progressBar.setVisibility(View.GONE);
                    arrayList.clear();
                    spinner = findViewById(R.id.growerSpinner);
                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(PuActivation.this, android.R.layout.simple_spinner_item, arrayList);
                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    spinner.setAdapter(null);
                }
                if (identifier_deposit.length() >= 5 && identifier_deposit.length() <= 8) {
                    fullName.setText(null);
                    phoneNumber.setText(null);
                    arrayList.clear();
                    spinner = findViewById(R.id.growerSpinner);
                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(PuActivation.this, android.R.layout.simple_spinner_item, arrayList);
                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    spinner.setAdapter(null);
                    progressBar.setVisibility(View.VISIBLE);
                    progressBar.getIndeterminateDrawable().setColorFilter(ContextCompat.getColor(PuActivation.this, R.color.btn_green), PorterDuff.Mode.SRC_IN );
                }
//                Toast.makeText(ShareDeposit.this, accountNumber, Toast.LENGTH_SHORT).show();
//                requestAccountName(accountNumber);

            }
        };

        adapter = new ArrayAdapter<GrowerDetails>(getApplicationContext(),
                R.layout.simple_spinner_dropdown_item, new ArrayList<GrowerDetails>());
        //adapter.setDropDownViewResource(R.layout.simple_spinner_dropdown_item);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                item = parent.getItemAtPosition(position).toString();
                text = spinner.getSelectedItem().toString();

                SharedPrefs.write(SharedPrefs.PU_ACTIVATION_G_NUMBER, text);
                Log.e("GROWER NUMBER", text);
                /*if (item != null) {
                    Toast.makeText(parent.getContext(), "Selected" +position, Toast.LENGTH_SHORT).show();

                }*/
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        identifier_deposit.addTextChangedListener(textWatcherId);

    }

    void requestFingerPrints() {

        final String TAG = "GET FINGERPRINTS";
        String URL = GFL.MSACCO_AGENT + GFL.GetFourFingerPrint;

        final FingerPrintRequest fingerPrintRequest = new FingerPrintRequest();
        fingerPrintRequest.accountidentifier = identifier_deposit.getText().toString();
        fingerPrintRequest.accountidentifiercode = "1";
        fingerPrintRequest.agentid = SharedPrefs.read(SharedPrefs.AGENT_ID_1, null);
        fingerPrintRequest.terminalid = MainGFLDashboard.imei;//SharedPrefs.read(SharedPrefs.DEVICE_ID, null);
        fingerPrintRequest.longitude = getLong(this);
        fingerPrintRequest.latitude = getLat(this);
        fingerPrintRequest.date = getFormattedDate();

        Log.e(TAG, fingerPrintRequest.getBody().toString());

        GFL.instance(this).request(URL, fingerPrintRequest, new GFL.RequestListener() {
            @Override
            public void onSuccess(String response) {
                /*deposit_progressBar.setVisibility(View.GONE);*/
                //progressDialog.dismiss();
                FingerPrintResponse fingerPrintResponse = GFL.instance(PuActivation.this)
                        .mGson.fromJson(response, FingerPrintResponse.class);
                if (fingerPrintResponse.is_successful) {
                    progressDialog.dismiss();


                    try {
                        JSONObject jsonObject = new JSONObject(response);

                        String leftRing = jsonObject.getString("leftring");
                        String leftLittle = jsonObject.getString("leftlittle");
                        String rightRing = jsonObject.getString("rightring");
                        String rightLittle = jsonObject.getString("rightlittle");

                        SharedPrefs.write(SharedPrefs.LEFT_RING, leftRing);
                        SharedPrefs.write(SharedPrefs.LEFT_LITTLE, leftLittle);
                        SharedPrefs.write(SharedPrefs.RIGHT_RING, rightRing);
                        SharedPrefs.write(SharedPrefs.RIGHT_LITTLE, rightLittle);

                        Log.d("LEFT_RING", leftRing);
                        Log.d("LEFT_LITTLE", leftLittle);
                        Log.d("RIGHT_RING", rightRing);
                        Log.d("RIGHT_LITTLE", rightLittle);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    SharedPrefs.transactionstypelogs(1, PuActivation.this);
                    SharedPrefs.growerslogs(text, PuActivation.this);
                    startActivity(new Intent(PuActivation.this, ValidateTunzhengbigBio.class));
                } else {
//                    Toast.makeText(ShareDeposit.this, "Accounts Fetching Failed", Toast.LENGTH_LONG).show();
                }

            }

            @Override
            public void onTokenExpired() {

            }

            @Override
            public void onError(String error) {

            }
        });

    }

    private void submitPuActivation(){

        final String TAG = "PU ACTIVATION";
        String URL = GFL.MSACCO_AGENT + GFL.PuActivation;
        String gNumber = SharedPrefs.read(SharedPrefs.PU_ACTIVATION_G_NUMBER, null);

        final PuActivationRequest puActivation = new PuActivationRequest();
        puActivation.idno = identifier_deposit.getText().toString();
        puActivation.growersno = gNumber;
        puActivation.phoneno = phoneNumber.getText().toString();
        puActivation.fullnames = fullName.getText().toString();
        puActivation.transactiontype = "Pu Activation";
        puActivation.agentid = SharedPrefs.read(SharedPrefs.AGENT_ID_1, null);
        puActivation.terminalid = SharedPrefs.read(SharedPrefs.DEVICE_ID, null);
        puActivation.longitude = getLong(this);
        puActivation.latitude = getLat(this);
        puActivation.date = getFormattedDate();
        puActivation.corporate_no = "CAP016";

        Log.e(TAG, puActivation.getBody().toString());

        GFL.instance(this).request(URL, puActivation, new GFL.RequestListener() {
            @Override
            public void onSuccess(String response) {
                /*deposit_progressBar.setVisibility(View.GONE);*/
                progressDialog.dismiss();
                PuActivationResponse puActivationResponse = GFL.instance(PuActivation.this)
                        .mGson.fromJson(response, PuActivationResponse.class);
                if (puActivationResponse.is_successful) {

                    String idno = identifier_deposit.getText().toString();
                    String pNumber = phoneNumber.getText().toString();
                    String names = fullName.getText().toString();

                    int result = PrinterInterface.open();
                    writetest(idno, pNumber, names);
                    PrinterInterface.close();

                    try {
                        JSONObject jsonObject = new JSONObject(response);

                        String receiptNo = jsonObject.getString("receiptNo");

                        String success = "PU Activated Successfully...";
                        AlertDialog.Builder builder1 = new AlertDialog.Builder(PuActivation.this);
                        builder1.setMessage(success);
                        builder1.setTitle("Success!");
                        builder1.setCancelable(true);

                        builder1.setPositiveButton(
                                "DONE",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        Intent intent = new Intent(getApplicationContext(), MainGFLDashboard.class);
                                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                        startActivity(intent);
                                    }
                                });

                        /*builder1.setNegativeButton(
                                "Cancel",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();
                                    }
                                });*/

                        AlertDialog alert11 = builder1.create();
                        alert11.show();
                        Toast.makeText(PuActivation.this, receiptNo, Toast.LENGTH_SHORT).show();

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                } else {
//                    Toast.makeText(ShareDeposit.this, "Accounts Fetching Failed", Toast.LENGTH_LONG).show();
                }

            }

            @Override
            public void onTokenExpired() {

            }

            @Override
            public void onError(String error) {

            }
        });

    }

    public void header(){

        try {
            byte[] start = null;
            byte[] saccoName = null;
            byte[] type = null;
            //byte[] gno = null;

            try {
                start = String.valueOf("              ").getBytes("GB2312");
                saccoName = String.valueOf("      GREENLAND FEDHA LTD     ").getBytes("GB2312");
                type = String.valueOf("      PU ACTIVATION REQUEST     ").getBytes("GB2312");
                //gno = String.valueOf("Grower Number : " + String.valueOf(growerNumber)).getBytes("GB2312");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            PrinterInterface.begin();
            byte[] cmd;
            cmd = new byte[]{(byte) 0x1B, (byte) 0x40};

            cmd = new byte[]{
                    (byte) 0x1B, (byte) 0x45, (byte) 0};

            write(cmd);
            write(start);
            writeLineBreak(1);
            write(saccoName);
            writeLineBreak(2);
            write(type);
            writeLineBreak(1);
            //write(gno);
            // print line break
            //writeLineBreak(1);
            PrinterInterface.end();

        }catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void writetest(String id, String phone, String names){
        //String middle = idNos.substring(2, 6);
        String first = id.substring(0, 2);
        String last = id.substring(6, id.length());
        String maskedId = first + "****"+last;

        //Old Phone Number
        String firstOP = phone.substring(0, 7);
        String lastOP = phone.substring(11, phone.length());
        String maskedOP = firstOP + "*****"+lastOP;
        try {
            byte[] fullNames = null;
            byte[] fName = null;
            byte[] isDefa = null;
            byte[] idNumber = null;
            byte[] phoneNumber = null;
            byte[] message = null;
            //byte[] reason = null;
            byte[] time = null;
            byte[] nameAgent = null;
            byte[] motto = null;

            SimpleDateFormat timeStampFormat = new SimpleDateFormat("yyyy/MM/dd - HH:mm");
            Date myDate = new Date();
            String timeCurrent = timeStampFormat.format(myDate);
            String aName = SharedPrefs.read(SharedPrefs.AGENT_NAME, null);

            try {
                fullNames = String.valueOf("Customer full names : ").getBytes("GB2312");
                fName = String.valueOf(String.valueOf(names)).getBytes("GB2312");
                isDefa = String.valueOf("Defaulter: "+SharedPrefs.read(SharedPrefs.IS_DEFAULTER, null)).getBytes("GB2312");
                idNumber = String.valueOf("ID Number : " + String.valueOf(maskedId)).getBytes("GB2312");
                phoneNumber = String.valueOf("Phone Number : " + String.valueOf(maskedOP)).getBytes("GB2312");
                message = String.valueOf("A new PIN will be received upon            approval").getBytes("GB2312");
                //reason = String.valueOf(sababu).getBytes("GB2312");
                time = String.valueOf("   Date : " + String.valueOf(timeCurrent)).getBytes("GB2312");
                nameAgent = String.valueOf("You were served by " + String.valueOf(aName)).getBytes("GB2312");
                motto = String.valueOf("     ......Motto....      ").getBytes("GB2312");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            PrinterInterface.begin();
            byte[] cmd;
            cmd = new byte[]{(byte) 0x1B, (byte) 0x40};

            cmd = new byte[]{
                    (byte) 0x1B, (byte) 0x45, (byte) 0};

            write(cmd);
            writeLineBreak(1);
            write(fullNames);
            writeLineBreak(1);
            write(fName);
            writeLineBreak(1);
            write(isDefa);
            writeLineBreak(1);
            write(idNumber);
            writeLineBreak(1);
            write(phoneNumber);
            writeLineBreak(2);
            write(message);
            writeLineBreak(2);
            write(time);
            writeLineBreak(2);
            write(nameAgent);
            writeLineBreak(1);
            write(motto);
            // print line break
            writeLineBreak(4);
            writeLineBreak(5);
            PrinterInterface.end();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void write(final byte[] arryData) {
        int result = PrinterInterface.write(arryData, arryData.length);
    }

    // print line break
    private void writeLineBreak(int lineNumber) {
        // write(new byte[]{'\n'});
        write(PrinterCommand.getCmdEscDN(lineNumber));
    }

    void requestGetAccountDetails(String identifier) {

        final String TAG = "GrowersAccounts";
//        EasyLocationMod easyLocationMod = new EasyLocationMod(CashWithdrawal.this);

        String URL = GFL.MSACCO_AGENT + GFL.GetGrowersDetails;

        final GrowersDetailsRequest growersDetails = new GrowersDetailsRequest();
        growersDetails.agentid = SharedPrefs.read(SharedPrefs.AGENT_ID_1, null);
        growersDetails.corporateno = "CAP016";
        growersDetails.accountidentifier = identifier;
        growersDetails.accountidentifiercode = "1";
        growersDetails.transactiontype = "1";
        growersDetails.terminalid = SharedPrefs.read(SharedPrefs.DEVICE_ID, null);
        growersDetails.longitude = getLong(this);
        growersDetails.latitude = getLat(this);
        growersDetails.date = getFormattedDate();

        Log.e(TAG, growersDetails.getBody().toString());

        arrayList.clear();
        spinner = findViewById(R.id.growerSpinner);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(PuActivation.this, android.R.layout.simple_spinner_item, arrayList);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(null);

        GFL.instance(this).request(URL, growersDetails, new GFL.RequestListener() {
            @Override
            public void onSuccess(String response) {
                /*deposit_progressBar.setVisibility(View.GONE);*/

                GrowersDetailsResponse growersDetailsResponse = GFL.instance(PuActivation.this)
                        .mGson.fromJson(response, GrowersDetailsResponse.class);
                if (growersDetailsResponse.is_successful) {
                    List<GrowerDetails> growerDetailsList = new ArrayList<>();

                    try {
                        JSONObject jsonObject = new JSONObject(response);

                        JSONArray jsonArray = jsonObject.getJSONArray("Growers");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                            JSONArray jsonArray1 = jsonObject1.getJSONArray("GrowerDetails");
                            for (int l = 0; l < jsonArray1.length(); l++) {
                                JSONObject jsonObject2 = jsonArray1.getJSONObject(l);
                                progressBar.setVisibility(View.GONE);
                                arrayList.add(String.valueOf(jsonObject2.getString("GrowerNo")));

                                String gNo = jsonObject2.getString("GrowerNo");
                                String pNumber = jsonObject2.getString("GrowerMobileNo");
                                String gName = jsonObject2.getString("GrowerName");
                                //SharedPrefs.write(SharedPrefs.PHONE_NUMBER, pNumber);
                                String isDefaulter = jsonObject2.getString("LoanDefaulter");

                                SharedPrefs.write(SharedPrefs.IS_DEFAULTER, isDefaulter);

                                fullName.setText(gName);
                                phoneNumber.setText(pNumber);
                            }

                        }

                        //Growers Number
                        spinner = findViewById(R.id.growerSpinner);
                        ArrayAdapter<String> adapter = new ArrayAdapter<String>(PuActivation.this, android.R.layout.simple_spinner_item, arrayList);
                        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                        spinner.setAdapter(adapter);


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                } else {
//                    Toast.makeText(ShareDeposit.this, "Accounts Fetching Failed", Toast.LENGTH_LONG).show();
                }

            }

            @Override
            public void onTokenExpired() {

            }

            @Override
            public void onError(String error) {

            }
        });
    }

    @Override
    protected void onResume() {
        SharedPrefs.init(getApplicationContext());
        super.onResume();
        String fpBoolean = SharedPrefs.read(SharedPrefs.FP_BOOLEAN, null);

        if (fpBoolean.equals("true")) {
            progressDialog.dismiss();
            int result = PrinterInterface.open();
            header();
            PrinterInterface.close();

            submitPuActivation();
        } else if (fpBoolean.equals("false")){
            progressDialog.dismiss();
            Toast.makeText(this, "Please authenticate to complete transaction!", Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                finish();
                //Toast.makeText(Account.this, "Back has been presses!", Toast.LENGTH_LONG).show();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.move_left_in_activity, R.anim.move_right_out_activity);
    }

}
