package com.coretec.agencyApplication.activities.gfl;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.cloudpos.DeviceException;
import com.cloudpos.OperationResult;
import com.cloudpos.POSTerminal;
import com.cloudpos.TimeConstants;
import com.cloudpos.fingerprint.Fingerprint;
import com.cloudpos.fingerprint.FingerprintDevice;
import com.cloudpos.fingerprint.FingerprintOperationResult;
import com.coretec.agencyApplication.R;
import com.coretec.agencyApplication.activities.BaseActivity;
import com.coretec.agencyApplication.activities.LoginActivity;
import com.coretec.agencyApplication.api.GFL;
import com.coretec.agencyApplication.api.requests.GetMatchLogsRequest;
import com.coretec.agencyApplication.fingerprint.util.TextViewUtil;
import com.coretec.agencyApplication.utils.ByteConvertStringUtil;
import com.coretec.agencyApplication.utils.LogHelper;
import com.coretec.agencyApplication.utils.QuickToast;
import com.coretec.agencyApplication.utils.SharedPrefs;

import static com.coretec.agencyApplication.utils.Utils.getFormattedDate;
import static com.coretec.agencyApplication.utils.Utils.getLat;
import static com.coretec.agencyApplication.utils.Utils.getLong;

public class ValidateAgentTunzhengbigBio extends AppCompatActivity {

    private Button identify,dismiss;
    private TextView show;
    private Context mContext = this;
    private int userID = 0;
    private int timeout = 10 * 1000;
    private FingerprintDevice fingerprintDevice = null;

    private SharedPreferences preferences;
    private SharedPreferences.Editor editor;

    private Handler handler;
    protected Handler mHandler = null;
    private static final int SHOW_NORMAL_MESSAGE = 5;
    private static final int SHOW_SUCCESS_MESSAGE = 6;
    private static final int SHOW_FAIL_MESSAGE = 7;
    private static final int SHOW_BTN = 8;
    private static final int HIDE_BTN = 9;
    public static boolean matched;

    Thread th = null;

    private QuickToast toast = new QuickToast(this);
    boolean doubleBackToExitPressedOnce = false;

    @SuppressLint("HandlerLeak")

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState); SharedPrefs.init(getApplicationContext());
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL, WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_WATCH_OUTSIDE_TOUCH, WindowManager.LayoutParams.FLAG_WATCH_OUTSIDE_TOUCH);
        setContentView(R.layout.agent_bio_authentication_dialog);

        show = findViewById(R.id.textView);
        show.setMovementMethod(ScrollingMovementMethod.getInstance());
        identify = findViewById(R.id.btn_identify);

        dismiss = findViewById(R.id.btn_dismiss);

        /*fingerprintDevice = (FingerprintDevice) POSTerminal.getInstance(this).getDevice("cloudpos.device.fingerprint");
        try {
            fingerprintDevice.open(1);
        } catch (DeviceException e) {
            e.printStackTrace();
        }*/

        dismiss.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("WrongConstant")
            @Override
            public void onClick(View v) {
                SharedPrefs.write(SharedPrefs.FP_BOOLEAN, "false");
                finish();
                Intent i = new Intent(ValidateAgentTunzhengbigBio.this, LoginActivity.class);
                i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(i);
            }
        });

        identify.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (th == null || th.getState() == Thread.State.TERMINATED) {

                    th = new Thread() {
                        @Override
                        public void run() {
                            String finger1 = SharedPrefs.read(SharedPrefs.AGENT_LEFT_RING, null);
                            String finger2 = SharedPrefs.read(SharedPrefs.AGENT_LEFT_LITTLE, null);
                            String finger3 = SharedPrefs.read(SharedPrefs.AGENT_RIGHT_RING, null);
                            String finger4 = SharedPrefs.read(SharedPrefs.AGENT_RIGHT_LITTLE, null);
                            String finger5 = SharedPrefs.read(SharedPrefs.AGENT_LEFT_MIDDLE, null);
                            String finger6 = SharedPrefs.read(SharedPrefs.AGENT_RIGHT_MIDDLE, null);
                            String finger7 = SharedPrefs.read(SharedPrefs.AGENT_LEFT_INDEX, null);
                            String finger8 = SharedPrefs.read(SharedPrefs.AGENT_RIGHT_INDEX, null);
                            String finger9 = SharedPrefs.read(SharedPrefs.AGENT_LEFT_THUMB, null);
                            String finger10 = SharedPrefs.read(SharedPrefs.AGENT_RIGHT_THUMB, null);


                            byte[] bytes1 = ByteConvertStringUtil.hexToBytes(finger1);
                            byte[] bytes2 = ByteConvertStringUtil.hexToBytes(finger2);
                            byte[] bytes3 = ByteConvertStringUtil.hexToBytes(finger3);
                            byte[] bytes4 = ByteConvertStringUtil.hexToBytes(finger4);

                            byte[] bytes5 = ByteConvertStringUtil.hexToBytes(finger5);
                            byte[] bytes6 = ByteConvertStringUtil.hexToBytes(finger6);
                            byte[] bytes7 = ByteConvertStringUtil.hexToBytes(finger7);
                            byte[] bytes8 = ByteConvertStringUtil.hexToBytes(finger8);
                            byte[] bytes9 = ByteConvertStringUtil.hexToBytes(finger9);
                            byte[] bytes10 = ByteConvertStringUtil.hexToBytes(finger10);

                            Fingerprint fingerprint1 = new Fingerprint();
                            fingerprint1.setFeature(bytes1);
                            Fingerprint fingerprint2 = new Fingerprint();
                            fingerprint2.setFeature(bytes2);
                            Fingerprint fingerprint3 = new Fingerprint();
                            fingerprint3.setFeature(bytes3);
                            Fingerprint fingerprint4 = new Fingerprint();
                            fingerprint4.setFeature(bytes4);

                            Fingerprint fingerprint5 = new Fingerprint();
                            fingerprint5.setFeature(bytes5);
                            Fingerprint fingerprint6 = new Fingerprint();
                            fingerprint6.setFeature(bytes6);

                            Fingerprint fingerprint7 = new Fingerprint();
                            fingerprint7.setFeature(bytes7);
                            Fingerprint fingerprint8 = new Fingerprint();
                            fingerprint8.setFeature(bytes8);
                            Fingerprint fingerprint9 = new Fingerprint();
                            fingerprint9.setFeature(bytes9);
                            Fingerprint fingerprint10 = new Fingerprint();
                            fingerprint10.setFeature(bytes10);

                            match(fingerprint1, fingerprint2, fingerprint3, fingerprint4, fingerprint5, fingerprint6, fingerprint7
                                    , fingerprint8, fingerprint9, fingerprint10);
                        }
                    };
                    th.start();
                }


            }
        });
        handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                super.handleMessage(msg);
                switch (msg.what) {
                    case SHOW_NORMAL_MESSAGE:
                        sendMsg((String) msg.obj);
                        scrollLogView();
                        break;
                    case SHOW_SUCCESS_MESSAGE:
                        sendSuccessMsg((String) msg.obj);
                        scrollLogView();
                        break;
                    case SHOW_FAIL_MESSAGE:
                        sendFailMsg((String) msg.obj);
                        scrollLogView();
                        break;

                    case SHOW_BTN:
                        break;
                    case HIDE_BTN:
                        break;
                }
            }
        };
        try {
            fingerprintDevice = (FingerprintDevice) POSTerminal.getInstance(this).getDevice("cloudpos.device.fingerprint");
            fingerprintDevice.open(1);
        } catch (DeviceException e) {
            e.printStackTrace();
        }

        mHandler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                String str = msg.obj + "\n";
                switch (msg.what) {
                    case R.id.log_default:
                        show.setText(str);
                        break;
                    case R.id.log_success:
                        TextViewUtil.infoBlueTextView(show, str);
                        break;
                    case R.id.log_failed:
                        TextViewUtil.infoRedTextView(show, str);
                        break;
                    case R.id.log_clean:
                        show.setText("");
                        break;
                }
            }
        };
    }

    private void writerLogInTextview(String log, int id) {
        Message msg = new Message();
        msg.what = id;
        msg.obj = log;
        mHandler.sendMessage(msg);
    }

    private void match(Fingerprint fingerprint1, Fingerprint fingerprint2, Fingerprint fingerprint3, Fingerprint fingerprint4,
                       Fingerprint fingerprint5,Fingerprint fingerprint6,Fingerprint fingerprint7,Fingerprint fingerprint8,
                       Fingerprint fingerprint9,Fingerprint fingerprint10){

        try {
            handler.obtainMessage(SHOW_SUCCESS_MESSAGE, getString(R.string.MESSAGE)).sendToTarget();
            Fingerprint fingerprint = getFingerprint();
            int index = 0;

            if (fingerprint != null) {
                int matchResult = 0;
                try {
                    if (fingerprint1.getFeature().length > 0) {
                        matchResult = fingerprintDevice.match(fingerprint, fingerprint1);
                        index = 1;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

                try {
                    if (fingerprint2.getFeature().length > 0) {
                        matchResult = fingerprintDevice.match(fingerprint, fingerprint2);
                        index = 2;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

                try {
                    if (fingerprint3.getFeature().length > 0) {
                        matchResult = fingerprintDevice.match(fingerprint, fingerprint3);
                        index = 3;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

                try {
                    if (fingerprint4.getFeature().length > 0) {
                        matchResult = fingerprintDevice.match(fingerprint, fingerprint4);
                        index = 4;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

                try {
                    if (fingerprint5.getFeature().length > 0) {
                        matchResult = fingerprintDevice.match(fingerprint, fingerprint5);
                        index = 5;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                try {
                    if (fingerprint6.getFeature().length > 0) {
                        matchResult = fingerprintDevice.match(fingerprint, fingerprint6);
                        index = 6;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                } try {
                    if (fingerprint7.getFeature().length > 0) {
                        matchResult = fingerprintDevice.match(fingerprint, fingerprint7);
                        index = 7;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                } try {
                    if (fingerprint8.getFeature().length > 0) {
                        matchResult = fingerprintDevice.match(fingerprint, fingerprint8);
                        index = 8;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                } try {
                    if (fingerprint9.getFeature().length > 0) {
                        matchResult = fingerprintDevice.match(fingerprint, fingerprint9);
                        index = 9;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                } try {
                    if (fingerprint10.getFeature().length > 0) {
                        matchResult = fingerprintDevice.match(fingerprint, fingerprint10);
                        index = 10;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

                if (matchResult > 10) {
                    matched = true;
                    requestMatchLogs();
                    handler.obtainMessage(SHOW_SUCCESS_MESSAGE, mContext.getString(R.string.MatchSuccess) + matchResult + ",fingerPrint index = " + index).sendToTarget();
                    SharedPrefs.write(SharedPrefs.FP_BOOLEAN, "true");

                    startActivity(new Intent(ValidateAgentTunzhengbigBio.this, MainGFLDashboard.class));
                    finish();

                } else {
                    matched = true;
                    requestMatchLogs();
                    handler.obtainMessage(SHOW_FAIL_MESSAGE, mContext.getString(R.string.MatchFailed)).sendToTarget();
                    SharedPrefs.write(SharedPrefs.FP_BOOLEAN, "false");
                }
            } else {
                SharedPrefs.write(SharedPrefs.FP_BOOLEAN, "false");
            }
        } catch (Exception e) {
            e.printStackTrace();
            handler.obtainMessage(SHOW_FAIL_MESSAGE, mContext.getString(R.string.DEVICEFAILED)).sendToTarget();
            SharedPrefs.write(SharedPrefs.FP_BOOLEAN, "false");
        } finally {
            handler.obtainMessage(SHOW_BTN).sendToTarget();
        }
    }


    private Fingerprint getFingerprint() {
        Fingerprint fingerprint = null;
        try {
            FingerprintOperationResult operationResult = fingerprintDevice.waitForFingerprint(TimeConstants.FOREVER);
            if (operationResult.getResultCode() == OperationResult.SUCCESS) {
                fingerprint = operationResult.getFingerprint(0, 0);
            } else {
                handler.obtainMessage(SHOW_FAIL_MESSAGE, this.getString(R.string.FAILEDINFO)).sendToTarget();
            }
        } catch (DeviceException e) {
            e.printStackTrace();
            handler.obtainMessage(SHOW_FAIL_MESSAGE, this.getString(R.string.DEVICEFAILED)).sendToTarget();
        }
        return fingerprint;
    }

    private void sendSuccessMsg(String msg) {
        LogHelper.infoAppendMsgForSuccess(msg + "\n", show);
        scrollLogView();
    }

    private void sendFailMsg(String msg) {
        LogHelper.infoAppendMsgForFailed(msg + "\n", show);
        scrollLogView();
    }

    private void sendMsg(String msg) {
        LogHelper.infoAppendMsg(msg + "\n", show);
        scrollLogView();
    }

    public void scrollLogView() {
        int offset = show.getLineCount() * show.getLineHeight();
        if (offset > show.getHeight()) {
            show.scrollTo(0, offset - show.getHeight());
        }
    }

    private String getStr(int strId) {
        return getResources().getString(strId);
    }

    @Override
    public void onBackPressed() {

        if (doubleBackToExitPressedOnce) {
            String msg = "Are you sure you want to exit loan application? ";
            AlertDialog.Builder builder1 = new AlertDialog.Builder(ValidateAgentTunzhengbigBio.this);
            builder1.setMessage(msg);
            builder1.setTitle("Alert!");
            builder1.setCancelable(false);

            builder1.setPositiveButton(
                    "yes",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                           /* finish();
                            System.exit(0);*/

                            finish();
                            Intent i = new Intent(ValidateAgentTunzhengbigBio.this, LoginActivity.class);
                            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                            //i.addFlags(IntentCompat.FLAG_ACTIVITY_CLEAR_TASK);
                            i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(i);
                            //onBackPressed();
                        }
                    });

            builder1.setNegativeButton(
                    "CANCEL",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                            //onBackPressed();
                        }
                    });

            AlertDialog alert11 = builder1.create();
            alert11.show();
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        toast.sinfo("Press back again to exit loan application");
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                doubleBackToExitPressedOnce=false;
            }
        }, 2000);
    }
    @Override
    protected void onResume() {
        super.onResume();
    }

    private void requestMatchLogs() {

        String URL = GFL.MSACCO_AGENT + GFL.userlogs;
        final GetMatchLogsRequest matchlogsrequest = new GetMatchLogsRequest();
        matchlogsrequest.match = matched;
        matchlogsrequest.corporate_no ="CAP016";
        matchlogsrequest.transactiontype =SharedPrefs.read(String.valueOf(SharedPrefs.TRANS_TYPE_BIOLOGS), 1);
        matchlogsrequest.growerno =SharedPrefs.read((SharedPrefs.GROWERS_NUMBER_BIOLOGS), null);
        matchlogsrequest.agentid = SharedPrefs.read(SharedPrefs.AGENT_ID_1, null);
        matchlogsrequest.terminalid = SharedPrefs.read(SharedPrefs.DEVICE_TERMINAL, null);
        matchlogsrequest.longitude = getLong(this);
        matchlogsrequest.latitude = getLat(this);
        matchlogsrequest.date = getFormattedDate();


        GFL.instance(this).request(URL, matchlogsrequest, new GFL.RequestListener() {
            @Override
            public void onSuccess(String response) {

            }

            @Override
            public void onTokenExpired() {

            }

            @Override
            public void onError(String error) {

            }
        });

    }

}
