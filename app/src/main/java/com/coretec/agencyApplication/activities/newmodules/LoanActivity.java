package com.coretec.agencyApplication.activities.newmodules;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import com.coretec.agencyApplication.R;
import com.coretec.agencyApplication.activities.BaseActivity;
import com.coretec.agencyApplication.activities.LoanRepayment;
import com.coretec.agencyApplication.api.Api;
import com.coretec.agencyApplication.api.requests.FlexibleMenuRequest;
import com.coretec.agencyApplication.api.responses.FlexibleMenuResponse;
import com.coretec.agencyApplication.utils.LogOutTimerUtil;
import com.coretec.agencyApplication.utils.SharedPrefs;
import com.coretec.agencyApplication.utils.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import static com.coretec.agencyApplication.utils.Utils.getFormattedDate;
import static com.coretec.agencyApplication.utils.Utils.getLat;
import static com.coretec.agencyApplication.utils.Utils.getLong;



public class LoanActivity extends BaseActivity  implements  LogOutTimerUtil.LogOutListener{
    CardView loanApplication, loanStatus,card_loan_repayment;
    private LinearLayout liniarloanApplication,liniarloan_repayment,liniarloanStatus;
    private ProgressBar loanmenu_progressBar;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_loans);
        // setContentView(R.layout.activity_loan_request_status);

        loanApplication = findViewById(R.id.card_loan_app);
        card_loan_repayment = findViewById(R.id.card_loan_repayment);
        loanStatus = findViewById(R.id.card_loan_status);

        liniarloanApplication=findViewById(R.id.liniarloanApplication);
        liniarloan_repayment=findViewById(R.id.liniarloan_repayment);
        liniarloanStatus=findViewById(R.id.liniarloanStatus);
        loanmenu_progressBar=findViewById(R.id.loanmenu_progressBar);
        dynamicMenu();

        loanApplication.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(LoanActivity.this, LoanApplicationActivity.class));
            }
        });

        card_loan_repayment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(LoanActivity.this, LoanRepayment.class));
            }
        });
        loanStatus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(LoanActivity.this, LoanStatusActivity.class));
            }
        });
    }


    @Override
    public void onBackPressed() {
        finish();
    }

    public void dynamicMenu(){
        final FlexibleMenuRequest dynamicMenu = new FlexibleMenuRequest();
        dynamicMenu.agentid = SharedPrefs.read(SharedPrefs.AGENT_ID_1, null);
        dynamicMenu.terminalid = MainDashboardActivity.imei;
        dynamicMenu.longitude = getLong(LoanActivity.this);
        dynamicMenu.latitude = getLat(LoanActivity.this);
        dynamicMenu.date = getFormattedDate();
        dynamicMenu.corporate_no =MainDashboardActivity.corporateno;

        Api.instance(LoanActivity.this).request(Api.MSACCO_AGENT + Api.DynamicMenu, dynamicMenu, new Api.RequestListener() {
            @Override
            public void onSuccess(String response) {
                loanmenu_progressBar.setVisibility(View.GONE);
                FlexibleMenuResponse dynamicmenuresponse = Api.instance(LoanActivity.this).mGson.fromJson(response, FlexibleMenuResponse.class);
                if (dynamicmenuresponse.operationsuccess) {
                    loanmenu_progressBar.setVisibility(View.GONE);
                    try {
                        JSONObject jsonObject = new JSONObject(response);

                        String loanapplication = jsonObject.getString("loanapplication");
                        String loanrepayment = jsonObject.getString("loanrepayment");

                        String platform = jsonObject.getString("platform");
                        String endofdayreport = jsonObject.getString("eodreport");
                        String printlasttransaction = jsonObject.getString("printlasttrans");
                        String usesbio = jsonObject.getString("usesbioauthentication");

                        if (loanapplication=="true"){
                            liniarloanApplication.setVisibility(View.VISIBLE);
                        }
                        if (loanrepayment=="true"){
                            liniarloan_repayment.setVisibility(View.VISIBLE);
                        }
                        /*if (statuss=="true"){
                            liniarloanStatus.setVisibility(View.VISIBLE);
                        }*/

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    Utils.showAlertDialog(LoanActivity.this, "Failed", "Menu didn't synch correctly");

                }

            }

            @Override
            public void onTokenExpired() {

            }

            @Override
            public void onError(String error) {
                Utils.showAlertDialog(LoanActivity.this, "Failed", error);
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
        //LogOutTimerUtil.startLogoutTimer(this, this);
    }

    @Override
    public void onUserInteraction() {
        super.onUserInteraction();
        LogOutTimerUtil.startLogoutTimer(this, this);
    }


    @Override
    protected void onPause() {
        super.onPause();
        LogOutTimerUtil.startLogoutTimer(this, this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        LogOutTimerUtil.startLogoutTimer(this, this);

    }

    @Override
    public void doLogout() {
        finish();
    }
}
