package com.coretec.agencyApplication.activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.Toast;

import com.cloudpos.jniinterface.PrinterInterface;
import com.coretec.agencyApplication.R;
import com.coretec.agencyApplication.activities.newmodules.MainDashboardActivity;
import com.coretec.agencyApplication.api.Api;
import com.coretec.agencyApplication.api.requests.AccountDetailsRequest;
import com.coretec.agencyApplication.api.requests.MiniStatementRequest;
import com.coretec.agencyApplication.api.requests.RequestMemberName;
import com.coretec.agencyApplication.api.responses.AccountDetailsResponse;
import com.coretec.agencyApplication.api.responses.AccountNameResponse;
import com.coretec.agencyApplication.api.responses.MiniStatementResponse;
import com.coretec.agencyApplication.api.responses.SaccoAccounts;
import com.coretec.agencyApplication.model.AccountDetails;
import com.coretec.agencyApplication.model.Transactions;
import com.coretec.agencyApplication.utils.PreferenceFileKeys;
import com.coretec.agencyApplication.utils.QuickToast;
import com.coretec.agencyApplication.utils.SharedPrefs;
import com.coretec.agencyApplication.utils.Utils;
import com.coretec.agencyApplication.wizarpos.function.ActionCallbackImpl;
import com.coretec.agencyApplication.wizarpos.function.printer.PrinterCommand;
import com.coretec.agencyApplication.wizarpos.mvc.base.ActionCallback;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import static com.coretec.agencyApplication.utils.Utils.getFormattedDate;
import static com.coretec.agencyApplication.utils.Utils.getLat;
import static com.coretec.agencyApplication.utils.Utils.getLong;


public class MiniStatement extends BaseActivity {

    private Button btnConfirmMinistatement, btn_account_search;
    private EditText identifier_number_ministatement;
    private TextInputLayout input_layout_ministatement_phone_number;
    //    private TextInputLayout input_layout_ministatement_corporate_number;
    private ProgressBar progressMiniStatement;
    public static Handler handler;
    public static ActionCallback callback;
    private String ministatementPhoneNumber, membername;

    EditText ministatement_identifier;
    SharedPreferences sharedPreferences;

    private Spinner spinner;
    private ArrayList<String> arrayList = new ArrayList<>();
    private ArrayAdapter<AccountDetails> adapter;
    String accountNo = "";

    private ProgressBar progressBar;
    private ProgressDialog progressDialog;
    private QuickToast toast = new QuickToast(this);

    private RadioGroup radioGroupAutharization;
    public RadioButton rbpin, rbbio;
    private static String autmode;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_ministatement);

        this.overridePendingTransition(R.anim.move_right_in_activity, R.anim.move_left_out_activity);

        handler = new Handler();
        callback = new ActionCallbackImpl(handler);
        sharedPreferences = getSharedPreferences(PreferenceFileKeys.PREFS_FILE, Context.MODE_PRIVATE);
        SharedPrefs.write(SharedPrefs.FP_BOOLEAN, "");
        rbpin = findViewById(R.id.rbpin);
        rbbio = findViewById(R.id.rbbio);


        progressMiniStatement = findViewById(R.id.ministatement_progressbar);
        identifier_number_ministatement = findViewById(R.id.ministatement_identifier);

        input_layout_ministatement_phone_number = findViewById(R.id.input_layout_ministatement_identifier);

        radioGroupAutharization = findViewById(R.id.radioGroupAutharization);
        radioGroupAutharization.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (checkedId == R.id.rbpin) {
                    autmode = "1";
                    btnConfirmMinistatement.setClickable(true);
                }
                if (checkedId == R.id.rbbio) {
                    autmode = "2";
                    btnConfirmMinistatement.setClickable(true);
                }
            }
        });

        btnConfirmMinistatement = findViewById(R.id.btn_confirm_ministatement);

        String coopnumber = SharedPrefs.read(SharedPrefs.VR_COOPORATE_NO, null);
        if (coopnumber !=null){
            switch (SharedPrefs.read(SharedPrefs.VR_COOPORATE_NO, null)) {
                case "CAP005":
                    btnConfirmMinistatement.setBackgroundResource(R.drawable.btn_rounded_primary_coloraccentforcoopmis);
                    break;
                case "CAP022":
                    btnConfirmMinistatement.setBackgroundResource(R.drawable.btn_rounded_primary_coloraccentforcoopmis);
                    break;
                default:
                    btnConfirmMinistatement.setBackgroundResource(R.drawable.btn_rounded_primary_coloraccent);
                    break;
            }

        }else {
            btnConfirmMinistatement.setBackgroundResource(R.drawable.btn_rounded_primary_coloraccent);
        }
        btnConfirmMinistatement.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ministatementPhoneNumber = identifier_number_ministatement.getText().toString().trim();

                if (ministatementPhoneNumber.isEmpty()) {
                    input_layout_ministatement_phone_number.setError("Enter Customer Account Number ");
                } else {

                    if (rbpin.isChecked() || rbbio.isChecked()) {
                        progressMiniStatement.setVisibility(View.VISIBLE);
                        progressDialog = new ProgressDialog(MiniStatement.this);
                        progressDialog.setMessage("Processing your request, please wait...");
                        progressDialog.setCancelable(false);
                        progressDialog.show();

                        if (spinner != null && spinner.getSelectedItem() != null) {
                            switch (autmode) {
                                case "1":
                                    requestMiniStatement();
                                    break;
                                case "2":
                                    String identity = identifier_number_ministatement.getText().toString();
                                    requestFingerPrints(MiniStatement.this, identity, progressMiniStatement);
                                    break;
                                default:
                                    requestMiniStatement();
                            }
                        } else {
                            progressDialog.dismiss();
                            progressMiniStatement.setVisibility(View.GONE);
                            Toast.makeText(MiniStatement.this, "Search account details", Toast.LENGTH_SHORT).show();
                        }

                        btnConfirmMinistatement.setActivated(false);

                    } else {
                        Toast.makeText(MiniStatement.this, "Please select authentication method", Toast.LENGTH_SHORT).show();
                    }
                }

            }
        });

        progressBar = findViewById(R.id.idiPb);
        spinner = findViewById(R.id.withdrawal_accounts);
        adapter = new ArrayAdapter<AccountDetails>(getApplicationContext(),
                R.layout.simple_spinner_dropdown_item, new ArrayList<AccountDetails>());
        adapter.setDropDownViewResource(R.layout.simple_spinner_dropdown_item);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                AccountDetails accountDetails = (AccountDetails) parent.getItemAtPosition(position);
                accountNo = accountDetails.getAccountno();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        ministatement_identifier = findViewById(R.id.ministatement_identifier);

        btn_account_search = findViewById(R.id.btn_account_search);
        btn_account_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String identifier = ministatement_identifier.getText().toString();

                if (ministatement_identifier.getText().toString().matches("")) {
                    toast.swarn("Please the identifier number to proceed!");
                } else {
                    progressBar.setVisibility(View.VISIBLE);
                    requestMemberDetailsById(identifier_number_ministatement.getText().toString());
                    requestGetAccountDetails(identifier);
                }
            }
        });


        ministatement_identifier.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

                adapter = new ArrayAdapter<AccountDetails>(getApplicationContext(),
                        R.layout.simple_spinner_dropdown_item, new ArrayList<AccountDetails>());
                adapter.setDropDownViewResource(R.layout.simple_spinner_dropdown_item);
                adapter.clear();
                spinner.setAdapter(adapter);

            }
        });

    }

    void requestGetAccountDetails(String identifier) {

        final String TAG = "MemberAccounts";
//        EasyLocationMod easyLocationMod = new EasyLocationMod(CashWithdrawal.this);

        String URL = Api.MSACCO_AGENT + Api.GetAllMemberSaccoDetails;

        final AccountDetailsRequest accountDetails = new AccountDetailsRequest();
        accountDetails.agentid = sharedPreferences.getString(PreferenceFileKeys.AGENT_ID, "");
        accountDetails.corporateno = MainDashboardActivity.corporateno;
        accountDetails.accountidentifier = identifier;
        accountDetails.accountidentifiercode = "0";
        accountDetails.transactiontype = "M";
        accountDetails.terminalid = MainDashboardActivity.imei;
        accountDetails.longitude = getLong(this);
        accountDetails.latitude = getLat(this);
        accountDetails.date = getFormattedDate();

        Log.e(TAG, accountDetails.getBody().toString());

        Api.instance(this).request(URL, accountDetails, new Api.RequestListener() {
            @Override
            public void onSuccess(String response) {


                AccountDetailsResponse accountDetailsResponse = Api.instance(MiniStatement.this)
                        .mGson.fromJson(response, AccountDetailsResponse.class);
                if (accountDetailsResponse.is_successful) {
                    progressBar.setVisibility(View.GONE);
                    List<AccountDetails> accountDetailsList = new ArrayList<>();
                    for (SaccoAccounts saccoAccounts : accountDetailsResponse.getSaccoAccounts()) {
                        accountDetailsList.addAll(saccoAccounts.getAccountDetails());
                    }
                    adapter.addAll(accountDetailsList);
                    spinner.setAdapter(adapter);

                } else {
//                    Toast.makeText(MiniStatement.this, "Accounts Fetching Failed", Toast.LENGTH_LONG).show();
                }

                try {
                    JSONObject jsonObject = new JSONObject(response);

                    JSONArray jsonArray = jsonObject.getJSONArray("saccoaccounts");
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                        JSONArray jsonArray1 = jsonObject1.getJSONArray("accountdetails");
                        for (int l = 0; l < jsonArray1.length(); l++) {
                            JSONObject jsonObject2 = jsonArray1.getJSONObject(l);
                            arrayList.add(String.valueOf(jsonObject2.getString("accountno") + " " + jsonObject2.getString("accounttype")));
                            // mpesaPhoneNumber = jsonObject2.getString("phonenumber");
                        }

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void onTokenExpired() {

            }

            @Override
            public void onError(String error) {

            }
        });
    }


    public void requestMiniStatement() {
        final String TAG = "request ministatement";

        String URL = Api.MSACCO_AGENT + Api.GetMiniStatement;

        final Timer t = new Timer();
        t.schedule(new TimerTask() {
            @Override
            public void run() {
                progressDialog.dismiss();
                t.cancel();
            }
        }, 6000);

        MiniStatementRequest request = new MiniStatementRequest();
        request.corporate_no = MainDashboardActivity.corporateno;
        request.accountidentifier = identifier_number_ministatement.getText().toString();
        request.accountidentifiercode = "0";
        request.accountno = accountNo;
        request.agentid = sharedPreferences.getString(PreferenceFileKeys.AGENT_ID, "");
        request.terminalid = MainDashboardActivity.imei;
        request.longitude = getLong(this);
        request.latitude = getLat(this);
        request.date = getFormattedDate();
        Log.e(TAG, request.getBody().toString());

        Api.instance(this).request(URL, request, new Api.RequestListener() {
            @Override
            public void onSuccess(String response) {
                progressMiniStatement.setVisibility(View.GONE);

                final MiniStatementResponse miniStatementResponse = Api.instance(MiniStatement.this).mGson.fromJson(response, MiniStatementResponse.class);

                if (miniStatementResponse.is_successful) {

                    final String agentName = sharedPreferences.getString(PreferenceFileKeys.AGENT_ACCOUNT_NAME, "");
                    final String saccoMotto = sharedPreferences.getString(PreferenceFileKeys.AGENT_SACCO_MOTTO, "");

                    if (PrinterInterface.queryStatus() == 0) {
                        Utils.showAlertDialog(MiniStatement.this, "NO THERMAL PAPER!", "Put thermal paper and click OK to continue.", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                                Utils.showAlertDialog(MiniStatement.this, "MiniStatement", "Mini statement Retrieved Successfully, click OK to print", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        PrinterInterface.open();
                                        writetest(agentName, miniStatementResponse.getTransactions(), miniStatementResponse.getSacconame(), saccoMotto, membername, miniStatementResponse.getReceiptno(), miniStatementResponse.getTransactiondate(), miniStatementResponse.getTransactiontype());
                                        progressDialog.dismiss();
                                        finish();
                                        Intent i = new Intent(MiniStatement.this, MainDashboardActivity.class);
                                        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                                        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                        startActivity(i);
                                    }
                                });

                            }
                        });
                        PrinterInterface.close();

                    } else {
                        Utils.showAlertDialog(MiniStatement.this, "MiniStatement", "Mini statement Retrieved Successfully, click OK to print", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                PrinterInterface.open();
                                writetest(agentName, miniStatementResponse.getTransactions(), miniStatementResponse.getSacconame(), saccoMotto, membername, miniStatementResponse.getReceiptno(), miniStatementResponse.getTransactiondate(), miniStatementResponse.getTransactiontype());
                                progressDialog.dismiss();
                                finish();
                                Intent i = new Intent(MiniStatement.this, MainDashboardActivity.class);
                                i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                                i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(i);
                            }
                        });
                        PrinterInterface.close();
                    }

                } else {
                    progressDialog.dismiss();
                    Utils.showAlertDialog(MiniStatement.this, "MiniStatement Failed", miniStatementResponse.getError());
                }

            }

            @Override
            public void onTokenExpired() {

            }

            @Override
            public void onError(String error) {
                Toast.makeText(MiniStatement.this, "An error occurred!", Toast.LENGTH_SHORT).show();
            }
        });
    }
    void requestMemberDetailsById(String identifier) {
        RequestMemberName requestMemberName = new RequestMemberName();
        requestMemberName.corporateno = MainDashboardActivity.corporateno;
        requestMemberName.accountidentifier = identifier;//identifier_deposit.getText().toString();
        requestMemberName.accountcode = "0";
        requestMemberName.agentid = SharedPrefs.read(SharedPrefs.AGENT_ID_1, null);
        requestMemberName.terminalid = MainDashboardActivity.imei;
        Api.instance(MiniStatement.this).request(Api.MSACCO_AGENT + Api.GetMembername, requestMemberName, new Api.RequestListener() {
            @Override
            public void onSuccess(String response) {
                progressBar.setVisibility(View.GONE);
                AccountNameResponse accountNameResponse = Api.instance(MiniStatement.this).mGson.fromJson(response, AccountNameResponse.class);
                if (accountNameResponse.is_successful) {
                    //deposit_account_name.setText(accountNameResponse.getCustomer_name());
                    membername=accountNameResponse.getCustomer_name();
                } else {

                    Toast.makeText(MiniStatement.this, "No Account Fetched", Toast.LENGTH_LONG).show();
                }

            }

            @Override
            public void onTokenExpired() {

            }

            @Override
            public void onError(String error) {

            }
        });
    }

    public void FingerprintAuthenticatedMiniStatement() {
        final String TAG = "request ministatement";

        String URL = Api.MSACCO_AGENT + Api.GetMiniStatement;

        final Timer t = new Timer();
        t.schedule(new TimerTask() {
            @Override
            public void run() {
                progressDialog.dismiss();
                t.cancel();
            }
        }, 6000);

        MiniStatementRequest request = new MiniStatementRequest();
        request.corporate_no = MainDashboardActivity.corporateno;
        request.accountidentifier = identifier_number_ministatement.getText().toString();
        request.accountidentifiercode = "0";
        request.accountno = accountNo;
        request.agentid = sharedPreferences.getString(PreferenceFileKeys.AGENT_ID, "");
        request.terminalid = MainDashboardActivity.imei;
        request.longitude = getLong(this);
        request.latitude = getLat(this);
        request.date = getFormattedDate();

        Log.e(TAG, request.getBody().toString());

        Api.instance(this).bioauthrequest(URL, request, new Api.RequestListener() {
            @Override
            public void onSuccess(String response) {
                Log.e(TAG, response);
                progressMiniStatement.setVisibility(View.GONE);

                final MiniStatementResponse miniStatementResponse = Api.instance(MiniStatement.this).mGson.fromJson(response, MiniStatementResponse.class);

                if (miniStatementResponse.is_successful) {

                    final String agentName = sharedPreferences.getString(PreferenceFileKeys.AGENT_ACCOUNT_NAME, "");
                    final String saccoMotto = sharedPreferences.getString(PreferenceFileKeys.AGENT_SACCO_MOTTO, "");

                    if (PrinterInterface.queryStatus() == 0) {
                        Utils.showAlertDialog(MiniStatement.this, "NO THERMAL PAPER!", "Put thermal paper and click OK to continue.", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                                Utils.showAlertDialog(MiniStatement.this, "MiniStatement", "Mini statement Retrieved Successfully, click OK to print.", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        PrinterInterface.open();
                                        writetest(agentName, miniStatementResponse.getTransactions(), miniStatementResponse.getSacconame(), saccoMotto, membername, miniStatementResponse.getReceiptno(), miniStatementResponse.getTransactiondate(), miniStatementResponse.getTransactiontype());

                                        progressDialog.dismiss();

                                        finish();
                                        Intent i = new Intent(MiniStatement.this, MainDashboardActivity.class);
                                        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                                        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                        startActivity(i);
                                    }
                                });
                            }
                        });
                        PrinterInterface.close();

                    } else {
                        Utils.showAlertDialog(MiniStatement.this, "MiniStatement", "Mini statement Retrieved Successfully, click OK to print", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                PrinterInterface.open();
                                writetest(agentName, miniStatementResponse.getTransactions(), miniStatementResponse.getSacconame(), saccoMotto, membername, miniStatementResponse.getReceiptno(), miniStatementResponse.getTransactiondate(), miniStatementResponse.getTransactiontype());
                                progressDialog.dismiss();

                                finish();
                                Intent i = new Intent(MiniStatement.this, MainDashboardActivity.class);
                                i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                                i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(i);
                            }
                        });
                        PrinterInterface.close();
                    }
                } else {
                    progressDialog.dismiss();
                    Utils.showAlertDialog(MiniStatement.this, "MiniStatement Failed", miniStatementResponse.getError());
                }

            }

            @Override
            public void onTokenExpired() {

            }

            @Override
            public void onError(String error) {
                Toast.makeText(MiniStatement.this, "An error occurred!", Toast.LENGTH_SHORT).show();
            }
        });
    }


    public void writetest(String agentName, List<Transactions> transactionsList, String saccoame, String saccoMotto, String customerName, String receiptNumber, String dateed, String transactionType) {
        try {

            byte[] arrySaccoName = null;
            byte[] arryCustomerName = null;
            byte[] arryDepositAmt = null;
            byte[] arryDate = null;
            byte[] arryDescription = null;
            byte[] arryAmount = null;

            byte[] arryMotto = null;
            byte[] terminalNo = null;
            byte[] receiptNo = null;
            byte[] date = null;
            byte[] transactionTYpe = null;
            byte[] servedBy = null;
            byte[] signature = null;
            byte[] id = null;


            try {
                arrySaccoName = String.valueOf("       " + String.valueOf(saccoame)).getBytes("GB2312");
                transactionTYpe = String.valueOf("        " + transactionType).getBytes("GB2312");

                terminalNo = String.valueOf(" Terminal ID :" + Utils.getIMEI(MiniStatement.this)).getBytes("GB2312");
                receiptNo = String.valueOf(" Receipt Number :" + receiptNumber).getBytes("GB2312");
                date = String.valueOf("Date : " + dateed).getBytes("GB2312");
                arryCustomerName = String.valueOf("Name :" + customerName).getBytes("GB2312");

                arryDepositAmt = String.valueOf("  ").getBytes("GB2312");
                id = String.valueOf("ID :_________________________").getBytes("GB2312");
                signature = String.valueOf("SIGNATURE : _____________").getBytes("GB2312");
                servedBy = String.valueOf(" You were Served By : " + agentName).getBytes("GB2312");
                arryMotto = String.valueOf("     " + saccoMotto).getBytes("GB2312");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            PrinterInterface.begin();

            byte[] cmd;
            cmd = new byte[]{(byte) 0x1B, (byte) 0x40};

            cmd = new byte[]{
                    (byte) 0x1B, (byte) 0x45, (byte) 0};

            write(cmd);
            write(arrySaccoName);
            writeLineBreak(1);
            write(transactionTYpe);
            writeLineBreak(2);
            write(terminalNo);
            writeLineBreak(1);
            write(receiptNo);
            writeLineBreak(1);
            write(date);
            writeLineBreak(1);
            write(arryCustomerName);
            writeLineBreak(1);


            write(arryDepositAmt);
            writeLineBreak(1);

            for (int i = 0; i < transactionsList.size(); i++) {
                arryDate = String.valueOf(transactionsList.get(i).getPosting_date()).getBytes("GB2312");
                write(arryDate);

                arryAmount = String.valueOf("  " + transactionsList.get(i).getAmount()).getBytes("GB2312");
                write(arryAmount);

                arryDescription = String.valueOf("  " + transactionsList.get(i).getDescription()).getBytes("GB2312");
                write(arryDescription);
                writeLineBreak(1);

            }

            writeLineBreak(1);
            write(id);
            writeLineBreak(2);
            write(signature);
            writeLineBreak(1);
            write(servedBy);
            writeLineBreak(1);
            write(arryMotto);
            writeLineBreak(4);
            PrinterInterface.end();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void write(final byte[] arryData) {
        int result = PrinterInterface.write(arryData, arryData.length);
    }

    // print line break
    private void writeLineBreak(int lineNumber) {
        // write(new byte[]{'\n'});
        write(PrinterCommand.getCmdEscDN(lineNumber));
    }




    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.move_left_in_activity, R.anim.move_right_out_activity);
    }


    @Override
    protected void onResume() {
        SharedPrefs.init(getApplicationContext());
        super.onResume();
        String fpBoolean = SharedPrefs.read(SharedPrefs.FP_BOOLEAN, null);

        if (fpBoolean.equals("true")) {
            FingerprintAuthenticatedMiniStatement();
        } else if (fpBoolean.equals("false")) {
            toast.swarn("For biometrics Authentication, Customer's fingerprints shall be required!");
        }
    }
}
