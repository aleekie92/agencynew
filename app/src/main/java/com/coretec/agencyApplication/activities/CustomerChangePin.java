package com.coretec.agencyApplication.activities;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.graphics.Rect;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.animation.DecelerateInterpolator;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;


import com.coretec.agencyApplication.R;
import com.coretec.agencyApplication.activities.newmodules.MainDashboardActivity;
import com.coretec.agencyApplication.api.Api;
import com.coretec.agencyApplication.api.requests.MemberActivationRequest;
import com.coretec.agencyApplication.api.requests.RequestMemberName;
import com.coretec.agencyApplication.api.requests.SendTextMessageRequest;
import com.coretec.agencyApplication.api.requests.SignaturePhotoRequest;
import com.coretec.agencyApplication.api.responses.MemberActivationResponse;
import com.coretec.agencyApplication.api.responses.MemberNameResponse;
import com.coretec.agencyApplication.api.responses.SendTextMessageResponse;
import com.coretec.agencyApplication.utils.PreferenceFileKeys;
import com.coretec.agencyApplication.utils.QuickToast;
import com.coretec.agencyApplication.utils.SharedPrefs;
import com.coretec.agencyApplication.utils.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;

import static com.coretec.agencyApplication.utils.Utils.getFormattedDate;
import static com.coretec.agencyApplication.utils.Utils.getLat;
import static com.coretec.agencyApplication.utils.Utils.getLong;
public class CustomerChangePin extends BaseActivity {

  private TextInputLayout input_layout_activation_identifier;
  private TextInputLayout layout_old_pin;
  private TextInputLayout layout_new_pin;
  private TextInputLayout input_otp;
  private EditText activation;
  private EditText activation_confirm_pin;
  private EditText activation_new_pin;
  private Button submitActivationRequest,btn_account_search;
  private Button btnProceed;
  private String identifierCode;
  private TextView activation_account_name;
  private TextView activation_phone_number;
  private TextView activation_message;
  private Spinner activation_spinner;
  private ArrayAdapter<String> changePinSpinnerAdapter;
  private ArrayList<String> arraIdentifierList = new ArrayList<>();
  private ProgressBar pin_progressbar;
  SharedPreferences sharedPreferences;
  private ProgressBar progressBar,pbphotos;
  private ProgressDialog progressDialog;
  String code,randomCode, plainCode;
  boolean find=true;
  String identifier,new_pin,confirm_pin;
  private QuickToast toast = new QuickToast(this);

  LinearLayout pic_layout;
  private ImageView picture;
  private ImageView IVsignature;

  private Animator currentAnimator;
  private int shortAnimationDuration;
  boolean doubleBackToExitPressedOnce = false;

  @Override
  protected void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    SharedPrefs.init(getApplicationContext());

    setContentView(R.layout.change_customer_pin);

    this.overridePendingTransition(R.anim.move_right_in_activity, R.anim.move_left_out_activity);
    sharedPreferences = getSharedPreferences(PreferenceFileKeys.PREFS_FILE, Context.MODE_PRIVATE);

    activation_account_name = findViewById(R.id.activation_account_name);
    activation_phone_number = findViewById(R.id.activation_phone_number);
    pin_progressbar = findViewById(R.id.pin_progressbar);
    activation_spinner = findViewById(R.id.activation_spinner);
    progressBar = findViewById(R.id.statusPb);

    input_layout_activation_identifier = findViewById(R.id.input_layout_activation_identifier);
    input_otp = findViewById(R.id.input_layout_otp);
    layout_old_pin = findViewById(R.id.input_layout_change_old_pin);
    layout_new_pin = findViewById(R.id.input_layout_change_confirm_pin);

    activation = findViewById(R.id.activation_identifier);
    activation_new_pin = findViewById(R.id.activation_new_pin);
    activation_confirm_pin = findViewById(R.id.activation_confirm_pin);

    activation_message = findViewById(R.id.activation_msg);
    pbphotos = findViewById(R.id.pbphotos);
    pic_layout = findViewById(R.id.pictures_layout);
    picture = findViewById(R.id.picture_imageview);
    IVsignature = findViewById(R.id.signature_imageview);

    btn_account_search = findViewById(R.id.btn_account_search);
    btn_account_search.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        String accountNumber = activation.getText().toString();

        if(activation.getText().toString().matches("")) {
          toast.swarn("Please the ID number to proceed!");
        } else {
          progressBar.setVisibility(View.VISIBLE);
          requestAccountName(accountNumber);
          pbphotos.setVisibility(View.VISIBLE);
          getPicture();
        }
      }
    });

    TextWatcher textWatcher = new TextWatcher() {

      @Override
      public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

      }

      @Override
      public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

      }

      @Override
      public void afterTextChanged(Editable editable) {

        activation_account_name.setText("*** ***");
        activation_phone_number.setText("*****");
      }
    };

    activation.addTextChangedListener(textWatcher);

    submitActivationRequest = findViewById(R.id.submitActivationRequest);
    btnProceed = findViewById(R.id.proceedBtn);

    String coopnumber = SharedPrefs.read(SharedPrefs.VR_COOPORATE_NO, null);
    if (coopnumber !=null){
      switch (SharedPrefs.read(SharedPrefs.VR_COOPORATE_NO, null)) {
        case "CAP005":
          btnProceed.setBackgroundResource(R.drawable.btn_rounded_primary_coloraccentforcoopmis);
          submitActivationRequest.setBackgroundResource(R.drawable.btn_rounded_primary_coloraccentforcoopmis);
          break;
        case "CAP022":
          btnProceed.setBackgroundResource(R.drawable.btn_rounded_primary_coloraccentforcoopmis);
          submitActivationRequest.setBackgroundResource(R.drawable.btn_rounded_primary_coloraccentforcoopmis);
          break;
        default:
          btnProceed.setBackgroundResource(R.drawable.btn_rounded_primary_coloraccent);
          submitActivationRequest.setBackgroundResource(R.drawable.btn_rounded_primary_coloraccent);
          break;
      }

    }else {
      btnProceed.setBackgroundResource(R.drawable.btn_rounded_primary_coloraccent);
      submitActivationRequest.setBackgroundResource(R.drawable.btn_rounded_primary_coloraccent);
    }

    btnProceed.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {

        try {
          if (activation.getText().toString().matches("") ||
                  activation.getText().toString().length() < 6 ||
                  activation_phone_number.getText().toString().matches("")) {
            Toast.makeText(getApplicationContext(), "Enter valid ID number!", Toast.LENGTH_LONG).show();
          } /*else if (activation_phone_number.getText().toString().matches("")) {
                        Toast.makeText(getApplicationContext(), "Enter phone  number to proceed!", Toast.LENGTH_LONG).show();
                    } */else {
            input_otp.setVisibility(View.VISIBLE);
            generatePINAndSend(activation_phone_number.getText().toString());
          }
        } catch (Exception e){
          e.printStackTrace();
          Toast.makeText(CustomerChangePin.this, "Something went wrong. Please try again!", Toast.LENGTH_SHORT).show();
        }
      }
    });

    TextWatcher confirmationMessage = new TextWatcher() {

      @Override
      public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

      }

      @Override
      public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

      }

      @Override
      public void afterTextChanged(Editable editable) {
        //here, after we introduced something in the EditText we get the string from it
        String confirmation = activation_message.getText().toString();

        if (confirmation.length() >= 4) {
//                    Toast.makeText(CustomerActivation.this, accountNumber, Toast.LENGTH_SHORT).show();
          if (confirmation.equals(SharedPrefs.read(SharedPrefs.SMS_CODE, null))) {
            layout_old_pin.setVisibility(View.VISIBLE);
            layout_new_pin.setVisibility(View.VISIBLE);
            btnProceed.setVisibility(View.GONE);
            submitActivationRequest.setVisibility(View.VISIBLE);
          } else {
            Toast.makeText(getApplicationContext(), "Invalid verification code! Please try again", Toast.LENGTH_SHORT).show();
            layout_old_pin.setVisibility(View.GONE);
            layout_new_pin.setVisibility(View.GONE);
          }
        }

      }
    };

    activation_message.addTextChangedListener(confirmationMessage);

    submitActivationRequest.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
         identifier = activation.getText().toString().trim();
         new_pin = activation_new_pin.getText().toString().trim();
         confirm_pin = activation_confirm_pin.getText().toString().trim();

        if (identifier.isEmpty()) {
          input_layout_activation_identifier.setError("Enter identifier ");
        } else if (new_pin.isEmpty()) {
          layout_new_pin.setError("Enter New Pin ");
        } else if (confirm_pin.isEmpty()) {
          layout_old_pin.setError("Confirm Pin ");
        }
        else if (new_pin.length() < 4 || confirm_pin.length() < 4){
          Toast.makeText(CustomerChangePin.this, "Input a 4 digit pin", Toast.LENGTH_SHORT).show();
        }
        else {
          progressDialog = new ProgressDialog(CustomerChangePin.this);
          progressDialog.setMessage("Processing your request, please wait...");
          progressDialog.setCancelable(false);
          progressDialog.show();

          submittransaction(find);
          //requestChangePIN(identifier, new_pin, confirm_pin);
          submitActivationRequest.setActivated(false);

          // memberAuthDialog();
        }

      }
    });

  }

  public  void submittransaction(boolean value){
    if(value) {
      //Toast.makeText(MainActivity.this, "success", Toast.LENGTH_LONG).show();
      try {

        if (isOnline()) {
          requestChangePIN(identifier, new_pin, confirm_pin);
          //Toast.makeText(MainActivity.this, "Checking new collection", Toast.LENGTH_LONG).show();
        } else {
          Toast.makeText(CustomerChangePin.this, "Check your Internet Connection and try again", Toast.LENGTH_LONG).show();
          progressDialog.dismiss();
        }


        //checkConnection();
      } catch (Exception e) {
        e.printStackTrace();
      }
    }
  }

  protected boolean isOnline() {
    ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
    NetworkInfo netInfo = cm.getActiveNetworkInfo();
    if (netInfo != null && netInfo.isConnectedOrConnecting()) {
      return true;
    } else {
      return false;
    }
  }
  public void generatePINAndSend(String phoneNumber) {
    String name = activation_account_name.getText().toString();
    //generate a 4 digit integer 1000 <10000
    int randomPIN = (int)(Math.random()*9000)+1000;
    randomCode ="Hi "+name+", your new PIN is "+ String.valueOf(randomPIN) +" . Enter this 4-digit PIN to proceed. Do not share this with anyone\nThank you.";
    plainCode = String.valueOf(randomPIN);
    SharedPrefs.write(SharedPrefs.SMS_CODE, String.valueOf(randomPIN));
    System.out.println("##"+plainCode);

    //put api request and response here
    final String TAG = "MPESA CHANGE";
    String URL = Api.MSACCO_AGENT + Api.SendTextMessage;

    final SendTextMessageRequest textMessageRequest = new SendTextMessageRequest();
    textMessageRequest.corporateno = MainDashboardActivity.corporateno;
    textMessageRequest.telephone = phoneNumber;
    textMessageRequest.text = randomCode;
    textMessageRequest.agentid = sharedPreferences.getString(PreferenceFileKeys.AGENT_ID, "");
    textMessageRequest.terminalid = MainDashboardActivity.imei;
    textMessageRequest.longitude = getLong(this);
    textMessageRequest.latitude = getLat(this);
    textMessageRequest.date = getFormattedDate();
    //textMessageRequest.corporate_no = "CAP016";

    Log.e(TAG, textMessageRequest.getBody().toString());

    Api.instance(this).request(URL, textMessageRequest, new Api.RequestListener() {
      @Override
      public void onSuccess(String response) {
        /*deposit_progressBar.setVisibility(View.GONE);*/
        // progressDialog.dismiss();
        SendTextMessageResponse textMessageResponse = Api.instance(CustomerChangePin.this)
                .mGson.fromJson(response, SendTextMessageResponse.class);
        if (textMessageResponse.is_successful) {
          //progressDialog.dismiss();
          try {
            JSONObject jsonObject = new JSONObject(response);

            //String receiptNo = jsonObject.getString("receiptNo");
            //Toast.makeText(MemberActivation.this, receiptNo, Toast.LENGTH_SHORT).show();

          } catch (JSONException e) {
            e.printStackTrace();
          }

        } else {
          Toast.makeText(CustomerChangePin.this, "Operation Failed, please try again", Toast.LENGTH_LONG).show();
        }

      }

      @Override
      public void onTokenExpired() {

      }

      @Override
      public void onError(String error) {

      }
    });

  }

  void requestAccountName(final String accountNumber) {

    String URL = Api.MSACCO_AGENT + Api.GetMembername;

    RequestMemberName requestMemberName = new RequestMemberName();
    requestMemberName.corporateno = MainDashboardActivity.corporateno;
    requestMemberName.accountidentifier = accountNumber;
    requestMemberName.accountcode = "0";
    requestMemberName.agentid = sharedPreferences.getString(PreferenceFileKeys.AGENT_ID, "");
    requestMemberName.terminalid = MainDashboardActivity.imei;

    Api.instance(this).request(URL, requestMemberName, new Api.RequestListener() {
      @Override
      public void onSuccess(String response) {
        pin_progressbar.setVisibility(View.GONE);
        Log.e("requestMemberName", response);
//                displayBalance(response);

        MemberNameResponse memberNameResponse = Api.instance(CustomerChangePin.this).mGson.fromJson(response, MemberNameResponse.class);
        if (memberNameResponse.is_successful) {
          progressBar.setVisibility(View.GONE);
          activation_account_name.setVisibility(View.VISIBLE);
          activation_phone_number.setVisibility(View.VISIBLE);
          //activation_phone_number.setText("+254712836398");
          activation_account_name.setText(memberNameResponse.getCustomer_name());
          activation_phone_number.setText(memberNameResponse.getPhoneno());
          if (memberNameResponse.getPhoneno().length()<1){
            //Toast.makeText(MemberActivation.this, "Could not get phone number. Enter manually", Toast.LENGTH_LONG).show();
            activation_phone_number.setText("Could not get phone number");
            activation_phone_number.setTextColor(getResources().getColor(R.color.colorPrimary));
          }

          if (accountNumber.length() == 8) {
            if (memberNameResponse.getCustomer_name().length() <1) {
              Toast.makeText(getApplicationContext(), "Member has not registered!", Toast.LENGTH_SHORT).show();
            }
          }
                    /*if (memberNameResponse.getHas_pin()) {
                        String mesg = "Customer already has a PIN. If forgotten contact support for help";
                        AlertDialog.Builder builder1 = new AlertDialog.Builder(CustomerChangePin.this);
                        builder1.setMessage(mesg);
                        builder1.setTitle("Alert!");
                        builder1.setCancelable(true);

                        builder1.setPositiveButton(
                                "DONE",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();
                                        startActivity(new Intent(CustomerChangePin.this, FpActivity.class));
                                        finish();
                                    }
                                });

                    *//*builder1.setNegativeButton(
                            "Cancel",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                }
                            });*//*

                        AlertDialog alert11 = builder1.create();
                        alert11.show();
                    }*/


        } else {
          progressBar.setVisibility(View.GONE);
          activation_account_name.setVisibility(View.GONE);
          Toast.makeText(CustomerChangePin.this, "Failed", Toast.LENGTH_LONG).show();
        }

      }

      @Override
      public void onTokenExpired() {

      }

      @Override
      public void onError(String error) {

      }
    });

  }

  void requestChangePIN(String identifier, String new_pin, String confirm_pin) {
    final String TAG = "request change pin";
    Log.e(TAG, identifier);

    String URL = Api.MSACCO_AGENT + Api.MemberActivation;

    MemberActivationRequest memberActivationRequest = new MemberActivationRequest();
    memberActivationRequest.corporate_no = MainDashboardActivity.corporateno;
    memberActivationRequest.accountidentifier = identifier;
    memberActivationRequest.newpin = new_pin;
    memberActivationRequest.confirmpin = confirm_pin;
    memberActivationRequest.accountidentifiercode = "0";
    memberActivationRequest.agentid = sharedPreferences.getString(PreferenceFileKeys.AGENT_ID, "");
    memberActivationRequest.terminalid = MainDashboardActivity.imei;
    memberActivationRequest.longitude = getLong(this);
    memberActivationRequest.latitude = getLat(this);
    memberActivationRequest.date = getFormattedDate();

    Log.e(TAG, memberActivationRequest.getBody().toString());

    Api.instance(this).request(URL, memberActivationRequest, new Api.RequestListener() {
      @Override
      public void onSuccess(String response) {
        //activation_spinner.setVisibility(View.GONE);
        Log.e(TAG, response);

        MemberActivationResponse memberActivationResponse = Api.instance(CustomerChangePin.this).mGson.fromJson(response, MemberActivationResponse.class);
        if (memberActivationResponse.is_successful) {
          progressDialog.dismiss();
//                    Toast.makeText(CustomerActivation.this, "Customer Activation Successful", Toast.LENGTH_SHORT).show();
          //Utils.showAlertDialog(CustomerActivation.this, "Customer Activation Successful", "You Account is now enabled for Agency Banking");
          String mesg = "Your PIN has been successfully changed. Please use this new PIN in the future.";
          AlertDialog.Builder builder1 = new AlertDialog.Builder(CustomerChangePin.this);
          builder1.setMessage(mesg);
          builder1.setTitle("Successful!");
          builder1.setCancelable(true);

          builder1.setPositiveButton(
                  "DONE",
                  new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                      dialog.cancel();
                      startActivity(new Intent(CustomerChangePin.this, MainDashboardActivity.class));
                      finish();
                    }
                  });

                    /*builder1.setNegativeButton(
                            "Cancel",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                }
                            });*/

          AlertDialog alert11 = builder1.create();
          alert11.show();
        } else {
          progressDialog.dismiss();
          Utils.showAlertDialog(CustomerChangePin.this, "Customer Activation Failed", memberActivationResponse.getError());
        }

//                displayBalance(response);

      }

      @Override
      public void onTokenExpired() {

      }

      @Override
      public void onError(String error) {

      }
    });

  }


  @Override
  public void onBackPressed() {

    if (doubleBackToExitPressedOnce) {
      String msg = "Are you sure you want to exit change pin?";
      AlertDialog.Builder builder1 = new AlertDialog.Builder(CustomerChangePin.this);
      builder1.setMessage(msg);
      builder1.setTitle("Alert!");
      builder1.setCancelable(false);

      builder1.setPositiveButton(
              "yes",
              new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                  dialog.cancel();
                  finish();
                  Intent i = new Intent(CustomerChangePin.this, MainDashboardActivity.class);
                  i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                  i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                  startActivity(i);
                }
              });

      builder1.setNegativeButton(
              "CANCEL",
              new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id) {
                  dialog.cancel();
                  //onBackPressed();
                }
              });

      AlertDialog alert11 = builder1.create();
      alert11.show();
      //System.exit(1);
      //Toast.makeText(this, "Are you sure you want to exit Member Registration", Toast.LENGTH_LONG).show();
      return;
    }

    this.doubleBackToExitPressedOnce = true;
    toast.sinfo("Press back again to exit change pin");

    new Handler().postDelayed(new Runnable() {

      @Override
      public void run() {
        doubleBackToExitPressedOnce=false;
      }
    }, 2000);
    //super.onBackPressed();
    //overridePendingTransition(R.anim.move_left_in_activity, R.anim.move_right_out_activity);
  }

  private void getPicture() {

    String URL = Api.MSACCO_AGENT + Api.PhotoSignature;
    final SignaturePhotoRequest signphotoreq = new SignaturePhotoRequest();
    signphotoreq.identifier = activation.getText().toString();
    signphotoreq.identifiercode = "2";
    signphotoreq.agentid = SharedPrefs.read(SharedPrefs.AGENT_ID_1, null);
    signphotoreq.terminalid = MainDashboardActivity.imei;

    Api.instance(CustomerChangePin.this).request(URL,signphotoreq, new Api.RequestListener() {

      @Override
      public void onSuccess(String response) {
        try {
          pbphotos.setVisibility(View.GONE);
          JSONObject jsonObject = new JSONObject(response);
          JSONObject jsonArray = jsonObject.getJSONObject("photosign");

          String photo = jsonArray.getString("photo");
          String signature = jsonArray.getString("signature");

          if (photo.equals("N/A")) {

            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            Bitmap bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.user);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
            byte[] imageBytes = baos.toByteArray();
            String imageString = Base64.encodeToString(imageBytes, Base64.DEFAULT);
            imageBytes = Base64.decode(imageString, Base64.DEFAULT);
            final Bitmap decodedImage = BitmapFactory.decodeByteArray(imageBytes, 0, imageBytes.length);
            picture.setImageBitmap(decodedImage);
            pic_layout.setVisibility(View.VISIBLE);

            final View passportphoto = findViewById(R.id.picture_imageview);
            passportphoto.setOnClickListener(new View.OnClickListener() {
              @Override
              public void onClick(View view) {
                zoomImageFromThumb(passportphoto,decodedImage);
              }
            });
            shortAnimationDuration = getResources().getInteger(
                    android.R.integer.config_shortAnimTime);
          }
          if (signature.equals("N/A")) {

            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            Bitmap bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.signature);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
            byte[] imageBytes = baos.toByteArray();
            String imageString = Base64.encodeToString(imageBytes, Base64.DEFAULT);

            imageBytes = Base64.decode(imageString, Base64.DEFAULT);
            Bitmap decodedImage = BitmapFactory.decodeByteArray(imageBytes, 0, imageBytes.length);
            IVsignature.setImageBitmap(decodedImage);
            pic_layout.setVisibility(View.VISIBLE);


          }

          if (signature.equals("null")) {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            Bitmap bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.signature);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
            byte[] imageBytes = baos.toByteArray();
            String imageString = Base64.encodeToString(imageBytes, Base64.DEFAULT);

            //decode base64 string to image
            imageBytes = Base64.decode(imageString, Base64.DEFAULT);
            Bitmap decodedImage = BitmapFactory.decodeByteArray(imageBytes, 0, imageBytes.length);
            IVsignature.setImageBitmap(decodedImage);
            pic_layout.setVisibility(View.VISIBLE);

          }
          if (photo.equals("null")) {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            Bitmap bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.user);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, baos);
            byte[] imageBytes = baos.toByteArray();
            String imageString = Base64.encodeToString(imageBytes, Base64.DEFAULT);

            //decode base64 string to image
            imageBytes = Base64.decode(imageString, Base64.DEFAULT);
            final Bitmap decodedImage = BitmapFactory.decodeByteArray(imageBytes, 0, imageBytes.length);
            picture.setImageBitmap(decodedImage);
            pic_layout.setVisibility(View.VISIBLE);

            final View passportphoto = findViewById(R.id.picture_imageview);
            passportphoto.setOnClickListener(new View.OnClickListener() {
              @Override
              public void onClick(View view) {
                zoomImageFromThumb(passportphoto,decodedImage);
              }
            });
            shortAnimationDuration = getResources().getInteger(
                    android.R.integer.config_shortAnimTime);
          }
          if (!signature.equals("null") && !photo.equals("null") && !signature.equals("N/A") && !photo.equals("N/A")) {
            pic_layout.setVisibility(View.VISIBLE);
            final Bitmap decodedphoto = StringToBitMap(photo);
            picture.setImageBitmap(decodedphoto);
            Bitmap decodedsignature = StringToBitMap(signature);
            IVsignature.setImageBitmap(decodedsignature);

            final View passportphoto = findViewById(R.id.picture_imageview);
            passportphoto.setOnClickListener(new View.OnClickListener() {
              @Override
              public void onClick(View view) {
                zoomImageFromThumb(passportphoto,decodedphoto);
              }
            });
            shortAnimationDuration = getResources().getInteger(
                    android.R.integer.config_shortAnimTime);
          }

        } catch (JSONException e) {
          e.printStackTrace();
        }
      }

      @Override
      public void onTokenExpired() {

      }

      @Override
      public void onError(String error) {
        Toast.makeText(CustomerChangePin.this, "Failed", Toast.LENGTH_SHORT).show();
      }
    });

  }

  public Bitmap StringToBitMap(String encodedString) {
    try {
      byte[] encodeByte = Base64.decode(encodedString, Base64.DEFAULT);
      Bitmap bitmap = BitmapFactory.decodeByteArray(encodeByte, 0, encodeByte.length);
      return bitmap;
    } catch (Exception e) {
      e.getMessage();
      return null;
    }
  }

  private void zoomImageFromThumb(final View thumbView, Bitmap imageResId) {
    // If there's an animation in progress, cancel it
    // immediately and proceed with this one.
    if (currentAnimator != null) {
      currentAnimator.cancel();
    }

    final ImageView expandedImageView = (ImageView) findViewById(
            R.id.expanded_image);
    expandedImageView.setImageBitmap(imageResId);


    // Calculate the starting and ending bounds for the zoomed-in image.
    // This step involves lots of math. Yay, math.
    final Rect startBounds = new Rect();
    final Rect finalBounds = new Rect();
    final Point globalOffset = new Point();

    // The start bounds are the global visible rectangle of the thumbnail,
    // and the final bounds are the global visible rectangle of the container
    // view. Also set the container view's offset as the origin for the
    // bounds, since that's the origin for the positioning animation
    // properties (X, Y).
    thumbView.getGlobalVisibleRect(startBounds);
    findViewById(R.id.container)
            .getGlobalVisibleRect(finalBounds, globalOffset);
    startBounds.offset(-globalOffset.x, -globalOffset.y);
    finalBounds.offset(-globalOffset.x, -globalOffset.y);

    // Adjust the start bounds to be the same aspect ratio as the final
    // bounds using the "center crop" technique. This prevents undesirable
    // stretching during the animation. Also calculate the start scaling
    // factor (the end scaling factor is always 1.0).
    float startScale;
    if ((float) finalBounds.width() / finalBounds.height()
            > (float) startBounds.width() / startBounds.height()) {
      // Extend start bounds horizontally
      startScale = (float) startBounds.height() / finalBounds.height();
      float startWidth = startScale * finalBounds.width();
      float deltaWidth = (startWidth - startBounds.width()) / 2;
      startBounds.left -= deltaWidth;
      startBounds.right += deltaWidth;
    } else {
      // Extend start bounds vertically
      startScale = (float) startBounds.width() / finalBounds.width();
      float startHeight = startScale * finalBounds.height();
      float deltaHeight = (startHeight - startBounds.height()) / 2;
      startBounds.top -= deltaHeight;
      startBounds.bottom += deltaHeight;
    }

    // Hide the thumbnail and show the zoomed-in view. When the animation
    // begins, it will position the zoomed-in view in the place of the
    // thumbnail.
    thumbView.setAlpha(0f);
    expandedImageView.setVisibility(View.VISIBLE);

    // Set the pivot point for SCALE_X and SCALE_Y transformations
    // to the top-left corner of the zoomed-in view (the default
    // is the center of the view).
    expandedImageView.setPivotX(0f);
    expandedImageView.setPivotY(0f);

    // Construct and run the parallel animation of the four translation and
    // scale properties (X, Y, SCALE_X, and SCALE_Y).
    AnimatorSet set = new AnimatorSet();
    set
            .play(ObjectAnimator.ofFloat(expandedImageView, View.X,
                    startBounds.left, finalBounds.left))
            .with(ObjectAnimator.ofFloat(expandedImageView, View.Y,
                    startBounds.top, finalBounds.top))
            .with(ObjectAnimator.ofFloat(expandedImageView, View.SCALE_X,
                    startScale, 1f))
            .with(ObjectAnimator.ofFloat(expandedImageView,
                    View.SCALE_Y, startScale, 1f));
    set.setDuration(shortAnimationDuration);
    set.setInterpolator(new DecelerateInterpolator());
    set.addListener(new AnimatorListenerAdapter() {
      @Override
      public void onAnimationEnd(Animator animation) {
        currentAnimator = null;
      }

      @Override
      public void onAnimationCancel(Animator animation) {
        currentAnimator = null;
      }
    });
    set.start();
    currentAnimator = set;

    // Upon clicking the zoomed-in image, it should zoom back down
    // to the original bounds and show the thumbnail instead of
    // the expanded image.
    final float startScaleFinal = startScale;
    expandedImageView.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        if (currentAnimator != null) {
          currentAnimator.cancel();
        }

        // Animate the four positioning/sizing properties in parallel,
        // back to their original values.
        AnimatorSet set = new AnimatorSet();
        set.play(ObjectAnimator
                .ofFloat(expandedImageView, View.X, startBounds.left))
                .with(ObjectAnimator
                        .ofFloat(expandedImageView,
                                View.Y,startBounds.top))
                .with(ObjectAnimator
                        .ofFloat(expandedImageView,
                                View.SCALE_X, startScaleFinal))
                .with(ObjectAnimator
                        .ofFloat(expandedImageView,
                                View.SCALE_Y, startScaleFinal));
        set.setDuration(shortAnimationDuration);
        set.setInterpolator(new DecelerateInterpolator());
        set.addListener(new AnimatorListenerAdapter() {
          @Override
          public void onAnimationEnd(Animator animation) {
            thumbView.setAlpha(1f);
            expandedImageView.setVisibility(View.GONE);
            currentAnimator = null;
          }

          @Override
          public void onAnimationCancel(Animator animation) {
            thumbView.setAlpha(1f);
            expandedImageView.setVisibility(View.GONE);
            currentAnimator = null;
          }
        });
        set.start();
        currentAnimator = set;
      }
    });
  }

}
