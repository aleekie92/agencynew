package com.coretec.agencyApplication.activities.gfl;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.coretec.agencyApplication.R;
import com.coretec.agencyApplication.activities.BaseActivity;
import com.coretec.agencyApplication.activities.newmodules.ValidateTunzhengbigBio;
import com.coretec.agencyApplication.api.GFL;
import com.coretec.agencyApplication.api.requests.FingerPrintRequest;
import com.coretec.agencyApplication.api.requests.GetAvailableKgsGuarantorsRequest;
import com.coretec.agencyApplication.api.requests.GetGuarantorsRequest;
import com.coretec.agencyApplication.api.requests.GrowersDetailsRequest;
import com.coretec.agencyApplication.api.requests.GuarantorRegistration_v2;
import com.coretec.agencyApplication.api.responses.FingerPrintResponse;
import com.coretec.agencyApplication.api.responses.GetAvailableKgsGuarantorsResponse;
import com.coretec.agencyApplication.api.responses.GetGuarantorsResponse;
import com.coretec.agencyApplication.api.responses.GrowersDetailsResponse;
import com.coretec.agencyApplication.api.responses.GuarantorRegistrationResponse;
import com.coretec.agencyApplication.model.GrowerDetails;
import com.coretec.agencyApplication.model.GuarantorObject;
import com.coretec.agencyApplication.utils.QuickToast;
import com.coretec.agencyApplication.utils.SharedPrefs;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import static com.coretec.agencyApplication.utils.Utils.getFormattedDate;
import static com.coretec.agencyApplication.utils.Utils.getLat;
import static com.coretec.agencyApplication.utils.Utils.getLong;

public class Guarantor extends AppCompatActivity {

    private QuickToast toast = new QuickToast(this);
    TextView document_no, grower_number, id_number, product_type, amount, req_kgs, disbursement;
    Button btnAdd;
    private Context mContext;
    private List<GuarantorObject> guarantorObjectList = new ArrayList<>();
    AlertDialog guarantorDialog;
    private LinearLayout guarantorsLayout;
    private Button saveBtn, submitLoanApplication;
    ProgressDialog progressDialog;
    String available_kgs;
    String identifier_id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.gfl_guarantor);

        this.overridePendingTransition(R.anim.move_right_in_activity, R.anim.move_left_out_activity);

        SharedPrefs.write(SharedPrefs.FP_BOOLEAN, "");

        Toolbar toolbar = findViewById(R.id.toolbar);
        TextView mTitle = toolbar.findViewById(R.id.toolbar_title);
        mTitle.setText(getResources().getText(R.string.add_guarantor));
        setSupportActionBar(toolbar);
        ActionBar actionbar = getSupportActionBar();
        actionbar.setDisplayHomeAsUpEnabled(true);
        actionbar.setHomeAsUpIndicator(R.drawable.ic_arrow_back);

        progressDialog = new ProgressDialog(Guarantor.this);
        progressDialog.setMessage("Fetching details, please wait...");
        progressDialog.setCancelable(false);
        progressDialog.show();

        String growerNumber = getIntent().getExtras().getString("grower_number");
        getGuarantors(growerNumber);

        document_no = findViewById(R.id.tv_document);
        grower_number = findViewById(R.id.tv_app_gnumber);
        id_number = findViewById(R.id.tv_app_id);
        product_type = findViewById(R.id.tv_app_ptype);
        amount = findViewById(R.id.tv_app_amount);
        req_kgs = findViewById(R.id.tv_app_reqkgs);
        disbursement = findViewById(R.id.tv_app_disbusement);
        guarantorsLayout = findViewById(R.id.linear_guarantors);
        btnAdd = findViewById(R.id.btnAdd);
        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createDialog();
            }
        });
        saveBtn = findViewById(R.id.btnSave);
        submitLoanApplication = findViewById(R.id.submitLoan);
        submitLoanApplication.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String msg = "Are you sure you want to complete loan application?";
                //final String qualKgs = SharedPrefs.read(SharedPrefs.LOAN_QUALIFIED_KGS, null);
                android.app.AlertDialog.Builder builder1 = new android.app.AlertDialog.Builder(Guarantor.this);
                builder1.setMessage(msg);
                builder1.setTitle("Alert!");
                builder1.setCancelable(false);

                builder1.setPositiveButton(
                        "Yes",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                                progressDialog = new ProgressDialog(Guarantor.this);
                                progressDialog.setMessage("Completing, please wait...");
                                progressDialog.setCancelable(false);
                                progressDialog.show();
                                SubmitLoanApplication();
                                submitLoanApplication.setClickable(false);
                                //Toast.makeText(Guarantor.this, "Loan application submitted successfully!", Toast.LENGTH_SHORT).show();
                            }
                        });

                builder1.setNegativeButton(
                        "Cancel",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });

                android.app.AlertDialog alert11 = builder1.create();
                alert11.show();
            }
        });

        saveBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String msg = "Are you sure you want to add guarantor?";
                android.app.AlertDialog.Builder builder1 = new android.app.AlertDialog.Builder(Guarantor.this);
                builder1.setMessage(msg);
                builder1.setTitle("Alert!");
                builder1.setCancelable(false);

                builder1.setPositiveButton(
                        "Yes",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                                progressDialog = new ProgressDialog(Guarantor.this);
                                progressDialog.setMessage("Submitting, please wait...");
                                progressDialog.setCancelable(false);
                                progressDialog.show();
                                saveGuarantor();
                            }
                        });

                builder1.setNegativeButton(
                        "Cancel",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });

                android.app.AlertDialog alert11 = builder1.create();
                alert11.show();
            }
        });

    }

    void saveGuarantor () {
        final String TAG = "GuarantorRegistration";

        //Toast.makeText(mContext, "Loading please wait...", Toast.LENGTH_LONG).show();
        //toast.linfo("Loading please wait...");

        String URL = GFL.MSACCO_AGENT + GFL.GuarantorRegistration;

        final GuarantorRegistration_v2 grRequest = new GuarantorRegistration_v2();
        grRequest.setApplidno(id_number.getText().toString());
        grRequest.setApplgrowerno(grower_number.getText().toString());
        grRequest.setApplreqkgs(req_kgs.getText().toString());
        grRequest.setApplptype(product_type.getText().toString());
        grRequest.setApplamount(amount.getText().toString());
        grRequest.setDisbursmentmethod(disbursement.getText().toString());
        grRequest.setApprovalstatus("4");
        grRequest.setGuarantorlist(guarantorObjectList);
        grRequest.setTransactiontype("1");
        grRequest.setOrigDocumentNo(document_no.getText().toString());
        grRequest.setAgentid(SharedPrefs.read(SharedPrefs.AGENT_ID_1, null));
        grRequest.setTerminalid(SharedPrefs.read(SharedPrefs.DEVICE_ID, null));
        grRequest.setLongitude( getLong(Guarantor.this));
        grRequest.setLatitude(getLat(Guarantor.this));
        grRequest.setDate(getFormattedDate());
        grRequest.setCorporate_no( "CAP016");

        Log.e(TAG, grRequest.toString());

        GFL.instance(Guarantor.this).request(URL, grRequest, new GFL.RequestListener() {
            @Override
            public void onSuccess(String response) {
                progressDialog.dismiss();

                /* deposit_progressBar.setVisibility(View.GONE); */
                GuarantorRegistrationResponse grResponse = GFL.instance(Guarantor.this)
                        .mGson.fromJson(response, GuarantorRegistrationResponse.class);
                if (grResponse.is_successful) {

                    try {
                        JSONObject jsonObject = new JSONObject(response);

                        String receiptNo = jsonObject.getString("receiptNo");
                        //Toast.makeText(Guarantor.this, receiptNo, Toast.LENGTH_LONG).show();
                        String msg = "Guarantor saved successfully";
                        android.app.AlertDialog.Builder builder1 = new android.app.AlertDialog.Builder(Guarantor.this);
                        builder1.setMessage(msg);
                        builder1.setTitle("Alert!");
                        builder1.setCancelable(false);

                        builder1.setPositiveButton(
                                "Okay",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                       /* SubmitLoanApplication();
                                        dialog.cancel();*/
                                        dialog.cancel();
                                        // startActivity(new Intent(Guarantor.this, Dashboard.class));
                                        finish();
                                    }
                                });

                        android.app.AlertDialog alert11 = builder1.create();
                        alert11.show();
                        toast.ssuccess(receiptNo);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                } else {
                    Toast.makeText(Guarantor.this, grResponse.getError(), Toast.LENGTH_LONG).show();
                    progressDialog.dismiss();
                }

            }

            @Override
            public void onTokenExpired() {

            }

            @Override
            public void onError(String error) {

            }
        });
    }

    void SubmitLoanApplication () {
        final String TAG = "GuarantorRegistration";

        //Toast.makeText(mContext, "Loading please wait...", Toast.LENGTH_LONG).show();

        String URL = GFL.MSACCO_AGENT + GFL.GuarantorRegistration;

        final GuarantorRegistration_v2 grRequest = new GuarantorRegistration_v2();
        grRequest.setApplidno(id_number.getText().toString());
        grRequest.setNewAppl(false);
        grRequest.setApplgrowerno(grower_number.getText().toString());
        grRequest.setApplreqkgs(req_kgs.getText().toString());
        grRequest.setApplptype(product_type.getText().toString());
        grRequest.setApplamount(amount.getText().toString());
        grRequest.setDisbursmentmethod(disbursement.getText().toString());
        grRequest.setApprovalstatus("5");
        grRequest.setGuarantorlist(guarantorObjectList);
        grRequest.setTransactiontype("1");
        grRequest.setOrigDocumentNo(document_no.getText().toString());
        grRequest.setAgentid(SharedPrefs.read(SharedPrefs.AGENT_ID_1, null));
        grRequest.setTerminalid(SharedPrefs.read(SharedPrefs.DEVICE_ID, null));
        grRequest.setLongitude( getLong(Guarantor.this));
        grRequest.setLatitude(getLat(Guarantor.this));
        grRequest.setDate(getFormattedDate());
        grRequest.setCorporate_no( "CAP016");

        Log.e(TAG, grRequest.toString());

        GFL.instance(Guarantor.this).request(URL, grRequest, new GFL.RequestListener() {
            @Override
            public void onSuccess(String response) {

                progressDialog.dismiss();
                GuarantorRegistrationResponse grResponse = GFL.instance(Guarantor.this)
                        .mGson.fromJson(response, GuarantorRegistrationResponse.class);
                if (grResponse.is_successful) {
                    try {
                        JSONObject jsonObject = new JSONObject(response);

                        String receiptNo = jsonObject.getString("receiptNo");
                        toast.ssuccess(receiptNo);
                        String msg = "Loan application submitted successfully!";
                        //final String qualKgs = SharedPrefs.read(SharedPrefs.LOAN_QUALIFIED_KGS, null);
                        android.app.AlertDialog.Builder builder1 = new android.app.AlertDialog.Builder(Guarantor.this);
                        builder1.setMessage(msg);
                        builder1.setTitle("Success!");
                        builder1.setCancelable(false);

                        builder1.setPositiveButton(
                                "Done",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();
                                        //startActivity(new Intent(Guarantor.this, Loans.class));
                                        finish();
                                    }
                                });

                        android.app.AlertDialog alert11 = builder1.create();
                        alert11.show();

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                } else {
                    //do something here
                }

            }

            @Override
            public void onTokenExpired() {

            }

            @Override
            public void onError(String error) {

            }
        });
    }

    void getGuarantors(String growerNumber) {

        final String TAG = "GrowersAccounts";
//        EasyLocationMod easyLocationMod = new EasyLocationMod(CashWithdrawal.this);

        String URL = GFL.MSACCO_AGENT + GFL.GetGuarantors;

        final GetGuarantorsRequest guarantorsRequest = new GetGuarantorsRequest();
        guarantorsRequest.agentid = SharedPrefs.read(SharedPrefs.AGENT_ID_1, null);
        guarantorsRequest.corporateno = "CAP016";
        guarantorsRequest.growerno = growerNumber;
        guarantorsRequest.terminalid = SharedPrefs.read(SharedPrefs.DEVICE_ID, null);
        guarantorsRequest.longitude = getLong(this);
        guarantorsRequest.latitude = getLat(this);
        guarantorsRequest.date = getFormattedDate();

        Log.e(TAG, guarantorsRequest.getBody().toString());

        GFL.instance(this).request(URL, guarantorsRequest, new GFL.RequestListener() {
            @Override
            public void onSuccess(String response) {

                GetGuarantorsResponse getGuarantorsResponse = GFL.instance(Guarantor.this)
                        .mGson.fromJson(response, GetGuarantorsResponse.class);
                if (getGuarantorsResponse.is_successful) {
                    progressDialog.dismiss();
                    try {
                        JSONObject jsonObject = new JSONObject(response);

                        String agNumber = jsonObject.getString("applicantgrowerno");
                        String aIdNo = jsonObject.getString("applicantidno");
                        String aReqKgs = jsonObject.getString("applicantregkgs");
                        String prodType = jsonObject.getString("producttype");
                        String appAmnt = jsonObject.getString("appliedamt");
                        String disType = jsonObject.getString("disbursmenttype");
                        String appStatus = jsonObject.getString("approvalstatus");
                        String documentNo = jsonObject.getString("OriginalAccountNo");

                        SharedPrefs.write(SharedPrefs.APPLICANT_ID, aIdNo);

                        document_no.setText(documentNo);
                        grower_number.setText(agNumber);
                        id_number.setText(aIdNo);
                        product_type.setText(prodType);
                        amount.setText(appAmnt);
                        req_kgs.setText(aReqKgs);
                        disbursement.setText(disType);

                        JSONArray guarantorlist = jsonObject.getJSONArray("guarantorlist");
                        for (int l = 0; l < guarantorlist.length(); l++) {
                            JSONObject jsonObject2 = guarantorlist.getJSONObject(l);

                            String gNo = jsonObject2.getString("GuarGrowerNo");
                            String gIdentity = jsonObject2.getString("GuarIdNo");
                            String guarAvailKgs = jsonObject2.getString("GuarAvailKgs");
                            String guarDocument = jsonObject2.getString("DocNum");

                            GuarantorObject guarantorObject = new GuarantorObject();
                            guarantorObject.setAvailableKgs(guarAvailKgs);
                            guarantorObject.setGrowerNumber(gNo);
                            guarantorObject.setPassport(gIdentity);
                            guarantorObject.setDocument(guarDocument);
                            guarantorObjectList.add(guarantorObject);

                            addGuarantorToList();

                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                } else {
                    progressDialog.dismiss();
                    toast.swarn("Could not find loan details for this grower");
                }

            }

            @Override
            public void onTokenExpired() {

            }

            @Override
            public void onError(String error) {

            }
        });
    }

    private void createDialog() {

        LayoutInflater myInflater = LayoutInflater.from(this);
        View guarantorView = myInflater.inflate(R.layout.gfl_dialog_guarantor, null);

        final ArrayList<String> arrayList = new ArrayList<>();
        ArrayAdapter<GrowerDetails> adapter;
        String item;

        //define and access view attributes
        final Button submitBtn = guarantorView.findViewById(R.id.btnDiagSubmit);
        final Button cancelBtn = guarantorView.findViewById(R.id.btnDiagCancel);
        final EditText identifier_deposit = guarantorView.findViewById(R.id.id_number);
        final Spinner spinner = guarantorView.findViewById(R.id.growerSpinner);
        final ProgressBar progressBar = guarantorView.findViewById(R.id.growersPb);
        final TextView growerName = guarantorView.findViewById(R.id.tvfNames);
        final TextView warNing = guarantorView.findViewById(R.id.warning);
        final TextView totalKilos = guarantorView.findViewById(R.id.totalKgs);
        final TextView kiloTakikana = guarantorView.findViewById(R.id.takikana);
        final TextView availKgs = guarantorView.findViewById(R.id.availableKgs);
        final ProgressBar loading = guarantorView.findViewById(R.id.loadingPb);
        final TextView remarks = guarantorView.findViewById(R.id.tvRemarks);

        kiloTakikana.setText(SharedPrefs.read(SharedPrefs.LOAN_QUALIFIED_KGS, null));
        adapter = new ArrayAdapter<GrowerDetails>(Guarantor.this,
                R.layout.simple_spinner_dropdown_item, new ArrayList<GrowerDetails>());
        //adapter.setDropDownViewResource(R.layout.simple_spinner_dropdown_item);

        warNing.setVisibility(View.GONE);
        loading.setVisibility(View.GONE);
        identifier_deposit.setInputType(InputType.TYPE_NULL);
        identifier_deposit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // showMyDialog();
            }
        });

        identifier_deposit.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    // showMyDialog();
                }
            }
        });
        /*Button cancelButton = guarantorView.findViewById(R.id.btnCancel);*/

        TextWatcher textWatcherId = new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                //progressBar.setVisibility(View.GONE);
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                String idNumber = SharedPrefs.read(SharedPrefs.APPLICANT_ID, null);
                //here, after we introduced something in the EditText we get the string from it
                String identifier_number = identifier_deposit.getText().toString();
                if (idNumber.equals(identifier_number)) {
                    warNing.setVisibility(View.VISIBLE);
                    progressBar.setVisibility(View.GONE);
                    growerName.setText("");
                    arrayList.clear();

                } else if (!(idNumber.equals(identifier_number))) {
                    if (identifier_deposit.length() >= 5 && identifier_deposit.length() <= 8) {
                        warNing.setVisibility(View.GONE);
                        submitBtn.setVisibility(View.GONE);
                        arrayList.clear();
                        progressBar.setVisibility(View.VISIBLE);
                        progressBar.getIndeterminateDrawable().setColorFilter(ContextCompat.getColor(Guarantor.this, R.color.btn_green), PorterDuff.Mode.SRC_IN);
                    }

                    boolean existsAlready = false;
                    for (GuarantorObject guarantorObject : guarantorObjectList) {
                        if (guarantorObject.getPassport().equals(identifier_deposit.getText().toString())) {
                            existsAlready = true;
                            break;
                        }
                    }

                    if (existsAlready) {
                        Toast.makeText(Guarantor.this, "Guarantor already added", Toast.LENGTH_SHORT).show();
                        return;
                    }

                    if (identifier_deposit.length() >= 7) {

                        //requestGetAccountDetails(identifier_number);
                        final String TAG = "GrowersAccounts";

                        String URL = GFL.MSACCO_AGENT + GFL.GetGrowersDetails;

                        final GrowersDetailsRequest growersDetails = new GrowersDetailsRequest();
                        growersDetails.agentid = SharedPrefs.read(SharedPrefs.AGENT_ID_1, null);
                        growersDetails.corporateno = "CAP016";
                        growersDetails.accountidentifier = identifier_number;
                        growersDetails.accountidentifiercode = "1";
                        growersDetails.transactiontype = "1";
                        growersDetails.terminalid = SharedPrefs.read(SharedPrefs.DEVICE_ID, null);
                        growersDetails.longitude = getLong(Guarantor.this);
                        growersDetails.latitude = getLat(Guarantor.this);
                        growersDetails.date = getFormattedDate();

                        Log.e(TAG, growersDetails.getBody().toString());

                        GFL.instance(Guarantor.this).request(URL, growersDetails, new GFL.RequestListener() {
                            @Override
                            public void onSuccess(String response) {
                                /*deposit_progressBar.setVisibility(View.GONE);*/

                                GrowersDetailsResponse growersDetailsResponse = GFL.instance(Guarantor.this)
                                        .mGson.fromJson(response, GrowersDetailsResponse.class);
                                if (growersDetailsResponse.is_successful) {
                                    List<GrowerDetails> growerDetailsList = new ArrayList<>();
                                    warNing.setVisibility(View.GONE);
                                    try {
                                        JSONObject jsonObject = new JSONObject(response);

                                        JSONArray jsonArray = jsonObject.getJSONArray("Growers");
                                        for (int i = 0; i < jsonArray.length(); i++) {
                                            JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                                            JSONArray jsonArray1 = jsonObject1.getJSONArray("GrowerDetails");
                                            for (int l = 0; l < jsonArray1.length(); l++) {
                                                JSONObject jsonObject2 = jsonArray1.getJSONObject(l);
                                                progressBar.setVisibility(View.GONE);
                                                arrayList.add(String.valueOf(jsonObject2.getString("GrowerNo")));

                                                String gName = jsonObject2.getString("GrowerName");
                                                String gNo = jsonObject2.getString("GrowerNo");
                                                String totalKgs = jsonObject2.getString("CurrentKG");
                                                String isDefaulter = jsonObject2.getString("LoanDefaulter");

                                                SharedPrefs.write(SharedPrefs.IS_DEFAULTER, isDefaulter);
                                                if (isDefaulter.equals("Yes")) {
                                                    String msg = "The grower selected has defaulted one or more loans. Please choose another one.";
                                                    //final String qualKgs = SharedPrefs.read(SharedPrefs.LOAN_QUALIFIED_KGS, null);
                                                    android.app.AlertDialog.Builder builder1 = new android.app.AlertDialog.Builder(Guarantor.this);
                                                    builder1.setMessage(msg);
                                                    builder1.setTitle("Defaulter Alert!");
                                                    builder1.setCancelable(false);

                                                    builder1.setPositiveButton(
                                                            "OK",
                                                            new DialogInterface.OnClickListener() {
                                                                public void onClick(DialogInterface dialog, int id) {
                                                                    dialog.cancel();
                                                                    guarantorDialog.dismiss();
                                                                }
                                                            });

                                                    /*builder1.setNegativeButton(
                                                            "Cancel",
                                                            new DialogInterface.OnClickListener() {
                                                                public void onClick(DialogInterface dialog, int id) {
                                                                    dialog.cancel();
                                                                }
                                                            });*/

                                                    android.app.AlertDialog alert11 = builder1.create();
                                                    alert11.show();
                                                }

                                                growerName.setText(gName);
                                                totalKilos.setText(totalKgs);
                                                Log.e("Growers NUMBER:", gNo);

                                                //add guarntor name to list
                                                //guarantorList.add(gName);
                                            }

                                        }

                                        //Growers Number
                                        //spinner = (Spinner) v.findViewById(R.id.growerSpinner);
                                        ArrayAdapter<String> adapter = new ArrayAdapter<String>(Guarantor.this, android.R.layout.simple_spinner_item, arrayList);
                                        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                                        spinner.setAdapter(adapter);
                                        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                            @Override
                                            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                                String item = parent.getItemAtPosition(position).toString();
                                                String text1 = spinner.getSelectedItem().toString();
                                                SharedPrefs.write(SharedPrefs.DROP_DOWN, text1);
                                                loading.setVisibility(View.VISIBLE);
                                                loading.getIndeterminateDrawable().setColorFilter(ContextCompat.getColor(Guarantor.this, R.color.white), PorterDuff.Mode.SRC_IN);
                                                final String TAG = "AvailableKGS";

                                                String URL = GFL.MSACCO_AGENT + GFL.GetAvailableKgsGuarantors;

                                                final GetAvailableKgsGuarantorsRequest availableKgs = new GetAvailableKgsGuarantorsRequest();
                                                availableKgs.agentid = SharedPrefs.read(SharedPrefs.AGENT_ID_1, null);
                                                availableKgs.corporateno = "CAP016";
                                                availableKgs.growerno = text1;
                                                availableKgs.transactiontype = "1";
                                                availableKgs.terminalid = SharedPrefs.read(SharedPrefs.DEVICE_ID, null);
                                                availableKgs.longitude = getLong(Guarantor.this);
                                                availableKgs.latitude = getLat(Guarantor.this);
                                                availableKgs.date = getFormattedDate();

                                                Log.e(TAG, availableKgs.getBody().toString());

                                                GFL.instance(Guarantor.this).request(URL, availableKgs, new GFL.RequestListener() {
                                                    @Override
                                                    public void onSuccess(String response) {
                                                        /*deposit_progressBar.setVisibility(View.GONE);*/

                                                        GetAvailableKgsGuarantorsResponse availableKgsResponse = GFL.instance(Guarantor.this)
                                                                .mGson.fromJson(response, GetAvailableKgsGuarantorsResponse.class);
                                                        if (availableKgsResponse.is_successful) {
                                                            List<GrowerDetails> growerDetailsList = new ArrayList<>();
                                                            //warNing.setVisibility(View.GONE);
                                                            try {
                                                                JSONObject jsonObject = new JSONObject(response);

                                                                String availableKgs = jsonObject.getString("availablekgs");
                                                                //String cleanKgs = availableKgs.substring(0, availableKgs.indexOf("."));
                                                                availKgs.setText(availableKgs);
                                                                //Toast.makeText(getContext(), availKgs, Toast.LENGTH_SHORT).show();
                                                                double kilos = Double.parseDouble(availableKgs);
                                                                if (kilos < 100.00 || kilos < 0.00) {
                                                                    cancelBtn.setVisibility(View.VISIBLE);
                                                                    loading.setVisibility(View.GONE);
                                                                    remarks.setText("Customer has over guaranteed");
                                                                    remarks.setTextColor(getResources().getColor(R.color.red));
                                                                    submitBtn.setVisibility(View.GONE);
                                                                } else {
                                                                    submitBtn.setVisibility(View.VISIBLE);
                                                                    cancelBtn.setVisibility(View.GONE);
                                                                    loading.setVisibility(View.GONE);
                                                                    remarks.setText("OK");
                                                                    remarks.setTextColor(getResources().getColor(R.color.btn_green));
                                                                }

                                                            } catch (JSONException e) {
                                                                e.printStackTrace();
                                                            }

                                                        } else {
                                                            //do something here
                                                        }

                                                    }

                                                    @Override
                                                    public void onTokenExpired() {

                                                    }

                                                    @Override
                                                    public void onError(String error) {

                                                    }
                                                });

                                                Log.e("LOAN CODE", text1);
                                            }

                                            @Override
                                            public void onNothingSelected(AdapterView<?> parent) {

                                            }
                                        });

                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }

                                } else {
//                    Toast.makeText(ShareDeposit.this, "Accounts Fetching Failed", Toast.LENGTH_LONG).show();
                                }

                            }

                            @Override
                            public void onTokenExpired() {

                            }

                            @Override
                            public void onError(String error) {

                            }
                        });
                    }

                }
//                Toast.makeText(ShareDeposit.this, accountNumber, Toast.LENGTH_SHORT).show();
//                requestAccountName(accountNumber);

            }
        };
        identifier_deposit.addTextChangedListener(textWatcherId);

        /*createButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createWallet();
            }
        });*/

        submitBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                progressDialog = new ProgressDialog(Guarantor.this);
                progressDialog.setMessage("Please wait...");
                progressDialog.setCancelable(false);
                progressDialog.show();

                available_kgs = availKgs.getText().toString();
                identifier_id = identifier_deposit.getText().toString();
                requestFingerPrints(identifier_deposit.getText().toString());

                /*GuarantorObject guarantorObject = new GuarantorObject();
                guarantorObject.setAvailableKgs(availKgs.getText().toString());
                guarantorObject.setGrowerNumber(SharedPrefs.read(SharedPrefs.DROP_DOWN, null));
                guarantorObject.setPassport(identifier_deposit.getText().toString());
                guarantorObjectList.add(guarantorObject);

                addGuarantorToList();
                guarantorDialog.dismiss();*/
            }
        });
        cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //guarantorList.add(SharedPrefs.read(SharedPrefs.DROP_DOWN, null));
                //addGuarantorToList();
                guarantorDialog.dismiss();
            }
        });

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setView(guarantorView);
        guarantorDialog = builder.create();
        guarantorDialog.show();

    }

    private void addGuarantorToList() {

        if (guarantorObjectList.size() >= 1 && guarantorObjectList.size() < 3) {
            //Toast.makeText(getActivity(), "Minimum of three guarantors required", Toast.LENGTH_SHORT).show();
            saveBtn.setVisibility(View.VISIBLE);
            submitLoanApplication.setVisibility(View.GONE);
        }
        if (guarantorObjectList.size() < 1) {
            //Toast.makeText(getActivity(), "Minimum of three guarantors required", Toast.LENGTH_SHORT).show();
            saveBtn.setVisibility(View.VISIBLE);
            submitLoanApplication.setVisibility(View.GONE);
        }
        if (guarantorObjectList.size() >= 3) {
            //Toast.makeText(getActivity(), "Minimum of three guarantors required", Toast.LENGTH_SHORT).show();
            saveBtn.setVisibility(View.GONE);
            submitLoanApplication.setVisibility(View.VISIBLE);
        }

        // remove previous gs
        guarantorsLayout.removeAllViews();

        // loop to add guarantors
        for (final GuarantorObject guarantorObject : guarantorObjectList) {
            // get g view
            View v = LayoutInflater.from(this).inflate(R.layout.item_guarantor, null);

            // get views
            TextView tvName = v.findViewById(R.id.tv_guarantors_name);
            TextView tvPass = v.findViewById(R.id.tv_guarantors_id);
            TextView tvKg = v.findViewById(R.id.tv_guarantors_kgs);
            ImageButton removeImageBtn = v.findViewById(R.id.imageButton_close);


            tvKg.setText(guarantorObject.getAvailableKgs() + " kgs");
            tvName.setText(guarantorObject.getGrowerNumber());
            tvPass.setText(guarantorObject.getPassport());

            removeImageBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    String msg = "Are you sure you want to remove guarantor?";
                    //final String qualKgs = SharedPrefs.read(SharedPrefs.LOAN_QUALIFIED_KGS, null);
                    android.app.AlertDialog.Builder builder1 = new android.app.AlertDialog.Builder(Guarantor.this);
                    builder1.setMessage(msg);
                    builder1.setTitle("Alert!");
                    builder1.setCancelable(false);

                    builder1.setPositiveButton(
                            "Yes",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                    guarantorObjectList.remove(guarantorObject);
                                    addGuarantorToList();
                                }
                            });

                    builder1.setNegativeButton(
                            "Cancel",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                }
                            });

                    android.app.AlertDialog alert11 = builder1.create();
                    alert11.show();

                }
            });

            // add view to linear
            guarantorsLayout.addView(v);
        }
    }

    void requestFingerPrints(String idNumber) {

        final String TAG = "GET FINGERPRINTS";
        String URL = GFL.MSACCO_AGENT + GFL.GetFourFingerPrint;

        final FingerPrintRequest fingerPrintRequest = new FingerPrintRequest();
        fingerPrintRequest.accountidentifier = idNumber;
        fingerPrintRequest.accountidentifiercode = "1";
        fingerPrintRequest.agentid = SharedPrefs.read(SharedPrefs.AGENT_ID_1, null);
        fingerPrintRequest.terminalid = MainGFLDashboard.imei;//SharedPrefs.read(SharedPrefs.DEVICE_ID, null);
        fingerPrintRequest.longitude = getLong(this);
        fingerPrintRequest.latitude = getLat(this);
        fingerPrintRequest.date = getFormattedDate();

        Log.e(TAG, fingerPrintRequest.getBody().toString());

        GFL.instance(this).request(URL, fingerPrintRequest, new GFL.RequestListener() {
            @Override
            public void onSuccess(String response) {
                /*deposit_progressBar.setVisibility(View.GONE);*/
                //progressDialog.dismiss();
                FingerPrintResponse fingerPrintResponse = GFL.instance(Guarantor.this)
                        .mGson.fromJson(response, FingerPrintResponse.class);
                if (fingerPrintResponse.is_successful) {

                    try {
                        JSONObject jsonObject = new JSONObject(response);

                        String leftRing = jsonObject.getString("leftring");
                        String leftLittle = jsonObject.getString("leftlittle");
                        String rightRing = jsonObject.getString("rightring");
                        String rightLittle = jsonObject.getString("rightlittle");

                        SharedPrefs.write(SharedPrefs.LEFT_RING, leftRing);
                        SharedPrefs.write(SharedPrefs.LEFT_LITTLE, leftLittle);
                        SharedPrefs.write(SharedPrefs.RIGHT_RING, rightRing);
                        SharedPrefs.write(SharedPrefs.RIGHT_LITTLE, rightLittle);

                        Log.d("LEFT_RING", leftRing);
                        Log.d("LEFT_LITTLE", leftLittle);
                        Log.d("RIGHT_RING", rightRing);
                        Log.d("RIGHT_LITTLE", rightLittle);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    startActivity(new Intent(Guarantor.this, ValidateTunzhengbigBio.class));

                } else {
                    progressDialog.dismiss();
                    toast.swarn("Could not get customer finger prints!");
                }

            }

            @Override
            public void onTokenExpired() {

            }

            @Override
            public void onError(String error) {

            }
        });

    }

    @Override
    protected void onResume() {
        SharedPrefs.init(getApplicationContext());
        super.onResume();
        String fpBoolean = SharedPrefs.read(SharedPrefs.FP_BOOLEAN, null);

        if (fpBoolean.equals("true")) {
            progressDialog.dismiss();

            GuarantorObject guarantorObject = new GuarantorObject();
            guarantorObject.setAvailableKgs(available_kgs);
            guarantorObject.setGrowerNumber(SharedPrefs.read(SharedPrefs.DROP_DOWN, null));
            guarantorObject.setPassport(identifier_id);
            guarantorObjectList.add(guarantorObject);

            addGuarantorToList();
            guarantorDialog.dismiss();

        } else if (fpBoolean.equals("false")){
            progressDialog.dismiss();
            toast.swarn("Please authenticate to complete transaction!");
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.move_left_in_activity, R.anim.move_right_out_activity);
    }

}
