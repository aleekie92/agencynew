package com.coretec.agencyApplication.activities.newmodules;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.coretec.agencyApplication.R;
import com.coretec.agencyApplication.activities.BaseActivity;
import com.coretec.agencyApplication.api.Api;
import com.coretec.agencyApplication.api.requests.GlobalVRequest;
import com.coretec.agencyApplication.api.requests.RequestMemberName;
import com.coretec.agencyApplication.api.requests.newmodulesrequests.AccountOpeningRequest;
import com.coretec.agencyApplication.api.requests.newmodulesrequests.GetMLoanProductsRequest;
import com.coretec.agencyApplication.api.responses.newmoduleresponses.AccountOpeningResponse;
import com.coretec.agencyApplication.api.responses.newmoduleresponses.GetMLoanProductsResponse;
import com.coretec.agencyApplication.api.responses.MemberNameResponse;
import com.coretec.agencyApplication.model.newmodulemodels.MLoanDetails;
import com.coretec.agencyApplication.utils.PreferenceFileKeys;
import com.coretec.agencyApplication.utils.SharedPrefs;
import com.coretec.agencyApplication.utils.Utils;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.TimeZone;

import static com.coretec.agencyApplication.utils.Utils.getFormattedDate;
import static com.coretec.agencyApplication.utils.Utils.getLat;
import static com.coretec.agencyApplication.utils.Utils.getLong;
import static com.coretec.agencyApplication.utils.Utils.snackMessage;

public class Account_Opening extends BaseActivity implements AdapterView.OnItemSelectedListener, View.OnClickListener {
    EditText etID, etName, etAmount, etGroupCode, etJuniorName, etDOB;
    TextView tvLoadMember, tvLoadProducts;
    Button btnSubmit, btnSearch;
    Spinner spProduct;
    TextInputLayout tlJunior, tlDOB,tl_Gc;
    LinearLayout linearLayout;
    Toolbar mToolbar;
    GlobalVRequest globalVRequest;
    SharedPreferences sharedPreferences;
    String identifier, identifiercode, s_phoneNumber, s_name, s_accountName, s_amount, s_productCode;
    ProgressDialog progressDialog;
    private ArrayList<String> arrayListLoan = new ArrayList<>();
    ArrayList<String> productCodeList = new ArrayList<>();
    ArrayList<String> descriptionList = new ArrayList<>();
    private int pYear, pMonth, pDay;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_capitalnew_account__opening);
        sharedPreferences = getSharedPreferences(PreferenceFileKeys.PREFS_FILE, Context.MODE_PRIVATE);
        final Calendar cal = Calendar.getInstance();
        pYear = cal.get(Calendar.YEAR);
        pMonth = cal.get(Calendar.MONTH);
        pDay = cal.get(Calendar.DAY_OF_MONTH);
        initialize();
    }

    void initialize() {
        etID = findViewById(R.id.et_account_id);
        etName = findViewById(R.id.et_account_name);
        etAmount = findViewById(R.id.et_account_amount);
        etGroupCode = findViewById(R.id.et_account_groupcode);
        tvLoadMember = findViewById(R.id.tv_account_loadMember);
        linearLayout = findViewById(R.id.root_view);
        btnSearch = findViewById(R.id.btn_account_search);
        tvLoadProducts = findViewById(R.id.tv_account_products);
        etJuniorName = findViewById(R.id.et_account_juniourName);
        etDOB = findViewById(R.id.et_account_dob);
        tlDOB = findViewById(R.id.tL_dob);
        tl_Gc = findViewById(R.id.tl_Gc);
        tlJunior = findViewById(R.id.tL_jName);

        spProduct = findViewById(R.id.sp_account_product);
        btnSubmit = findViewById(R.id.btn_account_open);

        String coopnumber = SharedPrefs.read(SharedPrefs.VR_COOPORATE_NO, null);
        if (coopnumber !=null){
            switch (SharedPrefs.read(SharedPrefs.VR_COOPORATE_NO, null)) {
                case "CAP005":
                    btnSubmit.setBackgroundResource(R.drawable.btn_rounded_primary_coloraccentforcoopmis);
                    break;
                case "CAP022":
                    btnSubmit.setBackgroundResource(R.drawable.btn_rounded_primary_coloraccentforcoopmis);
                    break;
                default:
                    btnSubmit.setBackgroundResource(R.drawable.btn_rounded_primary_coloraccent);
                    break;
            }

        }else {
            btnSubmit.setBackgroundResource(R.drawable.btn_rounded_primary_coloraccent);
        }

        /*listeners*/
        spProduct.setOnItemSelectedListener(this);
        etDOB.setOnClickListener(this);
        btnSearch.setOnClickListener(this);
        btnSubmit.setOnClickListener(this);

        etAmount.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                String amount = etAmount.getText().toString();
               /* if (!amount.isEmpty()) {
                    Double d_amount = Double.valueOf(amount);
                    if (d_amount < 100.0) {
                        etAmount.setFocusable(true);
                        etAmount.setError("This Field is Optional");
                    } else {
                        etAmount.setError(null);
                    }
                } else {
                    etAmount.setFocusable(true);
                    etAmount.setError("set amount");
                }*/
            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        });


    }

    private void requestMemberDetails(String id) {
        //include identifier and code
        String accountNumber = etID.getText().toString();
        RequestMemberName requestMemberName = new RequestMemberName();
        requestMemberName.corporateno = MainDashboardActivity.corporateno;
        requestMemberName.accountidentifier = accountNumber;
        requestMemberName.accountcode = "0";
        requestMemberName.agentid = sharedPreferences.getString(PreferenceFileKeys.AGENT_ID, "");
        requestMemberName.terminalid = MainDashboardActivity.imei;
        /* Log.d("Account Opening","MemberDetails imei|"+MainActivity.imei+"|corporate "+MainActivity.corporateno+"|agentId "+sharedPreferences.getString(PreferenceFileKeys.AGENT_ID, ""));*/
        tvLoadMember.setVisibility(View.VISIBLE);

        Api.instance(this).request(Api.MSACCO_AGENT + Api.GetMembername, requestMemberName, new Api.RequestListener() {
            @Override
            public void onSuccess(String response) {
                tvLoadMember.setVisibility(View.GONE);
                Log.d("Account Opening", "MemberDetails Response-" + response);
                MemberNameResponse memberNameResponse = Api.instance(getApplicationContext()).mGson.fromJson(response, MemberNameResponse.class);
                if (memberNameResponse.is_successful) {
                    etName.setText(memberNameResponse.getCustomer_name());
                    s_phoneNumber = memberNameResponse.getPhoneno();
                } else {
                    tvLoadMember.setVisibility(View.VISIBLE);
                    tvLoadMember.setText(memberNameResponse.getError().toLowerCase());
                }
            }

            @Override
            public void onTokenExpired() {
                tvLoadMember.setVisibility(View.GONE);
            }

            @Override
            public void onError(String error) {
                tvLoadMember.setText("failed to load..");
                Log.d("Account Opening", "MemberDetails Response- error" + error);
            }
        });
    }

    private void openAccount() {
        String s_groupCode = etGroupCode.getText().toString();
        if (s_groupCode.isEmpty()) {
            s_groupCode = " ";
        }
        String URL = Api.MSACCO_AGENT + Api.AccountOpening;
        AccountOpeningRequest accountOpeningRequest = new AccountOpeningRequest();
        accountOpeningRequest.amount = etAmount.getText().toString();
        accountOpeningRequest.productcode = s_productCode;
        accountOpeningRequest.groupcode = "CODE";//s_groupCode;//
        accountOpeningRequest.idnumber = etID.getText().toString();
        accountOpeningRequest.juniorname = "NAME";//etJuniorName.getText().toString();//
        accountOpeningRequest.juniordob = getFormattedDate();//etDOB.getText().toString();//
        accountOpeningRequest.corporateno = MainDashboardActivity.corporateno;
        accountOpeningRequest.agentid = sharedPreferences.getString(PreferenceFileKeys.AGENT_ID, "");
        accountOpeningRequest.terminalid = MainDashboardActivity.imei;
        accountOpeningRequest.longitude = Utils.getLong(this);
        accountOpeningRequest.latitude = Utils.getLat(this);
        accountOpeningRequest.date = getFormattedDate();//
        onProgress();

        Api.instance(Account_Opening.this).request(URL, accountOpeningRequest, new Api.RequestListener() {
            @Override
            public void onSuccess(String response) {
                dismissProgress();
                Log.d("Accountopening", "open: " + response);
                AccountOpeningResponse accountOpeningResponse = Api.instance(getApplicationContext()).mGson.fromJson(response, AccountOpeningResponse.class);

                Log.d("Accountopening", "open: " + accountOpeningResponse.getError());
                if (accountOpeningResponse.is_successful) {
                    success();
                    snackMessage(linearLayout, "account opened successfully");
                } else {
                    snackMessage(linearLayout, "failed to open account..please seek assistance");
                }
            }

            @Override
            public void onTokenExpired() {
                dismissProgress();
                snackMessage(linearLayout, "session expired..");
                // Log.d("Account Opening","open: session expired");
            }

            @Override
            public void onError(String error) {
                dismissProgress();
                Log.d("Accountopening", "open: " + error);
                snackMessage(linearLayout, "Network error!!");
            }
        });


    }

    String code, desc;

    void requestLoanProducts() {
        final String TAG = "LoanProducts";
        List<MLoanDetails> loanDetailsList = new ArrayList<>();

        String URL = Api.MSACCO_AGENT + Api.GetmLoanProducts;
        loanDetailsList.clear();
        productCodeList.clear();
        descriptionList.clear();


        final GetMLoanProductsRequest loansProducts = new GetMLoanProductsRequest();

        loansProducts.corporateno = MainDashboardActivity.corporateno;
        loansProducts.productioncode = "1";
        loansProducts.idnumber = etID.getText().toString();
        loansProducts.agentid = SharedPrefs.read(SharedPrefs.AGENT_ID_1, null);
        loansProducts.terminalid = MainDashboardActivity.imei;// SharedPrefs.read(SharedPrefs.DEVICE_ID, null);
        loansProducts.longitude = getLong(getApplicationContext());
        loansProducts.latitude = getLat(getApplicationContext());
        tvLoadProducts.setVisibility(View.VISIBLE);


        Api.instance(getApplicationContext()).request(URL, loansProducts, new Api.RequestListener() {
            @Override
            public void onSuccess(String response) {

                List<MLoanDetails> loanDetailsList = new ArrayList<>();

                loanDetailsList.add(new MLoanDetails("--", "--"));

                GetMLoanProductsResponse loanProductsResponse = Api.instance(getApplicationContext())
                        .mGson.fromJson(response, GetMLoanProductsResponse.class);
                if (loanProductsResponse.is_successful) {
                    Log.d("Account opening", "product success");

                    try {
                        tvLoadProducts.setVisibility(View.GONE);
                        JSONObject jsonObject = new JSONObject(response);

                        JSONArray jsonArray = jsonObject.getJSONArray("product");
                        Log.d("Account opening", "product json" + jsonArray);
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject loanObjects = jsonArray.getJSONObject(i);

                            code = loanObjects.getString("productid");
                            desc = loanObjects.getString("description");

                            Log.d("prodid","prodid------>"+code);
                            Log.d("prodid","prodid------>"+desc);

                            loanDetailsList.add(new MLoanDetails(code, code + "-" + desc));
                            productCodeList.add(loanDetailsList.get(i).getProductid());
                            descriptionList.add((loanDetailsList.get(i).getDescription()));
                            loadProductTypes(descriptionList, productCodeList);
                        }


                    } catch (JSONException e) {
                        e.printStackTrace();
                        loadProductsError();
                    }

                } else {
                    loadProductsError();

                }

            }

            @Override
            public void onTokenExpired() {

            }

            @Override
            public void onError(String error) {
                loadProductsError();
            }
        });
    }


    void onProgress() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("please wait..");
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    void dismissProgress() {
        progressDialog.dismiss();
    }

    void success() {
        final AlertDialog.Builder dialog = new AlertDialog.Builder(this);
        dialog.setTitle("Success");
        dialog.setMessage("Account opened successfully");
        dialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
                finish();
                Intent ii = new Intent(Account_Opening.this, MainDashboardActivity.class);
                ii.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                ii.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(ii);
            }
        });

        dialog.show();
    }

    void search() {
        String idNo = etID.getText().toString();
        if (idNo.length() == 8) {
            requestMemberDetails(idNo);
            requestLoanProducts();
        } else {
            etID.setError("ID should be 8 digits");
        }
    }

    void loadProductsError() {
        tvLoadProducts.setTextColor(getResources().getColor(R.color.colorAccent));
        tvLoadProducts.setText("failed to load products..");
    }

    void loadProductTypes(final List<String> stringList, final List<String> stringList1) {
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(getApplicationContext(), R.layout.simple_spinner_dropdown_item, stringList) {
            @Override
            public boolean isEnabled(int position) {
                if (position == 0) {
                    // Disable the first item from Spinner, used as hint
                    return false;
                } else {
                    return true;
                }

            }
        };
        spProduct.setAdapter(arrayAdapter);

    }

    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
        switch (adapterView.getId()) {
            case R.id.sp_account_product:
                String selected = descriptionList.get(i);
                s_productCode = productCodeList.get(i);
                validateOnProductType();
                Log.d("Accountopening", "selected on item" + "|desc|" + selected + " |code| " + s_productCode);
                break;
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }
    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.et_account_dob:
                showDatePickerDialog();
                break;
            case R.id.btn_account_search:
                search();
                break;
            case R.id.btn_account_open:

                String id = etID.getText().toString();
                String amount = etAmount.getText().toString();

                if (id.isEmpty()) {
                    Toast.makeText(this, "Enter ID number", Toast.LENGTH_SHORT).show();
                } else if (spProduct.getSelectedItem().toString().equals("--")) {
                    Toast.makeText(this, "Select Product code", Toast.LENGTH_SHORT).show();
                }  else {
                    if (SharedPrefs.read(SharedPrefs.VR_COOPORATE_NO,null).equals("CAP002")){
                        if (valid()){
                            openAccount();
                        }else {
                            snackMessage(linearLayout,"empty fields");
                        }
                    }else {
                        openAccount();
                    }
                   /* if (valid()){
                        openAccount();
                    }else {
                        snackMessage(linearLayout,"empty fields");
                    }*/
                }
                break;
        }
    }


    public void showDatePickerDialog() {

        Calendar cal = Calendar.getInstance(TimeZone.getDefault()); // Get current date
        DatePickerDialog datePicker = new DatePickerDialog(this,
                datePickerListener,
                cal.get(Calendar.YEAR),
                cal.get(Calendar.MONTH),
                cal.get(Calendar.DAY_OF_MONTH));
        datePicker.getDatePicker().setMaxDate(System.currentTimeMillis() - 1000);
        datePicker.setCancelable(false);
        datePicker.setTitle("Pick DOB");
        datePicker.show();
    }

    private DatePickerDialog.OnDateSetListener datePickerListener =
            new DatePickerDialog.OnDateSetListener() {

                public void onDateSet(DatePicker view, int year,
                                      int monthOfYear, int dayOfMonth) {
                    pYear = year;
                    pMonth = monthOfYear;
                    pDay = dayOfMonth;
                    updateDisplay();
                }

            };

    private void updateDisplay() {

        etDOB.setText(
                new StringBuilder()
                        .append(pYear).append("-")
                        .append(pMonth + 1).append("-")
                        .append(pDay)
        );
    }

     private boolean valid() {
        boolean valid = true;
        String group = etGroupCode.getText().toString();
         //s_productCode=" ";
         if (s_productCode.equals("220")) {
            if (group.isEmpty()) {
                valid = false;
                etGroupCode.setError("indicate group code");
            }else {
                valid=true;
                etGroupCode.setError(null);
            }
        }
         else if (s_productCode.equals("101")) {
             String dob = etDOB.getText().toString();
             String name = etJuniorName.getText().toString();
             if (dob.isEmpty()) {
                 valid = false;
                 etDOB.setError("indicate date of birth");
             } else if (name.isEmpty()) {
                 valid = false;
                 etJuniorName.setError("indicate name of junior");
             } else {
                 etJuniorName.setError(null);
             }
         }
        return valid;
    }

    void validateOnProductType() {
        //s_productCode="101";
        if (s_productCode.equals("101")) {
            tlJunior.setVisibility(View.VISIBLE);
            tlDOB.setVisibility(View.VISIBLE);
            tl_Gc.setVisibility(View.GONE);

        } else if(s_productCode.equals("220")) {
            tl_Gc.setVisibility(View.VISIBLE);
            tlJunior.setVisibility(View.GONE);
            tlDOB.setVisibility(View.GONE);
        }else {
            tlJunior.setVisibility(View.GONE);
            tlDOB.setVisibility(View.GONE);
        }
    }
}
