package com.coretec.agencyApplication.activities;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatButton;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.coretec.agencyApplication.R;
import com.coretec.agencyApplication.activities.gfl.ValidateAgentTunzhengbigBio;
import com.coretec.agencyApplication.activities.gfl.MainGFLDashboard;
import com.coretec.agencyApplication.activities.newmodules.MainDashboardActivity;
import com.coretec.agencyApplication.api.Api;
import com.coretec.agencyApplication.api.GFL;
import com.coretec.agencyApplication.api.requests.ChangePin;
import com.coretec.agencyApplication.api.requests.FingerPrintRequest;
import com.coretec.agencyApplication.api.requests.LoginRequest;
import com.coretec.agencyApplication.api.requests.NameRequest;
import com.coretec.agencyApplication.api.responses.FingerPrintResponse;
import com.coretec.agencyApplication.api.responses.LoginResponse;
import com.coretec.agencyApplication.api.responses.NameResponse;
import com.coretec.agencyApplication.api.responses.PinChangeResponse;
import com.coretec.agencyApplication.utils.Const;
import com.coretec.agencyApplication.utils.PreferenceFileKeys;
import com.coretec.agencyApplication.utils.QuickToast;
import com.coretec.agencyApplication.utils.SharedPrefs;
import com.coretec.agencyApplication.utils.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;

import static com.coretec.agencyApplication.utils.Utils.getFormattedDate;
import static com.coretec.agencyApplication.utils.Utils.getLat;
import static com.coretec.agencyApplication.utils.Utils.getLong;
public class LoginActivity extends AppCompatActivity {

    private QuickToast toast = new QuickToast(this);
    private static final String TAG = "String Agency Banking";
    private AppCompatButton btnLogin;
    private ImageView imgFingerprint;
    private EditText input_agent_phone_number;
    private TextInputLayout til_input_agent_phone_number;
    private EditText input_agent_password;
    private TextInputLayout til_input_agent_password;
    private String agentId, password;
    private ProgressBar progressBar;
    ProgressDialog progressDialog;//ag0159

    public static final String mDSN[] = {"usb,timeout=500", "wbf,timeout=500"};
    public static final String msDSNSerial = "sio,port=COM1,speed=115200,timeout=2000";
    public static final int miUseSerialConnection = 0;
    public static final int miRunningOnRealHardware = 1;
    private Thread mRunningOp = null;
    private final Object mCond = new Object();
    private String msNvmPath = null;
    SharedPreferences sharedPref;
    public String agentCode, loginAgent;
    int version = 2;
    AlertDialog changePasswordDialog;
    public boolean passChanged, isActive, isAdmin, hasBio, isDrs,isCommissioned;
    private TextView changepin;
    private String systime, systimeformatted, servertime;
    public String mydatetime;
    SimpleDateFormat timeFormatter = new SimpleDateFormat("h:mm");


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        SharedPrefs.init(getApplicationContext());
        switch (Api.MSACCO_AGENT) {
            case "http://172.16.11.145:3052/api/AgencyBanking/":
                setContentView(R.layout.activity_login_gfl);
                break;

            case "http://172.16.11.145:3051/api/AgencyBanking/":
                setContentView(R.layout.activity_login_gfl);
                break;

            case "http://172.18.6.8:35088/api/AgencyBanking/":
                String coopnumber = SharedPrefs.read(SharedPrefs.VR_COOPORATE_NO, null);
                if (coopnumber !=null){
                    switch (SharedPrefs.read(SharedPrefs.VR_COOPORATE_NO, null)) {
                        case "CAP022":
                            setContentView(R.layout.activity_login_coopmis);
                            break;

                        case "CAP027":
                            setContentView(R.layout.activity_login_coopmis);
                            break;

                        case "CAP028":
                            setContentView(R.layout.activity_login_coopmis);
                            break;

                        default:
                            setContentView(R.layout.activity_login);
                            break;
                    }

                }else {
                    setContentView(R.layout.activity_login);
                }
                break;
            default:
                setContentView(R.layout.activity_login);
                break;
        }
        Utils.hideKeyboard(LoginActivity.this);
        this.overridePendingTransition(R.anim.move_right_in_activity, R.anim.move_left_out_activity);
        sharedPref = getSharedPreferences(PreferenceFileKeys.PREFS_FILE, Context.MODE_PRIVATE);
        getSystemDate();
        Date now = new Date(System.currentTimeMillis());
        systime = timeFormatter.format(now);

        systimeformatted = systime.replace(":", "");
        progressBar = findViewById(R.id.progressBar1);
        btnLogin = findViewById(R.id.btn_login);

        input_agent_phone_number = findViewById(R.id.input_agent_phone_number);
        input_agent_phone_number.setText(sharedPref.getString(PreferenceFileKeys.AGENT_ID, ""));
        til_input_agent_phone_number = findViewById(R.id.til_input_agent_phone_number);
        input_agent_password = findViewById(R.id.input_agent_password);
        til_input_agent_password = findViewById(R.id.til_input_agent_password);
        changepin = findViewById(R.id.changepin);

        getAgentName(agentId);

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                agentId = input_agent_phone_number.getText().toString().trim();
                password = input_agent_password.getText().toString().trim();

                if (agentId.isEmpty()) {
                    input_agent_phone_number.setText(agentId);
                    til_input_agent_phone_number.setError("Enter your Agent Number ");
                } else if (password.isEmpty()) {
                    til_input_agent_password.setError("Enter your Pin");
                } else {
                    progressBar.setVisibility(View.VISIBLE);
                    loginRequest(agentId, password);
                }
            }
        });
    }

    public void getSystemDate() {
        String timeSettings = android.provider.Settings.System.getString(
                this.getContentResolver(),
                android.provider.Settings.System.AUTO_TIME);
        if (timeSettings.contentEquals("0")) {
            android.provider.Settings.System.putString(
                    this.getContentResolver(),
                    android.provider.Settings.System.AUTO_TIME, "1");
        }
    }


    @Override
    protected void onDestroy() {
        synchronized (mCond) {
            while (mRunningOp != null) {
                mRunningOp.interrupt();
                try {
                    mCond.wait();
                } catch (InterruptedException e) {
                }
            }
        }
        super.onDestroy();
    }

    public void loginRequest(final String agentcode, final String pin) {
        final String TAG = "login request";
        String URL = Api.MSACCO_AGENT + Api.AgentAuthentication;
        final LoginRequest loginRequest = new LoginRequest();
        loginRequest.agentcode = agentcode;
        loginRequest.terminalid = Utils.getIMEI(this);
        loginRequest.agentpin = pin;
        loginRequest.longitude = getLong(this);
        loginRequest.latitude = getLat(this);
        Log.e(TAG, loginRequest.getBody().toString());
        Api.instance(this).request(URL, loginRequest, new Api.RequestListener() {
            @Override
            public void onSuccess(String response) {
                progressBar.setVisibility(View.GONE);
                JSONObject jsonObject = null;
                try {
                    jsonObject = new JSONObject(response);
                    LoginResponse loginResponse = Api.instance(LoginActivity.this).mGson.fromJson(response, LoginResponse.class);
                    if (loginResponse.is_successful) {
                        Const.getInstance().setRequestToken(loginResponse.authorization_response.token);
                        SharedPreferences.Editor editor = sharedPref.edit();
                        editor.putBoolean(PreferenceFileKeys.IS_LOGGED_IN, true);
                        editor.putString(PreferenceFileKeys.AGENT_ID, agentId);
                        SharedPrefs.write(SharedPrefs.AGENT_ID_1, agentId);
                        editor.putString(PreferenceFileKeys.ACCESS_TOKEN, loginResponse.authorization_response.token);
                        editor.apply();

                        Api.pinn=pin;
                        agentCode = input_agent_phone_number.getText().toString();
                        loginAgent = agentCode.substring(0, 3);
                        passChanged = jsonObject.getBoolean("passwordchanged");
                        isActive = jsonObject.getBoolean("active");
                        hasBio = jsonObject.getBoolean("bioregistered");
                        isAdmin = jsonObject.getBoolean("admin");
                        isDrs = jsonObject.getBoolean("isdsr");

                        if (loginAgent.equals("GFL") || loginAgent.equals("gfl".equalsIgnoreCase(""))
                             || loginAgent.equals("CAS") || loginAgent.equals("cas".equalsIgnoreCase(""))) {
                            isCommissioned = jsonObject.getBoolean("iscommissioned");
                        }
                        String showdashboard = "";
                        if (isAdmin) {
                            showdashboard = "admin";
                        } else if (isDrs) {
                            showdashboard = "dsr";
                        } else if(isCommissioned){
                            showdashboard = "commissioned";
                        }else {
                            Toast.makeText(LoginActivity.this, "User type not set!", Toast.LENGTH_SHORT).show();
                        }

                        if (!passChanged) {
                            createDialogChangePassword();
                        } else {
                            if (loginAgent.equals("GFL") || loginAgent.equals("gfl".equalsIgnoreCase(""))
                                    || loginAgent.equals("CAS") || loginAgent.equals("cas".equalsIgnoreCase(""))) {
                                gfldashboard(isActive, showdashboard);
                            } else {
                                otherclientsdashboard(isActive, showdashboard);
                            }
                        }

                    } else {
                        Utils.showAlertDialog(LoginActivity.this,"Login Failed", loginResponse.getError());
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onTokenExpired() {

            }
            @Override
            public void onError(String error) {
                Utils.showAlertDialog(LoginActivity.this, "Failed", error);
            }
        });
    }

    private void gfldashboard(Boolean active, String switchDashboard) {
        if (active) {
            switch (switchDashboard) {
                case "admin":
                    SharedPrefs.dashboard(switchDashboard, LoginActivity.this);
                    SharedPrefs.coporate("gfl", LoginActivity.this);
                    maingflboard();
                    break;
                case "dsr":
                    SharedPrefs.dashboard(switchDashboard, LoginActivity.this);
                    SharedPrefs.coporate("gfl", LoginActivity.this);
                    maingflboard();
                    break;

                case "commissioned":
                    SharedPrefs.dashboard(switchDashboard, LoginActivity.this);
                    SharedPrefs.coporate("gfl", LoginActivity.this);
                    maingflboard();
                    break;

                default:
                    SharedPrefs.dashboard(switchDashboard, LoginActivity.this);
                    SharedPrefs.coporate("gfl", LoginActivity.this);
                    maingflboard();
                    break;

            }
        } else {
            Toast.makeText(LoginActivity.this, "Your account is not active, contact systems admin to activate", Toast.LENGTH_LONG).show();
        }
    }

    private void otherclientsdashboard(Boolean active, String switchDashboard) {
        if (active) {
            switch (switchDashboard) {
                case "admin":
                    SharedPrefs.dashboard(switchDashboard, LoginActivity.this);
                    SharedPrefs.coporate("notgfl", LoginActivity.this);
                    startActivity(new Intent(LoginActivity.this, MainDashboardActivity.class));
                    LoginActivity.this.finish();

                    break;
                default:
                    SharedPrefs.dashboard(switchDashboard, LoginActivity.this);
                    SharedPrefs.coporate("notgfl", LoginActivity.this);
                    startActivity(new Intent(LoginActivity.this, MainDashboardActivity.class));
                    LoginActivity.this.finish();
                    break;
            }
        } else {
            Toast.makeText(LoginActivity.this, "Your account is not active, contact systems admin to activate", Toast.LENGTH_LONG).show();
        }
    }

    public void maingflboard() {
        if (!hasBio) {//rudisha
            startActivity(new Intent(LoginActivity.this, MainGFLDashboard.class));
            LoginActivity.this.finish();
        } else{
            requestFingerPrints();
        }
    }

    public void getAgentName(String agentId) {
        final String TAG = "AGENT NAME";
        String URL = Api.MSACCO_AGENT + GFL.GetAgentname;
        final NameRequest nameRequest = new NameRequest();
        nameRequest.agentid = agentId;
        nameRequest.terminalid = SharedPrefs.read(SharedPrefs.DEVICE_ID, null);
        Log.e(TAG, nameRequest.getBody().toString());
        Api.instance(this).request(URL, nameRequest, new Api.RequestListener() {
            @Override
            public void onSuccess(String response) {
                NameResponse nameResponse = Api.instance(LoginActivity.this).mGson.fromJson(response, NameResponse.class);
                if (nameResponse.is_successful) {
                    SharedPrefs.write(SharedPrefs.AGENT_NAME, nameResponse.getAgent_name());
                } else {
                    //Utils.showAlertDialog(LoginActivity.this,"Failed",nameResponse.getError());
                }
            }

            @Override
            public void onTokenExpired() {

            }

            @Override
            public void onError(String error) {

            }
        });

    }

    public void dislayMessage(String text) {
        mHandler.sendMessage(mHandler.obtainMessage(0, 0, 0, text));
    }

    private Handler mHandler = new Handler() {
        public void handleMessage(Message aMsg) {
            ((TextView) findViewById(R.id.EnrollmentTextView)).setText((String) aMsg.obj);
        }
    };


    @Override
    public void onBackPressed() {
    }

    @Override
    protected void onResume() {
        super.onResume();
        btnLogin.setEnabled(true);
    }

    void requestFingerPrints() {
        final String TAG = "GET FINGERPRINTS";
        String URL = GFL.MSACCO_AGENT + GFL.GetAgentFourFingerPrint;
        final FingerPrintRequest fingerPrintRequest = new FingerPrintRequest();
        fingerPrintRequest.accountidentifier = SharedPrefs.read(SharedPrefs.AGENT_ID_1, null);
        fingerPrintRequest.accountidentifiercode = "1";
        fingerPrintRequest.agentid = SharedPrefs.read(SharedPrefs.AGENT_ID_1, null);
        fingerPrintRequest.terminalid =Utils.getIMEI(this);// SharedPrefs.read(SharedPrefs.DEVICE_ID, null);
        fingerPrintRequest.longitude = getLong(LoginActivity.this);
        fingerPrintRequest.latitude = getLat(LoginActivity.this);
        fingerPrintRequest.date = getFormattedDate();
        Log.e(TAG, fingerPrintRequest.getBody().toString());

        GFL.instance(LoginActivity.this).request(URL, fingerPrintRequest, new GFL.RequestListener() {
            @Override
            public void onSuccess(String response) {
                FingerPrintResponse fingerPrintResponse = GFL.instance(LoginActivity.this)
                        .mGson.fromJson(response, FingerPrintResponse.class);
                if (fingerPrintResponse.is_successful) {
                    btnLogin.setEnabled(false);
                    try {
                        JSONObject jsonObject = new JSONObject(response);

                        String leftRing = jsonObject.getString("leftring");
                        String leftLittle = jsonObject.getString("leftlittle");
                        String rightRing = jsonObject.getString("rightring");
                        String rightLittle = jsonObject.getString("rightlittle");

                        String leftmiddle = jsonObject.getString("leftthree");
                        String rightmiddle = jsonObject.getString("rightthree");
                        String leftindex = jsonObject.getString("leftfour");
                        String rightindex = jsonObject.getString("rightfour");
                        String leftthumb = jsonObject.getString("leftfive");
                        String rightthumb = jsonObject.getString("rightfive");

                        SharedPrefs.write(SharedPrefs.AGENT_LEFT_RING, leftRing);
                        SharedPrefs.write(SharedPrefs.AGENT_LEFT_LITTLE, leftLittle);
                        SharedPrefs.write(SharedPrefs.AGENT_RIGHT_RING, rightRing);
                        SharedPrefs.write(SharedPrefs.AGENT_RIGHT_LITTLE, rightLittle);

                        SharedPrefs.write(SharedPrefs.AGENT_LEFT_MIDDLE, leftmiddle);
                        SharedPrefs.write(SharedPrefs.AGENT_RIGHT_MIDDLE, rightmiddle);
                        SharedPrefs.write(SharedPrefs.AGENT_LEFT_INDEX, leftindex);
                        SharedPrefs.write(SharedPrefs.AGENT_RIGHT_INDEX, rightindex);
                        SharedPrefs.write(SharedPrefs.AGENT_LEFT_THUMB, leftthumb);
                        SharedPrefs.write(SharedPrefs.AGENT_RIGHT_THUMB, rightthumb);

                        SharedPrefs.transactionstypelogs(16, LoginActivity.this);
                        SharedPrefs.growerslogs(agentId, LoginActivity.this);
                        startActivity(new Intent(LoginActivity.this, ValidateAgentTunzhengbigBio.class));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                } else {
                    Toast.makeText(LoginActivity.this, "Could not get fingerprints", Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onTokenExpired() {

            }

            @Override
            public void onError(String error) {

            }
        });

    }

    private void createDialogChangePassword() {
        LayoutInflater myInflater = LayoutInflater.from(this);
        View v = myInflater.inflate(R.layout.dialog_change_password, null);
        //define and access view attributes
        final EditText et_agent_id = v.findViewById(R.id.et_agent_id);
        final EditText old_pass = v.findViewById(R.id.et_old_pass);
        final EditText new_pass = v.findViewById(R.id.et_new_pass);
        final EditText confirm_pass = v.findViewById(R.id.et_confirm_pass);
        Button submit = v.findViewById(R.id.button_submit);
        et_agent_id.setText(input_agent_phone_number.getText().toString().trim());
        et_agent_id.setKeyListener(null);

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String agentid = et_agent_id.getText().toString();
                String pass_word = input_agent_password.getText().toString();
                String oldPass = old_pass.getText().toString();
                String newPass = new_pass.getText().toString();
                String confirmPass = confirm_pass.getText().toString();

                if (!oldPass.equals(pass_word)) {
                    old_pass.setError("Pin does not match with the Old Password!");
                } else if (agentid.isEmpty()) {
                    et_agent_id.setError("Please enter your agent id!");
                } else if (oldPass.isEmpty()) {
                    old_pass.setError("Please enter your old Pin!");
                } else if (newPass.isEmpty()) {
                    new_pass.setError("Please enter your new Pin!");
                } else if (confirmPass.isEmpty()) {
                    confirm_pass.setError("Please confirm your new Pin!");
                } else if (!confirmPass.equals(newPass)) {
                    confirm_pass.setError("Pin does not match with the new Pin!");
                } else {
                    progressDialog = new ProgressDialog(LoginActivity.this);
                    progressDialog.setMessage("Processing your request...");
                    progressDialog.setCancelable(false);
                    progressDialog.show();
                    requestPasswordChange(agentid, pass_word, confirmPass);
                }

            }
        });

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setView(v);
        changePasswordDialog = builder.create();
        changePasswordDialog.show();
        changePasswordDialog.setCancelable(false);
    }

    private void requestPasswordChange(String agentidd, String userpin, String newpin) {

        String URL = Api.MSACCO_AGENT + Api.ChangePinAgent;
        final ChangePin pinchange = new ChangePin();
        pinchange.agentid = agentidd;
        pinchange.terminalid = Utils.getIMEI(this);
        pinchange.old_pin = userpin;
        pinchange.new_pin = newpin;
        pinchange.accountidentifier = agentidd;
        pinchange.accountidentifiercode = "0";
        pinchange.longitude = getLong(this);
        pinchange.latitude = getLat(this);
        pinchange.date = getFormattedDate();

        Api.instance(this).request(URL, pinchange, new Api.RequestListener() {
            @Override
            public void onSuccess(String response) {
                progressBar.setVisibility(View.GONE);

                JSONObject jsonObject = null;
                try {
                    jsonObject = new JSONObject(response);

                    PinChangeResponse pinchangeresponseResponse = Api.instance(LoginActivity.this).mGson.fromJson(response, PinChangeResponse.class);
                    if (pinchangeresponseResponse.is_successful) {

                        progressDialog.dismiss();
                        AlertDialog.Builder builder1 = new AlertDialog.Builder(LoginActivity.this);
                        builder1.setMessage("Pin Changed Succesfully");
                        builder1.setTitle("Success");
                        builder1.setCancelable(false);

                        builder1.setPositiveButton(
                                "OK",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();
                                        input_agent_password.setText("");
                                        changePasswordDialog.dismiss();
                                    }
                                });

                        AlertDialog alert11 = builder1.create();
                        alert11.show();

                    } else {
                        Utils.showAlertDialog(LoginActivity.this, "Please try again", pinchangeresponseResponse.getError());
                        progressDialog.dismiss();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }

            @Override
            public void onTokenExpired() {

            }

            @Override
            public void onError(String error) {

            }
        });
    }

}
