package com.coretec.agencyApplication.activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.Toast;

import com.cloudpos.jniinterface.PrinterInterface;
import com.coretec.agencyApplication.R;
import com.coretec.agencyApplication.activities.newmodules.MainDashboardActivity;
import com.coretec.agencyApplication.api.Api;
import com.coretec.agencyApplication.api.requests.AccountDetailsRequest;
import com.coretec.agencyApplication.api.requests.RequestDeposit;
import com.coretec.agencyApplication.api.responses.AccDetailsResponse;
import com.coretec.agencyApplication.api.responses.DepositResponse;
import com.coretec.agencyApplication.api.responses.SaccoSharesAcc;
import com.coretec.agencyApplication.model.SharesAccountDetails;
import com.coretec.agencyApplication.utils.PreferenceFileKeys;
import com.coretec.agencyApplication.utils.QuickToast;
import com.coretec.agencyApplication.utils.SharedPrefs;
import com.coretec.agencyApplication.utils.Utils;
import com.coretec.agencyApplication.wizarpos.function.printer.PrinterCommand;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import static com.coretec.agencyApplication.utils.Utils.getFormattedDate;
import static com.coretec.agencyApplication.utils.Utils.getLat;
import static com.coretec.agencyApplication.utils.Utils.getLong;


public class ShareDeposit extends BaseActivity {

    private Button btnConfirm,btn_account_search;
    private ProgressBar deposit_progressBar;
    private TextInputLayout layout_account_to_deposit;
    private TextInputLayout layout_amount_to_deposit;
    private TextInputLayout layout_depositors_name;
    private TextInputLayout layout_depositors_phone_number;
    private EditText identifier_deposit;
    private EditText amount_to_deposit;
    private EditText depositors_name, depositors_phone_number;
    private String deposit_amount, deposit_account_number, set_depositors_name, set_depositors_phone_number;
    private String identifiercode = "";
    private String accountNo = "";
    private String phoneNumber = "";

    private Spinner spinner;
    private ArrayList<String> arrayList = new ArrayList<>();
    private ArrayAdapter<SharesAccountDetails> adapter;

    private Spinner spinner2;
    private ArrayList<String> arrayList2 = new ArrayList<>();
    private ArrayAdapter<String> adapter2;

    private ProgressBar progressBar;
    private ProgressDialog progressDialog;
    SharedPreferences sharedPreferences;
    boolean find=true;
    private QuickToast toast = new QuickToast(this);

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        SharedPrefs.init(getApplicationContext());
        setContentView(R.layout.activity_cash_deposit);

        this.overridePendingTransition(R.anim.move_right_in_activity, R.anim.move_left_out_activity);

        sharedPreferences = getSharedPreferences(PreferenceFileKeys.PREFS_FILE, Context.MODE_PRIVATE);

        deposit_progressBar = findViewById(R.id.deposit_progressBar);
        deposit_progressBar.setVisibility(View.GONE);


        identifier_deposit = findViewById(R.id.deposit_identifier);
        amount_to_deposit = findViewById(R.id.amount_to_deposit);
        depositors_name = findViewById(R.id.depositors_name);
        depositors_phone_number = findViewById(R.id.depositors_phone_number);
        /*layout_account_to_deposit = findViewById(R.id.layout_account_to_deposit);*/
        layout_amount_to_deposit = findViewById(R.id.layout_amount_to_deposit);
        layout_depositors_name = findViewById(R.id.layout_depositors_name);
        layout_depositors_phone_number = findViewById(R.id.layout_depositors_phone_number);

        progressBar = findViewById(R.id.identity_pb);

        btn_account_search = findViewById(R.id.btn_account_search);
        btn_account_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String identifier_number = identifier_deposit.getText().toString();

                if(identifier_deposit.getText().toString().matches("")) {
                    toast.swarn("Please the identifier number to proceed!");
                } else {
                    progressBar.setVisibility(View.VISIBLE);
                    requestGetAccountDetails(identifier_number);
                }
            }
        });

        TextWatcher textWatcher = new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                adapter.clear();
                spinner.setAdapter(adapter);
            }
        };

        identifier_deposit.addTextChangedListener(textWatcher);

        spinner = findViewById(R.id.deposit_accounts);
        adapter = new ArrayAdapter<SharesAccountDetails>(getApplicationContext(),
                R.layout.simple_spinner_dropdown_item, new ArrayList<SharesAccountDetails>());
        adapter.setDropDownViewResource(R.layout.simple_spinner_dropdown_item);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                SharesAccountDetails accountDetails = (SharesAccountDetails) parent.getItemAtPosition(position);
                accountNo = accountDetails.getAccountno();


                depositors_phone_number.setText(accountDetails.getPhonenumber());
                depositors_phone_number.setFocusable(false);

                depositors_name.setText(accountDetails.getAccountname());
                depositors_name.setFocusable(false);
                String account_code = accountDetails.getTransactionCode();
                SharedPrefs.write(SharedPrefs.ACCOUNT_CODE, account_code);

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spinner2 = findViewById(R.id.spinner_deposit);
        adapter2 = new ArrayAdapter<String>(getApplicationContext(), R.layout.simple_spinner_dropdown_item, arrayList2);
        adapter2.setDropDownViewResource(R.layout.simple_spinner_dropdown_item);
        spinner2.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                identifiercode = String.valueOf(position);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        btnConfirm = findViewById(R.id.btn_submit_deposit);
        String coopnumber = SharedPrefs.read(SharedPrefs.VR_COOPORATE_NO, null);
        if (coopnumber !=null){
            switch (SharedPrefs.read(SharedPrefs.VR_COOPORATE_NO, null)) {
                case "CAP005":
                    btnConfirm.setBackgroundResource(R.drawable.btn_rounded_primary_coloraccentforcoopmis);
                    break;
                case "CAP022":
                    btnConfirm.setBackgroundResource(R.drawable.btn_rounded_primary_coloraccentforcoopmis);
                    break;
                default:
                    btnConfirm.setBackgroundResource(R.drawable.btn_rounded_primary_coloraccent);
                    break;
            }

        }else {
            btnConfirm.setBackgroundResource(R.drawable.btn_rounded_primary_coloraccent);
        }

        btnConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                deposit_amount = amount_to_deposit.getText().toString().trim();
                deposit_account_number = identifier_deposit.getText().toString().trim();
                set_depositors_name = depositors_name.getText().toString().trim();
                set_depositors_phone_number = depositors_phone_number.getText().toString().trim();

                if (deposit_amount.isEmpty()) {
                    layout_account_to_deposit.setError("Enter the Amount to be deposited");
                } else if (deposit_account_number.isEmpty()) {
                    layout_amount_to_deposit.setError("Enter the Member Account Number");
                } else if (set_depositors_name.isEmpty()) {
                    layout_depositors_name.setError("Enter the Name of the Depositor");
                } else if (set_depositors_phone_number.isEmpty()) {
                    layout_depositors_name.setError("Enter the Phone Number of the Depositor");
                } else {
                    Log.e("deposit_amount", deposit_amount);
                    Log.e("corporate_number", set_depositors_name);
                    deposit_progressBar.setVisibility(View.VISIBLE);
                    progressDialog = new ProgressDialog(ShareDeposit.this);
                    progressDialog.setMessage("Processing your request, please wait...");
                    progressDialog.setCancelable(false);
                    progressDialog.show();
                    submittransaction(find);
                    btnConfirm.setClickable(false);

                }
            }
        });
    }

    void requestGetAccountDetails(String identifier) {

        final String TAG = "MemberAccounts";
//        EasyLocationMod easyLocationMod = new EasyLocationMod(CashWithdrawal.this);

        String URL = Api.MSACCO_AGENT + Api.GetAllMemberSaccoDetails;

        final AccountDetailsRequest accountDetails = new AccountDetailsRequest();
        accountDetails.agentid = sharedPreferences.getString(PreferenceFileKeys.AGENT_ID, "");
        accountDetails.corporateno = MainDashboardActivity.corporateno;
        accountDetails.accountidentifier = identifier;
        accountDetails.accountidentifiercode = identifiercode;
        accountDetails.transactiontype = "S";
        accountDetails.terminalid = MainDashboardActivity.imei;
        accountDetails.longitude = getLong(this);
        accountDetails.latitude = getLat(this);
        accountDetails.date = getFormattedDate();

        Log.e(TAG, accountDetails.getBody().toString());

        Api.instance(this).request(URL, accountDetails, new Api.RequestListener() {
            @Override
            public void onSuccess(String response) {
                deposit_progressBar.setVisibility(View.GONE);

                AccDetailsResponse accountDetailsResponse = Api.instance(ShareDeposit.this)
                        .mGson.fromJson(response, AccDetailsResponse.class);

                if (accountDetailsResponse.is_successful) {
                    progressBar.setVisibility(View.GONE);
                    List<SharesAccountDetails> accountDetailsList = new ArrayList<>();
                    for (SaccoSharesAcc saccoAccounts : accountDetailsResponse.getSaccoAccounts()) {
                        accountDetailsList.addAll(saccoAccounts.getAccountDetails());
                    }
                    adapter.addAll(accountDetailsList);
                    spinner.setAdapter(adapter);

                } else {
                    progressBar.setVisibility(View.GONE);
                    Toast.makeText(ShareDeposit.this, "Accounts Fetching Failed", Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onTokenExpired() {

            }

            @Override
            public void onError(String error) {

            }
        });
    }

    void requestPostDeposit(final String depositors_name, final String depositors_phone_number, final int amount) {

        final String TAG = "deposit cash ";
        Log.e(TAG, String.valueOf(amount));

        String URL = Api.MSACCO_AGENT + Api.ShareDeposits;

        RequestDeposit requestDeposit = new RequestDeposit();
        requestDeposit.agentid = sharedPreferences.getString(PreferenceFileKeys.AGENT_ID, "");
        requestDeposit.corporate_no = MainDashboardActivity.corporateno;
        requestDeposit.idno = identifier_deposit.getText().toString();
        requestDeposit.memberaccno = accountNo;
        requestDeposit.depositorsname = depositors_name;
        requestDeposit.AccountType = SharedPrefs.read(SharedPrefs.ACCOUNT_CODE, null);
        requestDeposit.depositorsphoneno = depositors_phone_number;
        requestDeposit.amount = amount;
        requestDeposit.terminalid = MainDashboardActivity.imei;
        requestDeposit.longitude = getLong(this);
        requestDeposit.latitude = getLat(this);
        requestDeposit.date = getFormattedDate();

        Log.e(TAG, requestDeposit.getBody().toString());

        Api.instance(this).request(URL, requestDeposit, new Api.RequestListener() {
            @Override
            public void onSuccess(String response) {
                deposit_progressBar.setVisibility(View.GONE);
                Log.e(TAG, response);

                final DepositResponse depositResponse = Api.instance(ShareDeposit.this).mGson.fromJson(response, DepositResponse.class);
                if (depositResponse.is_successful) {

                    final String agentName = sharedPreferences.getString(PreferenceFileKeys.AGENT_ACCOUNT_NAME, "");
                    final String saccoMotto = sharedPreferences.getString(PreferenceFileKeys.AGENT_SACCO_MOTTO, "");


                    if (PrinterInterface.queryStatus() == 0) {
                        Utils.showAlertDialog(ShareDeposit.this, "NO THERMAL PAPER!", "Put thermal paper and click OK to continue.", new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                PrinterInterface.open();
                                writetest(accountNo, agentName, depositors_name, depositors_phone_number, depositResponse.getAmount(), depositResponse.getMemberaccno(), depositResponse.getSacconame(), saccoMotto, depositResponse.getCustomername(), depositResponse.getReceiptno(), depositResponse.getTransactiondate(), depositResponse.getTransactiontype());
                                progressDialog.dismiss();

                                Utils.showAlertDialog(ShareDeposit.this, "Share Deposited Successfully", " Press Ok to print another Receipt ",
                                 new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        PrinterInterface.open();
                                        writetest1(accountNo, agentName, depositors_name, depositors_phone_number, depositResponse.getAmount(), depositResponse.getMemberaccno(), depositResponse.getSacconame(), saccoMotto, depositResponse.getCustomername(), depositResponse.getReceiptno(), depositResponse.getTransactiondate(), depositResponse.getTransactiontype());
                                        finish();
                                        Intent i = new Intent(ShareDeposit.this, MainDashboardActivity.class);
                                        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                                        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                        startActivity(i);
                                    }
                                });
                            }
                        });
                        PrinterInterface.close();
                    } else {
                        PrinterInterface.open();
                        writetest(accountNo, agentName, depositors_name, depositors_phone_number, depositResponse.getAmount(), depositResponse.getMemberaccno(), depositResponse.getSacconame(), saccoMotto, depositResponse.getCustomername(), depositResponse.getReceiptno(), depositResponse.getTransactiondate(), depositResponse.getTransactiontype());
                        progressDialog.dismiss();

                        Utils.showAlertDialog(ShareDeposit.this, "Share Deposited Successfully", " Press Ok to print another Receipt ", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                PrinterInterface.open();
                                writetest1(accountNo, agentName, depositors_name, depositors_phone_number, depositResponse.getAmount(), depositResponse.getMemberaccno(), depositResponse.getSacconame(), saccoMotto, depositResponse.getCustomername(), depositResponse.getReceiptno(), depositResponse.getTransactiondate(), depositResponse.getTransactiontype());
                                finish();
                                Intent i = new Intent(ShareDeposit.this, MainDashboardActivity.class);
                                i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                                i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(i);
                            }
                        });
                        PrinterInterface.close();
                    }

                } else {
                    progressDialog.dismiss();
                    deposit_progressBar.setVisibility(View.GONE);
                    Utils.showAlertDialog(ShareDeposit.this, "Deposit Failed", depositResponse.getError());
                }

            }

            @Override
            public void onTokenExpired() {

            }

            @Override
            public void onError(String error) {

            }
        });

    }

    public  void submittransaction(boolean value){
        if(value) {
            try {

                if (isOnline()) {
                    requestPostDeposit(set_depositors_name, set_depositors_phone_number, Integer.parseInt(deposit_amount));
                } else {
                    Toast.makeText(ShareDeposit.this, "Check your Internet Connection and try again", Toast.LENGTH_LONG).show();
                    progressDialog.dismiss();
                }


                //checkConnection();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    protected boolean isOnline() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        if (netInfo != null && netInfo.isConnectedOrConnecting()) {
            return true;
        } else {
            return false;
        }
    }

    public void writetest(String accountNum, String agentName, String depositorsName, String depositors_phone_number, int amount, String accountNo, String saccoame, String saccoMotto, String customerName, String receiptNumber, String dateed, String transactionType) {
        try {

            byte[] arrySaccoName = null;
            byte[] arryCustomerName = null;
            byte[] arryDepositAmt = null;
            byte[] arryAmt = null;

            byte[] arryMotto = null;
            byte[] terminalNo = null;
            byte[] receiptNo = null;
            byte[] date = null;
            byte[] transactionTYpe = null;
            byte[] account_dposited_to = null;
            byte[] servedBy = null;
            byte[] signature = null;
            byte[] id = null;
            byte[] depositedBy = null;
            byte[] phone_number = null;
            byte[] customerReceipt = null;

            try {
                arrySaccoName = String.valueOf("       " + String.valueOf(saccoame)).getBytes("GB2312");
                terminalNo = String.valueOf(" Terminal ID :" + Utils.getIMEI(ShareDeposit.this)).getBytes("GB2312");
                receiptNo = String.valueOf(" Receipt Number :" + receiptNumber).getBytes("GB2312");
                date = String.valueOf("Date : " + dateed).getBytes("GB2312");
                arryCustomerName = String.valueOf("Name :" + depositorsName).getBytes("GB2312");//customername above
                transactionTYpe = String.valueOf("    " + transactionType).getBytes("GB2312");
                arryDepositAmt = String.valueOf("Deposited Amount is :").getBytes("GB2312");
                arryAmt = String.valueOf(String.valueOf("          " + SharedPrefs.read(SharedPrefs.CURRENCY, null) + " :" + amount)).getBytes("GB2312");
                account_dposited_to = String.valueOf("ACC NO :" + accountNum).getBytes("GB2312");
                servedBy = String.valueOf("You were Served By :" + agentName).getBytes("GB2312");
                depositedBy = String.valueOf("DepositedBy : " + depositorsName).getBytes("GB2312");
                phone_number = String.valueOf("Depositors' PhoneNo : " + depositors_phone_number).getBytes("GB2312");
                id = String.valueOf("ID :_______________________").getBytes("GB2312");
                signature = String.valueOf("SIGNATURE : ______________").getBytes("GB2312");
                customerReceipt = String.valueOf("         CUSTOMER COPY         ").getBytes("GB2312");

                arryMotto = String.valueOf("     "+ saccoMotto).getBytes("GB2312");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            PrinterInterface.begin();

            byte[] cmd;
            cmd = new byte[]{(byte) 0x1B, (byte) 0x40};

            cmd = new byte[]{
                    (byte) 0x1B, (byte) 0x45, (byte) 0};

            write(cmd);
            write(arrySaccoName);
            writeLineBreak(1);
            write(terminalNo);
            writeLineBreak(1);
            write(receiptNo);
            writeLineBreak(1);
            write(date);
            writeLineBreak(1);
            write(arryCustomerName);
            writeLineBreak(1);
            write(transactionTYpe);
            // print line break
            writeLineBreak(2);

            write(arryDepositAmt);
            writeLineBreak(1);
            write(arryAmt);
            writeLineBreak(1);
            write(account_dposited_to);
            writeLineBreak(1);
            write(depositedBy);
            writeLineBreak(1);
            write(phone_number);
            writeLineBreak(1);
            write(id);
            writeLineBreak(1);
            write(signature);
            writeLineBreak(1);
            write(servedBy);
            writeLineBreak(2);
            write(customerReceipt);
            writeLineBreak(2);
            write(arryMotto);
            writeLineBreak(4);
            PrinterInterface.end();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void writetest1(String accountNum, String agentName, String depositorsName,
                           String depositors_phone_number, int amount, String accountNo, String saccoame,
                           String saccoMotto, String customerName, String receiptNumber, String dateed, String transactionType) {
        try {

            byte[] arrySaccoName = null;
            byte[] arryCustomerName = null;
            byte[] arryDepositAmt = null;
            byte[] arryAmt = null;

            byte[] arryMotto = null;
            byte[] terminalNo = null;
            byte[] receiptNo = null;
            byte[] date = null;
            byte[] transactionTYpe = null;
            byte[] account_dposited_to = null;
            byte[] servedBy = null;
            byte[] signature = null;
            byte[] id = null;
            byte[] depositedBy = null;
            byte[] phone_number = null;
            byte[] agentReceipt = null;

            try {
                arrySaccoName = String.valueOf("       " + String.valueOf(saccoame)).getBytes("GB2312");
                terminalNo = String.valueOf(" Terminal ID :" + Utils.getIMEI(ShareDeposit.this)).getBytes("GB2312");
                receiptNo = String.valueOf(" Receipt Number : " + receiptNumber).getBytes("GB2312");
                date = String.valueOf("Date : " + dateed).getBytes("GB2312");
                arryCustomerName = String.valueOf("Name : " + customerName).getBytes("GB2312");
                transactionTYpe = String.valueOf("      " + transactionType).getBytes("GB2312");
                arryDepositAmt = String.valueOf("   Deposited Amount is :").getBytes("GB2312");
                arryAmt = String.valueOf(String.valueOf("     " + SharedPrefs.read(SharedPrefs.CURRENCY, null) + " :" + amount)).getBytes("GB2312");
                account_dposited_to = String.valueOf(" ACC NO : " + accountNum).getBytes("GB2312");
                servedBy = String.valueOf(" You were Served By : " + agentName).getBytes("GB2312");
                depositedBy = String.valueOf(" DepositedBy : " + depositorsName).getBytes("GB2312");
                phone_number = String.valueOf(" Depositors' PhoneNo : " + depositors_phone_number).getBytes("GB2312");
                id = String.valueOf("ID :_________________________").getBytes("GB2312");
                signature = String.valueOf("SIGNATURE : _______________").getBytes("GB2312");
                agentReceipt = String.valueOf("         AGENT COPY         ").getBytes("GB2312");

                arryMotto = String.valueOf("     "+ saccoMotto).getBytes("GB2312");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            PrinterInterface.begin();

            byte[] cmd;
            cmd = new byte[]{(byte) 0x1B, (byte) 0x40};

            cmd = new byte[]{
                    (byte) 0x1B, (byte) 0x45, (byte) 0};

            write(cmd);
            write(arrySaccoName);
            writeLineBreak(1);
            write(terminalNo);
            writeLineBreak(1);
            write(receiptNo);
            writeLineBreak(1);
            write(date);
            writeLineBreak(1);
            write(arryCustomerName);
            writeLineBreak(1);
            write(transactionTYpe);
            // print line break
            writeLineBreak(2);

            write(arryDepositAmt);
            writeLineBreak(1);
            write(arryAmt);
            writeLineBreak(1);
            write(account_dposited_to);
            writeLineBreak(1);
            write(depositedBy);
            writeLineBreak(1);
            write(phone_number);
            writeLineBreak(1);
            write(id);
            writeLineBreak(1);
            write(signature);
            writeLineBreak(1);
            write(servedBy);
            writeLineBreak(2);
            write(agentReceipt);
            writeLineBreak(2);
            write(arryMotto);
            writeLineBreak(4);
            PrinterInterface.end();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void write(final byte[] arryData) {
        int result = PrinterInterface.write(arryData, arryData.length);
    }

    // print line break
    private void writeLineBreak(int lineNumber) {
        // write(new byte[]{'\n'});
        write(PrinterCommand.getCmdEscDN(lineNumber));
    }




    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.move_left_in_activity, R.anim.move_right_out_activity);
    }
}
