package com.coretec.agencyApplication.activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.cloudpos.jniinterface.PrinterInterface;
import com.coretec.agencyApplication.R;
import com.coretec.agencyApplication.activities.newmodules.MainDashboardActivity;
import com.coretec.agencyApplication.api.Api;
import com.coretec.agencyApplication.api.requests.ActiveLoansRequest;
import com.coretec.agencyApplication.api.requests.LoanRepaymentRequest;
import com.coretec.agencyApplication.api.requests.RequestMemberName;
import com.coretec.agencyApplication.api.responses.ActiveLoansResponse;
import com.coretec.agencyApplication.api.responses.LoanRepaymentResponse;
import com.coretec.agencyApplication.api.responses.MemberNameResponse;
import com.coretec.agencyApplication.model.Loans;
import com.coretec.agencyApplication.utils.PreferenceFileKeys;
import com.coretec.agencyApplication.utils.QuickToast;
import com.coretec.agencyApplication.utils.SharedPrefs;
import com.coretec.agencyApplication.utils.Utils;
import com.coretec.agencyApplication.wizarpos.function.printer.PrinterCommand;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

import static com.coretec.agencyApplication.utils.Utils.getFormattedDate;
import static com.coretec.agencyApplication.utils.Utils.getLat;
import static com.coretec.agencyApplication.utils.Utils.getLong;


public class LoanRepayment extends BaseActivity {

    SharedPreferences sharedPreferences;
    private TextInputLayout input_layout_loan_identifier;
    private EditText txt_loan_amount,loanDepositedBy,loanIdentifier;
    private TextInputLayout input_layout_loan_amount;
    private Spinner activeLoanSpinner;
    private Button btnConfirmLoanRepayment,btn_account_search;
    private String identifier_number, payable_amount;
    private ArrayList<String> loansArrayList;
    private ArrayAdapter<String> adapter;
    private String loanAccountNo, membername, memberphone;
    private String splitedLoanAccNo, loannumberprintout;
    private TextView txtFullChequeDepositName,loading;
    ProgressBar progressBar;
    ProgressDialog progressDialog;
    boolean find=true;
    private QuickToast toast = new QuickToast(this);

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_loan_repayment);
        this.overridePendingTransition(R.anim.move_right_in_activity, R.anim.move_left_out_activity);
        sharedPreferences = getSharedPreferences(PreferenceFileKeys.PREFS_FILE, Context.MODE_PRIVATE);
        progressBar = findViewById(R.id.loanPb);
        activeLoanSpinner = findViewById(R.id.loan_repayment_loans);
        txtFullChequeDepositName = findViewById(R.id.txtFullChequeDepositName);
        loanDepositedBy = findViewById(R.id.loanDepositedBy);
        loading = findViewById(R.id.loading);

        activeLoanSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                loanAccountNo = (String) parent.getItemAtPosition(position);
                String[] separated = loanAccountNo.split(":");
                splitedLoanAccNo = separated[1];
                loannumberprintout= separated[0];
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        loanIdentifier = findViewById(R.id.loan_repayment_identifier);
        txt_loan_amount = findViewById(R.id.payable_txt_loan_amount);
        loansArrayList = new ArrayList<String>();
        input_layout_loan_identifier = findViewById(R.id.input_layout_loan_repayment_identifier);
        input_layout_loan_amount = findViewById(R.id.layout_amount_to_withdraw);
        btn_account_search = findViewById(R.id.btn_account_search);
        btn_account_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String loanidentifier = loanIdentifier.getText().toString();
                if(loanIdentifier.getText().toString().matches("")) {
                    toast.swarn("Please enter ID number to proceed!");
                } else {
                    loansArrayList.clear();
                    progressBar.setVisibility(View.VISIBLE);
                    requestMemberDetails(loanidentifier);
                    requestActiveLoans(loanidentifier);
                }
            }
        });

        TextWatcher textWatcher = new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {

                adapter = new ArrayAdapter<String>(getApplicationContext(),
                        R.layout.simple_spinner_dropdown_item, new ArrayList<String>());
                adapter.setDropDownViewResource(R.layout.simple_spinner_dropdown_item);
                adapter.addAll(loansArrayList);
                activeLoanSpinner.setAdapter(adapter);

            }
        };

        loanIdentifier.addTextChangedListener(textWatcher);
        btnConfirmLoanRepayment = findViewById(R.id.submitRepaymentRequest);

        String coopnumber = SharedPrefs.read(SharedPrefs.VR_COOPORATE_NO, null);
        if (coopnumber !=null){
            switch (SharedPrefs.read(SharedPrefs.VR_COOPORATE_NO, null)) {
                case "CAP005":
                    btnConfirmLoanRepayment.setBackgroundResource(R.drawable.btn_rounded_primary_coloraccentforcoopmis);
                    break;
                case "CAP022":
                    btnConfirmLoanRepayment.setBackgroundResource(R.drawable.btn_rounded_primary_coloraccentforcoopmis);
                    break;
                default:
                    btnConfirmLoanRepayment.setBackgroundResource(R.drawable.btn_rounded_primary_coloraccent);
                    break;
            }

        }else {
            btnConfirmLoanRepayment.setBackgroundResource(R.drawable.btn_rounded_primary_coloraccent);
        }

        btnConfirmLoanRepayment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                identifier_number = loanIdentifier.getText().toString().trim();
                payable_amount = txt_loan_amount.getText().toString().trim();

                if (identifier_number.isEmpty()) {
                    input_layout_loan_identifier.setError("Enter the Member Account Number");
                } else if (payable_amount.isEmpty()) {
                    txt_loan_amount.setError("Enter the Amount to Pay Loan");
                }else if (txtFullChequeDepositName.getText().equals("*******")) {
                    Toast.makeText(LoanRepayment.this, "Please click on search button", Toast.LENGTH_SHORT).show();
                }else if (loanDepositedBy.getText().toString().matches("")) {
                    Toast.makeText(LoanRepayment.this, "Please Enter depositors name.", Toast.LENGTH_SHORT).show();
                }  else {

                    progressDialog = new ProgressDialog(LoanRepayment.this);
                    progressDialog.setMessage("Processing your request, please wait...");
                    progressDialog.setCancelable(false);
                    progressDialog.show();
                    submittransaction(find);
                }
            }
        });

    }

    public  void submittransaction(boolean value){
        if(value) {
            try {
                if (isOnline()) {
                    requestLoanRepayment(identifier_number, Double.parseDouble(payable_amount));
                } else {
                    Toast.makeText(LoanRepayment.this, "Check your Internet Connection and try again", Toast.LENGTH_LONG).show();
                    progressDialog.dismiss();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    protected boolean isOnline() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo netInfo = cm.getActiveNetworkInfo();
        if (netInfo != null && netInfo.isConnectedOrConnecting()) {
            return true;
        } else {
            return false;
        }
    }
    void requestActiveLoans(final String identifier) {
        final String TAG = "MemberAccounts";

        Log.e(TAG, String.valueOf(identifier));

        String URL = Api.MSACCO_AGENT + Api.GetMemberActiveLoans;

        final ActiveLoansRequest accountDetails = new ActiveLoansRequest();

        accountDetails.corporate_no = MainDashboardActivity.corporateno;
        accountDetails.accountidentifier = identifier;
        accountDetails.accountidentifiercode = "0";
        accountDetails.agentid = sharedPreferences.getString(PreferenceFileKeys.AGENT_ID, "");
        accountDetails.terminalid = Utils.getIMEI(this);
        accountDetails.longitude = Utils.getLong(this);
        accountDetails.latitude = Utils.getLat(this);
        accountDetails.date = Utils.getFormattedDate();

        Log.e(TAG, accountDetails.getBody().toString());

        Api.instance(this).request(URL, accountDetails, new Api.RequestListener() {
            @Override
            public void onSuccess(String response) {
                ActiveLoansResponse activeLoansResponse = Api.instance(LoanRepayment.this).mGson.fromJson(response, ActiveLoansResponse.class);

                if (activeLoansResponse.is_successful) {
                    progressBar.setVisibility(View.GONE);
                    loansArrayList.clear();
                    for (Loans saccoAccounts : activeLoansResponse.getActive_loans()) {
                        loansArrayList.add(saccoAccounts.getLoantypename()+"  :" + saccoAccounts.getLoan_no()+":  (" + saccoAccounts.getLoanbalance()+")");
                    }

                        if (activeLoansResponse.getActive_loans().size() < 1) {
                            Toast.makeText(LoanRepayment.this, "User has no active loans", Toast.LENGTH_LONG).show();
                          }

                    adapter = new ArrayAdapter<String>(getApplicationContext(),
                            R.layout.simple_spinner_dropdown_item, new ArrayList<String>());
                    adapter.setDropDownViewResource(R.layout.simple_spinner_dropdown_item);
                    adapter.addAll(loansArrayList);
                    activeLoanSpinner.setAdapter(adapter);

                } else {
                    progressBar.setVisibility(View.GONE);
                    Toast.makeText(LoanRepayment.this, "Encountered a problem! Please Try again", Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onTokenExpired() {

            }

            @Override
            public void onError(String error) {

            }
        });


    }

    void requestLoanRepayment(final String identifier, final double payable_amount) {
        final String TAG = "MemberAccounts";
        String URL = Api.MSACCO_AGENT + Api.LoanRepayment;

        final LoanRepaymentRequest loanRepaymentRequest = new LoanRepaymentRequest();
        loanRepaymentRequest.corporate_no = MainDashboardActivity.corporateno;
        loanRepaymentRequest.accountidentifier = identifier;
        loanRepaymentRequest.accountidentifiercode = "0";
        loanRepaymentRequest.loan_no = splitedLoanAccNo;
        loanRepaymentRequest.repayment_amount = payable_amount;
        loanRepaymentRequest.agentid = sharedPreferences.getString(PreferenceFileKeys.AGENT_ID, "");
        loanRepaymentRequest.terminalid = MainDashboardActivity.imei;
        loanRepaymentRequest.longitude = getLong(this);
        loanRepaymentRequest.latitude = getLat(this);
        loanRepaymentRequest.date = getFormattedDate();
        Api.instance(this).request(URL, loanRepaymentRequest, new Api.RequestListener() {
            @Override
            public void onSuccess(String response) {
                final LoanRepaymentResponse loanRepaymentResponse = Api.instance(LoanRepayment.this).mGson.fromJson(response, LoanRepaymentResponse.class);
                if (loanRepaymentResponse.is_successful) {

                    final String agentName = sharedPreferences.getString(PreferenceFileKeys.AGENT_ACCOUNT_NAME, "");
                    final String saccoMotto = sharedPreferences.getString(PreferenceFileKeys.AGENT_SACCO_MOTTO, "");

                    if (PrinterInterface.queryStatus() == 0) {
                        Utils.showAlertDialog(LoanRepayment.this, "NO THERMAL PAPER!", "Put thermal paper and click OK to continue.", new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                progressDialog.dismiss();
                                PrinterInterface.open();
                                writetest(agentName, payable_amount, loannumberprintout, loanRepaymentResponse.getSacconame(), saccoMotto,
                                        loanRepaymentResponse.getReceiptno(), loanRepaymentResponse.getTransactiondate(),
                                        loanRepaymentResponse.getTransactiontype());                                progressDialog.dismiss();

                                Utils.showAlertDialog(LoanRepayment.this, "Loan Repayed Successfully", " Press Ok to print another Receipt ", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        progressDialog.dismiss();
                                        PrinterInterface.open();
                                        writetest(agentName, payable_amount, loannumberprintout, loanRepaymentResponse.getSacconame(), saccoMotto,
                                                loanRepaymentResponse.getReceiptno(), loanRepaymentResponse.getTransactiondate(),
                                                loanRepaymentResponse.getTransactiontype());                                        finish();
                                        Intent i = new Intent(LoanRepayment.this, MainDashboardActivity.class);
                                        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                                        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                        startActivity(i);
                                    }
                                });

                            }
                        });
                        PrinterInterface.close();
                    } else {
                        progressDialog.dismiss();
                        PrinterInterface.open();
                        writetest(agentName, payable_amount, loannumberprintout, loanRepaymentResponse.getSacconame(), saccoMotto,
                                loanRepaymentResponse.getReceiptno(), loanRepaymentResponse.getTransactiondate(),
                                loanRepaymentResponse.getTransactiontype());                        progressDialog.dismiss();

                        Utils.showAlertDialog(LoanRepayment.this, "Loan Repayed Successfully", " Press Ok to print another Receipt ", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                progressDialog.dismiss();
                                PrinterInterface.open();
                                writetest(agentName, payable_amount, loannumberprintout, loanRepaymentResponse.getSacconame(), saccoMotto,
                                        loanRepaymentResponse.getReceiptno(), loanRepaymentResponse.getTransactiondate(),
                                        loanRepaymentResponse.getTransactiontype());
                                finish();
                                Intent i = new Intent(LoanRepayment.this, MainDashboardActivity.class);
                                i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                                i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(i);
                            }
                        });
                        PrinterInterface.close();
                    }

                } else {
                    progressDialog.dismiss();
                    Utils.showAlertDialog(LoanRepayment.this, "Loan Repayment Failed", loanRepaymentResponse.getError());
                }
            }

            @Override
            public void onTokenExpired() {

            }

            @Override
            public void onError(String error) {

            }
        });


    }

    void requestMemberDetails(String id) {
        RequestMemberName requestMemberName = new RequestMemberName();
        requestMemberName.corporateno = MainDashboardActivity.corporateno;
        requestMemberName.accountidentifier = id;
        requestMemberName.accountcode = "0";
        requestMemberName.agentid = SharedPrefs.read(SharedPrefs.AGENT_ID_1, null);////
        requestMemberName.terminalid = MainDashboardActivity.imei;
        loading.setVisibility(View.VISIBLE);

        Api.instance(LoanRepayment.this).request(Api.MSACCO_AGENT + Api.GetMembername, requestMemberName, new Api.RequestListener() {
            @Override
            public void onSuccess(String response) {
                loading.setVisibility(View.GONE);

                MemberNameResponse memberNameResponse = Api.instance(LoanRepayment.this).mGson.fromJson(response, MemberNameResponse.class);
                if (memberNameResponse.is_successful) {
                    txtFullChequeDepositName.setText(memberNameResponse.getCustomer_name() +"  "+memberNameResponse.getPhoneno());
                    membername=memberNameResponse.getCustomer_name();
                    memberphone=memberNameResponse.getPhoneno();

                } else {
                    loading.setVisibility(View.VISIBLE);
                    loading.setText("Failed, input the correct ID number");
                }

            }

            @Override
            public void onTokenExpired() {
                loading.setVisibility(View.GONE);
            }

            @Override
            public void onError(String error) {
                loading.setText("failed to load..");
            }
        });
    }

    public void writetest(String agentName, double loanAmt, String loanAccNo, String saccoName, String saccoMotto, String receiptNum,
                          String transactionDate, String transactionType) {
        try {

            byte[] arryBeginText = null;
            byte[] arryAccType = null;
            byte[] arryAccNo = null;
            byte[] arryAmt = null;
            byte[] arryMotto = null;
            byte[] terminalNo = null;
            byte[] receiptNo = null;
            byte[] date = null;
            byte[] transactionTYpe = null;
            byte[] accountname = null;
            byte[] memberphoneno = null;
            byte[] servedBy = null;

            try {
                arryBeginText = String.valueOf("     " + saccoName).getBytes("GB2312");
                terminalNo = String.valueOf(" Terminal ID" + Utils.getIMEI(LoanRepayment.this)).getBytes("GB2312");
                receiptNo = String.valueOf(" Receipt Number : " + receiptNum).getBytes("GB2312");
                date = String.valueOf(" Date : " + transactionDate).getBytes("GB2312");
                transactionTYpe = String.valueOf("    " + transactionType).getBytes("GB2312");

                accountname = String.valueOf("Account Name :" + membername).getBytes("GB2312");
                memberphoneno = String.valueOf("Phone Number :" + memberphone).getBytes("GB2312");

                arryAccType = "Your Loan Has been successfully repaid ".getBytes("GB2312");
                arryAccNo = String.valueOf("Loan Type : " + loanAccNo).getBytes("GB2312");
                arryAmt = String.valueOf("Amount :" + SharedPrefs.read(SharedPrefs.CURRENCY, null) + " :" + loanAmt).getBytes("GB2312");
                servedBy = String.valueOf(" You were Served By : " + agentName).getBytes("GB2312");
                arryMotto = String.valueOf("     "+ saccoMotto).getBytes("GB2312");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            PrinterInterface.begin();

            byte[] cmd;
            cmd = new byte[]{(byte) 0x1B, (byte) 0x40};

            cmd = new byte[]{
                    (byte) 0x1B, (byte) 0x45, (byte) 0};

            write(cmd);
            write(arryBeginText);
            writeLineBreak(2);
            write(terminalNo);
            writeLineBreak(1);
            write(receiptNo);
            writeLineBreak(1);
            write(date);
            writeLineBreak(1);
            write(transactionTYpe);
            writeLineBreak(2);

            ///
            write(accountname);
            writeLineBreak(1);
            write(memberphoneno);
            writeLineBreak(1);
            // print line break
            writeLineBreak(2);
            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            // print line break
            writeLineBreak(1);
            // print text
            write(arryAccType);
            writeLineBreak(1);
            write(arryAccNo);
            writeLineBreak(1);
            write(arryAmt);
            // print line break
            writeLineBreak(2);
            write(servedBy);
            writeLineBreak(2);
            write(arryMotto);
            writeLineBreak(3);
            PrinterInterface.end();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void write(final byte[] arryData) {
        int result = PrinterInterface.write(arryData, arryData.length);
    }

    // print line break
    private void writeLineBreak(int lineNumber) {
        // write(new byte[]{'\n'});
        write(PrinterCommand.getCmdEscDN(lineNumber));
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.move_left_in_activity, R.anim.move_right_out_activity);
    }
}
