package com.coretec.agencyApplication.activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.cloudpos.jniinterface.PrinterInterface;
import com.coretec.agencyApplication.R;
import com.coretec.agencyApplication.activities.newmodules.MainDashboardActivity;
import com.coretec.agencyApplication.api.Api;
import com.coretec.agencyApplication.api.requests.AccountDetailsRequest;
import com.coretec.agencyApplication.api.requests.RequestDeposit;
import com.coretec.agencyApplication.api.requests.RequestMemberName;
import com.coretec.agencyApplication.api.requests.RequestStkPushRequest;
import com.coretec.agencyApplication.api.responses.AccountDetailsResponse;
import com.coretec.agencyApplication.api.responses.AccountNameResponse;
import com.coretec.agencyApplication.api.responses.DepositResponse;
import com.coretec.agencyApplication.api.responses.STKResponse;
import com.coretec.agencyApplication.api.responses.SaccoAccounts;
import com.coretec.agencyApplication.model.AccountDetails;
import com.coretec.agencyApplication.utils.PreferenceFileKeys;
import com.coretec.agencyApplication.utils.QuickToast;
import com.coretec.agencyApplication.utils.SharedPrefs;
import com.coretec.agencyApplication.utils.Utils;
import com.coretec.agencyApplication.wizarpos.function.printer.PrinterCommand;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import static com.coretec.agencyApplication.utils.Utils.getFormattedDate;
import static com.coretec.agencyApplication.utils.Utils.getLat;
import static com.coretec.agencyApplication.utils.Utils.getLong;

public class CashDeposit extends BaseActivity {

    private Button btnConfirm, btn_account_search, btn_account_search2;
    private ProgressBar deposit_progressBar;
    private TextInputLayout layout_account_to_deposit;
    private TextInputLayout layout_amount_to_deposit;
    private TextInputLayout layout_deposit_description;
    private TextInputLayout layout_depositorsname;
    private TextInputLayout layout_depositorsphoneno;
    private TextInputLayout input_layout_id_number;

    private EditText id_number;
    private EditText account_to_deposit;
    private TextView deposit_account_name;
    private EditText amount_to_deposit, depositorsname, depositorsphoneno, deposit_description;
    private String deposit_amount, deposit_account_number, description, depositorsName, depositorsPhoneNo;
    //private Switch id_accSwitch;

    private EditText identifier_deposit;

    private Spinner spinner;
    private ArrayList<String> arrayList = new ArrayList<>();
    private ArrayAdapter<AccountDetails> adapter;

    private ArrayList<String> arrayList2 = new ArrayList<>();
    private ArrayAdapter<String> adapter2;

    private String identifiercode = "";
    private String accountNo = "";
    private String phoneNumber = "";

    ProgressBar progressBar,namepb;
    ProgressDialog progressDialog;
    SharedPreferences sharedPreferences;
    boolean find = true;

    private QuickToast toast = new QuickToast(this);
    public RadioButton rbid, rbacc;
    private RadioGroup radioGroupIdentifier;
    private  AccountDetails accountDetails;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_deposit);

        this.overridePendingTransition(R.anim.move_right_in_activity, R.anim.move_left_out_activity);

        sharedPreferences = getSharedPreferences(PreferenceFileKeys.PREFS_FILE, Context.MODE_PRIVATE);

        deposit_progressBar = findViewById(R.id.deposit_progressBar);
        namepb = findViewById(R.id.namepb);
        deposit_progressBar.setVisibility(View.GONE);

        deposit_account_name = findViewById(R.id.deposit_account_name);
        account_to_deposit = findViewById(R.id.account_to_deposit);
        amount_to_deposit = findViewById(R.id.amount_to_deposit);

        layout_account_to_deposit = findViewById(R.id.layout_account_to_deposit);
        layout_amount_to_deposit = findViewById(R.id.layout_amount_to_deposit);
        layout_deposit_description = findViewById(R.id.layout_deposit_description);
        layout_depositorsname = findViewById(R.id.layout_depositorsname);
        layout_depositorsphoneno = findViewById(R.id.layout_depositorsphoneno);

        depositorsname = findViewById(R.id.depositorsname);
        depositorsphoneno = findViewById(R.id.depositorsphoneno);
        deposit_description = findViewById(R.id.deposit_description);

        input_layout_id_number = findViewById(R.id.input_layout_id_number);
        btn_account_search = findViewById(R.id.btn_account_search);
        btn_account_search2 = findViewById(R.id.btn_account_search2);
        spinner = findViewById(R.id.deposit_accounts);

        identifier_deposit = findViewById(R.id.id_number);
        identifiercode = "2";

        rbid = findViewById(R.id.rbid);
        rbacc = findViewById(R.id.rbacc);
        radioGroupIdentifier = findViewById(R.id.radioGroupIdentifier);

        radioGroupIdentifier.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {

                switch (checkedId){
                    case R.id.rbid:
                        identifiercode = "0";
                        input_layout_id_number.setVisibility(View.VISIBLE);
                        spinner.setVisibility(View.VISIBLE);
                        btn_account_search.setVisibility(View.VISIBLE);

                        spinner.setVisibility(View.VISIBLE);
                        layout_account_to_deposit.setVisibility(View.GONE);
                        btn_account_search2.setVisibility(View.GONE);
                        deposit_account_name.setText("");
                        depositorsname.setText("");
                        depositorsphoneno.setText("");
                        //identifier_deposit.setText("");
                        break;

                    case  R.id.rbacc:
                        identifiercode = "2";
                        input_layout_id_number.setVisibility(View.GONE);
                        spinner.setVisibility(View.GONE);

                        btn_account_search2.setVisibility(View.VISIBLE);
                        layout_account_to_deposit.setVisibility(View.VISIBLE);
                        btn_account_search.setVisibility(View.GONE);
                        deposit_account_name.setText("");
                        depositorsphoneno.setText("");
                        account_to_deposit.setText("");
                        depositorsname.setText("");
                        //identifier_deposit.setFocusable(true);
                        account_to_deposit.setFocusable(true);
                        depositorsphoneno.setFocusable(true);
                        break;

                        default:
                            break;

                }

            }
        });

        progressBar = findViewById(R.id.accountPb);
        btn_account_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String identifier_number = identifier_deposit.getText().toString();
                 if (identifier_number.matches("")) {
                    toast.swarn("Please enter ID number to proceed!");
                } else if (identifier_number.length() < 4) {
                    toast.swarn("Please enter the correct ID number!");
                } else {
                    deposit_progressBar.setVisibility(View.VISIBLE);
                    requestGetAccountDetails(identifier_number);
                    requestMemberDetailsById();


                }
            }
        });


        btn_account_search2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String accountNumber = account_to_deposit.getText().toString();
               if (accountNumber.matches("")) {
                    toast.swarn("Please enter account number!");
                } else if (accountNumber.length() < 4) {
                    toast.swarn("Please enter the correct account number to proceed!");
                } else {
                    progressBar.setVisibility(View.VISIBLE);
                    requestMemberDetailsByAccountNumber();
                }
            }
        });


        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                accountDetails = (AccountDetails) parent.getItemAtPosition(position);
                accountNo = accountDetails.getAccountno();
                phoneNumber = accountDetails.getPhonenumber();

                if (!accountDetails.equals("")) {
                    account_to_deposit.setText(accountNo);
                    account_to_deposit.setFocusable(false);
                    depositorsphoneno.setText(phoneNumber);
                    depositorsphoneno.setFocusable(false);
                } else {
                    account_to_deposit.setFocusable(true);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        TextWatcher textWatcher = new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {

               /* String inputPhone = depositorsphoneno.getText().toString();
                if (inputPhone.length() == 1) {
                    if (inputPhone.equals("0") ||  inputPhone.equals("+")) {
                        depositorsphoneno.setText("+254");
                        depositorsphoneno.setSelection(depositorsphoneno.getText().length());
                        Toast.makeText(CashDeposit.this, "Correct format auto completed. Please proceed!", Toast.LENGTH_SHORT).show();
                    } else {
                        depositorsphoneno.setText(null);
                        Toast.makeText(CashDeposit.this, "Please enter correct phone number format!", Toast.LENGTH_SHORT).show();
                    }
                }*/
            }
        };

        depositorsphoneno.addTextChangedListener(textWatcher);
        adapter2 = new ArrayAdapter<String>(getApplicationContext(), R.layout.simple_spinner_dropdown_item, arrayList2);
        adapter2.setDropDownViewResource(R.layout.simple_spinner_dropdown_item);
        btnConfirm = findViewById(R.id.btn_submit_deposit);
        String coopnumber = SharedPrefs.read(SharedPrefs.VR_COOPORATE_NO, null);
        if (coopnumber !=null){
            switch (SharedPrefs.read(SharedPrefs.VR_COOPORATE_NO, null)) {
                case "CAP005":
                    btnConfirm.setBackgroundResource(R.drawable.btn_rounded_primary_coloraccentforcoopmis);
                    break;
                case "CAP022":
                    btnConfirm.setBackgroundResource(R.drawable.btn_rounded_primary_coloraccentforcoopmis);
                    break;

                case "CAP020":
                    btnConfirm.setBackgroundResource(R.drawable.btn_rounded_primary_coloraccent);
                    btnConfirm.setText("REQUEST STK");
                    break;
                default:
                    btnConfirm.setBackgroundResource(R.drawable.btn_rounded_primary_coloraccent);
                    break;
            }

        }else {
            btnConfirm.setBackgroundResource(R.drawable.btn_rounded_primary_coloraccent);
        }


        btnConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                deposit_amount = amount_to_deposit.getText().toString().trim();
                deposit_account_number = account_to_deposit.getText().toString().trim();
                description = deposit_description.getText().toString().trim();
                depositorsPhoneNo = depositorsphoneno.getText().toString().trim();
                depositorsName = depositorsname.getText().toString().trim();

                if (deposit_amount.isEmpty()) {
                    layout_amount_to_deposit.setError("Enter the Amount to Deposit");
                } else if (deposit_account_number.isEmpty()) {
                    layout_account_to_deposit.setError("Enter the Member Account Number");
                } else if (depositorsName.isEmpty()) {
                    layout_depositorsname.setError("Enter the Name of the Depositor");
                } else if (depositorsPhoneNo.isEmpty()) {
                    layout_depositorsphoneno.setError("Enter the Phone Number of the Depositor");
                } else {
                    deposit_progressBar.setVisibility(View.VISIBLE);
                    progressDialog = new ProgressDialog(CashDeposit.this);
                    progressDialog.setMessage("Please wait...");
                    progressDialog.setCancelable(false);
                    progressDialog.show();

                    if (MainDashboardActivity.corporateno .equals("CAP020")){
                        if(btnConfirm.getText().equals("REQUEST STK")) {
                            requestStkPush();
                        }else{
                            requestPostDeposit(deposit_account_number, Integer.parseInt(deposit_amount), description, depositorsPhoneNo, depositorsName);
                            btnConfirm.setClickable(false);
                        }
                    }else{
                        requestPostDeposit(deposit_account_number, Integer.parseInt(deposit_amount), description, depositorsPhoneNo, depositorsName);
                        btnConfirm.setClickable(false);
                    }
                }
            }
        });
    }


    void requestGetAccountDetails(String identifier) {
        final String TAG = "MemberAccounts";
        String URL = Api.MSACCO_AGENT + Api.GetAllMemberSaccoDetails;
        final AccountDetailsRequest accountDetails = new AccountDetailsRequest();
        accountDetails.agentid = sharedPreferences.getString(PreferenceFileKeys.AGENT_ID, "");
        accountDetails.corporateno =MainDashboardActivity.corporateno;
        accountDetails.accountidentifier = identifier;
        accountDetails.accountidentifiercode = identifiercode;
        accountDetails.transactiontype = "D";
        accountDetails.terminalid = MainDashboardActivity.imei;
        accountDetails.longitude = getLong(this);
        accountDetails.latitude = getLat(this);
        accountDetails.date = getFormattedDate();

        Log.e(TAG, accountDetails.getBody().toString());
        Api.instance(this).request(URL, accountDetails, new Api.RequestListener() {
            @Override
            public void onSuccess(String response) {

                deposit_progressBar.setVisibility(View.GONE);
                AccountDetailsResponse accountDetailsResponse = Api.instance(CashDeposit.this)
                        .mGson.fromJson(response, AccountDetailsResponse.class);

                if (accountDetailsResponse.is_successful) {
                    progressBar.setVisibility(View.GONE);
                    List<AccountDetails> accountDetailsList = new ArrayList<>();

                    adapter = new ArrayAdapter<AccountDetails>(getApplicationContext(),
                            R.layout.simple_spinner_dropdown_item, new ArrayList<AccountDetails>());
                    adapter.setDropDownViewResource(R.layout.simple_spinner_dropdown_item);
                    adapter.clear();

                    for (SaccoAccounts saccoAccounts : accountDetailsResponse.getSaccoAccounts()) {
                        accountDetailsList.addAll(saccoAccounts.getAccountDetails());
                    }

                    adapter.addAll(accountDetailsList);
                    spinner.setAdapter(adapter);

                } else {
                    progressBar.setVisibility(View.GONE);
                    toast.swarn(accountDetailsResponse.getError());
                    adapter = new ArrayAdapter<AccountDetails>(getApplicationContext(),
                            R.layout.simple_spinner_dropdown_item, new ArrayList<AccountDetails>());
                    adapter.setDropDownViewResource(R.layout.simple_spinner_dropdown_item);
                    adapter.clear();
                    spinner.setAdapter(adapter);
                    if (identifier_deposit.length() == 8) {
                        Toast.makeText(CashDeposit.this, accountDetailsResponse.getError().toString(), Toast.LENGTH_LONG).show();
                    }
                }
            }

            @Override
            public void onTokenExpired() {

            }

            @Override
            public void onError(String error) {

            }
        });
    }

    void requestMemberDetailsById() {
        RequestMemberName requestMemberName = new RequestMemberName();
        requestMemberName.corporateno = MainDashboardActivity.corporateno;
        requestMemberName.accountidentifier = identifier_deposit.getText().toString();
        requestMemberName.accountcode = "0";
        requestMemberName.agentid = SharedPrefs.read(SharedPrefs.AGENT_ID_1, null);
        requestMemberName.terminalid = MainDashboardActivity.imei;

        Api.instance(CashDeposit.this).request(Api.MSACCO_AGENT + Api.GetMembername, requestMemberName, new Api.RequestListener() {
            @Override
            public void onSuccess(String response) {
                deposit_progressBar.setVisibility(View.GONE);
                namepb.setVisibility(View.VISIBLE);
                progressBar.setVisibility(View.GONE);
                AccountNameResponse accountNameResponse = Api.instance(CashDeposit.this).mGson.fromJson(response, AccountNameResponse.class);
                if (accountNameResponse.is_successful) {
                    namepb.setVisibility(View.GONE);
                    deposit_account_name.setText(accountNameResponse.getCustomer_name());
                    depositorsname.setText(accountNameResponse.getCustomer_name());
                } else {
                    namepb.setVisibility(View.GONE);
                    Toast.makeText(CashDeposit.this, "No Account Fetched", Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onTokenExpired() {

            }

            @Override
            public void onError(String error) {

            }
        });
    }
    void requestMemberDetailsByAccountNumber() {
        RequestMemberName requestMemberName = new RequestMemberName();
        requestMemberName.corporateno = MainDashboardActivity.corporateno;
        requestMemberName.accountidentifier = account_to_deposit.getText().toString();
        requestMemberName.accountcode = "2";
        requestMemberName.agentid = SharedPrefs.read(SharedPrefs.AGENT_ID_1, null);////
        requestMemberName.terminalid = MainDashboardActivity.imei;

        Api.instance(CashDeposit.this).request(Api.MSACCO_AGENT + Api.GetMembername, requestMemberName, new Api.RequestListener() {
            @Override
            public void onSuccess(String response) {

                deposit_progressBar.setVisibility(View.GONE);
                progressBar.setVisibility(View.GONE);
                AccountNameResponse accountNameResponse = Api.instance(CashDeposit.this).mGson.fromJson(response, AccountNameResponse.class);
                if (accountNameResponse.is_successful) {
                    deposit_account_name.setText(accountNameResponse.getCustomer_name() + " - " + accountNameResponse.getPhoneno());
                } else {
                    Toast.makeText(CashDeposit.this, "No Account Fetched", Toast.LENGTH_LONG).show();
                }

            }

            @Override
            public void onTokenExpired() {

            }

            @Override
            public void onError(String error) {

            }
        });
    }


    void requestStkPush(){
        
        String phonenumber=depositorsphoneno.getText().toString().substring(depositorsphoneno.getText().toString().length() - 9);

        RequestStkPushRequest stk = new RequestStkPushRequest();
        stk.userid = "";
        stk.msisdn = "254"+phonenumber;
        stk.accountreference = account_to_deposit.getText().toString();
        stk.amount = amount_to_deposit.getText().toString();
        stk.transactiontype = "";
        stk.agentid = SharedPrefs.read(SharedPrefs.AGENT_ID_1, null);
        stk.terminalid = MainDashboardActivity.imei;
        stk.longitude = getLong(CashDeposit.this);
        stk.latitude = getLat(CashDeposit.this);
        stk.date = getFormattedDate();
        stk.corporate_no ="926500";// MainDashboardActivity.corporateno;// tafutiwa cooperate number apa..

        Api.instance(this).request(Api.MSACCO_AGENT + Api.STKPush, stk, new Api.RequestListener() {
            @Override
            public void onSuccess(String response) {
                final STKResponse stkresponse = Api.instance(CashDeposit.this).mGson.fromJson(response, STKResponse.class);
                if (stkresponse.operationsuccess) {
                    btnConfirm.setText("SUBMIT");
                    progressDialog.dismiss();
                }else {
                    Toast.makeText(CashDeposit.this, stkresponse.getError(), Toast.LENGTH_LONG).show();
                    progressDialog.dismiss();
                }
            }

            @Override
            public void onTokenExpired() {

            }

            @Override
            public void onError(String error) {
                Toast.makeText(CashDeposit.this, "Failed!", Toast.LENGTH_LONG).show();
            }
        });
    }

    void requestPostDeposit(final String memberAccno, final int amount, final String deposit_description,
                            final String depositorsPhoneNo, final String depositorsName) {
        final String TAG = "deposit cash ";
        Log.e(TAG, memberAccno);
        Log.e(TAG, String.valueOf(amount));

        String URL = Api.MSACCO_AGENT + Api.FundsDeposits;
        RequestDeposit requestDeposit = new RequestDeposit();
        requestDeposit.agentid = sharedPreferences.getString(PreferenceFileKeys.AGENT_ID, "");
        requestDeposit.corporate_no = MainDashboardActivity.corporateno;
        requestDeposit.memberaccno = memberAccno;
        requestDeposit.depositorsname = depositorsName;
        requestDeposit.description = deposit_description;
        requestDeposit.depositorsphoneno = depositorsPhoneNo;
        requestDeposit.transactiontype = "D";
        requestDeposit.amount = amount;
        requestDeposit.terminalid = MainDashboardActivity.imei;
        requestDeposit.longitude = getLong(this);
        requestDeposit.latitude = getLat(this);
        requestDeposit.date = getFormattedDate();
        requestDeposit.depositorsname = depositorsname.getText().toString();


        Log.e(TAG, requestDeposit.getBody().toString());
        Api.instance(this).request(URL, requestDeposit, new Api.RequestListener() {
            @Override
            public void onSuccess(String response) {
                deposit_progressBar.setVisibility(View.GONE);
                final DepositResponse depositResponse = Api.instance(CashDeposit.this).mGson.fromJson(response, DepositResponse.class);
                if (depositResponse.is_successful) {

                    final String agentName = sharedPreferences.getString(PreferenceFileKeys.AGENT_ACCOUNT_NAME, "");
                    final String saccoMotto = sharedPreferences.getString(PreferenceFileKeys.AGENT_SACCO_MOTTO, "");
                    if (PrinterInterface.queryStatus() == 0) {
                        Utils.showAlertDialog(CashDeposit.this, "NO THERMAL PAPER!", "Put thermal paper and click OK to continue.", new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                PrinterInterface.open();
                                writetest(memberAccno, deposit_description, agentName, depositorsName, depositResponse.getAmount(), depositResponse.getMemberaccno(), depositResponse.getSacconame(), saccoMotto, depositResponse.getCustomername(), depositResponse.getReceiptno(), depositResponse.getTransactiondate(), depositResponse.getTransactiontype());
                                progressDialog.dismiss();

                                Utils.showAlertDialog(CashDeposit.this, "Deposited Successfully", " Press Ok to print another Receipt ", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        PrinterInterface.open();
                                        writetest1(memberAccno, deposit_description, agentName, depositorsName, depositResponse.getAmount(), depositResponse.getMemberaccno(), depositResponse.getSacconame(), saccoMotto, depositResponse.getCustomername(), depositResponse.getReceiptno(), depositResponse.getTransactiondate(), depositResponse.getTransactiontype());
                                        finish();
                                        Intent i = new Intent(CashDeposit.this, MainDashboardActivity.class);
                                        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                                        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                        startActivity(i);
                                    }
                                });

                            }
                        });
                        PrinterInterface.close();
                    } else {
                        PrinterInterface.open();
                        writetest(memberAccno, deposit_description, agentName, depositorsName, depositResponse.getAmount(), depositResponse.getMemberaccno(), depositResponse.getSacconame(), saccoMotto, depositResponse.getCustomername(), depositResponse.getReceiptno(), depositResponse.getTransactiondate(), depositResponse.getTransactiontype());
                        progressDialog.dismiss();

                        Utils.showAlertDialog(CashDeposit.this, "Deposited Successfully", " Press Ok to print another Receipt ", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                PrinterInterface.open();
                                writetest1(memberAccno, deposit_description, agentName, depositorsName, depositResponse.getAmount(), depositResponse.getMemberaccno(), depositResponse.getSacconame(), saccoMotto, depositResponse.getCustomername(), depositResponse.getReceiptno(), depositResponse.getTransactiondate(), depositResponse.getTransactiontype());
                                finish();
                                Intent i = new Intent(CashDeposit.this, MainDashboardActivity.class);
                                i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                                i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(i);
                            }
                        });
                        PrinterInterface.close();
                    }


                } else {
                    progressDialog.dismiss();
                    deposit_progressBar.setVisibility(View.GONE);
                    Utils.showAlertDialog(CashDeposit.this, "Deposit Failed", depositResponse.getError(), new DialogInterface.OnClickListener() {// depositResponse.getError());

                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            finish();
                            Intent i = new Intent(CashDeposit.this, MainDashboardActivity.class);
                            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                            i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(i);
                        }
                    });

                }

            }

            @Override
            public void onTokenExpired() {

            }

            @Override
            public void onError(String error) {

            }
        });

    }

    public void writetest(String memberAccno, String description, String agentName, String depositorsName, int amount, String accountNo, String saccoame, String saccoMotto, String customerName, String receiptNumber, String dateed, String transactionType) {
        try {

            byte[] arrySaccoName = null;
            byte[] terminalNo = null;
            byte[] receiptNo = null;
            byte[] date = null;
            byte[] arryWhat = null;
            byte[] arryCustomerName = null;
            byte[] account_dposited_to = null;
            byte[] transactionTYpe = null;
            byte[] arryAmt = null;
            byte[] dep_description = null;
            byte[] depositedBy = null;

            byte[] servedBy = null;
            byte[] customerReceipt = null;

            byte[] arryMotto = null;

            try {
                arrySaccoName = String.valueOf("       " + String.valueOf(saccoame)).getBytes("GB2312");
                terminalNo = String.valueOf("Terminal ID :" + Utils.getIMEI(CashDeposit.this)).getBytes("GB2312");
                receiptNo = String.valueOf("Receipt No : " + receiptNumber).getBytes("GB2312");
                date = String.valueOf("Date :" + dateed).getBytes("GB2312");

                arryWhat = String.valueOf("         AGENT DEPOSIT ").getBytes("GB2312");
                arryCustomerName = String.valueOf("Name :" + customerName).getBytes("GB2312");
                account_dposited_to = String.valueOf("Acc No : " + memberAccno).getBytes("GB2312");
                transactionTYpe = String.valueOf("Transaction :" + transactionType).getBytes("GB2312");
                arryAmt = String.valueOf(String.valueOf("Amount " + amount)).getBytes("GB2312");
                dep_description = String.valueOf(String.valueOf("Description :" + description)).getBytes("GB2312");
                depositedBy = String.valueOf("Deposited by " + depositorsName).getBytes("GB2312");

                servedBy = String.valueOf("You were Served By : " + agentName).getBytes("GB2312");

                customerReceipt = String.valueOf("         Customer Copy         ").getBytes("GB2312");

                arryMotto = String.valueOf("     " + saccoMotto).getBytes("GB2312");


            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            PrinterInterface.begin();

            byte[] cmd;
            cmd = new byte[]{(byte) 0x1B, (byte) 0x40};

            cmd = new byte[]{
                    (byte) 0x1B, (byte) 0x45, (byte) 0};

            write(cmd);
            writeLineBreak(1);
            write(arrySaccoName);
            writeLineBreak(2);

            write(terminalNo);
            writeLineBreak(1);
            write(receiptNo);
            writeLineBreak(1);
            write(date);
            writeLineBreak(2);

            write(arryWhat);
            writeLineBreak(2);

            write(arryCustomerName);
            writeLineBreak(1);
            write(account_dposited_to);
            writeLineBreak(1);
            write(transactionTYpe);
            writeLineBreak(1);
            write(arryAmt);
            writeLineBreak(1);
            write(dep_description);
            writeLineBreak(1);
            write(depositedBy);
            writeLineBreak(2);

            write(servedBy);
            writeLineBreak(2);

            write(customerReceipt);
            writeLineBreak(2);

            write(arryMotto);
            writeLineBreak(3);
            PrinterInterface.end();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void writetest1(String memberAccno, String description, String agentName, String depositorsName, int amount, String accountNo, String saccoame, String saccoMotto, String customerName, String receiptNumber, String dateed, String transactionType) {
        try {

            byte[] arrySaccoName = null;
            byte[] terminalNo = null;
            byte[] receiptNo = null;
            byte[] date = null;
            byte[] arryWhat = null;
            byte[] arryCustomerName = null;
            byte[] account_dposited_to = null;
            byte[] transactionTYpe = null;
            byte[] arryAmt = null;
            byte[] dep_description = null;
            byte[] depositedBy = null;

            byte[] servedBy = null;
            byte[] customerReceipt = null;

            byte[] arryMotto = null;

            try {
                arrySaccoName = String.valueOf("       " + String.valueOf(saccoame)).getBytes("GB2312");
                terminalNo = String.valueOf("Terminal ID :" + Utils.getIMEI(CashDeposit.this)).getBytes("GB2312");
                receiptNo = String.valueOf("Receipt No : " + receiptNumber).getBytes("GB2312");
                date = String.valueOf("Date : " + dateed).getBytes("GB2312");

                arryWhat = String.valueOf("         AGENT DEPOSIT").getBytes("GB2312");
                arryCustomerName = String.valueOf("Name :" + customerName).getBytes("GB2312");
                account_dposited_to = String.valueOf("Acc No :" + memberAccno).getBytes("GB2312");
                transactionTYpe = String.valueOf("Transaction :" + transactionType).getBytes("GB2312");
                arryAmt = String.valueOf(String.valueOf("Amount " + amount)).getBytes("GB2312");
                dep_description = String.valueOf(String.valueOf("Description :" + description)).getBytes("GB2312");
                depositedBy = String.valueOf("Deposited by " + depositorsName).getBytes("GB2312");

                servedBy = String.valueOf("You were Served By : " + agentName).getBytes("GB2312");

                customerReceipt = String.valueOf("         Agent Copy         ").getBytes("GB2312");

                arryMotto = String.valueOf("     " + saccoMotto).getBytes("GB2312");


            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            PrinterInterface.begin();

            byte[] cmd;
            cmd = new byte[]{(byte) 0x1B, (byte) 0x40};

            cmd = new byte[]{
                    (byte) 0x1B, (byte) 0x45, (byte) 0};

            write(cmd);
            writeLineBreak(1);
            write(arrySaccoName);
            writeLineBreak(2);

            write(terminalNo);
            writeLineBreak(1);
            write(receiptNo);
            writeLineBreak(1);
            write(date);
            writeLineBreak(2);

            write(arryWhat);
            writeLineBreak(2);

            write(arryCustomerName);
            writeLineBreak(1);
            write(account_dposited_to);
            writeLineBreak(1);
            write(transactionTYpe);
            writeLineBreak(1);
            write(arryAmt);
            writeLineBreak(1);
            write(dep_description);
            writeLineBreak(1);
            write(depositedBy);
            writeLineBreak(2);

            write(servedBy);
            writeLineBreak(2);

            write(customerReceipt);
            writeLineBreak(2);

            write(arryMotto);
            writeLineBreak(3);
            PrinterInterface.end();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void write(final byte[] arryData) {
        int result = PrinterInterface.write(arryData, arryData.length);

    }

    // print line break
    private void writeLineBreak(int lineNumber) {
        // write(new byte[]{'\n'});
        write(PrinterCommand.getCmdEscDN(lineNumber));
       // write(PrinterCommand.getF);
    }



    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.move_left_in_activity, R.anim.move_right_out_activity);
    }
}
