package com.coretec.agencyApplication.activities.gfl;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.cloudpos.jniinterface.PrinterInterface;
import com.coretec.agencyApplication.R;
import com.coretec.agencyApplication.activities.BaseActivity;
import com.coretec.agencyApplication.adapters.LastFiveAdapter;
import com.coretec.agencyApplication.adapters.ReportsAdapter;
import com.coretec.agencyApplication.api.Api;
import com.coretec.agencyApplication.api.requests.EodReportsRequest;
import com.coretec.agencyApplication.api.responses.Accounts;
import com.coretec.agencyApplication.api.responses.EodReportsResponse;
import com.coretec.agencyApplication.api.responses.LastFiveList;
import com.coretec.agencyApplication.api.responses.LastFiveReportsResponse;
import com.coretec.agencyApplication.utils.LogOutTimerUtil;
import com.coretec.agencyApplication.utils.PreferenceFileKeys;
import com.coretec.agencyApplication.utils.SharedPrefs;
import com.coretec.agencyApplication.utils.Utils;
import com.coretec.agencyApplication.wizarpos.function.printer.PrinterCommand;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.coretec.agencyApplication.utils.Utils.getFormattedDate;
import static com.coretec.agencyApplication.utils.Utils.getLat;
import static com.coretec.agencyApplication.utils.Utils.getLong;

public class LastFiveTransactions extends BaseActivity implements  LogOutTimerUtil.LogOutListener {

    private List<LastFiveList> lastfivelist;
    private RecyclerView reports_recyclerView;
    private LastFiveAdapter reportsAdapter;
    SharedPreferences sharedPreferences;
    private TextView reports_agent_name;
    private TextView reports_sacco_name;
    private ImageView backBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_last_five_transactions); this.overridePendingTransition(R.anim.move_right_in_activity, R.anim.move_left_out_activity);
        lastfivelist = new ArrayList<>();
        sharedPreferences = getSharedPreferences(PreferenceFileKeys.PREFS_FILE, Context.MODE_PRIVATE);

        String agentName = SharedPrefs.read(SharedPrefs.AGENT_NAME, null);
        String saccoName = sharedPreferences.getString(PreferenceFileKeys.AGENT_SACCO_NAME, "");

        Log.d("nammeeee","nammeeee------>"+agentName);
        reports_agent_name = findViewById(R.id.reports_agent_name);
        reports_agent_name.setText(agentName);

        reports_sacco_name = findViewById(R.id.reports_sacco_name);
        reports_sacco_name.setText("Green Fedha");

        reports_recyclerView = findViewById(R.id.lasttransRecyclerView);
        reports_recyclerView.setHasFixedSize(true);
        reportsAdapter = new LastFiveAdapter(this, lastfivelist);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
        reports_recyclerView.setLayoutManager(mLayoutManager);

        final Map<String, Object> params = new HashMap<>();
        params.put("accountsList", lastfivelist);


        final String TAG = "request reports";

        String URL = Api.MSACCO_AGENT + Api.GetEODReport;

        EodReportsRequest request = new EodReportsRequest();
        request.corporateno = MainGFLDashboard.corporateno;
        request.agentid = sharedPreferences.getString(PreferenceFileKeys.AGENT_ID, "");
        request.terminalid = MainGFLDashboard.imei;
        request.longitude = getLong(this);
        request.latitude = getLat(this);
        request.requestdate = getFormattedDate();


        Log.e(TAG, request.getBody().toString());

        Api.instance(this).request(URL, request, new Api.RequestListener() {
            @Override
            public void onSuccess(String response) {
                Log.e(TAG, response);
                final LastFiveReportsResponse lastfiveReportsResponse = Api.instance(LastFiveTransactions.this).mGson.fromJson(response, LastFiveReportsResponse.class);
                if (lastfiveReportsResponse.is_successful) {
                    if(lastfivelist.isEmpty()){
                        Utils.showAlertDialog(LastFiveTransactions.this, "Alert!", "last five transactions not retrieved");
                    }else {
                        lastfivelist.addAll(lastfiveReportsResponse.getLastfivetransaction());
                        reports_recyclerView.setAdapter(reportsAdapter);
                    }

                } else {
                    Utils.showAlertDialog(LastFiveTransactions.this, "Last five Transactions Reports Retrieval Failed", lastfiveReportsResponse.getError());
                }
            }

            @Override
            public void onTokenExpired() {

            }

            @Override
            public void onError(String error) {

            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.move_left_in_activity, R.anim.move_right_out_activity);
    }

    @Override
    protected void onStart() {
        super.onStart();
        //LogOutTimerUtil.startLogoutTimer(this, this);

    }

    @Override
    public void onUserInteraction() {
        super.onUserInteraction();
        LogOutTimerUtil.startLogoutTimer(this, this);

    }


    @Override
    protected void onPause() {
        super.onPause();
        LogOutTimerUtil.startLogoutTimer(this, this);

    }

    @Override
    protected void onResume() {
        super.onResume();
        LogOutTimerUtil.startLogoutTimer(this, this);

    }

    @Override
    public void doLogout() {
        finish();
    }
}
