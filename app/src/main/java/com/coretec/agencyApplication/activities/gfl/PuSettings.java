package com.coretec.agencyApplication.activities.gfl;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import com.coretec.agencyApplication.R;
import com.coretec.agencyApplication.activities.BaseActivity;
import com.coretec.agencyApplication.utils.LogOutTimerUtil;

public class PuSettings extends BaseActivity implements  LogOutTimerUtil.LogOutListener {

    private CardView cardviewpupinreset,cardviewblockpesaulipo,cardviewactivatepu;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_pu_settings);

        initializevalues();
    }

    private void initializevalues() {
        cardviewpupinreset=findViewById(R.id.cardviewpupinreset);
        cardviewblockpesaulipo=findViewById(R.id.cardviewblockpesaulipo);
        cardviewactivatepu=findViewById(R.id.cardviewactivatepu);

        cardviewpupinreset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(PuSettings.this, PuPinReset.class));
            }
        });
        cardviewblockpesaulipo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(PuSettings.this, PesaUlipo.class));
            }
        });
        cardviewactivatepu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(PuSettings.this, PuActivation.class));
            }
        });

    }
    @Override
    protected void onStart() {
        super.onStart();
        // LogOutTimerUtil.startLogoutTimer(this, this);

    }

    @Override
    public void onUserInteraction() {
        super.onUserInteraction();
        LogOutTimerUtil.startLogoutTimer(this, this);

    }


    @Override
    protected void onPause() {
        super.onPause();
        LogOutTimerUtil.startLogoutTimer(this, this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        LogOutTimerUtil.startLogoutTimer(this, this);

    }

    @Override
    public void doLogout() {
        finish();
    }
}
