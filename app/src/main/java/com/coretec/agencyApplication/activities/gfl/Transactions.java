package com.coretec.agencyApplication.activities.gfl;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;

import com.coretec.agencyApplication.R;
import com.coretec.agencyApplication.activities.BaseActivity;
import com.coretec.agencyApplication.utils.LogOutTimerUtil;

public class Transactions extends BaseActivity implements  LogOutTimerUtil.LogOutListener{
    private CardView cardviewbalanceinquiry,cardviewministatement,cardviewpaybillcorrection,cardviewlastfivetransactions;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_transactions);

        initializevalues();
    }

    private void initializevalues() {
        cardviewbalanceinquiry=findViewById(R.id.cardviewbalanceinquiry);
        cardviewministatement=findViewById(R.id.cardviewministatement);
        cardviewpaybillcorrection=findViewById(R.id.cardviewpaybillcorrection);
        cardviewlastfivetransactions=findViewById(R.id.cardviewlastfivetransactions);

        cardviewbalanceinquiry.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Transactions.this, com.coretec.agencyApplication.activities.gfl.BalanceInquiry.class));
            }
        });
        cardviewministatement.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Transactions.this, com.coretec.agencyApplication.activities.gfl.MiniStatement.class));
            }
        });
        cardviewpaybillcorrection.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Transactions.this, PaybillCorrection.class));
            }
        });

        cardviewlastfivetransactions.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Transactions.this, LastFiveTransactions.class));
            }
        });
    }
    @Override
    protected void onStart() {
        super.onStart();
        //LogOutTimerUtil.startLogoutTimer(this, this);

    }

    @Override
    public void onUserInteraction() {
        super.onUserInteraction();
        LogOutTimerUtil.startLogoutTimer(this, this);

    }


    @Override
    protected void onPause() {
        super.onPause();
        LogOutTimerUtil.startLogoutTimer(this, this);

    }

    @Override
    protected void onResume() {
        super.onResume();
        LogOutTimerUtil.startLogoutTimer(this, this);

    }

    @Override
    public void doLogout() {
        finish();
    }
}
