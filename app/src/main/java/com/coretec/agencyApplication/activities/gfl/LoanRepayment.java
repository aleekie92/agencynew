package com.coretec.agencyApplication.activities.gfl;

import android.app.ProgressDialog;
import android.graphics.PorterDuff;
import android.support.design.widget.TextInputLayout;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;

import com.coretec.agencyApplication.R;
import com.coretec.agencyApplication.activities.BaseActivity;
import com.coretec.agencyApplication.api.GFL;
import com.coretec.agencyApplication.api.requests.GetLoansInfoRequest;
import com.coretec.agencyApplication.api.requests.GrowersDetailsRequest;
import com.coretec.agencyApplication.api.requests.STKPushRequest;
import com.coretec.agencyApplication.api.responses.GetLoansInfoResponse;
import com.coretec.agencyApplication.api.responses.GrowersDetailsResponse;
import com.coretec.agencyApplication.api.responses.STKPushResponse;
import com.coretec.agencyApplication.model.GrowerDetails;
import com.coretec.agencyApplication.model.LoanDetails;
import com.coretec.agencyApplication.utils.QuickToast;
import com.coretec.agencyApplication.utils.SharedPrefs;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import static com.coretec.agencyApplication.utils.Utils.getFormattedDate;
import static com.coretec.agencyApplication.utils.Utils.getLat;
import static com.coretec.agencyApplication.utils.Utils.getLong;

public class LoanRepayment extends AppCompatActivity {

    private QuickToast toast = new QuickToast(this);
    private TextInputLayout input_layout_id_number;
    private EditText identifier_deposit;
    private EditText phoneNumber;
    private EditText amount;
    private Spinner spinner;
    ImageView backBtn;

    private ArrayList<String> arrayList = new ArrayList<>();
    private ArrayAdapter<GrowerDetails> adapter;
    private String item, itemLoan;

    private Spinner spinnerLoan;
    private ArrayList<String> arrayListLoan = new ArrayList<>();
    private ArrayAdapter<LoanDetails> adapterLoan;

    ProgressBar progressBar, loanBar;
    ProgressBar miniPB, maxiPB;
    ProgressDialog progressDialog;
    Button submit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        SharedPrefs.init(getApplicationContext());
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.gfl_loan_repayment);

        this.overridePendingTransition(R.anim.move_right_in_activity, R.anim.move_left_out_activity);

        //initialize your UI
        Toolbar toolbar = findViewById(R.id.toolbar);
        TextView mTitle = toolbar.findViewById(R.id.toolbar_title);
        mTitle.setText(getResources().getText(R.string.loan_repay));
        setSupportActionBar(toolbar);
        ActionBar actionbar = getSupportActionBar();
        actionbar.setDisplayHomeAsUpEnabled(true);
        actionbar.setHomeAsUpIndicator(R.drawable.ic_arrow_back);

        input_layout_id_number = findViewById(R.id.input_layout_id_number);
        identifier_deposit = findViewById(R.id.id_number);

        phoneNumber = findViewById(R.id.phone_number);
        phoneNumber.setSelection(phoneNumber.getText().length());
        phoneNumber.setInputType(InputType.TYPE_NULL);
        phoneNumber.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // showMyDialog();
            }
        });

        phoneNumber.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    // showMyDialog();
                }
            }
        });

        TextWatcher textWatcher1 = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                String inputPhone = phoneNumber.getText().toString();

                if (inputPhone.length() == 1) {
                    if (inputPhone.equals("0") || inputPhone.equals("7") || inputPhone.equals("+")) {
                        phoneNumber.setText("+254");
                        phoneNumber.setSelection(phoneNumber.getText().length());
                        toast.sinfo("Correct format auto completed. Please proceed!");
                    } else {
                        phoneNumber.setText(null);
                        toast.swarn( "Please enter correct phone number format!");
                    }
                }

            }
        };
        phoneNumber.addTextChangedListener(textWatcher1);

        amount = findViewById(R.id.enterAmount);

        identifier_deposit.setInputType(InputType.TYPE_NULL);
        identifier_deposit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // showMyDialog();
                identifier_deposit.post(new Runnable() {
                    @Override
                    public void run() {
                        identifier_deposit.setSelection(identifier_deposit.getText().toString().length());
                    }
                });
            }
        });

        identifier_deposit.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    // showMyDialog();
                    identifier_deposit.post(new Runnable() {
                        @Override
                        public void run() {
                            identifier_deposit.setSelection(identifier_deposit.getText().toString().length());
                        }
                    });
                }
            }
        });

        spinner = findViewById(R.id.growerSpinner);
        progressBar = findViewById(R.id.growersPb);

        loanBar = findViewById(R.id.loanPb);
        spinnerLoan = findViewById(R.id.productType);

        TextWatcher textWatcherId = new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                //here, after we introduced something in the EditText we get the string from it
                String identifier_number = identifier_deposit.getText().toString();
                if (identifier_deposit.length() >= 7) {
                    //arrayList.clear();
                    requestGetAccountDetails(identifier_number);
                }
                if (identifier_deposit.length() <= 5) {
                    phoneNumber.setText(null);
                    progressBar.setVisibility(View.GONE);
                    loanBar.setVisibility(View.GONE);
                    arrayList.clear();
                    spinner = findViewById(R.id.growerSpinner);
                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(LoanRepayment.this, android.R.layout.simple_spinner_item, arrayList);
                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    spinner.setAdapter(null);
                    arrayListLoan.clear();
                    spinnerLoan = findViewById(R.id.productType);
                    ArrayAdapter<String> adapter1 = new ArrayAdapter<String>(LoanRepayment.this, android.R.layout.simple_spinner_item, arrayListLoan);
                    adapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    spinnerLoan.setAdapter(null);
                }
                if (identifier_deposit.length() >= 5 && identifier_deposit.length() <= 8) {
                    phoneNumber.setText(null);
                    arrayList.clear();
                    spinner = findViewById(R.id.growerSpinner);
                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(LoanRepayment.this, android.R.layout.simple_spinner_item, arrayList);
                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    spinner.setAdapter(null);
                    arrayListLoan.clear();
                    spinnerLoan = findViewById(R.id.productType);
                    ArrayAdapter<String> adapter1 = new ArrayAdapter<String>(LoanRepayment.this, android.R.layout.simple_spinner_item, arrayListLoan);
                    adapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    spinnerLoan.setAdapter(null);
                    progressBar.setVisibility(View.VISIBLE);
                    progressBar.getIndeterminateDrawable().setColorFilter(ContextCompat.getColor(LoanRepayment.this, R.color.btn_green), PorterDuff.Mode.SRC_IN );
                }
//                Toast.makeText(ShareDeposit.this, accountNumber, Toast.LENGTH_SHORT).show();
//                requestAccountName(accountNumber);

            }
        };

        identifier_deposit.addTextChangedListener(textWatcherId);

        adapter = new ArrayAdapter<GrowerDetails>(LoanRepayment.this,
                R.layout.simple_spinner_dropdown_item, new ArrayList<GrowerDetails>());
        adapter.setDropDownViewResource(R.layout.simple_spinner_dropdown_item);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                item = parent.getItemAtPosition(position).toString();
                String text = spinner.getSelectedItem().toString();

                SharedPrefs.write(SharedPrefs.STK_APP_GROWER, text);
                Log.e("GROWER NUMBER", text);
                if (text.length()>=7) {
                    arrayListLoan.clear();
                    //checkDefaulter(text);
                    requestLoanProducts(text);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        adapterLoan = new ArrayAdapter<LoanDetails>(LoanRepayment.this,
                R.layout.simple_spinner_dropdown_item, new ArrayList<LoanDetails>());
        //adapter.setDropDownViewResource(R.layout.simple_spinner_dropdown_item);
        spinnerLoan.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                itemLoan = parent.getItemAtPosition(position).toString();
                String text1 = spinnerLoan.getSelectedItem().toString();
                String code1 = text1.substring(0,text1.indexOf("-"));
                SharedPrefs.write(SharedPrefs.STK_LOAN_KEY, code1);
                /*if (text1.length()>=1){
                    requestQualifiedAmount(text1);
                }*/
                Log.e("LOAN CODE", code1);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        submit = findViewById(R.id.btnSubmit);
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (identifier_deposit.getText().toString().matches("") || amount.getText().toString().matches("")) {
                    toast.swarn("Please fill in the missing fields to proceed!");
                } else if (phoneNumber.getText().length() < 12) {
                    toast.swarn("Invalid phone number");
                }
                else{
                    /* progress dialog */
                    progressDialog = new ProgressDialog(LoanRepayment.this);
                    progressDialog.setMessage("Submitting your request...");
                    progressDialog.setCancelable(false);
                    progressDialog.show();
                    submitStkPush();
                }
            }
        });

    }

    void submitStkPush() {
        final String TAG = "MPESA CHANGE";
        String URL = GFL.MSACCO_AGENT + GFL.STKPush;

        String accountRef = SharedPrefs.read(SharedPrefs.STK_APP_GROWER, null) + SharedPrefs.read(SharedPrefs.STK_LOAN_KEY, null);
        String numberRefined = null;
        String pNumber = phoneNumber.getText().toString();
        numberRefined = pNumber.replaceAll("[^0-9]","");

        Log.e("REFINED PHONE NUMBER", numberRefined);

        final STKPushRequest stkPushRequest = new STKPushRequest();
        stkPushRequest.userid = "";
        stkPushRequest.msisdn = numberRefined;
        stkPushRequest.accountreference = accountRef;
        stkPushRequest.amount = amount.getText().toString();
        stkPushRequest.transactiontype = "";
        stkPushRequest.agentid = SharedPrefs.read(SharedPrefs.AGENT_ID_1, null);
        stkPushRequest.terminalid = SharedPrefs.read(SharedPrefs.DEVICE_ID, null);
        stkPushRequest.longitude = getLong(this);
        stkPushRequest.latitude = getLat(this);
        stkPushRequest.date = getFormattedDate();
        stkPushRequest.corporate_no = "CAP016";

        Log.e(TAG, stkPushRequest.getBody().toString());

        GFL.instance(this).request(URL, stkPushRequest, new GFL.RequestListener() {
            @Override
            public void onSuccess(String response) {
                /*deposit_progressBar.setVisibility(View.GONE);*/

                STKPushResponse stkPushResponse = GFL.instance(LoanRepayment.this)
                        .mGson.fromJson(response, STKPushResponse.class);
                if (stkPushResponse.operationsuccess) {
                    progressDialog.dismiss();
                    try {
                        JSONObject jsonObject = new JSONObject(response);

                        String receiptNo = jsonObject.getString("receiptNo");
                        toast.ssuccess(receiptNo);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                } else {
                    progressDialog.dismiss();
                    toast.swarn("Request Failed!");
                }

            }

            @Override
            public void onTokenExpired() {

            }

            @Override
            public void onError(String error) {

            }
        });
    }

    void requestGetAccountDetails(String identifier) {
        SharedPrefs.write(SharedPrefs.LOAN_IDENTITY_NO, identifier);
        final String TAG = "GrowersAccounts";

        String URL = GFL.MSACCO_AGENT + GFL.GetGrowersDetails;

        final GrowersDetailsRequest growersDetails = new GrowersDetailsRequest();
        growersDetails.agentid = SharedPrefs.read(SharedPrefs.AGENT_ID_1, null);
        growersDetails.corporateno = "CAP016";
        growersDetails.accountidentifier = identifier;
        growersDetails.accountidentifiercode = "1";
        growersDetails.transactiontype = "1";
        growersDetails.terminalid = SharedPrefs.read(SharedPrefs.DEVICE_ID, null);
        growersDetails.longitude = getLong(this);
        growersDetails.latitude = getLat(this);
        growersDetails.date = getFormattedDate();

        Log.e(TAG, growersDetails.getBody().toString());
        arrayList.clear();
        spinner = findViewById(R.id.growerSpinner);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(LoanRepayment.this, android.R.layout.simple_spinner_item, arrayList);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(null);

        arrayListLoan.clear();
        spinnerLoan = findViewById(R.id.productType);
        ArrayAdapter<String> adapter1 = new ArrayAdapter<String>(LoanRepayment.this, android.R.layout.simple_spinner_item, arrayListLoan);
        adapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerLoan.setAdapter(null);

        GFL.instance(this).request(URL, growersDetails, new GFL.RequestListener() {
            @Override
            public void onSuccess(String response) {
                /*deposit_progressBar.setVisibility(View.GONE);*/

                GrowersDetailsResponse growersDetailsResponse = GFL.instance(LoanRepayment.this)
                        .mGson.fromJson(response, GrowersDetailsResponse.class);
                if (growersDetailsResponse.is_successful) {
                    List<GrowerDetails> growerDetailsList = new ArrayList<>();

                    try {
                        JSONObject jsonObject = new JSONObject(response);

                        JSONArray jsonArray = jsonObject.getJSONArray("Growers");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                            JSONArray jsonArray1 = jsonObject1.getJSONArray("GrowerDetails");
                            for (int l = 0; l < jsonArray1.length(); l++) {
                                JSONObject jsonObject2 = jsonArray1.getJSONObject(l);
                                progressBar.setVisibility(View.GONE);
                                arrayList.add(String.valueOf(jsonObject2.getString("GrowerNo")));
                                /*mpesaPhoneNumber = jsonObject2.getString("phonenumber");*/
                                String mobileNo = jsonObject2.getString("GrowerMobileNo");
                                String bankExists = jsonObject2.getString("BankExists");
                                String growerName = jsonObject2.getString("GrowerName");

                                /*SharedPrefs.write(SharedPrefs.LOAN_BANK_EXISTS, bankExists);
                                SharedPrefs.write(SharedPrefs.LOAN_APP_G_NAME, growerName);*/
                                phoneNumber.setText(mobileNo);

                                Log.e("MOBILE NUMBER", mobileNo);
                                Log.e("BANK EXISTS", bankExists);

                                loanBar = findViewById(R.id.loanPb);
                                loanBar.setVisibility(View.VISIBLE);
                                loanBar.getIndeterminateDrawable().setColorFilter(ContextCompat.getColor(LoanRepayment.this, R.color.btn_green), PorterDuff.Mode.SRC_IN );

                            }

                        }

                        //Growers Number
                        //spinner = (Spinner) v.findViewById(R.id.growerSpinner);
                        ArrayAdapter<String> adapter = new ArrayAdapter<String>(LoanRepayment.this, android.R.layout.simple_spinner_item, arrayList);
                        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                        spinner.setAdapter(adapter);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                } else {
//                    Toast.makeText(ShareDeposit.this, "Accounts Fetching Failed", Toast.LENGTH_LONG).show();
                }

            }

            @Override
            public void onTokenExpired() {

            }

            @Override
            public void onError(String error) {

            }
        });
    }

    void requestLoanProducts(String identifier){
        final String TAG = "LoansInfo";

        String URL = GFL.MSACCO_AGENT + GFL.GetLoansInfo;

        final GetLoansInfoRequest loansInfoRequest = new GetLoansInfoRequest();
        loansInfoRequest.corporateno = "CAP016";
        loansInfoRequest.growersno = identifier;
        loansInfoRequest.transactiontype = "1";
        loansInfoRequest.agentid = SharedPrefs.read(SharedPrefs.AGENT_ID_1, null);
        loansInfoRequest.terminalid = SharedPrefs.read(SharedPrefs.DEVICE_ID, null);
        loansInfoRequest.longitude = getLong(this);
        loansInfoRequest.latitude = getLat(this);
        loansInfoRequest.date = getFormattedDate();

        Log.e(TAG, loansInfoRequest.getBody().toString());

        GFL.instance(this).request(URL, loansInfoRequest, new GFL.RequestListener() {
            @Override
            public void onSuccess(String response) {
                /*deposit_progressBar.setVisibility(View.GONE);*/

                GetLoansInfoResponse loansInfoResponse = GFL.instance(LoanRepayment.this)
                        .mGson.fromJson(response, GetLoansInfoResponse.class);
                if (loansInfoResponse.is_successful) {
                    List<LoanDetails> loanDetailsList = new ArrayList<>();
                    arrayListLoan.clear();
                    try {
                        JSONObject jsonObject = new JSONObject(response);

                        JSONArray jsonArray = jsonObject.getJSONArray("GetLoansInfo");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                            JSONArray jsonArray1 = jsonObject1.getJSONArray("LoanDetails");
                            for (int l = 0; l < jsonArray1.length(); l++) {
                                JSONObject jsonObject2 = jsonArray1.getJSONObject(l);
                                loanBar.setVisibility(View.GONE);
                                String productType = jsonObject2.getString("ProductType");
                                String loansPayKey = jsonObject2.getString("LoansPayKey");
                                String loansStatus = jsonObject2.getString("LoansStatus");
                                String dropDown1 = loansPayKey + "-" + productType;

                                arrayListLoan.add(String.valueOf(dropDown1));
                                //SharedPrefs.write(SharedPrefs.PHONE_NUMBER, code);

                                /*Log.e("LOAN TYPE", loanType);
                                Log.e("LOAN NUMBER:", loanNo1);*/
                            }

                        }

                        //Growers Number
                        //spinner = findViewById(R.id.productType);
                        ArrayAdapter<String> adapter1 = new ArrayAdapter<String>(LoanRepayment.this, android.R.layout.simple_spinner_item, arrayListLoan);
                        adapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        spinnerLoan.setAdapter(adapter1);


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                } else {
//                    Toast.makeText(ShareDeposit.this, "Accounts Fetching Failed", Toast.LENGTH_LONG).show();
                }

            }

            @Override
            public void onTokenExpired() {

            }

            @Override
            public void onError(String error) {

            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                finish();
                //Toast.makeText(Account.this, "Back has been presses!", Toast.LENGTH_LONG).show();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.move_left_in_activity, R.anim.move_right_out_activity);
    }

}
