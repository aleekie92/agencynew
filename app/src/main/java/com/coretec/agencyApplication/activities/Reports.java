package com.coretec.agencyApplication.activities;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.cloudpos.jniinterface.PrinterInterface;
import com.coretec.agencyApplication.R;
import com.coretec.agencyApplication.activities.newmodules.MainDashboardActivity;
import com.coretec.agencyApplication.adapters.ReportsAdapter;
import com.coretec.agencyApplication.adapters.ReportsAdapterOthers;
import com.coretec.agencyApplication.api.Api;
import com.coretec.agencyApplication.api.requests.EodReportsRequest;
import com.coretec.agencyApplication.api.requests.GetAgentAccountInfo;
import com.coretec.agencyApplication.api.responses.Accounts;
import com.coretec.agencyApplication.api.responses.AccountsOtherClients;
import com.coretec.agencyApplication.api.responses.AgentAccountResponse;
import com.coretec.agencyApplication.api.responses.EodReportsResponse;
import com.coretec.agencyApplication.api.responses.EodReportsResponseOthers;
import com.coretec.agencyApplication.utils.LogOutTimerUtil;
import com.coretec.agencyApplication.utils.PreferenceFileKeys;
import com.coretec.agencyApplication.utils.SharedPrefs;
import com.coretec.agencyApplication.utils.Utils;
import com.coretec.agencyApplication.wizarpos.function.printer.PrinterCommand;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.coretec.agencyApplication.utils.Utils.getFormattedDate;
import static com.coretec.agencyApplication.utils.Utils.getIMEI;
import static com.coretec.agencyApplication.utils.Utils.getLat;
import static com.coretec.agencyApplication.utils.Utils.getLong;
public class Reports extends BaseActivity implements  LogOutTimerUtil.LogOutListener {

    private List<AccountsOtherClients> accountsList;
    private RecyclerView reports_recyclerView;
    private ReportsAdapterOthers reportsAdapter;
    SharedPreferences sharedPreferences;
    private TextView reports_agent_name;
    private TextView reports_sacco_name;
    private ProgressBar eodpb;
    private  String closingfloat;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.eod_report);

        this.overridePendingTransition(R.anim.move_right_in_activity, R.anim.move_left_out_activity);
        accountsList = new ArrayList<>();
        sharedPreferences = getSharedPreferences(PreferenceFileKeys.PREFS_FILE, Context.MODE_PRIVATE);

        String agentName = sharedPreferences.getString(PreferenceFileKeys.AGENT_ACCOUNT_NAME, "");
        String saccoName = sharedPreferences.getString(PreferenceFileKeys.AGENT_SACCO_NAME, "");

        reports_agent_name = findViewById(R.id.reports_agent_name);
        reports_agent_name.setText(agentName);

        reports_sacco_name = findViewById(R.id.reports_sacco_name);
        eodpb = findViewById(R.id.eodpb);
        eodpb.setVisibility(View.VISIBLE);
        reports_sacco_name.setText(saccoName);

        reports_recyclerView = (RecyclerView) findViewById(R.id.eodRecyclerView);
        reports_recyclerView.setHasFixedSize(false);
        reportsAdapter = new ReportsAdapterOthers(this, accountsList);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
        reports_recyclerView.setLayoutManager(mLayoutManager);
        final Map<String, Object> params = new HashMap<>();
        params.put("accountsList", accountsList);
        final String TAG = "request reports";
        String URL = Api.MSACCO_AGENT + Api.GetEODReport;
        EodReportsRequest request = new EodReportsRequest();
        request.corporateno = MainDashboardActivity.corporateno;
        request.agentid = sharedPreferences.getString(PreferenceFileKeys.AGENT_ID, "");
        request.terminalid = MainDashboardActivity.imei;
        request.longitude = getLong(this);
        request.latitude = getLat(this);
        request.requestdate = getFormattedDate();
        Log.e(TAG, request.getBody().toString());
        Api.instance(this).request(URL, request, new Api.RequestListener() {
            @Override
            public void onSuccess(String response) {
                Log.e(TAG, response);
                final EodReportsResponseOthers eodReportsResponse = Api.instance(Reports.this).mGson.fromJson(response, EodReportsResponseOthers.class);
                if (eodReportsResponse.is_successful) {
                    final String TAG = "request agent Info";
                    String URL = Api.MSACCO_AGENT + Api.GetAgentAccountInfo;
                    GetAgentAccountInfo accountInfo = new GetAgentAccountInfo();
                    accountInfo.agentid = sharedPreferences.getString(PreferenceFileKeys.AGENT_ID, "");
                    accountInfo.terminalid = getIMEI(Reports.this);
                    accountInfo.longitude = getLong(Reports.this);
                    accountInfo.latitude = getLat(Reports.this);
                    accountInfo.date = getFormattedDate();
                    Log.e(TAG, accountInfo.getBody().toString());
                    Api.instance(Reports.this).request(URL, accountInfo, new Api.RequestListener(){
                        @Override
                        public void onSuccess(String response) {
                            Log.e(TAG, response);
                            final AgentAccountResponse agentAccountResponse = Api.instance(Reports.this).mGson.fromJson(response, AgentAccountResponse.class);
                            if (agentAccountResponse.is_successful) {
                                closingfloat= agentAccountResponse.getAgentdetails().getAccountbalance();
                                eodpb.setVisibility(View.GONE);
                                accountsList.addAll(eodReportsResponse.getAccounts());
                                if(accountsList.isEmpty()){
                                    Toast.makeText(Reports.this, "No transactions captured today!", Toast.LENGTH_SHORT).show();
                                }else {
                                    reports_recyclerView.setAdapter(reportsAdapter);
                                    final String agentName = sharedPreferences.getString(PreferenceFileKeys.AGENT_ACCOUNT_NAME, "");
                                    final String agentSaccoName = sharedPreferences.getString(PreferenceFileKeys.AGENT_SACCO_NAME, "");
                                    final String agentBusinessName = sharedPreferences.getString(PreferenceFileKeys.AGENT_BUSINESS_NAME, "");
                                    final String saccoMotto = sharedPreferences.getString(PreferenceFileKeys.AGENT_SACCO_MOTTO, "");
                                    Utils.showAlertDialog(Reports.this, "End of day report", "End of day report Retrieved Successfully, click OK to print", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            PrinterInterface.open();
                                            writetest(SharedPrefs.read(SharedPrefs.VR_CLOSING_FLOAT, null),closingfloat,agentName, eodReportsResponse.getAccounts(), agentSaccoName, saccoMotto, eodReportsResponse.getTransactiondate(), "END OF DAY REPORT");
                                            SharedPrefs.write(SharedPrefs.VR_CLOSING_FLOAT, closingfloat);

                                            finish();
                                            Intent i = new Intent(Reports.this, MainDashboardActivity.class);
                                            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                                            i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                            startActivity(i);
                                        }
                                    });
                                    PrinterInterface.close();
                                    accountsList.clear();
                                }
                            } else {
                                Toast.makeText(getApplicationContext(), "Could not get agent information, please try again!", Toast.LENGTH_LONG).show();
                            }
                        }

                        @Override
                        public void onTokenExpired() {

                        }

                        @Override
                        public void onError(String error) {

                        }
                    });
                   /* eodpb.setVisibility(View.GONE);
                    accountsList.addAll(eodReportsResponse.getAccounts());
                    if(accountsList.isEmpty()){
                        Toast.makeText(Reports.this, "No transactions captured today!", Toast.LENGTH_SHORT).show();
                    }else {
                        reports_recyclerView.setAdapter(reportsAdapter);
                    }
                    //reports_recyclerView.setAdapter(reportsAdapter);//////
                    btnPrint = findViewById(R.id.btn_print_reports_receipt);

                    String coopnumber = SharedPrefs.read(SharedPrefs.VR_COOPORATE_NO, null);
                    if (coopnumber !=null){
                        switch (SharedPrefs.read(SharedPrefs.VR_COOPORATE_NO, null)) {
                            case "CAP005":
                                btnPrint.setBackgroundResource(R.drawable.btn_rounded_primary_coloraccentforcoopmis);
                                break;
                            case "CAP022":
                                btnPrint.setBackgroundResource(R.drawable.btn_rounded_primary_coloraccentforcoopmis);
                                break;
                            default:
                                btnPrint.setBackgroundResource(R.drawable.btn_rounded_primary_coloraccent);
                                break;
                        }

                    }else {
                        btnPrint.setBackgroundResource(R.drawable.btn_rounded_primary_coloraccent);
                    }

                    if(accountsList.isEmpty()){
                        btnPrint.setEnabled(false);
                        Toast.makeText(Reports.this, "No transactions captured today!", Toast.LENGTH_SHORT).show();
                    }else {
                        btnPrint.setEnabled(true);
                        reports_recyclerView.setAdapter(reportsAdapter);
                    }
                    //reports_recyclerView.setAdapter(reportsAdapter);/////
                    btnPrint.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            final String agentName = sharedPreferences.getString(PreferenceFileKeys.AGENT_ACCOUNT_NAME, "");
                            final String agentSaccoName = sharedPreferences.getString(PreferenceFileKeys.AGENT_SACCO_NAME, "");
                            final String agentBusinessName = sharedPreferences.getString(PreferenceFileKeys.AGENT_BUSINESS_NAME, "");
                            final String saccoMotto = sharedPreferences.getString(PreferenceFileKeys.AGENT_SACCO_MOTTO, "");

                            if (PrinterInterface.queryStatus() == 0) {
                                Utils.showAlertDialog(Reports.this, "NO THERMAL PAPER!", "Put thermal paper and click OK to continue.", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        Utils.showAlertDialog(Reports.this, "End of day report", "End of day report Retrieved Successfully, click OK to print", new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                PrinterInterface.open();
                                                writetest(SharedPrefs.read(SharedPrefs.VR_CLOSING_FLOAT, null),closingfloat,agentName, eodReportsResponse.getAccounts(), agentSaccoName, saccoMotto, eodReportsResponse.getTransactiondate(), "END OF DAY REPORT");

                                                Log.d("opening", "closing--->"+SharedPrefs.read(SharedPrefs.VR_CLOSING_FLOAT, null));
                                                Log.d("opening", "closing--->"+closingfloat);

                                                finish();
                                                Intent i = new Intent(Reports.this, MainDashboardActivity.class);
                                                i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                                                i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                                startActivity(i);
                                            }
                                        });
                                    }
                                });
                                PrinterInterface.close();

                            } else {
                                Utils.showAlertDialog(Reports.this, "End of day report", "End of day report Retrieved Successfully, click OK to print", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        PrinterInterface.open();
                                        writetest(SharedPrefs.read(SharedPrefs.VR_CLOSING_FLOAT, null),closingfloat,agentName, eodReportsResponse.getAccounts(), agentSaccoName, saccoMotto, eodReportsResponse.getTransactiondate(), "END OF DAY REPORT");
                                        SharedPrefs.write(SharedPrefs.VR_CLOSING_FLOAT, closingfloat);

                                        Log.d("opening", "closing--->"+SharedPrefs.read(SharedPrefs.VR_CLOSING_FLOAT, null));
                                        Log.d("opening", "closing--->"+closingfloat);

                                        finish();
                                        Intent i = new Intent(Reports.this, MainDashboardActivity.class);
                                        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                                        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                        startActivity(i);
                                    }
                                });
                                PrinterInterface.close();
                            }
                        }
                    });
*/

                } else {
                    Utils.showAlertDialog(Reports.this, "Reports Retrieval Failed", eodReportsResponse.getError());
                }
            }

            @Override
            public void onTokenExpired() {

            }

            @Override
            public void onError(String error) {

            }
        });
    }


    void requestAgentInfo() {


    }

    public void writetest(String sfloat, String cfloat, String agentName, List<AccountsOtherClients> accountsList, String saccoame, String saccoMotto, String dateed, String transactionType) {

        try {

            byte[] arrySaccoName = null;
            byte[] arryDepositAmt = null;
            byte[] arryRefID = null;
            byte[] arryAmount = null;
            byte[] arryTransactionType = null;

            byte[] arryMotto = null;
            byte[] terminalNo = null;
            byte[] date = null;
            byte[] transactionTYpe = null;
            byte[] servedBy = null;
            byte[] stfloat = null;
            byte[] clfloat = null;
            byte[] signature = null;
            byte[] id = null;


            try {
                arrySaccoName = String.valueOf("       " + String.valueOf(saccoame)).getBytes("GB2312");
                terminalNo = String.valueOf("Terminal ID :" + Utils.getIMEI(Reports.this)).getBytes("GB2312");
                stfloat = String.valueOf("Opening Float :" +sfloat).getBytes("GB2312");
                clfloat = String.valueOf("Closing Float :" + cfloat).getBytes("GB2312");

                date = String.valueOf("Date : " + dateed).getBytes("GB2312");

                transactionTYpe = String.valueOf("      " + transactionType).getBytes("GB2312");
                arryDepositAmt = String.valueOf("Ref No   Amount     Type  ").getBytes("GB2312");


                id = String.valueOf("ID :_______________________").getBytes("GB2312");
                signature = String.valueOf("SIGNATURE : _____________").getBytes("GB2312");
                servedBy = String.valueOf(" You were Served By : " + agentName).getBytes("GB2312");
                arryMotto = String.valueOf("     "+ saccoMotto).getBytes("GB2312");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            PrinterInterface.begin();

            byte[] cmd;
            cmd = new byte[]{(byte) 0x1B, (byte) 0x40};

            cmd = new byte[]{
                    (byte) 0x1B, (byte) 0x45, (byte) 0};


            write(cmd);
            write(arrySaccoName);
            writeLineBreak(2);
           /* write(stfloat);
            writeLineBreak(2);*/
            write(terminalNo);
            writeLineBreak(2);
            write(date);
            writeLineBreak(2);
            write(transactionTYpe);
            writeLineBreak(2);
            write(arryDepositAmt);

            writeLineBreak(2);

            for (int i = 0; i < accountsList.size(); i++) {

                arryRefID = String.valueOf(accountsList.get(i).getReferenceno()).getBytes("GB2312");
                write(arryRefID);
                arryAmount = String.valueOf("  " + accountsList.get(i).getAmount()).getBytes("GB2312");
                write(arryAmount);
                arryTransactionType = String.valueOf("  " + accountsList.get(i).getTransactiontype()).getBytes("GB2312");
                write(arryTransactionType);
                writeLineBreak(1);

            }
            accountsList.clear();

            writeLineBreak(2);
            write(clfloat);
            writeLineBreak(2);
            write(id);
            writeLineBreak(2);
            write(signature);
            writeLineBreak(2);
            write(servedBy);
            writeLineBreak(2);
            write(arryMotto);
            writeLineBreak(4);
            PrinterInterface.end();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void write(final byte[] arryData) {
        int result = PrinterInterface.write(arryData, arryData.length);
    }

    // print line break
    private void writeLineBreak(int lineNumber) {
        // write(new byte[]{'\n'});
        write(PrinterCommand.getCmdEscDN(lineNumber));
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.move_left_in_activity, R.anim.move_right_out_activity);
    }
    @Override
    protected void onStart() {
        super.onStart();
        // LogOutTimerUtil.startLogoutTimer(this, this);

    }

    @Override
    public void onUserInteraction() {
        super.onUserInteraction();
        LogOutTimerUtil.startLogoutTimer(this, this);

    }


    @Override
    protected void onPause() {
        super.onPause();
        LogOutTimerUtil.startLogoutTimer(this, this);

    }

    @Override
    protected void onResume() {
        super.onResume();
        LogOutTimerUtil.startLogoutTimer(this, this);

    }

    @Override
    public void doLogout() {
        finish();
    }
}
