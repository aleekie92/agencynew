package com.coretec.agencyApplication.activities;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.Toast;

import com.coretec.agencyApplication.R;
import com.coretec.agencyApplication.activities.newmodules.MainDashboardActivity;
import com.coretec.agencyApplication.api.Api;
import com.coretec.agencyApplication.api.requests.AccountDetailsRequest;
import com.coretec.agencyApplication.api.responses.AccountDetailsResponse;
import com.coretec.agencyApplication.api.responses.SaccoAccounts;
import com.coretec.agencyApplication.model.AccountDetails;
import com.coretec.agencyApplication.utils.PreferenceFileKeys;
import com.coretec.agencyApplication.utils.QuickToast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import static com.coretec.agencyApplication.utils.Utils.getFormattedDate;
import static com.coretec.agencyApplication.utils.Utils.getLat;
import static com.coretec.agencyApplication.utils.Utils.getLong;


public class CashTransfer extends BaseActivity {

    private Button btn_account_search,btn_confirm_deposit,btn_cancel_deposit;
    private EditText transfer_identifier;
    private QuickToast toast = new QuickToast(this);
    private ProgressBar progressBar;
    SharedPreferences sharedPreferences;
    private ArrayAdapter<AccountDetails> adapter;
    private Spinner spinner;
    private ArrayList<String> arrayList = new ArrayList<>();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_cash_transfer);

        this.overridePendingTransition(R.anim.move_right_in_activity, R.anim.move_left_out_activity);

        transfer_identifier=findViewById(R.id.transfer_identifier);
        btn_account_search=findViewById(R.id.btn_account_search);
        btn_confirm_deposit=findViewById(R.id.btn_confirm_deposit);
        btn_cancel_deposit=findViewById(R.id.btn_cancel_deposit);
        progressBar=findViewById(R.id.progressBar);
        spinner=findViewById(R.id.spinner);

        sharedPreferences = getSharedPreferences(PreferenceFileKeys.PREFS_FILE, Context.MODE_PRIVATE);

        btn_account_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String id = transfer_identifier.getText().toString();

                if(transfer_identifier.getText().toString().matches("")) {
                    toast.swarn("Please enter the identifier number to proceed!");
                } else {
                    progressBar.setVisibility(View.VISIBLE);
                    adapter = new ArrayAdapter<AccountDetails>(getApplicationContext(),
                            R.layout.simple_spinner_dropdown_item, new ArrayList<AccountDetails>());
                    adapter.setDropDownViewResource(R.layout.simple_spinner_dropdown_item);
                    adapter.clear();

                    spinner.setAdapter(adapter);
                    requestGetAccountDetails(id);
                }
            }
        });

        btn_cancel_deposit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(CashTransfer.this, "cancel", Toast.LENGTH_SHORT).show();
            }
        });
        btn_confirm_deposit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(CashTransfer.this, "confirm", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.move_left_in_activity, R.anim.move_right_out_activity);
    }

    void requestGetAccountDetails(String identifier) {

        final String TAG = "MemberAccounts";
        String URL = Api.MSACCO_AGENT + Api.GetAllMemberSaccoDetails;

        final AccountDetailsRequest accountDetails = new AccountDetailsRequest();
        accountDetails.agentid = sharedPreferences.getString(PreferenceFileKeys.AGENT_ID, "");
        accountDetails.corporateno = MainDashboardActivity.corporateno;
        accountDetails.accountidentifier = identifier;
        accountDetails.accountidentifiercode = "0";//identifiercode;
        accountDetails.transactiontype = "W";
        accountDetails.terminalid = MainDashboardActivity.imei;
        accountDetails.longitude = getLong(this);
        accountDetails.latitude = getLat(this);
        accountDetails.date = getFormattedDate();

        Log.e(TAG, accountDetails.getBody().toString());

        Api.instance(this).request(URL, accountDetails, new Api.RequestListener() {
            @Override
            public void onSuccess(String response) {
                progressBar.setVisibility(View.GONE);

                AccountDetailsResponse accountDetailsResponse = Api.instance(CashTransfer.this)
                        .mGson.fromJson(response, AccountDetailsResponse.class);
                if (accountDetailsResponse.is_successful) {
                    //Log.d("myresponce",accountDetailsResponse.getError());
                    progressBar.setVisibility(View.GONE);
                    List<AccountDetails> accountDetailsList = new ArrayList<>();
                    adapter = new ArrayAdapter<AccountDetails>(getApplicationContext(),
                            R.layout.simple_spinner_dropdown_item, new ArrayList<AccountDetails>());
                    adapter.setDropDownViewResource(R.layout.simple_spinner_dropdown_item);
                    adapter.clear();
                    spinner.setAdapter(adapter);

                    for (SaccoAccounts saccoAccounts : accountDetailsResponse.getSaccoAccounts()) {
                        accountDetailsList.addAll(saccoAccounts.getAccountDetails());
                    }

                    adapter = new ArrayAdapter<AccountDetails>(getApplicationContext(),
                            R.layout.simple_spinner_dropdown_item, new ArrayList<AccountDetails>());
                    adapter.setDropDownViewResource(R.layout.simple_spinner_dropdown_item);
                    adapter.addAll(accountDetailsList);
                    spinner.setAdapter(adapter);

                } else {
                    toast.swarn("Please confirm identity number and try again!");
                    progressBar.setVisibility(View.GONE);
                    //Toast.makeText(CashWithdrawal.this, "Operation failed. Please try again!", Toast.LENGTH_SHORT).show();
                }

                try {
                    JSONObject jsonObject = new JSONObject(response);

                    JSONArray jsonArray = jsonObject.getJSONArray("saccoaccounts");
                    for (int i = 0; i < jsonArray.length(); i++) {
                        JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                        JSONArray jsonArray1 = jsonObject1.getJSONArray("accountdetails");
                        for (int l = 0; l < jsonArray1.length(); l++) {
                            JSONObject jsonObject2 = jsonArray1.getJSONObject(l);
                            arrayList.add(String.valueOf(jsonObject2.getString("accountno") + " " + jsonObject2.getString("accounttype")));
                        }

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }

            @Override
            public void onTokenExpired() {

            }

            @Override
            public void onError(String error) {

            }
        });
    }


}
