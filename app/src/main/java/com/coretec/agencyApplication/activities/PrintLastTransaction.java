package com.coretec.agencyApplication.activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;

import com.cloudpos.jniinterface.PrinterInterface;
import com.coretec.agencyApplication.R;
import com.coretec.agencyApplication.activities.newmodules.MainDashboardActivity;
import com.coretec.agencyApplication.api.Api;
import com.coretec.agencyApplication.api.requests.RequestPrintLastTransaction;
import com.coretec.agencyApplication.api.responses.PrintLastTransactionResponse;
import com.coretec.agencyApplication.utils.LogOutTimerUtil;
import com.coretec.agencyApplication.utils.PreferenceFileKeys;
import com.coretec.agencyApplication.utils.SharedPrefs;
import com.coretec.agencyApplication.utils.Utils;
import com.coretec.agencyApplication.wizarpos.function.printer.PrinterCommand;

import java.io.UnsupportedEncodingException;
public class PrintLastTransaction extends BaseActivity implements  LogOutTimerUtil.LogOutListener{

    SharedPreferences sharedPreferences;
    ProgressDialog progressDialog;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_last_transaction);

        this.overridePendingTransition(R.anim.move_right_in_activity, R.anim.move_left_out_activity);

        sharedPreferences = getSharedPreferences(PreferenceFileKeys.PREFS_FILE, Context.MODE_PRIVATE);
        progressDialog = new ProgressDialog(PrintLastTransaction.this);
        progressDialog.setMessage("Printing last transaction, please wait...");
        progressDialog.setCancelable(false);
        progressDialog.show();
        requestLastTransaction();
    }

    void requestLastTransaction() {

        final String TAG = "printLastTransaction";

        String URL = Api.MSACCO_AGENT + Api.GetAgentLastTransaction;
        RequestPrintLastTransaction printLastTransaction = new RequestPrintLastTransaction();
        printLastTransaction.agentid = sharedPreferences.getString(PreferenceFileKeys.AGENT_ID, "");
        printLastTransaction.terminalid = MainDashboardActivity.imei;

        Log.e(TAG, printLastTransaction.getBody().toString());
        Api.instance(this).request(URL, printLastTransaction, new Api.RequestListener() {
            @Override
            public void onSuccess(String response) {
                Log.e(TAG, response);
                final PrintLastTransactionResponse lastTransactionResponse = Api.instance(PrintLastTransaction.this).mGson.fromJson(response, PrintLastTransactionResponse.class);
                if (lastTransactionResponse.is_successful) {
                    progressDialog.dismiss();
                    final String agentName = sharedPreferences.getString(PreferenceFileKeys.AGENT_ACCOUNT_NAME, "");
                    final String saccoMotto = sharedPreferences.getString(PreferenceFileKeys.AGENT_SACCO_MOTTO, "");

                    if (PrinterInterface.queryStatus() == 0) {
                        Utils.showAlertDialog(PrintLastTransaction.this, "NO THERMAL PAPER!", "Put thermal paper and click OK to continue.", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                                Utils.showAlertDialog(PrintLastTransaction.this, "Last Transaction", "Last Transaction Retrieved Successfully, click OK to print", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        PrinterInterface.open();
                                        writetest(agentName, lastTransactionResponse.getAmount(), lastTransactionResponse.getSacconame(), saccoMotto, lastTransactionResponse.getReceiptno(), lastTransactionResponse.getTransactiondate(), lastTransactionResponse.getTransactiontype(),
                                                lastTransactionResponse.getDescription(), lastTransactionResponse.AccountNo, lastTransactionResponse.getAccountName(),
                                                lastTransactionResponse.AccountName);
                                        progressDialog.dismiss();

                                        finish();
                                        Intent i = new Intent(PrintLastTransaction.this, MainDashboardActivity.class);
                                        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                                        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                        startActivity(i);
                                    }
                                });
                            }
                        });
                        PrinterInterface.close();
                    } else {
                        Utils.showAlertDialog(PrintLastTransaction.this, "Last Transaction", "Last Transaction Retrieved Successfully, click OK to print", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                PrinterInterface.open();
                                writetest(agentName, lastTransactionResponse.getAmount(), lastTransactionResponse.getSacconame(), saccoMotto, lastTransactionResponse.getReceiptno(), lastTransactionResponse.getTransactiondate(), lastTransactionResponse.getTransactiontype(),
                                        lastTransactionResponse.getDescription(), lastTransactionResponse.AccountNo, lastTransactionResponse.getAccountName(),
                                        lastTransactionResponse.AccountName);
                                progressDialog.dismiss();
                                finish();
                                Intent i = new Intent(PrintLastTransaction.this, MainDashboardActivity.class);
                                i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                                i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(i);
                            }
                        });
                        PrinterInterface.close();
                    }

                } else {
                    progressDialog.dismiss();
                    Utils.showAlertDialog(PrintLastTransaction.this, "Printing Failed", lastTransactionResponse.getError());
                }

            }

            @Override
            public void onTokenExpired() {

            }

            @Override
            public void onError(String error) {

            }
        });

    }


    public void writetest(String agentName, String amount, String saccoame, String saccoMotto, String receiptNumber, String dateed, String transactionType,
                          String description, String accountNo, String accountName, String depositedBy) {
        try {

            byte[] arrySaccoName = null;
            byte[] arryWithdrawAmt = null;
            byte[] arryAmt = null;

            byte[] arryMotto = null;
            byte[] terminalNo = null;
            byte[] receiptNo = null;
            byte[] date = null;
            byte[] transactionTYpe = null;

            byte[] account = null;
            byte[] desc = null;
            byte[] depBy = null;

            byte[] id = null;
            byte[] signature = null;
            byte[] servedBy = null;


            try {
                arrySaccoName = String.valueOf("    " + String.valueOf(saccoame)).getBytes("GB2312");
                terminalNo = String.valueOf("Terminal ID :" + Utils.getIMEI(PrintLastTransaction.this)).getBytes("GB2312");
                receiptNo = String.valueOf("Copy Receipt Number : " + receiptNumber).getBytes("GB2312");
                date = String.valueOf("Date :" + dateed).getBytes("GB2312");
                transactionTYpe = String.valueOf("Transaction :" + transactionType).getBytes("GB2312");
                arryWithdrawAmt = String.valueOf("Total Amount is :").getBytes("GB2312");
                arryAmt = String.valueOf(String.valueOf("            " + SharedPrefs.read(SharedPrefs.CURRENCY, null) + " :" + amount)).getBytes("GB2312");

                account = String.valueOf(String.valueOf("Account : " + accountNo + " - " + accountName)).getBytes("GB2312");
                desc = String.valueOf(String.valueOf("Description : " + description)).getBytes("GB2312");
                depBy = String.valueOf(String.valueOf("Deposited By : " + depositedBy)).getBytes("GB2312");

                id = String.valueOf("ID________________________").getBytes("GB2312");
                signature = String.valueOf("Signature______________________").getBytes("GB2312");
                servedBy = String.valueOf(" You were Served By " + agentName).getBytes("GB2312");

                arryMotto = String.valueOf("     "+ saccoMotto).getBytes("GB2312");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            PrinterInterface.begin();

            byte[] cmd;
            cmd = new byte[]{(byte) 0x1B, (byte) 0x40};

            cmd = new byte[]{
                    (byte) 0x1B, (byte) 0x45, (byte) 0};

            write(cmd);
            writeLineBreak(1);
            write(arrySaccoName);
            writeLineBreak(1);
            write(terminalNo);
            writeLineBreak(1);
            write(receiptNo);
            writeLineBreak(1);
            write(date);
            writeLineBreak(1);
            write(transactionTYpe);
            writeLineBreak(2);

            write(arryWithdrawAmt);
            writeLineBreak(1);
            write(arryAmt);
            // print line break
            writeLineBreak(2);

            write(account);
            writeLineBreak(1);
            write(desc);
            writeLineBreak(1);
            write(depBy);
            writeLineBreak(2);

            write(id);
            writeLineBreak(2);
            write(signature);
            writeLineBreak(2);
            write(servedBy);
            writeLineBreak(2);
            write(arryMotto);
            writeLineBreak(3);
            PrinterInterface.end();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void write(final byte[] arryData) {
        int result = PrinterInterface.write(arryData, arryData.length);
    }

    // print line break
    private void writeLineBreak(int lineNumber) {
        // write(new byte[]{'\n'});
        write(PrinterCommand.getCmdEscDN(lineNumber));
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.move_left_in_activity, R.anim.move_right_out_activity);
    }

    @Override
    protected void onStart() {
        super.onStart();
        // LogOutTimerUtil.startLogoutTimer(this, this);

    }

    @Override
    public void onUserInteraction() {
        super.onUserInteraction();
        com.coretec.agencyApplication.utils.LogOutTimerUtil.startLogoutTimer(this, this);

    }


    @Override
    protected void onPause() {
        super.onPause();
        com.coretec.agencyApplication.utils.LogOutTimerUtil.startLogoutTimer(this, this);

    }

    @Override
    protected void onResume() {
        super.onResume();
        LogOutTimerUtil.startLogoutTimer(this, this);

    }

    @Override
    public void doLogout() {
        finish();
    }
}
