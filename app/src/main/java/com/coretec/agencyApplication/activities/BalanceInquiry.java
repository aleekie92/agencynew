package com.coretec.agencyApplication.activities;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.Toast;

import com.cloudpos.jniinterface.PrinterInterface;
import com.coretec.agencyApplication.R;
import com.coretec.agencyApplication.activities.newmodules.MainDashboardActivity;
import com.coretec.agencyApplication.api.Api;
import com.coretec.agencyApplication.api.requests.AccountDetailsRequest;
import com.coretec.agencyApplication.api.requests.GetBalanceRequest;
import com.coretec.agencyApplication.api.requests.SendTextMessageRequest;
import com.coretec.agencyApplication.api.responses.AccountDetailsResponse;
import com.coretec.agencyApplication.api.responses.BalanceResponse;
import com.coretec.agencyApplication.api.responses.SaccoAccounts;
import com.coretec.agencyApplication.api.responses.SendTextMessageResponse;
import com.coretec.agencyApplication.model.AccountDetails;
import com.coretec.agencyApplication.utils.PreferenceFileKeys;
import com.coretec.agencyApplication.utils.QuickToast;
import com.coretec.agencyApplication.utils.SharedPrefs;
import com.coretec.agencyApplication.utils.Utils;
import com.coretec.agencyApplication.wizarpos.function.ActionCallbackImpl;
import com.coretec.agencyApplication.wizarpos.function.printer.PrinterCommand;
import com.coretec.agencyApplication.wizarpos.mvc.base.ActionCallback;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import static com.coretec.agencyApplication.utils.Utils.getFormattedDate;
import static com.coretec.agencyApplication.utils.Utils.getLat;
import static com.coretec.agencyApplication.utils.Utils.getLong;
public class BalanceInquiry extends BaseActivity {

    public static Handler handler;
    public static ActionCallback callback;
    private Button btnConfirm,btn_account_search;
    private ProgressBar bal_progressBar;
    private TextInputLayout input_layout_bal_phone_number;
    private TextInputLayout input_layout_telephone;
    private EditText txt_bal_phone_number;
    private EditText telephone;
    private String phone_number;
    private String identifierCode;
    private Spinner spinnerBalanceIdentifier;
    private ArrayAdapter<String> balanceIdentifierAdapter;
    private SharedPreferences sharedPreferences;

    private String accountNo = "";

    private Spinner spinner2;
    private ArrayList<String> arrayList2 = new ArrayList<>();
    private ArrayList<String> arrayList;
    private ArrayAdapter<AccountDetails> adapter2;

    private ProgressBar progressBar;
    private ProgressDialog progressDialog;
    private QuickToast toast = new QuickToast(this);

    private RadioGroup radioGroupAutharization;
    public RadioButton rbpin, rbbio;
    private static String autmode;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_balance_inquiry);
        this.overridePendingTransition(R.anim.move_right_in_activity, R.anim.move_left_out_activity);
        sharedPreferences = getSharedPreferences(PreferenceFileKeys.PREFS_FILE, Context.MODE_PRIVATE);

        handler = new Handler();
        callback = new ActionCallbackImpl(handler);
        sharedPreferences = getSharedPreferences(PreferenceFileKeys.PREFS_FILE, Context.MODE_PRIVATE);
        SharedPrefs.write(SharedPrefs.FP_BOOLEAN, "");
        rbpin = findViewById(R.id.rbpin);
        rbbio = findViewById(R.id.rbbio);
        bal_progressBar = findViewById(R.id.bal_progressbar);
        bal_progressBar.setVisibility(View.GONE);

        txt_bal_phone_number = findViewById(R.id.bal_identifier);
        input_layout_bal_phone_number = findViewById(R.id.input_layout_bal_identifier);

        telephone = findViewById(R.id.telephone);
        input_layout_telephone = findViewById(R.id.input_layout_bal_tel);
        btn_account_search = findViewById(R.id.btn_account_search);
        //telephone.setText("+2547");

        progressBar = findViewById(R.id.id_pb);
        btn_account_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String identifier_number = txt_bal_phone_number.getText().toString();

                if(txt_bal_phone_number.getText().toString().matches("")) {
                    toast.swarn("Please enter ID number to proceed!");
                } else {
                    progressBar.setVisibility(View.VISIBLE);
                    requestGetAccountDetails(identifier_number);
                }
            }
        });

        TextWatcher textWatcher = new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {

                    adapter2 = new ArrayAdapter<AccountDetails>(getApplicationContext(),
                            R.layout.simple_spinner_dropdown_item, new ArrayList<AccountDetails>());
                    adapter2.setDropDownViewResource(R.layout.simple_spinner_dropdown_item);
                    adapter2.clear();

                    telephone.setText(" ");
                    spinner2.setAdapter(adapter2);

            }
        };

        txt_bal_phone_number.addTextChangedListener(textWatcher);

        spinnerBalanceIdentifier = (Spinner) findViewById(R.id.spinnerBalanceIdentifier);
        balanceIdentifierAdapter = new ArrayAdapter<String>(getApplicationContext(), R.layout.simple_spinner_dropdown_item, arrayList2);
        balanceIdentifierAdapter.setDropDownViewResource(R.layout.simple_spinner_dropdown_item);
        spinnerBalanceIdentifier.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                identifierCode = String.valueOf(position);

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        spinner2 = (Spinner) findViewById(R.id.deposit_accounts);
        spinner2.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                AccountDetails accountDetails = (AccountDetails) parent.getItemAtPosition(position);
                accountNo = accountDetails.getAccountno();
                telephone.setText((accountDetails.getPhonenumber()) );
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        radioGroupAutharization = findViewById(R.id.radioGroupAutharization);
        radioGroupAutharization.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (checkedId == R.id.rbpin) {
                    autmode = "1";
                    btnConfirm.setClickable(true);
                }
                if (checkedId == R.id.rbbio) {
                    autmode = "2";
                    btnConfirm.setClickable(true);
                }
            }
        });

        btnConfirm =  findViewById(R.id.submitBalanceInquiry);
        String coopnumber = SharedPrefs.read(SharedPrefs.VR_COOPORATE_NO, null);
        if (coopnumber !=null){
            switch (SharedPrefs.read(SharedPrefs.VR_COOPORATE_NO, null)) {
                case "CAP005":
                    btnConfirm.setBackgroundResource(R.drawable.btn_rounded_primary_coloraccentforcoopmis);
                    break;
                case "CAP022":
                    btnConfirm.setBackgroundResource(R.drawable.btn_rounded_primary_coloraccentforcoopmis);
                    break;
                default:
                    btnConfirm.setBackgroundResource(R.drawable.btn_rounded_primary_coloraccent);
                    break;
            }

        }else {
            btnConfirm.setBackgroundResource(R.drawable.btn_rounded_primary_coloraccent);
        }
        btnConfirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                phone_number = txt_bal_phone_number.getText().toString().trim();
                String tele_phone = telephone.getText().toString().trim();

                if (phone_number.isEmpty()) {
                    input_layout_bal_phone_number.setError("Enter ID number");
                } else if (tele_phone.isEmpty()) {
                    input_layout_telephone.setError("Enter phone number");
                }


                else {
                    if (rbpin.isChecked() || rbbio.isChecked()) {
                    bal_progressBar.setVisibility(View.VISIBLE);
                    progressDialog = new ProgressDialog(BalanceInquiry.this);
                    progressDialog.setMessage("Processing your request, please wait...");
                    progressDialog.setCancelable(false);
                    progressDialog.show();


                        if (spinner2 != null && spinner2.getSelectedItem() != null) {
                            switch (autmode) {
                                case "1":
                                    requestBalanceForPinAuth(phone_number, accountNo);
                                    break;
                                case "2":
                                    String identity=txt_bal_phone_number.getText().toString();
                                    requestFingerPrints(BalanceInquiry.this, identity ,bal_progressBar);
                                    break;
                                default:
                                    requestBalanceForPinAuth(phone_number, accountNo);

                            }
                        } else {
                            progressDialog.dismiss();
                            Toast.makeText(BalanceInquiry.this, "Search account details", Toast.LENGTH_SHORT).show();
                        }

                    btnConfirm.setActivated(false);

                    } else {
                        Toast.makeText(BalanceInquiry.this, "Please select authentication method", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });

    }

    void requestGetAccountDetails(String identifier) {
        final String TAG = "MemberAccounts";
        String URL = Api.MSACCO_AGENT + Api.GetAllMemberSaccoDetails;

        final AccountDetailsRequest accountDetails = new AccountDetailsRequest();
        accountDetails.agentid = sharedPreferences.getString(PreferenceFileKeys.AGENT_ID, "");
        accountDetails.corporateno = MainDashboardActivity.corporateno;
        accountDetails.accountidentifier = identifier;
        accountDetails.accountidentifiercode = identifierCode;
        accountDetails.transactiontype = "B";
        accountDetails.terminalid = MainDashboardActivity.imei;
        accountDetails.longitude = getLong(this);
        accountDetails.latitude = getLat(this);
        accountDetails.date = getFormattedDate();

        Log.e(TAG, accountDetails.getBody().toString());

        Api.instance(this).request(URL, accountDetails, new Api.RequestListener() {
            @Override
            public void onSuccess(String response) {
                bal_progressBar.setVisibility(View.GONE);

                AccountDetailsResponse accountDetailsResponse = Api.instance(BalanceInquiry.this)
                        .mGson.fromJson(response, AccountDetailsResponse.class);

                if (accountDetailsResponse.is_successful) {
                    progressBar.setVisibility(View.GONE);
                    List<AccountDetails> accountDetailsList = new ArrayList<>();
                    adapter2 = new ArrayAdapter<AccountDetails>(getApplicationContext(),
                            R.layout.simple_spinner_dropdown_item, new ArrayList<AccountDetails>());
                    adapter2.setDropDownViewResource(R.layout.simple_spinner_dropdown_item);
                    adapter2.clear();
                    spinner2.setAdapter(adapter2);
                    for (SaccoAccounts saccoAccounts : accountDetailsResponse.getSaccoAccounts()) {
                        accountDetailsList.addAll(saccoAccounts.getAccountDetails());
                    }

                    adapter2.addAll(accountDetailsList);
                    spinner2.setAdapter(adapter2);

                } else {
                    progressBar.setVisibility(View.GONE);
                    toast.swarn("Please enter the correct ID number and try again!");
                    adapter2 = new ArrayAdapter<AccountDetails>(getApplicationContext(),
                            R.layout.simple_spinner_dropdown_item, new ArrayList<AccountDetails>());
                    adapter2.setDropDownViewResource(R.layout.simple_spinner_dropdown_item);
                    adapter2.clear();
                    spinner2.setAdapter(adapter2);
                    if (txt_bal_phone_number.getText().toString().length() == 8) {
                        Toast.makeText(BalanceInquiry.this, accountDetailsResponse.getError().toString(), Toast.LENGTH_LONG).show();
                    }
                }
            }

            @Override
            public void onTokenExpired() {

            }

            @Override
            public void onError(String error) {

            }
        });
    }

    void requestBalanceForPinAuth(String identifier, String accountno) {
        final String TAG = "request balance";
        Log.e(TAG, identifier);

        final Timer t = new Timer();
        t.schedule(new TimerTask() {
            @Override
            public void run() {
                progressDialog.dismiss();
                t.cancel();
            }
        }, 6000);

        String URL = Api.MSACCO_AGENT + Api.GetBalanceInquiry;

        GetBalanceRequest balrequest = new GetBalanceRequest();
        balrequest.accountidentifier = identifier;
        balrequest.accountidentifiercode = identifierCode;
        balrequest.corporate_no = MainDashboardActivity.corporateno;
        balrequest.accountno = accountno;
        balrequest.agentid = sharedPreferences.getString(PreferenceFileKeys.AGENT_ID, "");
        balrequest.terminalid = MainDashboardActivity.imei;
        balrequest.longitude = getLong(this);
        balrequest.latitude = getLat(this);
        balrequest.date = getFormattedDate();

        Log.e(TAG, balrequest.getBody().toString());

        Api.instance(this).request(URL, balrequest, new Api.RequestListener() {
            @Override
            public void onSuccess(String response) {
                Log.e(TAG, response);
                displayBalance(response);

            }

            @Override
            public void onTokenExpired() {

            }

            @Override
            public void onError(String error) {

            }
        });

    }


    void requestBalanceForBioAuth(String identifier, String accountno) {
        final String TAG = "request balance";
        Log.e(TAG, identifier);

        final Timer t = new Timer();
        t.schedule(new TimerTask() {
            @Override
            public void run() {
                progressDialog.dismiss();
                t.cancel();
            }
        }, 6000);

        String URL = Api.MSACCO_AGENT + Api.GetBalanceInquiry;

        GetBalanceRequest balrequest = new GetBalanceRequest();
        balrequest.accountidentifier = identifier;
        balrequest.accountidentifiercode = identifierCode;
        balrequest.corporate_no = MainDashboardActivity.corporateno;
        balrequest.accountno = accountno;
        balrequest.agentid = sharedPreferences.getString(PreferenceFileKeys.AGENT_ID, "");
        balrequest.terminalid = MainDashboardActivity.imei;
        balrequest.longitude = getLong(this);
        balrequest.latitude = getLat(this);
        balrequest.date = getFormattedDate();

        Log.e(TAG, balrequest.getBody().toString());

        Api.instance(this).bioauthrequest(URL, balrequest, new Api.RequestListener() {
            @Override
            public void onSuccess(String response) {
                Log.e(TAG, response);
                displayBalance(response);

            }

            @Override
            public void onTokenExpired() {

            }

            @Override
            public void onError(String error) {

            }
        });

    }

    void displayBalance(String response) {
        final BalanceResponse balanceResponse = Api.instance(this).mGson.fromJson(response, BalanceResponse.class);
        final double amount = balanceResponse.getBalance().getAmount();
        final String accountNo = balanceResponse.getBalance().getAccountno();
        final String accountName = balanceResponse.getBalance().getAccount();
        final String userName = balanceResponse.getCustomername();

        final String rNo = balanceResponse.getReceiptno();
        final String transType = balanceResponse.getTransactiontype();
        final String saccoName = balanceResponse.getSacconame();
        final String transDate = balanceResponse.getTransactiondate();
        if (balanceResponse.is_successful) {
            bal_progressBar.setVisibility(View.GONE);

            final String agentName = sharedPreferences.getString(PreferenceFileKeys.AGENT_ACCOUNT_NAME, "");
            final String saccoMotto = sharedPreferences.getString(PreferenceFileKeys.AGENT_SACCO_MOTTO, "");

            sendSMS(userName, amount, accountName, agentName, rNo,transType,saccoName,transDate,saccoMotto);

        } else {
            progressDialog.dismiss();
            Utils.showAlertDialog(BalanceInquiry.this, "Balance Retrieval Failed", balanceResponse.getError());
        }
    }

    public void sendSMS(final String name, final Double balance, final String acName, final String agent, final String receiptNo, final String transactionType, final String saccName, final String dated, final String motto) {

        String message ="Balance Enquiry\nHi "+name+ ", your total balance for account " +acName+ " is "+SharedPrefs.read(SharedPrefs.CURRENCY, null) + " :" + balance +".\nYou were served by "+ agent +". Thank you.";

        //put api request and response here
        final String TAG = "Balance Inquiry";
        String URL = Api.MSACCO_AGENT + Api.SendTextMessage;

        final SendTextMessageRequest textMessageRequest = new SendTextMessageRequest();
        textMessageRequest.corporateno = MainDashboardActivity.corporateno;
        textMessageRequest.telephone = telephone.getText().toString();
        textMessageRequest.text = message;
        textMessageRequest.agentid = sharedPreferences.getString(PreferenceFileKeys.AGENT_ID, "");
        textMessageRequest.terminalid = MainDashboardActivity.imei;
        textMessageRequest.longitude = getLong(this);
        textMessageRequest.latitude = getLat(this);
        textMessageRequest.date = getFormattedDate();
        //textMessageRequest.corporate_no = "CAP016";

        Log.e(TAG, textMessageRequest.getBody().toString());

        Api.instance(this).request(URL, textMessageRequest, new Api.RequestListener() {
            @Override
            public void onSuccess(String response) {

                SendTextMessageResponse textMessageResponse = Api.instance(BalanceInquiry.this)
                        .mGson.fromJson(response, SendTextMessageResponse.class);

                if (textMessageResponse.is_successful) {
                    try {
                        JSONObject jsonObject = new JSONObject(response);

                      /*  if (PrinterInterface.queryStatus() == 0) {
                            Utils.showAlertDialog(BalanceInquiry.this, "NO THERMAL PAPER!", "Put thermal paper and click OK to continue.", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {

                                    Utils.showAlertDialog(BalanceInquiry.this, "Balance Inquiry", "Text message sent successfully!, click OK to print receipt", new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            PrinterInterface.open();
                                            writetest( agent, balance, accountNo,  acName,  saccName,  motto,  name,  receiptNo, dated, transactionType);
                                            progressDialog.dismiss();
                                            finish();
                                            Intent i = new Intent(BalanceInquiry.this, MainDashboardActivity.class);
                                            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                                            i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                            startActivity(i);
                                        }
                                    });

                                }
                            });
                            PrinterInterface.close();

                        } else {*/
                            Utils.showAlertDialog(BalanceInquiry.this, "Balance Inquiry", "Text message sent successfully!", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    //PrinterInterface.open();
                                    //writetest( agent, balance, accountNo, acName, saccName,  motto,  name,  receiptNo, dated, transactionType);
                                    progressDialog.dismiss();
                                    finish();
                                    Intent i = new Intent(BalanceInquiry.this, MainDashboardActivity.class);
                                    i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                                    i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                    startActivity(i);
                                }
                            });
                            PrinterInterface.close();
                       // }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                } else {
                    Toast.makeText(BalanceInquiry.this, "Operation Failed, please try again", Toast.LENGTH_LONG).show();
                }

            }

            @Override
            public void onTokenExpired() {

            }

            @Override
            public void onError(String error) {

            }
        });

    }

    public void writetest(String agentName, double amount, String accountNo, String accountName, String saccoame, String saccoMotto, String customerName, String receiptNumber, String dateed, String transactionType) {
        try {
            byte[] arryBeginText = null;
            byte[] arryAmt = null;
            byte[] arryAccType = null;
            byte[] arryAccNo = null;
            byte[] memberName = null;
            byte[] arryMotto = null;
            byte[] terminalNo = null;
            byte[] receiptNo = null;
            byte[] date = null;
            byte[] transactionTYpe = null;
            byte[] servedBy = null;
            byte[] signature = null;

            try {
                arryBeginText = String.valueOf("       " + saccoame).getBytes("GB2312");
                terminalNo = String.valueOf(" Terminal ID :" + Utils.getIMEI(BalanceInquiry.this)).getBytes("GB2312");
                receiptNo = String.valueOf(" Receipt Number : " + receiptNumber).getBytes("GB2312");
                date = String.valueOf(" Date : " + dateed).getBytes("GB2312");
                memberName = String.valueOf(" Name : " + customerName).getBytes("GB2312");
                transactionTYpe = String.valueOf("       " + transactionType).getBytes("GB2312");
                arryAccType = String.valueOf(" Acc Type " + accountNo).getBytes("GB2312");
                arryAccNo = String.valueOf(" Acc No " + accountName).getBytes("GB2312");
                arryAmt = String.valueOf(" Total Balance is : " + SharedPrefs.read(SharedPrefs.CURRENCY, null) + " :"  + amount).getBytes("GB2312");
                servedBy = String.valueOf(" You were Served By " + agentName).getBytes("GB2312");
                signature = String.valueOf("SIGNATURE________________ ").getBytes("GB2312");
                arryMotto = String.valueOf("     "+ saccoMotto).getBytes("GB2312");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            PrinterInterface.begin();

            byte[] cmd;
            cmd = new byte[]{(byte) 0x1B, (byte) 0x40};

            cmd = new byte[]{
                    (byte) 0x1B, (byte) 0x45, (byte) 0};

            write(cmd);
            write(arryBeginText);
            writeLineBreak(2);
            write(terminalNo);
            writeLineBreak(1);
            write(receiptNo);
            writeLineBreak(1);
            write(date);
            writeLineBreak(1);
            write(memberName);
            writeLineBreak(1);
            write(transactionTYpe);
            // print line break
            writeLineBreak(2);
            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            // print line break
            writeLineBreak(2);
            // print text
//            write(arryAccType);
            write(arryAmt);
            writeLineBreak(2);
            write(arryAccType);
            writeLineBreak(1);
            write(arryAccNo);
            writeLineBreak(2);
            write(signature);
            writeLineBreak(2);
            write(servedBy);
            writeLineBreak(2);
            write(arryMotto);
            writeLineBreak(4);
            PrinterInterface.end();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void write(final byte[] arryData) {
        int result = PrinterInterface.write(arryData, arryData.length);
    }

    // print line break
    private void writeLineBreak(int lineNumber) {
        // write(new byte[]{'\n'});
        write(PrinterCommand.getCmdEscDN(lineNumber));
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.move_left_in_activity, R.anim.move_right_out_activity);
    }

    @Override
    protected void onResume() {
        SharedPrefs.init(getApplicationContext());
        super.onResume();
        String fpBoolean = SharedPrefs.read(SharedPrefs.FP_BOOLEAN, null);

        if (fpBoolean.equals("true")) {
            requestBalanceForBioAuth(phone_number, accountNo);
        } else if (fpBoolean.equals("false")) {
            //progressDialog.dismiss();
            toast.swarn("For biometrics Authentication, Customer's fingerprints shall be required!");
        }
    }
}
