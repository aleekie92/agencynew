package com.coretec.agencyApplication.activities.gfl;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.cloudpos.jniinterface.PrinterInterface;
import com.coretec.agencyApplication.R;
import com.coretec.agencyApplication.activities.BaseActivity;
import com.coretec.agencyApplication.activities.newmodules.ValidateTunzhengbigBio;
import com.coretec.agencyApplication.api.GFL;
import com.coretec.agencyApplication.api.requests.FingerPrintRequest;
import com.coretec.agencyApplication.api.requests.GetGrowerMemberBalanceRequest;
import com.coretec.agencyApplication.api.requests.GrowersDetailsRequest;
import com.coretec.agencyApplication.api.responses.FingerPrintResponse;
import com.coretec.agencyApplication.api.responses.GetGrowerMemberBalanceResponse;
import com.coretec.agencyApplication.api.responses.GrowersDetailsResponse;
import com.coretec.agencyApplication.model.GrowerDetails;
import com.coretec.agencyApplication.utils.QuickToast;
import com.coretec.agencyApplication.utils.SharedPrefs;
import com.coretec.agencyApplication.wizarpos.function.printer.PrinterCommand;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static com.coretec.agencyApplication.utils.Utils.getFormattedDate;
import static com.coretec.agencyApplication.utils.Utils.getLat;
import static com.coretec.agencyApplication.utils.Utils.getLong;

public class BalanceInquiry extends AppCompatActivity {

    private QuickToast toast = new QuickToast(this);
    ImageView backBtn;
    private TextInputLayout input_layout_id_number;
    private EditText identifier_deposit;
    private Spinner spinner;
    private ArrayList<String> arrayList = new ArrayList<>();
    private ArrayAdapter<GrowerDetails> adapter;

    private String identifiercode = "";
    private String growerNo = "";
    String text;

    private String item;
    RelativeLayout loading;
    ProgressBar progressBar;
    ProgressDialog progressDialog;
    Button submit;
    boolean doubleBackToExitPressedOnce = false;
    private String clientName, custcode, loanno, loantype, totalamt,loantypename,interestBalance,loanBalance;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.gfl_balance_inquiry);
        SharedPrefs.init(getApplicationContext());

        SharedPrefs.write(SharedPrefs.FP_BOOLEAN, "");
        SharedPrefs.write(SharedPrefs.BAL_CLIENT_NAME, "");

        Toolbar toolbar = findViewById(R.id.toolbar);
        TextView mTitle = toolbar.findViewById(R.id.toolbar_title);
        mTitle.setText("Balance Inquiry");
        setSupportActionBar(toolbar);
        ActionBar actionbar = getSupportActionBar();
        actionbar.setDisplayHomeAsUpEnabled(true);
        actionbar.setHomeAsUpIndicator(R.drawable.ic_arrow_back);


        input_layout_id_number = findViewById(R.id.input_layout_id_number);
        identifier_deposit = findViewById(R.id.id_number);
        loading = findViewById(R.id.relativeLoading);

        identifier_deposit.setInputType(InputType.TYPE_NULL);
        identifier_deposit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // showMyDialog();
            }
        });

        identifier_deposit.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    // showMyDialog();
                }
            }
        });

        submit = findViewById(R.id.btnSubmit);
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                clientName = SharedPrefs.read(SharedPrefs.BAL_CLIENT_NAME, null);
                custcode = SharedPrefs.read(SharedPrefs.CUST_CODE, null);
                loanno = SharedPrefs.read(SharedPrefs.LOAN_NO, null);
                loantype = SharedPrefs.read(SharedPrefs.LOAN_TYPE, null);
                totalamt = SharedPrefs.read(SharedPrefs.TOTAL_BAL, null);
                loantypename = SharedPrefs.read(SharedPrefs.LOAN_TYPE_NAME, null);

                interestBalance= SharedPrefs.read(SharedPrefs.INT_BAL, null);
                loanBalance= SharedPrefs.read(SharedPrefs.LOAN_BAL, null);
                //String clientName ="Alexander";

                if (identifier_deposit.getText().toString().matches("")) {
                    toast.swarn("Please fill in the missing fields to proceed!");
                } /*else if (clientName.isEmpty()) {
                    toast.swarn("Could not get balance please retry again!");
                }*/
                else{
                        /* progress dialog */
                        progressDialog = new ProgressDialog(BalanceInquiry.this);
                        progressDialog.setMessage("Please wait...");
                        progressDialog.setCancelable(false);
                        progressDialog.show();
                        requestFingerPrints();

                }
            }
        });

        spinner = findViewById(R.id.growerSpinner);

        progressBar = findViewById(R.id.growersPb);

        TextWatcher textWatcherId = new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                //here, after we introduced something in the EditText we get the string from it
                String identifier_number = identifier_deposit.getText().toString();
                if (identifier_deposit.length() >= 7) {
                    requestGetAccountDetails(identifier_number);
                }
                if (identifier_deposit.length() <= 5) {
                    progressBar.setVisibility(View.GONE);
                    arrayList.clear();
                    spinner = findViewById(R.id.growerSpinner);
                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(BalanceInquiry.this, android.R.layout.simple_spinner_item, arrayList);
                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    spinner.setAdapter(null);
                }
                /*if (identifier_deposit.length() >= 7 && identifier_deposit.length() <= 8) {
                    getHeaderDetails();
                }*/
                if (identifier_deposit.length() >= 5 && identifier_deposit.length() <= 8) {
                    arrayList.clear();
                    spinner = findViewById(R.id.growerSpinner);
                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(BalanceInquiry.this, android.R.layout.simple_spinner_item, arrayList);
                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    spinner.setAdapter(null);
                    progressBar.setVisibility(View.VISIBLE);
                    progressBar.getIndeterminateDrawable().setColorFilter(ContextCompat.getColor(BalanceInquiry.this, R.color.btn_green), PorterDuff.Mode.SRC_IN );
                }
//                Toast.makeText(ShareDeposit.this, accountNumber, Toast.LENGTH_SHORT).show();
//                requestAccountName(accountNumber);

            }
        };

        adapter = new ArrayAdapter<GrowerDetails>(getApplicationContext(),
                R.layout.simple_spinner_dropdown_item, new ArrayList<GrowerDetails>());
        adapter.setDropDownViewResource(R.layout.simple_spinner_dropdown_item);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                item = parent.getItemAtPosition(position).toString();
                text = spinner.getSelectedItem().toString();

                if (!text.isEmpty()) {
                    loading.setVisibility(View.VISIBLE);
                    getHeaderDetails(text);
                }
                //SharedPrefs.write(SharedPrefs.BALANCE_G_NUMBER, text);
                Log.e("GROWER NUMBER", text);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        identifier_deposit.addTextChangedListener(textWatcherId);

    }

    void requestFingerPrints() {

        final String TAG = "GET FINGERPRINTS";
        String URL = GFL.MSACCO_AGENT + GFL.GetFourFingerPrint;

        final FingerPrintRequest fingerPrintRequest = new FingerPrintRequest();
        fingerPrintRequest.accountidentifier = identifier_deposit.getText().toString();
        fingerPrintRequest.accountidentifiercode = "1";
        fingerPrintRequest.agentid = SharedPrefs.read(SharedPrefs.AGENT_ID_1, null);
        fingerPrintRequest.terminalid = MainGFLDashboard.imei;//SharedPrefs.read(SharedPrefs.DEVICE_ID, null);
        fingerPrintRequest.longitude = getLong(this);
        fingerPrintRequest.latitude = getLat(this);
        fingerPrintRequest.date = getFormattedDate();

        Log.e(TAG, fingerPrintRequest.getBody().toString());

        GFL.instance(this).request(URL, fingerPrintRequest, new GFL.RequestListener() {
            @Override
            public void onSuccess(String response) {
                FingerPrintResponse fingerPrintResponse = GFL.instance(BalanceInquiry.this)
                        .mGson.fromJson(response, FingerPrintResponse.class);
                if (fingerPrintResponse.is_successful) {
                    try {
                        JSONObject jsonObject = new JSONObject(response);

                        String leftRing = jsonObject.getString("leftring");
                        String leftLittle = jsonObject.getString("leftlittle");
                        String rightRing = jsonObject.getString("rightring");
                        String rightLittle = jsonObject.getString("rightlittle");

                        SharedPrefs.write(SharedPrefs.LEFT_RING, leftRing);
                        SharedPrefs.write(SharedPrefs.LEFT_LITTLE, leftLittle);
                        SharedPrefs.write(SharedPrefs.RIGHT_RING, rightRing);
                        SharedPrefs.write(SharedPrefs.RIGHT_LITTLE, rightLittle);


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    SharedPrefs.transactionstypelogs(2, BalanceInquiry.this);
                    SharedPrefs.growerslogs(text, BalanceInquiry.this);
                    startActivity(new Intent(BalanceInquiry.this, ValidateTunzhengbigBio.class));
                }
                else {
                    progressDialog.dismiss();
                    toast.swarn("Could not get customer finger prints!");
                }


            }

            @Override
            public void onTokenExpired() {

            }

            @Override
            public void onError(String error) {

            }
        });

    }

    void getHeaderDetails(String gNumber) {

        final String TAG = "Get Growers Name";
//        EasyLocationMod easyLocationMod = new EasyLocationMod(CashWithdrawal.this);

        String URL = GFL.MSACCO_AGENT + GFL.GetGrowerMemberBalance;
        //String gNumber = SharedPrefs.read(SharedPrefs.BALANCE_G_NUMBER, null);

        final GetGrowerMemberBalanceRequest balanceRequest = new GetGrowerMemberBalanceRequest();
        balanceRequest.corporateno = "CAP016";
        balanceRequest.accountidentifier = gNumber;
        balanceRequest.accountidentifiercode = "2";
        balanceRequest.transactiontype = "Balance Inquiry";
        balanceRequest.agentid = SharedPrefs.read(SharedPrefs.AGENT_ID_1, null);
        balanceRequest.terminalid = SharedPrefs.read(SharedPrefs.DEVICE_ID, null);
        balanceRequest.longitude = getLong(this);
        balanceRequest.latitude = getLat(this);
        balanceRequest.date = getFormattedDate();

        Log.e(TAG, balanceRequest.getBody().toString());

        GFL.instance(this).request(URL, balanceRequest, new GFL.RequestListener() {
            @Override
            public void onSuccess(String response) {
                loading.setVisibility(View.GONE);

                GetGrowerMemberBalanceResponse balanceResponse = GFL.instance(BalanceInquiry.this)
                        .mGson.fromJson(response, GetGrowerMemberBalanceResponse.class);
                if (balanceResponse.is_successful) {
                    //List<GetLoansCalculatorList> loansCalculatorLists = new ArrayList<>();

                    try {
                        JSONObject jsonObject = new JSONObject(response);

                        JSONArray jsonArray = jsonObject.getJSONArray("getgrowermemberbalance");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                            JSONArray jsonArray1 = jsonObject1.getJSONArray("GrowerMemberBalance");
                            for (int l = 0; l < jsonArray1.length(); l++) {
                                JSONObject jsonObject2 = jsonArray1.getJSONObject(l);


                                //String gNo = jsonObject2.getString("GrowerNo");
                                clientName = jsonObject2.getString("ClientName");
                                //Json responses for the string
                                custcode = jsonObject2.getString("ClientCode");
                                loanno = jsonObject2.getString("LoanNo");
                                loantype = jsonObject2.getString("LoanType");
                                loantypename = jsonObject2.getString("LoanTypeName");
                                interestBalance = jsonObject2.getString("InterestBalance");
                                loanBalance = jsonObject2.getString("LoanBalance");

                                totalamt = jsonObject2.getString("TotalBalance");

                                SharedPrefs.write(SharedPrefs.BAL_CLIENT_NAME, clientName);
                                SharedPrefs.write(SharedPrefs.CUST_CODE, custcode);
                                SharedPrefs.write(SharedPrefs.LOAN_TYPE, loantype);
                                SharedPrefs.write(SharedPrefs.LOAN_TYPE_NAME, loantypename);
                                SharedPrefs.write(SharedPrefs.LOAN_NO, loanno);
                                SharedPrefs.write(SharedPrefs.INT_BAL, interestBalance);
                                SharedPrefs.write(SharedPrefs.LOAN_BAL, loanBalance);
                                SharedPrefs.write(SharedPrefs.TOTAL_BAL, totalamt);


                            }

                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                } else {
//                    Toast.makeText(ShareDeposit.this, "Accounts Fetching Failed", Toast.LENGTH_LONG).show();
                }

            }

            @Override
            public void onTokenExpired() {

            }

            @Override
            public void onError(String error) {

            }
        });
    }

    private void submitBalanceInquiry() {

        final String TAG = "Get Growers Name";
        String URL = GFL.MSACCO_AGENT + GFL.GetGrowerMemberBalance;
        String gNumber = SharedPrefs.read(SharedPrefs.BALANCE_G_NUMBER, null);
        final GetGrowerMemberBalanceRequest balanceRequest = new GetGrowerMemberBalanceRequest();
        balanceRequest.corporateno = "CAP016";
        balanceRequest.accountidentifier = gNumber;
        balanceRequest.accountidentifiercode = "2";
        balanceRequest.transactiontype = "Balance Inquiry";
        balanceRequest.agentid = SharedPrefs.read(SharedPrefs.AGENT_ID_1, null);
        balanceRequest.terminalid = SharedPrefs.read(SharedPrefs.DEVICE_ID, null);
        balanceRequest.longitude = getLong(this);
        balanceRequest.latitude = getLat(this);
        balanceRequest.date = getFormattedDate();

        Log.e(TAG, balanceRequest.getBody().toString());

        GFL.instance(this).request(URL, balanceRequest, new GFL.RequestListener() {
            @Override
            public void onSuccess(String response) {
                loading.setVisibility(View.GONE);

                GetGrowerMemberBalanceResponse balanceResponse = GFL.instance(BalanceInquiry.this)
                        .mGson.fromJson(response, GetGrowerMemberBalanceResponse.class);
                if (balanceResponse.is_successful) {
                    //List<GetLoansCalculatorList> loansCalculatorLists = new ArrayList<>();

                    try {
                        JSONObject jsonObject = new JSONObject(response);

                        JSONArray jsonArray = jsonObject.getJSONArray("getgrowermemberbalance");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                            JSONArray jsonArray1 = jsonObject1.getJSONArray("GrowerMemberBalance");
                            for (int l = 0; l < jsonArray1.length(); l++) {
                                JSONObject jsonObject2 = jsonArray1.getJSONObject(l);
                                //progressDialog.dismiss();

                                //String gNo = jsonObject2.getString("GrowerNo");
                                String clientNamee = jsonObject2.getString("ClientName");
                                //Json responses for the string
                                String clientCodee = jsonObject2.getString("ClientCode");
                                String loanNoo = jsonObject2.getString("LoanNo");
                                String loanTypee = jsonObject2.getString("LoanType");
                                String loanTypeNamee = jsonObject2.getString("LoanTypeName");
                                String interestBalancee = jsonObject2.getString("InterestBalance");
                                String loanBalancee = jsonObject2.getString("LoanBalance");


                                //SharedPrefs.write(SharedPrefs.LOAN_F_N, fullNames);
                                //SharedPrefs.write(SharedPrefs.LOAN_ID, idNos);


                                int result = PrinterInterface.open();
                                writetest(custcode, loantype, totalamt,loantypename, loanBalance);
                                PrinterInterface.close();
                            }

                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    String agentName = SharedPrefs.read(SharedPrefs.AGENT_NAME, null);
                    int result = PrinterInterface.open();
                    footer(agentName);
                    PrinterInterface.close();

                } else {
//                    Toast.makeText(ShareDeposit.this, "Accounts Fetching Failed", Toast.LENGTH_LONG).show();
                }

            }

            @Override
            public void onTokenExpired() {

            }

            @Override
            public void onError(String error) {

            }
        });
    }

    public void header(String growerNumber){

        //String middle = idNos.substring(2, 6);
       // String first = growerNumber.substring(0, 2);
        //String last = growerNumber.substring(6, growerNumber.length());
       // String maskedG = first + "****"+last;
        try {
            byte[] start = null;
            byte[] saccoName = null;
            byte[] type = null;
           //byte[] gno = null;

            try {
                start = String.valueOf("              ").getBytes("GB2312");
                saccoName = String.valueOf("      GREENLAND FEDHA LTD     ").getBytes("GB2312");
                type = String.valueOf("     BALANCE ENQUIRY     ").getBytes("GB2312");
               // gno = String.valueOf("Grower Number : " + String.valueOf(SharedPrefs.read(SharedPrefs.BALANCE_G_NUMBER, null))).getBytes("GB2312");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            PrinterInterface.begin();
            byte[] cmd;
            cmd = new byte[]{(byte) 0x1B, (byte) 0x40};

            cmd = new byte[]{
                    (byte) 0x1B, (byte) 0x45, (byte) 0};

            write(cmd);
            write(start);
            writeLineBreak(1);
            write(saccoName);
            writeLineBreak(2);
            write(type);
            writeLineBreak(1);
           /* write(gno);
            writeLineBreak(1);*/
            PrinterInterface.end();

        }catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void footer(String aName){
        try {
            byte[] time = null;
            byte[] nameAgent = null;
            byte[] motto = null;

            SimpleDateFormat timeStampFormat = new SimpleDateFormat("yyyy/MM/dd - HH:mm");
            Date myDate = new Date();
            String timeCurrent = timeStampFormat.format(myDate);

            try {
                time = String.valueOf("   Date : " + String.valueOf(timeCurrent)).getBytes("GB2312");
                nameAgent = String.valueOf("You were served by " + String.valueOf(aName)).getBytes("GB2312");
                motto = String.valueOf("        Customer Focus       ").getBytes("GB2312");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            PrinterInterface.begin();

            byte[] cmd;
            cmd = new byte[]{(byte) 0x1B, (byte) 0x40};

            cmd = new byte[]{
                    (byte) 0x1B, (byte) 0x45, (byte) 0};

            write(cmd);

            write(cmd);
            writeLineBreak(1);
            write(time);
            writeLineBreak(2);
            write(nameAgent);
            writeLineBreak(1);
            write(motto);
            // print line break
            writeLineBreak(4);
            writeLineBreak(5);
            PrinterInterface.end();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void writetest(String customercode, String loantypee,String totalbalancee, String loantypenamee,
                          String loanbalancee){
        try {
            byte[] custcode = null;
            byte[] ltype = null;
            byte[] ltotal = null;
            byte[] ltypename = null;
            byte[] lbal = null;


            try {
                custcode= String.valueOf("Grower Number : " + String.valueOf(customercode)).getBytes("GB2312");
                ltype = String.valueOf("Loan Type : " + String.valueOf(loantypee)).getBytes("GB2312");
                ltotal = String.valueOf("Total : " + String.valueOf(totalbalancee)).getBytes("GB2312");
                ltypename = String.valueOf("Total : " + String.valueOf(loantypenamee)).getBytes("GB2312");
                lbal = String.valueOf("Total : " + String.valueOf(loanbalancee)).getBytes("GB2312");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            PrinterInterface.begin();

            byte[] cmd;
            cmd = new byte[]{(byte) 0x1B, (byte) 0x40};

            cmd = new byte[]{
                    (byte) 0x1B, (byte) 0x45, (byte) 0};

            write(cmd);
            //write(titles);
            //writeLineBreak(1);

            write(custcode);
            writeLineBreak(1);
            write(ltype);
            writeLineBreak(2);
            write(ltotal);
            // print line break
            writeLineBreak(3);

            write(ltypename);
            // print line break
            writeLineBreak(4);
            write(lbal);
            // print line break
            writeLineBreak(5);

            PrinterInterface.end();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void write(final byte[] arryData) {
        int result = PrinterInterface.write(arryData, arryData.length);
    }

    // print line break
    private void writeLineBreak(int lineNumber) {
        // write(new byte[]{'\n'});
        write(PrinterCommand.getCmdEscDN(lineNumber));
    }

    void requestGetAccountDetails(String identifier) {

        final String TAG = "GrowersAccounts";

        String URL = GFL.MSACCO_AGENT + GFL.GetGrowersDetails;

        final GrowersDetailsRequest growersDetails = new GrowersDetailsRequest();
        growersDetails.agentid = SharedPrefs.read(SharedPrefs.AGENT_ID_1, null);
        growersDetails.corporateno = "CAP016";
        growersDetails.accountidentifier = identifier;
        growersDetails.accountidentifiercode = "1";
        growersDetails.transactiontype = "1";
        growersDetails.terminalid = SharedPrefs.read(SharedPrefs.DEVICE_ID, null);
        growersDetails.longitude = getLong(this);
        growersDetails.latitude = getLat(this);
        growersDetails.date = getFormattedDate();

        Log.e(TAG, growersDetails.getBody().toString());

        arrayList.clear();
        spinner = findViewById(R.id.growerSpinner);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(BalanceInquiry.this, android.R.layout.simple_spinner_item, arrayList);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(null);

        GFL.instance(this).request(URL, growersDetails, new GFL.RequestListener() {
            @Override
            public void onSuccess(String response) {
                /*deposit_progressBar.setVisibility(View.GONE);*/

                GrowersDetailsResponse growersDetailsResponse = GFL.instance(BalanceInquiry.this)
                        .mGson.fromJson(response, GrowersDetailsResponse.class);
                if (growersDetailsResponse.is_successful) {
                    List<GrowerDetails> growerDetailsList = new ArrayList<>();

                    try {
                        JSONObject jsonObject = new JSONObject(response);

                        JSONArray jsonArray = jsonObject.getJSONArray("Growers");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                            JSONArray jsonArray1 = jsonObject1.getJSONArray("GrowerDetails");
                            for (int l = 0; l < jsonArray1.length(); l++) {
                                JSONObject jsonObject2 = jsonArray1.getJSONObject(l);
                                progressBar.setVisibility(View.GONE);
                                arrayList.add(String.valueOf(jsonObject2.getString("GrowerNo")));
                                String gNo = jsonObject2.getString("GrowerNo");
                                String isDefaulter = jsonObject2.getString("LoanDefaulter");

                                SharedPrefs.write(SharedPrefs.IS_DEFAULTER, isDefaulter);
                                Log.e("Growers NUMBER:", gNo);
                            }

                        }

                        //Growers Number
                        spinner = findViewById(R.id.growerSpinner);
                        ArrayAdapter<String> adapter = new ArrayAdapter<String>(BalanceInquiry.this, android.R.layout.simple_spinner_item, arrayList);
                        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                        spinner.setAdapter(adapter);


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                } else {
//                    Toast.makeText(ShareDeposit.this, "Accounts Fetching Failed", Toast.LENGTH_LONG).show();
                }

            }

            @Override
            public void onTokenExpired() {

            }

            @Override
            public void onError(String error) {

            }
        });
    }

    @Override
    protected void onResume() {
        SharedPrefs.init(getApplicationContext());
        super.onResume();
        String fpBoolean = SharedPrefs.read(SharedPrefs.FP_BOOLEAN, null);

        if (fpBoolean.equals("true")) {
            progressDialog.dismiss();
            int result = PrinterInterface.open();
            header(SharedPrefs.read(SharedPrefs.BALANCE_G_NUMBER, null));
            PrinterInterface.close();
            //getHeaderDetails();
            submitBalanceInquiry();
        } else if(fpBoolean.equals("false")){
            progressDialog.dismiss();
            toast.swarn( "Please authenticate to complete transaction!");
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                finish();
                //Toast.makeText(Account.this, "Back has been presses!", Toast.LENGTH_LONG).show();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        overridePendingTransition(R.anim.move_left_in_activity, R.anim.move_right_out_activity);
    }
}
