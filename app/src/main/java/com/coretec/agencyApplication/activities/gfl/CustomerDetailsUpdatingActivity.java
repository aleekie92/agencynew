package com.coretec.agencyApplication.activities.gfl;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import com.coretec.agencyApplication.R;
import com.coretec.agencyApplication.activities.BaseActivity;
import com.coretec.agencyApplication.adapters.CustomerDetailsUpdatingAdapter;
import com.coretec.agencyApplication.adapters.StepperAdapter;
import com.coretec.agencyApplication.utils.QuickToast;
import com.stepstone.stepper.StepperLayout;
import com.stepstone.stepper.VerificationError;

public class CustomerDetailsUpdatingActivity extends BaseActivity implements StepperLayout.StepperListener{

    private QuickToast toast = new QuickToast(this);
    private StepperLayout mStepperLayout;
    private CustomerDetailsUpdatingAdapter mStepperAdapter;
    boolean doubleBackToExitPressedOnce = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customer_details_updating);
        this.overridePendingTransition(R.anim.move_right_in_activity, R.anim.move_left_out_activity);

        mStepperLayout = findViewById(R.id.stepperLayout);
        mStepperAdapter = new CustomerDetailsUpdatingAdapter(getSupportFragmentManager(), this);
        mStepperLayout.setAdapter(mStepperAdapter);
        mStepperLayout.setListener(this);
    }

    @Override
    public void onCompleted(View completeButton) {

    }

    @Override
    public void onError(VerificationError verificationError) {

    }

    @Override
    public void onStepSelected(int newStepPosition) {

    }

    @Override
    public void onReturn() {

    }

    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            String msg = "Are you sure you want to exit Updating member?";
            AlertDialog.Builder builder1 = new AlertDialog.Builder(CustomerDetailsUpdatingActivity.this);
            builder1.setMessage(msg);
            builder1.setTitle("Alert!");
            builder1.setCancelable(false);

            builder1.setPositiveButton(
                    "yes",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                            //startActivity(new Intent(CustomerRegistration.this, Dashboard.class));
                            finish();
                            //onBackPressed();
                        }
                    });

            builder1.setNegativeButton(
                    "CANCEL",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                            //onBackPressed();
                        }
                    });

            AlertDialog alert11 = builder1.create();
            alert11.show();
            //System.exit(1);
            //Toast.makeText(this, "Are you sure you want to exit Member Registration", Toast.LENGTH_LONG).show();
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        toast.sinfo("Press back again to exit member registration");

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce=false;
            }
        }, 2000);
        //super.onBackPressed();
        //overridePendingTransition(R.anim.move_left_in_activity, R.anim.move_right_out_activity);
    }
}
