package com.coretec.agencyApplication.activities.newmodules;

import android.os.Bundle;

import com.coretec.agencyApplication.R;
import com.coretec.agencyApplication.activities.BaseActivity;
import com.coretec.agencyApplication.utils.QuickToast;
import com.coretec.agencyApplication.utils.SharedPrefs;

public class LoanStatusActivity extends BaseActivity {
    private QuickToast toast = new QuickToast(this);


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        SharedPrefs.init(getApplicationContext());
        setContentView(R.layout.activity_loan_status_check);

        this.overridePendingTransition(R.anim.move_right_in_activity, R.anim.move_left_out_activity);
    }
}
