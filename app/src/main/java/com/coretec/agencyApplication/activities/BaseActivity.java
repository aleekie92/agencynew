package com.coretec.agencyApplication.activities;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.Location;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.coretec.agencyApplication.R;
import com.coretec.agencyApplication.activities.newmodules.MainDashboardActivity;
import com.coretec.agencyApplication.activities.newmodules.ValidateCrossmatchBio;
import com.coretec.agencyApplication.activities.newmodules.ValidateTunzhengbigBio;
import com.coretec.agencyApplication.api.Api;
import com.coretec.agencyApplication.api.requests.FingerPrintRequest;
import com.coretec.agencyApplication.api.responses.FingerPrintResponse;
import com.coretec.agencyApplication.utils.QuickToast;
import com.coretec.agencyApplication.utils.SharedPrefs;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.Result;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsStatusCodes;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import static com.coretec.agencyApplication.utils.Utils.getFormattedDate;
import static com.coretec.agencyApplication.utils.Utils.getLat;
import static com.coretec.agencyApplication.utils.Utils.getLong;

public class BaseActivity extends AppCompatActivity implements GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        LocationListener{

    private Location mylocation;
    private GoogleApiClient googleApiClient;
    private final static int REQUEST_CHECK_SETTINGS_GPS = 0x1;
    private final static int REQUEST_ID_MULTIPLE_PERMISSIONS = 0x2;

    static Context cxt;
    private static QuickToast toast = new QuickToast(cxt);
    public static  String appName;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        themes(BaseActivity.this);
        setTheme(SharedPrefs.read(String.valueOf(SharedPrefs.VR_THEME), 0));
    }

    public static void themes(Context context){
        SharedPreferences prefs = context.getSharedPreferences("MyNamePrefs", MODE_PRIVATE);
        switch (Api.MSACCO_AGENT) {
            case "http://172.16.11.145:3052/api/AgencyBanking/":
                SharedPrefs.write(String.valueOf(SharedPrefs.VR_THEME), R.style.AppThemeGfl);
                SharedPreferences.Editor editor = prefs.edit();
                editor.putString("app_name", "PESA ULIPO");
                editor.apply();
                appName = prefs.getString("app_name", null);
                break;

            case "http://172.16.11.145:3051/api/AgencyBanking/":
                SharedPrefs.write(String.valueOf(SharedPrefs.VR_THEME), R.style.AppThemeGfl);
                SharedPreferences.Editor editortest = prefs.edit();
                editortest.putString("app_name", "PESA ULIPO");
                editortest.apply();
                appName = prefs.getString("app_name", null);
                break;

            case "http://172.18.6.8:35088/api/AgencyBanking/":

                String coopnumber = SharedPrefs.read(SharedPrefs.VR_COOPORATE_NO, null);
                if (coopnumber !=null){
                    switch (SharedPrefs.read(SharedPrefs.VR_COOPORATE_NO, null)) {
                        case "CAP005":
                            SharedPrefs.write(String.valueOf(SharedPrefs.VR_THEME), R.style.AppThemeCoopmis);
                            SharedPreferences.Editor coopm1 = prefs.edit();
                            coopm1.putString("app_name", "CoopMIS");
                            coopm1.apply();
                            appName = prefs.getString("app_name", null);
                            SharedPrefs.write(SharedPrefs.VR_APP_NAME, appName);
                            break;

                        case "CAP022":
                            SharedPrefs.write(String.valueOf(SharedPrefs.VR_THEME), R.style.AppThemeCoopmis);
                            SharedPreferences.Editor coopm2 = prefs.edit();
                            coopm2.putString("app_name", "CoopMIS");
                            coopm2.apply();
                            appName = prefs.getString("app_name", null);
                            SharedPrefs.write(SharedPrefs.VR_APP_NAME, appName);
                            break;

                        case "CAP027":
                            SharedPrefs.write(String.valueOf(SharedPrefs.VR_THEME), R.style.AppThemeCoopmis);
                            SharedPreferences.Editor coopm3 = prefs.edit();
                            coopm3.putString("app_name", "CoopMIS");
                            coopm3.apply();
                            appName = prefs.getString("app_name", null);
                            SharedPrefs.write(SharedPrefs.VR_APP_NAME, appName);
                            break;

                        case "CAP028":
                            SharedPrefs.write(String.valueOf(SharedPrefs.VR_THEME), R.style.AppThemeCoopmis);
                            SharedPreferences.Editor coopm4 = prefs.edit();
                            coopm4.putString("app_name", "CoopMIS");
                            coopm4.apply();
                            appName = prefs.getString("app_name", null);
                            SharedPrefs.write(SharedPrefs.VR_APP_NAME, appName);
                            break;

                        default:
                            SharedPrefs.write(String.valueOf(SharedPrefs.VR_THEME), R.style.AppTheme);

                            SharedPreferences.Editor majority = prefs.edit();
                            majority.putString("app_name", "AGENCY BANKING");
                            majority.apply();
                            appName = prefs.getString("app_name", null);
                            SharedPrefs.write(SharedPrefs.VR_APP_NAME, appName);
                            break;
                    }

                }else {
                    SharedPrefs.write(String.valueOf(SharedPrefs.VR_THEME), R.style.AppTheme);
                    SharedPreferences.Editor majority = prefs.edit();
                    majority.putString("app_name", "AGENCY BANKING");
                    majority.apply();
                    appName = prefs.getString("app_name", null);
                    SharedPrefs.write(SharedPrefs.VR_APP_NAME, appName);
                }
                break;

            default:
                SharedPrefs.write(String.valueOf(SharedPrefs.VR_THEME), R.style.AppTheme);
                SharedPreferences.Editor defaultt = prefs.edit();
                defaultt.putString("app_name", "AGENCY BANKING");
                defaultt.apply();
                appName = prefs.getString("app_name", null);
                SharedPrefs.write(SharedPrefs.VR_APP_NAME, appName);
                break;
        }
    }


    @Override
    public void onConnected(@Nullable Bundle bundle) {
        checkPermissions();
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    public void onLocationChanged(Location location) {
        mylocation = location;
        if (mylocation != null) {
            Double latitude = mylocation.getLatitude();
            Double longitude = mylocation.getLongitude();

        }
    }

    public synchronized void setUpGClient() {
        googleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this, 0, this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        googleApiClient.connect();
    }

    private void checkPermissions() {
        int permissionLocation = ContextCompat.checkSelfPermission(BaseActivity.this,
                android.Manifest.permission.ACCESS_FINE_LOCATION);
        List<String> listPermissionsNeeded = new ArrayList<>();
        if (permissionLocation != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(android.Manifest.permission.ACCESS_FINE_LOCATION);
            if (!listPermissionsNeeded.isEmpty()) {
                ActivityCompat.requestPermissions(this,
                        listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), REQUEST_ID_MULTIPLE_PERMISSIONS);
            }
        } else {
            getMyLocation();
        }

    }

    private void getMyLocation() {
        if (googleApiClient != null) {
            if (googleApiClient.isConnected()) {
                int permissionLocation = ContextCompat.checkSelfPermission(BaseActivity.this,
                        Manifest.permission.ACCESS_FINE_LOCATION);
                if (permissionLocation == PackageManager.PERMISSION_GRANTED) {
                    mylocation = LocationServices.FusedLocationApi.getLastLocation(googleApiClient);
                    LocationRequest locationRequest = new LocationRequest();
                    locationRequest.setInterval(3000);
                    locationRequest.setFastestInterval(3000);
                    locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
                    LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                            .addLocationRequest(locationRequest);
                    builder.setAlwaysShow(true);
                    LocationServices.FusedLocationApi
                            .requestLocationUpdates(googleApiClient, locationRequest, this);
                    PendingResult result =
                            LocationServices.SettingsApi
                                    .checkLocationSettings(googleApiClient, builder.build());
                    result.setResultCallback(new ResultCallback() {
                        @Override
                        public void onResult(@NonNull Result result) {
                            final Status status = result.getStatus();
                            switch (status.getStatusCode()) {
                                case LocationSettingsStatusCodes.SUCCESS:

                                    int permissionLocation = ContextCompat
                                            .checkSelfPermission(BaseActivity.this,
                                                    Manifest.permission.ACCESS_FINE_LOCATION);
                                    if (permissionLocation == PackageManager.PERMISSION_GRANTED) {
                                        mylocation = LocationServices.FusedLocationApi
                                                .getLastLocation(googleApiClient);
                                    }
                                    break;
                                case LocationSettingsStatusCodes.RESOLUTION_REQUIRED:

                                    try {

                                        status.startResolutionForResult(BaseActivity.this,
                                                REQUEST_CHECK_SETTINGS_GPS);
                                    } catch (IntentSender.SendIntentException e) {

                                    }
                                    break;
                                case LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE:

                                    break;
                            }
                        }

                    });
                }
            }
        }
    }

   /* @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case REQUEST_CHECK_SETTINGS_GPS:
                switch (resultCode) {
                    case Activity.RESULT_OK:
                        getMyLocation();
                        break;
                    case Activity.RESULT_CANCELED:
                        finish();
                        break;
                }
                break;
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        int permissionLocation = ContextCompat.checkSelfPermission(BaseActivity.this,
                Manifest.permission.ACCESS_FINE_LOCATION);
        if (permissionLocation == PackageManager.PERMISSION_GRANTED) {
            getMyLocation();
        }
    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }


    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    public void onUserInteraction() {
        super.onUserInteraction();
    }

    @Override
    protected void onPause() {
        super.onPause();
        //LogOutTimerUtil.startLogoutTimer(this, this);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        return super.onTouchEvent(event);
    }

    @Override
    protected void onUserLeaveHint() {
        super.onUserLeaveHint();
        LogOutTimerUtil.startLogoutTimer(this, this);
        //System.exit(0);
    }



    @Override
    protected void onStop() {
        super.onStop();
        LogOutTimerUtil.startLogoutTimer(this, this);
    }

    @Override
    public void doLogout() {
         finish();
       *//* runOnUiThread(new Runnable() {
            public void run() {
                String msg = "Session has expired";
                AlertDialog.Builder builder1 = new AlertDialog.Builder(BaseActivity.this);
                builder1.setMessage(msg);
                builder1.setTitle("Alert!");
                builder1.setCancelable(false);

                builder1.setPositiveButton(
                        "OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                                finish();
                            }
                        });
            }
        });*//*
    }*/

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        int permissionLocation = ContextCompat.checkSelfPermission(BaseActivity.this,
                Manifest.permission.ACCESS_FINE_LOCATION);
        if (permissionLocation == PackageManager.PERMISSION_GRANTED) {
            getMyLocation();
        }
    }

    public static void requestFingerPrints(final Activity activity, String id, final ProgressBar pb) {
        final String module = SharedPrefs.read(SharedPrefs.FINGERMODULE, null);
        final String TAG = "GET FINGERPRINTS";

        String URL = Api.MSACCO_AGENT + Api.GetFingerPrint;
        final FingerPrintRequest fingerPrintRequest = new FingerPrintRequest();
        fingerPrintRequest.accountidentifier = id;//withdrawal_identifier.getText().toString();
        fingerPrintRequest.accountidentifiercode = "1";
        fingerPrintRequest.agentid = SharedPrefs.read(SharedPrefs.AGENT_ID_1, null);
        fingerPrintRequest.terminalid = MainDashboardActivity.imei;
        fingerPrintRequest.longitude = getLong(activity);
        fingerPrintRequest.latitude = getLat(activity);
        fingerPrintRequest.date = getFormattedDate();

        Log.e(TAG, fingerPrintRequest.getBody().toString());
        Api.instance(activity).request(URL, fingerPrintRequest, new Api.RequestListener() {
            @Override
            public void onSuccess(String response) {

                FingerPrintResponse fingerPrintResponse = Api.instance(activity)
                        .mGson.fromJson(response, FingerPrintResponse.class);
                if (fingerPrintResponse.is_successful) {
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        String leftRing = jsonObject.getString("leftring");
                        String leftLittle = jsonObject.getString("leftlittle");
                        String rightRing = jsonObject.getString("rightring");
                        String rightLittle = jsonObject.getString("rightlittle");

                        SharedPrefs.write(SharedPrefs.LEFT_RING, leftRing);
                        SharedPrefs.write(SharedPrefs.LEFT_LITTLE, leftLittle);
                        SharedPrefs.write(SharedPrefs.RIGHT_RING, rightRing);
                        SharedPrefs.write(SharedPrefs.RIGHT_LITTLE, rightLittle);

                        if (module.equals("1")) {
                            activity.startActivity(new Intent(activity, ValidateTunzhengbigBio.class));
                        }
                        if (module.equals("0")) {
                            activity.startActivity(new Intent(activity, ValidateCrossmatchBio.class));
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    pb.setVisibility(View.GONE);
                    Toast.makeText(activity, "Could not get customer's finger prints!", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onTokenExpired() {
            }

            @Override
            public void onError(String error) {
            }
        });
    }
}
