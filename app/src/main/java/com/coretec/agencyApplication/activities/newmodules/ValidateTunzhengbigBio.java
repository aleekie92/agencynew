package com.coretec.agencyApplication.activities.newmodules;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.os.Message;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.cloudpos.DeviceException;
import com.cloudpos.OperationResult;
import com.cloudpos.POSTerminal;
import com.cloudpos.TimeConstants;
import com.cloudpos.fingerprint.Fingerprint;
import com.cloudpos.fingerprint.FingerprintDevice;
import com.cloudpos.fingerprint.FingerprintOperationResult;
import com.coretec.agencyApplication.R;
import com.coretec.agencyApplication.activities.BaseActivity;
import com.coretec.agencyApplication.activities.LoginActivity;
import com.coretec.agencyApplication.activities.gfl.MainGFLDashboard;
import com.coretec.agencyApplication.api.GFL;
import com.coretec.agencyApplication.api.requests.GetMatchLogsRequest;
import com.coretec.agencyApplication.fingerprint.util.TextViewUtil;
import com.coretec.agencyApplication.utils.ByteConvertStringUtil;
import com.coretec.agencyApplication.utils.LogHelper;
import com.coretec.agencyApplication.utils.QuickToast;
import com.coretec.agencyApplication.utils.SharedPrefs;

import static com.coretec.agencyApplication.utils.Utils.getFormattedDate;
import static com.coretec.agencyApplication.utils.Utils.getLat;
import static com.coretec.agencyApplication.utils.Utils.getLong;


public class ValidateTunzhengbigBio extends AppCompatActivity {

    private Button identify,dismiss;
    private TextView show;
    private Context mContext = this;
    private int userID = 0;
    private int timeout = 10 * 1000;
    private FingerprintDevice fingerprintDevice = null;

    private SharedPreferences preferences;
    private SharedPreferences.Editor editor;

    private Handler handler;
    protected Handler mHandler = null;
    private static final int SHOW_NORMAL_MESSAGE = 5;
    private static final int SHOW_SUCCESS_MESSAGE = 6;
    private static final int SHOW_FAIL_MESSAGE = 7;
    private static final int SHOW_BTN = 8;
    private static final int HIDE_BTN = 9;
    public static boolean matched;

    Thread th = null;
    private QuickToast toast = new QuickToast(this);
    boolean doubleBackToExitPressedOnce = false;

    @SuppressLint("HandlerLeak")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        SharedPrefs.init(getApplicationContext());
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL, WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_WATCH_OUTSIDE_TOUCH, WindowManager.LayoutParams.FLAG_WATCH_OUTSIDE_TOUCH);

        setContentView(R.layout.finger_print_dialog);

        show = findViewById(R.id.textView);
        show.setMovementMethod(ScrollingMovementMethod.getInstance());

        identify = findViewById(R.id.btn_identify);
        dismiss = findViewById(R.id.btn_dismiss);

        dismiss.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("WrongConstant")
            @Override
            public void onClick(View v) {
                SharedPrefs.write(SharedPrefs.FP_BOOLEAN, "false");
                finish();
                Intent i=new Intent();
                if (SharedPrefs.read(SharedPrefs.COPORATE, null).equals("gfl")){
                    if(SharedPrefs.read(String.valueOf(SharedPrefs.TRANS_TYPE_BIOLOGS), 1) == 13){
                        finish();
                    }else {
                        i = new Intent(ValidateTunzhengbigBio.this, MainGFLDashboard.class);
                        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        startActivity(i);
                    }
                }if(SharedPrefs.read(SharedPrefs.COPORATE, null).equals("notgfl")) {
                    i = new Intent(ValidateTunzhengbigBio.this, MainDashboardActivity.class);
                    i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                    i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    startActivity(i);
                }

            }
        });

        identify.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (th == null || th.getState() == Thread.State.TERMINATED) {

                    th = new Thread() {
                        @Override
                        public void run() {
                            String finger1 = SharedPrefs.read(SharedPrefs.LEFT_RING, null);
                            String finger2 = SharedPrefs.read(SharedPrefs.LEFT_LITTLE, null);
                            String finger3 = SharedPrefs.read(SharedPrefs.RIGHT_RING, null);
                            String finger4 = SharedPrefs.read(SharedPrefs.RIGHT_LITTLE, null);
                            byte[] bytes1 = ByteConvertStringUtil.hexToBytes(finger1);
                            byte[] bytes2 = ByteConvertStringUtil.hexToBytes(finger2);
                            byte[] bytes3 = ByteConvertStringUtil.hexToBytes(finger3);
                            byte[] bytes4 = ByteConvertStringUtil.hexToBytes(finger4);

                            Fingerprint fingerprint1 = new Fingerprint();
                            fingerprint1.setFeature(bytes1);
                            Fingerprint fingerprint2 = new Fingerprint();
                            fingerprint2.setFeature(bytes2);
                            Fingerprint fingerprint3 = new Fingerprint();
                            fingerprint3.setFeature(bytes3);
                            Fingerprint fingerprint4 = new Fingerprint();
                            fingerprint4.setFeature(bytes4);

                            match(fingerprint1, fingerprint2, fingerprint3, fingerprint4);

                        }
                    };
                    th.start();
                }


            }
        });
        handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                super.handleMessage(msg);
                switch (msg.what) {
                    case SHOW_NORMAL_MESSAGE:
                        sendMsg((String) msg.obj);
                        scrollLogView();
                        break;
                    case SHOW_SUCCESS_MESSAGE:
                        sendSuccessMsg((String) msg.obj);
                        scrollLogView();
                        break;
                    case SHOW_FAIL_MESSAGE:
                        sendFailMsg((String) msg.obj);
                        scrollLogView();
                        break;

                    case SHOW_BTN:
                        break;
                    case HIDE_BTN:
                        break;
                }
            }
        };
        try {

            fingerprintDevice = (FingerprintDevice) POSTerminal.getInstance(this).getDevice("cloudpos.device.fingerprint");
            fingerprintDevice.open(1);
        } catch (DeviceException e) {
            e.printStackTrace();
        }
        mHandler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                String str = msg.obj + "\n";
                switch (msg.what) {
                    case R.id.log_default:
                        show.setText(str);
                        break;
                    case R.id.log_success:
                        // String str = msg.obj + "\n";
                        TextViewUtil.infoBlueTextView(show, str);
                        break;
                    case R.id.log_failed:
                        // String str = msg.obj + "\n";
                        TextViewUtil.infoRedTextView(show, str);
                        break;
                    case R.id.log_clean:
                        show.setText("");
                        break;
                }
            }
        };
    }
    private void writerLogInTextview(String log, int id) {
        Message msg = new Message();
        msg.what = id;
        msg.obj = log;
        mHandler.sendMessage(msg);
    }
    private void match(Fingerprint fingerprint1, Fingerprint fingerprint2, Fingerprint fingerprint3, Fingerprint fingerprint4){

        try {
            handler.obtainMessage(SHOW_SUCCESS_MESSAGE, getString(R.string.MESSAGE)).sendToTarget();
            Fingerprint fingerprint = getFingerprint();
            int index = 0;
            if (fingerprint != null) {
                int matchResult = 0;

                try {
                    if (fingerprint1.getFeature().length > 0) {
                        matchResult = fingerprintDevice.match(fingerprint, fingerprint1);
                        index = 1;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

                try {
                    if (fingerprint2.getFeature().length > 0) {
                        matchResult = fingerprintDevice.match(fingerprint, fingerprint2);
                        index = 2;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

                try {
                    if (fingerprint3.getFeature().length > 0) {
                        matchResult = fingerprintDevice.match(fingerprint, fingerprint3);
                        index = 3;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

                try {
                    if (fingerprint4.getFeature().length > 0) {
                        matchResult = fingerprintDevice.match(fingerprint, fingerprint4);
                        index = 4;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                if (matchResult > 10) {
                    matched = true;
                    requestMatchLogs();
                    handler.obtainMessage(SHOW_SUCCESS_MESSAGE, mContext.getString(R.string.MatchSuccess) + matchResult + ",fingerPrint index = " + index).sendToTarget();
                    handler.obtainMessage(SHOW_SUCCESS_MESSAGE, "Success, continue");
                    SharedPrefs.write(SharedPrefs.FP_BOOLEAN, "true");
                    finish();
                } else {
                    matched=false;
                    requestMatchLogs();
                    handler.obtainMessage(SHOW_FAIL_MESSAGE, mContext.getString(R.string.MatchFailed)).sendToTarget();
                    handler.obtainMessage(SHOW_FAIL_MESSAGE, "Position your finger correctly!");
                    SharedPrefs.write(SharedPrefs.FP_BOOLEAN, "false");
                }
            } else {
                handler.obtainMessage(SHOW_FAIL_MESSAGE, mContext.getString(R.string.FAILEDINFO)).sendToTarget();
                handler.obtainMessage(SHOW_FAIL_MESSAGE, "Try a differnt finger!");
                SharedPrefs.write(SharedPrefs.FP_BOOLEAN, "false");
            }
        } catch (Exception e) {
            e.printStackTrace();
            handler.obtainMessage(SHOW_FAIL_MESSAGE, mContext.getString(R.string.DEVICEFAILED)).sendToTarget();
            handler.obtainMessage(SHOW_FAIL_MESSAGE,"Check on device compatibility");
            SharedPrefs.write(SharedPrefs.FP_BOOLEAN, "false");
        } finally {
            handler.obtainMessage(SHOW_BTN).sendToTarget();
        }
    }


    private Fingerprint getFingerprint() {
        Fingerprint fingerprint = null;
        try {
            FingerprintOperationResult operationResult = fingerprintDevice.waitForFingerprint(TimeConstants.FOREVER);
            if (operationResult.getResultCode() == OperationResult.SUCCESS) {
                fingerprint = operationResult.getFingerprint(0, 0);
            } else {
                handler.obtainMessage(SHOW_FAIL_MESSAGE, this.getString(R.string.FAILEDINFO)).sendToTarget();
            }
        } catch (DeviceException e) {
            e.printStackTrace();
            handler.obtainMessage(SHOW_FAIL_MESSAGE, this.getString(R.string.DEVICEFAILED)).sendToTarget();
        }
        return fingerprint;
    }

    private void sendSuccessMsg(String msg) {
        LogHelper.infoAppendMsgForSuccess(msg + "\n", show);
        scrollLogView();
    }

    private void sendFailMsg(String msg) {
        LogHelper.infoAppendMsgForFailed(msg + "\n", show);
        scrollLogView();
    }

    private void sendMsg(String msg) {
        LogHelper.infoAppendMsg(msg + "\n", show);
        scrollLogView();
    }

    public void scrollLogView() {
        int offset = show.getLineCount() * show.getLineHeight();
        if (offset > show.getHeight()) {
            show.scrollTo(0, offset - show.getHeight());
        }
    }

    private String getStr(int strId) {
        return getResources().getString(strId);
    }

    @Override
    public void onBackPressed() {

        if (doubleBackToExitPressedOnce) {
            String msg = "Are you sure you want to close finger print dialog? ";
            android.app.AlertDialog.Builder builder1 = new android.app.AlertDialog.Builder(ValidateTunzhengbigBio.this);
            builder1.setMessage(msg);
            builder1.setTitle("Alert!");
            builder1.setCancelable(false);

            builder1.setPositiveButton(
                    "yes",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();

                            LoginActivity la=new LoginActivity();
                            finish();
                            Intent i=new Intent();

                            if (SharedPrefs.read(SharedPrefs.COPORATE, null).equals("gfl")){
                                i = new Intent(ValidateTunzhengbigBio.this, MainGFLDashboard.class);
                            }if(SharedPrefs.read(SharedPrefs.COPORATE, null).equals("notgfl")) {
                                i = new Intent(ValidateTunzhengbigBio.this, MainDashboardActivity.class);
                            }

                            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                            //i.addFlags(IntentCompat.FLAG_ACTIVITY_CLEAR_TASK);
                            i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(i);
                            //onBackPressed();
                        }
                    });

            builder1.setNegativeButton(
                    "CANCEL",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                            //onBackPressed();
                        }
                    });

            AlertDialog alert11 = builder1.create();
            alert11.show();
            //System.exit(1);
            //Toast.makeText(this, "Are you sure you want to exit Member Registration", Toast.LENGTH_LONG).show();
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        toast.sinfo("Press back again to exit");
        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce=false;
            }
        }, 2000);
        //super.onBackPressed();
        //overridePendingTransition(R.anim.move_left_in_activity, R.anim.move_right_out_activity);
    }
    @Override
    protected void onResume() {
        super.onResume();
    }
    private void requestMatchLogs() {

        String URL = GFL.MSACCO_AGENT + GFL.userlogs;
        final GetMatchLogsRequest matchlogsrequest = new GetMatchLogsRequest();
        matchlogsrequest.match = matched;
        matchlogsrequest.corporate_no ="CAP016";
        matchlogsrequest.transactiontype =SharedPrefs.read(String.valueOf(SharedPrefs.TRANS_TYPE_BIOLOGS), 1);
        matchlogsrequest.growerno =SharedPrefs.read((SharedPrefs.GROWERS_NUMBER_BIOLOGS), null);
        matchlogsrequest.agentid = SharedPrefs.read(SharedPrefs.AGENT_ID_1, null);
        matchlogsrequest.terminalid = SharedPrefs.read(SharedPrefs.DEVICE_TERMINAL, null);
        matchlogsrequest.longitude = getLong(this);
        matchlogsrequest.latitude = getLat(this);
        matchlogsrequest.date = getFormattedDate();



        GFL.instance(this).request(URL, matchlogsrequest, new GFL.RequestListener() {
            @Override
            public void onSuccess(String response) {

            }

            @Override
            public void onTokenExpired() {

            }

            @Override
            public void onError(String error) {

            }
        });

    }

}
