package com.coretec.agencyApplication.activities.gfl;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.Toast;

import com.coretec.agencyApplication.R;
import com.coretec.agencyApplication.activities.BaseActivity;
import com.coretec.agencyApplication.activities.LoginActivity;
import com.coretec.agencyApplication.activities.SplashActivity;
import com.coretec.agencyApplication.activities.newmodules.MainDashboardActivity;
import com.coretec.agencyApplication.api.Api;
import com.coretec.agencyApplication.api.GFL;
import com.coretec.agencyApplication.api.requests.GetAgentAccountInfoRequest;
import com.coretec.agencyApplication.api.requests.NameRequest;
import com.coretec.agencyApplication.api.responses.GflAgentAccountResponse;
import com.coretec.agencyApplication.api.responses.NameResponse;
import com.coretec.agencyApplication.utils.LogOutTimerUtil;
import com.coretec.agencyApplication.utils.PreferenceFileKeys;
import com.coretec.agencyApplication.utils.QuickToast;
import com.coretec.agencyApplication.utils.SharedPrefs;
import com.coretec.agencyApplication.utils.Utils;

import java.util.Calendar;

import static com.coretec.agencyApplication.utils.Utils.getFormattedDate;
import static com.coretec.agencyApplication.utils.Utils.getIMEI;
import static com.coretec.agencyApplication.utils.Utils.getLat;
import static com.coretec.agencyApplication.utils.Utils.getLong;

public class MainGFLDashboard extends BaseActivity implements LogOutTimerUtil.LogOutListener{

    private QuickToast toast = new QuickToast(this);
    private Button btnLoanApplication, btnCustomerRegistration, btnReports,btnAgentAccountInfo,
            btnpusettings,btntransactions;

    private CardView reg,loanapp, agentinfor,reports,cardviewpusettings,cardviewtransactions;
    SharedPreferences sharedPref;
    boolean doubleBackToExitPressedOnce = false;
    public static String imei = "", corporateno = "", saccomotto = "";
    public String greetings;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.gfl_admi_dash);
        this.overridePendingTransition(R.anim.move_right_in_activity, R.anim.move_left_out_activity);
        imei = Utils.getIMEI(this);

        sharedPref = getSharedPreferences(PreferenceFileKeys.PREFS_FILE, Context.MODE_PRIVATE);
        SharedPrefs. write(SharedPrefs.DEVICE_TERMINAL, imei);
        getAgentName();
        requestAgentInfo();
        btnCustomerRegistration = findViewById(R.id.btnFarmerRegistration);
        btnLoanApplication = findViewById(R.id.btnLoanApplication);
        btnpusettings = findViewById(R.id.btnpusettings);
        btntransactions = findViewById(R.id.btntransactions);
        btnAgentAccountInfo = findViewById(R.id.btnAgentAccountInfo);
        btnReports = findViewById(R.id.btnReports);

        reg= findViewById(R.id.reg);
        loanapp= findViewById(R.id.loanapp);
        agentinfor= findViewById(R.id.agentinfor);
        reports= findViewById(R.id.reports);
        cardviewpusettings= findViewById(R.id.cardviewpusettings);
        cardviewtransactions= findViewById(R.id.cardviewtransactions);


        if (SharedPrefs.read(SharedPrefs.MENU_SEPERATOR, null).equals("admin")){
            reg.setVisibility(View.VISIBLE);
            loanapp.setVisibility(View.VISIBLE);
            agentinfor.setVisibility(View.VISIBLE);
            reports.setVisibility(View.VISIBLE);
            cardviewpusettings.setVisibility(View.VISIBLE);
            cardviewtransactions.setVisibility(View.VISIBLE);

        }else if (SharedPrefs.read(SharedPrefs.MENU_SEPERATOR, null).equals("dsr")){
            reg.setVisibility(View.VISIBLE);
            loanapp.setVisibility(View.VISIBLE);

        }else if(SharedPrefs.read(SharedPrefs.MENU_SEPERATOR, null).equals("commissioned")){
            reg.setVisibility(View.VISIBLE);
            loanapp.setVisibility(View.VISIBLE);
            agentinfor.setVisibility(View.VISIBLE);
            reports.setVisibility(View.VISIBLE);
        }

        else {
            reg.setVisibility(View.VISIBLE);
            loanapp.setVisibility(View.VISIBLE);
            agentinfor.setVisibility(View.VISIBLE);
            reports.setVisibility(View.VISIBLE);
            cardviewpusettings.setVisibility(View.VISIBLE);
            cardviewtransactions.setVisibility(View.VISIBLE);
        }

        getTime();
        btnCustomerRegistration.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainGFLDashboard.this, GflMemberRegistry.class));
            }
        });

        btnLoanApplication.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainGFLDashboard.this, Loans.class));
            }
        });


        btnpusettings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainGFLDashboard.this, PuSettings.class));
            }
        });

        btntransactions.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainGFLDashboard.this, Transactions.class));
            }
        });

        btnAgentAccountInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MainGFLDashboard.this, AgentInfo.class));
            }
        });
        btnReports.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Toast.makeText(MainGFLDashboard.this, "under review", Toast.LENGTH_SHORT).show();
                startActivity(new Intent(MainGFLDashboard.this, com.coretec.agencyApplication.activities.gfl.Reports.class));
            }
        });

    }

    public void getTime() {
        Calendar c = Calendar.getInstance();
        int timeOfDay = c.get(Calendar.HOUR_OF_DAY);

        if (timeOfDay >= 0 && timeOfDay < 8) {
            greetings = "Good Morning";
        } else if (timeOfDay >= 8 && timeOfDay < 12) {
            greetings = "Good Morning";
        } else if (timeOfDay >= 12 && timeOfDay < 16) {
            greetings = "Good Afternoon";
        } else if (timeOfDay >= 16 && timeOfDay < 22) {
            greetings = "Good Evening";
        } else if (timeOfDay >= 22 && timeOfDay < 24) {
            greetings = "Hi,it's bed time";
        }
    }


    void requestAgentInfo() {
        final String TAG = "request agent Info";
        String URL = GFL.MSACCO_AGENT + GFL.GetAgentAccountInfo;
        GetAgentAccountInfoRequest accountInfo = new GetAgentAccountInfoRequest();
        accountInfo.agentid = sharedPref.getString(PreferenceFileKeys.AGENT_ID, "");
        accountInfo.terminalid = getIMEI(this);
        accountInfo.longitude = getLong(this);
        accountInfo.latitude = getLat(this);
        accountInfo.date = getFormattedDate();
        Log.e(TAG, accountInfo.getBody().toString());

        GFL.instance(this).request(URL, accountInfo, new GFL.RequestListener() {
            @Override
            public void onSuccess(String response) {
                Log.e(TAG, response);
                /*final GetAgentAccountInfoResponse getAgentAccountInfoResponse = GFL.instance(MainGFLDashboard.this)
                        .mGson.fromJson(response, GetAgentAccountInfoResponse.class);*/

                final GflAgentAccountResponse getAgentAccountInfoResponse = GFL.instance(MainGFLDashboard.this)
                        .mGson.fromJson(response, GflAgentAccountResponse.class);

                if (getAgentAccountInfoResponse.is_successful) {
                    corporateno = getAgentAccountInfoResponse.getCorporateno();
                    SharedPreferences.Editor editor = sharedPref.edit();
                    editor.putString(PreferenceFileKeys.AGENT_SACCO_NAME, getAgentAccountInfoResponse.getSacconame());
                    saccomotto = getAgentAccountInfoResponse.getSaccomotto();
                    editor.putString(PreferenceFileKeys.AGENT_ACCOUNT_NAME, getAgentAccountInfoResponse.getAgentaccountname());
                    editor.putString(PreferenceFileKeys.AGENT_ACCOUNT_NO, getAgentAccountInfoResponse.getAgentaccountno());
                    editor.putString(PreferenceFileKeys.AGENT_ACCOUNT_BALANCE, getAgentAccountInfoResponse.agentaccountbal);
                    editor.putString(PreferenceFileKeys.AGENT_COMMISSION_BALANCE, getAgentAccountInfoResponse.getAgentcommisionbal());
                    editor.apply();

                   /* try {
                        JSONObject jsonObject = new JSONObject(response);

                        JSONArray jsonArray = jsonObject.getJSONArray("agentdetails");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                            SharedPreferences.Editor editor = sharedPref.edit();
                            editor.putString(PreferenceFileKeys.AGENT_SACCO_NAME, jsonObject.getString("sacconame"));
                            editor.putString(PreferenceFileKeys.AGENT_SACCO_MOTTO, jsonObject1.getString("PostingDate"));
                            saccomotto = getAgentAccountInfoResponse.getSaccomotto();
                            editor.putString(PreferenceFileKeys.AGENT_ACCOUNT_NAME, jsonObject1.getString("AgentAccountName"));
                            editor.putString(PreferenceFileKeys.AGENT_BUSINESS_NAME, jsonObject1.getString("PostingDate"));
                            editor.putString(PreferenceFileKeys.AGENT_ACCOUNT_NO, jsonObject1.getString("PostingDate"));
                            editor.putString(PreferenceFileKeys.AGENT_LOCATION, jsonObject1.getString("PostingDate"));
                            editor.putString(PreferenceFileKeys.AGENT_ACCOUNT_BALANCE, jsonObject1.getString("PostingDate"));
                            editor.putString(PreferenceFileKeys.AGENT_COMMISSION_BALANCE, jsonObject1.getString("PostingDate"));
                            editor.apply();
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }*/
                } else {

                }

            }

            @Override
            public void onTokenExpired() {
                //finish();
            }

            @Override
            public void onError(String error) {

            }
        });

    }


    public void getAgentName () {
        final String TAG = "AGENT NAME";
        String URL = Api.MSACCO_AGENT + GFL.GetAgentname;
        final NameRequest nameRequest = new NameRequest();
        nameRequest.agentid = sharedPref.getString(PreferenceFileKeys.AGENT_ID, "");
        nameRequest.terminalid = SharedPrefs.read(SharedPrefs.DEVICE_ID,  null);

        Log.e(TAG, nameRequest.getBody().toString());

        Api.instance(MainGFLDashboard.this).request(URL, nameRequest, new Api.RequestListener() {
            @Override
            public void onSuccess(String response) {

                NameResponse nameResponse = Api.instance(MainGFLDashboard.this).mGson.fromJson(response, NameResponse.class);
                if (nameResponse.is_successful) {
                    Toast.makeText(MainGFLDashboard.this, getString(R.string.app_time, nameResponse.getAgent_name(), greetings ), Toast.LENGTH_LONG).show();
                    SharedPrefs.write(SharedPrefs.GFL_AGENT_NAME,nameResponse.getAgent_name());

                } else {
                    toast.swarn("Could not get agent details!");
                }
            }

            @Override
            public void onTokenExpired() {

            }

            @Override
            public void onError(String error) {

            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_logout) {
            //return true;
            String msg = "Are you sure you want to Logout? ";
            AlertDialog.Builder builder1 = new AlertDialog.Builder(MainGFLDashboard.this);
            builder1.setMessage(msg);
            builder1.setTitle("Alert!");
            builder1.setCancelable(false);

            builder1.setPositiveButton(
                    "yes",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                            //System.exit(0);
                            finish();
                            Intent i  = new Intent(MainGFLDashboard.this, LoginActivity.class);
                            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                            i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

                            startActivity(i);
                        }

                    });

            builder1.setNegativeButton(
                    "CANCEL",
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int id) {
                            dialog.cancel();
                            //onBackPressed();
                        }
                    });

            //System.exit(1);
            AlertDialog alert11 = builder1.create();
            alert11.show();
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        SharedPreferences settings = getSharedPreferences("IS_ADMIN", Context.MODE_PRIVATE);
        settings.edit().clear().commit();
        //super.onBackPressed();
        Toast.makeText(this, "You need some rest? Use Logout button", Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    public void onUserInteraction() {
        super.onUserInteraction();

    }


    @Override
    protected void onPause() {
        super.onPause();
        LogOutTimerUtil.startLogoutTimer(this, this);

    }

   /* @Override
    protected void onStop() {
        super.onStop();
        finish();
        //System.exit(0);
        Intent i  = new Intent(MainGFLDashboard.this, LoginActivity.class);
        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(i);
    }*/

    @Override
    protected void onResume() {
        LogOutTimerUtil.startLogoutTimer(this, this);
        super.onResume();

    }

   /* @Override
    protected void onStop() {
        super.onStop();
        Intent i = new Intent(MainGFLDashboard.this, SplashActivity.class);
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        this.startActivity(i);
    }*/

    @Override
    public void doLogout() {
        finish();
        //System.exit(0);
        Intent i  = new Intent(MainGFLDashboard.this, LoginActivity.class);
        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(i);
    }
}


