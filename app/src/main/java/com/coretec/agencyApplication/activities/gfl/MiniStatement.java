package com.coretec.agencyApplication.activities.gfl;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.cloudpos.jniinterface.PrinterInterface;
import com.coretec.agencyApplication.R;
import com.coretec.agencyApplication.activities.BaseActivity;
import com.coretec.agencyApplication.activities.newmodules.ValidateTunzhengbigBio;
import com.coretec.agencyApplication.api.GFL;
import com.coretec.agencyApplication.api.requests.FingerPrintRequest;
import com.coretec.agencyApplication.api.requests.GetMiniStatementRequest;
import com.coretec.agencyApplication.api.requests.GrowersDetailsRequest;
import com.coretec.agencyApplication.api.responses.FingerPrintResponse;
import com.coretec.agencyApplication.api.responses.GetMiniStatementResponse;
import com.coretec.agencyApplication.api.responses.GrowersDetailsResponse;
import com.coretec.agencyApplication.model.GrowerDetails;
import com.coretec.agencyApplication.utils.SharedPrefs;
import com.coretec.agencyApplication.wizarpos.function.printer.PrinterCommand;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static com.coretec.agencyApplication.utils.Utils.getFormattedDate;
import static com.coretec.agencyApplication.utils.Utils.getLat;
import static com.coretec.agencyApplication.utils.Utils.getLong;

public class MiniStatement extends AppCompatActivity {

    private ImageView backBtn;
    Button submit;

    private TextInputLayout input_layout_id_number;
    private EditText identifier_deposit;
    private Spinner spinner;
    private ArrayList<String> arrayList = new ArrayList<>();
    private ArrayAdapter<GrowerDetails> adapter;

    private Spinner spinner2;
    private ArrayList<String> arrayList2 = new ArrayList<>();
    private ArrayAdapter<String> adapter2;

    private String identifiercode = "";
    private String growerNo = "";

    private String item;
    ProgressBar progressBar;
    ProgressDialog progressDialog;
    String text;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.gfl_mini_statement);
        SharedPrefs.init(getApplicationContext());

        this.overridePendingTransition(R.anim.move_right_in_activity, R.anim.move_left_out_activity);

        SharedPrefs.write(SharedPrefs.FP_BOOLEAN, "");
        SharedPrefs.write(SharedPrefs.MINI_G_NUMBER, "");

        Toolbar toolbar = findViewById(R.id.toolbar);
        TextView mTitle = toolbar.findViewById(R.id.toolbar_title);
        mTitle.setText(getResources().getText(R.string.mini_statement));
        setSupportActionBar(toolbar);
        ActionBar actionbar = getSupportActionBar();
        actionbar.setDisplayHomeAsUpEnabled(true);
        actionbar.setHomeAsUpIndicator(R.drawable.ic_arrow_back);

        submit = findViewById(R.id.btnSubmit);
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (identifier_deposit.getText().toString().matches("")) {
                    Toast.makeText(getBaseContext(), "Please fill in the missing fields to proceed!", Toast.LENGTH_LONG).show();
                } else {
                    /* progress dialog */
                    progressDialog = new ProgressDialog(MiniStatement.this);
                    progressDialog.setMessage("Please wait...");
                    progressDialog.setCancelable(false);
                    progressDialog.show();
                    requestFingerPrints();
                    //getMiniStatement();
                }
            }
        });

        input_layout_id_number = findViewById(R.id.input_layout_id_number);
        identifier_deposit = findViewById(R.id.id_number);
        identifier_deposit.setInputType(InputType.TYPE_NULL);
        identifier_deposit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // showMyDialog();
            }
        });

        identifier_deposit.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    // showMyDialog();
                }
            }
        });

        spinner = findViewById(R.id.growerSpinner);
        spinner2 = findViewById(R.id.spinner_deposit);

        progressBar = findViewById(R.id.growersPb);

        TextWatcher textWatcherId = new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                //here, after we introduced something in the EditText we get the string from it
                String identifier_number = identifier_deposit.getText().toString();
                if (identifier_deposit.length() >= 7) {
                    requestGetAccountDetails(identifier_number);
                }
                if (identifier_deposit.length() <= 5) {
                    progressBar.setVisibility(View.GONE);
                    arrayList.clear();
                    spinner = findViewById(R.id.growerSpinner);
                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(MiniStatement.this, android.R.layout.simple_spinner_item, arrayList);
                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    spinner.setAdapter(null);
                }
                if (identifier_deposit.length() >= 5 && identifier_deposit.length() <= 8) {
                    arrayList.clear();
                    spinner = findViewById(R.id.growerSpinner);
                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(MiniStatement.this, android.R.layout.simple_spinner_item, arrayList);
                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    spinner.setAdapter(null);
                    progressBar.setVisibility(View.VISIBLE);
                    progressBar.getIndeterminateDrawable().setColorFilter(ContextCompat.getColor(MiniStatement.this, R.color.btn_green), PorterDuff.Mode.SRC_IN);
                }
            }
        };

        adapter = new ArrayAdapter<GrowerDetails>(getApplicationContext(),
                R.layout.simple_spinner_dropdown_item, new ArrayList<GrowerDetails>());
        adapter.setDropDownViewResource(R.layout.simple_spinner_dropdown_item);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                item = parent.getItemAtPosition(position).toString();
                text = spinner.getSelectedItem().toString();

                SharedPrefs.write(SharedPrefs.MINI_G_NUMBER, text);
                Log.e("GROWER NUMBER", text);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        adapter2 = new ArrayAdapter<String>(getApplicationContext(), R.layout.simple_spinner_dropdown_item, arrayList2);
        adapter2.setDropDownViewResource(R.layout.simple_spinner_dropdown_item);
        spinner2.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                identifiercode = String.valueOf(position);
                // Toast.makeText(getApplicationContext(),String.valueOf(position),Toast.LENGTH_LONG).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        identifier_deposit.addTextChangedListener(textWatcherId);

    }

    void requestFingerPrints() {

        final String TAG = "GET FINGERPRINTS";
        String URL = GFL.MSACCO_AGENT + GFL.GetFourFingerPrint;
        final FingerPrintRequest fingerPrintRequest = new FingerPrintRequest();
        fingerPrintRequest.accountidentifier = identifier_deposit.getText().toString();
        fingerPrintRequest.accountidentifiercode = "1";
        fingerPrintRequest.agentid = SharedPrefs.read(SharedPrefs.AGENT_ID_1, null);
        fingerPrintRequest.terminalid = SharedPrefs.read(SharedPrefs.DEVICE_ID, null);
        fingerPrintRequest.longitude = getLong(MiniStatement.this);
        fingerPrintRequest.latitude = getLat(MiniStatement.this);
        fingerPrintRequest.date = getFormattedDate();

        Log.e(TAG, fingerPrintRequest.getBody().toString());

        GFL.instance(MiniStatement.this).request(URL, fingerPrintRequest, new GFL.RequestListener() {
            @Override
            public void onSuccess(String response) {
                FingerPrintResponse fingerPrintResponse = GFL.instance(MiniStatement.this)
                        .mGson.fromJson(response, FingerPrintResponse.class);
                if (fingerPrintResponse.is_successful){

                    try {
                        JSONObject jsonObject = new JSONObject(response);

                        String leftRing = jsonObject.getString("leftring");
                        String leftLittle = jsonObject.getString("leftlittle");
                        String rightRing = jsonObject.getString("rightring");
                        String rightLittle = jsonObject.getString("rightlittle");

                        SharedPrefs.write(SharedPrefs.LEFT_RING, leftRing);
                        SharedPrefs.write(SharedPrefs.LEFT_LITTLE, leftLittle);
                        SharedPrefs.write(SharedPrefs.RIGHT_RING, rightRing);
                        SharedPrefs.write(SharedPrefs.RIGHT_LITTLE, rightLittle);

                        Log.d("LEFT_RING", leftRing);
                        Log.d("LEFT_LITTLE", leftLittle);
                        Log.d("RIGHT_RING", rightRing);
                        Log.d("RIGHT_LITTLE", rightLittle);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    SharedPrefs.transactionstypelogs(9, MiniStatement.this);
                    SharedPrefs.growerslogs(text, MiniStatement.this);
                    startActivity(new Intent(MiniStatement.this, ValidateTunzhengbigBio.class));

                } else {
                    progressDialog.dismiss();
                    //toast.swarn("Could not get customer finger prints!");
                    Toast.makeText(MiniStatement.this, "Could not get fingerprints", Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onTokenExpired() {

            }

            @Override
            public void onError(String error) {

            }
        });

    }

    private void getMiniStatement() {

        final String TAG = "Mini statement";
        String URL = GFL.MSACCO_AGENT + GFL.GetMiniStatement;
        String gNumber = SharedPrefs.read(SharedPrefs.MINI_G_NUMBER, null);

        final GetMiniStatementRequest miniStatement = new GetMiniStatementRequest();
        miniStatement.corporate_no = "CAP016";
        miniStatement.growerno = gNumber;
        miniStatement.noofentries = "10";
        miniStatement.agentid = SharedPrefs.read(SharedPrefs.AGENT_ID_1, null);
        miniStatement.terminalid = SharedPrefs.read(SharedPrefs.DEVICE_ID, null);
        miniStatement.longitude = getLong(this);
        miniStatement.latitude = getLat(this);
        miniStatement.date = getFormattedDate();

        Log.e(TAG, miniStatement.getBody().toString());

        GFL.instance(this).request(URL, miniStatement, new GFL.RequestListener() {
            @Override
            public void onSuccess(String response) {
                /*deposit_progressBar.setVisibility(View.GONE);*/
                progressDialog.dismiss();
                final GetMiniStatementResponse miniStatementResponse1 = GFL.instance(MiniStatement.this)
                        .mGson.fromJson(response, GetMiniStatementResponse.class);
                if (miniStatementResponse1.is_successful) {
                    progressBar.setVisibility(View.GONE);


                    try {
                        JSONObject jsonObject = new JSONObject(response);

                        JSONArray jsonArray = jsonObject.getJSONArray("growerministatement");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject1 = jsonArray.getJSONObject(i);


                            //String gNo = jsonObject2.getString("GrowerNo");
                            String postingDate = jsonObject1.getString("PostingDate");
                            String LoanNo = jsonObject1.getString("LoanNo");
                            String description = jsonObject1.getString("Description");
                            String amount = jsonObject1.getString("Amount");

                            SharedPrefs.write(SharedPrefs.MINI_POSTING_DATE, postingDate);
                            SharedPrefs.write(SharedPrefs.LOAN_CODE, LoanNo);
                            SharedPrefs.write(SharedPrefs.MINI_DESCRIPTION, description);
                            SharedPrefs.write(SharedPrefs.MINI_AMOUNT, amount);

                            Log.e("POSTING DATE", postingDate);
                            Log.e("LoanNo", LoanNo);
                            Log.e("DESCRIPTION", description);
                            Log.e("AMOUNT", amount);

                            String mpd = SharedPrefs.read(SharedPrefs.MINI_POSTING_DATE, null);
                            String mln = SharedPrefs.read(SharedPrefs.LOAN_CODE, null);
                            String md = SharedPrefs.read(SharedPrefs.MINI_DESCRIPTION, null);
                            String ma = SharedPrefs.read(SharedPrefs.MINI_AMOUNT, null);
                            Log.d("dataaaaa",mpd);

                            int result = PrinterInterface.open();
                            writetest(mpd,mln, md, ma);
                            PrinterInterface.close();

                        }


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    String agentName = SharedPrefs.read(SharedPrefs.AGENT_NAME, null);
                    int result = PrinterInterface.open();
                    footer(agentName);
                    PrinterInterface.close();

                } else {
//                    Toast.makeText(ShareDeposit.this, "Accounts Fetching Failed", Toast.LENGTH_LONG).show();
                }

            }

            @Override
            public void onTokenExpired() {

            }

            @Override
            public void onError(String error) {

            }
        });

    }

    public void footer(String aName){
        try {
            byte[] time = null;
            byte[] nameAgent = null;

            SimpleDateFormat timeStampFormat = new SimpleDateFormat("yyyy/MM/dd - HH:mm");
            Date myDate = new Date();
            String timeCurrent = timeStampFormat.format(myDate);

            try {
                time = String.valueOf("   Date : " + String.valueOf(timeCurrent)).getBytes("GB2312");
                nameAgent = String.valueOf("You were served by " + String.valueOf(aName)).getBytes("GB2312");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            PrinterInterface.begin();
            byte[] cmd;
            cmd = new byte[]{(byte) 0x1B, (byte) 0x40};

            cmd = new byte[]{
                    (byte) 0x1B, (byte) 0x45, (byte) 0};

            write(cmd);
            write(time);
            writeLineBreak(2);
            write(nameAgent);
            // print line break
            writeLineBreak(4);
            writeLineBreak(5);
            PrinterInterface.end();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void header(String growerNumber){

        //String middle = idNos.substring(2, 6);
        String first = growerNumber.substring(0, 2);
        String last = growerNumber.substring(6, growerNumber.length());
        String maskedG = first + "****"+last;
        try {
            byte[] start = null;
            byte[] saccoName = null;
            byte[] type = null;
            byte[] gno = null;
            byte[] isDefa = null;

            try {
                start = String.valueOf("              ").getBytes("GB2312");
                saccoName = String.valueOf("      GREENLAND FEDHA LTD     ").getBytes("GB2312");
                type = String.valueOf("     MINISTATEMENT REQUEST     ").getBytes("GB2312");
                gno = String.valueOf("Grower Number : " + String.valueOf(maskedG)).getBytes("GB2312");
                isDefa = String.valueOf("Defaulter: "+SharedPrefs.read(SharedPrefs.IS_DEFAULTER, null)).getBytes("GB2312");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            PrinterInterface.begin();
            byte[] cmd;
            cmd = new byte[]{(byte) 0x1B, (byte) 0x40};

            cmd = new byte[]{
                    (byte) 0x1B, (byte) 0x45, (byte) 0};

            write(cmd);
            write(start);
            writeLineBreak(1);
            write(saccoName);
            writeLineBreak(2);
            write(type);
            writeLineBreak(1);
            write(gno);
            writeLineBreak(1);
            write(isDefa);
            writeLineBreak(2);
            PrinterInterface.end();

        }catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void writetest(String datePosting, String loannumber, String desc, String amnt) {
        try {
            byte[] loannum = null;
            byte[] date = null;
            byte[] descriptn = null;
            byte[] mini_amount = null;


            try {
                date = String.valueOf("Posting Date : " + String.valueOf(datePosting)).getBytes("GB2312");
                loannum = String.valueOf("Loan Number : " + String.valueOf(loannumber)).getBytes("GB2312");
                descriptn = String.valueOf("Description : " + String.valueOf(desc)).getBytes("GB2312");
                mini_amount = String.valueOf("Amount : " + String.valueOf(amnt)).getBytes("GB2312");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            PrinterInterface.begin();

            byte[] cmd;
            cmd = new byte[]{(byte) 0x1B, (byte) 0x40};

            cmd = new byte[]{
                    (byte) 0x1B, (byte) 0x45, (byte) 0};

            write(cmd);
            //write(titles);
            //writeLineBreak(1);
            write(date);
            writeLineBreak(0);
            write(loannum);
            writeLineBreak(0);
            write(descriptn);
            // print line break
            writeLineBreak(0);
            write(mini_amount);
            writeLineBreak(1);
            PrinterInterface.end();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void write(final byte[] arryData) {
        int result = PrinterInterface.write(arryData, arryData.length);
    }

    // print line break
    private void writeLineBreak(int lineNumber) {
        // write(new byte[]{'\n'});
        write(PrinterCommand.getCmdEscDN(lineNumber));
    }

    void requestGetAccountDetails(String identifier) {

        final String TAG = "GrowersAccounts";
//        EasyLocationMod easyLocationMod = new EasyLocationMod(CashWithdrawal.this);

        String URL = GFL.MSACCO_AGENT + GFL.GetGrowersDetails;

        final GrowersDetailsRequest growersDetails = new GrowersDetailsRequest();
        growersDetails.agentid = SharedPrefs.read(SharedPrefs.AGENT_ID_1, null);
        growersDetails.corporateno = "CAP016";
        growersDetails.accountidentifier = identifier;
        growersDetails.accountidentifiercode = "1";
        growersDetails.transactiontype = "1";
        growersDetails.terminalid = SharedPrefs.read(SharedPrefs.DEVICE_ID, null);
        growersDetails.longitude = getLong(this);
        growersDetails.latitude = getLat(this);
        growersDetails.date = getFormattedDate();

        Log.e(TAG, growersDetails.getBody().toString());
        arrayList.clear();
        spinner = findViewById(R.id.growerSpinner);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(MiniStatement.this, android.R.layout.simple_spinner_item, arrayList);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(null);

        GFL.instance(this).request(URL, growersDetails, new GFL.RequestListener() {
            @Override
            public void onSuccess(String response) {
                /*deposit_progressBar.setVisibility(View.GONE);*/

                GrowersDetailsResponse growersDetailsResponse = GFL.instance(MiniStatement.this)
                        .mGson.fromJson(response, GrowersDetailsResponse.class);
                if (growersDetailsResponse.is_successful) {
                    List<GrowerDetails> growerDetailsList = new ArrayList<>();

                    try {
                        JSONObject jsonObject = new JSONObject(response);

                        JSONArray jsonArray = jsonObject.getJSONArray("Growers");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                            JSONArray jsonArray1 = jsonObject1.getJSONArray("GrowerDetails");
                            for (int l = 0; l < jsonArray1.length(); l++) {
                                JSONObject jsonObject2 = jsonArray1.getJSONObject(l);
                                progressBar.setVisibility(View.GONE);
                                arrayList.add(String.valueOf(jsonObject2.getString("GrowerNo")));
                                /*mpesaPhoneNumber = jsonObject2.getString("phonenumber");*/
                                String gNo = jsonObject2.getString("GrowerNo");
                                String isDefaulter = jsonObject2.getString("LoanDefaulter");

                                SharedPrefs.write(SharedPrefs.IS_DEFAULTER, isDefaulter);

                                Log.e("Growers NUMBER:", gNo);
                            }

                        }

                        //Growers Number
                        spinner = findViewById(R.id.growerSpinner);
                        ArrayAdapter<String> adapter = new ArrayAdapter<String>(MiniStatement.this, android.R.layout.simple_spinner_item, arrayList);
                        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                        spinner.setAdapter(adapter);


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                } else {
//                    Toast.makeText(ShareDeposit.this, "Accounts Fetching Failed", Toast.LENGTH_LONG).show();
                }

            }

            @Override
            public void onTokenExpired() {

            }

            @Override
            public void onError(String error) {

            }
        });
    }

    @Override
    protected void onResume() {
        SharedPrefs.init(getApplicationContext());
        super.onResume();
        String fpBoolean = SharedPrefs.read(SharedPrefs.FP_BOOLEAN, null);

        if (fpBoolean.equals("true")) {
            progressDialog.dismiss();
            String gNumber = SharedPrefs.read(SharedPrefs.MINI_G_NUMBER, null);

            int result = PrinterInterface.open();
            header(gNumber);
            PrinterInterface.close();
            getMiniStatement();
        } else if (fpBoolean.equals("false")){
            progressDialog.dismiss();
            Toast.makeText(this, "Please authenticate to complete transaction!", Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                finish();
                //Toast.makeText(Account.this, "Back has been presses!", Toast.LENGTH_LONG).show();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        overridePendingTransition(R.anim.move_left_in_activity, R.anim.move_right_out_activity);
    }
}
