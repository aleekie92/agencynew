package com.coretec.agencyApplication.activities.newmodules;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.coretec.agencyApplication.R;
import com.coretec.agencyApplication.activities.BalanceInquiry;
import com.coretec.agencyApplication.activities.BaseActivity;
import com.coretec.agencyApplication.activities.CashDeposit;
import com.coretec.agencyApplication.activities.CashTransfer;
import com.coretec.agencyApplication.activities.CashWithdrawal;
import com.coretec.agencyApplication.activities.MiniStatement;
import com.coretec.agencyApplication.activities.ShareDeposit;
import com.coretec.agencyApplication.api.Api;
import com.coretec.agencyApplication.api.requests.FlexibleMenuRequest;
import com.coretec.agencyApplication.api.responses.FlexibleMenuResponse;
import com.coretec.agencyApplication.utils.LogOutTimerUtil;
import com.coretec.agencyApplication.utils.SharedPrefs;
import com.coretec.agencyApplication.utils.Utils;

import org.json.JSONException;
import org.json.JSONObject;

import static com.coretec.agencyApplication.utils.Utils.getFormattedDate;
import static com.coretec.agencyApplication.utils.Utils.getLat;
import static com.coretec.agencyApplication.utils.Utils.getLong;

public class CustomerTransaction extends BaseActivity   implements  LogOutTimerUtil.LogOutListener{
    private LinearLayout liniardeposit,liniarwithdrawal,liniarcashtrans,liniarsharediposit,liniarbalanceenquiry,
            liniarministatement,liniarmicrogrouprepayment,liniaropenacc, liniarreceiptReprint;

    private  CardView cardviewdeposit,cardwithdrawal,cardcashtrans,cardsharediposit,cardbalanceenquiry,
            cardministatement,cardmicrogrouprepayment,cardopenacc, cardreceiptReprint;
    private ProgressBar transactionsmenu_progressBar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_customer_transaction);
        cardviewdeposit=findViewById(R.id.cardviewdeposit);
        cardwithdrawal=findViewById(R.id.cardwithdrawal);
        cardcashtrans=findViewById(R.id.cardcashtrans);
        cardsharediposit=findViewById(R.id.cardsharediposit);
        cardbalanceenquiry=findViewById(R.id.cardbalanceenquiry);
        cardministatement=findViewById(R.id.cardministatement);
        cardmicrogrouprepayment = findViewById(R.id.cardmicrogrouprepayment);
        cardopenacc = findViewById(R.id.cardopenacc);
        cardreceiptReprint = findViewById(R.id.cardreceiptReprint);

        liniardeposit=findViewById(R.id.liniardeposit);
        liniarwithdrawal=findViewById(R.id.liniarwithdrawal);
        liniarcashtrans=findViewById(R.id.liniarcashtrans);
        liniarsharediposit=findViewById(R.id.liniarsharediposit);
        liniarbalanceenquiry=findViewById(R.id.liniarbalanceenquiry);
        liniarministatement=findViewById(R.id.liniarministatement);
        liniarmicrogrouprepayment = findViewById(R.id.liniarmicrogrouprepayment);
        liniaropenacc = findViewById(R.id.liniaropenacc);
        liniarreceiptReprint = findViewById(R.id.liniarreceiptReprint);
        transactionsmenu_progressBar = findViewById(R.id.transactionsmenu_progressBar);

        dynamicMenu();
        cardviewdeposit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(CustomerTransaction.this, CashDeposit.class));
            }
        });

        cardwithdrawal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(CustomerTransaction.this, CashWithdrawal.class));
            }
        });

        cardcashtrans.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(CustomerTransaction.this, CashTransfer.class));
            }
        });
        cardsharediposit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(CustomerTransaction.this, ShareDeposit.class));
            }
        });
        cardbalanceenquiry.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                startActivity(new Intent(CustomerTransaction.this, BalanceInquiry.class));
            }
        });
        cardministatement.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                startActivity(new Intent(CustomerTransaction.this, MiniStatement.class));
            }
        });
        cardmicrogrouprepayment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(CustomerTransaction.this, MicroGroupRepayment.class));
            }
        });
        cardopenacc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(CustomerTransaction.this, Account_Opening.class));
            }
        });
        cardreceiptReprint.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // startActivity(new Intent(MainDashboardActivity.this, ReceiptReprint.class));
                Toast.makeText(CustomerTransaction.this, "Module under development", Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void dynamicMenu(){
        final FlexibleMenuRequest dynamicMenu = new FlexibleMenuRequest();
        dynamicMenu.agentid =SharedPrefs.read(SharedPrefs.AGENT_ID_1, null);
        dynamicMenu.terminalid = MainDashboardActivity.imei;
        dynamicMenu.longitude = getLong(CustomerTransaction.this);
        dynamicMenu.latitude = getLat(CustomerTransaction.this);
        dynamicMenu.date = getFormattedDate();
        dynamicMenu.corporate_no = MainDashboardActivity.corporateno;


        Api.instance(CustomerTransaction.this).request(Api.MSACCO_AGENT + Api.DynamicMenu, dynamicMenu, new Api.RequestListener() {
            @Override
            public void onSuccess(String response) {
                transactionsmenu_progressBar.setVisibility(View.GONE);
                FlexibleMenuResponse dynamicmenuresponse = Api.instance(CustomerTransaction.this).mGson.fromJson(response, FlexibleMenuResponse.class);
                if (dynamicmenuresponse.operationsuccess) {
                    transactionsmenu_progressBar.setVisibility(View.GONE);
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        String cashdeposit = jsonObject.getString("deposit");
                        String cashwithdrawal = jsonObject.getString("withdrawal");
                        String cashtransfer = jsonObject.getString("cashtransfer");
                        String sharedeposit = jsonObject.getString("sharedeposit");
                        String balanceinqury = jsonObject.getString("balance");
                        String ministatement = jsonObject.getString("ministatement");
                        String grouploanrepayment = jsonObject.getString("grouptransaction");
                        String accointopenning = jsonObject.getString("accountopenning");
                        String receiptreprint = jsonObject.getString("reprintreceipt");

                        if (cashdeposit=="true"){
                            liniardeposit.setVisibility(View.VISIBLE);
                        }
                        if (cashwithdrawal=="true"){
                            liniarwithdrawal.setVisibility(View.VISIBLE);
                        }
                        if (cashtransfer=="true"){
                            liniarcashtrans.setVisibility(View.VISIBLE);
                        }
                        if (sharedeposit=="true"){
                            liniarsharediposit.setVisibility(View.VISIBLE);
                        }
                        if (balanceinqury=="true"){
                            liniarbalanceenquiry.setVisibility(View.VISIBLE);
                        }
                        if (ministatement=="true"){
                            liniarministatement.setVisibility(View.VISIBLE);
                        }
                        if (grouploanrepayment=="true"){
                            liniarmicrogrouprepayment.setVisibility(View.VISIBLE);
                        }
                        if (accointopenning=="true"){
                            liniaropenacc.setVisibility(View.VISIBLE);
                        }
                        if (receiptreprint=="true"){
                            liniarreceiptReprint.setVisibility(View.VISIBLE);
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    Utils.showAlertDialog(CustomerTransaction.this, "Failed", "Menu didn't synch correctly");
                }

            }

            @Override
            public void onTokenExpired() {

            }

            @Override
            public void onError(String error) {
                Utils.showAlertDialog(CustomerTransaction.this, "Failed", error);
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
    }

    @Override
    public void onUserInteraction() {
        super.onUserInteraction();
        LogOutTimerUtil.startLogoutTimer(this, this);

    }


    @Override
    protected void onPause() {
        super.onPause();
        LogOutTimerUtil.startLogoutTimer(this, this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        LogOutTimerUtil.startLogoutTimer(this, this);
    }

    @Override
    public void doLogout() {
        //System.exit(0);
        finish();
    }
}
