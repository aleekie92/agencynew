package com.coretec.agencyApplication.activities.newmodules;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;
import com.coretec.agencyApplication.R;
import com.coretec.agencyApplication.activities.BaseActivity;
import com.coretec.agencyApplication.activities.CustomerChangePin;
import com.coretec.agencyApplication.activities.MemberActivation;
import com.coretec.agencyApplication.api.Api;
import com.coretec.agencyApplication.api.requests.FlexibleMenuRequest;
import com.coretec.agencyApplication.api.responses.FlexibleMenuResponse;
import com.coretec.agencyApplication.utils.LogOutTimerUtil;
import com.coretec.agencyApplication.utils.SharedPrefs;
import com.coretec.agencyApplication.utils.Utils;
import org.json.JSONException;
import org.json.JSONObject;
import static com.coretec.agencyApplication.utils.Utils.getFormattedDate;
import static com.coretec.agencyApplication.utils.Utils.getLat;
import static com.coretec.agencyApplication.utils.Utils.getLong;

public class RegistryActivity extends BaseActivity  implements  LogOutTimerUtil.LogOutListener{

    private CardView cardbioregistration,card_member_registration,cardmemberactivation,cardchangecustomerpin,cardmicrogroupregistration;

    private LinearLayout lobio,liniarmicrogroupregistration,liniar_member_registration,liniarchangecustomerpin,liniarmemberactivation;
    private ProgressBar registrymenu_progressBar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_registry);

        cardbioregistration=findViewById(R.id.cardbioregistration);
        card_member_registration=findViewById(R.id.card_member_registration);
        cardmemberactivation=findViewById(R.id.cardmemberactivation);
        cardchangecustomerpin=findViewById(R.id.cardchangecustomerpin);
        cardmicrogroupregistration=findViewById(R.id.cardmicrogroupregistration);

        lobio=findViewById(R.id.lobio);
        liniarmicrogroupregistration=findViewById(R.id.liniarmicrogroupregistration);
        liniar_member_registration=findViewById(R.id.liniar_member_registration);
        liniarchangecustomerpin=findViewById(R.id.liniarchangecustomerpin);
        liniarmemberactivation=findViewById(R.id.liniarmemberactivation);
        registrymenu_progressBar=findViewById(R.id.registrymenu_progressBar);

        dynamicMenu();

        cardmemberactivation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(RegistryActivity.this, MemberActivation.class));
            }
        });

        cardchangecustomerpin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(RegistryActivity.this, CustomerChangePin.class));
            }
        });


        cardmicrogroupregistration.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //startActivity(new Intent(RegistryActivity.this, Gr.class));
                Toast.makeText(RegistryActivity.this, "Under Development", Toast.LENGTH_SHORT).show();
            }
        });


        cardbioregistration.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(RegistryActivity.this, BioRegistration.class));
            }
        });

        card_member_registration.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(RegistryActivity.this, RegistrationParentActivity.class));
            }
        });
    }

    public void dynamicMenu(){
        final FlexibleMenuRequest dynamicMenu = new FlexibleMenuRequest();
        dynamicMenu.agentid = SharedPrefs.read(SharedPrefs.AGENT_ID_1, null);
        dynamicMenu.terminalid = MainDashboardActivity.imei;
        dynamicMenu.longitude = getLong(RegistryActivity.this);
        dynamicMenu.latitude = getLat(RegistryActivity.this);
        dynamicMenu.date = getFormattedDate();
        dynamicMenu.corporate_no = MainDashboardActivity.corporateno;

        Api.instance(RegistryActivity.this).request(Api.MSACCO_AGENT + Api.DynamicMenu, dynamicMenu, new Api.RequestListener() {
            @Override
            public void onSuccess(String response) {
                registrymenu_progressBar.setVisibility(View.GONE);
                FlexibleMenuResponse dynamicmenuresponse = Api.instance(RegistryActivity.this).mGson.fromJson(response, FlexibleMenuResponse.class);
                if (dynamicmenuresponse.operationsuccess) {
                    registrymenu_progressBar.setVisibility(View.GONE);
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        String memberreg = jsonObject.getString("memberregistration");
                        String memberActivation = jsonObject.getString("customerchangepin");
                        String customerchangepin = jsonObject.getString("printlasttrans");
                        String bioregistration = jsonObject.getString("bioregister");
                        String groupregistration = jsonObject.getString("groupRegistration");
                        if (memberreg=="true"){
                            liniar_member_registration.setVisibility(View.VISIBLE);
                        }
                        if (memberActivation=="true"){
                            liniarmemberactivation.setVisibility(View.VISIBLE);
                        }
                        if (customerchangepin=="true"){
                            liniarchangecustomerpin.setVisibility(View.VISIBLE);
                        }
                        if (bioregistration=="true" && SharedPrefs.read(SharedPrefs.MENU_SEPERATOR, null).equals("admin")){
                            lobio.setVisibility(View.VISIBLE);
                        }
                        if (groupregistration=="true"){
                            liniarmicrogroupregistration.setVisibility(View.VISIBLE);
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    Utils.showAlertDialog(RegistryActivity.this, "Failed", "Menu didn't synch correctly");
                    //Toast.makeText(RegistryActivity.this, accountNameResponse.getError(), Toast.LENGTH_LONG).show();
                }

            }

            @Override
            public void onTokenExpired() {

            }

            @Override
            public void onError(String error) {
                Utils.showAlertDialog(RegistryActivity.this, "Failed", error);
            }
        });
    }

    @Override
    protected void onStart() {
        super.onStart();
       // LogOutTimerUtil.startLogoutTimer(this, this);

    }

    @Override
    public void onUserInteraction() {
        super.onUserInteraction();
        LogOutTimerUtil.startLogoutTimer(this, this);

    }


    @Override
    protected void onPause() {
        super.onPause();
        LogOutTimerUtil.startLogoutTimer(this, this);

    }

    @Override
    protected void onResume() {
        super.onResume();
        LogOutTimerUtil.startLogoutTimer(this, this);

    }

    @Override
    public void doLogout() {
        finish();
    }
}
