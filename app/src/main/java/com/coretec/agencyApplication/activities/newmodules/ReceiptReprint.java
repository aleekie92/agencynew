package com.coretec.agencyApplication.activities.newmodules;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.cloudpos.jniinterface.PrinterInterface;
import com.coretec.agencyApplication.R;
import com.coretec.agencyApplication.activities.BaseActivity;
import com.coretec.agencyApplication.api.Apis;
import com.coretec.agencyApplication.utils.EnglishNumberToWords;
import com.coretec.agencyApplication.utils.SharedPrefs;
import com.coretec.agencyApplication.wizarpos.function.printer.PrinterCommand;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class ReceiptReprint extends BaseActivity {
    private EditText transaction_code;
    Button fetch;
    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment_receipt);

        this.overridePendingTransition(R.anim.move_right_in_activity, R.anim.move_left_out_activity);

        //Toolbar declarations...Reusable
        Toolbar toolbar = findViewById(R.id.toolbar);
        TextView mTitle = toolbar.findViewById(R.id.toolbar_title);
        mTitle.setText("RECEIPT REPRINT");
        setSupportActionBar(toolbar);
        ActionBar actionbar = getSupportActionBar();
        actionbar.setDisplayHomeAsUpEnabled(true);
        actionbar.setHomeAsUpIndicator(R.drawable.ic_arrow_back);

        transaction_code = findViewById(R.id.et_transaction_code);
        fetch = findViewById(R.id.button_fetch);

        String coopnumber = SharedPrefs.read(SharedPrefs.VR_COOPORATE_NO, null);
        if (coopnumber !=null){
            switch (SharedPrefs.read(SharedPrefs.VR_COOPORATE_NO, null)) {
                case "CAP005":
                    fetch.setBackgroundResource(R.drawable.btn_rounded_primary_coloraccentforcoopmis);
                    break;
                case "CAP022":
                    fetch.setBackgroundResource(R.drawable.btn_rounded_primary_coloraccentforcoopmis);
                    break;
                default:
                    fetch.setBackgroundResource(R.drawable.btn_rounded_primary_coloraccent);
                    break;
            }

        }else {
            fetch.setBackgroundResource(R.drawable.btn_rounded_primary_coloraccent);
        }

        String transactionNo ="APOS116";

        if (!transactionNo.equals("Normal")) {
            transaction_code.setText(transactionNo);
        }

        fetch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Toast.makeText(ReceiptReprint.this, "fetching", Toast.LENGTH_SHORT).show();
               /* progressDialog = new ProgressDialog(ReceiptReprint.this);
                progressDialog.setMessage("Fetching transaction...");
                progressDialog.setCancelable(false);
                progressDialog.show();
                fetchTransaction();*/
            }
        });

    }

    void fetchTransaction() {

        Apis.getVolley(ReceiptReprint.this, Apis.PrintReceipt, "{\n" +
                "    \"transactionNo\": \"" + transaction_code.getText().toString() + "\"\n" +
                "}", new Apis.VolleyCallback() {

            @Override
            public void onSuccess(String Response) {
                Log.e("RESULTS", Response);

                try {
                    JSONObject jsonObject = new JSONObject(Response);

                    Log.e("RESULTS", Response);

                    if (jsonObject.getString("error").equalsIgnoreCase("false")) {
                        //Log.e("Success Response", new Gson().toJson(jsonObject.toString()));
                        Toast.makeText(ReceiptReprint.this, jsonObject.getString("message"), Toast.LENGTH_LONG).show();

                        final JSONObject dataObject = jsonObject.getJSONObject("data");

                        final String no = dataObject.getString("no");
                        final String type = dataObject.getString("type");
                        final String date = dataObject.getString("date");
                        final String agent = dataObject.getString("agent");
                        final String device = dataObject.getString("device");

                        switch (type) {
                            case "Cash Deposit": {
                                final String amount = dataObject.getString("amount");
                                final String name = dataObject.getString("name");
                                final String account = dataObject.getString("account");
                                final String depositedBy = dataObject.getString("transactedBy");
                                final String narration = dataObject.getString("narration");
                                int result = PrinterInterface.open();
                                writeCashDepo(no, type, date, amount, agent, device, name, account, depositedBy, narration);
                                PrinterInterface.close();
                                progressDialog.dismiss();

                                break;
                            }
                            case "Cash Withdrawal": {
                                final String amount = dataObject.getString("amount");
                                String name = dataObject.getString("name");
                                String account = dataObject.getString("account");
                                String depositedBy = dataObject.getString("transactedBy");
                                String nationalID = dataObject.getString("nationalID");
                                String balance = dataObject.getString("balance");
                                int result = PrinterInterface.open();
                                writeCashWithdrawal(no, type, date, amount, agent, device, name, account, depositedBy, nationalID, balance);
                                PrinterInterface.close();
                                progressDialog.dismiss();
                                break;
                            }
                            case "Cheque Deposit": {
                                final String amount = dataObject.getString("amount");
                                String name = dataObject.getString("name");
                                String account = dataObject.getString("account");
                                String transactedBy = dataObject.getString("transactedBy");
                                String drawer = dataObject.getString("drawer");
                                String narration = dataObject.getString("narration");
                                String chequeNo = dataObject.getString("chequeNo");
                                String chequeDate = dataObject.getString("chequeDate");
                                String maturity = dataObject.getString("maturity");
                                int result = PrinterInterface.open();
                                writeChequeDeposit(no, date, amount, agent, device, name, account, transactedBy, drawer, narration, chequeNo, chequeDate, maturity);
                                PrinterInterface.close();
                                progressDialog.dismiss();
                                break;
                            }
                            case "Bankers Cheque": {
                                Double amount = dataObject.getDouble("amount");
                                String name = dataObject.getString("name");

                                if (name.equals("null")) {
                                    String account = dataObject.getString("account");
                                    String payee = dataObject.getString("payee");
                                    String chequeNo = dataObject.getString("chequeNo");
                                    String applicant = dataObject.getString("applicant");
                                    int result = PrinterInterface.open();
                                    writeBankersChequeCash(no, date, amount, agent, device, name, account, payee, chequeNo, applicant);
                                    PrinterInterface.close();
                                    progressDialog.dismiss();
                                } else {
                                    String account = dataObject.getString("account");
                                    String payee = dataObject.getString("payee");
                                    String chequeNo = dataObject.getString("chequeNo");
                                    String transactedBy = dataObject.getString("transactedBy");
                                    int result = PrinterInterface.open();
                                    writeBankersChequeAccount(no, date, amount, agent, device, name, account, payee, chequeNo, transactedBy);
                                    PrinterInterface.close();
                                    progressDialog.dismiss();
                                }
                                break;
                            }
                            case "Account Transfer": {
                                String amount = dataObject.getString("amount");
                                String source = dataObject.getString("source");
                                String destination = dataObject.getString("destination");
                                String transactedBy = dataObject.getString("transactedBy");

                                String sourceName = source.substring(source.indexOf(" "));
                                Log.e("SOURCE NAME", sourceName);
                                String destinationName = destination.substring(destination.indexOf(" "));
                                Log.e("DESTINATION NAME", destinationName);

                                if (sourceName.equals(destinationName)) {
                                    String sourceAccount = source.substring(0, source.indexOf(" "));
                                    String destinationAccount = destination.substring(0, destination.indexOf(" "));
                                    int result = PrinterInterface.open();
                                    writeTransferToSelf(no, date, amount, agent, device, sourceAccount, sourceName, destinationAccount, destinationName, transactedBy);
                                    PrinterInterface.close();
                                    progressDialog.dismiss();
                                } else {
                                    String sourceAccount = source.substring(0, source.indexOf(" "));
                                    String destinationAccount = destination.substring(0, destination.indexOf(" "));
                                    int result = PrinterInterface.open();
                                    writeTransferToOther(no, date, amount, agent, device, sourceAccount, sourceName, destinationAccount, destinationName, transactedBy);
                                    PrinterInterface.close();
                                    progressDialog.dismiss();
                                }
                                break;
                            }
                            case "Credit Receipt": {
                                String amount = dataObject.getString("amount");
                                String name = dataObject.getString("name");
                                String loanNo = dataObject.getString("loanNo");
                                String product = dataObject.getString("product");
                                String transactedBy = dataObject.getString("transactedBy");
                                String balance = "0";
                                int result = PrinterInterface.open();
                                writeCreditReceipt(no, date, amount, agent, device, name, loanNo, transactedBy, balance, product);
                                PrinterInterface.close();
                                progressDialog.dismiss();
                                break;
                            }
                            case "Cash Receipt": {
                                String amount = dataObject.getString("amount");
                                String transactedBy = dataObject.getString("transactedBy");
                                String narration = dataObject.getString("narration");
                                int result = PrinterInterface.open();
                                writeCashReceipt(no, date, amount, agent, device, transactedBy, narration);
                                PrinterInterface.close();
                                progressDialog.dismiss();
                                break;
                            }
                            default:
                                String msg = "Cannot print " + type + " at the moment. Please try another transaction type!";
                                AlertDialog.Builder builder1 = new AlertDialog.Builder(ReceiptReprint.this);
                                builder1.setMessage(msg);
                                builder1.setTitle("Alert!");
                                builder1.setCancelable(false);

                                builder1.setPositiveButton(
                                        "OK",
                                        new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int id) {
                                                dialog.cancel();
                                                progressDialog.dismiss();
                                                //onBackPressed();
                                            }
                                        });

                                AlertDialog alert11 = builder1.create();
                                alert11.show();
                                break;
                        }

                    }
                    if (jsonObject.getString("error").equalsIgnoreCase("true")) {
                        progressDialog.dismiss();
                        Toast.makeText(ReceiptReprint.this, jsonObject.getString("message"), Toast.LENGTH_LONG).show();

                        String msg = jsonObject.getString("message");
                        AlertDialog.Builder builder1 = new AlertDialog.Builder(ReceiptReprint.this);
                        builder1.setMessage(msg);
                        builder1.setTitle("Alert!");
                        builder1.setCancelable(false);

                        builder1.setPositiveButton(
                                "OK",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();
                                    }
                                });

                        AlertDialog alert11 = builder1.create();
                        alert11.show();

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

    }

    public void writeCashDepo(String no, String type, String date, String amount, String agent, String device, String name, String account,
                              String transactedBy, String narration) {

        String return_val_in_english;
        if (amount.contains(",")) {
            String firstPart = amount.substring(0, amount.indexOf(","));
            String secondPart = amount.substring(amount.lastIndexOf(",") + 1);

            String whole = firstPart + secondPart;
            int number = Integer.parseInt(whole);
            return_val_in_english = EnglishNumberToWords.convert(number);

        } else {

            int number1 = Integer.parseInt(amount);
            return_val_in_english = EnglishNumberToWords.convert(number1);

        }

        try {

            byte[] start = null;
            byte[] saccoName = null;
            byte[] arrytype = null;

            byte[] terminalNo = null;
            byte[] transaction = null;
            byte[] time = null;
            byte[] transDate = null;
            byte[] account_dposited_to = null;
            byte[] idNumber = null;
            byte[] fullNames = null;
            byte[] fName = null;
            byte[] depositedBy = null;
            byte[] arryNarration = null;

            //byte[] phoneNumber = null;
            byte[] arryDepositAmt = null;
            byte[] arryWordsHeader = null;
            byte[] arryInWords = null;

            byte[] acknowLedge = null;
            byte[] nameVerify = null;
            byte[] idVerify = null;
            byte[] signature = null;

            byte[] nameAgent = null;
            byte[] printAgent = null;
            byte[] customerReceipt = null;
            byte[] motto = null;

            SimpleDateFormat timeStampFormat = new SimpleDateFormat("dd/MM/yyyy - HH:mm");
            Date myDate = new Date();
            String timeCurrent = timeStampFormat.format(myDate);
            String aName = SharedPrefs.read(SharedPrefs.AGENT_NAME, null);

            try {
                start = String.valueOf("              ").getBytes("GB2312");
                saccoName = String.valueOf("      CAPITAL SACCO LTD     ").getBytes("GB2312");
                arrytype = String.valueOf("        CASH DEPOSIT     ").getBytes("GB2312");

                terminalNo = String.valueOf("Terminal ID : " + device).getBytes("GB2312");
                transaction = String.valueOf("Transaction No : " + no).getBytes("GB2312");
                time = String.valueOf("Print Date : " + String.valueOf(timeCurrent)).getBytes("GB2312");
                transDate = String.valueOf("Trans.Date:" + date).getBytes("GB2312");
                account_dposited_to = String.valueOf("Account NO : " + account).getBytes("GB2312");
                //idNumber = String.valueOf("ID Number : " + String.valueOf(idNo)).getBytes("GB2312");
                fullNames = String.valueOf("Customer Full Names : ").getBytes("GB2312");
                fName = String.valueOf(name).getBytes("GB2312");

                arryDepositAmt = String.valueOf("Amount : KSH" + amount).getBytes("GB2312");
                arryWordsHeader = String.valueOf("Amount in Words: ").getBytes("GB2312");
                arryInWords = String.valueOf(return_val_in_english + " Kenya Shillings Only").getBytes("GB2312");
                depositedBy = String.valueOf("Deposited By " + transactedBy).getBytes("GB2312");
                arryNarration = String.valueOf("Narration : " + narration).getBytes("GB2312");

                acknowLedge = String.valueOf("I acknowledge deposit of the above amount").getBytes("GB2312");
                nameVerify = String.valueOf("Name___________________________").getBytes("GB2312");
                idVerify = String.valueOf("ID No___________________________").getBytes("GB2312");
                signature = String.valueOf("Signature_______________________").getBytes("GB2312");

                nameAgent = String.valueOf("SERVED BY " + String.valueOf(agent)).getBytes("GB2312");
                printAgent = String.valueOf("REPRINTED BY " + String.valueOf(aName)).getBytes("GB2312");
                customerReceipt = String.valueOf("         REPRINTED COPY         ").getBytes("GB2312");

                motto = String.valueOf("         You, our Concern      ").getBytes("GB2312");

            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            PrinterInterface.begin();

            byte[] cmd;
            cmd = new byte[]{(byte) 0x1B, (byte) 0x40};

            cmd = new byte[]{
                    (byte) 0x1B, (byte) 0x45, (byte) 0};

            write(cmd);
            write(start);
            writeLineBreak(1);
            write(saccoName);
            writeLineBreak(2);
            write(arrytype);
            writeLineBreak(1);

            write(terminalNo);
            writeLineBreak(1);
            write(transaction);
            writeLineBreak(1);
            write(time);
            writeLineBreak(1);
            write(transDate);
            writeLineBreak(1);
            write(account_dposited_to);
            writeLineBreak(1);
            /*write(idNumber);
            writeLineBreak(1);*/
            write(fullNames);
            writeLineBreak(0);
            write(fName);
            writeLineBreak(2);

            write(arryDepositAmt);
            writeLineBreak(1);
            write(arryWordsHeader);
            writeLineBreak(0);
            write(arryInWords);
            writeLineBreak(1);
            write(depositedBy);
            writeLineBreak(1);
            write(arryNarration);
            writeLineBreak(2);

            write(acknowLedge);
            writeLineBreak(1);
            write(nameVerify);
            writeLineBreak(1);
            write(idVerify);
            writeLineBreak(1);
            write(signature);
            writeLineBreak(2);

            write(nameAgent);
            writeLineBreak(1);
            write(printAgent);
            writeLineBreak(2);
            write(customerReceipt);
            writeLineBreak(2);
            write(motto);
            // print line break
            writeLineBreak(4);

            PrinterInterface.end();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void writeCashWithdrawal(String no, String type, String date, String amount, String agent, String device, String name, String account,
                                    String transactedBy, String nationalId, String balance) {

        String return_val_in_english;
        if (amount.contains(",")) {
            String firstPart = amount.substring(0, amount.indexOf(","));
            String secondPart = amount.substring(amount.lastIndexOf(",") + 1);

            String whole = firstPart + secondPart;
            int number = Integer.parseInt(whole);
            return_val_in_english = EnglishNumberToWords.convert(number);

        } else {

            int number1 = Integer.parseInt(amount);
            return_val_in_english = EnglishNumberToWords.convert(number1);

        }

        /*double bal = Double.parseDouble(balance);
        String currentBal = String.format("%.2f", bal);*/

        try {

            byte[] start = null;
            byte[] saccoName = null;
            byte[] arrytype = null;

            byte[] terminalNo = null;
            byte[] transaction = null;
            byte[] time = null;
            byte[] transDate = null;
            byte[] account_dposited_to = null;
            byte[] idNumber = null;
            byte[] fullNames = null;
            byte[] fName = null;

            //byte[] phoneNumber = null;
            byte[] arryWithdrawAmt = null;
            byte[] arryBalance = null;
            byte[] arryWordsHeader = null;
            byte[] arryInWords = null;
            byte[] arrytransactedBy = null;

            byte[] acknowLedge = null;
            byte[] nameVerify = null;
            byte[] idVerify = null;
            byte[] signature = null;

            byte[] customerReceipt = null;
            byte[] nameAgent = null;
            byte[] printAgent = null;
            byte[] motto = null;

            SimpleDateFormat timeStampFormat = new SimpleDateFormat("dd/MM/yyyy - HH:mm");
            Date myDate = new Date();
            String timeCurrent = timeStampFormat.format(myDate);
            String aName = SharedPrefs.read(SharedPrefs.AGENT_NAME, null);

            try {
                start = String.valueOf("              ").getBytes("GB2312");
                saccoName = String.valueOf("       CAPITAL SACCO LTD      ").getBytes("GB2312");
                arrytype = String.valueOf("        CASH WITHDRAWAL     ").getBytes("GB2312");

                terminalNo = String.valueOf("Terminal ID : " + device).getBytes("GB2312");
                transaction = String.valueOf("Transaction No : " + no).getBytes("GB2312");
                time = String.valueOf("Print Date : " + String.valueOf(timeCurrent)).getBytes("GB2312");
                transDate = String.valueOf("Trans. Date : " + date).getBytes("GB2312");
                account_dposited_to = String.valueOf("Account NO : " + account).getBytes("GB2312");
                idNumber = String.valueOf("ID Number : " + nationalId).getBytes("GB2312");

                fullNames = String.valueOf("Customer Full Names : ").getBytes("GB2312");
                fName = String.valueOf(name).getBytes("GB2312");

                //phoneNumber = String.valueOf("Phone Number : " + String.valueOf(maskedOP)).getBytes("GB2312");
                arryWithdrawAmt = String.valueOf("Amount withdrawn : KSH " + amount).getBytes("GB2312");
                arryWordsHeader = String.valueOf("Amount in Words: ").getBytes("GB2312");
                arryInWords = String.valueOf(return_val_in_english + " Kenya Shillings Only").getBytes("GB2312");
                arryBalance = String.valueOf("Current Balance : KSH " + balance).getBytes("GB2312");
                arrytransactedBy = String.valueOf("Transacted By " + transactedBy).getBytes("GB2312");

                acknowLedge = String.valueOf("I acknowledge receipt of the above amount").getBytes("GB2312");
                nameVerify = String.valueOf("Name___________________________").getBytes("GB2312");
                idVerify = String.valueOf("ID No___________________________").getBytes("GB2312");
                signature = String.valueOf("Signature_______________________").getBytes("GB2312");

                nameAgent = String.valueOf("SERVED BY " + String.valueOf(agent)).getBytes("GB2312");
                printAgent = String.valueOf("REPRINTED BY " + String.valueOf(aName)).getBytes("GB2312");
                customerReceipt = String.valueOf("         REPRINTED COPY         ").getBytes("GB2312");

                motto = String.valueOf("         BASE FOR GROWTH       ").getBytes("GB2312");

            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            PrinterInterface.begin();

            byte[] cmd;
            cmd = new byte[]{(byte) 0x1B, (byte) 0x40};

            cmd = new byte[]{
                    (byte) 0x1B, (byte) 0x45, (byte) 0};

            write(cmd);
            write(start);
            writeLineBreak(1);
            write(saccoName);
            writeLineBreak(2);
            write(arrytype);
            writeLineBreak(1);

            write(terminalNo);
            writeLineBreak(1);
            write(transaction);
            writeLineBreak(1);
            write(time);
            writeLineBreak(1);
            write(transDate);
            writeLineBreak(1);
            write(account_dposited_to);
            writeLineBreak(1);
            write(idNumber);
            writeLineBreak(1);
            write(fullNames);
            writeLineBreak(0);
            write(fName);
            writeLineBreak(2);

            write(arryWithdrawAmt);
            writeLineBreak(1);
            write(arryWordsHeader);
            writeLineBreak(0);
            write(arryInWords);
            writeLineBreak(1);
            write(arryBalance);
            writeLineBreak(1);
            write(arrytransactedBy);
            writeLineBreak(2);

            write(acknowLedge);
            writeLineBreak(1);
            write(nameVerify);
            writeLineBreak(1);
            write(idVerify);
            writeLineBreak(1);
            write(signature);
            writeLineBreak(2);

            write(nameAgent);
            writeLineBreak(1);
            write(printAgent);
            writeLineBreak(2);
            write(customerReceipt);
            writeLineBreak(2);
            write(motto);
            // print line break
            writeLineBreak(4);

            PrinterInterface.end();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void writeChequeDeposit(String no, String date, String amount, String agent, String device, String name, String account,
                                   String transactedBy, String drawer, String narration, String chequeNo, String cheque_date, String maturity) {

        String accountNumber = "ACCOUNT NUMBER";
        String firstAc = accountNumber.substring(0, 4);
        String lastAc = accountNumber.substring(8, accountNumber.length());
        String maskedAc = firstAc + "****" + lastAc;

        String return_val_in_english;
        if (amount.contains(",")) {
            String firstPart = amount.substring(0, amount.indexOf(","));
            String secondPart = amount.substring(amount.lastIndexOf(",") + 1);

            String whole = firstPart + secondPart;
            int number = Integer.parseInt(whole);
            return_val_in_english = EnglishNumberToWords.convert(number);

        } else {

            int number1 = Integer.parseInt(amount);
            return_val_in_english = EnglishNumberToWords.convert(number1);

        }

        try {

            byte[] start = null;
            byte[] saccoName = null;
            byte[] type = null;

            byte[] terminalNo = null;
            byte[] transaction = null;
            byte[] time = null;
            byte[] transDate = null;
            byte[] account_dposited_to = null;
            byte[] idNumber = null;
            byte[] fullNames = null;
            byte[] fName = null;
            byte[] depositedBy = null;

            //byte[] phoneNumber = null;
            byte[] chequeNumber = null;
            byte[] arrydrawer = null;
            byte[] chequeDate = null;
            byte[] chqMaturityDate = null;
            //byte[] chequeType = null;
            byte[] arryDepositAmt = null;
            byte[] arryWordsHeader = null;
            byte[] arryInWords = null;

            byte[] acknowLedge = null;
            byte[] nameVerify = null;
            byte[] idVerify = null;
            byte[] signature = null;

            byte[] narration_txt = null;

            byte[] nameAgent = null;
            byte[] printAgent = null;
            byte[] customerReceipt = null;
            byte[] motto = null;

            SimpleDateFormat timeStampFormat = new SimpleDateFormat("dd/MM/yyyy - HH:mm");
            Date myDate = new Date();
            String timeCurrent = timeStampFormat.format(myDate);
            String aName = SharedPrefs.read(SharedPrefs.AGENT_NAME, null);

            try {
                start = String.valueOf("              ").getBytes("GB2312");
                saccoName = String.valueOf("     CAPITAL SACCO LTD     ").getBytes("GB2312");
                type = String.valueOf("        CHEQUE DEPOSIT     ").getBytes("GB2312");

                terminalNo = String.valueOf("Terminal ID : " + device).getBytes("GB2312");
                transaction = String.valueOf("Transaction No : " + no).getBytes("GB2312");
                time = String.valueOf("Print Date : " + String.valueOf(timeCurrent)).getBytes("GB2312");
                transDate = String.valueOf("Trans. Date : " + date).getBytes("GB2312");
                account_dposited_to = String.valueOf("Account NO : " + account).getBytes("GB2312");
                //idNumber = String.valueOf("ID Number : " + String.valueOf(idNo)).getBytes("GB2312");
                fullNames = String.valueOf("Customer Full Names : ").getBytes("GB2312");
                fName = String.valueOf(name).getBytes("GB2312");

                //phoneNumber = String.valueOf("Phone Number : " + String.valueOf(maskedOP)).getBytes("GB2312");
                chequeNumber = String.valueOf("Cheque Number : " + chequeNo).getBytes("GB2312");
                arrydrawer = String.valueOf("Drawer : " + drawer).getBytes("GB2312");
                chequeDate = String.valueOf("Cheque Date : " + cheque_date).getBytes("GB2312");
                chqMaturityDate = String.valueOf("Chq Maturity Date : " + maturity).getBytes("GB2312");
                arryDepositAmt = String.valueOf("Amount : KSH" + amount).getBytes("GB2312");
                arryWordsHeader = String.valueOf("Amount in Words: ").getBytes("GB2312");
                arryInWords = String.valueOf(return_val_in_english + " Kenya Shillings Only").getBytes("GB2312");

                narration_txt = String.valueOf("Narration : " + narration).getBytes("GB2312");
                depositedBy = String.valueOf("Deposited By " + transactedBy).getBytes("GB2312");

                acknowLedge = String.valueOf("I acknowledge deposit of the above amount").getBytes("GB2312");
                nameVerify = String.valueOf("Name___________________________").getBytes("GB2312");
                idVerify = String.valueOf("ID No___________________________").getBytes("GB2312");
                signature = String.valueOf("Signature_______________________").getBytes("GB2312");

                nameAgent = String.valueOf("SERVED BY " + String.valueOf(agent)).getBytes("GB2312");
                printAgent = String.valueOf("REPRINTED BY " + String.valueOf(aName)).getBytes("GB2312");
                customerReceipt = String.valueOf("         REPRINTED COPY         ").getBytes("GB2312");
                motto = String.valueOf("         BASE FOR GROWTH        ").getBytes("GB2312");

            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            PrinterInterface.begin();

            byte[] cmd;
            cmd = new byte[]{(byte) 0x1B, (byte) 0x40};

            cmd = new byte[]{
                    (byte) 0x1B, (byte) 0x45, (byte) 0};

            write(cmd);
            write(start);
            writeLineBreak(1);
            write(saccoName);
            writeLineBreak(2);
            write(type);
            writeLineBreak(1);


            write(terminalNo);
            writeLineBreak(1);
            write(transaction);
            writeLineBreak(1);
            write(time);
            writeLineBreak(1);
            write(transDate);
            writeLineBreak(1);
            write(account_dposited_to);
            writeLineBreak(1);
            write(fullNames);
            writeLineBreak(0);
            write(fName);
            writeLineBreak(2);

            write(chequeNumber);
            writeLineBreak(1);
            write(arrydrawer);
            writeLineBreak(1);
            write(chequeDate);
            writeLineBreak(1);
            write(chqMaturityDate);
            writeLineBreak(1);
            write(arryDepositAmt);
            writeLineBreak(1);
            write(arryWordsHeader);
            writeLineBreak(0);
            write(arryInWords);
            writeLineBreak(2);

            write(narration_txt);
            writeLineBreak(1);
            write(depositedBy);
            writeLineBreak(2);

            write(acknowLedge);
            writeLineBreak(1);
            write(nameVerify);
            writeLineBreak(1);
            write(idVerify);
            writeLineBreak(1);
            write(signature);
            writeLineBreak(2);

            write(nameAgent);
            writeLineBreak(1);
            write(printAgent);
            writeLineBreak(2);
            write(customerReceipt);
            writeLineBreak(2);
            write(motto);
            // print line break
            writeLineBreak(4);

            PrinterInterface.end();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void writeBankersChequeCash(String no, String date, Double amount, String agent, String device, String name, String account,
                                       String payee, String chequeNo, String applicant) {
        //String the_amount = String.valueOf(amount);
        String the_amount = String.format("%.2f", amount);
        String return_val_in_english;

        if (the_amount.contains(".")) {
            String firstPart = the_amount.substring(0, the_amount.indexOf("."));

            String decimalPart = the_amount.substring(the_amount.lastIndexOf(".") + 1);

            int wholeNo = Integer.parseInt(firstPart);
            int cents = Integer.parseInt(decimalPart);

            String whole_value = EnglishNumberToWords.convert(wholeNo);
            String cent_value = EnglishNumberToWords.convert(cents);
            return_val_in_english = whole_value + " Kenya Shillings and " + cent_value + " cents";

        } else {

            int number = Integer.parseInt(the_amount);

            return_val_in_english = EnglishNumberToWords.convert(number);

        }

        try {

            byte[] start = null;
            byte[] saccoName = null;
            byte[] type = null;

            byte[] terminalNo = null;
            byte[] transaction = null;
            byte[] time = null;
            byte[] transDate = null;
            byte[] idNumber = null;
            byte[] fullNames = null;
            byte[] fName = null;
            byte[] arryChqNo = null;
            byte[] arryPayee = null;

            //byte[] phoneNumber = null;
            byte[] arryWithdrawAmt = null;
            byte[] arryWordsHeader = null;
            byte[] arryInWords = null;
            byte[] depositedBy = null;

            byte[] acknowLedge = null;
            byte[] nameVerify = null;
            byte[] idVerify = null;
            byte[] signature = null;

            byte[] customerReceipt = null;
            byte[] nameAgent = null;
            byte[] printAgent = null;
            byte[] motto = null;

            SimpleDateFormat timeStampFormat = new SimpleDateFormat("dd/MM/yyyy - HH:mm");
            Date myDate = new Date();
            String timeCurrent = timeStampFormat.format(myDate);
            String aName = SharedPrefs.read(SharedPrefs.AGENT_NAME, null);
            //String cheque_no = SharedPrefs.read(SharedPrefs.CHEQUE_NUMBER, null);

            try {
                start = String.valueOf("              ").getBytes("GB2312");
                saccoName = String.valueOf("      CAPITAL SACCO LTD      ").getBytes("GB2312");
                type = String.valueOf("        BANKER CHEQUE     ").getBytes("GB2312");

                terminalNo = String.valueOf("Terminal ID : " + device).getBytes("GB2312");
                transaction = String.valueOf("Transaction No : " + no).getBytes("GB2312");
                time = String.valueOf("Print Date : " + String.valueOf(timeCurrent)).getBytes("GB2312");
                transDate = String.valueOf("Trans. Date : " + date).getBytes("GB2312");
                //account_dposited_to = String.valueOf("Account NO : " + account_number).getBytes("GB2312");

                fullNames = String.valueOf("Applicant Names : ").getBytes("GB2312");
                fName = String.valueOf(applicant).getBytes("GB2312");
                arryChqNo = String.valueOf("Cheque Number : " + chequeNo).getBytes("GB2312");
                arryPayee = String.valueOf("Payee :" + payee).getBytes("GB2312");

                //phoneNumber = String.valueOf("Phone Number : " + String.valueOf(maskedOP)).getBytes("GB2312");
                arryWithdrawAmt = String.valueOf("Amount : KSH " + the_amount).getBytes("GB2312");
                arryWordsHeader = String.valueOf("Amount in Words: ").getBytes("GB2312");
                arryInWords = String.valueOf(return_val_in_english).getBytes("GB2312");
                //depositedBy = String.valueOf("Deposited By " + transacted_by.getText().toString()).getBytes("GB2312");

                acknowLedge = String.valueOf("I acknowledge receipt of the banker cheque of the above amount").getBytes("GB2312");
                nameVerify = String.valueOf("Name___________________________").getBytes("GB2312");
                idVerify = String.valueOf("ID No___________________________").getBytes("GB2312");
                signature = String.valueOf("Signature_______________________").getBytes("GB2312");

                nameAgent = String.valueOf("SERVED BY " + String.valueOf(agent)).getBytes("GB2312");
                printAgent = String.valueOf("REPRINTED BY " + String.valueOf(aName)).getBytes("GB2312");
                customerReceipt = String.valueOf("         REPRINTED COPY         ").getBytes("GB2312");

                motto = String.valueOf("        BASE FOR GROWTH      ").getBytes("GB2312");

            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            PrinterInterface.begin();

            byte[] cmd;
            cmd = new byte[]{(byte) 0x1B, (byte) 0x40};

            cmd = new byte[]{
                    (byte) 0x1B, (byte) 0x45, (byte) 0};

            write(cmd);
            write(start);
            writeLineBreak(1);
            write(saccoName);
            writeLineBreak(2);
            write(type);
            writeLineBreak(1);

            write(terminalNo);
            writeLineBreak(1);
            write(transaction);
            writeLineBreak(1);
            write(time);
            writeLineBreak(1);
            write(transDate);
            writeLineBreak(1);
            write(fullNames);
            writeLineBreak(0);
            write(fName);
            writeLineBreak(1);
            write(arryChqNo);
            writeLineBreak(1);
            write(arryPayee);
            writeLineBreak(2);

            write(arryWithdrawAmt);
            writeLineBreak(1);
            write(arryWordsHeader);
            writeLineBreak(0);
            write(arryInWords);
            writeLineBreak(2);

            write(acknowLedge);
            writeLineBreak(1);
            write(nameVerify);
            writeLineBreak(1);
            write(idVerify);
            writeLineBreak(1);
            write(signature);
            writeLineBreak(2);

            write(nameAgent);
            writeLineBreak(1);
            write(printAgent);
            writeLineBreak(2);
            write(customerReceipt);
            writeLineBreak(2);
            write(motto);
            // print line break
            writeLineBreak(4);

            PrinterInterface.end();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void writeBankersChequeAccount(String no, String date, Double amount, String agent, String device, String name, String account,
                                          String payee, String chequeNo, String transactedBy) {
        //String _amount = String.valueOf(amount);
        String the_amount = String.format("%.2f", amount);
        Log.e("AMOUNT", the_amount);
        String return_val_in_english;

        if (the_amount.contains(".")) {
            String firstPart = the_amount.substring(0, the_amount.indexOf("."));

            String decimalPart = the_amount.substring(the_amount.lastIndexOf(".") + 1);

            int wholeNo = Integer.parseInt(firstPart);
            int cents = Integer.parseInt(decimalPart);

            String whole_value = EnglishNumberToWords.convert(wholeNo);
            String cent_value = EnglishNumberToWords.convert(cents);
            return_val_in_english = whole_value + " Kenya Shillings and " + cent_value + " cents";

        } else {

            int number = Integer.parseInt(the_amount);

            return_val_in_english = EnglishNumberToWords.convert(number);

        }

        try {

            byte[] start = null;
            byte[] saccoName = null;
            byte[] type = null;

            byte[] terminalNo = null;
            byte[] transaction = null;
            byte[] time = null;
            byte[] transDate = null;
            byte[] idNumber = null;
            byte[] fullNames = null;
            byte[] fName = null;
            byte[] arryChqNo = null;
            byte[] arryPayee = null;

            //byte[] phoneNumber = null;
            byte[] arryWithdrawAmt = null;
            byte[] arryWordsHeader = null;
            byte[] arryInWords = null;
            byte[] depositedBy = null;

            byte[] acknowLedge = null;
            byte[] nameVerify = null;
            byte[] idVerify = null;
            byte[] signature = null;

            byte[] customerReceipt = null;
            byte[] nameAgent = null;
            byte[] printAgent = null;
            byte[] motto = null;

            SimpleDateFormat timeStampFormat = new SimpleDateFormat("dd/MM/yyyy - HH:mm");
            Date myDate = new Date();
            String timeCurrent = timeStampFormat.format(myDate);
            String aName = SharedPrefs.read(SharedPrefs.AGENT_NAME, null);
            //String cheque_no = SharedPrefs.read(SharedPrefs.CHEQUE_NUMBER, null);

            try {
                start = String.valueOf("              ").getBytes("GB2312");
                saccoName = String.valueOf("     CAPITAL SACCO LTD     ").getBytes("GB2312");
                type = String.valueOf("        BANKER CHEQUE     ").getBytes("GB2312");

                terminalNo = String.valueOf("Terminal ID : " + device).getBytes("GB2312");
                transaction = String.valueOf("Transaction No : " + no).getBytes("GB2312");
                time = String.valueOf("Print Date : " + String.valueOf(timeCurrent)).getBytes("GB2312");
                transDate = String.valueOf("Trans. Date : " + date).getBytes("GB2312");
                //account_dposited_to = String.valueOf("Account NO : " + account_number).getBytes("GB2312");

                fullNames = String.valueOf("Applicant Names : ").getBytes("GB2312");
                fName = String.valueOf(name).getBytes("GB2312");
                arryChqNo = String.valueOf("Cheque Number : " + chequeNo).getBytes("GB2312");
                arryPayee = String.valueOf("Payee :" + payee).getBytes("GB2312");

                //phoneNumber = String.valueOf("Phone Number : " + String.valueOf(maskedOP)).getBytes("GB2312");
                arryWithdrawAmt = String.valueOf("Amount : KSH " + the_amount).getBytes("GB2312");
                arryWordsHeader = String.valueOf("Amount in Words: ").getBytes("GB2312");
                arryInWords = String.valueOf(return_val_in_english).getBytes("GB2312");
                depositedBy = String.valueOf("Deposited By " + transactedBy).getBytes("GB2312");

                acknowLedge = String.valueOf("I acknowledge receipt of the banker cheque of the above amount").getBytes("GB2312");
                nameVerify = String.valueOf("Name___________________________").getBytes("GB2312");
                idVerify = String.valueOf("ID No___________________________").getBytes("GB2312");
                signature = String.valueOf("Signature_______________________").getBytes("GB2312");

                nameAgent = String.valueOf("SERVED BY " + String.valueOf(agent)).getBytes("GB2312");
                printAgent = String.valueOf("REPRINTED BY " + String.valueOf(aName)).getBytes("GB2312");
                customerReceipt = String.valueOf("         REPRINTED COPY         ").getBytes("GB2312");

                motto = String.valueOf("         BASE FOR GROWTH       ").getBytes("GB2312");

            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            PrinterInterface.begin();

            byte[] cmd;
            cmd = new byte[]{(byte) 0x1B, (byte) 0x40};

            cmd = new byte[]{
                    (byte) 0x1B, (byte) 0x45, (byte) 0};

            write(cmd);
            write(start);
            writeLineBreak(1);
            write(saccoName);
            writeLineBreak(2);
            write(type);
            writeLineBreak(1);

            write(terminalNo);
            writeLineBreak(1);
            write(transaction);
            writeLineBreak(1);
            write(time);
            writeLineBreak(1);
            write(transDate);
            writeLineBreak(1);
            write(fullNames);
            writeLineBreak(0);
            write(fName);
            writeLineBreak(1);
            write(arryChqNo);
            writeLineBreak(1);
            write(arryPayee);
            writeLineBreak(2);

            write(arryWithdrawAmt);
            writeLineBreak(1);
            write(arryWordsHeader);
            writeLineBreak(0);
            write(arryInWords);
            writeLineBreak(2);
            write(depositedBy);
            writeLineBreak(2);

            write(acknowLedge);
            writeLineBreak(1);
            write(nameVerify);
            writeLineBreak(1);
            write(idVerify);
            writeLineBreak(1);
            write(signature);
            writeLineBreak(2);

            write(nameAgent);
            writeLineBreak(1);
            write(printAgent);
            writeLineBreak(2);
            write(customerReceipt);
            writeLineBreak(2);
            write(motto);
            // print line break
            writeLineBreak(4);

            PrinterInterface.end();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void writeTransferToSelf(String no, String date, String amount, String agent, String device, String sourceAc, String sourceName,
                                    String destinationAc, String destinationName, String transacted_by) {

        String return_val_in_english;
        if (amount.contains(",")) {
            String firstPart = amount.substring(0, amount.indexOf(","));
            String secondPart = amount.substring(amount.lastIndexOf(",") + 1);

            String whole = firstPart + secondPart;
            int number = Integer.parseInt(whole);
            return_val_in_english = EnglishNumberToWords.convert(number);

        } else {

            int number1 = Integer.parseInt(amount);
            return_val_in_english = EnglishNumberToWords.convert(number1);

        }

        try {

            byte[] start = null;
            byte[] saccoName = null;
            byte[] type = null;

            byte[] terminalNo = null;
            byte[] transaction = null;
            byte[] time = null;
            byte[] transDate = null;
            byte[] account_from = null;
            byte[] from_name = null;
            byte[] account_to = null;
            byte[] to_name = null;
            byte[] fullNames = null;
            byte[] fName = null;

            //byte[] phoneNumber = null;
            byte[] arryDepositAmt = null;
            byte[] arryWordsHeader = null;
            byte[] arryInWords = null;
            byte[] transactedBy = null;

            byte[] acknowLedge = null;
            byte[] nameVerify = null;
            byte[] idVerify = null;
            byte[] signature = null;

            byte[] nameAgent = null;
            byte[] printAgent = null;
            byte[] customerReceipt = null;
            byte[] motto = null;

            SimpleDateFormat timeStampFormat = new SimpleDateFormat("dd/MM/yyyy - HH:mm");
            Date myDate = new Date();
            String timeCurrent = timeStampFormat.format(myDate);
            String aName = SharedPrefs.read(SharedPrefs.AGENT_NAME, null);
            //String depositorsName = deposited_by.getText().toString();

            try {
                start = String.valueOf("              ").getBytes("GB2312");
                saccoName = String.valueOf("      CAPITAL SACCO LTD      ").getBytes("GB2312");
                type = String.valueOf("      TRANSFER TO SELF     ").getBytes("GB2312");

                terminalNo = String.valueOf("Terminal ID : " + device).getBytes("GB2312");
                transaction = String.valueOf("Transaction No : " + no).getBytes("GB2312");
                time = String.valueOf("Print Date : " + String.valueOf(timeCurrent)).getBytes("GB2312");
                transDate = String.valueOf("Trans. Date : " + date).getBytes("GB2312");
                account_from = String.valueOf("Account From : " + sourceAc).getBytes("GB2312");
                from_name = String.valueOf(" Name : " + sourceName).getBytes("GB2312");
                account_to = String.valueOf("Account To : " + destinationAc).getBytes("GB2312");
                to_name = String.valueOf(" Name : " +  destinationName).getBytes("GB2312");

                arryDepositAmt = String.valueOf("Transferred Amount : KSH " + amount).getBytes("GB2312");
                arryWordsHeader = String.valueOf("Amount in Words: ").getBytes("GB2312");
                arryInWords = String.valueOf(return_val_in_english + " Kenya Shillings Only").getBytes("GB2312");
                transactedBy = String.valueOf("Transacted By: " + transacted_by).getBytes("GB2312");

                acknowLedge = String.valueOf("I acknowledge transfer of the above amount").getBytes("GB2312");
                nameVerify = String.valueOf("Name___________________________").getBytes("GB2312");
                idVerify = String.valueOf("ID No___________________________").getBytes("GB2312");
                signature = String.valueOf("Signature_______________________").getBytes("GB2312");

                nameAgent = String.valueOf("SERVED BY " + String.valueOf(agent)).getBytes("GB2312");
                printAgent = String.valueOf("REPRINTED BY " + String.valueOf(aName)).getBytes("GB2312");
                customerReceipt = String.valueOf("         REPRINTED COPY         ").getBytes("GB2312");

                motto = String.valueOf("        BASE FOR GROWTH       ").getBytes("GB2312");

            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            PrinterInterface.begin();

            byte[] cmd;
            cmd = new byte[]{(byte) 0x1B, (byte) 0x40};

            cmd = new byte[]{
                    (byte) 0x1B, (byte) 0x45, (byte) 0};

            write(cmd);
            write(start);
            writeLineBreak(1);
            write(saccoName);
            writeLineBreak(2);
            write(type);
            writeLineBreak(1);

            write(terminalNo);
            writeLineBreak(1);
            write(transaction);
            writeLineBreak(1);
            write(time);
            writeLineBreak(1);
            write(transDate);
            writeLineBreak(1);
            write(account_from);
            writeLineBreak(0);
            write(from_name);
            writeLineBreak(1);
            write(account_to);
            writeLineBreak(0);
            write(to_name);
            writeLineBreak(2);
            /*write(fullNames);
            writeLineBreak(0);
            write(fName);
            writeLineBreak(2);*/

            write(arryDepositAmt);
            writeLineBreak(1);
            write(arryWordsHeader);
            writeLineBreak(0);
            write(arryInWords);
            writeLineBreak(1);
            write(transactedBy);
            writeLineBreak(1);

            write(acknowLedge);
            writeLineBreak(1);
            write(nameVerify);
            writeLineBreak(1);
            write(idVerify);
            writeLineBreak(1);
            write(signature);
            writeLineBreak(2);

            write(nameAgent);
            writeLineBreak(1);
            write(printAgent);
            writeLineBreak(2);
            write(customerReceipt);
            writeLineBreak(2);
            write(motto);
            // print line break
            writeLineBreak(4);

            PrinterInterface.end();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void writeTransferToOther(String no, String date, String amount, String agent, String device, String sourceAc, String sourceName,
                                     String destinationAc, String destinationName, String transacted_by) {

        String return_val_in_english;
        if (amount.contains(",")) {
            String firstPart = amount.substring(0, amount.indexOf(","));
            String secondPart = amount.substring(amount.lastIndexOf(",") + 1);

            String whole = firstPart + secondPart;
            int number = Integer.parseInt(whole);
            return_val_in_english = EnglishNumberToWords.convert(number);

        } else {

            int number1 = Integer.parseInt(amount);
            return_val_in_english = EnglishNumberToWords.convert(number1);

        }

        try {

            byte[] start = null;
            byte[] saccoName = null;
            byte[] type = null;

            byte[] terminalNo = null;
            byte[] transaction = null;
            byte[] time = null;
            byte[] transDate = null;
            byte[] account_from = null;
            byte[] from_name = null;
            byte[] account_to = null;
            byte[] to_name = null;
            byte[] fullNames = null;
            byte[] fName = null;

            //byte[] phoneNumber = null;
            byte[] arryDepositAmt = null;
            byte[] arryWordsHeader = null;
            byte[] arryInWords = null;
            byte[] transactedBy = null;

            byte[] acknowLedge = null;
            byte[] nameVerify = null;
            byte[] idVerify = null;
            byte[] signature = null;

            byte[] nameAgent = null;
            byte[] printAgent = null;
            byte[] customerReceipt = null;
            byte[] motto = null;

            SimpleDateFormat timeStampFormat = new SimpleDateFormat("dd/MM/yyyy - HH:mm");
            Date myDate = new Date();
            String timeCurrent = timeStampFormat.format(myDate);
            String aName = SharedPrefs.read(SharedPrefs.AGENT_NAME, null);
            //String depositorsName = deposited_by.getText().toString();

            try {
                start = String.valueOf("              ").getBytes("GB2312");
                saccoName = String.valueOf("     CAPITAL SACCO LTD      ").getBytes("GB2312");
                type = String.valueOf("      TRANSFER TO OTHER     ").getBytes("GB2312");

                terminalNo = String.valueOf("Terminal ID : " + device).getBytes("GB2312");
                transaction = String.valueOf("Transaction No : " + no).getBytes("GB2312");
                time = String.valueOf("Print Date : " + String.valueOf(timeCurrent)).getBytes("GB2312");
                transDate = String.valueOf("Trans. Date : " + date).getBytes("GB2312");
                account_from = String.valueOf("Account From : " + sourceAc).getBytes("GB2312");
                from_name = String.valueOf(" Name : " + sourceName).getBytes("GB2312");
                account_to = String.valueOf("Account To : " + destinationAc).getBytes("GB2312");
                to_name = String.valueOf(" Name : " +  destinationName).getBytes("GB2312");

                arryDepositAmt = String.valueOf("Transferred Amount : KSH " + amount).getBytes("GB2312");
                arryWordsHeader = String.valueOf("Amount in Words: ").getBytes("GB2312");
                arryInWords = String.valueOf(return_val_in_english + " Kenya Shillings Only").getBytes("GB2312");
                transactedBy = String.valueOf("Transacted By: " + transacted_by).getBytes("GB2312");

                acknowLedge = String.valueOf("I acknowledge transfer of the above amount").getBytes("GB2312");
                nameVerify = String.valueOf("Name___________________________").getBytes("GB2312");
                idVerify = String.valueOf("ID No___________________________").getBytes("GB2312");
                signature = String.valueOf("Signature_______________________").getBytes("GB2312");

                nameAgent = String.valueOf("SERVED BY " + String.valueOf(agent)).getBytes("GB2312");
                printAgent = String.valueOf("REPRINTED BY " + String.valueOf(aName)).getBytes("GB2312");
                customerReceipt = String.valueOf("         REPRINTED COPY         ").getBytes("GB2312");

                motto = String.valueOf("         BASE FOR GROWTH       ").getBytes("GB2312");

            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            PrinterInterface.begin();

            byte[] cmd;
            cmd = new byte[]{(byte) 0x1B, (byte) 0x40};

            cmd = new byte[]{
                    (byte) 0x1B, (byte) 0x45, (byte) 0};

            write(cmd);
            write(start);
            writeLineBreak(1);
            write(saccoName);
            writeLineBreak(2);
            write(type);
            writeLineBreak(1);

            write(terminalNo);
            writeLineBreak(1);
            write(transaction);
            writeLineBreak(1);
            write(time);
            writeLineBreak(1);
            write(transDate);
            writeLineBreak(1);
            write(account_from);
            writeLineBreak(0);
            write(from_name);
            writeLineBreak(1);
            write(account_to);
            writeLineBreak(0);
            write(to_name);
            writeLineBreak(2);
            /*write(fullNames);
            writeLineBreak(0);
            write(fName);
            writeLineBreak(2);*/

            write(arryDepositAmt);
            writeLineBreak(1);
            write(arryWordsHeader);
            writeLineBreak(0);
            write(arryInWords);
            writeLineBreak(1);
            write(transactedBy);
            writeLineBreak(1);

            write(acknowLedge);
            writeLineBreak(1);
            write(nameVerify);
            writeLineBreak(1);
            write(idVerify);
            writeLineBreak(1);
            write(signature);
            writeLineBreak(2);

            write(nameAgent);
            writeLineBreak(1);
            write(printAgent);
            writeLineBreak(2);
            write(customerReceipt);
            writeLineBreak(2);
            write(motto);
            // print line break
            writeLineBreak(4);

            PrinterInterface.end();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void writeCreditReceipt(String no, String date, String amount, String agent, String device, String name,
                                   String loanNo, String transactedBy, String balance, String product) {

        try {

            byte[] start = null;
            byte[] saccoName = null;
            byte[] type = null;

            byte[] terminalNo = null;
            byte[] transaction = null;
            byte[] time = null;
            byte[] transDate = null;
            byte[] loan_number = null;
            byte[] idNumber = null;
            byte[] fullNames = null;
            byte[] fName = null;
            byte[] depositedBy = null;

            byte[] arryDepositAmt = null;
            byte[] arryWordsHeader = null;
            byte[] arryInWords = null;

            byte[] acknowLedge = null;
            byte[] nameVerify = null;
            byte[] idVerify = null;
            byte[] signature = null;

            byte[] nameAgent = null;
            byte[] printAgent = null;
            byte[] customerReceipt = null;
            byte[] motto = null;

            SimpleDateFormat timeStampFormat = new SimpleDateFormat("dd/MM/yyyy - HH:mm");
            Date myDate = new Date();
            String timeCurrent = timeStampFormat.format(myDate);
            String aName = SharedPrefs.read(SharedPrefs.AGENT_NAME, null);
            //String depositorsName = deposited_by.getText().toString();

            String return_val_in_english;
            if (amount.contains(",")) {
                String firstPart = amount.substring(0, amount.indexOf(","));
                String secondPart = amount.substring(amount.lastIndexOf(",") + 1);

                String whole = firstPart + secondPart;
                int number = Integer.parseInt(whole);
                return_val_in_english = EnglishNumberToWords.convert(number);

            } else {

                int number1 = Integer.parseInt(amount);
                return_val_in_english = EnglishNumberToWords.convert(number1);

            }

            try {
                start = String.valueOf("              ").getBytes("GB2312");
                saccoName = String.valueOf("        CAPITAL SACCO LTD     ").getBytes("GB2312");
                type = String.valueOf("      CASH CREDIT RECEIPT     ").getBytes("GB2312");

                terminalNo = String.valueOf("Terminal ID : " + device).getBytes("GB2312");
                transaction = String.valueOf("Transaction No : " + no).getBytes("GB2312");
                time = String.valueOf("Print Date : " + String.valueOf(timeCurrent)).getBytes("GB2312");
                transDate = String.valueOf("Trans. Date : " + date).getBytes("GB2312");
                fullNames = String.valueOf("Customer Full Names : ").getBytes("GB2312");
                fName = String.valueOf(name).getBytes("GB2312");
                loan_number = String.valueOf("Loan Number : " + loanNo).getBytes("GB2312");
                idNumber = String.valueOf("Loan Product Name : " + product).getBytes("GB2312");

                arryDepositAmt = String.valueOf("Amount : KSH" + amount).getBytes("GB2312");
                arryWordsHeader = String.valueOf("Amount in Words: ").getBytes("GB2312");
                arryInWords = String.valueOf(return_val_in_english +" Kenya Shillings Only").getBytes("GB2312");
                depositedBy = String.valueOf("Loan Paid By " + transactedBy).getBytes("GB2312");

                acknowLedge = String.valueOf("I acknowledge the transaction of the above amount").getBytes("GB2312");
                nameVerify = String.valueOf("Name___________________________").getBytes("GB2312");
                idVerify = String.valueOf("ID No___________________________").getBytes("GB2312");
                signature = String.valueOf("Signature_______________________").getBytes("GB2312");

                nameAgent = String.valueOf(" SERVED BY " + String.valueOf(agent)).getBytes("GB2312");
                printAgent = String.valueOf(" REPRINTED BY " + String.valueOf(aName)).getBytes("GB2312");
                customerReceipt = String.valueOf("         REPRINTED COPY         ").getBytes("GB2312");
                motto = String.valueOf("         BASE FOR GROWTH      ").getBytes("GB2312");

            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            PrinterInterface.begin();

            byte[] cmd;
            cmd = new byte[]{(byte) 0x1B, (byte) 0x40};

            cmd = new byte[]{
                    (byte) 0x1B, (byte) 0x45, (byte) 0};

            write(cmd);
            write(start);
            writeLineBreak(1);
            write(saccoName);
            writeLineBreak(2);
            write(type);
            writeLineBreak(1);

            write(terminalNo);
            writeLineBreak(1);
            write(transaction);
            writeLineBreak(1);
            write(time);
            writeLineBreak(1);
            write(transDate);
            writeLineBreak(1);
            write(fullNames);
            writeLineBreak(0);
            write(fName);
            writeLineBreak(1);
            write(loan_number);
            writeLineBreak(1);
            write(idNumber);
            writeLineBreak(2);

            write(arryDepositAmt);
            writeLineBreak(1);
            write(arryWordsHeader);
            writeLineBreak(0);
            write(arryInWords);
            writeLineBreak(1);
            write(depositedBy);
            writeLineBreak(2);

            write(acknowLedge);
            writeLineBreak(1);
            write(nameVerify);
            writeLineBreak(1);
            write(idVerify);
            writeLineBreak(1);
            write(signature);
            writeLineBreak(2);

            write(nameAgent);
            writeLineBreak(1);
            write(printAgent);
            writeLineBreak(2);
            write(customerReceipt);
            writeLineBreak(2);
            write(motto);
            // print line break
            writeLineBreak(4);

            PrinterInterface.end();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void writeCashReceipt(String no, String date, String amount, String agent, String device, String transactedBy, String narration) {

        String return_val_in_english;
        if (amount.contains(",")) {
            String firstPart = amount.substring(0, amount.indexOf(","));
            String secondPart = amount.substring(amount.lastIndexOf(",") + 1);

            String whole = firstPart + secondPart;
            int number = Integer.parseInt(whole);
            return_val_in_english = EnglishNumberToWords.convert(number);

        } else {

            int number1 = Integer.parseInt(amount);
            return_val_in_english = EnglishNumberToWords.convert(number1);

        }

        try {

            byte[] start = null;
            byte[] saccoName = null;
            byte[] type = null;

            byte[] terminalNo = null;
            byte[] transaction = null;
            byte[] time = null;
            byte[] transDate = null;
            byte[] account_dposited_to = null;
            byte[] idNumber = null;
            byte[] fullNames = null;
            byte[] fName = null;
            byte[] depositedBy = null;
            byte[] arryNarration = null;

            //byte[] phoneNumber = null;
            byte[] arryDepositAmt = null;
            byte[] arryWordsHeader = null;
            byte[] arryInWords = null;

            byte[] acknowLedge = null;
            byte[] nameVerify = null;
            byte[] idVerify = null;
            byte[] signature = null;

            byte[] nameAgent = null;
            byte[] printAgent = null;
            byte[] customerReceipt = null;
            byte[] motto = null;

            SimpleDateFormat timeStampFormat = new SimpleDateFormat("dd/MM/yyyy - HH:mm");
            Date myDate = new Date();
            String timeCurrent = timeStampFormat.format(myDate);
            String aName = SharedPrefs.read(SharedPrefs.AGENT_NAME, null);
            //String depositorsName = deposited_by.getText().toString();

            try {
                start = String.valueOf("              ").getBytes("GB2312");
                saccoName = String.valueOf("     CAPITAL SACCO LTD     ").getBytes("GB2312");
                type = String.valueOf("        CASH RECEIPT     ").getBytes("GB2312");

                terminalNo = String.valueOf("Terminal ID : " + device).getBytes("GB2312");
                transaction = String.valueOf("Transaction No : " + no).getBytes("GB2312");
                time = String.valueOf("Print Date : " + String.valueOf(timeCurrent)).getBytes("GB2312");
                transDate = String.valueOf("Trans. Date : " + date).getBytes("GB2312");

                arryDepositAmt = String.valueOf("Amount : KSH" + amount).getBytes("GB2312");
                arryWordsHeader = String.valueOf("Amount in Words: ").getBytes("GB2312");
                arryInWords = String.valueOf(return_val_in_english + " Kenya Shillings Only").getBytes("GB2312");
                depositedBy = String.valueOf("Transacted By " + transactedBy).getBytes("GB2312");
                arryNarration = String.valueOf("Narration : " + narration).getBytes("GB2312");

                acknowLedge = String.valueOf("I acknowledge payment of the above amount").getBytes("GB2312");
                nameVerify = String.valueOf("Name___________________________").getBytes("GB2312");
                idVerify = String.valueOf("ID No___________________________").getBytes("GB2312");
                signature = String.valueOf("Signature_______________________").getBytes("GB2312");

                nameAgent = String.valueOf("SERVED BY " + String.valueOf(aName)).getBytes("GB2312");
                printAgent = String.valueOf("REPRINTED BY " + String.valueOf(aName)).getBytes("GB2312");
                customerReceipt = String.valueOf("         REPRINTED COPY         ").getBytes("GB2312");

                motto = String.valueOf("         BASE FOR GROWTH      ").getBytes("GB2312");

            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            PrinterInterface.begin();

            byte[] cmd;
            cmd = new byte[]{(byte) 0x1B, (byte) 0x40};

            cmd = new byte[]{
                    (byte) 0x1B, (byte) 0x45, (byte) 0};

            write(cmd);
            write(start);
            writeLineBreak(1);
            write(saccoName);
            writeLineBreak(2);
            write(type);
            writeLineBreak(1);

            write(terminalNo);
            writeLineBreak(1);
            write(transaction);
            writeLineBreak(1);
            write(time);
            writeLineBreak(1);
            write(transDate);
            writeLineBreak(2);

            write(arryDepositAmt);
            writeLineBreak(1);
            write(arryWordsHeader);
            writeLineBreak(0);
            write(arryInWords);
            writeLineBreak(1);
            write(depositedBy);
            writeLineBreak(1);
            write(arryNarration);
            writeLineBreak(2);

            write(acknowLedge);
            writeLineBreak(1);
            write(nameVerify);
            writeLineBreak(1);
            write(idVerify);
            writeLineBreak(1);
            write(signature);
            writeLineBreak(2);

            write(nameAgent);
            writeLineBreak(1);
            write(printAgent);
            writeLineBreak(2);
            write(customerReceipt);
            writeLineBreak(2);
            write(motto);
            // print line break
            writeLineBreak(4);

            PrinterInterface.end();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void write(final byte[] arryData) {
        int result = PrinterInterface.write(arryData, arryData.length);
    }

    // print line break
    private void writeLineBreak(int lineNumber) {
        // write(new byte[]{'\n'});
        write(PrinterCommand.getCmdEscDN(lineNumber));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                finish();
                //Toast.makeText(Account.this, "Back has been presses!", Toast.LENGTH_LONG).show();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.move_left_in_activity, R.anim.move_right_out_activity);
    }
}
