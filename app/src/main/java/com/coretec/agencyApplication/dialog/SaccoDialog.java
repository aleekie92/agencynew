package com.coretec.agencyApplication.dialog;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.PorterDuff;
import android.graphics.drawable.BitmapDrawable;
import android.preference.PreferenceManager;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.coretec.agencyApplication.R;
import com.coretec.agencyApplication.api.GFL;
import com.coretec.agencyApplication.api.requests.GetSaccoRequest;
import com.coretec.agencyApplication.api.requests.GrowersDetailsRequest;
import com.coretec.agencyApplication.api.responses.GetBanksAllResponse;
import com.coretec.agencyApplication.api.responses.GrowersDetailsResponse;
import com.coretec.agencyApplication.model.GrowerDetails;
import com.coretec.agencyApplication.utils.QuickToast;
import com.coretec.agencyApplication.utils.SharedPrefs;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.List;

import static com.coretec.agencyApplication.utils.Utils.getFormattedDate;
import static com.coretec.agencyApplication.utils.Utils.getLat;
import static com.coretec.agencyApplication.utils.Utils.getLong;

public class SaccoDialog extends AppCompatActivity {

    QuickToast toast = new QuickToast(this);
    LinearLayout saccoCard;

    private static final int CAMERA_REQUEST = 1888;
    private ImageView identity_imageView;
    private EditText saccoDetails, saccoaccountNo;
    Button done, dismiss;
    TextView saccoNamee;
    ProgressBar progressBar;
    String hasSacco;

    private ArrayList<String> arrayList = new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        SharedPrefs.init(getApplicationContext());
        // Make us non-modal, so that others can receive touch events.
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL, WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL);

        // ...but notify us that it happened.
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_WATCH_OUTSIDE_TOUCH, WindowManager.LayoutParams.FLAG_WATCH_OUTSIDE_TOUCH);
        setContentView(R.layout.activity_sacco_dialog);



        //define and access view attributes
        saccoDetails = findViewById(R.id.et_sacco_code);
        saccoNamee = findViewById(R.id.saccoNamee);
        progressBar = findViewById(R.id.saccoPb);

        dismiss = findViewById(R.id.btn_dismiss);
        done = findViewById(R.id.btn_done);
        saccoDetails.setInputType(InputType.TYPE_NULL);
        saccoDetails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // showMyDialog();
            }
        });

        saccoDetails.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    // showMyDialog();
                }
            }
        });

        saccoaccountNo = findViewById(R.id.et_sacco_acc_number);
        saccoaccountNo.setInputType(InputType.TYPE_NULL);
        saccoaccountNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // showMyDialog();
            }
        });

        saccoaccountNo.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    // showMyDialog();
                }
            }
        });

        identity_imageView = findViewById(R.id.imageview_identity);
        saccoCard = findViewById(R.id.saccoCard);
        saccoCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(cameraIntent, CAMERA_REQUEST);
            }
        });

          requestGetAccountDetails(SharedPrefs.read(SharedPrefs.REG_GROWER_NO, null));
        //hasSacco = "FALSE";
        if (hasSacco=="Yes"){
            requestGetAccountDetails(SharedPrefs.read(SharedPrefs.REG_GROWER_NO, null));
            progressBar.setVisibility(View.GONE);
        }else {
            saccoDetails.setText(null);
            saccoaccountNo.setText(null);
            TextWatcher textWatcherId = new TextWatcher() {

                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void afterTextChanged(Editable editable) {
                    //here, after we introduced something in the EditText we get the string from it
                    String identifier_number = saccoDetails.getText().toString();
                    if (saccoDetails.length() >= 3 && saccoDetails.length() <= 4) {
                        requestGetSaccoDetails(identifier_number, saccoDetails, progressBar, saccoNamee);
                    }
                    if (saccoDetails.length() >= 2 && saccoDetails.length() <= 3) {
                        //adapter.clear();
                        //spinner.setAdapter(null);
                        saccoNamee.setVisibility(View.GONE);
                        progressBar.setVisibility(View.VISIBLE);
                        progressBar.getIndeterminateDrawable().setColorFilter(ContextCompat.getColor(SaccoDialog.this, R.color.btn_green), PorterDuff.Mode.SRC_IN);
                    }
                }
            };
        saccoDetails.addTextChangedListener(textWatcherId);
        }
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(SaccoDialog.this);
        if (!prefs.getBoolean("first_time_sacco", false)) {
            SharedPreferences.Editor editor = prefs.edit();
            editor.putBoolean("first_time_sacco", true);
            editor.commit();

            String defaultSacco = "iVBORw0KGgoAAAANSUhEUgAAASwAAADCCAIAAAB8JmhkAAAAA3NCSVQICAjb4U/gAAAgAElEQVR4";

            Bitmap img = ((BitmapDrawable) identity_imageView.getDrawable()).getBitmap();
            String encodedImage = bitmapToBase64(img);

            String testImage = encodedImage.substring(0, 76);

            if (saccoDetails.getText().toString().isEmpty()) {
                SharedPrefs.write(SharedPrefs.REG_SACCO_AC_NO, "null");
            } else if (saccoaccountNo.getText().toString().isEmpty()) {
                SharedPrefs.write(SharedPrefs.REG_SACCO_CODE, "null");
            } else if (testImage.equals(defaultSacco)) {
                SharedPrefs.write(SharedPrefs.REG_SACOO_CARD, "null");
            }

        } else {
            String img = SharedPrefs.read(SharedPrefs.REG_SACOO_CARD, null);
            String bbc = SharedPrefs.read(SharedPrefs.REG_SACCO_AC_NO, null);
            String acc_no = SharedPrefs.read(SharedPrefs.REG_SACCO_CODE, null);

            if (!img.equals("null")) {

                byte[] decodedString = Base64.decode(img, Base64.DEFAULT);
                Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);

                identity_imageView.setImageBitmap(decodedByte);
            }

            if (!bbc.equals("null")) {
                saccoDetails.setText(bbc);
            }

            if (!acc_no.equals("null")) {
                saccoaccountNo.setText(acc_no);
            }
        }

        dismiss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPrefs.write(SharedPrefs.REG_SACCO_AC_NO, "null");
                SharedPrefs.write(SharedPrefs.REG_SACCO_CODE, "null");

                Bitmap img = ((BitmapDrawable) identity_imageView.getDrawable()).getBitmap();
                String encodedImage = bitmapToBase64(img);

                SharedPrefs.write(SharedPrefs.REG_SACOO_CARD, "null");
                toast.swarn("Please enter bank or sacco details to proceed!");
                finish();
            }
        });

        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String detailsBank = saccoDetails.getText().toString();
                String acc_number = saccoaccountNo.getText().toString();

                String defaultSacco = "iVBORw0KGgoAAAANSUhEUgAAASwAAADCCAIAAAB8JmhkAAAAA3NCSVQICAjb4U/gAAAgAElEQVR4";
                String defaultBank = "iVBORw0KGgoAAAANSUhEUgAAAjcAAAFlCAIAAACY5F6YAAAAA3NCSVQICAjb4U/gAAAgAElEQVR4";

                Bitmap img = ((BitmapDrawable) identity_imageView.getDrawable()).getBitmap();
                String encodedImage = bitmapToBase64(img);

                String testImage = encodedImage.substring(0, 76);

                if (testImage.equals(defaultSacco)) {
                    toast.swarn("Please take atm picture to proceed!");
                } else if (detailsBank.length() != 3) {
                    toast.swarn("Please fill in sacco code!");
                } else if (acc_number.isEmpty()) {
                    toast.swarn("Please enter account number!");
                } else{
                    SharedPrefs.write(SharedPrefs.REG_SACOO_CARD, encodedImage);
                    SharedPrefs.write(SharedPrefs.REG_SACCO_AC_NO,  saccoaccountNo.getText().toString());
                    SharedPrefs.write(SharedPrefs.REG_SACCO_CODE,saccoDetails.getText().toString());

                    SharedPrefs.write(SharedPrefs.REG_ATM_CARD, defaultBank);
                    SharedPrefs.write(SharedPrefs.REG_BANK_DETAILS, "");
                    SharedPrefs.write(SharedPrefs.REG_ACCOUNT_NO, "");
                    finish();
                }
            }
        });

    }

  void  requestGetSaccoDetails(String identifier, final TextView saccoDetails, final ProgressBar progressBar, final TextView saccoNamee) {

        final String TAG = "SaccoAccountDetails";
//        EasyLocationMod easyLocationMod = new EasyLocationMod(CashWithdrawal.this);

        String URL = GFL.MSACCO_AGENT + GFL.GetSaccoAccounts;

        final GetSaccoRequest getSaccoAllRequest = new GetSaccoRequest();
        getSaccoAllRequest.agentid = SharedPrefs.read(SharedPrefs.AGENT_ID_1, null);
        getSaccoAllRequest.corporateno = "CAP016";
        getSaccoAllRequest.saccocode = identifier;
        getSaccoAllRequest.transactiontype = "1";
        getSaccoAllRequest.terminalid = SharedPrefs.read(SharedPrefs.DEVICE_ID, null);
        getSaccoAllRequest.longitude = getLong(SaccoDialog.this);
        getSaccoAllRequest.latitude = getLat(SaccoDialog.this);
        getSaccoAllRequest.date = getFormattedDate();

        Log.e(TAG, getSaccoAllRequest.getBody().toString());
        //clear contents of spinner

        GFL.instance(SaccoDialog.this).request(URL, getSaccoAllRequest, new GFL.RequestListener() {
            @Override
            public void onSuccess(String response) {
                /*deposit_progressBar.setVisibility(View.GONE);*/

                GetBanksAllResponse getBanksAllResponse = GFL.instance(SaccoDialog.this)
                        .mGson.fromJson(response, GetBanksAllResponse.class);
                if (getBanksAllResponse.is_successful) {

                    try {
                        JSONObject jsonObject = new JSONObject(response);

                        JSONArray jsonArray = jsonObject.getJSONArray("getsaccoalllist");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                            JSONArray jsonArray1 = jsonObject1.getJSONArray("saccoList");

                            if (saccoDetails.length() == 2) {
                                if (jsonArray1.length() < 1) {
                                    progressBar.setVisibility(View.GONE);
                                    Toast.makeText(SaccoDialog.this, "Could not get sacco details!", Toast.LENGTH_SHORT).show();
                                    saccoNamee.setVisibility(View.VISIBLE);
                                    saccoNamee.setText("Invalid Sacco Code");
                                    saccoNamee.setTextColor(getResources().getColor(R.color.red));
                                }
                            } else {

                                for (int l = 0; l < jsonArray1.length(); l++) {
                                    progressBar.setVisibility(View.GONE);
                                    JSONObject jsonObject2 = jsonArray1.getJSONObject(l);

                                    Log.d("valiues",jsonObject2.toString());

                                    String bCode = jsonObject2.getString("SaccoCode");
                                    String bName = jsonObject2.getString("SaccoBranchName");
                                    //String fullCode = bCode + " " + bName;

                                    Log.d("detaillsss", bCode+""+bName);
                                    saccoNamee.setVisibility(View.VISIBLE);
                                    saccoNamee.setText(bName);
                                    saccoNamee.setTextColor(getResources().getColor(R.color.btn_green));

                                    Log.e("Sacco Name", bName);
                                }
                            }

                        }


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                } else {
                   Toast.makeText(SaccoDialog.this, "Accounts Fetching Failed", Toast.LENGTH_LONG).show();
                }

            }

            @Override
            public void onTokenExpired() {

            }

            @Override
            public void onError(String error) {

            }
        });
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case CAMERA_REQUEST:
                if (resultCode == FragmentActivity.RESULT_OK) {
                    try {
                        Bitmap photo = (Bitmap) data.getExtras().get("data");
                        identity_imageView.setImageBitmap(photo);

                        //imageView.setImageBitmap(bitmap);
                        /*Toast.makeText(this, imageview_reason.toString(),
                                Toast.LENGTH_SHORT).show();*/
                    } catch (Exception e) {
                        Toast.makeText(SaccoDialog.this, "Failed to load", Toast.LENGTH_SHORT)
                                .show();
                        Log.e("Camera", e.toString());
                    }
                }
        }
    }

    private String bitmapToBase64(Bitmap bitmap) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
        byte[] byteArray = byteArrayOutputStream.toByteArray();
        return Base64.encodeToString(byteArray, Base64.DEFAULT);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        // If we've received a touch notification that the user has touched
        // outside the app, finish the activity.
        if (MotionEvent.ACTION_OUTSIDE == event.getAction()) {
            //finish();
            return true;
        }

        // Delegate everything else to Activity.
        return super.onTouchEvent(event);
    }


    void requestGetAccountDetails(String identifier) {

        final String TAG = "GrowersAccounts";

        String URL = GFL.MSACCO_AGENT + GFL.GetGrowersDetails;

        final GrowersDetailsRequest growersDetails = new GrowersDetailsRequest();
        growersDetails.agentid = SharedPrefs.read(SharedPrefs.AGENT_ID_1, null);
        growersDetails.corporateno = "CAP016";
        growersDetails.accountidentifier = identifier;
        growersDetails.accountidentifiercode = "2";
        growersDetails.transactiontype = "1";
        growersDetails.terminalid = SharedPrefs.read(SharedPrefs.DEVICE_ID, null);
        growersDetails.longitude = getLong(SaccoDialog.this);
        growersDetails.latitude = getLat(SaccoDialog.this);
        growersDetails.date = getFormattedDate();

        Log.e(TAG, growersDetails.getBody().toString());
        //clear contents of spinner

        GFL.instance(SaccoDialog.this).request(URL, growersDetails, new GFL.RequestListener() {
            @Override
            public void onSuccess(String response) {
                /*deposit_progressBar.setVisibility(View.GONE);*/

                GrowersDetailsResponse growersDetailsResponse = GFL.instance(SaccoDialog.this)
                        .mGson.fromJson(response, GrowersDetailsResponse.class);
                if (growersDetailsResponse.is_successful) {
                    progressBar.setVisibility(View.GONE);
                    List<GrowerDetails> growerDetailsList = new ArrayList<>();

                    try {
                        JSONObject jsonObject = new JSONObject(response);

                        JSONArray jsonArray = jsonObject.getJSONArray("Growers");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                            JSONArray jsonArray1 = jsonObject1.getJSONArray("GrowerDetails");

                            if (jsonArray1.length() < 1 ) {
                                Toast.makeText(SaccoDialog.this, "Customer does not exist", Toast.LENGTH_LONG).show();
                            } else {

                                for (int l = 0; l < jsonArray1.length(); l++) {
                                    JSONObject jsonObject2 = jsonArray1.getJSONObject(l);
                                    arrayList.add(String.valueOf(jsonObject2.getString("GrowerNo")));
                                    /*mpesaPhoneNumber = jsonObject2.getString("phonenumber");*/
                                    //String hasBank = jsonObject2.getString("BankExists");
                                    hasSacco = jsonObject2.getString("SaccoExists");

                                    String.valueOf(growersDetailsResponse.getGrowers());

                                    Log.d("mydetailsss", hasSacco);

                                    String saccoacc = jsonObject2.getString("SaccoMemberAcc");
                                    String saccocode = jsonObject2.getString("SaccoCode");

                                    saccoDetails.setText(saccocode);
                                    saccoaccountNo.setText(saccoacc);

                                }
                            }

                            /*if (jsonObject1.getJSONArray("GrowerDetails").length()<=0){
                                status.setVisibility(View.VISIBLE);
                                status.setText("Invalid grower Number");
                                status.setTextColor(getResources().getColor(R.color.red));

                            }*/

                        }


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                } else {
//                    Toast.makeText(ShareDeposit.this, "Accounts Fetching Failed", Toast.LENGTH_LONG).show();
                }

            }

            @Override
            public void onTokenExpired() {

            }

            @Override
            public void onError(String error) {

            }
        });
    }

}