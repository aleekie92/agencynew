package com.coretec.agencyApplication.dialog;

import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.PorterDuff;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.coretec.agencyApplication.R;
import com.coretec.agencyApplication.api.GFL;
import com.coretec.agencyApplication.api.requests.GetBanksAllRequest;
import com.coretec.agencyApplication.api.requests.GrowersDetailsRequest;
import com.coretec.agencyApplication.api.responses.GetBanksAllResponse;
import com.coretec.agencyApplication.api.responses.GrowersDetailsResponse;
import com.coretec.agencyApplication.model.GrowerDetails;
import com.coretec.agencyApplication.utils.QuickToast;
import com.coretec.agencyApplication.utils.SharedPrefs;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.List;

import static com.coretec.agencyApplication.utils.Utils.getFormattedDate;
import static com.coretec.agencyApplication.utils.Utils.getLat;
import static com.coretec.agencyApplication.utils.Utils.getLong;

public class BankUpdateDialog extends AppCompatActivity { QuickToast toast = new QuickToast(this);
    private static final int CAMERA_REQUEST = 1888;
    LinearLayout bankAtm;
    private ImageView identity_imageView;
    private EditText bankDetails, accountNo;
    TextView bankName;
    Button done, dismiss;

    ProgressBar progressBar;
    String hasBank;
    private ArrayList<String> arrayList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        SharedPrefs.init(getApplicationContext());
        // Make us non-modal, so that others can receive touch events.
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL, WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL);

        // ...but notify us that it happened.
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_WATCH_OUTSIDE_TOUCH, WindowManager.LayoutParams.FLAG_WATCH_OUTSIDE_TOUCH);
        setContentView(R.layout.activity_update_bank_dialog);


        //define and access view attributes
        bankDetails = findViewById(R.id.bank_details);
        bankName = findViewById(R.id.bankname);
        progressBar = findViewById(R.id.bankPb);

        dismiss = findViewById(R.id.btn_dismiss);
        done = findViewById(R.id.btn_done);
        //bankDetails.setInputType(InputType.TYPE_NULL);
        bankDetails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // showMyDialog();
            }
        });

        bankDetails.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    // showMyDialog();
                }
            }
        });

        accountNo = findViewById(R.id.account_number);
        //accountNo.setInputType(InputType.TYPE_NULL);
        accountNo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // showMyDialog();
            }
        });

        accountNo.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    // showMyDialog();
                }
            }
        });

        identity_imageView = findViewById(R.id.imageview_identity);
        bankAtm = findViewById(R.id.bankAtm);
        bankAtm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(cameraIntent, CAMERA_REQUEST);
            }
        });

        requestGetAccountDetails(SharedPrefs.read(SharedPrefs.REG_GROWER_NO, null));
        if (hasBank == "Yes"){
            requestGetAccountDetails(SharedPrefs.read(SharedPrefs.REG_GROWER_NO, null));
            progressBar.setVisibility(View.GONE);

        }else {
            bankDetails.setText(null);
            accountNo.setText(null);
            TextWatcher textWatcherId = new TextWatcher() {

                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void afterTextChanged(Editable editable) {
                    //here, after we introduced something in the EditText we get the string from it
                    String identifier_number = bankDetails.getText().toString();
                    if (bankDetails.length() >= 5 && bankDetails.length() <= 6) {
                        requestGetBankDetails(identifier_number, bankDetails, progressBar, bankName);
                    }
                    if (bankDetails.length() >= 4 && bankDetails.length() <= 5) {
                        //adapter.clear();
                        //spinner.setAdapter(null);
                        bankName.setVisibility(View.GONE);
                        progressBar.setVisibility(View.VISIBLE);
                        progressBar.getIndeterminateDrawable().setColorFilter(ContextCompat.getColor(BankUpdateDialog.this, R.color.btn_green), PorterDuff.Mode.SRC_IN);
                    }
                }
            };
            bankDetails.addTextChangedListener(textWatcherId);
        }

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(BankUpdateDialog.this);
        if (!prefs.getBoolean("first_time_bank", false)) {
            SharedPreferences.Editor editor = prefs.edit();
            editor.putBoolean("first_time_bank", true);
            editor.commit();

            String defaultBank = "iVBORw0KGgoAAAANSUhEUgAAAjcAAAFlCAIAAACY5F6YAAAAA3NCSVQICAjb4U/gAAAgAElEQVR4";

            Bitmap img = ((BitmapDrawable) identity_imageView.getDrawable()).getBitmap();
            String encodedImage = bitmapToBase64(img);

            String testImage = encodedImage.substring(0, 76);

            if (bankDetails.getText().toString().isEmpty()) {
                SharedPrefs.write(SharedPrefs.REG_BANK_DETAILS, "null");
            } else if (accountNo.getText().toString().isEmpty()) {
                SharedPrefs.write(SharedPrefs.REG_ACCOUNT_NO, "null");
            } else if (testImage.equals(defaultBank)) {
                SharedPrefs.write(SharedPrefs.REG_ATM_CARD, "null");
            }

        } else {
            String img = SharedPrefs.read(SharedPrefs.REG_ATM_CARD, null);
            String bbc = SharedPrefs.read(SharedPrefs.REG_BANK_DETAILS, null);
            String acc_no = SharedPrefs.read(SharedPrefs.REG_ACCOUNT_NO, null);
        }

        dismiss.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SharedPrefs.write(SharedPrefs.REG_BANK_DETAILS, "null");
                SharedPrefs.write(SharedPrefs.REG_ACCOUNT_NO, "null");

                Bitmap img = ((BitmapDrawable) identity_imageView.getDrawable()).getBitmap();
                String encodedImage = bitmapToBase64(img);

                SharedPrefs.write(SharedPrefs.REG_ATM_CARD, "null");
                toast.swarn("Please enter bank or sacco details to proceed!");
                finish();
            }
        });

        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String detailsBank = bankDetails.getText().toString();
                String acc_number = accountNo.getText().toString();

                String defaultBank = "iVBORw0KGgoAAAANSUhEUgAAAjcAAAFlCAIAAACY5F6YAAAAA3NCSVQICAjb4U/gAAAgAElEQVR4";
                String defaultSacco = "iVBORw0KGgoAAAANSUhEUgAAASwAAADCCAIAAAB8JmhkAAAAA3NCSVQICAjb4U/gAAAgAElEQVR4";

                Bitmap img = ((BitmapDrawable) identity_imageView.getDrawable()).getBitmap();
                String encodedImage = bitmapToBase64(img);

                String testImage = encodedImage.substring(0, 76);

                if (testImage.equals(defaultBank)) {
                    toast.swarn("Please take atm picture to proceed!");
                } else if (detailsBank.length() != 5 && detailsBank.length() != 6) {
                    toast.swarn("Please fill in bank and branch code!");
                } else if (acc_number.isEmpty()) {
                    toast.swarn("Please enter account number!");
                } else{
                    SharedPrefs.write(SharedPrefs.REG_ATM_CARD, encodedImage);
                    SharedPrefs.write(SharedPrefs.REG_BANK_DETAILS, bankDetails.getText().toString());
                    SharedPrefs.write(SharedPrefs.REG_ACCOUNT_NO, accountNo.getText().toString());

                    Log.d("bankImage",encodedImage);
                    Log.d("bankImage",bankDetails.getText().toString());
                    Log.d("bankImage",accountNo.getText().toString());
                    finish();
                }
            }
        });

    }

    void requestGetBankDetails(String identifier, final TextView bankDetails, final ProgressBar progressBar, final TextView bankName) {

        final String TAG = "BankAccountDetails";

        String URL = GFL.MSACCO_AGENT + GFL.GetBanksAll;

        final GetBanksAllRequest getBanksAllRequest = new GetBanksAllRequest();
        getBanksAllRequest.agentid = SharedPrefs.read(SharedPrefs.AGENT_ID_1, null);
        getBanksAllRequest.corporateno = "CAP016";
        getBanksAllRequest.bankcode = identifier;
        getBanksAllRequest.transactiontype = "1";
        getBanksAllRequest.terminalid = SharedPrefs.read(SharedPrefs.DEVICE_ID, null);
        getBanksAllRequest.longitude = getLong(BankUpdateDialog.this);
        getBanksAllRequest.latitude = getLat(BankUpdateDialog.this);
        getBanksAllRequest.date = getFormattedDate();

        Log.e(TAG, getBanksAllRequest.getBody().toString());
        //clear contents of spinner

        GFL.instance(BankUpdateDialog.this).request(URL, getBanksAllRequest, new GFL.RequestListener() {
            @Override
            public void onSuccess(String response) {
                /*deposit_progressBar.setVisibility(View.GONE);*/

                GetBanksAllResponse getBanksAllResponse = GFL.instance(BankUpdateDialog.this)
                        .mGson.fromJson(response, GetBanksAllResponse.class);
                if (getBanksAllResponse.is_successful) {

                    try {
                        JSONObject jsonObject = new JSONObject(response);

                        JSONArray jsonArray = jsonObject.getJSONArray("getbanksalllist");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                            JSONArray jsonArray1 = jsonObject1.getJSONArray("BanksList");

                            if (bankDetails.length() == 6) {
                                if (jsonArray1.length() < 1) {
                                    progressBar.setVisibility(View.GONE);
                                    Toast.makeText(BankUpdateDialog.this, "Could not get bank details!", Toast.LENGTH_SHORT).show();
                                    bankName.setVisibility(View.VISIBLE);
                                    bankName.setText("Invalid Bank And Branch Code");
                                    bankName.setTextColor(getResources().getColor(R.color.red));
                                }
                            } else {

                                for (int l = 0; l < jsonArray1.length(); l++) {
                                    progressBar.setVisibility(View.GONE);
                                    JSONObject jsonObject2 = jsonArray1.getJSONObject(l);

                                    String bCode = jsonObject2.getString("Code");
                                    String bName = jsonObject2.getString("Name");
                                    String bankBranch = jsonObject2.getString("Branch");
                                    String fullCode = bName + " " + bankBranch;

                                    bankName.setVisibility(View.VISIBLE);
                                    bankName.setText(fullCode);
                                    bankName.setTextColor(getResources().getColor(R.color.btn_green));

                                    Log.e("Bank Name", bName);
                                }
                            }

                        }


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                } else {
//                    Toast.makeText(ShareDeposit.this, "Accounts Fetching Failed", Toast.LENGTH_LONG).show();
                }

            }

            @Override
            public void onTokenExpired() {

            }

            @Override
            public void onError(String error) {

            }
        });
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case CAMERA_REQUEST:
                if (resultCode == FragmentActivity.RESULT_OK) {
                    try {
                        Bitmap photo = (Bitmap) data.getExtras().get("data");
                        identity_imageView.setImageBitmap(photo);

                        //imageView.setImageBitmap(bitmap);
                        /*Toast.makeText(this, imageview_reason.toString(),
                                Toast.LENGTH_SHORT).show();*/
                    } catch (Exception e) {
                        Toast.makeText(BankUpdateDialog.this, "Failed to load", Toast.LENGTH_SHORT)
                                .show();
                        Log.e("Camera", e.toString());
                    }
                }
        }
    }

    private String bitmapToBase64(Bitmap bitmap) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
        byte[] byteArray = byteArrayOutputStream.toByteArray();
        return Base64.encodeToString(byteArray, Base64.DEFAULT);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        // If we've received a touch notification that the user has touched
        // outside the app, finish the activity.
        if (MotionEvent.ACTION_OUTSIDE == event.getAction()) {
            //finish();
            return true;
        }

        // Delegate everything else to Activity.
        return super.onTouchEvent(event);
    }

    void requestGetAccountDetails(String identifier) {

        final String TAG = "GrowersAccounts";

        String URL = GFL.MSACCO_AGENT + GFL.GetGrowersDetails;

        final GrowersDetailsRequest growersDetails = new GrowersDetailsRequest();
        growersDetails.agentid = SharedPrefs.read(SharedPrefs.AGENT_ID_1, null);
        growersDetails.corporateno = "CAP016";
        growersDetails.accountidentifier = SharedPrefs.read(SharedPrefs.IDENTIFIER_FOR_UPDATING, null);;
        growersDetails.accountidentifiercode = "1";
        growersDetails.transactiontype = "1";
        growersDetails.terminalid = SharedPrefs.read(SharedPrefs.DEVICE_ID, null);
        growersDetails.longitude = getLong(BankUpdateDialog.this);
        growersDetails.latitude = getLat(BankUpdateDialog.this);
        growersDetails.date = getFormattedDate();

        Log.e(TAG, growersDetails.getBody().toString());
        //clear contents of spinner

        GFL.instance(BankUpdateDialog.this).request(URL, growersDetails, new GFL.RequestListener() {
            @Override
            public void onSuccess(String response) {
                /*deposit_progressBar.setVisibility(View.GONE);*/

                GrowersDetailsResponse growersDetailsResponse = GFL.instance(BankUpdateDialog.this)
                        .mGson.fromJson(response, GrowersDetailsResponse.class);
                if (growersDetailsResponse.is_successful) {
                    progressBar.setVisibility(View.GONE);
                    List<GrowerDetails> growerDetailsList = new ArrayList<>();

                    try {
                        JSONObject jsonObject = new JSONObject(response);

                        JSONArray jsonArray = jsonObject.getJSONArray("Growers");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                            JSONArray jsonArray1 = jsonObject1.getJSONArray("GrowerDetails");

                            if (jsonArray1.length() < 1 ) {
                                Toast.makeText(BankUpdateDialog.this, "Customer does not exist", Toast.LENGTH_LONG).show();
                            } else {

                                for (int l = 0; l < jsonArray1.length(); l++) {
                                    JSONObject jsonObject2 = jsonArray1.getJSONObject(l);
                                    arrayList.add(String.valueOf(jsonObject2.getString("GrowerNo")));
                                    /*mpesaPhoneNumber = jsonObject2.getString("phonenumber");*/
                                    //String hasBank = jsonObject2.getString("BankExists");
                                    hasBank = jsonObject2.getString("BankExists");

                                    Log.d("bankdetailllllllll", hasBank);

                                    String bankacc = jsonObject2.getString("BankMemberAcc");
                                    String bankcode = jsonObject2.getString("BankCode");

                                    bankDetails.setText(bankcode);
                                    accountNo.setText(bankacc);

                                }

                            }
                            /*if (jsonObject1.getJSONArray("GrowerDetails").length()<=0){
                                status.setVisibility(View.VISIBLE);
                                status.setText("Invalid grower Number");
                                status.setTextColor(getResources().getColor(R.color.red));

                            }*/

                        }


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                } else {
//                    Toast.makeText(ShareDeposit.this, "Accounts Fetching Failed", Toast.LENGTH_LONG).show();
                }

            }

            @Override
            public void onTokenExpired() {

            }

            @Override
            public void onError(String error) {

            }
        });
    }

}
