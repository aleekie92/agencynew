package com.coretec.agencyApplication.api.responses;

public class LoanExistResponse extends BaseResponse {

    private boolean Exist;
    private String error;

    public boolean isExist() {
        return Exist;
    }

    public void setExist(boolean exist) {
        Exist = exist;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }
}
