package com.coretec.agencyApplication.api.responses;

import com.coretec.agencyApplication.model.netAmount;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class getnetamountresponselist implements Serializable {

    @SerializedName("netAmount")
    private List<netAmount> netAmounts;

    public List<netAmount> getNetAmounts() {
        return netAmounts;
    }

    public void setNetAmounts(List<netAmount> netAmounts) {
        this.netAmounts = netAmounts;
    }
}
