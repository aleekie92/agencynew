package com.coretec.agencyApplication.api.responses;

/**
 * Created by ofula on 18/06/24.
 */

public class MemberNameResponse extends BaseResponse {

    private String error;
    private String customer_name;
    private String phoneno;
    private Boolean has_pin;

    public String getCustomer_name() {
        return customer_name;
    }

    public void setCustomer_name(String customer_name) {
        this.customer_name = customer_name;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getPhoneno() {
        return phoneno;
    }

    public void setPhoneno(String phoneno) {
        this.phoneno = phoneno;
    }

    public Boolean getHas_pin() {
        return has_pin;
    }

    public void setHas_pin(Boolean has_pin) {
        this.has_pin = has_pin;
    }

    public MemberNameResponse() {

    }

}
