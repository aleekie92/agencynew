package com.coretec.agencyApplication.api.responses;

public class BioAuthResponse extends BaseResponse {

    private String error;
    private boolean bioregistered;

    public boolean isBioregistered() {
        return bioregistered;
    }

    public void setBioregistered(boolean bioregistered) {
        this.bioregistered = bioregistered;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }
    public BioAuthResponse() {

    }
}
