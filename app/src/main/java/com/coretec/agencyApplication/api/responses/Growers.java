package com.coretec.agencyApplication.api.responses;

import com.coretec.agencyApplication.model.GrowerDetails;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * Created by ofula on 18/06/24.
 */

public class Growers implements Serializable {

    @SerializedName("GrowerDetails")
    private List<GrowerDetails> growerDetails;

    public List<GrowerDetails> getGrowerDetails() {
        return growerDetails;
    }

    public void setGrowerDetails(List<GrowerDetails> growerDetails) {
        this.growerDetails = growerDetails;
    }
}
