package com.coretec.agencyApplication.api.responses;

public class NameResponse extends BaseResponse {

    private String agent_name;
    private  boolean hasbio;
    private String error;

    public String getAgent_name() {
        return agent_name;
    }

    public void setAgent_name(String agent_name) {
        this.agent_name = agent_name;
    }

    public boolean isHasbio() {
        return hasbio;
    }

    public void setHasbio(boolean hasbio) {
        this.hasbio = hasbio;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }
}
