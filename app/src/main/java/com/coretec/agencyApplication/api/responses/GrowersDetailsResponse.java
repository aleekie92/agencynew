package com.coretec.agencyApplication.api.responses;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * Created by ofula on 18/07/15.
 */

public class GrowersDetailsResponse extends BaseResponse implements Serializable {

    private String error;
    @SerializedName("transactiondate")
    private String transactiondate;
    @SerializedName("Growers")
    private List<Growers> growers;

    public GrowersDetailsResponse() {

    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getTransactiondate() {
        return transactiondate;
    }

    public void setTransactiondate(String transactiondate) {
        this.transactiondate = transactiondate;
    }

    public List<Growers> getGrowers() {
        return growers;
    }

    public void setGrowers(List<Growers> growers) {
        this.growers = growers;
    }
}
