package com.coretec.agencyApplication.api.requests;

import com.coretec.agencyApplication.utils.Const;

public class LoanApplicationRequest extends BaseRequest {

    public String userid;
    public String salescode;
    public String idno;
    public String accountname;
    public String disbursementtype;
    public String growerno;
    public String loanproducttype;
    public Boolean activitycode;
    public String amount;
    public int approvalstatus;
    public Double qualifiedkgs;
    public String transactiontype;
    public String agentid;
    public String terminalid;
    public double longitude;
    public double latitude;
    public String date;
    public String corporate_no;

    public LoanApplicationRequest() {
        super();
    }

    @Override
    public void setCredentials() {
        authorization_credentials.api_key = "12345";
        authorization_credentials.token = Const.getInstance().getLoginToken();
    }

}
