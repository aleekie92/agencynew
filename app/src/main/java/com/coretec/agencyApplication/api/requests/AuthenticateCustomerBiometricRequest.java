package com.coretec.agencyApplication.api.requests;

import com.coretec.agencyApplication.utils.Const;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class AuthenticateCustomerBiometricRequest extends BaseRequest implements Serializable{

	@SerializedName("date")
	private String date;

	@SerializedName("agentid")
	private String agentid;

	@SerializedName("latitude")
	private double latitude;

	@SerializedName("fingerprint")
	private Object fingerprint;

	@SerializedName("accountidentifiercode")
	private String accountidentifiercode;

	@SerializedName("terminalid")
	private String terminalid;

	@SerializedName("accountidentifier")
	private String accountidentifier;

	@SerializedName("longitude")
	private double longitude;

	public void setDate(String date){
		this.date = date;
	}

	public String getDate(){
		return date;
	}

	public void setAgentid(String agentid){
		this.agentid = agentid;
	}

	public String getAgentid(){
		return agentid;
	}

	public void setLatitude(double latitude){
		this.latitude = latitude;
	}

	public double getLatitude(){
		return latitude;
	}

	public void setFingerprint(Object fingerprint){
		this.fingerprint = fingerprint;
	}

	public Object getFingerprint(){
		return fingerprint;
	}

	public void setAccountidentifiercode(String accountidentifiercode){
		this.accountidentifiercode = accountidentifiercode;
	}

	public String getAccountidentifiercode(){
		return accountidentifiercode;
	}

	public void setTerminalid(String terminalid){
		this.terminalid = terminalid;
	}

	public String getTerminalid(){
		return terminalid;
	}

	public void setAccountidentifier(String accountidentifier){
		this.accountidentifier = accountidentifier;
	}

	public String getAccountidentifier(){
		return accountidentifier;
	}

	public void setLongitude(double longitude){
		this.longitude = longitude;
	}

	public double getLongitude(){
		return longitude;
	}

    @Override
    public void setCredentials() {
        authorization_credentials.api_key = "12345";
        authorization_credentials.token = Const.getInstance().getLoginToken();
    }
}