package com.coretec.agencyApplication.api.requests;

/**
 * Created by ofula on 18/06/24.
 */

public class GenerateTokenRequest extends BaseRequest {

    public GenerateTokenRequest(){
        super();
    }
    @Override
    public void setCredentials() {
        authorization_credentials.api_key = "12345";
        authorization_credentials.app_secret ="12345";
    }

    @Override
    public Object getBody() {
        return authorization_credentials;
    }
}
