package com.coretec.agencyApplication.api.responses;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class StatusList implements Serializable {
    @SerializedName("Loanstatus")
    private List<StatusList> status;

    public List<StatusList> getStatus() {
        return status;
    }

    public void setStatus(List<StatusList> status) {
        this.status = status;
    }
}
