package com.coretec.agencyApplication.api.requests;

import com.coretec.agencyApplication.model.GuarantorObject;
import com.coretec.agencyApplication.utils.Const;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class GuarantorRegistration_v2 extends BaseRequest {

    private String applgrowerno;
    private String applidno;
    @SerializedName("NewAppl")
    private boolean NewAppl;
    private String applreqkgs;
    private String applptype;
    private String applamount;
    private String disbursmentmethod;
    private String approvalstatus;
    private String transactiontype;
    @SerializedName("OrigDocumentNo")
    private String OrigDocumentNo;
    private String agentid;
    private String terminalid;
    private double longitude;
    private double latitude;
    private String date;
    private String corporate_no;
    private List<GuarantorObject> guarantorlist;

    public String getApplgrowerno() {
        return applgrowerno;
    }

    public void setApplgrowerno(String applgrowerno) {
        this.applgrowerno = applgrowerno;
    }

    public String getApplidno() {
        return applidno;
    }

    public void setApplidno(String applidno) {
        this.applidno = applidno;
    }

    public boolean isNewAppl() {
        return NewAppl;
    }

    public void setNewAppl(boolean newAppl) {
        NewAppl = newAppl;
    }

    public String getApplreqkgs() {
        return applreqkgs;
    }

    public void setApplreqkgs(String applreqkgs) {
        this.applreqkgs = applreqkgs;
    }

    public String getApplptype() {
        return applptype;
    }

    public void setApplptype(String applptype) {
        this.applptype = applptype;
    }

    public String getApplamount() {
        return applamount;
    }

    public void setApplamount(String applamount) {
        this.applamount = applamount;
    }

    public String getDisbursmentmethod() {
        return disbursmentmethod;
    }

    public void setDisbursmentmethod(String disbursmentmethod) {
        this.disbursmentmethod = disbursmentmethod;
    }

    public String getApprovalstatus() {
        return approvalstatus;
    }

    public void setApprovalstatus(String approvalstatus) {
        this.approvalstatus = approvalstatus;
    }

    public String getOrigDocumentNo() {
        return OrigDocumentNo;
    }

    public void setOrigDocumentNo(String origDocumentNo) {
        OrigDocumentNo = origDocumentNo;
    }

    public String getTransactiontype() {
        return transactiontype;
    }

    public void setTransactiontype(String transactiontype) {
        this.transactiontype = transactiontype;
    }

    public String getAgentid() {
        return agentid;
    }

    public void setAgentid(String agentid) {
        this.agentid = agentid;
    }

    public String getTerminalid() {
        return terminalid;
    }

    public void setTerminalid(String terminalid) {
        this.terminalid = terminalid;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getCorporate_no() {
        return corporate_no;
    }

    public void setCorporate_no(String corporate_no) {
        this.corporate_no = corporate_no;
    }

    public List<GuarantorObject> getGuarantorlist() {
        return guarantorlist;
    }

        public void setGuarantorlist(List<GuarantorObject> guarantorlist) {
        this.guarantorlist = guarantorlist;
    }

    @Override
    public void setCredentials() {
        authorization_credentials.api_key = "12345";
        authorization_credentials.token = Const.getInstance().getLoginToken();
    }

    /*public static class AuthorizationCredentialsBean {
        *//**
         * api_key : sample string 1
         * token : sample string 2
         *//*

        private String api_key;
        private String token;

        public String getApi_key() {
            return api_key;
        }

        public void setApi_key(String api_key) {
            this.api_key = api_key;
        }

        public String getToken() {
            return token;
        }

        public void setToken(String token) {
            this.token = token;
        }
    }*/

    public static class GuarantorlistBean {
        /**
         * grower_number : sample string 1
         * guarid : sample string 2
         * available_kgs : sample string 3
         */

        private String grower_number;
        private String guarid;
        private String available_kgs;

        public String getGrower_number() {
            return grower_number;
        }

        public void setGrower_number(String grower_number) {
            this.grower_number = grower_number;
        }

        public String getGuarid() {
            return guarid;
        }

        public void setGuarid(String guarid) {
            this.guarid = guarid;
        }

        public String getAvailable_kgs() {
            return available_kgs;
        }

        public void setAvailable_kgs(String available_kgs) {
            this.available_kgs = available_kgs;
        }
    }
}
