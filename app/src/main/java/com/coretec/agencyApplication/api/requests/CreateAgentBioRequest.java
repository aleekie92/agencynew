package com.coretec.agencyApplication.api.requests;

import com.coretec.agencyApplication.utils.Const;

public class CreateAgentBioRequest extends BaseRequest {

    public String leftring;
    public String leftlittle;
    public String rightring;
    public String rightlittle;
    public String agentid;
    public String agentidlogged;
    public String terminalid;

    public String leftthree;
    public String rightthree;
    public String leftfour;
    public String rightfour;
    public String leftfive;
    public String rightfive;

    public String passportphoto;
    public String idno;


    public CreateAgentBioRequest(){
        super();
    }

    @Override
    public void setCredentials() {
        authorization_credentials.api_key = "12345";
        authorization_credentials.token = Const.getInstance().getLoginToken();
    }

}
