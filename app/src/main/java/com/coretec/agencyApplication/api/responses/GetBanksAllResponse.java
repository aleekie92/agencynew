package com.coretec.agencyApplication.api.responses;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class GetBanksAllResponse extends BaseResponse implements Serializable {

    private String error;
    @SerializedName("transactiondate")
    private String transactiondate;
    @SerializedName("getbanksalllist")
    private List<getbanksalllist> getbanksalllistList;

    public GetBanksAllResponse() {

    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getTransactiondate() {
        return transactiondate;
    }

    public void setTransactiondate(String transactiondate) {
        this.transactiondate = transactiondate;
    }

    public List<getbanksalllist> getGetbanksalllistList() {
        return getbanksalllistList;
    }

    public void setGetbanksalllistList(List<getbanksalllist> getbanksalllistList) {
        this.getbanksalllistList = getbanksalllistList;
    }
}
