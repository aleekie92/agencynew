package com.coretec.agencyApplication.api.responses;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class GetLoansInfoResponse extends BaseResponse implements Serializable {

    private String error;
    @SerializedName("transactiondate")
    private String transactiondate;
    @SerializedName("GetLoansInfo")
    private List<GetLoansInfo> loansInfo;

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getTransactiondate() {
        return transactiondate;
    }

    public void setTransactiondate(String transactiondate) {
        this.transactiondate = transactiondate;
    }

    public List<GetLoansInfo> getLoansInfo() {
        return loansInfo;
    }

    public void setLoansInfo(List<GetLoansInfo> loansInfo) {
        this.loansInfo = loansInfo;
    }
}
