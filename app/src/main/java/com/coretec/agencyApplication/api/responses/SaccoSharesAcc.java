package com.coretec.agencyApplication.api.responses;

import com.coretec.agencyApplication.model.SharesAccountDetails;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class SaccoSharesAcc implements Serializable {

    private String sacconame;
    private String corporateno;
    @SerializedName("accountdetails")
    private List<SharesAccountDetails> accountDetails;

    public String getSacconame() {
        return sacconame;
    }

    public void setSacconame(String sacconame) {
        this.sacconame = sacconame;
    }

    public String getCorporateno() {
        return corporateno;
    }

    public void setCorporateno(String corporateno) {
        this.corporateno = corporateno;
    }

    public List<SharesAccountDetails> getAccountDetails() {
        return accountDetails;
    }

    public void setAccountDetails(List<SharesAccountDetails> accountDetails) {
        this.accountDetails = accountDetails;
    }
}
