package com.coretec.agencyApplication.api.responses;

import com.coretec.agencyApplication.model.growerministatement;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class GetMiniStatementResponse extends BaseResponse implements Serializable {

    private String error;
    @SerializedName("transactiondate")
    private String transactiondate;
    @SerializedName("growerministatement")
    private List<growerministatement> growerministatements;

    public GetMiniStatementResponse() {

    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getTransactiondate() {
        return transactiondate;
    }

    public void setTransactiondate(String transactiondate) {
        this.transactiondate = transactiondate;
    }

    public List<growerministatement> getGrowerministatements() {
        return growerministatements;
    }

    public void setGrowerministatements(List<growerministatement> growerministatements) {
        this.growerministatements = growerministatements;
    }
}
