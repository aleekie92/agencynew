package com.coretec.agencyApplication.api.responses;

/**
 * Created by ofula on 18/06/24.
 */

public class MemberAuthResponse extends BaseResponse {
    private String error;
    private boolean is_active;
    private boolean registered;
    private int attempts_left;

    public boolean is_active() {
        return is_active;
    }

    public void setIs_active(boolean is_active) {
        this.is_active = is_active;
    }

    public boolean isRegistered() {
        return registered;
    }

    public void setRegistered(boolean registered) {
        this.registered = registered;
    }

    public int getAttempts_left() {
        return attempts_left;
    }

    public void setAttempts_left(int attempts_left) {
        this.attempts_left = attempts_left;
    }

    public MemberAuthResponse() {

    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }
}
