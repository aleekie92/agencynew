package com.coretec.agencyApplication.api.responses;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class AccDetailsResponse extends BaseResponse implements Serializable {

    private String error;
    @SerializedName("transactiondate")
    private String transactiondate;
    @SerializedName("saccoaccounts")
    private List<SaccoSharesAcc> saccoAccounts;

    public AccDetailsResponse() {

    }

    public String getTransactiondate() {
        return transactiondate;
    }

    public void setTransactiondate(String transactiondate) {
        this.transactiondate = transactiondate;
    }

    public List<SaccoSharesAcc> getSaccoAccounts() {
        return saccoAccounts;
    }

    public void setSaccoAccounts(List<SaccoSharesAcc> saccoAccounts) {
        this.saccoAccounts = saccoAccounts;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

}
