package com.coretec.agencyApplication.api.requests;

import com.coretec.agencyApplication.activities.newmodules.MainDashboardActivity;
import com.coretec.agencyApplication.utils.Const;
import com.coretec.agencyApplication.utils.SharedPrefs;

public class RequestStkPushRequest extends BaseRequest {
    public String  userid;
    public String  msisdn;
    public String  accountreference;
    public String  amount;
    public String  transactiontype;
    public String agentid;
    public String terminalid;
    public double longitude;
    public double latitude;
    public String date;
    public String corporate_no;

    public RequestStkPushRequest(){
        super();
    }

    @Override
    public void setCredentials() {
        authorization_credentials.api_key = "12345";
        authorization_credentials.token = Const.getInstance().getLoginToken();
    }

}
