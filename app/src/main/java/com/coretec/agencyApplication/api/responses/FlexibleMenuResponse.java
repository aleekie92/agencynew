package com.coretec.agencyApplication.api.responses;

public class FlexibleMenuResponse extends BaseResponse {

    private String isadmin;
    private String memberregistration;
    private String memberactivation;
    private String withdrawal;
    private String deposit;
    private String sharedeposit;
    private String loanapplication;
    private String loanrepayment;
    private String balance;
    private String ministatement;
    private String reprintreceipt;
    private String agentinfo;
    private String bioregister;
    private String grouptransaction;
    private String groupRegistration;
    private String accountopenning;
    private String eodreport;
    private String printlasttrans;
    private String platform;
    private String customerchangepin;
    private String cashtransfer;
    private String usesbioauthentication;
    //public boolean operationsuccess;
    private String error;
    private String currencycode;

    public String getIsadmin() {
        return isadmin;
    }

    public void setIsadmin(String isadmin) {
        this.isadmin = isadmin;
    }

    public String getMemberregistration() {
        return memberregistration;
    }

    public void setMemberregistration(String memberregistration) {
        this.memberregistration = memberregistration;
    }

    public String getMemberactivation() {
        return memberactivation;
    }

    public void setMemberactivation(String memberactivation) {
        this.memberactivation = memberactivation;
    }

    public String getWithdrawal() {
        return withdrawal;
    }

    public void setWithdrawal(String withdrawal) {
        this.withdrawal = withdrawal;
    }

    public String getDeposit() {
        return deposit;
    }

    public void setDeposit(String deposit) {
        this.deposit = deposit;
    }

    public String getSharedeposit() {
        return sharedeposit;
    }

    public void setSharedeposit(String sharedeposit) {
        this.sharedeposit = sharedeposit;
    }

    public String getLoanapplication() {
        return loanapplication;
    }

    public void setLoanapplication(String loanapplication) {
        this.loanapplication = loanapplication;
    }

    public String getLoanrepayment() {
        return loanrepayment;
    }

    public void setLoanrepayment(String loanrepayment) {
        this.loanrepayment = loanrepayment;
    }

    public String getBalance() {
        return balance;
    }

    public void setBalance(String balance) {
        this.balance = balance;
    }

    public String getMinistatement() {
        return ministatement;
    }

    public void setMinistatement(String ministatement) {
        this.ministatement = ministatement;
    }

    public String getReprintreceipt() {
        return reprintreceipt;
    }

    public void setReprintreceipt(String reprintreceipt) {
        this.reprintreceipt = reprintreceipt;
    }

    public String getAgentinfo() {
        return agentinfo;
    }

    public void setAgentinfo(String agentinfo) {
        this.agentinfo = agentinfo;
    }

    public String getBioregister() {
        return bioregister;
    }

    public void setBioregister(String bioregister) {
        this.bioregister = bioregister;
    }

    public String getGrouptransaction() {
        return grouptransaction;
    }

    public void setGrouptransaction(String grouptransaction) {
        this.grouptransaction = grouptransaction;
    }

    public String getGroupRegistration() {
        return groupRegistration;
    }

    public void setGroupRegistration(String groupRegistration) {
        this.groupRegistration = groupRegistration;
    }

    public String getAccountopenning() {
        return accountopenning;
    }

    public void setAccountopenning(String accountopenning) {
        this.accountopenning = accountopenning;
    }

    public String getEodreport() {
        return eodreport;
    }

    public void setEodreport(String eodreport) {
        this.eodreport = eodreport;
    }

    public String getPrintlasttrans() {
        return printlasttrans;
    }

    public void setPrintlasttrans(String printlasttrans) {
        this.printlasttrans = printlasttrans;
    }

    public String getPlatform() {
        return platform;
    }

    public void setPlatform(String platform) {
        this.platform = platform;
    }

    public String getCustomerchangepin() {
        return customerchangepin;
    }

    public void setCustomerchangepin(String customerchangepin) {
        this.customerchangepin = customerchangepin;
    }

    public String getCashtransfer() {
        return cashtransfer;
    }

    public void setCashtransfer(String cashtransfer) {
        this.cashtransfer = cashtransfer;
    }

    public String getUsesbioauthentication() {
        return usesbioauthentication;
    }

    public void setUsesbioauthentication(String usesbioauthentication) {
        this.usesbioauthentication = usesbioauthentication;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getCurrencycode() {
        return currencycode;
    }

    public void setCurrencycode(String currencycode) {
        this.currencycode = currencycode;
    }
}
