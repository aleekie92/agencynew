package com.coretec.agencyApplication.api.requests.newmodulesrequests;

import com.coretec.agencyApplication.api.requests.BaseRequest;
import com.coretec.agencyApplication.utils.Const;

public class GroupRepaymentRequest extends BaseRequest {

    public String corporate_no;
    public String groupcode;
    public String totalloanpaid;
    public String totalloanunpaid;
    public String totalsavingspaid;
    public String totalpaid;
    public String verifiedby;
    public String verifyphone;
    public String initializationtime;
    public String totalpresentmember;
    public String totalabstmember;
    public String groupimage;

    public String remarks;
    public String agentid;
    public String terminalid;
    public double longitude;
    public double latitude;
    public String date;

    public GroupRepaymentRequest(){
        super();
    }

    @Override
    public void setCredentials() {
        authorization_credentials.api_key = "12345";
        authorization_credentials.token = Const.getInstance().getLoginToken();
    }

}
