package com.coretec.agencyApplication.api.responses;

import java.io.Serializable;
import java.util.List;


public class EodReportsResponseOthers extends BaseResponse implements Serializable {

    private String error;
    private String transactiondate;
    private List<AccountsOtherClients> accounts;

    public EodReportsResponseOthers() {

    }

    public List<AccountsOtherClients> getAccounts() {
        return accounts;
    }

    public void setAccounts(List<AccountsOtherClients> accounts) {
        this.accounts = accounts;
    }

    public String getTransactiondate() {
        return transactiondate;
    }

    public void setTransactiondate(String transactiondate) {
        this.transactiondate = transactiondate;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }
}
