package com.coretec.agencyApplication.api.responses;

/**
 * Created by ofula on 18/06/24.
 */

public class AccountNameResponse extends BaseResponse {

    private String error;
    private String customer_name;
    private String phone_number;
    private String phoneno;

    public String getPhoneno() {
        return phoneno;
    }

    public void setPhoneno(String phoneno) {
        this.phoneno = phoneno;
    }

    public String getPhone_number() {
        return phone_number;
    }

    public void setPhone_number(String phone_number) {
        this.phone_number = phone_number;
    }

    public String getCustomer_name() {
        return customer_name;
    }

    public void setCustomer_name(String customer_name) {
        this.customer_name = customer_name;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public AccountNameResponse() {

    }

}
