package com.coretec.agencyApplication.api.requests;

import com.coretec.agencyApplication.utils.Const;

/**
 * Created by ofula on 18/06/24.
 */

public class AccountBalanceRequest  extends BaseRequest{
    public String corporate_no;
    public String msisdn;
    public String account_number;

    @Override
    public void setCredentials() {
        authorization_credentials.api_key = "12345";
        authorization_credentials.token = Const.getInstance().getRequestToken();
    }
}
