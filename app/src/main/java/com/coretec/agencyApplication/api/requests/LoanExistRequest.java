package com.coretec.agencyApplication.api.requests;

import com.coretec.agencyApplication.utils.Const;
import com.google.gson.annotations.SerializedName;

public class LoanExistRequest extends BaseRequest {

    @SerializedName("AgentCode")
    public String AgentCode;
    @SerializedName("GrowerNo")
    public String GrowerNo;
    @SerializedName("LoanProduct")
    public String LoanProduct;

    public LoanExistRequest(){
        super();
    }

    @Override
    public void setCredentials() {
        authorization_credentials.api_key = "12345";
        authorization_credentials.token = Const.getInstance().getLoginToken();
    }

}
