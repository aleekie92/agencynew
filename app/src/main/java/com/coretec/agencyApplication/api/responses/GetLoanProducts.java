package com.coretec.agencyApplication.api.responses;

import com.coretec.agencyApplication.model.LoanProducts;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class GetLoanProducts implements Serializable {

    @SerializedName("LoanProducts")
    private List<LoanProducts> loanProducts;

    public List<LoanProducts> getLoanProducts() {
        return loanProducts;
    }

    public void setLoanProducts(List<LoanProducts> loanProducts) {
        this.loanProducts = loanProducts;
    }
}
