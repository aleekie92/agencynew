package com.coretec.agencyApplication.api.responses;

import com.coretec.agencyApplication.model.QualifiedLoans;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class GetGrowersQualifiedLoans implements Serializable {

    @SerializedName("QualifiedLoans")
    private List<QualifiedLoans> qualifiedLoans ;

    public List<QualifiedLoans> getQualifiedLoans() {
        return qualifiedLoans;
    }

    public void setQualifiedLoans(List<QualifiedLoans> qualifiedLoans) {
        this.qualifiedLoans = qualifiedLoans;
    }
}
