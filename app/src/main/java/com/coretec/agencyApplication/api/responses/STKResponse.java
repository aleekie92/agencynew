package com.coretec.agencyApplication.api.responses;


public class STKResponse extends BaseResponse {

    private String error;
    private String transactiondate;

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getTransactiondate() {
        return transactiondate;
    }

    public void setTransactiondate(String transactiondate) {
        this.transactiondate = transactiondate;
    }

    public STKResponse() {

    }

}
