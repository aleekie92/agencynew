package com.coretec.agencyApplication.api;

import android.content.Context;
import android.util.Log;

import com.coretec.agencyApplication.activities.SplashActivity;
import com.coretec.agencyApplication.api.requests.GenerateTokenRequest;
import com.coretec.agencyApplication.api.responses.GenerateTokenResponse;
import com.coretec.agencyApplication.utils.Const;
import com.orhanobut.logger.Logger;

public class Server {

    public static void generateToken(final SplashActivity splashActivity) {

        final String TAG = "token generated";
        final Context ctx = null;
        GenerateTokenRequest generateToken = new GenerateTokenRequest();
        String URL = Api.MSACCO_AGENT + Api.GenerateToken;
        Log.i(TAG, URL);
        Log.i(TAG, generateToken.getBody().toString());

        Api.instance(splashActivity).request(URL, generateToken, new Api.RequestListener() {
            @Override
            public void onSuccess(String response) {
                Logger.json(response);
                GenerateTokenResponse generateTokenResponse = Api.instance(splashActivity).mGson.fromJson(response, GenerateTokenResponse.class);
                if (generateTokenResponse.is_successful) {
                    Const.getInstance().setLoginToken(generateTokenResponse.token);
                    splashActivity.onTokenGenerated(generateTokenResponse);
                } else {
                    splashActivity.onError("Wrong credentials");
                }
            }

            @Override
            public void onTokenExpired() {

            }

            @Override
            public void onError(String error) {
                splashActivity.onError(error);
            }
        });
    }

    public interface GenerateTokenListener {
        void onTokenGenerated(GenerateTokenResponse generateTokenResponse);
        void onError(String error);
    }
}
