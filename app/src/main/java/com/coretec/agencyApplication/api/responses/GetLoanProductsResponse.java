package com.coretec.agencyApplication.api.responses;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class GetLoanProductsResponse extends BaseResponse implements Serializable {

    private String error;
    @SerializedName("transactiondate")
    private String transactiondate;
    @SerializedName("GetLoanProducts")
    private List<GetLoanProducts> loanProducts;

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getTransactiondate() {
        return transactiondate;
    }

    public void setTransactiondate(String transactiondate) {
        this.transactiondate = transactiondate;
    }

    public List<GetLoanProducts> getLoanProducts() {
        return loanProducts;
    }

    public void setLoanProducts(List<GetLoanProducts> loanProducts) {
        this.loanProducts = loanProducts;
    }
}
