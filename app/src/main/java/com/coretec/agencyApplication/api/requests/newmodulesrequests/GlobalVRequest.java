package com.coretec.agencyApplication.api.requests.newmodulesrequests;

import com.coretec.agencyApplication.api.requests.BaseRequest;
import com.coretec.agencyApplication.utils.Const;

import java.util.Date;

public class GlobalVRequest extends BaseRequest {


    public String name;//
    public String idno;//
    public String phoneno;//
    public String gender;//
    public String amount;//
    public String dob;//
    public String passportphoto;//
    public String maritalstatus;//
    public String citizenship;//
    public String email;//
    public String corporate_no;//
    public String idfrontphoto;//
    public String idbackphoto;//
    public String signphoto;//


    public String nextofkin;//
    public String nextofkinid;//
    public String nextofkinphone;//
    public String relationship;//
    public Boolean msaccoregister;//
    public String leftring;//
    public String leftlittle;//
    public String rightring;//
    public String rightlittle;//
    public String transactiontype;//
    public String agentid;//
    public String terminalid;//
    public double longitude;//
    public double latitude;//
    public String date;//

    public String salutation;//
    public String nearestbranch;///////////////////////
    public String locality;//
    public String nearestmarket;///////////////////////

    public String applformfront;//
    public String applformback;//

    public String employeecode;//
    public String payrollno;//
    public String nextofkindob;//
    public String nextofkingender;//

    public GlobalVRequest(){
        super();
    }

    @Override
    public void setCredentials() {
        authorization_credentials.api_key = "12345";
        authorization_credentials.token = Const.getInstance().getLoginToken();
    }

}
