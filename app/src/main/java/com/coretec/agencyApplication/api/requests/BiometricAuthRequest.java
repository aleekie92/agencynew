package com.coretec.agencyApplication.api.requests;

import com.coretec.agencyApplication.api.Api;

/**
 * Created by ofula on 18/06/24.
 */

public class BiometricAuthRequest extends MemberAuthRequest {
    public String fingerprint;

    public BiometricAuthRequest() {
    }

    @Override
    public String getAuthenticationUrl() {
        return Api.AuthenticateCustomerBiometric;
    }

    public BiometricAuthRequest(String fingerprint) {
        this.fingerprint = fingerprint;
    }
}
