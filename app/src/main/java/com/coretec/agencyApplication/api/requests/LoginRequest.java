package com.coretec.agencyApplication.api.requests;

import com.coretec.agencyApplication.utils.Const;


public class LoginRequest extends BaseRequest{
    public String agentpin;
    public String agentcode;
    public String terminalid;

    public double longitude;
    public double latitude;

    public LoginRequest(){
        super();
    }
    @Override
    public void setCredentials() {
        authorization_credentials.api_key = "12345";
        authorization_credentials.token = Const.getInstance().getLoginToken();
    }
}
