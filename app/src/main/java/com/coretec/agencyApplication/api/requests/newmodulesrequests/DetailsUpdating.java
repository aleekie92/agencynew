package com.coretec.agencyApplication.api.requests.newmodulesrequests;

import com.coretec.agencyApplication.api.requests.BaseRequest;
import com.coretec.agencyApplication.utils.Const;

public class DetailsUpdating extends BaseRequest {


    public String idno;
    public String leftring;
    public String leftlittle;
    public String rightring;
    public String rightlittle;
    public String transactiontype;
    public String agentid;
    public String terminalid;

    public double longitude;
    public double latitude;

    public String date;
    public String corporate_no;

    public DetailsUpdating() {
        super();
    }

    @Override
    public void setCredentials() {
        authorization_credentials.api_key = "12345";
        authorization_credentials.token = Const.getInstance().getLoginToken();
    }
}
