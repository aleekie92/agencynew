package com.coretec.agencyApplication.api.responses;

import java.io.Serializable;

/**
 * Created by ofula on 18/06/24.
 */

public class AccBalance implements Serializable {

    private String account;
    private double amount;
    private String accountno;

    public AccBalance() {
    }

    public AccBalance(String Account, double Amount) {
        this.account = Account;
        this.amount = Amount;
    }

    public String getAccountno() {
        return accountno;
    }

    public void setAccountno(String accountno) {
        this.accountno = accountno;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        account = account;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        amount = amount;
    }
}
