package com.coretec.agencyApplication.api.responses.newmoduleresponses;

import com.coretec.agencyApplication.model.newmodulemodels.MLoanProducts;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class GetMLoanProducts implements Serializable {

    @SerializedName("product")
    private List<MLoanProducts> Mproduct;

    public List<MLoanProducts> getMproduct() {
        return Mproduct;
    }

    public void setMproduct(List<MLoanProducts> mproduct) {
        Mproduct = mproduct;
    }
}
