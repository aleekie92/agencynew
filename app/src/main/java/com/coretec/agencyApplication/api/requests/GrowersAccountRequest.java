package com.coretec.agencyApplication.api.requests;

import com.coretec.agencyApplication.utils.Const;

/**
 * Created by ofula on 18/07/15.
 */

public class GrowersAccountRequest extends BaseRequest {

    public String corporateno;
    public String growerno;
    public String transactiontype;
    public String agentid;
    public String terminalid;
    public  String phoneno;
    public double longitude;
    public double latitude;
    public String date;


    public GrowersAccountRequest(){
        super();
    }

    @Override
    public void setCredentials() {
        authorization_credentials.api_key = "12345";
        authorization_credentials.token = Const.getInstance().getLoginToken();
    }

}
