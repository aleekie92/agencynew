package com.coretec.agencyApplication.api.requests;

import com.coretec.agencyApplication.utils.Const;

public class PuBlockRequest extends BaseRequest {

    public String bitmap;
    public String idno;
    public String growersno;
    public String accountname;
    public String message;
    public String salescode;
    public String userid;
    public String description;
    public String transactiontype;
    public String agentid;
    public String terminalid;
    public double longitude;
    public double latitude;
    public String date;
    public  String phoneno;
    public String corporate_no;

    public PuBlockRequest(){
        super();
    }

    @Override
    public void setCredentials() {
        authorization_credentials.api_key = "12345";
        authorization_credentials.token = Const.getInstance().getLoginToken();
    }

}
