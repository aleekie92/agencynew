package com.coretec.agencyApplication.api.responses;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class GetLoanStatusResponse extends BaseResponse implements Serializable {

    private String error;
    @SerializedName("transactiondate")
    private String transactiondate;

    @SerializedName("loanstatus")
    private List<StatusList> loanstatus;

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getTransactiondate() {
        return transactiondate;
    }

    public void setTransactiondate(String transactiondate) {
        this.transactiondate = transactiondate;
    }

    public List<StatusList> getLoanstatus() {
        return loanstatus;
    }

    public void setLoanstatus(List<StatusList> loanstatus) {
        this.loanstatus = loanstatus;
    }
}
