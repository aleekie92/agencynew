package com.coretec.agencyApplication.api.requests;

import com.coretec.agencyApplication.utils.Const;

public class ChangePin extends BaseRequest{
    public String agentid;
    public String terminalid;
    public String old_pin;
    public String new_pin;
    public String accountidentifier;
    public String accountidentifiercode;

    public double longitude;
    public double latitude;
    public String date;

    public ChangePin(){
        super();
    }
    @Override
    public void setCredentials() {
        authorization_credentials.api_key = "12345";
        authorization_credentials.token = Const.getInstance().getLoginToken();
    }
}