package com.coretec.agencyApplication.api.requests;

import com.coretec.agencyApplication.utils.Const;

/**
 * Created by ofula on 18/06/24.
 */

public class RequestMemberName extends BaseRequest {

    public String corporateno;
    public String accountidentifier;
    public String accountcode;
    public String agentid;
    public String terminalid;

    public RequestMemberName() {
        super();
    }

    @Override
    public void setCredentials() {
        authorization_credentials.api_key = "12345";
        authorization_credentials.token = Const.getInstance().getLoginToken();
    }
}
