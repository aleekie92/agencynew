package com.coretec.agencyApplication.api.responses;

import com.coretec.agencyApplication.model.Employercode;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class GetEmployerCodeResponse extends BaseResponse implements Serializable {

    public String error;
    @SerializedName("employercode")
    private List<Employercode> employercodeList;

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public List<Employercode> getEmployercodeList() {
        return employercodeList;
    }

    public void setEmployercodeList(List<Employercode> employercodeList) {
        this.employercodeList = employercodeList;
    }
}
