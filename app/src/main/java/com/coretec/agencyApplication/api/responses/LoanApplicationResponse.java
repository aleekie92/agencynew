package com.coretec.agencyApplication.api.responses;

public class LoanApplicationResponse extends BaseResponse {

    private String error;
    //private String operationsuccess;
    private String receiptNo;
    private String transactiondate;

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    /*public String getOperationsuccess() {
        return operationsuccess;
    }

    public void setOperationsuccess(String operationsuccess) {
        this.operationsuccess = operationsuccess;
    }*/

    public String getReceiptNo() {
        return receiptNo;
    }

    public void setReceiptNo(String receiptNo) {
        this.receiptNo = receiptNo;
    }

    public String getTransactiondate() {
        return transactiondate;
    }

    public void setTransactiondate(String transactiondate) {
        this.transactiondate = transactiondate;
    }
}
