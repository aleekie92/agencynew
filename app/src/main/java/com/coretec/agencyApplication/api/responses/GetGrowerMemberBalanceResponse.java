package com.coretec.agencyApplication.api.responses;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class GetGrowerMemberBalanceResponse extends BaseResponse implements Serializable {

    private String error;
    @SerializedName("transactiondate")
    private String transactiondate;
    @SerializedName("getgrowermemberbalance")
    private List<getgrowermemberbalance> getgrowermemberbalances;

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getTransactiondate() {
        return transactiondate;
    }

    public void setTransactiondate(String transactiondate) {
        this.transactiondate = transactiondate;
    }

    public List<getgrowermemberbalance> getGetgrowermemberbalances() {
        return getgrowermemberbalances;
    }

    public void setGetgrowermemberbalances(List<getgrowermemberbalance> getgrowermemberbalances) {
        this.getgrowermemberbalances = getgrowermemberbalances;
    }
}
