package com.coretec.agencyApplication.api.responses;

import com.coretec.agencyApplication.model.agentdetails;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class GetAgentAccountInfoResponse extends BaseResponse implements Serializable {

    public String error;
    public String sacconame;
    public String corporateno;
    public String saccomotto;
    public String date;
    @SerializedName("transactiondate")
    private String transactiondate;
    @SerializedName("agentdetails")
    private List<agentdetails> agentdetailsList;
    public GetAgentAccountInfoResponse() {

    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getTransactiondate() {
        return transactiondate;
    }

    public void setTransactiondate(String transactiondate) {
        this.transactiondate = transactiondate;
    }

    public List<agentdetails> getAgentdetailsList() {
        return agentdetailsList;
    }

    public void setAgentdetailsList(List<agentdetails> agentdetailsList) {
        this.agentdetailsList = agentdetailsList;
    }

    public String getSacconame() {
        return sacconame;
    }

    public void setSacconame(String sacconame) {
        this.sacconame = sacconame;
    }

    public String getCorporateno() {
        return corporateno;
    }

    public void setCorporateno(String corporateno) {
        this.corporateno = corporateno;
    }

    public String getSaccomotto() {
        return saccomotto;
    }

    public void setSaccomotto(String saccomotto) {
        this.saccomotto = saccomotto;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
