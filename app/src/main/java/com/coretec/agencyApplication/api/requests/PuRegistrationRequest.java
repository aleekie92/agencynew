package com.coretec.agencyApplication.api.requests;

import com.coretec.agencyApplication.utils.Const;

public class PuRegistrationRequest extends BaseRequest {

    public String idno;
    public String growersno;
    public String phoneno;
    public String fullnames;
    public String transactiontype;
    public String agentid;
    public String terminalid;
    public double longitude;
    public double latitude;
    public String date;
    public String corporate_no;

    public PuRegistrationRequest(){
        super();
    }

    @Override
    public void setCredentials() {
        authorization_credentials.api_key = "12345";
        authorization_credentials.token = Const.getInstance().getLoginToken();
    }

}
