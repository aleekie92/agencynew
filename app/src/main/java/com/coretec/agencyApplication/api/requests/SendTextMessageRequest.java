package com.coretec.agencyApplication.api.requests;

import com.coretec.agencyApplication.utils.Const;

public class SendTextMessageRequest extends BaseRequest {

    public String corporateno;
    public String telephone;
    public String text;
    public String agentid;
    public String terminalid;
    public double longitude;
    public double latitude;
    public String date;

    public SendTextMessageRequest(){
        super();
    }

    @Override
    public void setCredentials() {
        authorization_credentials.api_key = "12345";
        authorization_credentials.token = Const.getInstance().getLoginToken();
    }

}
