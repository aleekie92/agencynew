package com.coretec.agencyApplication.api.responses.newmoduleresponses;

import com.coretec.agencyApplication.api.responses.BaseResponse;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class GetOtherLoansInfoResponse extends BaseResponse implements Serializable {
    private String error;
    @SerializedName("transactiondate")
    private String transactiondate;
    //@SerializedName("GetLoansInfo")
   // private List<GetLoansInfo> loansInfo;


    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getTransactiondate() {
        return transactiondate;
    }

    public void setTransactiondate(String transactiondate) {
        this.transactiondate = transactiondate;
    }
}
