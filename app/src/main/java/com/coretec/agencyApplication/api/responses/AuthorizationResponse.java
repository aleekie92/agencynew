package com.coretec.agencyApplication.api.responses;

import java.io.Serializable;


public class AuthorizationResponse implements Serializable {
    public String token;
    public int status_code;
}
