package com.coretec.agencyApplication.api.requests;

public abstract class BaseRequest {
    public AuthorizationCredentials authorization_credentials =  new AuthorizationCredentials();

    public String url = "";

    public BaseRequest() {
        setCredentials();
    }
    public abstract void setCredentials();

    public Object getBody(){
        return this;
    }

}
