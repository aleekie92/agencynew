package com.coretec.agencyApplication.api.responses.newmoduleresponses;

import com.coretec.agencyApplication.api.responses.BaseResponse;
import com.coretec.agencyApplication.model.newmodulemodels.GroupObject;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class GetGroupResponse extends BaseResponse implements Serializable {

    private String error;
    private String sacconame;
    private String transactiondate;
    private String saccoName;
    @SerializedName("microgroups")
    private List<GroupObject> microgroups;



    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getSacconame() {
        return sacconame;
    }

    public void setSacconame(String sacconame) {
        this.sacconame = sacconame;
    }

    public String getTransactiondate() {
        return transactiondate;
    }

    public void setTransactiondate(String transactiondate) {
        this.transactiondate = transactiondate;
    }

    public String getSaccoName() {
        return saccoName;
    }

    public void setSaccoName(String saccoName) {
        this.saccoName = saccoName;
    }

    public List<GroupObject> getMicrogroups() {
        return microgroups;
    }

    public void setMicrogroups(List<GroupObject> microgroups) {
        this.microgroups = microgroups;
    }
}
