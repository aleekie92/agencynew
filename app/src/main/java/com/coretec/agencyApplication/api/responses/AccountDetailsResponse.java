package com.coretec.agencyApplication.api.responses;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * Created by ofula on 18/06/24.
 */

public class AccountDetailsResponse extends BaseResponse implements Serializable {

    private String error;
    @SerializedName("transactiondate")
    private String transactiondate;
    @SerializedName("saccoaccounts")
    private List<SaccoAccounts> saccoAccounts;

    public AccountDetailsResponse() {

    }

    public String getTransactiondate() {
        return transactiondate;
    }

    public void setTransactiondate(String transactiondate) {
        this.transactiondate = transactiondate;
    }

    public List<SaccoAccounts> getSaccoAccounts() {
        return saccoAccounts;
    }

    public void setSaccoAccounts(List<SaccoAccounts> saccoAccounts) {
        this.saccoAccounts = saccoAccounts;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }
}
