package com.coretec.agencyApplication.api.requests.newmodulesrequests;

import com.coretec.agencyApplication.api.requests.BaseRequest;
import com.coretec.agencyApplication.utils.Const;
import com.coretec.agencyApplication.utils.RequiresAuthorization;

public class LApplicationRequest extends BaseRequest implements RequiresAuthorization {

    public String corporate_no;
    public String idno;
    public String loantype;
    public double amount;
    public String phonenumber;
    public String agentid;
    public String loanapplicationformfrontphoto;
    public String loanapplicationformbackphoto;
    public String terminalid;
    public double longitude;
    public double latitude;
    public String date;

    public LApplicationRequest(){
        super();
    }

    @Override
    public void setCredentials() {
        authorization_credentials.api_key = "12345";
        authorization_credentials.token = Const.getInstance().getLoginToken();
    }
    //to have am

    @Override
    public String getPhoneNumber() {
        return phonenumber;
    }
}
