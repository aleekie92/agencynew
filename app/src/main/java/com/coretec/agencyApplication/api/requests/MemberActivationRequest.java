package com.coretec.agencyApplication.api.requests;

import com.coretec.agencyApplication.utils.Const;

/**
 * Created by ofula on 18/06/24.
 */

public class MemberActivationRequest extends BaseRequest {

    public String corporate_no;
    public String accountidentifier;
    public String newpin;
    public String confirmpin;
    public String accountidentifiercode;
    public String agentid;
    public String terminalid;
    public double longitude;
    public double latitude;
    public String date;
    public String PhoneNumber;

    public MemberActivationRequest() {
        super();
    }

    @Override
    public void setCredentials() {
        authorization_credentials.api_key = "12345";
        authorization_credentials.token = Const.getInstance().getLoginToken();
    }
}
