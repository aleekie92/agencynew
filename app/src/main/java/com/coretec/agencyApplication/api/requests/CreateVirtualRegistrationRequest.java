package com.coretec.agencyApplication.api.requests;

import com.coretec.agencyApplication.utils.Const;

import java.util.Date;

public class CreateVirtualRegistrationRequest extends BaseRequest {

    public String growerno;
    public String accountname;
    public String idno;
    public String phoneno;
    public String gender;
    public Date dob;
    public String passportphoto;
    public String idfrontphoto;
    public String idbackphoto;
    public String nextofkin;
    public String relationship;
    public Boolean puregister;
    public String bankandbranchcode;
    public String bankaccountno;
    public String bankcardphoto;
    public String leftring;
    public String leftlittle;
    public String rightring;
    public String rightlittle;
    public String transactiontype;
    public String agentid;
    public String terminalid;
    public double longitude;
    public double latitude;
    public String date;
    public String corporate_no;
    public String saccocode;
    public String saccoaccno;
    public String saccocardphoto;
    public String pureglanguage;
    public String nextofkinid;


    public CreateVirtualRegistrationRequest(){
        super();
    }

    @Override
    public void setCredentials() {
        authorization_credentials.api_key = "12345";
        authorization_credentials.token = Const.getInstance().getLoginToken();
    }

}
