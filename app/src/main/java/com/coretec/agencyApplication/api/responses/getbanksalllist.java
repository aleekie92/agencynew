package com.coretec.agencyApplication.api.responses;

import com.coretec.agencyApplication.model.BanksList;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class getbanksalllist implements Serializable {

    @SerializedName("BanksList")
    private List<BanksList> banksLists;

    public List<BanksList> getBanksLists() {
        return banksLists;
    }

    public void setBanksLists(List<BanksList> banksLists) {
        this.banksLists = banksLists;
    }
}
