package com.coretec.agencyApplication.api.requests;

import com.coretec.agencyApplication.utils.Const;

/**
 * Created by ofula on 18/06/24.
 */

public class LoanRepaymentRequest extends BaseRequest /*implements RequiresAuthorization*/ {

    public String corporate_no;
    public String accountidentifier;
    public String accountidentifiercode;
    public String loan_no;
    public double repayment_amount;
    public String agentid;
    public String terminalid;
    public double longitude;
    public double latitude;
    public String date;


    public LoanRepaymentRequest() {
        super();
    }

    @Override
    public void setCredentials() {
        authorization_credentials.api_key = "12345";
        authorization_credentials.token = Const.getInstance().getLoginToken();
    }

    //Enable comments if you want to allow authorisations
    /*@Override
    public String getPhoneNumber() {
        return accountidentifier;
    }*/
}
