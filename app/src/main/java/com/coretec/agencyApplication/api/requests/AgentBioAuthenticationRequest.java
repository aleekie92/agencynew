package com.coretec.agencyApplication.api.requests;

import com.coretec.agencyApplication.utils.Const;

public class AgentBioAuthenticationRequest extends BaseRequest {

    public String agentcode;
    public String agentpin;
    public String terminalid;

    public AgentBioAuthenticationRequest() {
        super();
    }

    @Override
    public void setCredentials() {
        authorization_credentials.api_key = "12345";
        authorization_credentials.token = Const.getInstance().getLoginToken();
    }

}
