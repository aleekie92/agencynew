package com.coretec.agencyApplication.api.responses;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class GetGrowersQualifiedLoansResponse extends BaseResponse implements Serializable {

    private String error;
    @SerializedName("transactiondate")
    private String transactiondate;
    @SerializedName("GetGrowersQualifiedLoans")
    private List<GetGrowersQualifiedLoans> getGrowersQualifiedLoans;

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getTransactiondate() {
        return transactiondate;
    }

    public void setTransactiondate(String transactiondate) {
        this.transactiondate = transactiondate;
    }

    public List<GetGrowersQualifiedLoans> getGetGrowersQualifiedLoans() {
        return getGrowersQualifiedLoans;
    }

    public void setGetGrowersQualifiedLoans(List<GetGrowersQualifiedLoans> getGrowersQualifiedLoans) {
        this.getGrowersQualifiedLoans = getGrowersQualifiedLoans;
    }
}
