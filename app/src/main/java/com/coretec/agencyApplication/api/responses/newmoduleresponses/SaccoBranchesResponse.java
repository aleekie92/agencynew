package com.coretec.agencyApplication.api.responses.newmoduleresponses;

import com.coretec.agencyApplication.api.responses.BaseResponse;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class SaccoBranchesResponse extends BaseResponse implements Serializable {

    private String error;
    @SerializedName("Branches")
    private List<Branches> branches;

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public List<Branches> getBranches() {
        return branches;
    }

    public void setBranches(List<Branches> branches) {
        this.branches = branches;
    }

    public SaccoBranchesResponse() {

    }


}
