package com.coretec.agencyApplication.api.requests;

import com.coretec.agencyApplication.utils.Const;

public class MpesaPaybillRequest extends BaseRequest {

    public String mpesareceipt;
    public String growersno;
    public String loanproduccode;
    public String phoneno;
    public String amount;
    public String description;
    public boolean needschange;
    public String message;
    public String transactiontype;
    public String agentid;
    public String terminalid;
    public double longitude;
    public double latitude;
    public String date;
    public String corporate_no;
    public  String accNo;

    public MpesaPaybillRequest() {
        super();
    }

    @Override
    public void setCredentials() {
        authorization_credentials.api_key = "12345";
        authorization_credentials.token = Const.getInstance().getLoginToken();
    }

}
