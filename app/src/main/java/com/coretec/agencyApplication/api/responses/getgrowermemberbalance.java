package com.coretec.agencyApplication.api.responses;

import com.coretec.agencyApplication.model.GrowerMemberBalance;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class getgrowermemberbalance implements Serializable {

    @SerializedName("GrowerMemberBalance")
    private List<GrowerMemberBalance> growerMemberBalances;

    public List<GrowerMemberBalance> getGrowerMemberBalances() {
        return growerMemberBalances;
    }

    public void setGrowerMemberBalances(List<GrowerMemberBalance> growerMemberBalances) {
        this.growerMemberBalances = growerMemberBalances;
    }
}
