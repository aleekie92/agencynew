package com.coretec.agencyApplication.api.responses.newmoduleresponses;

import com.coretec.agencyApplication.api.responses.BaseResponse;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class GetMLoanProductsResponse extends BaseResponse implements Serializable {

    private String error;
    @SerializedName("product")
    private List<GetMLoanProducts> mloanProducts;

}
