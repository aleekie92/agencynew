package com.coretec.agencyApplication.api.responses;

public class GetQualifiedKgsResponse extends BaseResponse {

    private String error;
    private String qualifiedkgs;
    private String transactiondate;

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getQualifiedkgs() {
        return qualifiedkgs;
    }

    public void setQualifiedkgs(String qualifiedkgs) {
        this.qualifiedkgs = qualifiedkgs;
    }

    public String getTransactiondate() {
        return transactiondate;
    }

    public void setTransactiondate(String transactiondate) {
        this.transactiondate = transactiondate;
    }
}
