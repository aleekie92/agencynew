package com.coretec.agencyApplication.api.responses;

import java.io.Serializable;

public class LastFiveList implements Serializable {

    private double amount;
    private String transactiontype;

    public LastFiveList() {
    }

    public LastFiveList(String referenceno, double amount, String transactiontype) {
        this.amount = amount;
        this.transactiontype = transactiontype;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public String getTransactiontype() {
        return transactiontype;
    }

    public void setTransactiontype(String transactiontype) {
        this.transactiontype = transactiontype;
    }
}
