package com.coretec.agencyApplication.api.responses;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class GetGrowersPaybillTransactionsResponse extends BaseResponse implements Serializable {

    private String error;
    @SerializedName("transactiondate")
    private String transactiondate;
    @SerializedName("Growers")
    private List<Growers1> growers1;

    public GetGrowersPaybillTransactionsResponse() {

    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getTransactiondate() {
        return transactiondate;
    }

    public void setTransactiondate(String transactiondate) {
        this.transactiondate = transactiondate;
    }

    public List<Growers1> getGrowers1() {
        return growers1;
    }

    public void setGrowers1(List<Growers1> growers1) {
        this.growers1 = growers1;
    }
}
