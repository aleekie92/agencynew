package com.coretec.agencyApplication.api.requests;

import com.coretec.agencyApplication.utils.Const;

public class GetMatchLogsRequest extends BaseRequest {

    public boolean match;
    public String agentid;
    public String corporate_no;
    public int transactiontype;
    public String growerno;
    public String terminalid;
    public double longitude;
    public double latitude;
    public String date;

    public GetMatchLogsRequest() {
        super();
    }

    @Override
    public void setCredentials() {
        authorization_credentials.api_key = "12345";
        authorization_credentials.token = Const.getInstance().getLoginToken();
    }
}
