package com.coretec.agencyApplication.api.responses;

import java.io.Serializable;


public class BaseResponse implements Serializable {
    public AuthorizationResponse authorization_response;
    public boolean is_successful = false;
    public boolean operationsuccess = false;
}
