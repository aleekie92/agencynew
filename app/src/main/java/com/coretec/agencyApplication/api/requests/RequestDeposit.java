package com.coretec.agencyApplication.api.requests;

import com.coretec.agencyApplication.utils.Const;
import com.google.gson.annotations.SerializedName;

/**
 * Created by ofula on 18/06/24.
 */

public class RequestDeposit extends BaseRequest {

    @SerializedName("AgentId")
    public String agentid;
    @SerializedName("idno")
    public String idno;
    public String corporate_no;
    public String memberaccno;
    public String depositorsname;
    @SerializedName("AccountType")
    public String AccountType;
    public String description;
    public String depositorsphoneno;
    public int amount;
    public String terminalid;
    public String transactiontype;
    public double longitude;
    public double latitude;
    public String date;

    public RequestDeposit() {
        super();
    }

    @Override
    public void setCredentials() {
        authorization_credentials.api_key = "12345";
        authorization_credentials.token = Const.getInstance().getLoginToken();
    }
}
