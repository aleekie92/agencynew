package com.coretec.agencyApplication.api.responses;

public class PinChangeResponse extends BaseResponse {
    private String error;
    private boolean pin_valid;

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public boolean isPin_valid() {
        return pin_valid;
    }

    public void setPin_valid(boolean pin_valid) {
        this.pin_valid = pin_valid;
    }
}
