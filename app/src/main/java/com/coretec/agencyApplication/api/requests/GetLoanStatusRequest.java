package com.coretec.agencyApplication.api.requests;

import com.coretec.agencyApplication.utils.Const;

public class GetLoanStatusRequest extends BaseRequest {

    //public String corporateno;
    public String growerno;
    public String agentid;
    public String terminalid;
    public double longitude;
    public double latitude;
    public String date;

    public GetLoanStatusRequest(){
        super();
    }

    @Override
    public void setCredentials() {
        authorization_credentials.api_key = "12345";
        authorization_credentials.token = Const.getInstance().getLoginToken();
    }

}
