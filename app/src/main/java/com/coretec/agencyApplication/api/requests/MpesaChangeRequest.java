package com.coretec.agencyApplication.api.requests;

import com.coretec.agencyApplication.utils.Const;

public class MpesaChangeRequest extends BaseRequest {

    public String id;
    public String growersno;
    public String accountname;
    public String phoneno;
    public String salescode;
    public String userid;
    public String reason;
    public String puregister;
    public String transactiontype;
    public String agentid;
    public String terminalid;
    public double longitude;
    public double latitude;
    public String date;
    public String corporate_no;

    public MpesaChangeRequest(){
        super();
    }

    @Override
    public void setCredentials() {
        authorization_credentials.api_key = "12345";
        authorization_credentials.token = Const.getInstance().getLoginToken();
    }

}
