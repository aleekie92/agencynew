package com.coretec.agencyApplication.api.requests;

import com.coretec.agencyApplication.utils.Const;

public class SignaturePhotoRequest extends BaseRequest {

    public String identifier;
    public String identifiercode;
    public String agentid;
    public String terminalid;

    public SignaturePhotoRequest(){
        super();
    }

    @Override
    public void setCredentials() {
        authorization_credentials.api_key = "12345";
        authorization_credentials.token = Const.getInstance().getLoginToken();
    }

}
