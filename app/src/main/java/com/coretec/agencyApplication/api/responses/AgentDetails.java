package com.coretec.agencyApplication.api.responses;

import java.io.Serializable;

/**
 * Created by ofula on 18/06/24.
 */

public class AgentDetails implements Serializable {

    private String accountname;
    private String proposedname;
    private String agentaccno;
    private String location;
    private String accountbalance;
    private String accountcommissionbalance;

    public AgentDetails() {
    }

    public AgentDetails(String accountname, String proposedname, String agentaccno, String location, String accountbalance, String accountcommissionbalance) {
        this.accountname = accountname;
        this.proposedname = proposedname;
        this.agentaccno = agentaccno;
        this.location = location;
        this.accountbalance = accountbalance;
        this.accountcommissionbalance = accountcommissionbalance;
    }

    public String getAccountname() {
        return accountname;
    }

    public void setAccountname(String accountname) {
        this.accountname = accountname;
    }

    public String getProposedname() {
        return proposedname;
    }

    public void setProposedname(String proposedname) {
        this.proposedname = proposedname;
    }

    public String getAgentaccno() {
        return agentaccno;
    }

    public void setAgentaccno(String agentaccno) {
        this.agentaccno = agentaccno;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getAccountbalance() {
        return accountbalance;
    }

    public void setAccountbalance(String accountbalance) {
        this.accountbalance = accountbalance;
    }

    public String getAccountcommissionbalance() {
        return accountcommissionbalance;
    }

    public void setAccountcommissionbalance(String accountcommissionbalance) {
        this.accountcommissionbalance = accountcommissionbalance;
    }
}
