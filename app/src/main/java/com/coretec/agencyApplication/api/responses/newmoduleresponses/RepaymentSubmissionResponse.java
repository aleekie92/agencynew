package com.coretec.agencyApplication.api.responses.newmoduleresponses;

import com.coretec.agencyApplication.api.responses.BaseResponse;

public class RepaymentSubmissionResponse extends BaseResponse {

    private String error;
    private String receiptno;
    private String transactiondate;
    private String corporate_no;
    private String sacconame;

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getReceiptno() {
        return receiptno;
    }

    public void setReceiptno(String receiptno) {
        this.receiptno = receiptno;
    }

    public String getTransactiondate() {
        return transactiondate;
    }

    public void setTransactiondate(String transactiondate) {
        this.transactiondate = transactiondate;
    }

    public String getCorporate_no() {
        return corporate_no;
    }

    public void setCorporate_no(String corporate_no) {
        this.corporate_no = corporate_no;
    }

    public String getSacconame() {
        return sacconame;
    }

    public void setSacconame(String sacconame) {
        this.sacconame = sacconame;
    }
}
