package com.coretec.agencyApplication.api.responses;

import com.coretec.agencyApplication.model.GuarantorObject;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class GetGuarantorsResponse extends BaseResponse implements Serializable {

    private String applicantgrowerno;
    private String applicantidno;
    private String applicantregkgs;
    private String producttype;
    private String appliedamt;
    private String disbursmenttype;
    private String approvalstatus;
    @SerializedName("guarantorlist")
    private List<GuarantorObject> guarantorObject;

    public String getApplicantgrowerno() {
        return applicantgrowerno;
    }

    public void setApplicantgrowerno(String applicantgrowerno) {
        this.applicantgrowerno = applicantgrowerno;
    }

    public String getApplicantidno() {
        return applicantidno;
    }

    public void setApplicantidno(String applicantidno) {
        this.applicantidno = applicantidno;
    }

    public String getApplicantregkgs() {
        return applicantregkgs;
    }

    public void setApplicantregkgs(String applicantregkgs) {
        this.applicantregkgs = applicantregkgs;
    }

    public String getProducttype() {
        return producttype;
    }

    public void setProducttype(String producttype) {
        this.producttype = producttype;
    }

    public String getAppliedamt() {
        return appliedamt;
    }

    public void setAppliedamt(String appliedamt) {
        this.appliedamt = appliedamt;
    }

    public String getDisbursmenttype() {
        return disbursmenttype;
    }

    public void setDisbursmenttype(String disbursmenttype) {
        this.disbursmenttype = disbursmenttype;
    }

    public String getApprovalstatus() {
        return approvalstatus;
    }

    public void setApprovalstatus(String approvalstatus) {
        this.approvalstatus = approvalstatus;
    }

    public List<GuarantorObject> getGuarantorObject() {
        return guarantorObject;
    }

    public void setGuarantorObject(List<GuarantorObject> guarantorObject) {
        this.guarantorObject = guarantorObject;
    }
}
