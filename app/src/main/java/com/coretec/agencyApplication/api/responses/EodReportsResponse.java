package com.coretec.agencyApplication.api.responses;

import java.io.Serializable;
import java.util.List;


public class EodReportsResponse extends BaseResponse implements Serializable {

    private String error;
    private String transactiondate;
    private List<Accounts> accounts;

    public EodReportsResponse() {

    }

    public List<Accounts> getAccounts() {
        return accounts;
    }

    public void setAccounts(List<Accounts> accounts) {
        this.accounts = accounts;
    }

    public String getTransactiondate() {
        return transactiondate;
    }

    public void setTransactiondate(String transactiondate) {
        this.transactiondate = transactiondate;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }
}
