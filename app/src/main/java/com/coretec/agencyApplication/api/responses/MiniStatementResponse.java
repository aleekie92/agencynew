package com.coretec.agencyApplication.api.responses;

import com.coretec.agencyApplication.model.Transactions;

import java.io.Serializable;
import java.util.List;

/**
 * Created by ofula on 18/06/24.
 */

public class MiniStatementResponse extends BaseResponse implements Serializable {

    private String error;
    private String receiptno;
    private String customer_name;
    private String transactiontype;
    private String sacconame;
    private String transactiondate;
    private List<Transactions> transactions;

    public MiniStatementResponse() {

    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getReceiptno() {
        return receiptno;
    }

    public void setReceiptno(String receiptno) {
        this.receiptno = receiptno;
    }

    public String getCustomer_name() {
        return customer_name;
    }

    public void setCustomer_name(String customer_name) {
        this.customer_name = customer_name;
    }

    public String getTransactiontype() {
        return transactiontype;
    }

    public void setTransactiontype(String transactiontype) {
        this.transactiontype = transactiontype;
    }

    public String getSacconame() {
        return sacconame;
    }

    public void setSacconame(String sacconame) {
        this.sacconame = sacconame;
    }

    public String getTransactiondate() {
        return transactiondate;
    }

    public void setTransactiondate(String transactiondate) {
        this.transactiondate = transactiondate;
    }

    public List<Transactions> getTransactions() {
        return transactions;
    }

    public void setTransactions(List<Transactions> transactions) {
        this.transactions = transactions;
    }
}
