package com.coretec.agencyApplication.api.responses;

import com.coretec.agencyApplication.model.AccountDetails;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * Created by ofula on 18/06/24.
 */

public class SaccoAccounts implements Serializable {

    private String sacconame;
    private String corporateno;
    @SerializedName("accountdetails")
    private List<AccountDetails> accountDetails;

    public String getSacconame() {
        return sacconame;
    }

    public void setSacconame(String sacconame) {
        this.sacconame = sacconame;
    }

    public String getCorporateno() {
        return corporateno;
    }

    public void setCorporateno(String corporateno) {
        this.corporateno = corporateno;
    }

    public List<AccountDetails> getAccountDetails() {
        return accountDetails;
    }

    public void setAccountDetails(List<AccountDetails> accountDetails) {
        this.accountDetails = accountDetails;
    }
}
