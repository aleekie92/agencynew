package com.coretec.agencyApplication.api;
import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.AppCompatButton;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.coretec.agencyApplication.R;
import com.coretec.agencyApplication.api.requests.BaseRequest;
import com.coretec.agencyApplication.api.requests.MemberAuthRequest;
import com.coretec.agencyApplication.api.requests.PinAuthRequest;
import com.coretec.agencyApplication.api.responses.MemberAuthResponse;
import com.coretec.agencyApplication.fragments.newmodule.NewLoanApplicationFragment;
import com.coretec.agencyApplication.utils.AppController;
import com.coretec.agencyApplication.utils.PreferenceFileKeys;
import com.coretec.agencyApplication.utils.RequiresAuthorization;
import com.coretec.agencyApplication.utils.SharedPrefs;
import com.coretec.agencyApplication.utils.Utils;
import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;
public class Api {
     //public static String MSACCO_AGENT = "http://172.18.6.8:35088/api/AgencyBanking/"; //new environment1
      public static String MSACCO_AGENT = "https://197.248.9.253:2020/agency/api//AgencyBanking/"; //cloud.
     //public static String MSACCO_AGENT = "http://172.16.11.145:3052/api/AgencyBanking/"; // gflinternet live
     //public static String MSACCO_AGENT = "http://172.16.11.145:3051/api/AgencyBanking/"; // gflinternet test

    public static String GenerateToken = "GenerateToken";
    public static String AgentAuthentication = "AgentAuthentication";
    public static String GetMiniStatement = "GetMiniStatement";
    public static String GetBalanceInquiry = "GetBalanceEnquiry";
    public static String FundsDeposits = "FundsDeposits";
    public static String STKPush = "STKPush";
    public static String AgentFundsWithdrawal = "AgentFundsWithdrawal";
    public static String AuthenticateCustomerLogin = "AuthenticateCustomerLogin";
    public static String GetAllMemberSaccoDetails = "GetAllMemberSaccoDetails";
    public static String GetEODReport = "GetEODReport";
    public static String GetMemberActiveLoans = "GetMemberActiveLoans";
    public static String LoanRepayment = "LoanRepayment";
    public static String GetMembername = "GetMembername";
    public static String MemberActivation = "MemberActivation";
    public static String ShareDeposits = "ShareDeposits";
    public static String GetAgentAccountInfo = "GetAgentAccountInfo";
    public static String AuthenticateCustomerBiometric = "AuthenticateCustomerBiometric";
    public static String SendTextMessage = "SendTextMessage";
    public static String GetAgentLastTransaction = "GetAgentLastTransaction";
    //GFL Apis
    public static String GetGrowersDetails = "GetGrowersDetails";
    //new modules
    public static String CreateVirtualRegistration = "CreateVirtualRegistration";
    public static String GetSaccoBranch = "GetSaccoBranch";
    public static String GetMicroGroups = "GetMicroGroups";
    public static String MicroGroupTransaction = "MicroGroupTransaction";
    public static String BioRegistration = "BioRegistration";
    public static String GetmLoanProducts = "GetmLoanProducts";
    public static String mloanApplication = "mloanApplication";
    public static String GetQualifiedAmount = "GetQualifiedAmount";
    public static String AccountOpening = "AccountOpening";
    public static String GetFourFingerPrint = "GetFourFingerPrint";
    public static String ChangePinAgent = "ChangePinAgent";
    public static String AgentPINChange = "AgentPINChange";
    public static String GetLoansInfo = "GetLoansInfo";
    public static String getTransaction = "getTransaction";
    public static String PhotoSignature = "PhotoSignature";
    public static String GetFingerPrint = "GetFingerPrint";
    public static String DynamicMenu = "DynamicMenu";


    private Context mContext;
    public  static  String pinn;
    public Gson mGson = new GsonBuilder()
            .setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
            .setLenient()
            .serializeNulls()
            .create();
    private boolean isFingerprintOpened = false;

    SharedPreferences sharedPreferences;

    private Api() {

    }

    private Api(Context mContext) {
        this.mContext = mContext;
    }

    final void memberAuthDialog(final RequiresAuthorization requiresAuthorizationRequest, final AuthRequestListener authRequestListener) {
        AlertDialog.Builder mBuilder = new AlertDialog.Builder(mContext);
        View mView = ((Activity) mContext).getLayoutInflater().inflate(R.layout.member_auth, null);
        final EditText mPin = mView.findViewById(R.id.member_auth__password);
        final TextInputLayout layoutPin = mView.findViewById(R.id.layout_member_auth__password);

        mBuilder.setView(mView);

        final AppCompatButton btnConfirmMemberAuth = mView.findViewById(R.id.btn_confirm_member_auth);
        final AppCompatButton btnFingerPrintMemberAuth = mView.findViewById(R.id.btn_fingerprint_member_auth);
        final LinearLayout mFingerPrintAuth = mView.findViewById(R.id.fingerprint_auth);
        final LinearLayout mPinAuth = mView.findViewById(R.id.pin_auth);


        final AlertDialog dialog = mBuilder
         .create();

        dialog.setCancelable(false);
        dialog.show();

        btnConfirmMemberAuth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //dialog.dismiss();
                final String pin = mPin.getText().toString().trim();

                if (pin.isEmpty()) {
                    mPin.setError("Please Enter Your Pin");

                } else if (pin.trim().length() > 4 || pin.trim().length() < 4) {
                    mPin.setError("Enter the 4 digit pin");
                } else {
                    dialog.dismiss();
                    authMemberRequest(requiresAuthorizationRequest, authRequestListener, new PinAuthRequest(pin));
                }
            }
        });
    }

    public void authMemberRequest(RequiresAuthorization requiresAuthorizationRequest,
                                  final AuthRequestListener authRequestListener, @NonNull MemberAuthRequest authRequest) {
        String TAG = "Auth Member request";
        sharedPreferences = mContext.getSharedPreferences(PreferenceFileKeys.PREFS_FILE, Context.MODE_PRIVATE);
        String URL = Api.MSACCO_AGENT + authRequest.getAuthenticationUrl();

        authRequest.accountidentifier = requiresAuthorizationRequest.getPhoneNumber();
        authRequest.accountidentifiercode = "0";
        authRequest.agentid = sharedPreferences.getString(PreferenceFileKeys.AGENT_ID, "");
        authRequest.terminalid = Utils.getIMEI(mContext);
        authRequest.longitude = Utils.getLong(mContext);
        authRequest.latitude = Utils.getLat(mContext);
        authRequest.date = Utils.getFormattedDate();

        Log.e(TAG, authRequest.getBody().toString());

        // TODO: implement cancel logic

        Api.instance(mContext).request(URL, authRequest, new Api.RequestListener() {
            @Override
            public void onSuccess(String response) {
                MemberAuthResponse memberAuthResponse = Api.instance(mContext).mGson.fromJson(response, MemberAuthResponse.class);

                if (memberAuthResponse.is_successful) {
                    // TODO: proceed to request
                    authRequestListener.onSuccess("Pin success");
                } else {
                    // TODO: Request failed
                    Toast.makeText(mContext, memberAuthResponse.getError()+".Please try again!", Toast.LENGTH_LONG).show();
                    ((Activity)mContext).recreate();
                    authRequestListener.onError("Authentication failed");
                }
            }

            @Override
            public void onTokenExpired() {

            }

            @Override
            public void onError(String error) {

            }
        });

    }

    public void request(final String url, final BaseRequest request, final RequestListener requestListener) {
        request(RequestMethod.POST, url, request, requestListener);
    }

    public void request(final RequestMethod method, final String url, final BaseRequest request, final RequestListener requestListener) {

        if (request instanceof RequiresAuthorization) {

            memberAuthDialog((RequiresAuthorization) request, new AuthRequestListener() {
                @Override
                public void onSuccess(String response) {

                    if (method == RequestMethod.GET) {
                    } else {

                        post(url, request, requestListener);
                    }
                }

                @Override
                public void onError(String error) {
                    requestListener.onError(error);
                }

                @Override
                public void onCancel() {
                    requestListener.onError("Auth canceled");
                }
            });
        } else {
            if (method == RequestMethod.GET) {

            } else {

                post(url, request, requestListener);
            }
        }
    }

    public void bioauthrequest(final String url, final BaseRequest request, final RequestListener requestListener) {
        bioauthrequest(RequestMethod.POST, url, request, requestListener);
    }

    public void bioauthrequest(final RequestMethod method, final String url, final BaseRequest request, final RequestListener requestListener) {

            if (method == RequestMethod.GET) {

            } else {
                post(url, request, requestListener);
            }
    }


    public void post(String url, final BaseRequest request, final RequestListener requestListener) {

        final String mRequestBody = mGson.toJson(request.getBody());

        final StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                requestListener.onSuccess(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                requestListener.onError(error.getMessage());
            }
        })

        {
            @Override
            public String getBodyContentType() {
                return "application/json";
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return mRequestBody == null ? null : mRequestBody.getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", mRequestBody, "utf-8");
                    return null;
                }
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");

                return params;
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                600000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        AppController.getInstance().addToRequestQueue(stringRequest);
    }

    public static Api instance(Context mContext) {
        return new Api(mContext);
    }

    public interface RequestListener {
        void onSuccess(String response);

        void onTokenExpired();

        void onError(String error);
    }

    public interface AuthRequestListener {
        void onSuccess(String response);

        void onError(String error);

        void onCancel();
    }

    public enum RequestMethod {
        POST,
        GET
    }
    final void AuthenticateTokenPin() {
        AlertDialog.Builder mBuilder = new AlertDialog.Builder(mContext);
        View mView = ((Activity) mContext).getLayoutInflater().inflate(R.layout.authmemberloginpin, null);
        final EditText mPin = mView.findViewById(R.id.member_auth__password);
        final TextInputLayout layoutPin = mView.findViewById(R.id.layout_member_auth__password);
        final AppCompatButton btnConfirmMemberAuth = mView.findViewById(R.id.btn_confirm_member_auth);
        mBuilder.setView(mView);
        final AlertDialog dialog = mBuilder
                .create();

        btnConfirmMemberAuth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //dialog.dismiss();
                final String pin = mPin.getText().toString().trim();

                if (pinn.equals(pin)) {
                    dialog.dismiss();
                    Toast.makeText(mContext, "genarate token", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(mContext, "token expired", Toast.LENGTH_SHORT).show();
                }
            }
        });
        dialog.setCancelable(false);
        dialog.show();
    }


}
