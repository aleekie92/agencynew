package com.coretec.agencyApplication.api.responses.newmoduleresponses;

import com.coretec.agencyApplication.model.newmodulemodels.BranchDetails;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class Branches implements Serializable {

    @SerializedName("BranchDetails")
    private List<BranchDetails> branchDetails;

    public List<BranchDetails> getBranchDetails() {
        return branchDetails;
    }

    public void setBranchDetails(List<BranchDetails> branchDetails) {
        this.branchDetails = branchDetails;
    }
}
