package com.coretec.agencyApplication.api.requests;

import com.coretec.agencyApplication.utils.Const;

import java.util.Date;

public class GlobalVRequest extends BaseRequest {

    public String fullnames;
    public String idno;
    public String phoneno;
    public String gender;
    public Date dob;
    public String passportphoto;
    public String maritalstatus;
    public String citizenship;
    public String email;
    public String employer;
    public String corporate_no;
    public String frontid;
    public String backid;
    public String signature;
    public String entryfee;
    public String finger1;
    public String finger2;
    public String finger3;
    public String finger4;
    public String transactiontype;
    public String agentid;
    public String terminalid;
    public double longitude;
    public double latitude;
    public String date;

    public GlobalVRequest(){
        super();
    }

    @Override
    public void setCredentials() {
        authorization_credentials.api_key = "12345";
        authorization_credentials.token = Const.getInstance().getLoginToken();
    }

}
