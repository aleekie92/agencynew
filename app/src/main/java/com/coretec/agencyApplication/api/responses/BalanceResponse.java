package com.coretec.agencyApplication.api.responses;

import java.io.Serializable;

/**
 * Created by ofula on 18/06/24.
 */

public class BalanceResponse extends BaseResponse implements Serializable {

    public String error;
    public String receiptno;
    public String customername;
    public String transactiontype;
    public String sacconame;
    public String transactiondate;
    private AccBalance balance;


    public AccBalance getBalance() {
        return balance;
    }

    public void setBalance(AccBalance balance) {
        this.balance = balance;
    }

    public String getReceiptno() {
        return receiptno;
    }

    public void setReceiptno(String receiptno) {
        this.receiptno = receiptno;
    }

    public String getCustomername() {
        return customername;
    }

    public void setCustomername(String customername) {
        this.customername = customername;
    }

    public String getTransactiontype() {
        return transactiontype;
    }

    public void setTransactiontype(String transactiontype) {
        this.transactiontype = transactiontype;
    }

    public String getSacconame() {
        return sacconame;
    }

    public void setSacconame(String sacconame) {
        this.sacconame = sacconame;
    }

    public String getTransactiondate() {
        return transactiondate;
    }

    public void setTransactiondate(String transactiondate) {
        this.transactiondate = transactiondate;
    }

    public String getError() {

        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

}
