package com.coretec.agencyApplication.api.responses;

/**
 * Created by ofula on 18/06/24.
 */

public class ChangePinResponse extends BaseResponse {
    private String error;
    private boolean pin_valid;

    public ChangePinResponse() {

    }

    public boolean isPin_valid() {
        return pin_valid;
    }

    public void setPin_valid(boolean pin_valid) {
        this.pin_valid = pin_valid;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }
}
