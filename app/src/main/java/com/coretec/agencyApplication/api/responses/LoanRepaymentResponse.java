package com.coretec.agencyApplication.api.responses;

import java.io.Serializable;

/**
 * Created by ofula on 18/06/24.
 */

public class LoanRepaymentResponse extends BaseResponse implements Serializable {

    private String error;
    private String sacconame;
    private String receiptno;
    private String transactiontype;
    private String transactiondate;

    public LoanRepaymentResponse() {

    }

    public LoanRepaymentResponse(String error, String sacconame, String receiptno, String transactiontype, String transactiondate) {
        this.error = error;
        this.sacconame = sacconame;
        this.receiptno = receiptno;
        this.transactiontype = transactiontype;
        this.transactiondate = transactiondate;
    }

    public String getTransactiondate() {
        return transactiondate;
    }

    public void setTransactiondate(String transactiondate) {
        this.transactiondate = transactiondate;
    }

    public String getReceiptno() {
        return receiptno;
    }

    public void setReceiptno(String receiptno) {
        this.receiptno = receiptno;
    }

    public String getTransactiontype() {
        return transactiontype;
    }

    public void setTransactiontype(String transactiontype) {
        this.transactiontype = transactiontype;
    }

    public String getSacconame() {
        return sacconame;
    }

    public void setSacconame(String sacconame) {
        this.sacconame = sacconame;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }
}
