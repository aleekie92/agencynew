package com.coretec.agencyApplication.api.requests;

import com.coretec.agencyApplication.model.GuarantorObject;
import com.coretec.agencyApplication.utils.Const;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class GuarantorRegistrationRequest extends BaseRequest implements Serializable {

    public String applidno;
    public String applgrowerno;
    //public String loancode;
    public String applreqkgs;
    @SerializedName("guarantorlist")
    public List<GuarantorObject> guarantorObjects;
    //public String guarantorList;
    public String transactiontype;
    public String agentid;
    public String terminalid;
    public double longitude;
    public double latitude;
    public String date;
    public String corporate_no;

    public GuarantorRegistrationRequest(){
        super();
    }

    @Override
    public void setCredentials() {
        authorization_credentials.api_key = "12345";
        authorization_credentials.token = Const.getInstance().getLoginToken();
    }

}
