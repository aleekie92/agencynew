package com.coretec.agencyApplication.api.requests;

import com.coretec.agencyApplication.utils.Const;

public class GetNetAmountRequest extends BaseRequest {

    public String corporateno;
    public String growersno;
    public String loanproductcode;
    public String amount;
    public String modeofdisbursement;
    public String transactiontype;
    public String agentid;
    public String terminalid;
    public double longitude;
    public double latitude;
    public String date;

    public GetNetAmountRequest() {
        super();
    }

    @Override
    public void setCredentials() {
        authorization_credentials.api_key = "12345";
        authorization_credentials.token = Const.getInstance().getLoginToken();
    }

}
