package com.coretec.agencyApplication.api.requests.newmodulesrequests;

import com.coretec.agencyApplication.api.requests.BaseRequest;
import com.coretec.agencyApplication.utils.Const;

public class RequestQualifiedAmount extends BaseRequest {

    public String  idnumber;
    public String  productcode;
    public String  corporateno;
    public String agentid;
    public String terminalid;
    public double longitude;
    public double latitude;
    public String date;

    public RequestQualifiedAmount(){
        super();
    }

    @Override
    public void setCredentials() {
        authorization_credentials.api_key = "12345";
        authorization_credentials.token = Const.getInstance().getLoginToken();
    }

}
