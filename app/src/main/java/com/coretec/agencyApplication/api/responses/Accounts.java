package com.coretec.agencyApplication.api.responses;

import java.io.Serializable;

public class Accounts implements Serializable {

    //private String referenceno;
    private String count;
    //private double amount;
    private String transactiontype;

    public Accounts() {
    }

    public Accounts( String count, String transactiontype) {
        //this.referenceno = referenceno;
        this.count = count;
        this.transactiontype = transactiontype;
    }

    public String getCount() {
        return count;
    }

    public void setCount(String count) {
        this.count = count;
    }

    public String getTransactiontype() {
        return transactiontype;
    }

    public void setTransactiontype(String transactiontype) {
        this.transactiontype = transactiontype;
    }
}
