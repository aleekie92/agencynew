package com.coretec.agencyApplication.api.responses;

import com.coretec.agencyApplication.model.LoanDetails;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class GetLoansInfo implements Serializable {

    @SerializedName("LoanDetails")
    private List<LoanDetails> loanDetails;

    public List<LoanDetails> getLoanDetails() {
        return loanDetails;
    }

    public void setLoanDetails(List<LoanDetails> loanDetails) {
        this.loanDetails = loanDetails;
    }
}
