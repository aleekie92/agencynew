package com.coretec.agencyApplication.api.responses;

import java.io.Serializable;
import java.util.List;

public class LastFiveReportsResponse extends BaseResponse implements Serializable {

    private String error;
    private String transactiondate;
    private List<LastFiveList> lastfivetransaction;

    public LastFiveReportsResponse() {

    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getTransactiondate() {
        return transactiondate;
    }

    public void setTransactiondate(String transactiondate) {
        this.transactiondate = transactiondate;
    }

    public List<LastFiveList> getLastfivetransaction() {
        return lastfivetransaction;
    }

    public void setLastfivetransaction(List<LastFiveList> lastfivetransaction) {
        this.lastfivetransaction = lastfivetransaction;
    }
}
