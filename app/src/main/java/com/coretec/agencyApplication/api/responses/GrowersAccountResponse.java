package com.coretec.agencyApplication.api.responses;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class GrowersAccountResponse extends BaseResponse implements Serializable {

    private String error;
    @SerializedName("transactiondate")
    private String transactiondate;
    @SerializedName("GetGrowersAccountList")
    private List<Growers> GrowersAccount;

    public GrowersAccountResponse() {

    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getTransactiondate() {
        return transactiondate;
    }

    public void setTransactiondate(String transactiondate) {
        this.transactiondate = transactiondate;
    }

    public List<Growers> getGrowersAccount() {
        return GrowersAccount;
    }

    public void setGrowersAccount(List<Growers> growersAccount) {
        GrowersAccount = growersAccount;
    }
}
