package com.coretec.agencyApplication.api.responses;

public class GetMatchlogsResponse extends BaseResponse {
    private String error;
    private boolean match;

    public boolean isMatch() {
        return match;
    }

    public void setMatch(boolean match) {
        this.match = match;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }
}
