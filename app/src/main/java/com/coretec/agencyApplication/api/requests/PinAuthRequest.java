package com.coretec.agencyApplication.api.requests;

import com.coretec.agencyApplication.api.Api;



public class PinAuthRequest extends MemberAuthRequest {
    public String pin;

    public PinAuthRequest() {

    }

    @Override
    public String getAuthenticationUrl() {
        return Api.AuthenticateCustomerLogin;
    }

    public PinAuthRequest(String pin) {
        this.pin = pin;
    }
}
