package com.coretec.agencyApplication.api.responses;

import java.io.Serializable;
import java.util.List;

public class GflAgentAccountResponse extends BaseResponse implements Serializable {


    public String error;
    public String agentaccountname;
    public String agentaccountno;
    public String factoryname;
    public String agentlacationcode;
    public String agentcommisionbal;
    public String agentaccountbal;

    public String userid;
    public String salescode;
    public String isSupervisor;
    public String isadmin;
    public String agentblocked;
    public String region;
    public String zone;
    public String vendorname;
    public String phonenumber;
    public String sacconame;
    public String saccomotto;
    public String corporateno;
    public String date;

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getAgentaccountname() {
        return agentaccountname;
    }

    public void setAgentaccountname(String agentaccountname) {
        this.agentaccountname = agentaccountname;
    }

    public String getAgentaccountno() {
        return agentaccountno;
    }

    public void setAgentaccountno(String agentaccountno) {
        this.agentaccountno = agentaccountno;
    }

    public String getFactoryname() {
        return factoryname;
    }

    public void setFactoryname(String factoryname) {
        this.factoryname = factoryname;
    }

    public String getAgentlacationcode() {
        return agentlacationcode;
    }

    public void setAgentlacationcode(String agentlacationcode) {
        this.agentlacationcode = agentlacationcode;
    }

    public String getAgentcommisionbal() {
        return agentcommisionbal;
    }

    public void setAgentcommisionbal(String agentcommisionbal) {
        this.agentcommisionbal = agentcommisionbal;
    }

    public String getAgentaccountbal() {
        return agentaccountbal;
    }

    public void setAgentaccountbal(String agentaccountbal) {
        this.agentaccountbal = agentaccountbal;
    }

    public String getUserid() {
        return userid;
    }

    public void setUserid(String userid) {
        this.userid = userid;
    }

    public String getSalescode() {
        return salescode;
    }

    public void setSalescode(String salescode) {
        this.salescode = salescode;
    }

    public String getIsSupervisor() {
        return isSupervisor;
    }

    public void setIsSupervisor(String isSupervisor) {
        this.isSupervisor = isSupervisor;
    }

    public String getIsadmin() {
        return isadmin;
    }

    public void setIsadmin(String isadmin) {
        this.isadmin = isadmin;
    }

    public String getAgentblocked() {
        return agentblocked;
    }

    public void setAgentblocked(String agentblocked) {
        this.agentblocked = agentblocked;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getZone() {
        return zone;
    }

    public void setZone(String zone) {
        this.zone = zone;
    }

    public String getVendorname() {
        return vendorname;
    }

    public void setVendorname(String vendorname) {
        this.vendorname = vendorname;
    }

    public String getPhonenumber() {
        return phonenumber;
    }

    public void setPhonenumber(String phonenumber) {
        this.phonenumber = phonenumber;
    }

    public String getSacconame() {
        return sacconame;
    }

    public void setSacconame(String sacconame) {
        this.sacconame = sacconame;
    }

    public String getSaccomotto() {
        return saccomotto;
    }

    public void setSaccomotto(String saccomotto) {
        this.saccomotto = saccomotto;
    }

    public String getCorporateno() {
        return corporateno;
    }

    public void setCorporateno(String corporateno) {
        this.corporateno = corporateno;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}