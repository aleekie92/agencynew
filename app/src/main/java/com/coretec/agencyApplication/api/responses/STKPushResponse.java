package com.coretec.agencyApplication.api.responses;

public class STKPushResponse extends BaseResponse {

    private String error;

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }
}
