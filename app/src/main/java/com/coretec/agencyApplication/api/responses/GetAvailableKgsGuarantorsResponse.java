package com.coretec.agencyApplication.api.responses;

public class GetAvailableKgsGuarantorsResponse extends BaseResponse {

    private String availablekgs;
    private String error;
    private String transactiondate;

    public String getAvailablekgs() {
        return availablekgs;
    }

    public void setAvailablekgs(String availablekgs) {
        this.availablekgs = availablekgs;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getTransactiondate() {
        return transactiondate;
    }

    public void setTransactiondate(String transactiondate) {
        this.transactiondate = transactiondate;
    }
}
