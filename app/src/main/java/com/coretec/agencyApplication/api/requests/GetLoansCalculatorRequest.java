package com.coretec.agencyApplication.api.requests;

import com.coretec.agencyApplication.utils.Const;

public class GetLoansCalculatorRequest extends BaseRequest {

    public String corporateno;
    public String growerno;
    public String idno;
    public String transactiontype;
    public String agentid;
    public String terminalid;
    public double longitude;
    public double latitude;
    public String date;

    public GetLoansCalculatorRequest(){
        super();
    }

    @Override
    public void setCredentials() {
        authorization_credentials.api_key = "12345";
        authorization_credentials.token = Const.getInstance().getLoginToken();
    }

}
