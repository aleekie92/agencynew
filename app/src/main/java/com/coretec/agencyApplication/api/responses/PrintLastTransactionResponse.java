package com.coretec.agencyApplication.api.responses;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by ofula on 18/06/24.
 */

public class PrintLastTransactionResponse extends BaseResponse implements Serializable {

    public String error;
    public String receiptno;
    public String transactiontype;
    public String sacconame;
    public String amount;
    public String transactiondate;
    @SerializedName("Description")
    public String Description;
    @SerializedName("AccountNo")
    public String AccountNo;
    @SerializedName("AccountName")
    public String AccountName;
    @SerializedName("DepositedBy")
    public String DepositedBy;

    public String getReceiptno() {
        return receiptno;
    }

    public void setReceiptno(String receiptno) {
        this.receiptno = receiptno;
    }


    public String getTransactiontype() {
        return transactiontype;
    }

    public void setTransactiontype(String transactiontype) {
        this.transactiontype = transactiontype;
    }

    public String getSacconame() {
        return sacconame;
    }

    public void setSacconame(String sacconame) {
        this.sacconame = sacconame;
    }

    public String getTransactiondate() {
        return transactiondate;
    }

    public void setTransactiondate(String transactiondate) {
        this.transactiondate = transactiondate;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getError() {

        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }

    public String getAccountNo() {
        return AccountNo;
    }

    public void setAccountNo(String accountNo) {
        AccountNo = accountNo;
    }

    public String getAccountName() {
        return AccountName;
    }

    public void setAccountName(String accountName) {
        AccountName = accountName;
    }

    public String getDepositedBy() {
        return DepositedBy;
    }

    public void setDepositedBy(String depositedBy) {
        DepositedBy = depositedBy;
    }
}
