package com.coretec.agencyApplication.api.responses;

import com.coretec.agencyApplication.model.LoanCalculator;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class GetLoansCalculatorList implements Serializable {

    @SerializedName("LoanCalculator")
    private List<LoanCalculator> loanCalculator;

    public List<LoanCalculator> getLoanCalculator() {
        return loanCalculator;
    }

    public void setLoanCalculator(List<LoanCalculator> loanCalculator) {
        this.loanCalculator = loanCalculator;
    }
}
