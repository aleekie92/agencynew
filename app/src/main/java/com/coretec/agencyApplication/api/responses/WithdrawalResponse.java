package com.coretec.agencyApplication.api.responses;

import java.io.Serializable;

/**
 * Created by ofula on 18/06/24.
 */

public class WithdrawalResponse extends BaseResponse implements Serializable {

    private String error;
    private String corporate_no;
    private String customername;
    private int amount;
    private String receiptno;
    private String transactiontype;
    private String sacconame;
    /*private String saccomotto;*/
    private String transactiondate;
    private String receiptType;

    public String getCustomername() {
        return customername;
    }

    public void setCustomername(String customername) {
        this.customername = customername;
    }

    public String getReceiptType() {
        return receiptType;
    }

    public void setReceiptType(String receiptType) {
        this.receiptType = receiptType;
    }

    public String getReceiptno() {
        return receiptno;
    }

    public void setReceiptno(String receiptno) {
        this.receiptno = receiptno;
    }

    public String getTransactiontype() {
        return transactiontype;
    }

    public void setTransactiontype(String transactiontype) {
        this.transactiontype = transactiontype;
    }

    public String getSacconame() {
        return sacconame;
    }

    public void setSacconame(String sacconame) {
        this.sacconame = sacconame;
    }

    /*public String getSaccomotto() {
        return saccomotto;
    }

    public void setSaccomotto(String saccomotto) {
        this.saccomotto = saccomotto;
    }*/

    public String getTransactiondate() {
        return transactiondate;
    }

    public void setTransactiondate(String transactiondate) {
        this.transactiondate = transactiondate;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getCorporate_no() {
        return corporate_no;
    }

    public void setCorporate_no(String corporate_no) {
        this.corporate_no = corporate_no;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public WithdrawalResponse() {

    }

}
