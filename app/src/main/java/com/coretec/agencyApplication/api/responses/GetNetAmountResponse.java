package com.coretec.agencyApplication.api.responses;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class GetNetAmountResponse extends BaseResponse implements Serializable {

    private String error;
    @SerializedName("transactiondate")
    private String transactiondate;
    @SerializedName("getnetamountresponselist")
    private List<getnetamountresponselist> getnetamountresponselistList;

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getTransactiondate() {
        return transactiondate;
    }

    public void setTransactiondate(String transactiondate) {
        this.transactiondate = transactiondate;
    }

    public List<getnetamountresponselist> getGetnetamountresponselistList() {
        return getnetamountresponselistList;
    }

    public void setGetnetamountresponselistList(List<getnetamountresponselist> getnetamountresponselistList) {
        this.getnetamountresponselistList = getnetamountresponselistList;
    }
}
