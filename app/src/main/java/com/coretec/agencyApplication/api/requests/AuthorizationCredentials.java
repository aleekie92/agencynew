package com.coretec.agencyApplication.api.requests;

/**
 * Created by ofula on 18/06/24.
 */

public class AuthorizationCredentials {
    public String api_key;
    public String token;
    public String app_secret;
}
