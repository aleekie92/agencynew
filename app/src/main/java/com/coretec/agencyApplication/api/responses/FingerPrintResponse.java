package com.coretec.agencyApplication.api.responses;

public class FingerPrintResponse extends BaseResponse {

    private String leftring;
    private String leftlittle;
    private String rightring;
    private String rightlittle;
    private String accountidentifier;
    private String error;
    private String operationsuccessful;
    private String receiptno;
    private String transactiondate;

    public String getLeftring() {
        return leftring;
    }

    public void setLeftring(String leftring) {
        this.leftring = leftring;
    }

    public String getLeftlittle() {
        return leftlittle;
    }

    public void setLeftlittle(String leftlittle) {
        this.leftlittle = leftlittle;
    }

    public String getRightring() {
        return rightring;
    }

    public void setRightring(String rightring) {
        this.rightring = rightring;
    }

    public String getRightlittle() {
        return rightlittle;
    }

    public void setRightlittle(String rightlittle) {
        this.rightlittle = rightlittle;
    }

    public String getAccountidentifier() {
        return accountidentifier;
    }

    public void setAccountidentifier(String accountidentifier) {
        this.accountidentifier = accountidentifier;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getOperationsuccessful() {
        return operationsuccessful;
    }

    public void setOperationsuccessful(String operationsuccessful) {
        this.operationsuccessful = operationsuccessful;
    }

    public String getReceiptno() {
        return receiptno;
    }

    public void setReceiptno(String receiptno) {
        this.receiptno = receiptno;
    }

    public String getTransactiondate() {
        return transactiondate;
    }

    public void setTransactiondate(String transactiondate) {
        this.transactiondate = transactiondate;
    }
}
