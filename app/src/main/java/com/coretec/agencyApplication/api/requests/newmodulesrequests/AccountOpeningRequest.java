package com.coretec.agencyApplication.api.requests.newmodulesrequests;

import com.coretec.agencyApplication.api.requests.BaseRequest;
import com.coretec.agencyApplication.utils.Const;

/**
 * Created by ofula on 18/06/24.
 */

public class AccountOpeningRequest extends BaseRequest {



    public String amount;
    public String productcode;
    public String groupcode;
    public String idnumber;
    public String juniorname;
    public String juniordob;
    public String corporateno;
    public String agentid;
    public String terminalid;
    public double longitude;
    public double latitude;
    public String date;

    public AccountOpeningRequest() {
        super();
    }

    @Override
    public void setCredentials() {
        authorization_credentials.api_key = "12345";
        authorization_credentials.token = Const.getInstance().getLoginToken();
    }
}
