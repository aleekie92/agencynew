package com.coretec.agencyApplication.api.responses;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class GetLoansCalculatorResponse extends BaseResponse implements Serializable {

    private String error;
    @SerializedName("transactiondate")
    private String transactiondate;
    @SerializedName("GetLoansCalculatorList")
    private List<GetLoansCalculatorList> loansCalculator;

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getTransactiondate() {
        return transactiondate;
    }

    public void setTransactiondate(String transactiondate) {
        this.transactiondate = transactiondate;
    }

    public List<GetLoansCalculatorList> getLoansCalculator() {
        return loansCalculator;
    }

    public void setLoansCalculator(List<GetLoansCalculatorList> loansCalculator) {
        this.loansCalculator = loansCalculator;
    }
}
