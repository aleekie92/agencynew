package com.coretec.agencyApplication.api.responses;

import com.coretec.agencyApplication.model.GrowerInfo;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class Growers1 implements Serializable {

    @SerializedName("GrowerDetails")
    private List<GrowerInfo> growerInfo;

    public List<GrowerInfo> getGrowerInfo() {
        return growerInfo;
    }

    public void setGrowerInfo(List<GrowerInfo> growerInfo) {
        this.growerInfo = growerInfo;
    }
}
