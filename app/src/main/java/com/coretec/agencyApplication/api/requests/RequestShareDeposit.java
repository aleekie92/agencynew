package com.coretec.agencyApplication.api.requests;

import com.coretec.agencyApplication.utils.Const;

/**
 * Created by ofula on 18/06/24.
 */

public class RequestShareDeposit extends BaseRequest {

    public String agentid;
    public String corporate_no;
    public String memberaccno;
    public int amount;
    public String terminalid;
    public double longitude;
    public double latitude;
    public String date;
    public String depositorsphoneno;
    public  String depositorsname;

    public RequestShareDeposit() {
        super();
    }

    @Override
    public void setCredentials() {
        authorization_credentials.api_key = "12345";
        authorization_credentials.token = Const.getInstance().getLoginToken();
    }
}
