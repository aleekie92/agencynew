package com.coretec.agencyApplication.api;
import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.AppCompatButton;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.StringRequest;
import com.coretec.agencyApplication.R;
import com.coretec.agencyApplication.api.requests.BaseRequest;
import com.coretec.agencyApplication.api.requests.MemberAuthRequest;
import com.coretec.agencyApplication.api.requests.PinAuthRequest;
import com.coretec.agencyApplication.api.responses.MemberAuthResponse;
import com.coretec.agencyApplication.utils.AppController;
import com.coretec.agencyApplication.utils.PreferenceFileKeys;
import com.coretec.agencyApplication.utils.RequiresAuthorization;
import com.coretec.agencyApplication.utils.Utils;
import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.Map;
public class GFL {
    //public static String MSACCO_AGENT = "http://172.16.11.145:3052/api/AgencyBanking/"; //live
    public static String MSACCO_AGENT = "http://172.16.11.145:3051/api/AgencyBanking/"; //test
    public static String LoanRepayment = "LoanRepayment";
    public static String GetMembername = "GetMembername";
    public static String MemberActivation = "MemberActivation";

    //GFL Apis
    public static String GetAgentAccountInfo = "GetAgentAccountInfo";
    public static String GetGrowersDetails = "GetGrowersDetails";
    public static String GetGrowersAccount = "GetGrowersAccount";
    public static String AgentFundsWithdrawal = "AgentFundsWithdrawal";
    public static String MpesaChange = "MpesaChange";
    public static String GetGrowersPaybillTransactions = "GetGrowersPaybillTransactions";
    public static String GetLoanProducts = "GetLoanProducts";
    public static String GetLoansInfo = "GetLoansInfo";
    public static String MpesaPaybill = "MpesaPaybill";
    public static String PuBlock = "PuBlock";
    public static String PuActivation = "PuActivation";
    public static String PuRegistration = "PuRegistration";
    public static String GetLoansCalculator = "GetLoansCalculator";
    public static String GetGrowerMemberBalance = "GetGrowerMemberBalance";
    public static String GetMiniStatement = "GetMiniStatement";
    public static String PinReset = "PinReset";
    public static String GetGrowersQualifiedLoans = "GetGrowersQualifiedLoans";
    public static String LoanExist = "LoanExist ";
    public static String GetNetAmount = "GetNetAmount";
    public static String GetQualifiedKgs = "GetQualifiedKgs";
    public static String LoanApplication = "LoanApplication";
    public static String CreateVirtualRegistration = "CreateVirtualRegistration";
    public static String UpdateMemberDetails = "UpdateMemberDetails";
    public static String GetBanksAll = "GetBanksAll";
    public static String GetSaccoAccounts = "GetSaccoAccounts";
    public static String GetAvailableKgsGuarantors = "GetAvailableKgsGuarantors";
    public static String GuarantorRegistration = "GuarantorRegistration";
    public static String GetFourFingerPrint = "GetFourFingerPrint";
    public static String GetGuarantors = "GetGuarantors";
    public static String GetLoanStatus = "GetLoanStatus";
    public static String GlobalVR = "GlobalVR";
    public static String STKPush = "STKPush";
    public static String GetAgentname = "GetAgentname";
    public static String AgentHasBio = "AgentHasBio";

    public static String RegisterAgentFingerPrints = "RegisterAgentFingerPrints";
    public static String AgentBioAuthentication = "AgentBioAuthentication";
    public static String GetAgentFourFingerPrint = "GetAgentFourFingerPrint";
    public static String userlogs = "userlogs";
    private Context mContext;

    public Gson mGson = new GsonBuilder()
            .setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES)
            .setLenient()
            .serializeNulls()
            .create();
    private boolean isFingerprintOpened = false;

    SharedPreferences sharedPreferences;
    private GFL() {

    }

    private GFL(Context mContext) {
        this.mContext = mContext;
    }

    final void memberAuthDialog(final RequiresAuthorization requiresAuthorizationRequest, final AuthRequestListener authRequestListener) {

        AlertDialog.Builder mBuilder = new AlertDialog.Builder(mContext);
        View mView = ((Activity) mContext).getLayoutInflater().inflate(R.layout.gfl_auth, null);
        final EditText mPin = mView.findViewById(R.id.member_auth__password);
        final TextInputLayout layoutPin = mView.findViewById(R.id.layout_member_auth__password);
        final TextView message = mView.findViewById(R.id.EnrollmentTextView);


        mBuilder.setView(mView);
        final AppCompatButton btnConfirmMemberAuth = mView.findViewById(R.id.btn_confirm_member_auth);
        final AppCompatButton btnFingerPrintMemberAuth = mView.findViewById(R.id.btn_fingerprint_member_auth);
        final LinearLayout mFingerPrintAuth = mView.findViewById(R.id.fingerprint_auth);
        final LinearLayout mPinAuth = mView.findViewById(R.id.pin_auth);
        final TextView error_msg = mView.findViewById(R.id.errorMessage);

        final AlertDialog dialog = mBuilder.create();
        dialog.show();


        btnConfirmMemberAuth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                final String pin = mPin.getText().toString().trim();
                final String getMsg = message.getText().toString();

                if (getMsg.isEmpty()) {
                    layoutPin.setError("Please enter your fingerprint");

                } else if (pin.trim().length() > 4 || pin.trim().length() < 4) {
                    layoutPin.setError("Enter the 4 digit pin");
                } else {
                    authMemberRequest(requiresAuthorizationRequest, authRequestListener, new PinAuthRequest(pin));
                }
            }
        });

        btnFingerPrintMemberAuth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

    }

    private void sendMsg(String msg, Handler handler) {
        handler.obtainMessage(0, msg).sendToTarget();
    }

    public void authMemberRequest(RequiresAuthorization requiresAuthorizationRequest,
                                  final AuthRequestListener authRequestListener, @NonNull MemberAuthRequest authRequest) {

        String TAG = "Auth Member request";
        sharedPreferences = mContext.getSharedPreferences(PreferenceFileKeys.PREFS_FILE, Context.MODE_PRIVATE);

        String URL = GFL.MSACCO_AGENT + authRequest.getAuthenticationUrl();

        authRequest.accountidentifier = requiresAuthorizationRequest.getPhoneNumber();
        authRequest.accountidentifiercode = "0";
        authRequest.agentid = sharedPreferences.getString(PreferenceFileKeys.AGENT_ID, "");
        authRequest.terminalid = Utils.getIMEI(mContext);
        authRequest.longitude = Utils.getLong(mContext);
        authRequest.latitude = Utils.getLat(mContext);
        authRequest.date = Utils.getFormattedDate();
        Log.e(TAG, authRequest.getBody().toString());

        // TODO: implement cancel logic

        GFL.instance(mContext).request(URL, authRequest, new GFL.RequestListener() {
            @Override
            public void onSuccess(String response) {
                MemberAuthResponse memberAuthResponse = GFL.instance(mContext).mGson.fromJson(response, MemberAuthResponse.class);
                if (memberAuthResponse.is_successful) {
                    // TODO: proceed to request
                    authRequestListener.onSuccess("Pin success");
                } else {
                    // TODO: Request failed
                    Toast.makeText(mContext, memberAuthResponse.getError(), Toast.LENGTH_SHORT).show();
                    authRequestListener.onError("Authentication failed");
                }
            }

            @Override
            public void onTokenExpired() {

            }

            @Override
            public void onError(String error) {

            }
        });

    }

    public void request(final String url, final BaseRequest request, final RequestListener requestListener) {
        request(RequestMethod.POST, url, request, requestListener);
    }

    public void request(final RequestMethod method, final String url, final BaseRequest request, final RequestListener requestListener) {
        if (request instanceof RequiresAuthorization) {
            memberAuthDialog((RequiresAuthorization) request, new AuthRequestListener() {
                @Override
                public void onSuccess(String response) {
                    if (method == RequestMethod.GET) {
                    } else {
                        post(url, request, requestListener);
                    }
                }

                @Override
                public void onError(String error) {
                    requestListener.onError(error);
                }

                @Override
                public void onCancel() {
                    requestListener.onError("Auth canceled");
                }
            });
        } else {
            if (method == RequestMethod.GET) {

            } else {
                post(url, request, requestListener);
            }
        }
    }

    public void post(String url, final BaseRequest request, final RequestListener requestListener) {
        final String mRequestBody = mGson.toJson(request.getBody());
        Log.e("Request", mRequestBody);
        final StringRequest stringRequest = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.i("LOG_VOLLEY", response);
                requestListener.onSuccess(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("LOG_VOLLEY", error.toString());
                requestListener.onError(error.getMessage());
            }
        })

        {
            @Override
            public String getBodyContentType() {
                return "application/json";
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return mRequestBody == null ? null : mRequestBody.getBytes("utf-8");
                } catch (UnsupportedEncodingException uee) {
                    VolleyLog.wtf("Unsupported Encoding while trying to get the bytes of %s using %s", mRequestBody, "utf-8");
                    return null;
                }
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {

                Map<String, String> params = new HashMap<String, String>();
                params.put("Content-Type", "application/json");

                return params;
            }
        };

        stringRequest.setRetryPolicy(new DefaultRetryPolicy(
                600000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

        AppController.getInstance().addToRequestQueue(stringRequest);
    }

    public static GFL instance(Context mContext) {
        return new GFL(mContext);
    }

    public interface RequestListener {
        void onSuccess(String response);

        void onTokenExpired();

        void onError(String error);
    }

    public interface AuthRequestListener {
        void onSuccess(String response);

        void onError(String error);

        void onCancel();
    }

    public enum RequestMethod {
        POST,
        GET
    }

}
