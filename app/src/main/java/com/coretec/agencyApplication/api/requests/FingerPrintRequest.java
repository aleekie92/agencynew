package com.coretec.agencyApplication.api.requests;

import com.coretec.agencyApplication.utils.Const;

/**
 * Created by ofula on 19/03/12.
 */

public class FingerPrintRequest extends BaseRequest {

    //public String corporateno;
    public String accountidentifier;
    public String accountidentifiercode;
    public String agentid;
    public String terminalid;
    public double longitude;
    public double latitude;
    public String date;

    public FingerPrintRequest(){
        super();
    }

    @Override
    public void setCredentials() {
        authorization_credentials.api_key = "12345";
        authorization_credentials.token = Const.getInstance().getLoginToken();
    }

}
