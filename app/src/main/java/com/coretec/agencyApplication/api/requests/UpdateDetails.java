package com.coretec.agencyApplication.api.requests;

import com.coretec.agencyApplication.utils.Const;

import java.util.Date;

public class UpdateDetails extends BaseRequest {

    public String idnumber;
    public String nokname;
    public String nextofkinid;
    public String relationship;
    public Boolean puregister;
    //public String puregister;
    public String bankandbranchcode;
    public String bankaccountno;
    public String bankcardphoto;
    public String agentid;
    public String transactiontype;
    public String terminalid;
    public double longitude;
    public double latitude;
    public String date;
    public String corporate_no;
    public String saccocode;
    public String saccoaccno;
    public String saccocardphoto;
    public String growerlist;

    public UpdateDetails(){
        super();
    }

    @Override
    public void setCredentials() {
        authorization_credentials.api_key = "12345";
        authorization_credentials.token = Const.getInstance().getLoginToken();
    }

}
