package com.coretec.agencyApplication.api.responses;


public class DepositResponse extends BaseResponse {

    private String error;
    private String corporate_no;
    private String customername;
    private String depositedby;
    private String memberaccno;
    private int amount;
    private String receiptno;
    private String transactiontype;
    private String sacconame;
    private String transactiondate;
    private String Time;

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getCorporate_no() {
        return corporate_no;
    }

    public void setCorporate_no(String corporate_no) {
        this.corporate_no = corporate_no;
    }

    public String getMemberaccno() {
        return memberaccno;
    }

    public void setMemberaccno(String memberaccno) {
        this.memberaccno = memberaccno;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public String getTime() {
        return Time;
    }

    public void setTime(String time) {
        Time = time;
    }

    public String getCustomername() {
        return customername;
    }

    public void setCustomername(String customername) {
        this.customername = customername;
    }

    public String getDepositedby() {
        return depositedby;
    }

    public void setDepositedby(String depositedby) {
        this.depositedby = depositedby;
    }

    public String getReceiptno() {
        return receiptno;
    }

    public void setReceiptno(String receiptno) {
        this.receiptno = receiptno;
    }

    public String getTransactiontype() {
        return transactiontype;
    }

    public void setTransactiontype(String transactiontype) {
        this.transactiontype = transactiontype;
    }

    public String getSacconame() {
        return sacconame;
    }

    public void setSacconame(String sacconame) {
        this.sacconame = sacconame;
    }

    public String getTransactiondate() {
        return transactiondate;
    }

    public void setTransactiondate(String transactiondate) {
        this.transactiondate = transactiondate;
    }

    public DepositResponse() {

    }

}
