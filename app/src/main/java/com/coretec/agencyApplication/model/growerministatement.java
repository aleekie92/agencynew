package com.coretec.agencyApplication.model;

import java.io.Serializable;

public class growerministatement implements Serializable {

    private String PostingDate;
    private String Description;
    private double Amount;

    public growerministatement(String PostingDate, String Description, double Amount) {
        this.PostingDate = PostingDate;
        this.Description = Description;
        this.Amount = Amount;
    }

    public growerministatement() {

    }

    public String getPostingDate() {
        return PostingDate;
    }

    public void setPostingDate(String postingDate) {
        PostingDate = postingDate;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }

    public double getAmount() {
        return Amount;
    }

    public void setAmount(double amount) {
        Amount = amount;
    }
}
