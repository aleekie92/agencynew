package com.coretec.agencyApplication.model;

/**
 * Created by ofula on 18/06/24.
 */

public class Dashboard {
    private String name;
    private int numberItems;
    private int image;

    public Dashboard() {

    }

    public Dashboard(String name, int numberItems, int image) {

        this.name = name;
        this.numberItems = numberItems;
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getNumberItems() {
        return numberItems;
    }

    public void setNumberItems(int numberItems) {
        this.numberItems = numberItems;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }
}
