package com.coretec.agencyApplication.model;

import java.io.Serializable;

public class QualifiedLoans implements Serializable {

    private String ProductType;
    private String LoansPayKey;
    private String LoanProductName;
    private String Amount;
    private String MinimumBalance;
    private String LoanValidation;
    private String LoanTopUpValidation;

    public String getProductType() {
        return ProductType;
    }

    public void setProductType(String productType) {
        ProductType = productType;
    }

    public String getLoansPayKey() {
        return LoansPayKey;
    }

    public void setLoansPayKey(String loansPayKey) {
        LoansPayKey = loansPayKey;
    }

    public String getLoanProductName() {
        return LoanProductName;
    }

    public void setLoanProductName(String loanProductName) {
        LoanProductName = loanProductName;
    }

    public String getAmount() {
        return Amount;
    }

    public void setAmount(String amount) {
        Amount = amount;
    }

    public String getMinimumBalance() {
        return MinimumBalance;
    }

    public void setMinimumBalance(String minimumBalance) {
        MinimumBalance = minimumBalance;
    }

    public String getLoanValidation() {
        return LoanValidation;
    }

    public void setLoanValidation(String loanValidation) {
        LoanValidation = loanValidation;
    }

    public String getLoanTopUpValidation() {
        return LoanTopUpValidation;
    }

    public void setLoanTopUpValidation(String loanTopUpValidation) {
        LoanTopUpValidation = loanTopUpValidation;
    }
}
