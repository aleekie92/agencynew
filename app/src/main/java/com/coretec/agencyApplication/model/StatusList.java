package com.coretec.agencyApplication.model;

import java.io.Serializable;

public class StatusList implements Serializable {
    private String Status;
    private String AccountNo;

    public String getStatus() {
        return Status;
    }

    public void setStatus(String status) {
        Status = status;
    }

    public String getAccountNo() {
        return AccountNo;
    }

    public void setAccountNo(String accountNo) {
        AccountNo = accountNo;
    }
}
