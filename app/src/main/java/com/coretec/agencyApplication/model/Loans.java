package com.coretec.agencyApplication.model;
import java.io.Serializable;
public class Loans implements Serializable {

    private String loan_no;
    private String loan_type;
    private String loantypename;
    private double loanbalance;
    private double intrestbalance;

    public Loans() {
    }

    public Loans(String loan_no, String loan_type, String loantypename, double loanbalance, double intrestbalance) {
        this.loan_no = loan_no;
        this.loan_type = loan_type;
        this.loantypename = loantypename;
        this.loanbalance = loanbalance;
        this.intrestbalance = intrestbalance;
    }

    public String getLoan_no() {
        return loan_no;
    }

    public void setLoan_no(String loan_no) {
        this.loan_no = loan_no;
    }

    public String getLoan_type() {
        return loan_type;
    }

    public void setLoan_type(String loan_type) {
        this.loan_type = loan_type;
    }

    public String getLoantypename() {
        return loantypename;
    }

    public void setLoantypename(String loantypename) {
        this.loantypename = loantypename;
    }

    public double getLoanbalance() {
        return loanbalance;
    }

    public void setLoanbalance(double loanbalance) {
        this.loanbalance = loanbalance;
    }

    public double getIntrestbalance() {
        return intrestbalance;
    }

    public void setIntrestbalance(double intrestbalance) {
        this.intrestbalance = intrestbalance;
    }
}