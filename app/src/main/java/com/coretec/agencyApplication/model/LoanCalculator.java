package com.coretec.agencyApplication.model;

import java.io.Serializable;

public class LoanCalculator implements Serializable {

    private String GrowerNo;
    private String GrowerFullNames;
    private String GrowerIDNos;
    private String GrowerStatus;
    private String GrowerPURegistered;
    private String ProductType;
    private String LoansPayKey;
    private String LoanProductName;
    private String PreviousYearKgs;
    private String CurrentKgs;
    private String TotCurrent;
    private String RatePerKG;
    private String FactBonusRate;
    private String BonusRate;
    private String PercantageOfPrevious;
    private String ProrationRate;
    private String ProrationRateLimit;
    private String QualAmount;

    public String getGrowerNo() {
        return GrowerNo;
    }

    public void setGrowerNo(String growerNo) {
        GrowerNo = growerNo;
    }

    public String getGrowerFullNames() {
        return GrowerFullNames;
    }

    public void setGrowerFullNames(String growerFullNames) {
        GrowerFullNames = growerFullNames;
    }

    public String getGrowerIDNos() {
        return GrowerIDNos;
    }

    public void setGrowerIDNos(String growerIDNos) {
        GrowerIDNos = growerIDNos;
    }

    public String getGrowerStatus() {
        return GrowerStatus;
    }

    public void setGrowerStatus(String growerStatus) {
        GrowerStatus = growerStatus;
    }

    public String getGrowerPURegistered() {
        return GrowerPURegistered;
    }

    public void setGrowerPURegistered(String growerPURegistered) {
        GrowerPURegistered = growerPURegistered;
    }

    public String getProductType() {
        return ProductType;
    }

    public void setProductType(String productType) {
        ProductType = productType;
    }

    public String getLoansPayKey() {
        return LoansPayKey;
    }

    public void setLoansPayKey(String loansPayKey) {
        LoansPayKey = loansPayKey;
    }

    public String getLoanProductName() {
        return LoanProductName;
    }

    public void setLoanProductName(String loanProductName) {
        LoanProductName = loanProductName;
    }

    public String getPreviousYearKgs() {
        return PreviousYearKgs;
    }

    public void setPreviousYearKgs(String previousYearKgs) {
        PreviousYearKgs = previousYearKgs;
    }

    public String getCurrentKgs() {
        return CurrentKgs;
    }

    public void setCurrentKgs(String currentKgs) {
        CurrentKgs = currentKgs;
    }

    public String getTotCurrent() {
        return TotCurrent;
    }

    public void setTotCurrent(String totCurrent) {
        TotCurrent = totCurrent;
    }

    public String getRatePerKG() {
        return RatePerKG;
    }

    public void setRatePerKG(String ratePerKG) {
        RatePerKG = ratePerKG;
    }

    public String getFactBonusRate() {
        return FactBonusRate;
    }

    public void setFactBonusRate(String factBonusRate) {
        FactBonusRate = factBonusRate;
    }

    public String getBonusRate() {
        return BonusRate;
    }

    public void setBonusRate(String bonusRate) {
        BonusRate = bonusRate;
    }

    public String getPercantageOfPrevious() {
        return PercantageOfPrevious;
    }

    public void setPercantageOfPrevious(String percantageOfPrevious) {
        PercantageOfPrevious = percantageOfPrevious;
    }

    public String getProrationRate() {
        return ProrationRate;
    }

    public void setProrationRate(String prorationRate) {
        ProrationRate = prorationRate;
    }

    public String getProrationRateLimit() {
        return ProrationRateLimit;
    }

    public void setProrationRateLimit(String prorationRateLimit) {
        ProrationRateLimit = prorationRateLimit;
    }

    public String getQualAmount() {
        return QualAmount;
    }

    public void setQualAmount(String qualAmount) {
        QualAmount = qualAmount;
    }
}
