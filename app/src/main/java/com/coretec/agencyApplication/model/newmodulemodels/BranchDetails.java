package com.coretec.agencyApplication.model.newmodulemodels;

import java.io.Serializable;

public class BranchDetails implements Serializable {
    private String branchname;
    private String branchcode;

    public String getBranchname() {
        return branchname;
    }

    public void setBranchname(String branchname) {
        this.branchname = branchname;
    }

    public String getBranchcode() {
        return branchcode;
    }

    public void setBranchcode(String branchcode) {
        this.branchcode = branchcode;
    }
}
