package com.coretec.agencyApplication.model;

import java.io.Serializable;

public class Employercode implements Serializable {

    private String customername;
    private String number;

    public String getCustomername() {
        return customername;
    }

    public void setCustomername(String customername) {
        this.customername = customername;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }
}
