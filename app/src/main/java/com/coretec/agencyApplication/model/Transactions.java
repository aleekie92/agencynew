package com.coretec.agencyApplication.model;

import java.io.Serializable;

/**
 * Created by ofula on 18/06/24.
 */

public class Transactions implements Serializable {

    private String posting_date;
    private String description;
    private double amount;

    public Transactions(String posting_date, String description, double amount) {
        this.posting_date = posting_date;
        this.description = description;
        this.amount = amount;
    }

    public Transactions() {

    }

    public String getPosting_date() {
        return posting_date;
    }

    public void setPosting_date(String posting_date) {
        this.posting_date = posting_date;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }
}
