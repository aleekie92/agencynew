package com.coretec.agencyApplication.model;

import java.io.Serializable;

public class agentdetails implements Serializable {

    private String AgentAccountNo;
    private String FactoryName;
    private String AgentAccountName;
    private String AgentLocationCode;
    private String UserId;
    private String SalesCode;
    private String AgentCommisionBal;
    private String AgentAccountBal;

    public agentdetails() {

    }

    public String getAgentAccountNo() {
        return AgentAccountNo;
    }

    public void setAgentAccountNo(String agentAccountNo) {
        AgentAccountNo = agentAccountNo;
    }

    public String getFactoryName() {
        return FactoryName;
    }

    public void setFactoryName(String factoryName) {
        FactoryName = factoryName;
    }

    public String getAgentAccountName() {
        return AgentAccountName;
    }

    public void setAgentAccountName(String agentAccountName) {
        AgentAccountName = agentAccountName;
    }

    public String getAgentLocationCode() {
        return AgentLocationCode;
    }

    public void setAgentLocationCode(String agentLocationCode) {
        AgentLocationCode = agentLocationCode;
    }

    public String getUserId() {
        return UserId;
    }

    public void setUserId(String userId) {
        UserId = userId;
    }

    public String getSalesCode() {
        return SalesCode;
    }

    public void setSalesCode(String salesCode) {
        SalesCode = salesCode;
    }

    public String getAgentCommisionBal() {
        return AgentCommisionBal;
    }

    public void setAgentCommisionBal(String agentCommisionBal) {
        AgentCommisionBal = agentCommisionBal;
    }

    public String getAgentAccountBal() {
        return AgentAccountBal;
    }

    public void setAgentAccountBal(String agentAccountBal) {
        AgentAccountBal = agentAccountBal;
    }
}
