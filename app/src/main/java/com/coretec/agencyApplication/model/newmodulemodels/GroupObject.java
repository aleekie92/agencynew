package com.coretec.agencyApplication.model.newmodulemodels;

public class GroupObject {

    private String memberno;
    private String accountname;
    private String expectedamount;
    private double totalexpected;
    private double count;

    public GroupObject(String memberno, String accountname, String expectedamount, double totalexpected, double count) {
        this.memberno = memberno;
        this.accountname = accountname;
        this.expectedamount = expectedamount;
        this.totalexpected = totalexpected;
        this.count = count;
    }

    public String getMemberno() {
        return memberno;
    }

    public void setMemberno(String memberno) {
        this.memberno = memberno;
    }

    public String getAccountname() {
        return accountname;
    }

    public void setAccountname(String accountname) {
        this.accountname = accountname;
    }

    public String getExpectedamount() {
        return expectedamount;
    }

    public void setExpectedamount(String expectedamount) {
        this.expectedamount = expectedamount;
    }

    public double getTotalexpected() {
        return totalexpected;
    }

    public void setTotalexpected(double totalexpected) {
        this.totalexpected = totalexpected;
    }

    public double getCount() {
        return count;
    }

    public void setCount(double count) {
        this.count = count;
    }
}
