package com.coretec.agencyApplication.model;

import java.io.Serializable;

public class Guarantor implements Serializable {

    private String guargrowerno;
    private String guaridno;
    private String guaravailkgs;

    public String getGuargrowerno() {
        return guargrowerno;
    }

    public void setGuargrowerno(String guargrowerno) {
        this.guargrowerno = guargrowerno;
    }

    public String getGuaridno() {
        return guaridno;
    }

    public void setGuaridno(String guaridno) {
        this.guaridno = guaridno;
    }

    public String getGuaravailkgs() {
        return guaravailkgs;
    }

    public void setGuaravailkgs(String guaravailkgs) {
        this.guaravailkgs = guaravailkgs;
    }
}
