package com.coretec.agencyApplication.model;

import java.io.Serializable;

public class LoanDetails implements Serializable {

    private String LoanNo;
    private String ProductType;
    private String LoansPayKey;
    private String LoanProductName;
    private String OutstandingBalance;
    private String Interest;
    private String LoansStatus;
    private String IssuedDate;
    private String ExpCompDate;
    private String DefaultedAmount;
    private String IsDefaulter;

    public String getLoanNo() {
        return LoanNo;
    }

    public void setLoanNo(String loanNo) {
        LoanNo = loanNo;
    }

    public String getProductType() {
        return ProductType;
    }

    public void setProductType(String productType) {
        ProductType = productType;
    }

    public String getLoansPayKey() {
        return LoansPayKey;
    }

    public void setLoansPayKey(String loansPayKey) {
        LoansPayKey = loansPayKey;
    }

    public String getLoanProductName() {
        return LoanProductName;
    }

    public void setLoanProductName(String loanProductName) {
        LoanProductName = loanProductName;
    }

    public String getOutstandingBalance() {
        return OutstandingBalance;
    }

    public void setOutstandingBalance(String outstandingBalance) {
        OutstandingBalance = outstandingBalance;
    }

    public String getInterest() {
        return Interest;
    }

    public void setInterest(String interest) {
        Interest = interest;
    }

    public String getLoansStatus() {
        return LoansStatus;
    }

    public void setLoansStatus(String loansStatus) {
        LoansStatus = loansStatus;
    }

    public String getIssuedDate() {
        return IssuedDate;
    }

    public void setIssuedDate(String issuedDate) {
        IssuedDate = issuedDate;
    }

    public String getExpCompDate() {
        return ExpCompDate;
    }

    public void setExpCompDate(String expCompDate) {
        ExpCompDate = expCompDate;
    }

    public String getDefaultedAmount() {
        return DefaultedAmount;
    }

    public void setDefaultedAmount(String defaultedAmount) {
        DefaultedAmount = defaultedAmount;
    }

    public String getIsDefaulter() {
        return IsDefaulter;
    }

    public void setIsDefaulter(String isDefaulter) {
        IsDefaulter = isDefaulter;
    }
}
