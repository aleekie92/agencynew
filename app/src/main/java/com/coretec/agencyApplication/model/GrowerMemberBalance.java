package com.coretec.agencyApplication.model;

import java.io.Serializable;

public class GrowerMemberBalance implements Serializable {

    private String ClientCode;
    private String LoanNo;
    private String LoanType;
    private String LoanTypeName;
    private String InterestBalance;
    private String LoanBalance;

    public String getClientCode() {
        return ClientCode;
    }

    public void setClientCode(String clientCode) {
        ClientCode = clientCode;
    }

    public String getLoanNo() {
        return LoanNo;
    }

    public void setLoanNo(String loanNo) {
        LoanNo = loanNo;
    }

    public String getLoanType() {
        return LoanType;
    }

    public void setLoanType(String loanType) {
        LoanType = loanType;
    }

    public String getLoanTypeName() {
        return LoanTypeName;
    }

    public void setLoanTypeName(String loanTypeName) {
        LoanTypeName = loanTypeName;
    }

    public String getInterestBalance() {
        return InterestBalance;
    }

    public void setInterestBalance(String interestBalance) {
        InterestBalance = interestBalance;
    }

    public String getLoanBalance() {
        return LoanBalance;
    }

    public void setLoanBalance(String loanBalance) {
        LoanBalance = loanBalance;
    }
}
