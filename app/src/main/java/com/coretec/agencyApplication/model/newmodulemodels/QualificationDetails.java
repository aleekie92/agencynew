package com.coretec.agencyApplication.model.newmodulemodels;

import java.io.Serializable;

public class QualificationDetails implements Serializable {

    private String producttype;
    private String loanpname;
    private String amount;
    private String minbal;

    public String getProducttype() {
        return producttype;
    }

    public void setProducttype(String producttype) {
        this.producttype = producttype;
    }

    public String getLoanpname() {
        return loanpname;
    }

    public void setLoanpname(String loanpname) {
        this.loanpname = loanpname;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getMinbal() {
        return minbal;
    }

    public void setMinbal(String minbal) {
        this.minbal = minbal;
    }
}
