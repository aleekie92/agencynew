package com.coretec.agencyApplication.model;

import com.google.gson.annotations.SerializedName;

public class GuarantorObject {
    @SerializedName("GuarGrowerNo")
    private String growerNumber;
    @SerializedName("GuarAvailKgs")
    private String availableKgs;
    @SerializedName("GuarIdNo")
    private String passport;
    @SerializedName("DocNum")
    private String document;

    public String getGrowerNumber() {
        return growerNumber;
    }

    public void setGrowerNumber(String growerNumber) {
        this.growerNumber = growerNumber;
    }

    public String getAvailableKgs() {
        return availableKgs;
    }

    public void setAvailableKgs(String availableKgs) {
        this.availableKgs = availableKgs;
    }

    public String getPassport() {
        return passport;
    }

    public void setPassport(String passport) {
        this.passport = passport;
    }

    public String getDocument() {
        return document;
    }

    public void setDocument(String document) {
        this.document = document;
    }
}
