package com.coretec.agencyApplication.model;

import java.io.Serializable;

public class netAmount implements Serializable {

    private String NetDisbursement;
    private String NetChaGreaterNetDis;

    public String getNetDisbursement() {
        return NetDisbursement;
    }

    public void setNetDisbursement(String netDisbursement) {
        NetDisbursement = netDisbursement;
    }

    public String getNetChaGreaterNetDis() {
        return NetChaGreaterNetDis;
    }

    public void setNetChaGreaterNetDis(String netChaGreaterNetDis) {
        NetChaGreaterNetDis = netChaGreaterNetDis;
    }
}
