package com.coretec.agencyApplication.model;

/**
 * Created by ofula on 18/06/24.
 */

public class Transact {
    private String name;
    private int image;

    public Transact() {

    }

    public Transact(String name, int image) {
        this.name = name;
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }
}
