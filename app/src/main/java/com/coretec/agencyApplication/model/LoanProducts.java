package com.coretec.agencyApplication.model;

import java.io.Serializable;

public class LoanProducts implements Serializable {

    private String LoanProductCode;
    private String ProductDescription;
    private String IsAvailableOnMobile;
    private String MinLoanAmount;
    private String MaxLoanAmount;
    private String InstallmentPeriod;
    private String IsBonus;
    private String PercentageBonus;
    private String NoOfInstallments;
    private String RatePerKG;
    private String ModeDisbursment;

    public String getLoanProductCode() {
        return LoanProductCode;
    }

    public void setLoanProductCode(String loanProductCode) {
        LoanProductCode = loanProductCode;
    }

    public String getProductDescription() {
        return ProductDescription;
    }

    public void setProductDescription(String productDescription) {
        ProductDescription = productDescription;
    }

    public String getIsAvailableOnMobile() {
        return IsAvailableOnMobile;
    }

    public void setIsAvailableOnMobile(String isAvailableOnMobile) {
        IsAvailableOnMobile = isAvailableOnMobile;
    }

    public String getMinLoanAmount() {
        return MinLoanAmount;
    }

    public void setMinLoanAmount(String minLoanAmount) {
        MinLoanAmount = minLoanAmount;
    }

    public String getMaxLoanAmount() {
        return MaxLoanAmount;
    }

    public void setMaxLoanAmount(String maxLoanAmount) {
        MaxLoanAmount = maxLoanAmount;
    }

    public String getInstallmentPeriod() {
        return InstallmentPeriod;
    }

    public void setInstallmentPeriod(String installmentPeriod) {
        InstallmentPeriod = installmentPeriod;
    }

    public String getIsBonus() {
        return IsBonus;
    }

    public void setIsBonus(String isBonus) {
        IsBonus = isBonus;
    }

    public String getPercentageBonus() {
        return PercentageBonus;
    }

    public void setPercentageBonus(String percentageBonus) {
        PercentageBonus = percentageBonus;
    }

    public String getNoOfInstallments() {
        return NoOfInstallments;
    }

    public void setNoOfInstallments(String noOfInstallments) {
        NoOfInstallments = noOfInstallments;
    }

    public String getRatePerKG() {
        return RatePerKG;
    }

    public void setRatePerKG(String ratePerKG) {
        RatePerKG = ratePerKG;
    }

    public String getModeDisbursment() {
        return ModeDisbursment;
    }

    public void setModeDisbursment(String modeDisbursment) {
        ModeDisbursment = modeDisbursment;
    }
}
