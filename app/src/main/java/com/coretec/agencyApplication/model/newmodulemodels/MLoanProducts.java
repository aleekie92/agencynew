package com.coretec.agencyApplication.model.newmodulemodels;

import java.io.Serializable;

public class MLoanProducts implements Serializable {

    private String productid;
    private String description;

    public String getProductid() {
        return productid;
    }

    public void setProductid(String productid) {
        this.productid = productid;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
