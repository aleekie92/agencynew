package com.coretec.agencyApplication.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class SharesAccountDetails implements Serializable {

    private String accountno;
    private String accountname;
    private String accounttype;
    private String accountbalance;
    private String phonenumber;
    private String transactionstatus;
    @SerializedName("TransactionCode")
    private String TransactionCode;


    public String getAccountno() {
        return accountno;
    }

    public void setAccountno(String accountno) {
        this.accountno = accountno;
    }

    public String getAccountname() {
        return accountname;
    }

    public void setAccountname(String accountname) {
        this.accountname = accountname;
    }

    public String getAccounttype() {
        return accounttype;
    }

    public void setAccounttype(String accounttype) {
        this.accounttype = accounttype;
    }

    public String getAccountbalance() {
        return accountbalance;
    }

    public void setAccountbalance(String accountbalance) {
        this.accountbalance = accountbalance;
    }

    public String getPhonenumber() {
        return phonenumber;
    }

    public void setPhonenumber(String phonenumber) {
        this.phonenumber = phonenumber;
    }

    public String getTransactionstatus() {
        return transactionstatus;
    }

    public void setTransactionstatus(String transactionstatus) {
        this.transactionstatus = transactionstatus;
    }

    public String getTransactionCode() {
        return TransactionCode;
    }

    public void setTransactionCode(String transactionCode) {
        TransactionCode = transactionCode;
    }

    @Override
    public String toString() {

        return TransactionCode+ "-" +accounttype + " - " + accountno;
    }

    /*//@Override
    public String toString1() {
        return TransactionCode+ "-" +accounttype + " - " + accountno;
    }*/

}
