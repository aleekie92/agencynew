package com.coretec.agencyApplication.model;

import java.io.Serializable;

public class BanksList implements Serializable {

    private String Code;
    private String Name;
    private String Branch;

    public String getCode() {
        return Code;
    }

    public void setCode(String code) {
        Code = code;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getBranch() {
        return Branch;
    }

    public void setBranch(String branch) {
        Branch = branch;
    }
}
