package com.coretec.agencyApplication.model.newmodulemodels;

import java.io.Serializable;

public class MLoanDetails implements Serializable {

    private String productid;
    private String description;
    private String cancreate;
    public MLoanDetails(String code, String desc) {
        this.productid=code;
        this.description=desc;
    }

    public String getProductid() {
        return productid;
    }

    public void setProductid(String productid) {
        this.productid = productid;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
