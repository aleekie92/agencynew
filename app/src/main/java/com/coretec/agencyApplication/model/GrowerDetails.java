package com.coretec.agencyApplication.model;

import java.io.Serializable;

/**
 * Created by ofula on 18/06/24.
 */

public class GrowerDetails implements Serializable {

    private String GrowerNo;
    private String GrowerName;
    private String GrowerMobileNo;
    private String FactoryCode;
    private String CustomerPosting;
    private String Status;
    private String Mfedha;
    private String LoanDefaulter;
    private String CurrentKG;
    private String PreviousYrKG;
    private String OutstandingBalance;

    public String getGrowerNo() {
        return GrowerNo;
    }

    public void setGrowerNo(String growerNo) {
        GrowerNo = growerNo;
    }

    public String getGrowerName() {
        return GrowerName;
    }

    public void setGrowerName(String growerName) {
        GrowerName = growerName;
    }

    public String getGrowerMobileNo() {
        return GrowerMobileNo;
    }

    public void setGrowerMobileNo(String growerMobileNo) {
        GrowerMobileNo = growerMobileNo;
    }

    public String getFactoryCode() {
        return FactoryCode;
    }

    public void setFactoryCode(String factoryCode) {
        FactoryCode = factoryCode;
    }

    public String getCustomerPosting() {
        return CustomerPosting;
    }

    public void setCustomerPosting(String customerPosting) {
        CustomerPosting = customerPosting;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String status) {
        Status = status;
    }

    public String getMfedha() {
        return Mfedha;
    }

    public void setMfedha(String mfedha) {
        Mfedha = mfedha;
    }

    public String getLoanDefaulter() {
        return LoanDefaulter;
    }

    public void setLoanDefaulter(String loanDefaulter) {
        LoanDefaulter = loanDefaulter;
    }

    public String getCurrentKG() {
        return CurrentKG;
    }

    public void setCurrentKG(String currentKG) {
        CurrentKG = currentKG;
    }

    public String getPreviousYrKG() {
        return PreviousYrKG;
    }

    public void setPreviousYrKG(String previousYrKG) {
        PreviousYrKG = previousYrKG;
    }

    public String getOutstandingBalance() {
        return OutstandingBalance;
    }

    public void setOutstandingBalance(String outstandingBalance) {
        OutstandingBalance = outstandingBalance;
    }

    @Override
    public String toString() {

        return GrowerNo + " - " + GrowerMobileNo;
    }
}
