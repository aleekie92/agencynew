package com.coretec.agencyApplication.fragments;

import android.content.SharedPreferences;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.coretec.agencyApplication.R;
import com.coretec.agencyApplication.api.GFL;
import com.coretec.agencyApplication.api.requests.GrowersDetailsRequest;
import com.coretec.agencyApplication.api.responses.GrowersDetailsResponse;
import com.coretec.agencyApplication.model.GrowerDetails;
import com.coretec.agencyApplication.utils.SharedPrefs;
import com.stepstone.stepper.BlockingStep;
import com.stepstone.stepper.StepperLayout;
import com.stepstone.stepper.VerificationError;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import static com.coretec.agencyApplication.utils.Utils.getFormattedDate;
import static com.coretec.agencyApplication.utils.Utils.getLat;
import static com.coretec.agencyApplication.utils.Utils.getLong;

public class CustomerDetailsFragment extends Fragment implements BlockingStep {

    private EditText growers_number;
    private EditText registration_name, registration_id, registration_phone_no;
    ProgressBar progressBar;
    private TextView status;

    private int previousLength;
    private boolean backSpace;

    private String phoneisthere, idrexists, bssregistered, statuss, blocked, canregister, rejectionreason;
    private ArrayList<String> arrayList = new ArrayList<>();
    String gNumbers;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.gfl_fragment_registration, container, false);
        SharedPrefs.init(getContext());

        //initialize your UI
        registration_name = v.findViewById(R.id.registration_name);
        registration_id = v.findViewById(R.id.registration_id);
        registration_id.setInputType(InputType.TYPE_NULL);
        registration_id.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // showMyDialog();
            }
        });
        registration_name.setKeyListener(null);

        registration_id.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    // showMyDialog();
                }
            }
        });

        registration_phone_no = v.findViewById(R.id.registration_phone_no);
        registration_phone_no.setSelection(registration_phone_no.getText().length());
        registration_phone_no.setInputType(InputType.TYPE_NULL);
        registration_phone_no.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // showMyDialog();
            }
        });

        registration_phone_no.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    // showMyDialog();
                }
            }
        });

        final TextWatcher textWatcher1 = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                String inputPhone = registration_phone_no.getText().toString();

                if (inputPhone.length() == 1) {
                    if (inputPhone.equals("0") || inputPhone.equals("7") || inputPhone.equals("+")) {
                        registration_phone_no.setText("+2547");
                        registration_phone_no.setSelection(registration_phone_no.getText().length());
                        Toast.makeText(getContext(), "Correct format auto completed. Please proceed!", Toast.LENGTH_SHORT).show();
                    } else {
                        registration_phone_no.setText(null);
                        Toast.makeText(getContext(), "Please enter correct phone number format!", Toast.LENGTH_SHORT).show();
                    }
                }

            }
        };
        registration_phone_no.addTextChangedListener(textWatcher1);

        progressBar = v.findViewById(R.id.growersPb);
        status = v.findViewById(R.id.status);

        growers_number = v.findViewById(R.id.growers_number);

        TextWatcher textWatcherId = new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                //here, after we introduced something in the EditText we get the string from it
                final String identifier_number = growers_number.getText().toString();

                if (growers_number.length() >= 9 && growers_number.length() <= 9) {
                    requestGetAccountDetails(identifier_number);

                }
                if (growers_number.length() >= 5 && growers_number.length() <= 9) {
                    //adapter.clear();
                    //spinner.setAdapter(null);
                    status.setVisibility(View.GONE);
                    registration_name.setText(null);
                    registration_id.setText(null);
                    //date.setText(null);
                    registration_phone_no.setText(null);
                    progressBar.setVisibility(View.VISIBLE);
                    progressBar.getIndeterminateDrawable().setColorFilter(ContextCompat.getColor(getContext(), R.color.btn_green), PorterDuff.Mode.SRC_IN);
                }
                if (identifier_number.length() > 6) {
                    growers_number.setOnKeyListener(new View.OnKeyListener() {
                        @Override
                        public boolean onKey(View v, int keyCode, KeyEvent event) {
                            //You can identify which key pressed buy checking keyCode value with KeyEvent.KEYCODE_
                            if (keyCode == KeyEvent.KEYCODE_DEL) {
                                //this is for backspace

                            } else {
                                if (identifier_number.length() == 9 || identifier_number.length() == 19 || identifier_number.length() == 29) {
                                    growers_number.append(";");
                                }
                            }
                            return false;
                        }
                    });

                }
            }
        };
        growers_number.addTextChangedListener(textWatcherId);

        /// Log.d("is there", phoneisthere);
        Toast.makeText(getActivity(), phoneisthere, Toast.LENGTH_SHORT).show();

        //ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, arrayList);
        for (int i = 0; i < arrayList.size(); i++) {

            gNumbers = arrayList.get(i);
            //integerTextView.setText(integerTextView.getText() + " " + integerData.get(i) + " , ");
        }
        return v;
    }

    public String method(String str) {
        if (str != null && str.length() > 0 && str.charAt(str.length() - 1) == 'x') {
            str = str.substring(0, str.length() - 1);
        }
        return str;
    }

    @Override
    public void onNextClicked(final StepperLayout.OnNextClickedCallback callback) {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                statuss = "pending";
                switch (canregister) {

                    case "Yes":
                        if (phoneisthere != "Yes") {
                            registration_phone_no.setEnabled(true);
                        } else {
                            registration_phone_no.setEnabled(false);
                        }

                        if (growers_number.getText().toString().matches("") ||
                                registration_id.getText().toString().matches("") ||
                                registration_name.getText().toString().matches("") ||
                                registration_phone_no.getText().toString().matches("")) {
                            Toast.makeText(getContext(), "Please make sure all details are filled!", Toast.LENGTH_SHORT).show();
                        } else if (registration_id.getText().toString().length() < 7) {
                            Toast.makeText(getContext(), "Please enter a valid ID Number!", Toast.LENGTH_SHORT).show();
                        } else if (registration_phone_no.getText().toString().length() < 13) {
                            Toast.makeText(getContext(), "Please enter a valid phone Number!", Toast.LENGTH_SHORT).show();
                        } else {
                            //you can do anythings you want
                            String grower_no = growers_number.getText().toString();
                            if (grower_no.length() >= 9 && grower_no.charAt(grower_no.length() - 1) == ';') {
                                grower_no = grower_no.substring(0, grower_no.length() - 1);
                            }
                            SharedPrefs.write(SharedPrefs.REG_GROWER_NO, grower_no);
                            SharedPrefs.write(SharedPrefs.REG_ACC_NAME, registration_name.getText().toString());
                            SharedPrefs.write(SharedPrefs.REG_IDNUMBER, registration_id.getText().toString());
                            SharedPrefs.write(SharedPrefs.REG_PHONE, registration_phone_no.getText().toString());
                            final String identifier_number = growers_number.getText().toString();
                            String msg = "";
                            for (int m = 0; m < arrayList.size(); m++) {
                                msg = "Grower Number : " + arrayList.get(m);
                            }

                            callback.goToNextStep();
                        }
                        break;
                    case "No":
                        Toast.makeText(getContext(), rejectionreason, Toast.LENGTH_LONG).show();
                        break;
                    default:
                        Toast.makeText(getContext(), "Contact systems admin", Toast.LENGTH_LONG).show();
                        break;

                }
            }
        }, 0L);
    }

    @Override
    public void onCompleteClicked(StepperLayout.OnCompleteClickedCallback callback) {
    }

    @Override
    public void onBackClicked(final StepperLayout.OnBackClickedCallback callback) {
    }

    @Override
    public VerificationError verifyStep() {
        return null;
    }

    @Override
    public void onSelected() {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getContext());
        if (!prefs.getBoolean("first_time", false)) {
            SharedPreferences.Editor editor = prefs.edit();
            editor.putBoolean("first_time", true);
            editor.commit();
            if (growers_number.getText().toString().isEmpty()) {
                SharedPrefs.write(SharedPrefs.REG_GROWER_NO, "null");
            }
        } else {
            String g_number = SharedPrefs.read(SharedPrefs.REG_GROWER_NO, null);
            if (!g_number.equals("null")) {
                growers_number.setText(g_number);
            }
        }

    }

    @Override
    public void onError(@NonNull VerificationError error) {
    }

    void requestGetAccountDetails(String identifier) {

        final String TAG = "GrowersAccounts";
        String URL = GFL.MSACCO_AGENT + GFL.GetGrowersDetails;

        final GrowersDetailsRequest growersDetails = new GrowersDetailsRequest();
        growersDetails.agentid = SharedPrefs.read(SharedPrefs.AGENT_ID_1, null);
        growersDetails.corporateno = "CAP016";
        growersDetails.accountidentifier = identifier;
        growersDetails.accountidentifiercode = "2";
        growersDetails.transactiontype = "1";
        growersDetails.terminalid = SharedPrefs.read(SharedPrefs.DEVICE_ID, null);
        growersDetails.longitude = getLong(getContext());
        growersDetails.latitude = getLat(getContext());
        growersDetails.date = getFormattedDate();

        Log.e(TAG, growersDetails.getBody().toString());
        //clear contents of spinner

        GFL.instance(getContext()).request(URL, growersDetails, new GFL.RequestListener() {
            @Override
            public void onSuccess(String response) {
                /*deposit_progressBar.setVisibility(View.GONE);*/

                GrowersDetailsResponse growersDetailsResponse = GFL.instance(getContext())
                        .mGson.fromJson(response, GrowersDetailsResponse.class);
                if (growersDetailsResponse.is_successful) {
                    progressBar.setVisibility(View.GONE);
                    List<GrowerDetails> growerDetailsList = new ArrayList<>();

                    try {
                        JSONObject jsonObject = new JSONObject(response);

                        JSONArray jsonArray = jsonObject.getJSONArray("Growers");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                            JSONArray jsonArray1 = jsonObject1.getJSONArray("GrowerDetails");

                            if (jsonArray1.length() < 1) {
                                registration_name.setText(null);
                                registration_id.setText(null);
                                registration_phone_no.setText(null);
                                Toast.makeText(getContext(), "Customer does not exist in KTDA database, Kindly contact system Administrator or try again!", Toast.LENGTH_LONG).show();
                            } else {

                                for (int l = 0; l < jsonArray1.length(); l++) {
                                    JSONObject jsonObject2 = jsonArray1.getJSONObject(l);
                                    arrayList.add(String.valueOf(jsonObject2.getString("GrowerNo")));
                                    /*mpesaPhoneNumber = jsonObject2.getString("phonenumber");*/
                                    String gName = jsonObject2.getString("GrowerName");
                                    String pNumber = jsonObject2.getString("GrowerMobileNo");
                                    String gIdNumber = jsonObject2.getString("GrowerIdNo");
                                    String dateOB = jsonObject2.getString("Dob");

                                    phoneisthere = jsonObject2.getString("ChangePhoneNumber");
                                    idrexists = jsonObject2.getString("GrowerIdNo");
                                    bssregistered = jsonObject2.getString("BssRegistered");
                                    statuss = jsonObject2.getString("Status");

                                    blocked = jsonObject2.getString("Blocked");
                                    canregister = jsonObject2.getString("isPendingReg");
                                    rejectionreason = jsonObject2.getString("RegrejectReason");

                                    status.setVisibility(View.GONE);
                                    registration_name.setText(gName);
                                    registration_id.setText(gIdNumber);
                                    registration_id.setFocusable(true);
                                    registration_id.setFocusableInTouchMode(true);
                                    registration_phone_no.setSelection(registration_phone_no.getText().length());
                                    registration_id.setSelection(registration_id.getText().length());
                                    registration_phone_no.setText(pNumber);
                                    if (pNumber.length() < 13) {
                                        registration_phone_no.setFocusable(true);
                                        registration_phone_no.setFocusableInTouchMode(true);
                                        registration_phone_no.setSelection(registration_phone_no.getText().length());
                                    } else {
                                        registration_phone_no.setFocusable(false);
                                        registration_phone_no.setFocusableInTouchMode(false);
                                    }
                                }

                            }

                        }


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                } else {
//                    Toast.makeText(ShareDeposit.this, "Accounts Fetching Failed", Toast.LENGTH_LONG).show();
                }

            }

            @Override
            public void onTokenExpired() {

            }

            @Override
            public void onError(String error) {

            }
        });
    }

}
