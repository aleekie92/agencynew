package com.coretec.agencyApplication.fragments.newmodule;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.coretec.agencyApplication.R;
import com.coretec.agencyApplication.activities.newmodules.MainDashboardActivity;
import com.coretec.agencyApplication.api.Api;
import com.coretec.agencyApplication.api.requests.newmodulesrequests.GroupRepaymentRequest;
import com.coretec.agencyApplication.api.responses.newmoduleresponses.RepaymentSubmissionResponse;
import com.coretec.agencyApplication.utils.SharedPrefs;
import com.stepstone.stepper.BlockingStep;
import com.stepstone.stepper.StepperLayout;
import com.stepstone.stepper.VerificationError;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;

import static com.android.volley.VolleyLog.TAG;
import static com.coretec.agencyApplication.utils.Utils.getFormattedDate;
import static com.coretec.agencyApplication.utils.Utils.getLat;
import static com.coretec.agencyApplication.utils.Utils.getLong;

public class GroupPhotoFragment  extends Fragment implements BlockingStep{

        private CardView card_group_photo;
        private static final int CAMERA_REQUEST = 1888;
        private ImageView imageview_group;

        private Button submitPassportPhoto;
        ProgressDialog progressDialog;

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
                View v = inflater.inflate(R.layout.adapter_photo_details, container, false);
                SharedPrefs.init(getContext());

                //initialize your UI
                imageview_group =  v.findViewById(R.id.imageview_group);
                card_group_photo =  v.findViewById(R.id.card_group_photo);

                card_group_photo.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                                Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                                startActivityForResult(cameraIntent, CAMERA_REQUEST);
                        }
                });

                return v;
        }
        private String bitmapToBase64(Bitmap bitmap) {
                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
                byte[] byteArray = byteArrayOutputStream.toByteArray();
                return Base64.encodeToString(byteArray, Base64.DEFAULT);

                //suding this function
        /*Bitmap img = ((BitmapDrawable) passport_imageView.getDrawable()).getBitmap();
        String encodedImage = bitmapToBase64(img);*/
        }

        @Override
        public void onNextClicked(final StepperLayout.OnNextClickedCallback callback) {
               //
        }
        @Override
        public void onCompleteClicked(StepperLayout.OnCompleteClickedCallback callback) {
                new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {


                                String defaultsignature  = "iVBORw0KGgoAAAANSUhEUgAAAVIAAAFSCAIAAAAvt8C4AAAAA3NCSVQICAjb4U/gAAAgAElEQVR4";

                                Bitmap img = ((BitmapDrawable) imageview_group.getDrawable()).getBitmap();
                                final String encodedImage = bitmapToBase64(img);

                                String testImage = encodedImage.substring(0,76);

                                if (testImage.equals(defaultsignature)) {
                                        Toast.makeText(getContext(), "Click to capture group photo!", Toast.LENGTH_SHORT).show();
                                } else {
                                        SharedPrefs.groupphoto(encodedImage,getActivity());

                                        String msg = "Are you sure you want to submit micro group payment?";
                                        //final String qualKgs = SharedPrefs.read(SharedPrefs.LOAN_QUALIFIED_KGS, null);
                                        AlertDialog.Builder builder1 = new AlertDialog.Builder(getContext());
                                        builder1.setMessage(msg);
                                        builder1.setTitle("Confirm!");
                                        builder1.setCancelable(false);

                                        builder1.setPositiveButton(
                                                "YES",
                                                new DialogInterface.OnClickListener() {
                                                        public void onClick(DialogInterface dialog, int id) {
                                                                dialog.cancel();

                                                                progressDialog = new ProgressDialog(getContext());
                                                                progressDialog.setMessage("Submitting group payments details...");
                                                                progressDialog.setCancelable(false);
                                                                progressDialog.show();
                                                                submitGroupPayments();

                                                        }
                                                });

                                        builder1.setNegativeButton(
                                                "Cancel",
                                                new DialogInterface.OnClickListener() {
                                                        public void onClick(DialogInterface dialog, int id) {
                                                                dialog.cancel();
                                                        }
                                                });

                                        AlertDialog alert11 = builder1.create();
                                        alert11.show();
                                }
                        }
                }, 0L);
        }
        @Override
        public void onBackClicked(final StepperLayout.OnBackClickedCallback callback) {
                callback.goToPrevStep();
        }
        @Override
        public VerificationError verifyStep() {
                return null;
        }
        @Override
        public void onSelected() {
        }
        @Override
        public void onError(@NonNull VerificationError error) {
        }

        @Override
        public void onActivityResult(int requestCode, int resultCode, Intent data) {
                super.onActivityResult(requestCode, resultCode, data);
                switch (requestCode) {
                        case CAMERA_REQUEST:
                                if (resultCode == Activity.RESULT_OK) {
                                        try {
                                                Bitmap photo = (Bitmap) data.getExtras().get("data");
                                                imageview_group.setImageBitmap(photo);

                                                //imageView.setImageBitmap(bitmap);
                        /*Toast.makeText(this, imageview_reason.toString(),
                                Toast.LENGTH_SHORT).show();*/
                                        } catch (Exception e) {
                                                Toast.makeText(getContext(), "Failed to load", Toast.LENGTH_SHORT)
                                                        .show();
                                                Log.e("Camera", e.toString());
                                        }
                                }
                }
        }

        void submitGroupPayments(){

                String URL = Api.MSACCO_AGENT + Api.MicroGroupTransaction;

                final GroupRepaymentRequest reprequest = new GroupRepaymentRequest();
                reprequest.corporate_no=SharedPrefs.read(SharedPrefs.VR_COOPORATE_NO,null);// "CAP002";
                reprequest.groupcode=SharedPrefs.read(SharedPrefs.VR_GROUP_CODE, null);//store it in shared preference at memberfragment stage.
                reprequest.totalloanpaid= SharedPrefs.read(SharedPrefs.VR_TOTAL_LOANS, null);
                reprequest.totalloanunpaid= SharedPrefs.read(SharedPrefs.VR_TOTAL_UNPAID, null);
                reprequest.totalpaid= SharedPrefs.read(SharedPrefs.VR_TOTAL_SAVING_AND_LOAN_PAID, null);
                reprequest.totalsavingspaid= SharedPrefs.read(SharedPrefs.VR_TOTAL_SAVINGS, null);
                reprequest.verifiedby= SharedPrefs.read(SharedPrefs.VR_GROUP_PAYMENT_VERIFIED_BY, null);
                reprequest.verifyphone= SharedPrefs.read(SharedPrefs.VR_GROUP_PAYMENT_VERIFIER_PHONE, null);
                reprequest.initializationtime= getFormattedDate();
                reprequest.totalpresentmember= SharedPrefs.read(SharedPrefs.VR_TOTAL_PRESENT_MEMBERS, null);
                reprequest.totalabstmember= SharedPrefs.read(SharedPrefs.VR_TOTAL_ABSENT_MEMBERS, null);
                reprequest.groupimage = SharedPrefs.read(SharedPrefs.VR_GROUP_PHOTO, null);
                reprequest.remarks= SharedPrefs.read(SharedPrefs.VR_GROUP_REMARKS, null);
                reprequest.agentid= SharedPrefs.read(SharedPrefs.AGENT_ID_1, null);
                reprequest.terminalid= MainDashboardActivity.imei;
                reprequest.longitude=getLong(getContext());
                reprequest.latitude= getLat(getContext());
                reprequest.date= getFormattedDate();
                Log.e(TAG, reprequest.getBody().toString());


                Api.instance(getContext()).request(URL, reprequest, new Api.RequestListener() {
                        @Override
                        public void onSuccess(String response) {
                                /*deposit_progressBar.setVisibility(View.GONE);*/

                                RepaymentSubmissionResponse repaymentResponse = Api.instance(getContext())
                                        .mGson.fromJson(response, RepaymentSubmissionResponse.class);

                               // Log.e("myresponccccc", String.valueOf(repaymentResponse.is_successful));
                                //Log.e("myresponccccc", reprequest.groupimage);
                                if (repaymentResponse.is_successful) {
                                        progressDialog.dismiss();

                                        try {
                                                JSONObject jsonObject = new JSONObject(response);

                                                String receiptNo = jsonObject.getString("receiptno");
                                                String sacconameee = jsonObject.getString("sacconame");

                                                Toast.makeText(getContext(), receiptNo, Toast.LENGTH_LONG).show();
                                                Log.e("rrrrrrrtttt", receiptNo);
                                                Log.e("sssssscccc", sacconameee);

                                        } catch (JSONException e) {
                                                e.printStackTrace();
                                        }
                                        String msg = "Group Payments submitted successfully!";
                                        AlertDialog.Builder builder1 = new AlertDialog.Builder(getContext());
                                        builder1.setMessage(msg);
                                        builder1.setTitle("Success!");
                                        builder1.setCancelable(false);

                                        builder1.setPositiveButton(
                                                "DONE",
                                                new DialogInterface.OnClickListener() {
                                                        public void onClick(DialogInterface dialog, int id) {
                                                                dialog.cancel();
                                                                getActivity().finish();
                                                        }
                                                });

                                        AlertDialog alert11 = builder1.create();
                                        alert11.show();

                                } else {
                                        progressDialog.dismiss();
                                        Toast.makeText(getContext(), "micro group payment failed. Try again later!", Toast.LENGTH_LONG).show();
                                }

                        }

                        @Override
                        public void onTokenExpired() {

                        }

                        @Override
                        public void onError(String error) {

                        }
                });
        }
}
