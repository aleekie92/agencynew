package com.coretec.agencyApplication.fragments;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.cloudpos.DeviceException;
import com.cloudpos.OperationResult;
import com.cloudpos.POSTerminal;
import com.cloudpos.TimeConstants;
import com.cloudpos.fingerprint.Fingerprint;
import com.cloudpos.fingerprint.FingerprintDevice;
import com.cloudpos.fingerprint.FingerprintOperationResult;
import com.coretec.agencyApplication.R;
import com.coretec.agencyApplication.activities.gfl.MainGFLDashboard;
import com.coretec.agencyApplication.api.GFL;
import com.coretec.agencyApplication.api.requests.CreateAgentBioRequest;
import com.coretec.agencyApplication.api.responses.CreateAgentBioResponse;
import com.coretec.agencyApplication.utils.ByteConvertStringUtil;
import com.coretec.agencyApplication.utils.LogHelper;
import com.coretec.agencyApplication.utils.SharedPrefs;
import com.stepstone.stepper.BlockingStep;
import com.stepstone.stepper.StepperLayout;
import com.stepstone.stepper.VerificationError;

import java.text.SimpleDateFormat;
import java.util.Date;

public class AgentBioRegistrationFragment extends Fragment implements BlockingStep, View.OnClickListener {

    //Button enroll;
    Button leftRing, leftLittle, rightRing, rightLittle,
            leftmiddle, rightmiddle,leftindex,rightindex,leftthumb,rightthumb;
    Button delete;
    Button clear;
    Button quit;
    TextView enrollText;

    ProgressDialog progressDialog;

    //crossmatch fingerprint declarations
    protected TextView log_text;
    protected Handler mHandler = null;
    protected Runnable runnable = null;

    //Fingerprint Declarations
    private TextView show;


    //private Context context = getActivity();
    //private Context mContext = this;
    private int userID = 0;
    private int timeout = 10 * 1000;
    private FingerprintDevice fingerprintDevice = null;

    private SharedPreferences preferences;
    private SharedPreferences.Editor editor;

    final String AGENTFINGERINDEX1 = "AFinger1";
    final String AGENTFINGERINDEX2 = "AFinger2";
    final String AGENTFINGERINDEX3 = "AFinger3";
    final String AGENTFINGERINDEX4 = "AFinger4";

    final String AGENTFINGERINDEX5 = "AFinger5";
    final String AGENTFINGERINDEX6 = "AFinger6";
    final String AGENTFINGERINDEX7 = "AFinger7";
    final String AGENTFINGERINDEX8 = "AFinger8";
    final String AGENTFINGERINDEX9 = "AFinger9";
    final String AGENTFINGERINDEX10 = "AFinger10";

    private Handler handler;
    private static final int SHOW_NORMAL_MESSAGE = 0;
    private static final int SHOW_SUCCESS_MESSAGE = 1;
    private static final int SHOW_FAIL_MESSAGE = 2;
    private static final int SHOW_BTN = 3;
    private static final int HIDE_BTN = 4;
    Thread th = null;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.agentfingerprints_fragment_bioregistration, container, false);
        SharedPrefs.init(getContext());

        //initialize your UI
        leftRing = v.findViewById(R.id.btn_left_ring);
        leftLittle = v.findViewById(R.id.btn_left_little);
        rightRing = v.findViewById(R.id.btn_right_ring);
        rightLittle = v.findViewById(R.id.btn_right_little);

        leftmiddle = v.findViewById(R.id.btn_left_middle);
        rightmiddle = v.findViewById(R.id.btn_right_middle);
        leftindex = v.findViewById(R.id.btn_left_index);
        rightindex = v.findViewById(R.id.btn_right_index);
        leftthumb = v.findViewById(R.id.btn_left_thumb);
        rightthumb = v.findViewById(R.id.btn_right_thumb);

        delete = v.findViewById(R.id.ButtonDelete);
        clear = v.findViewById(R.id.ButtonClear);
        show = v.findViewById(R.id.EnrollmentTextView);
        leftRing.setOnClickListener(this);
        leftLittle.setOnClickListener(this);
        rightRing.setOnClickListener(this);
        rightLittle.setOnClickListener(this);

        leftmiddle.setOnClickListener(this);
        rightmiddle.setOnClickListener(this);
        leftindex.setOnClickListener(this);
        rightindex.setOnClickListener(this);
        leftthumb.setOnClickListener(this);
        rightthumb.setOnClickListener(this);

        delete.setOnClickListener(this);
        clear.setOnClickListener(this);
        preferences = getActivity().getSharedPreferences("userFinger", Context.MODE_PRIVATE);
        editor = preferences.edit();
        editor.putString(AGENTFINGERINDEX1, "0");
        editor.putString(AGENTFINGERINDEX2, "0");
        editor.putString(AGENTFINGERINDEX3, "0");
        editor.putString(AGENTFINGERINDEX4, "0");

        editor.putString(AGENTFINGERINDEX5, "0");
        editor.putString(AGENTFINGERINDEX6, "0");
        editor.putString(AGENTFINGERINDEX7, "0");
        editor.putString(AGENTFINGERINDEX8, "0");
        editor.putString(AGENTFINGERINDEX9, "0");
        editor.putString(AGENTFINGERINDEX10, "0");
        editor.apply();

        //log_text = this.findViewById(R.id.text_result);
        show.setMovementMethod(ScrollingMovementMethod.getInstance());

        handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                super.handleMessage(msg);
                switch (msg.what) {
                    case SHOW_NORMAL_MESSAGE:
                        sendMsg((String) msg.obj);
                        scrollLogView();
                        break;
                    case SHOW_SUCCESS_MESSAGE:
                        sendSuccessMsg((String) msg.obj);
                        scrollLogView();
                        break;
                    case SHOW_FAIL_MESSAGE:
                        sendFailMsg((String) msg.obj);
                        scrollLogView();
                        break;

                    case SHOW_BTN:

                        break;
                    case HIDE_BTN:

                        break;
                }
            }
        };
        try {
            fingerprintDevice = (FingerprintDevice) POSTerminal.getInstance(getActivity()).getDevice("cloudpos.device.fingerprint");
            fingerprintDevice.open(1);
            //delAllFingers();
        } catch (DeviceException e) {
            e.printStackTrace();
        }

        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    int result = fingerprintDevice.delAllFingers();
                    if (result >= 0) {
                        handler.obtainMessage(SHOW_SUCCESS_MESSAGE, getStr(R.string.Cleared)).sendToTarget();
                    }
                } catch (DeviceException e) {
                    e.printStackTrace();
                    handler.obtainMessage(SHOW_FAIL_MESSAGE, getStr(R.string.MatchFailed)).sendToTarget();
                }
                Toast.makeText(getActivity(), getText(R.string.Cleared), Toast.LENGTH_SHORT).show();
            }
        });
        clear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //textView.setText("");
                show.setText("");
            }
        });

        return  v;
    }
    @Override
    public void onNextClicked(StepperLayout.OnNextClickedCallback callback) {

    }

    @Override
    public void onCompleteClicked(StepperLayout.OnCompleteClickedCallback callback) {

        String msg = "Are you sure you want to submit bio data?";
        //final String qualKgs = SharedPrefs.read(SharedPrefs.LOAN_QUALIFIED_KGS, null);
        AlertDialog.Builder builder1 = new AlertDialog.Builder(getActivity());
        builder1.setMessage(msg);
        builder1.setTitle("Confirm!");
        builder1.setCancelable(false);

        builder1.setPositiveButton(
                "YES",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();

                        SharedPreferences prefs = getActivity().getSharedPreferences("userFinger", Context.MODE_PRIVATE);
                        String finger1 = prefs.getString("AFinger1", null);
                        String finger2 = prefs.getString("AFinger2", null);
                        String finger3 = prefs.getString("AFinger3", null);
                        String finger4 = prefs.getString("AFinger4", null);

                        String finger5 = prefs.getString("AFinger5", null);
                        String finger6 = prefs.getString("AFinger6", null);
                        String finger7 = prefs.getString("AFinger7", null);
                        String finger8 = prefs.getString("AFinger8", null);
                        String finger9 = prefs.getString("AFinger9", null);
                        String finger10 = prefs.getString("AFinger10", null);



                        assert finger1 != null;
                        assert finger2 != null;
                        assert finger3 != null;
                        assert finger4 != null;

                        assert finger5 != null;
                        assert finger6 != null;
                        assert finger7 != null;
                        assert finger8 != null;
                        assert finger9 != null;
                        assert finger10 != null;

                        if (finger1.equals("0") || finger2.equals("0") || finger3.equals("0") || finger4.equals("0")|| finger5.equals("0")
                                || finger6.equals("0")|| finger7.equals("0")|| finger8.equals("0")|| finger9.equals("0")|| finger10.equals("0")) {
                            Toast.makeText(getActivity(), "Please input user fingerprints to proceed!", Toast.LENGTH_SHORT).show();
                        } else {

                            progressDialog = new ProgressDialog(getActivity());
                            progressDialog.setMessage("Agent bio registration details...");
                            progressDialog.setCancelable(false);
                            progressDialog.show();
                            submitAgentBioRegistration();
                            //startActivity(new Intent(getContext(), Loans.class));
                        }
                    }
                });

        builder1.setNegativeButton(
                "Cancel",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

        AlertDialog alert11 = builder1.create();
        alert11.show();
    }

    @Override
    public void onBackClicked(StepperLayout.OnBackClickedCallback callback) {
        callback.goToPrevStep();
    }

    @Override
    public VerificationError verifyStep() {
        return null;
    }

    @Override
    public void onSelected() {

    }

    @Override
    public void onError(@NonNull VerificationError error) {

    }

    @Override
    public void onClick(View v) {

        if (v.getId() != R.id.ButtonDelete) {
            sendMsg(getStr(R.string.MESSAGE));
        }
        if (v.getId() == R.id.ButtonDelete || v.getId() == R.id.ButtonClear) {
            switch (v.getId()) {
                case R.id.ButtonDelete:
                    try {
                        int result = fingerprintDevice.delAllFingers();
                        if (result >= 0) {
                            handler.obtainMessage(SHOW_SUCCESS_MESSAGE, getStr(R.string.Cleared)).sendToTarget();
                        }
                    } catch (DeviceException e) {
                        e.printStackTrace();
                        handler.obtainMessage(SHOW_FAIL_MESSAGE, getStr(R.string.MatchFailed)).sendToTarget();
                    }

                    break;
                case R.id.ButtonClear:
                    show.setText("");
                    break;
            }
        } else {
            if (th == null || th.getState() == Thread.State.TERMINATED) {
                switch (v.getId()) {
                    case R.id.btn_left_ring:
                        th = new Thread() {
                            @Override
                            public void run() {
                                fingerPut(AGENTFINGERINDEX1);
                            }
                        };

                        break;
                    case R.id.btn_left_little:
                        th = new Thread() {
                            @Override
                            public void run() {
                                fingerPut(AGENTFINGERINDEX2);
                            }
                        };
                        break;
                    case R.id.btn_right_ring:
                        th = new Thread() {
                            @Override
                            public void run() {
                                fingerPut(AGENTFINGERINDEX3);
                            }
                        };
                        break;
                    case R.id.btn_right_little:
                        th = new Thread() {
                            @Override
                            public void run() {
                                fingerPut(AGENTFINGERINDEX4);
                            }
                        };
                        break;



                    case R.id.btn_left_middle:
                        th = new Thread() {
                            @Override
                            public void run() {
                                fingerPut(AGENTFINGERINDEX5);
                            }
                        };
                        break;
                    case R.id.btn_right_middle:
                        th = new Thread() {
                            @Override
                            public void run() {
                                fingerPut(AGENTFINGERINDEX6);
                            }
                        };
                        break;
                    case R.id.btn_left_index:
                        th = new Thread() {
                            @Override
                            public void run() {
                                fingerPut(AGENTFINGERINDEX7);
                            }
                        };
                        break;
                    case R.id.btn_right_index:
                        th = new Thread() {
                            @Override
                            public void run() {
                                fingerPut(AGENTFINGERINDEX8);
                            }
                        };
                        break;
                    case R.id.btn_left_thumb:
                        th = new Thread() {
                            @Override
                            public void run() {
                                fingerPut(AGENTFINGERINDEX9);
                            }
                        };
                        break;
                    case R.id.btn_right_thumb:
                        th = new Thread() {
                            @Override
                            public void run() {
                                fingerPut(AGENTFINGERINDEX10);
                            }
                        };
                        break;
                }
                th.start();
            }
        }

    } private void fingerPut(String fingerIndex) {
        preferences = getActivity().getSharedPreferences("userFinger", Context.MODE_PRIVATE);
        editor = preferences.edit();
        Fingerprint fingerprint = getFingerprint();
        if (fingerprint != null) {
            byte[] buffer1 = fingerprint.getFeature();
            Log.e("FINGERPRINT_DEMO", buffer1.length + "");
            editor.putString(fingerIndex, ByteConvertStringUtil.bytesToHexString(buffer1));
            editor.apply();
        }
        handler.obtainMessage(SHOW_SUCCESS_MESSAGE, getString(R.string.entry) + fingerIndex).sendToTarget();
    }

    private Fingerprint getFingerprint() {
        Fingerprint fingerprint = null;
        try {
            FingerprintOperationResult operationResult = fingerprintDevice.waitForFingerprint(TimeConstants.FOREVER);
            if (operationResult.getResultCode() == OperationResult.SUCCESS) {
                fingerprint = operationResult.getFingerprint(0, 0);
                Log.e("FINGERPRINT_DEMO", "operationResult SUCCESS!" + fingerprint.getFeature().length);
                //handler.obtainMessage(SHOW_SUCCESS_MESSAGE, mContext.getString(R.string.SUCCESSINFO)).sendToTarget();
            } else {
                handler.obtainMessage(SHOW_FAIL_MESSAGE, getString(R.string.FAILEDINFO)).sendToTarget();
            }
        } catch (DeviceException e) {
            e.printStackTrace();
            handler.obtainMessage(SHOW_FAIL_MESSAGE, getString(R.string.DEVICEFAILED)).sendToTarget();
        }
        return fingerprint;
    }

    private void sendSuccessMsg(String msg) {
        LogHelper.infoAppendMsgForSuccess(msg + "\n", show);
        scrollLogView();
    }

    private void sendFailMsg(String msg) {
        LogHelper.infoAppendMsgForFailed(msg + "\n", show);
        scrollLogView();
    }

    private void sendMsg(String msg) {
        LogHelper.infoAppendMsg(msg + "\n", show);
        scrollLogView();
    }

    public void scrollLogView() {
        int offset = show.getLineCount() * show.getLineHeight();
        if (offset > show.getHeight()) {
            show.scrollTo(0, offset - show.getHeight());
        }
    }

    private String getStr(int strId) {
        return getResources().getString(strId);
    }

    void submitAgentBioRegistration(){
        SharedPreferences prefs = getActivity().getSharedPreferences("userFinger", Context.MODE_PRIVATE);
        String finger1 = prefs.getString("AFinger1", null);
        String finger2 = prefs.getString("AFinger2", null);
        String finger3 = prefs.getString("AFinger3", null);
        String finger4 = prefs.getString("AFinger4", null);

        String finger5 = prefs.getString("AFinger5", null);
        String finger6 = prefs.getString("AFinger6", null);
        String finger7 = prefs.getString("AFinger7", null);
        String finger8 = prefs.getString("AFinger8", null);
        String finger9 = prefs.getString("AFinger9", null);
        String finger10 = prefs.getString("AFinger10", null);

        String URL = GFL.MSACCO_AGENT + GFL.RegisterAgentFingerPrints;
        SimpleDateFormat timeStampFormat = new SimpleDateFormat("yyyy/MM/dd - HH:mm");
        Date myDate = new Date();



        final CreateAgentBioRequest bioData = new CreateAgentBioRequest();

        final String TAG = "AgentBioRegistration";
        bioData.leftring = finger1;
        bioData.leftlittle = finger2;
        bioData.rightring = finger3;
        bioData.rightlittle = finger4;
        bioData.leftthree = finger5;
        bioData.rightthree = finger6;
        bioData.leftfour = finger7;
        bioData.rightfour = finger8;
        bioData.leftfive = finger9;
        bioData.rightfive = finger10;

        bioData.idno = SharedPrefs.read(SharedPrefs.AGENT_ID_NO, null);
        bioData.passportphoto = SharedPrefs.read(SharedPrefs.AGENT_PASS_PHOTO, null);
        bioData.agentid = SharedPrefs.read(SharedPrefs.BIO_AGENT_CODE, null);//from input in the first fragment
        bioData.agentidlogged = SharedPrefs.read(SharedPrefs.AGENT_ID_1, null);//loggedin agent
        bioData.terminalid = MainGFLDashboard.imei;
        Log.e(TAG, bioData.getBody().toString());
        GFL.instance(getActivity()).request(URL, bioData, new GFL.RequestListener() {
            @Override
            public void onSuccess(String response) {

                CreateAgentBioResponse createVirtualRegistrationResponse = GFL.instance(getActivity())
                        .mGson.fromJson(response, CreateAgentBioResponse.class);
                if (createVirtualRegistrationResponse.is_successful) {
                    progressDialog.dismiss();

                    String msg = "Bio data submitted successfully!";
                    AlertDialog.Builder builder1 = new AlertDialog.Builder(getActivity());
                    builder1.setMessage(msg);
                    builder1.setTitle("Success!");
                    builder1.setCancelable(false);

                    builder1.setPositiveButton(
                            "DONE",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                    getActivity().finish();
                                }
                            });



                    AlertDialog alert11 = builder1.create();
                    alert11.show();
                    //submit.setVisibility(View.GONE);
                    // getGuarantor();
                } else {
                    progressDialog.dismiss();
                    String message = createVirtualRegistrationResponse.getError();
                    android.app.AlertDialog.Builder builder1 = new android.app.AlertDialog.Builder(getActivity());
                    builder1.setMessage(message);
                    builder1.setTitle("Failed!");
                    builder1.setCancelable(true);

                    builder1.setPositiveButton(
                            "Okay",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    getActivity().finish();
                                }
                            });

                    android.app.AlertDialog alert11 = builder1.create();
                    alert11.show();
                    //Toast.makeText(getActivity(), "Agennt registration failed. Try again later!", Toast.LENGTH_LONG).show();
                }

            }

            @Override
            public void onTokenExpired() {

            }

            @Override
            public void onError(String error) {

            }
        });
    }
}