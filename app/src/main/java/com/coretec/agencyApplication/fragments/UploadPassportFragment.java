package com.coretec.agencyApplication.fragments;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.coretec.agencyApplication.R;
import com.coretec.agencyApplication.utils.SharedPrefs;
import com.stepstone.stepper.BlockingStep;
import com.stepstone.stepper.StepperLayout;
import com.stepstone.stepper.VerificationError;

import java.io.ByteArrayOutputStream;

public class UploadPassportFragment extends Fragment implements BlockingStep {

    private CardView card_passport_photo;
    private static final int CAMERA_REQUEST = 1888;
    private ImageView passport_imageView;
    private Button submitPassportPhoto;
    String defaultPhoto;
    String comparePhoto;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.gfl_fragment_passport, container, false);
        SharedPrefs.init(getContext());

        passport_imageView = v.findViewById(R.id.imageview_passport);

        card_passport_photo = v.findViewById(R.id.card_passport_photo);
        card_passport_photo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(cameraIntent, CAMERA_REQUEST);
            }
        });

        return v;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case CAMERA_REQUEST:
                if (resultCode == Activity.RESULT_OK) {
                    try {
                        Bitmap photo = (Bitmap) data.getExtras().get("data");
                        passport_imageView.setImageBitmap(photo);
                    } catch (Exception e) {
                        Toast.makeText(getContext(), "Failed to load", Toast.LENGTH_SHORT)
                                .show();
                        Log.e("Camera", e.toString());
                    }
                }
        }
    }

    private String bitmapToBase64(Bitmap bitmap) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
        byte[] byteArray = byteArrayOutputStream.toByteArray();
        return Base64.encodeToString(byteArray, Base64.DEFAULT);

    }

    @Override
    public void onNextClicked(final StepperLayout.OnNextClickedCallback callback) {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                String defaultPassport = "iVBORw0KGgoAAAANSUhEUgAAAcIAAAHCCAIAAADzel4SAAAAA3NCSVQICAjb4U";

                Bitmap img = ((BitmapDrawable) passport_imageView.getDrawable()).getBitmap();
                String encodedImage = bitmapToBase64(img);

                String testImage = encodedImage.substring(0, 62);

                if (testImage.equals(defaultPassport)) {
                    Toast.makeText(getContext(), "Please take a picture to proceed!", Toast.LENGTH_SHORT).show();
                } else {
                    Log.d("image", "image---->" + encodedImage);
                    SharedPrefs.write(SharedPrefs.REG_PASSPORT, encodedImage);
                    //you can do anythings you want
                    callback.goToNextStep();
                }
            }
        }, 0L);// delay open another fragment,
    }

    @Override
    public void onCompleteClicked(StepperLayout.OnCompleteClickedCallback callback) {
    }

    @Override
    public void onBackClicked(final StepperLayout.OnBackClickedCallback callback) {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                //you can do anythings you want
                /*callback.goToNextStep();*/
                callback.goToPrevStep();
            }
        }, 0L);// delay open another fragment,
    }

    @Override
    public VerificationError verifyStep() {
        return null;
    }

    @Override
    public void onSelected() {

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getContext());
        if (!prefs.getBoolean("first_time_pass", false)) {
            SharedPreferences.Editor editor = prefs.edit();
            editor.putBoolean("first_time_pass", true);
            editor.commit();

            String defaultPassport = "iVBORw0KGgoAAAANSUhEUgAAAcIAAAHCCAIAAADzel4SAAAAA3NCSVQICAjb4U";

            Bitmap img = ((BitmapDrawable) passport_imageView.getDrawable()).getBitmap();
            String encodedImage = bitmapToBase64(img);

            String testImage = encodedImage.substring(0, 62);

            if (testImage.equals(defaultPassport)) {
                SharedPrefs.write(SharedPrefs.REG_PASSPORT, "null");
            }
        } else {
            String imgPassport = SharedPrefs.read(SharedPrefs.REG_PASSPORT, null);

            if (!imgPassport.equals("null")) {

                byte[] decodedString = Base64.decode(imgPassport, Base64.DEFAULT);
                Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);

                passport_imageView.setImageBitmap(decodedByte);
            }
        }
    }

    @Override
    public void onError(@NonNull VerificationError error) {
    }

}
