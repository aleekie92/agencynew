package com.coretec.agencyApplication.fragments;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.coretec.agencyApplication.R;
import com.coretec.agencyApplication.activities.LoanRepayment;
import com.coretec.agencyApplication.activities.gfl.MiniStatement;
import com.coretec.agencyApplication.activities.newmodules.MainDashboardActivity;
import com.coretec.agencyApplication.api.Api;
import com.coretec.agencyApplication.api.requests.GetEmployerCodeRequest;
import com.coretec.agencyApplication.api.requests.RequestMemberName;
import com.coretec.agencyApplication.api.requests.newmodulesrequests.SacooBranchRequest;
import com.coretec.agencyApplication.api.responses.GetEmployerCodeResponse;
import com.coretec.agencyApplication.api.responses.MemberNameResponse;
import com.coretec.agencyApplication.api.responses.newmoduleresponses.SaccoBranchesResponse;
import com.coretec.agencyApplication.utils.PreferenceFileKeys;
import com.coretec.agencyApplication.utils.SharedPrefs;
import com.stepstone.stepper.BlockingStep;
import com.stepstone.stepper.StepperLayout;
import com.stepstone.stepper.VerificationError;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;

import in.galaxyofandroid.spinerdialog.OnSpinerItemClick;
import in.galaxyofandroid.spinerdialog.SpinnerDialog;

import static com.coretec.agencyApplication.utils.Utils.getLat;
import static com.coretec.agencyApplication.utils.Utils.getLong;

public class AgencyCustomerFragment extends Fragment implements BlockingStep {

    private EditText registration_name, registration_id, registration_phone_no,registration_amount,edtemploymentnumber,employercode;
    private TextInputLayout input_layout_registration_name,input_layout_registration_id, input_layout_registration_phone_no,input_layout_registration_amount;

    DatePickerDialog datePickerDialog;
    private Spinner spinner,salutation;
    SpinnerDialog spinnerDialog;
    private Spinner status, citizen;

    ProgressBar progressBar;
    private TextView idMessage, date,loading;
    private RadioGroup radiogroupemploymentdetails;
    private RadioButton rbemployed,rbnotemployed;
   // private LinearLayout linearemployment;
    private  TextInputLayout tilemploymentnumber,txtinputemployercode;
    private ArrayList<String> employersList = new ArrayList<>();
    SharedPreferences sharedPref;
    private  String membername, validatephoneno;
    int agee;
    private ArrayList<String> nationalityList = new ArrayList<>();
    String result;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.adapter_customer_details, container, false);
        SharedPrefs.init(getContext());
        sharedPref = getActivity().getSharedPreferences(PreferenceFileKeys.PREFS_FILE, Context.MODE_PRIVATE);
        input_layout_registration_name = v.findViewById(R.id.input_layout_registration_name);
        input_layout_registration_id = v.findViewById(R.id.input_layout_registration_id);
        input_layout_registration_phone_no = v.findViewById(R.id.input_layout_registration_phone_no);
        input_layout_registration_amount = v.findViewById(R.id.input_layout_registration_amount);
        registration_name = v.findViewById(R.id.registration_name);
        registration_id = v.findViewById(R.id.registration_id);
        registration_phone_no = v.findViewById(R.id.registration_phone_no);

        registration_amount = v.findViewById(R.id.registration_amount);
        date =  v.findViewById(R.id.dob);

        idMessage = v.findViewById(R.id.status);
        loading = v.findViewById(R.id.loading);

        citizen = v.findViewById(R.id.citizenship);
        citizen.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String text = citizen.getSelectedItem().toString();
                result =  text.substring(0,text.lastIndexOf("."));
                SharedPrefs.citizenship(result,getActivity());

                if(!result.equals("+268")){
                    registration_phone_no.setVisibility(View.VISIBLE);
                    registration_amount.setVisibility(View.VISIBLE);
                }/*if (result.equals("Select-citizenship.")){
                    registration_phone_no.setVisibility(View.GONE);
                    registration_amount.setVisibility(View.GONE);
                }*/
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        radiogroupemploymentdetails = v.findViewById(R.id.radiogroupemploymentdetails);
        tilemploymentnumber = v.findViewById(R.id.tilemploymentnumber);
        edtemploymentnumber = v.findViewById(R.id.edtemploymentnumber);
        txtinputemployercode= v.findViewById(R.id.txtinputemployercode);

        rbemployed = v.findViewById(R.id.rbemployed);
        rbnotemployed = v.findViewById(R.id.rbnotemployed);
        employercode = v.findViewById(R.id.employercode);

        SharedPrefs.employer("NODETAILS",getActivity());
        SharedPrefs.employernumber("00", getActivity());

        date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Calendar c = Calendar.getInstance();
                int mYear = c.get(Calendar.YEAR); // current year
                int mMonth = c.get(Calendar.MONTH); // current month
                int mDay = c.get(Calendar.DAY_OF_MONTH); // current day
                // date picker dialog
                datePickerDialog = new DatePickerDialog(getContext(),
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {

                                date.setText(dayOfMonth + "/"
                                        + (monthOfYear + 1) + "/" + year);
                                String dateSelected = date.getText().toString();
                                SharedPrefs.dob(dateSelected,getActivity());

                                Calendar calendar = Calendar.getInstance();
                                int yy = calendar.get(Calendar.YEAR);
                                agee = yy - year;

                            }
                        }, mYear, mMonth, mDay);
                datePickerDialog.show();
                datePickerDialog.updateDate(2001, 1, 1);
            }
        });
        spinner = v.findViewById(R.id.registration_spinner);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                String text = spinner.getSelectedItem().toString();
                SharedPrefs.gender(text,getActivity());
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        salutation = v.findViewById(R.id.salutation);
        salutation.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                //String item = parent.getItemAtPosition(position).toString();
                String text = salutation.getSelectedItem().toString();
                SharedPrefs.salutation(text,getActivity());

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        employercode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    spinnerDialog.showSpinerDialog();
                }catch (Exception e){
                    Toast.makeText(getActivity(), "give it a second....", Toast.LENGTH_SHORT).show();
                }
            }
        });

        final TextWatcher textWatcherIdNumber = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                String identifier_number = registration_id.getText().toString();

                if (identifier_number.length() >= 7) {
                    loading.setVisibility(View.VISIBLE);
                    requestMemberDetails(identifier_number);
                }

            }
        };
        registration_id.addTextChangedListener(textWatcherIdNumber);
        registration_phone_no.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    // showMyDialog();
                }
            }
        });

        final TextWatcher textWatcher1 = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }
            @Override
            public void afterTextChanged(Editable s) {
                String inputPhone = registration_phone_no.getText().toString();

                if (inputPhone.length() == 1) {
                    if (inputPhone.equals("0") || inputPhone.equals("7") || inputPhone.equals("+")) {
                        registration_phone_no.setText(result);
                        registration_phone_no.setSelection(registration_phone_no.getText().length());
                        Toast.makeText(getContext(), "Correct format auto completed. Please proceed!", Toast.LENGTH_SHORT).show();
                    } else {
                        registration_phone_no.setText(null);
                        Toast.makeText(getContext(), "Please enter correct phone number format!", Toast.LENGTH_SHORT).show();
                    }
                }

            }
        };
        registration_phone_no.addTextChangedListener(textWatcher1);

        radiogroupemploymentdetails.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (checkedId == R.id.rbemployed) {
                    //linearemployment.setVisibility(View.VISIBLE);
                    tilemploymentnumber.setVisibility(View.VISIBLE);
                    txtinputemployercode.setVisibility(View.VISIBLE);
                } else if (checkedId == R.id.rbnotemployed) {
                    //linearemployment.setVisibility(View.GONE);
                    tilemploymentnumber .setVisibility(View.GONE);
                    txtinputemployercode .setVisibility(View.GONE);
                    SharedPrefs.employer("NODETAILS",getActivity());
                    SharedPrefs.employernumber("00", getActivity());
                }
            }
        });

        return v;
    }

    @Override
    public void onNextClicked(final StepperLayout.OnNextClickedCallback callback) {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                String name = registration_name.getText().toString().trim();
                String id = registration_id.getText().toString().trim();
                String phone_number = registration_phone_no.getText().toString().trim();
                String amount = registration_amount.getText().toString().trim();
                String number = edtemploymentnumber.getText().toString().trim();
                String s_output="";
               try {
                   if (phone_number.length() == 13) {
                        s_output = phone_number.substring(4, 13);
                   }
               }catch (Exception e){

               }
//
                if (result.equals("+268")){
                    if (name.isEmpty()) {
                        Toast.makeText(getActivity(), "Enter the Full Account Name", Toast.LENGTH_LONG).show();
                    } else if (id.isEmpty()) {
                        Toast.makeText(getActivity(), "Enter the National ID", Toast.LENGTH_LONG).show();
                    } else if (citizen.getSelectedItem().toString().trim().equals("Select-citizenship.")
                            || citizen.getSelectedItem().toString().trim().equals(" ")) {
                        Toast.makeText(getActivity(), "Please select Citizenship", Toast.LENGTH_LONG).show();
                    } else if (spinner.getSelectedItem().toString().trim().equals("SELECT GENDER")) {
                        Toast.makeText(getActivity(), "Please select Gender", Toast.LENGTH_LONG).show();
                    } else if (salutation.getSelectedItem().toString().trim().equals("SELECT TITLE")) {
                        Toast.makeText(getActivity(), "Please select Title", Toast.LENGTH_LONG).show();

                    } else if (date.getText().toString().trim().equals("Select Date")) {
                        Toast.makeText(getActivity(), "Please select date of birth", Toast.LENGTH_LONG).show();
                    }
                    else {
                        if (membername.equals("")) {
                            if (loading.getVisibility() == View.VISIBLE) {
                                Toast.makeText(getActivity(), "Please wait......", Toast.LENGTH_LONG).show();
                            } else {
                                SharedPrefs.amount(amount, getActivity());
                                SharedPrefs.employernumber(number, getActivity());
                                SharedPrefs.putname(name, id, s_output, getActivity());//write(SharedPrefs.VR_NAMES, registration_name.getText().toString());

                                callback.goToNextStep();
                            }
                        } else {
                            Toast.makeText(getActivity(), "Member already registered", Toast.LENGTH_SHORT).show();
                        }
                    }
                }else {
                    if (name.isEmpty()) {
                        Toast.makeText(getActivity(), "Enter the Full Account Name", Toast.LENGTH_LONG).show();
                    } else if (id.isEmpty()) {
                        Toast.makeText(getActivity(), "Enter the National ID", Toast.LENGTH_LONG).show();
                    } else if (citizen.getSelectedItem().toString().trim().equals("Select-citizenship.")
                            || citizen.getSelectedItem().toString().trim().equals(" ")) {
                        Toast.makeText(getActivity(), "Please select Citizenship", Toast.LENGTH_LONG).show();
                    } else if (spinner.getSelectedItem().toString().trim().equals("SELECT GENDER")) {
                        Toast.makeText(getActivity(), "Please select Gender", Toast.LENGTH_LONG).show();
                    } else if (salutation.getSelectedItem().toString().trim().equals("SELECT TITLE")) {
                        Toast.makeText(getActivity(), "Please select Title", Toast.LENGTH_LONG).show();

                    } else if (date.getText().toString().trim().equals("Select Date")) {
                        Toast.makeText(getActivity(), "Please select date of birth", Toast.LENGTH_LONG).show();
                    } else if (phone_number.isEmpty()) {
                        Toast.makeText(getActivity(), "Enter the Phone Number", Toast.LENGTH_LONG).show();
                    } else if (amount.isEmpty()) {
                        Toast.makeText(getActivity(), "Enter Registration Amount", Toast.LENGTH_LONG).show();
                    } else if (agee < 18) {
                        Toast.makeText(getActivity(), "The customer doesn't meet the required minimum age", Toast.LENGTH_LONG).show();
                    } else {

                        if (membername.equals("")) {
                            if (loading.getVisibility() == View.VISIBLE) {
                                Toast.makeText(getActivity(), "Please wait......", Toast.LENGTH_LONG).show();
                            } else {
                                SharedPrefs.amount(amount, getActivity());
                                SharedPrefs.employernumber(number, getActivity());
                                SharedPrefs.putname(name, id, s_output, getActivity());//write(SharedPrefs.VR_NAMES, registration_name.getText().toString());

                                callback.goToNextStep();
                            }
                        } else {
                            Toast.makeText(getActivity(), "Member already registered", Toast.LENGTH_SHORT).show();
                        }
                    }
                }

            }
        }, 0L);// delay open another fragment,
    }
    @Override
    public void onCompleteClicked(StepperLayout.OnCompleteClickedCallback callback) {
    }
    @Override
    public void onBackClicked(final StepperLayout.OnBackClickedCallback callback) {

    }
    @Override
    public VerificationError verifyStep() {
        return null;
    }
    @Override
    public void onSelected() {
    }
    @Override
    public void onError(@NonNull VerificationError error) {
    }


    public void requestNationaliy(){

        String URL = Api.MSACCO_AGENT + Api.GetSaccoBranch;

        final SacooBranchRequest saccoBranches = new SacooBranchRequest();
        saccoBranches.agentid = sharedPref.getString(PreferenceFileKeys.AGENT_ID, "");
        saccoBranches.corporateno = MainDashboardActivity.corporateno;
        saccoBranches.code = "3";
        saccoBranches.terminalid = MainDashboardActivity.imei;
        saccoBranches.longitude = getLong(getContext());
        saccoBranches.latitude = getLat(getContext());

        Api.instance(getActivity()).request(URL, saccoBranches, new Api.RequestListener() {
            @Override
            public void onSuccess(String response) {
                SaccoBranchesResponse branchesResponse = Api.instance(getContext())
                        .mGson.fromJson(response, SaccoBranchesResponse.class);
                if (branchesResponse.is_successful) {
                    nationalityList.clear();
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        JSONArray jsonArray1 = jsonObject.getJSONArray("saccobarch");

                        for (int l = 0; l < jsonArray1.length(); l++) {
                            JSONObject jsonObject2 = jsonArray1.getJSONObject(l);

                            String branch = jsonObject2.getString("branchname");
                            String branchcode = jsonObject2.getString("branchcode");
                            nationalityList.add(branchcode+".  "+branch);

                        }

                        if(citizen.getCount()==0) {
                            Toast.makeText(getActivity(), "No nationality! Contact administrator to add.", Toast.LENGTH_LONG).show();
                        }else {
                            try {
                                ArrayAdapter<String> adapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_spinner_item, nationalityList);
                                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                citizen.setAdapter(adapter);
                            }catch (Exception e){
                                e.printStackTrace();
                            }
                        }


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                } else {
                    Toast.makeText(getActivity(), "Failed to fetch Nationalities", Toast.LENGTH_LONG).show();
                }

            }

            @Override
            public void onTokenExpired() {

            }

            @Override
            public void onError(String error) {

            }
        });
    }

    private void requestEmployers(){
        String URL = Api.MSACCO_AGENT + Api.GetSaccoBranch;
        final SacooBranchRequest saccoBranches = new SacooBranchRequest();
        saccoBranches.agentid = sharedPref.getString(PreferenceFileKeys.AGENT_ID, "");
        saccoBranches.corporateno = MainDashboardActivity.corporateno;
        saccoBranches.code = "4";
        saccoBranches.terminalid = MainDashboardActivity.imei;
        saccoBranches.longitude = getLong(getContext());
        saccoBranches.latitude = getLat(getContext());

        Api.instance(getActivity()).request(URL, saccoBranches, new Api.RequestListener() {
            @Override
            public void onSuccess(String response) {
                SaccoBranchesResponse branchesResponse = Api.instance(getContext())
                        .mGson.fromJson(response, SaccoBranchesResponse.class);
                if (branchesResponse.is_successful) {
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        JSONArray jsonArray1 = jsonObject.getJSONArray("saccobarch");
                        for (int l = 0; l < jsonArray1.length(); l++) {
                            JSONObject jsonObject2 = jsonArray1.getJSONObject(l);
                            String branch = jsonObject2.getString("branchname");
                            String branchcode = jsonObject2.getString("branchcode");
                            employersList.add(branchcode+".  "+branch);
                        }
                        spinnerDialog=new SpinnerDialog(getActivity(),employersList,"Select or Search Employer",R.style.DialogAnimations_SmileWindow,"Close Button Text");// With 	Animation
                        spinnerDialog.bindOnSpinerListener(new OnSpinerItemClick() {
                            @Override
                            public void onClick(String item, int position) {
                                employercode.setText(item);
                                String result = item.substring(0, item.indexOf("."));
                                SharedPrefs.employer(result,getActivity());
                            }
                        });

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                } else {
                    Toast.makeText(getActivity(), "Failed to fetch employers", Toast.LENGTH_LONG).show();
                }

            }

            @Override
            public void onTokenExpired() {

            }

            @Override
            public void onError(String error) {

            }
        });
    }

    void requestMemberDetails(String id) {
        RequestMemberName requestMemberName = new RequestMemberName();
        requestMemberName.corporateno = MainDashboardActivity.corporateno;
        requestMemberName.accountidentifier = id;
        requestMemberName.accountcode = "0";
        requestMemberName.agentid = SharedPrefs.read(SharedPrefs.AGENT_ID_1, null);
        requestMemberName.terminalid = MainDashboardActivity.imei;

        Api.instance(getActivity()).request(Api.MSACCO_AGENT + Api.GetMembername, requestMemberName, new Api.RequestListener() {
            @Override
            public void onSuccess(String response) {

                MemberNameResponse memberNameResponse = Api.instance(getActivity()).mGson.fromJson(response, MemberNameResponse.class);
                if (memberNameResponse.is_successful) {
                    loading.setVisibility(View.GONE);
                    membername=memberNameResponse.getCustomer_name();
                    validatephoneno=memberNameResponse.getPhoneno();
                } else {
                    Toast.makeText(getActivity(), "Failed", Toast.LENGTH_SHORT).show();
                }

            }

            @Override
            public void onTokenExpired() {
            }

            @Override
            public void onError(String error) {
                Toast.makeText(getActivity(), "Failed to load", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        requestNationaliy();
        employersList.clear();
        employersList.add("Select-Employer.");
        requestEmployers();
    }

}
