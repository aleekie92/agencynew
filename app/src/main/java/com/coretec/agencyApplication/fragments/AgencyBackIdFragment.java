package com.coretec.agencyApplication.fragments;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.coretec.agencyApplication.R;
import com.coretec.agencyApplication.utils.SharedPrefs;
import com.stepstone.stepper.BlockingStep;
import com.stepstone.stepper.StepperLayout;
import com.stepstone.stepper.VerificationError;

import java.io.ByteArrayOutputStream;

public class AgencyBackIdFragment extends Fragment implements BlockingStep {

    private CardView card_identity_photo;
    private static final int CAMERA_REQUEST = 1888;
    private ImageView identity_imageView;
    private Button submitIdentityPhoto;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.gfl_fragment_id_back, container, false);
        SharedPrefs.init(getContext());

        //initialize your UI
        identity_imageView = v.findViewById(R.id.imageview_identity);

        /*Bitmap imgg = ((BitmapDrawable)  identity_imageView.getDrawable()).getBitmap();
        String loggg = bitmapToBase64(imgg);
        Log.d("frontid","frontid------>"+loggg);*/

        card_identity_photo = v.findViewById(R.id.card_identity_photo);
        card_identity_photo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(cameraIntent, CAMERA_REQUEST);
            }
        });

        return v;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case CAMERA_REQUEST:
                if (resultCode == Activity.RESULT_OK) {
                    try {
                        Bitmap photo = (Bitmap) data.getExtras().get("data");
                        identity_imageView.setImageBitmap(photo);

                        //imageView.setImageBitmap(bitmap);
                        /*Toast.makeText(this, imageview_reason.toString(),
                                Toast.LENGTH_SHORT).show();*/
                    } catch (Exception e) {
                        Toast.makeText(getContext(), "Failed to load", Toast.LENGTH_SHORT)
                                .show();
                        Log.e("Camera", e.toString());
                    }
                }
        }
    }

    private String bitmapToBase64(Bitmap bitmap) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
        byte[] byteArray = byteArrayOutputStream.toByteArray();
        return Base64.encodeToString(byteArray, Base64.DEFAULT);
    }

    @Override
    public void onNextClicked(final StepperLayout.OnNextClickedCallback callback) {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                //String defaultBack = "iVBORw0KGgoAAAANSUhEUgAAAPwAAACZCAIAAACE8MDZAAAAA3NCSVQICAjb4U/gAAAgAElEQVR4";
                String defaultBack = "iVBORw0KGgoAAAANSUhEUgAAAPwAAACbCAIAAADJOGHSAAAAA3NCSVQICAjb4U/gAAAgAElEQVR4";
                Bitmap img = ((BitmapDrawable) identity_imageView.getDrawable()).getBitmap();
                String encodedImage = bitmapToBase64(img);

                String testImage = encodedImage.substring(0,76);

                if (testImage.equals(defaultBack)) {
                    Toast.makeText(getContext(), "Please take back id picture to proceed!", Toast.LENGTH_SHORT).show();
                } else {
                    SharedPrefs.write(SharedPrefs.VR_BACK_ID, encodedImage);
                    //you can do anythings you want
                    callback.goToNextStep();
                }
               // callback.goToNextStep();

            }
        }, 0L);// delay open another fragment,
    }
    @Override
    public void onCompleteClicked(StepperLayout.OnCompleteClickedCallback callback) {
    }
    @Override
    public void onBackClicked(final StepperLayout.OnBackClickedCallback callback) {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                //you can do anythings you want
                /*callback.goToNextStep();*/
                callback.goToPrevStep();
            }
        }, 0L);// delay open another fragment,
    }
    @Override
    public VerificationError verifyStep() {
        return null;
    }
    @Override
    public void onSelected() {
    }
    @Override
    public void onError(@NonNull VerificationError error) {
    }

}
