package com.coretec.agencyApplication.fragments;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.Toast;

import com.cloudpos.jniinterface.PrinterInterface;
import com.coretec.agencyApplication.R;
import com.coretec.agencyApplication.activities.CashWithdrawal;
import com.coretec.agencyApplication.activities.gfl.MainGFLDashboard;
import com.coretec.agencyApplication.activities.newmodules.MainDashboardActivity;
import com.coretec.agencyApplication.dialog.BankDialog;
import com.coretec.agencyApplication.dialog.SaccoDialog;
import com.coretec.agencyApplication.utils.SharedPrefs;
import com.coretec.agencyApplication.wizarpos.function.printer.PrinterCommand;
import com.stepstone.stepper.BlockingStep;
import com.stepstone.stepper.StepperLayout;
import com.stepstone.stepper.VerificationError;

import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DetailsFragment extends Fragment implements BlockingStep {

    LinearLayout bankAtm;
    LinearLayout layout_languages;
    private static final int CAMERA_REQUEST = 1888;
    private ImageView identity_imageView;
    private EditText fullNames, kin_id;
    private Spinner spinner;
    private Spinner lang_spinner;
    private RadioGroup radioGroup, radioGroupAccount;
    private RadioButton yesBtn, noBtn, rbBank, rbSacco,rbMpesa;
    boolean puRegistered = false;
    AlertDialog bankDialog, saccoDialog;
    /*private TextView bankName;
    ProgressBar progressBar;*/

    String idOfKin;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.gfl_fragment_details, container, false);
        SharedPrefs.init(getContext());

        //initialize your UI
        fullNames = v.findViewById(R.id.full_names);
        kin_id = v.findViewById(R.id.et_kin_id);
        kin_id.setInputType(InputType.TYPE_NULL);
        kin_id.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // showMyDialog();
            }
        });

        kin_id.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    // showMyDialog();
                }
            }
        });
        layout_languages = v.findViewById(R.id.linearLanguages);
        lang_spinner = v.findViewById(R.id.langSpinner);
        radioGroup = v.findViewById(R.id.radioGroup);
        radioGroupAccount = v.findViewById(R.id.radioGroupAccount);
        yesBtn = v.findViewById(R.id.rb1);
        noBtn = v.findViewById(R.id.rb2);
        rbBank = v.findViewById(R.id.rbBank);
        rbSacco = v.findViewById(R.id.rbSacco);

        rbMpesa = v.findViewById(R.id.rbMpesa);

        spinner = v.findViewById(R.id.relSpinner);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String item = parent.getItemAtPosition(position).toString();
                String text = spinner.getSelectedItem().toString();

                if (text.equals("relationship")) {
                    SharedPrefs.write(SharedPrefs.REG_RELATIONSHIP, "null");
                } else {
                    SharedPrefs.write(SharedPrefs.REG_RELATIONSHIP, text);
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        lang_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String item = parent.getItemAtPosition(position).toString();
                String text = lang_spinner.getSelectedItem().toString();

                if (!text.equals("select-language")) {
                    String code = text.substring(0, text.indexOf("-"));
                    SharedPrefs.write(SharedPrefs.REG_LANG_SPINNER, code);
                } else {
                    SharedPrefs.write(SharedPrefs.REG_LANG_SPINNER, "null");
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (checkedId == R.id.rb1) {
                    SharedPrefs.write(String.valueOf(SharedPrefs.PU_REGISTERED), true);
                    layout_languages.setVisibility(View.VISIBLE);
                } else if (checkedId == R.id.rb2) {
                    SharedPrefs.write(String.valueOf(SharedPrefs.PU_REGISTERED), "");
                    layout_languages.setVisibility(View.GONE);
                }
            }
        });

        rbBank.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (rbBank.isChecked()) {
                    startActivity(new Intent(getContext(), BankDialog.class));
                }
            }
        });

        rbSacco.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (rbSacco.isChecked()) {
                    //createDialogSacco();
                    startActivity(new Intent(getContext(), SaccoDialog.class));
                }
            }
        });


        rbMpesa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (rbMpesa.isChecked()) {
                    String defaultBank = "iVBORw0KGgoAAAANSUhEUgAAAjcAAAFlCAIAAACY5F6YAAAAA3NCSVQICAjb4U/gAAAgAElEQVR4";

                    String defaultSacco = "iVBORw0KGgoAAAANSUhEUgAAASwAAADCCAIAAAB8JmhkAAAAA3NCSVQICAjb4U/gAAAgAElEQVR4";
                    SharedPrefs.write(SharedPrefs.REG_SACOO_CARD, defaultSacco);
                    SharedPrefs.write(SharedPrefs.REG_SACCO_AC_NO,  "");
                    SharedPrefs.write(SharedPrefs.REG_SACCO_CODE, "");

                    SharedPrefs.write(SharedPrefs.REG_ATM_CARD, defaultBank);
                    SharedPrefs.write(SharedPrefs.REG_BANK_DETAILS, "");
                    SharedPrefs.write(SharedPrefs.REG_ACCOUNT_NO, "");
                }
            }
        });

        return v;
    }


    @Override
    public void onNextClicked(final StepperLayout.OnNextClickedCallback callback) {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                idOfKin = kin_id.getText().toString();
                //Bank details strings
                String bnkDetails = SharedPrefs.read(SharedPrefs.REG_BANK_DETAILS, null);

                //Sacco details strings
                String saccOde = SharedPrefs.read(SharedPrefs.REG_SACCO_CODE, null);

                if (fullNames.getText().toString().matches("")) {
                    Toast.makeText(getContext(), "Please fill in the next of kin!", Toast.LENGTH_SHORT).show();
                }
                else if (SharedPrefs.read(SharedPrefs.REG_RELATIONSHIP, null).matches("null")) {
                    Toast.makeText(getContext(), "Please select next of kin relationship!", Toast.LENGTH_SHORT).show();
                } else if (!rbBank.isChecked() && !rbSacco.isChecked() && !rbMpesa.isChecked()) {
                    Toast.makeText(getContext(), "Please fill in account details!", Toast.LENGTH_SHORT).show();
                } else if (rbBank.isChecked() && bnkDetails.equals("null")) {
                    Toast.makeText(getContext(), "Please fill your bank details!", Toast.LENGTH_SHORT).show();
                } else if (rbSacco.isChecked() && saccOde.equals("null")) {
                    Toast.makeText(getContext(), "Please fill your sacco details!", Toast.LENGTH_SHORT).show();
                } else {
                    if (!idOfKin.isEmpty() && idOfKin.length() > 7) {
                        //you can do anythings you want
                        SharedPrefs.write(SharedPrefs.REG_FULL_NAME, fullNames.getText().toString());
                        SharedPrefs.kinidnumber(idOfKin,getActivity());
                        String url="http://gfedha.com/index.php/agency-banking-services";
                        String msg = "To proceed, please accept our terms and conditions."+"\n"+url;

                        android.app.AlertDialog.Builder builder1 = new android.app.AlertDialog.Builder(getContext());
                        builder1.setMessage(msg);
                        builder1.setTitle("Alert!");
                        builder1.setCancelable(false);


                        builder1.setPositiveButton(
                                "Accept",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();
                                        int result = PrinterInterface.open();
                                        writetest();
                                        PrinterInterface.close();
                                        callback.goToNextStep();
                                    }
                                });
                    builder1.setNegativeButton(
                            "Reject",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    Intent i = new Intent(getActivity(), MainGFLDashboard.class);
                                    i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                                    i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                    startActivity(i);
                                }
                            });

                        android.app.AlertDialog alert11 = builder1.create();
                        alert11.show();

                    } else if (idOfKin.length() < 7) {
                        Toast.makeText(getContext(), "Please enter a valid ID number", Toast.LENGTH_SHORT).show();
                    } else {
                        SharedPrefs.write(SharedPrefs.REG_FULL_NAME, fullNames.getText().toString());
                        SharedPrefs.kinidnumber(idOfKin,getActivity());
                        String url="http://gfedha.com/index.php/agency-banking-services";
                        String msg = "To proceed, please accept our terms and conditions."+"\n"+url;
                        android.app.AlertDialog.Builder builder1 = new android.app.AlertDialog.Builder(getContext());
                        builder1.setMessage(msg);
                        builder1.setTitle("Alert!");
                        builder1.setCancelable(false);

                        builder1.setPositiveButton(
                                "Accept",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();
                                        int result = PrinterInterface.open();
                                        writetest();
                                        PrinterInterface.close();
                                        SharedPrefs.write(SharedPrefs.REG_FULL_NAME, fullNames.getText().toString());
                                        SharedPrefs.kinidnumber(idOfKin,getActivity());
                                        callback.goToNextStep();
                                    }
                                });

                        builder1.setNegativeButton(
                                "Reject",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        Intent i = new Intent(getActivity(), MainGFLDashboard.class);
                                        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                                        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                        startActivity(i);
                                    }
                                });

                        android.app.AlertDialog alert11 = builder1.create();
                        alert11.show();
                    }

                }
            }
        }, 0L);
    }

    @Override
    public void onCompleteClicked(StepperLayout.OnCompleteClickedCallback callback) {

    }

    @Override
    public void onBackClicked(final StepperLayout.OnBackClickedCallback callback) {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                //you can do anythings you want
                /*callback.goToNextStep();*/
                callback.goToPrevStep();
            }
        }, 0L);// delay open another fragment,
    }

    @Override
    public VerificationError verifyStep() {
        return null;
    }

    @Override
    public void onSelected() {
    }

    @Override
    public void onError(@NonNull VerificationError error) {
    }

    public void writetest() {
        try {
           // byte[] start = null;
            byte[] saccoName = null;
            byte[] type = null;
            byte[] gno = null;
            byte[] accName = null;
            byte[] idNumber = null;
            byte[] phoneNumber = null;
            byte[] nok = null;
            byte[] relationship = null;

            byte[] bankDetails = null;
            byte[] accNumber = null;

            byte[] puReg = null;
            byte[] puLang = null;

            byte[] saccoCode = null;
            byte[] saccoAccNo = null;

            byte[] time = null;
            byte[] nameAgent = null;
            byte[] terms = null;
            //byte[] motto = null;

            SimpleDateFormat timeStampFormat = new SimpleDateFormat("yyyy/MM/dd - HH:mm");
            Date myDate = new Date();
            String timeCurrent = timeStampFormat.format(myDate);
            String aName = SharedPrefs.read(SharedPrefs.AGENT_ID_1, null);
            boolean puRegistered = SharedPrefs.read("PU_REGISTERED", SharedPrefs.PU_REGISTERED);
            String saccode="";
            String sacco_acc="";
            String bank_details="";
            String bankAcc="";

            if(rbMpesa.isChecked()){
                saccode="None";
                sacco_acc="None";
                bank_details="None";
                bankAcc="None";
            }
            if(rbSacco.isChecked()){
                bank_details="None";
                bankAcc="None";
                saccode = SharedPrefs.read(SharedPrefs.REG_SACCO_CODE, null);
                sacco_acc = SharedPrefs.read(SharedPrefs.REG_SACCO_AC_NO, null);
            }
            if(rbBank.isChecked()){
                saccode="None";
                sacco_acc="None";
                bank_details = SharedPrefs.read(SharedPrefs.REG_BANK_DETAILS, null);
                bankAcc = SharedPrefs.read(SharedPrefs.REG_ACCOUNT_NO, null);
            }

            try {
                //start = String.valueOf("              ").getBytes("GB2312");
                saccoName = String.valueOf("      GREENLAND FEDHA LTD     ").getBytes("GB2312");
                type = String.valueOf("     REGISTRATION RECEIPT     ").getBytes("GB2312");
                gno = String.valueOf("Grower Number : " + SharedPrefs.read(SharedPrefs.REG_GROWER_NO, null)).getBytes("GB2312");
                accName = String.valueOf("Account Name : " + SharedPrefs.read(SharedPrefs.REG_ACC_NAME, null)).getBytes("GB2312");
                idNumber = String.valueOf("ID Number : " + SharedPrefs.read(SharedPrefs.REG_IDNUMBER, null)).getBytes("GB2312");
                phoneNumber = String.valueOf("Phone Number : " + SharedPrefs.read(SharedPrefs.REG_PHONE, null)).getBytes("GB2312");
                nok = String.valueOf("Next Of Kin : " + SharedPrefs.read(SharedPrefs.REG_FULL_NAME, null)).getBytes("GB2312");
                relationship = String.valueOf("Relationship : " + SharedPrefs.read(SharedPrefs.REG_RELATIONSHIP, null)).getBytes("GB2312");

                puReg = String.valueOf("PU Registered : " + puRegistered).getBytes("GB2312");
                puLang = String.valueOf("PU Language : " + SharedPrefs.read(SharedPrefs.REG_LANG_SPINNER, null)).getBytes("GB2312");

                bankDetails = String.valueOf("Bank & Branch Code : " + bank_details).getBytes("GB2312");
                accNumber = String.valueOf("Account No : " + bankAcc).getBytes("GB2312");

                saccoCode = String.valueOf("Sacco Code : " + saccode).getBytes("GB2312");
                saccoAccNo = String.valueOf("Sacco A/c No : " + sacco_acc).getBytes("GB2312");

                time = String.valueOf("   Date : " + String.valueOf(timeCurrent)).getBytes("GB2312");
                nameAgent = String.valueOf("You were served by " + String.valueOf(aName)).getBytes("GB2312");
                terms = String.valueOf("Terms and Conditions: " +"\n"+ String.valueOf("http://gfedha.com/index.php/agency-banking-services")).getBytes("GB2312");
                //motto = String.valueOf("     -    GFL Motto       ").getBytes("GB2312");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            PrinterInterface.begin();
            byte[] cmd;
            cmd = new byte[]{(byte) 0x1B, (byte) 0x40};

            cmd = new byte[]{
                    (byte) 0x1B, (byte) 0x45, (byte) 0};

            write(cmd);
            writeLineBreak(1);
          /*  write(start);
            writeLineBreak(1);*/
            write(saccoName);
            writeLineBreak(1);
            write(type);
            writeLineBreak(1);
            write(gno);
            writeLineBreak(1);
            write(accName);
            writeLineBreak(1);
            write(idNumber);
            writeLineBreak(1);
            write(phoneNumber);
            writeLineBreak(1);
            write(nok);
            writeLineBreak(1);
            write(relationship);
            writeLineBreak(1);

            if (puRegistered) {
                write(puReg);
                writeLineBreak(1);
                write(puLang);
                writeLineBreak(1);
            }

            if (!bank_details.equals("null")) {
                write(bankDetails);
                writeLineBreak(1);
                write(accNumber);
                writeLineBreak(1);
            }

            if (!saccode.equals("null")) {
                write(saccoCode);
                writeLineBreak(1);
                write(saccoAccNo);
                writeLineBreak(2);
            }

            write(time);
            writeLineBreak(2);
            write(nameAgent);
            writeLineBreak(2);
            write(terms);
            writeLineBreak(4);
            writeLineBreak(5);
            PrinterInterface.end();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void write(final byte[] arryData) {
        int result = PrinterInterface.write(arryData, arryData.length);
    }

    // print line break
    private void writeLineBreak(int lineNumber) {
        // write(new byte[]{'\n'});
        write(PrinterCommand.getCmdEscDN(lineNumber));
    }

}
