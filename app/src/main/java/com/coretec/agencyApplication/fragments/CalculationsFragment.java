package com.coretec.agencyApplication.fragments;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.cloudpos.DeviceException;
import com.cloudpos.POSTerminal;
import com.cloudpos.fingerprint.FingerprintDevice;
import com.cloudpos.jniinterface.PrinterInterface;
import com.coretec.agencyApplication.R;
import com.coretec.agencyApplication.activities.LoginActivity;
import com.coretec.agencyApplication.activities.gfl.MainGFLDashboard;
import com.coretec.agencyApplication.api.GFL;
import com.coretec.agencyApplication.api.requests.GetNetAmountRequest;
import com.coretec.agencyApplication.api.requests.GetQualifiedKgsRequest;
import com.coretec.agencyApplication.api.requests.GrowersDetailsRequest;
import com.coretec.agencyApplication.api.requests.GuarantorRegistration_v2;
import com.coretec.agencyApplication.api.responses.GetNetAmountResponse;
import com.coretec.agencyApplication.api.responses.GetQualifiedKgsResponse;
import com.coretec.agencyApplication.api.responses.GrowersDetailsResponse;
import com.coretec.agencyApplication.api.responses.GuarantorRegistrationResponse;
import com.coretec.agencyApplication.model.GrowerDetails;
import com.coretec.agencyApplication.model.LoanDetails;
import com.coretec.agencyApplication.utils.SharedPrefs;
import com.coretec.agencyApplication.utils.Utils;
import com.coretec.agencyApplication.wizarpos.function.printer.PrinterCommand;
import com.stepstone.stepper.BlockingStep;
import com.stepstone.stepper.StepperLayout;
import com.stepstone.stepper.VerificationError;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import static com.coretec.agencyApplication.utils.Utils.getFormattedDate;
import static com.coretec.agencyApplication.utils.Utils.getLat;
import static com.coretec.agencyApplication.utils.Utils.getLong;

public class CalculationsFragment extends Fragment implements BlockingStep {

    private TextView minimum, maximum;
    private EditText enter_amount;
    private TextView netAmount, finalAmount;
    ProgressBar progressBar, progressBar1;
    Button submit;
    private Spinner spinner;
    private ArrayList<String> arrayList = new ArrayList<>();
    public ArrayAdapter<LoanDetails> adapterLoan;
    private String item;

    ProgressDialog progressDialog, submitDialog;
    AlertDialog fingerpDialog;

    //Fingerprint declarations
    protected TextView log_text;
    protected Handler handler = null;
    protected Runnable runnable = null;
    String productCode;

    private SharedPreferences preferences;
    private SharedPreferences.Editor editor;
    private static final int SHOW_NORMAL_MESSAGE = 0;
    private static final int SHOW_SUCCESS_MESSAGE = 1;
    private static final int SHOW_FAIL_MESSAGE = 2;
    private static final int SHOW_BTN = 3;
    private static final int HIDE_BTN = 4;
    private FingerprintDevice fingerprintDevice;
    SharedPreferences sharedPreferences;
    //String bankExists,saccoExists;
    String bankExists, saccoExists, age;
    int loanlmt;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.gfl_fragment_calculations, container, false);


        SharedPrefs.init(getContext());
        //initialize your UI
        minimum = v.findViewById(R.id.tvMin);
        maximum = v.findViewById(R.id.tvMax);
        minimum.setText("xxxxx");
        maximum.setText("xxxxx");

        enter_amount = v.findViewById(R.id.amount);
        netAmount = v.findViewById(R.id.netAmount);
        finalAmount = v.findViewById(R.id.netTrans);
        progressBar = v.findViewById(R.id.amt1PB);
        progressBar1 = v.findViewById(R.id.amt2PB);

        enter_amount.setInputType(InputType.TYPE_NULL);
        enter_amount.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // showMyDialog();
            }
        });

        enter_amount.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    // showMyDialog();
                }
            }
        });
       /* try {
            fingerprintDevice = (FingerprintDevice) POSTerminal.getInstance(mContext).getDevice("cloudpos.device.fingerprint");
            fingerprintDevice.open(1);
            //delAllFingers();
        } catch (DeviceException e) {
            e.printStackTrace();
        }*/


        spinner = v.findViewById(R.id.disbursementMethod);
        submit = v.findViewById(R.id.btnSubmit);
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String eAmount = enter_amount.getText().toString();

                if (!eAmount.isEmpty()) {
                    String minimumAmount = SharedPrefs.read(SharedPrefs.LOAN_NEW_MINIMUM, null);
                    String maximumAmount = SharedPrefs.read(SharedPrefs.LOAN_NEW_MAXIMUM, null);
                    int amount = Integer.parseInt(enter_amount.getText().toString());
                    String min1 = minimumAmount.substring(0, minimumAmount.indexOf("."));
                    String max1 = maximumAmount.substring(0, maximumAmount.indexOf("."));

                    int minAmount = Integer.parseInt(min1);
                    int maxAmount = Integer.parseInt(max1);

                    if (amount >= minAmount && amount <= maxAmount) {
                        String msg = "Are you sure you want to submit this loan application?";
                        final String qualKgs = SharedPrefs.read(SharedPrefs.LOAN_QUALIFIED_KGS, null);
                        AlertDialog.Builder builder1 = new AlertDialog.Builder(getContext());
                        builder1.setMessage(msg + " Equivalent kgs is " + qualKgs);
                        builder1.setTitle("Confirm!");
                        builder1.setCancelable(false);

                        builder1.setPositiveButton(
                                "YES",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();

                                        SharedPrefs.write(SharedPrefs.LOAN_ENTER_AMT, enter_amount.getText().toString());
                                        progressDialog = new ProgressDialog(getContext());
                                        progressDialog.setMessage("Please wait...");
                                        progressDialog.setCancelable(false);
                                        progressDialog.show();
                                        progressDialog.dismiss();
                                        final String qualKgs = SharedPrefs.read(SharedPrefs.LOAN_QUALIFIED_KGS, null);

                                        submitLoanApp(qualKgs);

                                    }
                                });

                        builder1.setNegativeButton(
                                "Cancel",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();
                                    }
                                });

                        AlertDialog alert11 = builder1.create();
                        alert11.show();
                    } else {
                        String msg = "Please input amount that ranges between minimum and maximum to proceed!";
                        AlertDialog.Builder builder2 = new AlertDialog.Builder(getContext());
                        builder2.setMessage(msg);
                        builder2.setTitle("Error!");
                        builder2.setCancelable(false);

                        builder2.setPositiveButton(
                                "OK",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();

                                    }
                                });

                        AlertDialog alert12 = builder2.create();
                        alert12.show();
                    }
                } else {
                    Toast.makeText(getContext(), "Please input amount to proceed!", Toast.LENGTH_SHORT).show();
                }
            }
        });

        return v;
    }

    private Context mContext;

    @Override
    public void onNextClicked(final StepperLayout.OnNextClickedCallback callback) {

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                String eAmount = enter_amount.getText().toString();
                if (!eAmount.isEmpty()) {
                    productCode = SharedPrefs.read(SharedPrefs.LOAN_PT_CODE, null);
                    String minimumAmount1 = SharedPrefs.read(SharedPrefs.LOAN_NEW_MINIMUM, null);
                    String maximumAmount1 = SharedPrefs.read(SharedPrefs.LOAN_NEW_MAXIMUM, null);
                    int amount1 = Integer.parseInt(enter_amount.getText().toString());
                    String min11 = minimumAmount1.substring(0, minimumAmount1.indexOf("."));
                    String max11 = maximumAmount1.substring(0, maximumAmount1.indexOf("."));

                    int minAmount1 = Integer.parseInt(min11);
                    int maxAmount1 = Integer.parseInt(max11);

                    int growerage=Integer.parseInt(SharedPrefs.read(SharedPrefs.GROWERS_AGE, null));

                    if (!productCode.equals("BONUS ADV") && amount1 >= minAmount1 && amount1 <= maxAmount1) {
                        SharedPrefs.write(SharedPrefs.LOAN_ENTER_AMT, enter_amount.getText().toString());
                        loanlmt = Integer.parseInt(SharedPrefs.read(SharedPrefs.MPESA_LIMIT, null).replaceAll(",",""));
                        if (spinner.getSelectedItem().equals("Mpesa")) {
                            if (Integer.parseInt(netAmount.getText().toString()) > loanlmt) {
                                Toast.makeText(getContext(), "Mpesa can not allow more than "+ loanlmt+" transaction", Toast.LENGTH_SHORT).show();
                            } else {
                                callback.goToNextStep();
                            }
                        } else if (spinner.getSelectedItem().equals("EFT") || spinner.getSelectedItem().equals("Sacco")) {
                            if (growerage>80){
                                callback.goToNextStep();
                            }else{
                                if (Integer.parseInt(netAmount.getText().toString()) > loanlmt) {
                                    callback.goToNextStep();
                                } else {
                                    Toast.makeText(getContext(), "Select Mpesa, the amount is less than "+loanlmt, Toast.LENGTH_SHORT).show();
                                }
                            }

                        } else {
                            Toast.makeText(getContext(), "Select the disbursment mode correctly", Toast.LENGTH_SHORT).show();
                        }


                    } else if (productCode.equals("BONUS ADV")) {
                        Toast.makeText(getContext(), "This is Bonus advance, click 'SUBMIT' to coplete", Toast.LENGTH_SHORT).show();
                    } else {
                        String msg = "Please input amount that ranges between minimum and maximum to proceed!";
                        AlertDialog.Builder builder2 = new AlertDialog.Builder(getContext());
                        builder2.setMessage(msg);
                        builder2.setTitle("Error!");
                        builder2.setCancelable(false);

                        builder2.setPositiveButton(
                                "OK",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();
                                        //startActivity(new Intent(getContext(), Loans.class));
                                    }
                                });

                        AlertDialog alert12 = builder2.create();
                        alert12.show();
                    }
                } else {
                    Toast.makeText(getContext(), "Please input amount to proceed!", Toast.LENGTH_SHORT).show();
                }
            }
        }, 0L);
    }

    @Override
    public void onCompleteClicked(StepperLayout.OnCompleteClickedCallback callback) {
    }

    @Override
    public void onBackClicked(final StepperLayout.OnBackClickedCallback callback) {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                SharedPreferences bankexiste = getActivity().getSharedPreferences("BankExists", Context.MODE_PRIVATE);
                bankexiste.edit().clear().commit();
                enter_amount.setText(null);
                netAmount.setText(null);
                finalAmount.setText(null);
                callback.goToPrevStep();
            }
        }, 0L);// delay open another fragment,
    }

    @Override
    public VerificationError verifyStep() {
        return null;
    }

    @Override
    public void onSelected() {

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getContext());
        if (!prefs.getBoolean("first_time_calc", false)) {
            SharedPreferences.Editor editor = prefs.edit();
            editor.putBoolean("first_time_calc", true);
            editor.commit();
            if (enter_amount.getText().toString().isEmpty()) {
                SharedPrefs.write(SharedPrefs.LOAN_ENTER_AMT, "null");
            }
        } else {
            String productCode1 = SharedPrefs.read(SharedPrefs.LOAN_PT_CODE, null);
            String entered_amount = SharedPrefs.read(SharedPrefs.LOAN_ENTER_AMT, null);

            if (productCode1.equals("BONUS ADV") && !entered_amount.equals("null")) {
                enter_amount.setText(entered_amount);
                enter_amount.setSelection(enter_amount.getText().length());
            }
        }

        String productCode = SharedPrefs.read(SharedPrefs.LOAN_PT_CODE, null);
        minimum.setText(SharedPrefs.read(SharedPrefs.LOAN_MINIMUM, null));
        maximum.setText(SharedPrefs.read(SharedPrefs.LOAN_MAXIMUM, null));
        String min = SharedPrefs.read(SharedPrefs.LOAN_MINIMUM, null);
        String max = SharedPrefs.read(SharedPrefs.LOAN_MAXIMUM, null);
        String newMin = min.replaceAll(",", "");
        String newMax = max.replaceAll(",", "");
        SharedPrefs.write(SharedPrefs.LOAN_NEW_MINIMUM, newMin);
        SharedPrefs.write(SharedPrefs.LOAN_NEW_MAXIMUM, newMax);

        Log.e("NEW MINIMUM", newMin);
        Log.e("NEW MAXIMUM:", newMax);

        if (productCode.equals("BONUS ADV")) {
            submit.setVisibility(View.VISIBLE);
        } else if (!productCode.equals("BONUS ADV")) {
            submit.setVisibility(View.GONE);
        }


        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                item = parent.getItemAtPosition(position).toString();
                String text = spinner.getSelectedItem().toString();
                if (!text.equals("disbursement-method")) {
                    SharedPrefs.write(SharedPrefs.LOAN_SPINNER, text);
                    Log.e("METHOD", text);
                    progressBar1.setVisibility(View.VISIBLE);
                    if (text.equals("Mpesa")) {
                        String mpesaIdentifier = "3";
                        getNetTransactionAmount(mpesaIdentifier);
                    }
                    if (text.equals("Sacco")) {
                        String chequeIdentifier = "7";
                        getNetTransactionAmount(chequeIdentifier);
                    }
                    if (text.equals("EFT")) {
                        String eftIdentifier = "6";
                        getNetTransactionAmount(eftIdentifier);
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        TextWatcher textWatcherId = new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {

                String amount_identifier = enter_amount.getText().toString();

                if (amount_identifier.length() >= 3) {
                    List<String> spinnerArray = new ArrayList<>();
                    spinnerArray.add("Mpesa");
                    spinnerArray.add("Cheque");
                    spinnerArray.add("EFT");

                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(
                            getContext(),
                            android.R.layout.simple_spinner_item,
                            spinnerArray
                    );
                    progressBar.setVisibility(View.VISIBLE);
                    getNetAmount(amount_identifier);
                    getQualifiedKgs(amount_identifier);
                }
            }
        };

        enter_amount.addTextChangedListener(textWatcherId);
    }

    @Override
    public void onError(@NonNull VerificationError error) {
    }

    void submitLoanApp(String QualifiedKgs) {
        final String TAG = "LOAN APPLICATION";

        String URL = GFL.MSACCO_AGENT + GFL.GuarantorRegistration;
        String growerNumBer = SharedPrefs.read(SharedPrefs.LOAN_APP_GROWER, null);
        String kodi = SharedPrefs.read(SharedPrefs.LOAN_PT_CODE, null);
        String idNumber = SharedPrefs.read(SharedPrefs.LOAN_IDENTITY_NO, null);
        String growerName = SharedPrefs.read(SharedPrefs.LOAN_APP_G_NAME, null);
        String spinnerItem = SharedPrefs.read(SharedPrefs.LOAN_SPINNER, null);

        final GuarantorRegistration_v2 grRequest = new GuarantorRegistration_v2();
        grRequest.setApplidno(idNumber);
        grRequest.setApplgrowerno(growerNumBer);
        grRequest.setApplreqkgs(SharedPrefs.read(SharedPrefs.LOAN_QUALIFIED_KGS, null));
        grRequest.setApplptype(SharedPrefs.read(SharedPrefs.LOAN_PT_CODE, null));
        grRequest.setApplamount(enter_amount.getText().toString());
        grRequest.setDisbursmentmethod(SharedPrefs.read(SharedPrefs.LOAN_SPINNER, null));
        grRequest.setApprovalstatus("5");
        grRequest.setGuarantorlist(null);
        grRequest.setTransactiontype("1");
        grRequest.setOrigDocumentNo(null);
        grRequest.setAgentid(SharedPrefs.read(SharedPrefs.AGENT_ID_1, null));
        grRequest.setTerminalid(SharedPrefs.read(SharedPrefs.DEVICE_ID, null));
        grRequest.setLongitude(getLong(getContext()));
        grRequest.setLatitude(getLat(getContext()));
        grRequest.setDate(getFormattedDate());
        grRequest.setCorporate_no("CAP016");

        Log.e(TAG, grRequest.getBody().toString());

        GFL.instance(getContext()).request(URL, grRequest, new GFL.RequestListener() {
            @Override
            public void onSuccess(String response) {
                /*deposit_progressBar.setVisibility(View.GONE);*/

                GuarantorRegistrationResponse grResponse = GFL.instance(getContext())
                        .mGson.fromJson(response, GuarantorRegistrationResponse.class);
                if (grResponse.is_successful) {
                    //submitDialog.dismiss();
                    try {
                        JSONObject jsonObject = new JSONObject(response);

                        String agent = SharedPrefs.read(SharedPrefs.AGENT_ID_1, null);//sharedPreferences.getString(PreferenceFileKeys.AGENT_ACCOUNT_NAME, "");
                        String growerno = grRequest.getApplgrowerno();
                        String qualifiedkgs = grRequest.getApplreqkgs();//SharedPrefs.read(SharedPrefs.LOAN_QUALIFIED_KGS, null);
                        String loancode = grRequest.getApplptype();//SharedPrefs.read(SharedPrefs.LOAN_PT_CODE, null);
                        String appliedamt = grRequest.getApplamount();//SharedPrefs.read(SharedPrefs.LOAN_ENTER_AMT, null);
                        String disbursmentmethod = grRequest.getDisbursmentmethod();//SharedPrefs.read(SharedPrefs.LOAN_SPINNER, null);
                        String saccoName = "GreenLand Fedha";

                        PrinterInterface.open();

                        writetest(agent, growerno, qualifiedkgs, loancode, appliedamt,
                                disbursmentmethod, saccoName, grResponse.getReceiptNo(),
                                grResponse.getTransactiondate(), "Loan Application");
                        PrinterInterface.close();

                        String receiptNo = jsonObject.getString("receiptNo");
                        Toast.makeText(getContext(), receiptNo, Toast.LENGTH_SHORT).show();
                        String msg = "Loan application submitted successfully!";
                        AlertDialog.Builder builder1 = new AlertDialog.Builder(getContext());
                        builder1.setMessage(msg);
                        builder1.setTitle("Success!");
                        builder1.setCancelable(false);

                        builder1.setPositiveButton(
                                "DONE",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();
                                        SharedPrefs.write(SharedPrefs.LOAN_IDENTITY_NO, "null");
                                        SharedPrefs.write(SharedPrefs.LOAN_ENTER_AMT, "null");
                                        getActivity().finish();
                                        Intent i = new Intent(getActivity(), MainGFLDashboard.class);
                                        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                                        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                        startActivity(i);
                                    }
                                });

                        AlertDialog alert11 = builder1.create();
                        alert11.show();
                        Log.e("SUCCESSFUL", receiptNo);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    //submitDialog.dismiss();
                    Toast.makeText(getContext(), "Loan Application failed. Try again later!", Toast.LENGTH_LONG).show();
                }

            }

            @Override
            public void onTokenExpired() {

            }

            @Override
            public void onError(String error) {

            }
        });
    }

    public void writetest(String agentName, String growerno, String qualifiedkgs, String loancode,
                          String appliedamt, String disbursmentmethod, String saccoame,
                          String receiptNumber, String dateed, String transactionType) {
        try {

            byte[] arrySaccoName = null;

            byte[] arrygrowerno = null;
            byte[] arryqualifiedkgs = null;
            byte[] arryloancode = null;
            byte[] aarryppliedamt = null;
            byte[] arrydisbursmentmethod = null;

            byte[] arryWithdrawAmt = null;
            byte[] arryAmt = null;

            // byte[] arryMotto = null;
            byte[] terminalNo = null;
            // byte[] receiptNo = null;
            byte[] date = null;
            byte[] transactionTYpe = null;
            byte[] servedBy = null;
            byte[] signature = null;


            try {
                arrySaccoName = String.valueOf("       " + String.valueOf(saccoame)).getBytes("GB2312");
                terminalNo = String.valueOf("Terminal ID :" + Utils.getIMEI(getContext())).getBytes("GB2312");
                // receiptNo = String.valueOf(" Copy Receipt Number  : " + receiptNumber).getBytes("GB2312");
                date = String.valueOf("Date : " + dateed).getBytes("GB2312");
                transactionTYpe = String.valueOf("      " + transactionType).getBytes("GB2312");

                arrygrowerno = String.valueOf("Grower Number  :" + growerno).getBytes("GB2312");
                arryloancode = String.valueOf(String.valueOf("Loan Product   :" + loancode)).getBytes("GB2312");
                aarryppliedamt = String.valueOf(String.valueOf("Applied Amount  :" + appliedamt)).getBytes("GB2312");
                arryqualifiedkgs = String.valueOf(String.valueOf("Qualified Kgs " + qualifiedkgs)).getBytes("GB2312");
                arrydisbursmentmethod = String.valueOf(String.valueOf("Disbursement Mode   :" + disbursmentmethod)).getBytes("GB2312");

                servedBy = String.valueOf(" You were Served By " + agentName).getBytes("GB2312");
                signature = String.valueOf("Signature____________________").getBytes("GB2312");

            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            PrinterInterface.begin();

            byte[] cmd;
            cmd = new byte[]{(byte) 0x1B, (byte) 0x40};

            cmd = new byte[]{
                    (byte) 0x1B, (byte) 0x45, (byte) 0};

            write(cmd);
            writeLineBreak(3);
            write(arrySaccoName);
            writeLineBreak(1);
            write(terminalNo);
            writeLineBreak(1);
            write(date);
            writeLineBreak(1);
            write(transactionTYpe);
            writeLineBreak(3);

            write(arrygrowerno);
            writeLineBreak(1);
            write(arryqualifiedkgs);
            writeLineBreak(1);
            write(arryloancode);
            writeLineBreak(1);
            write(aarryppliedamt);
            writeLineBreak(1);
            write(arrydisbursmentmethod);
            writeLineBreak(3);

            write(servedBy);
            writeLineBreak(3);
            write(signature);
            writeLineBreak(5);
            writeLineBreak(5);
            PrinterInterface.end();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void write(final byte[] arryData) {
        int result = PrinterInterface.write(arryData, arryData.length);
    }

    private void writeLineBreak(int lineNumber) {
        write(PrinterCommand.getCmdEscDN(lineNumber));
    }


    void getQualifiedKgs(String identifier) {
        final String TAG = "QUALIFIED KGS";

        String URL = GFL.MSACCO_AGENT + GFL.GetQualifiedKgs;
        String gNumber = SharedPrefs.read(SharedPrefs.LOAN_APP_GROWER, null);
        String loanCode = SharedPrefs.read(SharedPrefs.LOAN_PT_CODE, null);

        final GetQualifiedKgsRequest getQualifiedKgsRequest = new GetQualifiedKgsRequest();
        getQualifiedKgsRequest.corporateno = "CAP016";
        getQualifiedKgsRequest.growerno = gNumber;
        getQualifiedKgsRequest.loanproduct = loanCode;
        getQualifiedKgsRequest.amount = identifier;
        getQualifiedKgsRequest.transactiontype = "";
        getQualifiedKgsRequest.agentid = SharedPrefs.read(SharedPrefs.AGENT_ID_1, null);
        getQualifiedKgsRequest.terminalid = SharedPrefs.read(SharedPrefs.DEVICE_ID, null);
        getQualifiedKgsRequest.longitude = getLong(getContext());
        getQualifiedKgsRequest.latitude = getLat(getContext());
        getQualifiedKgsRequest.date = getFormattedDate();

        Log.e(TAG, getQualifiedKgsRequest.getBody().toString());

        GFL.instance(getContext()).request(URL, getQualifiedKgsRequest, new GFL.RequestListener() {
            @Override
            public void onSuccess(String response) {
                GetQualifiedKgsResponse getQualifiedKgsResponse = GFL.instance(getContext())
                        .mGson.fromJson(response, GetQualifiedKgsResponse.class);
                if (getQualifiedKgsResponse.is_successful) {
                    try {
                        JSONObject jsonObject = new JSONObject(response);

                        String qualifiedKgs = jsonObject.getString("qualifiedkgs");
                        SharedPrefs.write(SharedPrefs.LOAN_QUALIFIED_KGS, qualifiedKgs);
                        Log.e("QUALIFIED KGS", qualifiedKgs);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    //submit.setVisibility(View.GONE);
                    // getGuarantor();
                } else {
//                    Toast.makeText(ShareDeposit.this, "Accounts Fetching Failed", Toast.LENGTH_LONG).show();
                }

            }

            @Override
            public void onTokenExpired() {

            }

            @Override
            public void onError(String error) {

            }
        });
    }

    void getNetTransactionAmount(String identifier) {
        final String TAG = "TRANSACTION AMOUNT";

        String URL = GFL.MSACCO_AGENT + GFL.GetNetAmount;
        String gNumber = SharedPrefs.read(SharedPrefs.LOAN_APP_GROWER, null);
        String loanCode = SharedPrefs.read(SharedPrefs.LOAN_PT_CODE, null);

        final GetNetAmountRequest getNetAmountRequest = new GetNetAmountRequest();
        getNetAmountRequest.corporateno = "CAP016";
        getNetAmountRequest.growersno = gNumber;
        getNetAmountRequest.loanproductcode = loanCode;
        getNetAmountRequest.amount = enter_amount.getText().toString();
        getNetAmountRequest.modeofdisbursement = identifier;
        getNetAmountRequest.transactiontype = "";
        getNetAmountRequest.agentid = SharedPrefs.read(SharedPrefs.AGENT_ID_1, null);
        getNetAmountRequest.terminalid = SharedPrefs.read(SharedPrefs.DEVICE_ID, null);
        getNetAmountRequest.longitude = getLong(getContext());
        getNetAmountRequest.latitude = getLat(getContext());
        getNetAmountRequest.date = getFormattedDate();

        Log.e(TAG, getNetAmountRequest.getBody().toString());

        GFL.instance(getContext()).request(URL, getNetAmountRequest, new GFL.RequestListener() {
            @Override
            public void onSuccess(String response) {

                GetNetAmountResponse netAmountResponse = GFL.instance(getContext())
                        .mGson.fromJson(response, GetNetAmountResponse.class);
                if (netAmountResponse.is_successful) {
                    progressBar1.setVisibility(View.GONE);
                    try {
                        JSONObject jsonObject = new JSONObject(response);

                        JSONArray jsonArray = jsonObject.getJSONArray("getnetamountresponselist");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                            JSONArray jsonArray1 = jsonObject1.getJSONArray("netAmount");
                            for (int l = 0; l < jsonArray1.length(); l++) {
                                JSONObject jsonObject2 = jsonArray1.getJSONObject(l);
                                String netDisbursement = jsonObject2.getString("NetDisbursement");
                                String netChaGreaterNetDis = jsonObject2.getString("NetChaGreaterNetDis");
                                finalAmount.setText(netDisbursement);
                            }

                        }//888267
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {

                }

            }

            @Override
            public void onTokenExpired() {

            }

            @Override
            public void onError(String error) {

            }
        });
    }

    void getNetAmount(String amount) {
        final String TAG = "GET NET AMOUNT";

        String URL = GFL.MSACCO_AGENT + GFL.GetNetAmount;
        String gNumber = SharedPrefs.read(SharedPrefs.LOAN_APP_GROWER, null);
        String loanCode = SharedPrefs.read(SharedPrefs.LOAN_PT_CODE, null);

        final GetNetAmountRequest getNetAmountRequest = new GetNetAmountRequest();
        getNetAmountRequest.corporateno = "CAP016";
        getNetAmountRequest.growersno = gNumber;
        getNetAmountRequest.loanproductcode = loanCode;
        getNetAmountRequest.amount = amount;
        getNetAmountRequest.modeofdisbursement = "0";
        getNetAmountRequest.transactiontype = "";
        getNetAmountRequest.agentid = SharedPrefs.read(SharedPrefs.AGENT_ID_1, null);
        getNetAmountRequest.terminalid = SharedPrefs.read(SharedPrefs.DEVICE_ID, null);
        getNetAmountRequest.longitude = getLong(getContext());
        getNetAmountRequest.latitude = getLat(getContext());
        getNetAmountRequest.date = getFormattedDate();

        Log.e(TAG, getNetAmountRequest.getBody().toString());

        GFL.instance(getContext()).request(URL, getNetAmountRequest, new GFL.RequestListener() {
            @Override
            public void onSuccess(String response) {
                GetNetAmountResponse netAmountResponse = GFL.instance(getContext())
                        .mGson.fromJson(response, GetNetAmountResponse.class);
                if (netAmountResponse.is_successful) {
                    progressBar.setVisibility(View.GONE);
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        JSONArray jsonArray = jsonObject.getJSONArray("getnetamountresponselist");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                            JSONArray jsonArray1 = jsonObject1.getJSONArray("netAmount");
                            for (int l = 0; l < jsonArray1.length(); l++) {
                                JSONObject jsonObject2 = jsonArray1.getJSONObject(l);
                                String netDisbursement = jsonObject2.getString("NetDisbursement");
                                String netChaGreaterNetDis = jsonObject2.getString("NetChaGreaterNetDis");
                                netAmount.setText(netDisbursement);
                                String idee = SharedPrefs.read(SharedPrefs.IDENTIFIER_DIP_NUMBER, null);
                                String amount_identifier = netAmount.getText().toString();
                                String ProductCode = SharedPrefs.read(SharedPrefs.LOAN_PT_CODE, null);
                                bankExists = SharedPrefs.read(SharedPrefs.LOAN_BANK_EXISTS, null);
                                saccoExists = SharedPrefs.read(SharedPrefs.LOAN_SACCO_EXISTS, null);

                                age = SharedPrefs.read(SharedPrefs.GROWERS_AGE, null);

                                loanlmt = Integer.parseInt(SharedPrefs.read(SharedPrefs.MPESA_LIMIT, null).replaceAll(",",""));
                                try {
                                    if (saccoExists.equals("Yes")) {
                                        int amount = Integer.parseInt(amount_identifier);
                                        List<String> spinnerArray = new ArrayList<>();
                                        if (Integer.parseInt(age)>80){
                                            spinnerArray.add("Sacco");
                                        }else {
                                            if (amount < loanlmt) {
                                                spinnerArray.add("Mpesa");
                                            }else {
                                                spinnerArray.add("Sacco");
                                            }

                                        }
                                        ArrayAdapter<String> adapter = new ArrayAdapter<String>(
                                                getContext(),
                                                android.R.layout.simple_spinner_item,
                                                spinnerArray
                                        );
                                        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                        spinner.setAdapter(adapter);
                                    }
                                    if (bankExists.equals("Yes")) {
                                        int amount = Integer.parseInt(amount_identifier);
                                            List<String> spinnerArray = new ArrayList<>();
                                            if (Integer.parseInt(age)>80){
                                                spinnerArray.add("EFT");
                                            }else {
                                                if (amount < loanlmt) {
                                                    spinnerArray.add("Mpesa");
                                                }else {
                                                    spinnerArray.add("EFT");
                                                }

                                            }
                                            ArrayAdapter<String> adapter = new ArrayAdapter<String>(
                                                    getContext(),
                                                    android.R.layout.simple_spinner_item,
                                                    spinnerArray
                                            );
                                            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                            spinner.setAdapter(adapter);
                                    }
                                    if (bankExists.equals("Yes") && saccoExists.equals("Yes")) {
                                        int amount = Integer.parseInt(amount_identifier);
                                        List<String> spinnerArray = new ArrayList<>();
                                        if (Integer.parseInt(age)>80){
                                            spinnerArray.add("EFT");
                                        }else {
                                            if (amount < loanlmt) {
                                                spinnerArray.add("Mpesa");
                                            }else {
                                                spinnerArray.add("EFT");
                                                spinnerArray.add("Sacco");
                                            }

                                        }
                                        ArrayAdapter<String> adapter = new ArrayAdapter<String>(
                                                getContext(),
                                                android.R.layout.simple_spinner_item,
                                                spinnerArray
                                        );
                                        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                        spinner.setAdapter(adapter);
                                    }

                                    if (!bankExists.equals("Yes") && !saccoExists.equals("Yes")) {
                                        int amount = Integer.parseInt(amount_identifier);
                                        if (amount < loanlmt) {
                                            List<String> spinnerArray = new ArrayList<>();

                                            if (Integer.parseInt(age)>80){
                                                Utils.showAlertDialog(getActivity(),"Alert", "Update your Bank/Sacco details");
                                            }else {
                                                spinnerArray.add("Mpesa");
                                            }
                                            ArrayAdapter<String> adapter = new ArrayAdapter<String>(
                                                    getContext(),
                                                    android.R.layout.simple_spinner_item,
                                                    spinnerArray
                                            );
                                            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                            spinner.setAdapter(adapter);
                                        }

                                        if (amount > loanlmt) {
                                            Toast.makeText(getContext(), "The amount can not be disbursed via Mpesa", Toast.LENGTH_SHORT).show();
                                        }

                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }

                        }


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                } else {
//                    Toast.makeText(ShareDeposit.this, "Accounts Fetching Failed", Toast.LENGTH_LONG).show();
                }

            }

            @Override
            public void onTokenExpired() {

            }

            @Override
            public void onError(String error) {

            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
    }
}
