package com.coretec.agencyApplication.fragments;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.text.InputType;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.coretec.agencyApplication.R;
import com.coretec.agencyApplication.utils.SharedPrefs;
import com.stepstone.stepper.BlockingStep;
import com.stepstone.stepper.StepperLayout;
import com.stepstone.stepper.VerificationError;

import java.io.ByteArrayOutputStream;

public class SaccoFragment extends Fragment implements BlockingStep {

    LinearLayout saccoCard;
    private static final int CAMERA_REQUEST = 1888;
    private ImageView identity_imageView;
    private EditText sacco_code, sacco_account;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.gfl_sacco_details, container, false);
        SharedPrefs.init(getContext());

        //initialize your UI
        identity_imageView = v.findViewById(R.id.imageview_identity);
        sacco_code = v.findViewById(R.id.et_sacco_code);
        sacco_account = v.findViewById(R.id.et_sacco_acc_number);
        sacco_code.setInputType(InputType.TYPE_NULL);
        sacco_code.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // showMyDialog();
            }
        });

        sacco_code.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    // showMyDialog();
                }
            }
        });
        sacco_account.setInputType(InputType.TYPE_NULL);
        sacco_account.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // showMyDialog();
            }
        });

        sacco_account.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    // showMyDialog();
                }
            }
        });

        saccoCard = v.findViewById(R.id.saccoCard);
        saccoCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(cameraIntent, CAMERA_REQUEST);
            }
        });

        return v;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case CAMERA_REQUEST:
                if (resultCode == Activity.RESULT_OK) {
                    try {
                        Bitmap photo = (Bitmap) data.getExtras().get("data");
                        identity_imageView.setImageBitmap(photo);

                        //imageView.setImageBitmap(bitmap);
                        /*Toast.makeText(this, imageview_reason.toString(),
                                Toast.LENGTH_SHORT).show();*/
                    } catch (Exception e) {
                        Toast.makeText(getContext(), "Failed to load", Toast.LENGTH_SHORT)
                                .show();
                        Log.e("Camera", e.toString());
                    }
                }
        }
    }

    private String bitmapToBase64(Bitmap bitmap) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
        byte[] byteArray = byteArrayOutputStream.toByteArray();
        return Base64.encodeToString(byteArray, Base64.DEFAULT);
    }

    @Override
    public void onNextClicked(final StepperLayout.OnNextClickedCallback callback) {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Bitmap img = ((BitmapDrawable) identity_imageView.getDrawable()).getBitmap();
                String encodedImage = bitmapToBase64(img);

                if (sacco_account.getText().toString().matches("") ||
                        sacco_code.getText().toString().matches("")){
                    SharedPrefs.write(SharedPrefs.REG_SACCO_CODE, "null");
                    SharedPrefs.write(SharedPrefs.REG_SACCO_AC_NO, "null");

                    SharedPrefs.write(SharedPrefs.REG_SACOO_CARD, "null");
                    //Toast.makeText(getContext(), "Please make sure all details are filled!", Toast.LENGTH_SHORT).show();
                    callback.goToNextStep();
                } else {
                    //you can do anythings you want
                    SharedPrefs.write(SharedPrefs.REG_SACCO_CODE, sacco_code.getText().toString());
                    SharedPrefs.write(SharedPrefs.REG_SACCO_AC_NO, sacco_account.getText().toString());

                    SharedPrefs.write(SharedPrefs.REG_SACOO_CARD, encodedImage);
                    //you can do anythings you want
                    callback.goToNextStep();
                }
            }
        }, 0L);// delay open another fragment,
    }
    @Override
    public void onCompleteClicked(StepperLayout.OnCompleteClickedCallback callback) {
        /*Toast.makeText(getActivity(),"Registration Successful",Toast.LENGTH_SHORT).show();
        startActivity(new Intent(getActivity(), Dashboard.class));
        getActivity().finish();*/
    }
    @Override
    public void onBackClicked(final StepperLayout.OnBackClickedCallback callback) {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                //you can do anythings you want
                /*callback.goToNextStep();*/
                callback.goToPrevStep();
            }
        }, 0L);// delay open another fragment,
    }
    @Override
    public VerificationError verifyStep() {
        return null;
    }
    @Override
    public void onSelected() {
    }
    @Override
    public void onError(@NonNull VerificationError error) {
    }

}
