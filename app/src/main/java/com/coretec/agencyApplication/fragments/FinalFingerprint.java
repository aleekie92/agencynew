package com.coretec.agencyApplication.fragments;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.cloudpos.DeviceException;
import com.cloudpos.OperationResult;
import com.cloudpos.POSTerminal;
import com.cloudpos.TimeConstants;
import com.cloudpos.fingerprint.Fingerprint;
import com.cloudpos.fingerprint.FingerprintDevice;
import com.cloudpos.fingerprint.FingerprintOperationResult;
import com.coretec.agencyApplication.R;
import com.coretec.agencyApplication.api.GFL;
import com.coretec.agencyApplication.api.requests.CreateVirtualRegistrationRequest;
import com.coretec.agencyApplication.api.responses.CreateVirtualRegistrationResponse;
import com.coretec.agencyApplication.utils.ByteConvertStringUtil;
import com.coretec.agencyApplication.utils.LogHelper;
import com.coretec.agencyApplication.utils.SharedPrefs;
import com.stepstone.stepper.BlockingStep;
import com.stepstone.stepper.StepperLayout;
import com.stepstone.stepper.VerificationError;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;

import static com.coretec.agencyApplication.utils.Utils.getFormattedDate;
import static com.coretec.agencyApplication.utils.Utils.getLat;
import static com.coretec.agencyApplication.utils.Utils.getLong;

public class FinalFingerprint extends Fragment implements BlockingStep, View.OnClickListener {

    //Button enroll;
    Button leftRing, leftLittle, rightRing, rightLittle;
    Button delete;
    Button clear;
    Button quit;
    TextView enrollText;

    ProgressDialog progressDialog;

    //crossmatch fingerprint declarations
    protected TextView log_text;
    protected Handler mHandler = null;
    protected Runnable runnable = null;

    //Fingerprint Declarations
    private TextView show;


    //private Context context = getActivity();
    //private Context mContext = this;
    private int userID = 0;
    private int timeout = 10 * 1000;
    private FingerprintDevice fingerprintDevice = null;

    private SharedPreferences preferences;
    private SharedPreferences.Editor editor;

    private static final String FINGERINDEX1 = "Finger1";
    private static final String FINGERINDEX2 = "Finger2";
    private static final String FINGERINDEX3 = "Finger3";
    private static final String FINGERINDEX4 = "Finger4";

    private Handler handler;
    private static final int SHOW_NORMAL_MESSAGE = 0;
    private static final int SHOW_SUCCESS_MESSAGE = 1;
    private static final int SHOW_FAIL_MESSAGE = 2;
    private static final int SHOW_BTN = 3;
    private static final int HIDE_BTN = 4;
    private LinearLayout enrollButtons;
    private RelativeLayout RelativeLayout02;
    Thread th = null;

    @SuppressLint("HandlerLeak")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        SharedPrefs.init(getContext());
        View v = inflater.inflate(R.layout.final_fingerprint, container, false);

        //initialize your UI
        leftRing = v.findViewById(R.id.btn_left_ring);
        leftLittle = v.findViewById(R.id.btn_left_little);
        rightRing = v.findViewById(R.id.btn_right_ring);
        rightLittle = v.findViewById(R.id.btn_right_little);
        delete = v.findViewById(R.id.ButtonDelete);
        clear = v.findViewById(R.id.ButtonClear);
        show = v.findViewById(R.id.EnrollmentTextView);

        enrollButtons = v.findViewById(R.id.enrollButtons);
        RelativeLayout02 = v.findViewById(R.id.RelativeLayout02);
        enrollButtons.setVisibility(View.VISIBLE);
        RelativeLayout02.setVisibility(View.VISIBLE);

        leftRing.setOnClickListener(this);
        leftLittle.setOnClickListener(this);
        rightRing.setOnClickListener(this);
        rightLittle.setOnClickListener(this);
        delete.setOnClickListener(this);
        clear.setOnClickListener(this);


        fingerprintDevice = (FingerprintDevice) POSTerminal.getInstance(getActivity()).getDevice("cloudpos.device.fingerprint");
        preferences = this.getActivity().getSharedPreferences("userFinger", Context.MODE_PRIVATE);
        editor = preferences.edit();
        editor.putString(FINGERINDEX1, "0");
        editor.putString(FINGERINDEX2, "0");
        editor.putString(FINGERINDEX3, "0");
        editor.putString(FINGERINDEX4, "0");
        editor.apply();

        //log_text = this.findViewById(R.id.text_result);
        show.setMovementMethod(ScrollingMovementMethod.getInstance());

        handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                super.handleMessage(msg);
                switch (msg.what) {
                    case SHOW_NORMAL_MESSAGE:
                        sendMsg((String) msg.obj);
                        scrollLogView();
                        break;
                    case SHOW_SUCCESS_MESSAGE:
                        sendSuccessMsg((String) msg.obj);
                        scrollLogView();
                        break;
                    case SHOW_FAIL_MESSAGE:
                        sendFailMsg((String) msg.obj);
                        scrollLogView();
                        break;

                    case SHOW_BTN:

                        break;
                    case HIDE_BTN:

                        break;
                }
            }
        };

        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    int result = fingerprintDevice.delAllFingers();
                    if (result >= 0) {
                        handler.obtainMessage(SHOW_SUCCESS_MESSAGE, getStr(R.string.Cleared)).sendToTarget();
                    }
                } catch (DeviceException e) {
                    e.printStackTrace();
                    handler.obtainMessage(SHOW_FAIL_MESSAGE, getStr(R.string.MatchFailed)).sendToTarget();
                }
                Toast.makeText(getActivity(), getActivity().getText(R.string.Cleared), Toast.LENGTH_SHORT).show();
            }
        });
        clear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //textView.setText("");
                show.setText("");
            }
        });

        return v;
    }

    private void fingerPut(String fingerIndex) {
        preferences = this.getActivity().getSharedPreferences("userFinger", Context.MODE_PRIVATE);
        editor = preferences.edit();
        Fingerprint fingerprint = getFingerprint();
        if (fingerprint != null) {
            byte[] buffer1 = fingerprint.getFeature();
            Log.e("FINGERPRINT_DEMO", buffer1.length + "");
            editor.putString(fingerIndex, ByteConvertStringUtil.bytesToHexString(buffer1));
            editor.apply();
        }
        handler.obtainMessage(SHOW_SUCCESS_MESSAGE, this.getActivity().getString(R.string.entry) + fingerIndex).sendToTarget();
    }

    private Fingerprint getFingerprint() {
        Fingerprint fingerprint = null;
        try {
            FingerprintOperationResult operationResult = fingerprintDevice.waitForFingerprint(TimeConstants.FOREVER);
            if (operationResult.getResultCode() == OperationResult.SUCCESS) {
                fingerprint = operationResult.getFingerprint(0, 0);
                Log.e("FINGERPRINT_DEMO", "operationResult SUCCESS!" + fingerprint.getFeature().length);
                //handler.obtainMessage(SHOW_SUCCESS_MESSAGE, mContext.getString(R.string.SUCCESSINFO)).sendToTarget();
            } else {
                handler.obtainMessage(SHOW_FAIL_MESSAGE, this.getActivity().getString(R.string.FAILEDINFO)).sendToTarget();
            }
        } catch (DeviceException e) {
            e.printStackTrace();
            handler.obtainMessage(SHOW_FAIL_MESSAGE, this.getActivity().getString(R.string.DEVICEFAILED)).sendToTarget();
        }
        return fingerprint;
    }

    private void sendSuccessMsg(String msg) {
        LogHelper.infoAppendMsgForSuccess(msg + "\n", show);
        scrollLogView();
    }

    private void sendFailMsg(String msg) {
        LogHelper.infoAppendMsgForFailed(msg + "\n", show);
        scrollLogView();
    }

    private void sendMsg(String msg) {
        LogHelper.infoAppendMsg(msg + "\n", show);
        scrollLogView();
    }

    public void scrollLogView() {
        int offset = show.getLineCount() * show.getLineHeight();
        if (offset > show.getHeight()) {
            show.scrollTo(0, offset - show.getHeight());
        }
    }

    private String getStr(int strId) {
        return getResources().getString(strId);
    }




    @Override
    public void onNextClicked(final StepperLayout.OnNextClickedCallback callback) {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                SharedPreferences prefs = getActivity().getSharedPreferences("userFinger", Context.MODE_PRIVATE);
                callback.goToNextStep();
            }
        }, 0L);// delay open another fragment,
    }

    @Override
    public void onCompleteClicked(StepperLayout.OnCompleteClickedCallback callback) {
        String msg = "Are you sure you want to submit customer registration?";
        //final String qualKgs = SharedPrefs.read(SharedPrefs.LOAN_QUALIFIED_KGS, null);
        AlertDialog.Builder builder1 = new AlertDialog.Builder(getContext());
        builder1.setMessage(msg);
        builder1.setTitle("Confirm!");
        builder1.setCancelable(false);

        builder1.setPositiveButton(
                "YES",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();

                        SharedPreferences prefs = getActivity().getSharedPreferences("userFinger", Context.MODE_PRIVATE);
                        String finger1 = prefs.getString("Finger1", null);
                        String finger2 = prefs.getString("Finger2", null);
                        String finger3 = prefs.getString("Finger3", null);
                        String finger4 = prefs.getString("Finger4", null);



                        assert finger1 != null;
                        assert finger2 != null;
                        assert finger3 != null;
                        assert finger4 != null;
                        if (finger1.equals("0") || finger2.equals("0") || finger3.equals("0") || finger4.equals("0")) {
                            Toast.makeText(getContext(), "Please input user fingerprints to proceed!", Toast.LENGTH_SHORT).show();
                        } else {

                            progressDialog = new ProgressDialog(getContext());
                            progressDialog.setMessage("Submitting customer registration details...");
                            progressDialog.setCancelable(false);
                            progressDialog.show();
                            submitCustomerRegistration();
                            //startActivity(new Intent(getContext(), Loans.class));
                        }
                    }
                });

        builder1.setNegativeButton(
                "Cancel",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

        AlertDialog alert11 = builder1.create();
        alert11.show();

    }

    @Override
    public void onBackClicked(final StepperLayout.OnBackClickedCallback callback) {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                //you can do anythings you want
                /*callback.goToNextStep();*/
                callback.goToPrevStep();
            }
        }, 0L);// delay open another fragment,
    }

    @Override
    public VerificationError verifyStep() {
        return null;
    }

    @Override
    public void onSelected() {

    }

    @Override
    public void onError(@NonNull VerificationError error) {

    }

    void submitCustomerRegistration() {
        SharedPreferences prefs = getActivity().getSharedPreferences("userFinger", Context.MODE_PRIVATE);
        String finger1 = prefs.getString("Finger1", null);
        String finger2 = prefs.getString("Finger2", null);
        String finger3 = prefs.getString("Finger3", null);
        String finger4 = prefs.getString("Finger4", null);

        final String TAG = "CUSTOMER REGISTRATION";

        String URL = GFL.MSACCO_AGENT + GFL.CreateVirtualRegistration;
        String growerNumBer = SharedPrefs.read(SharedPrefs.LOAN_APP_GROWER, null);
        String kodi = SharedPrefs.read(SharedPrefs.LOAN_PT_CODE, null);
        String idNumber = SharedPrefs.read(SharedPrefs.LOAN_IDENTITY_NO, null);
        String growerName = SharedPrefs.read(SharedPrefs.LOAN_APP_G_NAME, null);
        String spinnerItem = SharedPrefs.read(SharedPrefs.LOAN_SPINNER, null);
        SimpleDateFormat timeStampFormat = new SimpleDateFormat("yyyy/MM/dd - HH:mm");
        Date myDate = new Date();

        Log.e("LEFT RING", finger1);
        Log.e("LEFT LITTLE", finger2);
        Log.e("RIGHT RING", finger3);
        Log.e("RIGHT LITTLE", finger4);

        final CreateVirtualRegistrationRequest createVirtualRegistrationRequest = new CreateVirtualRegistrationRequest();
        createVirtualRegistrationRequest.growerno = SharedPrefs.read(SharedPrefs.REG_GROWER_NO, null);
        createVirtualRegistrationRequest.accountname = SharedPrefs.read(SharedPrefs.REG_ACC_NAME, null);
        createVirtualRegistrationRequest.idno = SharedPrefs.read(SharedPrefs.REG_IDNUMBER, null);
        createVirtualRegistrationRequest.phoneno = SharedPrefs.read(SharedPrefs.REG_PHONE, null);
        createVirtualRegistrationRequest.gender = SharedPrefs.read(SharedPrefs.REG_GENDER, null);
        createVirtualRegistrationRequest.dob =myDate;
        createVirtualRegistrationRequest.passportphoto = SharedPrefs.read(SharedPrefs.REG_PASSPORT, null);
        createVirtualRegistrationRequest.idfrontphoto = SharedPrefs.read(SharedPrefs.REG_FRONT_ID, null);
        createVirtualRegistrationRequest.idbackphoto = SharedPrefs.read(SharedPrefs.REG_BACK_ID, null);
        createVirtualRegistrationRequest.nextofkin = SharedPrefs.read(SharedPrefs.REG_FULL_NAME, null);
        createVirtualRegistrationRequest.relationship = SharedPrefs.read(SharedPrefs.REG_RELATIONSHIP, null);
        createVirtualRegistrationRequest.puregister = SharedPrefs.read("PU_REGISTERED", SharedPrefs.PU_REGISTERED);
        createVirtualRegistrationRequest.bankandbranchcode = SharedPrefs.read(SharedPrefs.REG_BANK_DETAILS, null);
        createVirtualRegistrationRequest.bankaccountno = SharedPrefs.read(SharedPrefs.REG_ACCOUNT_NO, null);
        createVirtualRegistrationRequest.bankcardphoto = SharedPrefs.read(SharedPrefs.REG_ATM_CARD, null);
        createVirtualRegistrationRequest.leftring = finger1;
        createVirtualRegistrationRequest.leftlittle = finger2;
        createVirtualRegistrationRequest.rightring = finger3;
        createVirtualRegistrationRequest.rightlittle = finger4;
        createVirtualRegistrationRequest.transactiontype = "";
        createVirtualRegistrationRequest.agentid = SharedPrefs.read(SharedPrefs.AGENT_ID_1, null);
        createVirtualRegistrationRequest.terminalid = SharedPrefs.read(SharedPrefs.DEVICE_ID, null);
        createVirtualRegistrationRequest.longitude = getLong(getContext());
        createVirtualRegistrationRequest.latitude = getLat(getContext());
        createVirtualRegistrationRequest.date = getFormattedDate();
        createVirtualRegistrationRequest.corporate_no = "CAP016";

        createVirtualRegistrationRequest.saccoaccno = SharedPrefs.read(SharedPrefs.REG_SACCO_AC_NO, null);
        createVirtualRegistrationRequest.saccocode = SharedPrefs.read(SharedPrefs.REG_SACCO_CODE, null);
        createVirtualRegistrationRequest.saccocardphoto = SharedPrefs.read(SharedPrefs.REG_SACOO_CARD, null);
        createVirtualRegistrationRequest.pureglanguage = SharedPrefs.read(SharedPrefs.REG_LANG_SPINNER, null);
        createVirtualRegistrationRequest.nextofkinid = SharedPrefs.read(SharedPrefs.REG_KIN_ID, null);

        Log.e(TAG, createVirtualRegistrationRequest.getBody().toString());

        GFL.instance(getContext()).request(URL, createVirtualRegistrationRequest, new GFL.RequestListener() {
            @Override
            public void onSuccess(String response) {
                CreateVirtualRegistrationResponse createVirtualRegistrationResponse = GFL.instance(getContext())
                        .mGson.fromJson(response, CreateVirtualRegistrationResponse.class);
                if (createVirtualRegistrationResponse.operationsuccess) {
                    progressDialog.dismiss();
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        String receiptNo = jsonObject.getString("receiptNo");
                        Toast.makeText(getContext(), receiptNo, Toast.LENGTH_SHORT).show();
                        Log.e("SUCCESSFUL", receiptNo);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    String msg = "Customer registration submitted successfully!";
                    AlertDialog.Builder builder1 = new AlertDialog.Builder(getContext());
                    builder1.setMessage(msg);
                    builder1.setTitle("Success!");
                    builder1.setCancelable(false);

                    builder1.setPositiveButton(
                            "DONE",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                    /*int result = PrinterInterface.open();
                                    writetest();
                                    PrinterInterface.close();*/

                                    SharedPrefs.write(SharedPrefs.REG_GROWER_NO, "null");
                                    SharedPrefs.write(SharedPrefs.REG_ACC_NAME, "null");
                                    SharedPrefs.write(SharedPrefs.REG_IDNUMBER, "null");
                                    SharedPrefs.write(SharedPrefs.REG_PHONE, "null");
                                    SharedPrefs.write(SharedPrefs.REG_GENDER, "null");
                                    SharedPrefs.write(SharedPrefs.REG_PASSPORT, "null");
                                    SharedPrefs.write(SharedPrefs.REG_FRONT_ID, "null");
                                    SharedPrefs.write(SharedPrefs.REG_BACK_ID, "null");
                                    SharedPrefs.write(SharedPrefs.REG_FULL_NAME, "null");
                                    SharedPrefs.write(SharedPrefs.REG_RELATIONSHIP, "null");
                                    //SharedPrefs.write("PU_REGISTERED", SharedPrefs.PU_REGISTERED);
                                    SharedPrefs.write(SharedPrefs.REG_BANK_DETAILS, "null");
                                    SharedPrefs.write(SharedPrefs.REG_ACCOUNT_NO, "null");
                                    SharedPrefs.write(SharedPrefs.REG_ATM_CARD, "null");

                                    SharedPrefs.write(SharedPrefs.REG_SACCO_AC_NO, "null");
                                    SharedPrefs.write(SharedPrefs.REG_SACCO_CODE, "null");
                                    SharedPrefs.write(SharedPrefs.REG_SACOO_CARD, "null");
                                    SharedPrefs.write(SharedPrefs.REG_LANG_SPINNER, "null");

                                    getActivity().finish();
                                }
                            });



                    AlertDialog alert11 = builder1.create();
                    alert11.show();

                } else {
                    progressDialog.dismiss();
                    Toast.makeText(getContext(), "Customer registration failed. Try again later!", Toast.LENGTH_LONG).show();
                }

            }

            @Override
            public void onTokenExpired() {

            }

            @Override
            public void onError(String error) {

            }
        });
    }

    @Override
    public void onClick(View v) {

        if (v.getId() != R.id.ButtonDelete) {
            sendMsg(getStr(R.string.MESSAGE));
        }
        if (v.getId() == R.id.ButtonDelete || v.getId() == R.id.ButtonClear) {
            switch (v.getId()) {
                case R.id.ButtonDelete:
                    try {
                        int result = fingerprintDevice.delAllFingers();
                        if (result >= 0) {
                            handler.obtainMessage(SHOW_SUCCESS_MESSAGE, getStr(R.string.Cleared)).sendToTarget();
                        }
                    } catch (DeviceException e) {
                        e.printStackTrace();
                        handler.obtainMessage(SHOW_FAIL_MESSAGE, getStr(R.string.MatchFailed)).sendToTarget();
                    }

                    break;
                case R.id.ButtonClear:
                    show.setText("");
                    break;
            }
        } else {
            if (th == null || th.getState() == Thread.State.TERMINATED) {
                switch (v.getId()) {
                    case R.id.btn_left_ring:
                        th = new Thread() {
                            @Override
                            public void run() {
                                fingerPut(FINGERINDEX1);
                            }
                        };

                        break;
                    case R.id.btn_left_little:
                        th = new Thread() {
                            @Override
                            public void run() {
                                fingerPut(FINGERINDEX2);
                            }
                        };
                        break;
                    case R.id.btn_right_ring:
                        th = new Thread() {
                            @Override
                            public void run() {
                                fingerPut(FINGERINDEX3);
                            }
                        };
                        break;
                    case R.id.btn_right_little:
                        th = new Thread() {
                            @Override
                            public void run() {
                                fingerPut(FINGERINDEX4);
                            }
                        };
                        break;
                }
                th.start();
            }
        }

    }

    void  openDevice(){
        try {
            fingerprintDevice.open(1);
        } catch (DeviceException e) {
            e.printStackTrace();
        }
    }


    @Override
    public void onResume() {
        super.onResume();
        openDevice();
    }
}
