package com.coretec.agencyApplication.fragments.newmodule;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.coretec.agencyApplication.R;
import com.coretec.agencyApplication.activities.LoanRepayment;
import com.coretec.agencyApplication.activities.newmodules.MainDashboardActivity;
import com.coretec.agencyApplication.api.Api;
import com.coretec.agencyApplication.api.requests.RequestMemberName;
import com.coretec.agencyApplication.api.responses.MemberNameResponse;
import com.coretec.agencyApplication.model.newmodulemodels.PaymentObject;
import com.coretec.agencyApplication.utils.SharedPrefs;
import com.stepstone.stepper.BlockingStep;
import com.stepstone.stepper.StepperLayout;
import com.stepstone.stepper.VerificationError;

import java.util.ArrayList;
import java.util.List;

public class PaymentFragment extends Fragment implements BlockingStep{

    private EditText idename, idephone, totalpresent,totalabsent,remarks,loan_repayment_identifier;
    private Button btn_account_search;
    private ProgressBar grouprepayment_progressBar;
    private TextInputLayout input_layout_name,input_layout_idephone,input_layout_totalpresent,input_layout_absent,input_layout_remarks;
    private List<PaymentObject> paymentObjectList = new ArrayList<>();
        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            View v = inflater.inflate(R.layout.adapter_payment_details, container, false);

            input_layout_name = v.findViewById(R.id.input_layout_name);
            input_layout_idephone = v.findViewById(R.id.input_layout_idephone);
            input_layout_totalpresent = v.findViewById(R.id.input_layout_totalpresent);
            input_layout_absent = v.findViewById(R.id.input_layout_absent);
            input_layout_remarks = v.findViewById(R.id.input_layout_remarks);

            loan_repayment_identifier = v.findViewById(R.id.loan_repayment_identifier);
            btn_account_search = v.findViewById(R.id.btn_account_search);
            grouprepayment_progressBar = v.findViewById(R.id.grouprepayment_progressBar);


            idename = v.findViewById(R.id.idename);
            idephone = v.findViewById(R.id.idephone);
            totalpresent = v.findViewById(R.id.totalpresent);
            totalabsent = v.findViewById(R.id.totalabsent);
            remarks = v.findViewById(R.id.remarks);

            idename.setFocusable(false);
            idephone.setFocusable(false);
            totalpresent.setFocusable(false);
            totalabsent.setFocusable(false);
            btn_account_search.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String identity=loan_repayment_identifier.getText().toString();

                    if (identity.matches("")){
                        Toast.makeText(getActivity(), "Please Enter ID number to proceed!", Toast.LENGTH_SHORT).show();
                    }else {
                        grouprepayment_progressBar.setVisibility(View.VISIBLE);
                        requestMemberDetails(identity);
                        totalpresent.setText(SharedPrefs.read(SharedPrefs.VR_TOTAL_PRESENT_MEMBERS, null));
                        totalabsent.setText(SharedPrefs.read(SharedPrefs.VR_TOTAL_ABSENT_MEMBERS, null));
                    }

                }
            });

            return v;
        }

        @Override
        public void onNextClicked(final StepperLayout.OnNextClickedCallback callback) {
                new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {

                                String name = idename.getText().toString().trim();
                                String phone = idephone.getText().toString().trim();
                                String totalp = totalpresent.getText().toString().trim();
                                String totala = totalabsent.getText().toString().trim();
                                String remarkss = remarks.getText().toString().trim();

                                if (loan_repayment_identifier.getText().toString().matches("")) {
                                    input_layout_name.setError("Enter the ID and click on search");
                                }else if (name.isEmpty()) {
                                    input_layout_name.setError("Enter the Verifier Name");
                                }  else if (phone.isEmpty()) {
                                        input_layout_idephone.setError("Enter the verifier phone number ");
                                } else if (totalp.isEmpty()) {
                                        input_layout_totalpresent.setError("Enter the total members present ");
                                } else if (totala.isEmpty()) {
                                        input_layout_absent.setError("Enter the total absent ");
                                } else if (remarkss.isEmpty()) {
                                        input_layout_remarks.setError("Enter the repayments remarks ");
                                } else {
                                        //you can do anythings you want
                                        SharedPrefs.moredetails(name,phone,remarkss,getActivity());
                                        callback.goToNextStep();
                                }

                        }
                }, 0L);// delay open another fragment,
        }

        @Override
        public void onCompleteClicked(StepperLayout.OnCompleteClickedCallback callback) {

        }

        @Override
        public void onBackClicked(final StepperLayout.OnBackClickedCallback callback) {

            SharedPreferences presentmembers,absentmembers;
            presentmembers = getActivity().getSharedPreferences("VR_TOTAL_PRESENT_MEMBERS", Context.MODE_PRIVATE);
            absentmembers = getActivity().getSharedPreferences("VR_TOTAL_ABSENT_MEMBERS", Context.MODE_PRIVATE);
            presentmembers.edit().clear().commit();
            absentmembers.edit().clear().commit();

            totalpresent.setText("");
            totalabsent.setText("");
            idename.setText("");
            idephone.setText("");
            remarks.setText("");
            loan_repayment_identifier.setText("");

            GroupMembersFragment gmf=new GroupMembersFragment();
            gmf.totalL=0.00;
            gmf.totalS=0.00;
            //callback.goToPrevStep();
        }

        @Override
        public VerificationError verifyStep() {
            return null;
        }

        @Override
        public void onSelected() {

        }

        @Override
        public void onError(@NonNull VerificationError error) {

        }

    void requestMemberDetails(String id) {
        RequestMemberName requestMemberName = new RequestMemberName();
        requestMemberName.corporateno = MainDashboardActivity.corporateno;
        requestMemberName.accountidentifier = id;
        requestMemberName.accountcode = "0";
        requestMemberName.agentid = SharedPrefs.read(SharedPrefs.AGENT_ID_1, null);////
        requestMemberName.terminalid = MainDashboardActivity.imei;

        Api.instance(getActivity()).request(Api.MSACCO_AGENT + Api.GetMembername, requestMemberName, new Api.RequestListener() {
            @Override
            public void onSuccess(String response) {
                grouprepayment_progressBar.setVisibility(View.GONE);

                MemberNameResponse memberNameResponse = Api.instance(getActivity()).mGson.fromJson(response, MemberNameResponse.class);
                if (memberNameResponse.is_successful) {
                    idename.setText(memberNameResponse.getCustomer_name());
                    idephone.setText(memberNameResponse.getPhoneno());
                }

                else {
                    grouprepayment_progressBar.setVisibility(View.VISIBLE);

                }

            }

            @Override
            public void onTokenExpired() {
                grouprepayment_progressBar.setVisibility(View.GONE);
            }

            @Override
            public void onError(String error) {
                Toast.makeText(getActivity(), "Failed to load", Toast.LENGTH_SHORT).show();
            }
        });
    }
}
