package com.coretec.agencyApplication.fragments;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.Toast;

import com.coretec.agencyApplication.R;
import com.coretec.agencyApplication.activities.newmodules.MainDashboardActivity;
import com.coretec.agencyApplication.api.Api;
import com.coretec.agencyApplication.api.requests.newmodulesrequests.SacooBranchRequest;
import com.coretec.agencyApplication.api.responses.newmoduleresponses.SaccoBranchesResponse;
import com.coretec.agencyApplication.model.newmodulemodels.BranchDetails;
import com.coretec.agencyApplication.utils.PreferenceFileKeys;
import com.coretec.agencyApplication.utils.SharedPrefs;
import com.stepstone.stepper.BlockingStep;
import com.stepstone.stepper.StepperLayout;
import com.stepstone.stepper.VerificationError;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import static com.coretec.agencyApplication.utils.Utils.getLat;
import static com.coretec.agencyApplication.utils.Utils.getLong;

public class AgencyMoreFragment extends Fragment implements BlockingStep {

  private Spinner status,nearestbranch;
  private EditText email,locality,nearestmarket;
  String[] employers;
  private ArrayList<String> branchesList = new ArrayList<>();

  private String item, itemLoan;
  private ArrayList<String> arrayListBranches = new ArrayList<>();
  private ArrayAdapter<BranchDetails> adapter;
  SharedPreferences sharedPreferences;
  private ProgressBar progressBar2;

  String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\..+[a-z]+";

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
    View v = inflater.inflate(R.layout.adapter_more_details, container, false);
    SharedPrefs.init(getContext());
    email = v.findViewById(R.id.registration_email);
    locality= v.findViewById(R.id.locality);

    nearestmarket= v.findViewById(R.id.nearestmarket);
    status = v.findViewById(R.id.maritalStatus);

    if (MainDashboardActivity.corporateno.equals("CAP021")){
      nearestmarket.setVisibility(View.GONE);
      status.setVisibility(View.GONE);
    }else {
      nearestmarket.setVisibility(View.VISIBLE);
      status.setVisibility(View.VISIBLE);
    }

    sharedPreferences = getContext().getSharedPreferences(PreferenceFileKeys.PREFS_FILE, Context.MODE_PRIVATE);
    status.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
      @Override
      public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        String item = parent.getItemAtPosition(position).toString();
        String text = status.getSelectedItem().toString();
        if (MainDashboardActivity.corporateno.equals("CAP021")){
          SharedPrefs.status("1",getActivity());
        }else {
          if(text.equals("Single")){SharedPrefs.status("1",getActivity());}
          if(text.equals("Married")){SharedPrefs.status("2",getActivity());}
          if(text.equals("Widower")){SharedPrefs.status("3",getActivity());}
          if(text.equals("Widow")){SharedPrefs.status("4",getActivity());}
        }
      }

      @Override
      public void onNothingSelected(AdapterView<?> parent) {

      }
    });



    nearestbranch= v.findViewById(R.id.nearestbranch);
    nearestbranch.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
      @Override
      public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        String text = nearestbranch.getSelectedItem().toString();
        String result = text.substring(0,text.lastIndexOf("."));
        SharedPrefs.branch(result,getActivity());
      }

      @Override
      public void onNothingSelected(AdapterView<?> parent) {

      }
    });
    return v;
  }

  private void requestNearestBranches() {

    String URL = Api.MSACCO_AGENT + Api.GetSaccoBranch;

    final SacooBranchRequest saccoBranches = new SacooBranchRequest();
    saccoBranches.agentid = sharedPreferences.getString(PreferenceFileKeys.AGENT_ID, "");
    saccoBranches.corporateno = MainDashboardActivity.corporateno;
    //2 pulls relationship
    saccoBranches.code = "1";
    saccoBranches.terminalid = MainDashboardActivity.imei;
    saccoBranches.longitude = getLong(getContext());
    saccoBranches.latitude = getLat(getContext());

    Api.instance(getActivity()).request(URL, saccoBranches, new Api.RequestListener() {
      @Override
      public void onSuccess(String response) {
        SaccoBranchesResponse branchesResponse = Api.instance(getContext())
                .mGson.fromJson(response, SaccoBranchesResponse.class);
        if (branchesResponse.is_successful) {
          // List<BranchDetails> branchList = new ArrayList<>();
          branchesList.clear();
          try {
            JSONObject jsonObject = new JSONObject(response);
            JSONArray jsonArray1 = jsonObject.getJSONArray("saccobarch");

            for (int l = 0; l < jsonArray1.length(); l++) {
              JSONObject jsonObject2 = jsonArray1.getJSONObject(l);

              String branch = jsonObject2.getString("branchname");
              String branchcode = jsonObject2.getString("branchcode");
              branchesList.add(branchcode+".  "+branch);

            }

            if(nearestbranch.getCount()==0) {
              Toast.makeText(getActivity(), "No branches! Contact administrator to add.", Toast.LENGTH_LONG).show();
            }else {
              try {
                ArrayAdapter<String> adapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_spinner_item, branchesList);
                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                nearestbranch.setAdapter(adapter);
              }catch (Exception e){
                e.printStackTrace();
              }

            }


          } catch (JSONException e) {
            e.printStackTrace();
          }

        } else {
          Toast.makeText(getActivity(), "Failed", Toast.LENGTH_LONG).show();
        }

      }

      @Override
      public void onTokenExpired() {

      }

      @Override
      public void onError(String error) {

      }
    });
  }

  @Override
  public void onNextClicked(final StepperLayout.OnNextClickedCallback callback) {
    new Handler().postDelayed(new Runnable() {
      @Override
      public void run() {

        if (MainDashboardActivity.corporateno .equals("CAP021")){
          if (email.getText().toString().matches("") ||
                  locality.getText().toString().matches("")) {
            Toast.makeText(getContext(), "Please fill all details!", Toast.LENGTH_SHORT).show();
          }
          else if(!email.getText().toString().trim().matches(emailPattern)){
            email.setError("Enter the correct email format");
          }

          else if (nearestbranch.getCount()==0) {
            Toast.makeText(getActivity(), "Contact administrator to add branches", Toast.LENGTH_LONG).show();
          }
          else  {
              SharedPrefs.contactss(email.getText().toString(),locality.getText().toString(),"MARKET",getActivity());
            callback.goToNextStep();
          }
        }else {
          if (email.getText().toString().matches("") || nearestmarket.getText().toString().matches("") ||
                  locality.getText().toString().matches("")) {
            Toast.makeText(getContext(), "Please fill all details!", Toast.LENGTH_SHORT).show();
          }
          else if(!email.getText().toString().trim().matches(emailPattern)){
            email.setError("Enter the correct email format");
          }

          else if (nearestbranch.getCount()==0) {
            Toast.makeText(getActivity(), "Contact administrator to add branches", Toast.LENGTH_LONG).show();
          }
          else if (status.getSelectedItem().toString().trim().equals("Marital Status")) {
            Toast.makeText(getActivity(), "Please select marital status", Toast.LENGTH_LONG).show();
          }
          else  {
              SharedPrefs.contactss(email.getText().toString(),locality.getText().toString(),nearestmarket.getText().toString(),getActivity());
              callback.goToNextStep();
          }
        }

      }
    }, 0L);// delay open another fragment,
  }

  @Override
  public void onCompleteClicked(StepperLayout.OnCompleteClickedCallback callback) {
  }

  @Override
  public void onBackClicked(final StepperLayout.OnBackClickedCallback callback) {
    callback.goToPrevStep();
  }

  @Override
  public VerificationError verifyStep() {
    return null;
  }

  @Override
  public void onSelected() {

  }

  @Override
  public void onError(@NonNull VerificationError error) {
  }

  @Override
  public void onResume() {
    super.onResume();
    requestNearestBranches();
  }
}
