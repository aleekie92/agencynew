package com.coretec.agencyApplication.fragments;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.coretec.agencyApplication.R;
import com.coretec.agencyApplication.activities.gfl.BalanceInquiry;
import com.coretec.agencyApplication.activities.gfl.MainGFLDashboard;
import com.coretec.agencyApplication.activities.gfl.MpesaChange;
import com.coretec.agencyApplication.activities.newmodules.ValidateTunzhengbigBio;
import com.coretec.agencyApplication.api.GFL;
import com.coretec.agencyApplication.api.requests.FingerPrintRequest;
import com.coretec.agencyApplication.api.requests.GrowersDetailsRequest;
import com.coretec.agencyApplication.api.requests.UpdateDetails;
import com.coretec.agencyApplication.api.responses.FingerPrintResponse;
import com.coretec.agencyApplication.api.responses.GrowersDetailsResponse;
import com.coretec.agencyApplication.api.responses.updateResponse;
import com.coretec.agencyApplication.dialog.BankDialog;
import com.coretec.agencyApplication.dialog.BankUpdateDialog;
import com.coretec.agencyApplication.dialog.SaccoDialog;
import com.coretec.agencyApplication.dialog.SaccoUpdateDialog;
import com.coretec.agencyApplication.model.GrowerDetails;
import com.coretec.agencyApplication.utils.QuickToast;
import com.coretec.agencyApplication.utils.SharedPrefs;
import com.stepstone.stepper.BlockingStep;
import com.stepstone.stepper.StepperLayout;
import com.stepstone.stepper.VerificationError;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static com.coretec.agencyApplication.utils.Utils.getFormattedDate;
import static com.coretec.agencyApplication.utils.Utils.getLat;
import static com.coretec.agencyApplication.utils.Utils.getLong;

public class CustomerUpdateDetailsFragment extends Fragment implements BlockingStep {

    private RadioGroup radioGroupDetails, radioGroupAccountDetails, radioGroupPU;
    private RadioButton rb1,rb2,rbimsi;
    private LinearLayout accountDetails, nextofkinDetails, puDetails;
    ProgressDialog progressDialog;
    private QuickToast toast = new QuickToast(getActivity());
    private ProgressBar details_progressbar, growersPb;
    String name, rship, puRegistered;
    private EditText full_names, et_kin_id, phone_number;
    private Spinner relSpinner;
    private TextView fname;

    private Button btn_done_on_next_of_kin;

    private String item, relitem;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.gfl_customer_details_update, container, false);
        SharedPrefs.init(getContext());
        SharedPrefs.write(SharedPrefs.FP_BOOLEAN, "");

        radioGroupDetails = v.findViewById(R.id.radioGroupDetails);
        radioGroupAccountDetails = v.findViewById(R.id.radioGroupAccountDetails);
        radioGroupPU = v.findViewById(R.id.radioGroupPU);

        accountDetails = v.findViewById(R.id.accountDetails);
        nextofkinDetails = v.findViewById(R.id.nextofkinDetails);
        puDetails = v.findViewById(R.id.puDetails);


        full_names = v.findViewById(R.id.full_names);
        et_kin_id = v.findViewById(R.id.et_kin_id);
        phone_number = v.findViewById(R.id.phone_number);
        relSpinner = v.findViewById(R.id.relSpinner);
        details_progressbar = v.findViewById(R.id.details_progressbar);
        growersPb = v.findViewById(R.id.growersPb);

        rb1 = v.findViewById(R.id.rb1);
        rb2 = v.findViewById(R.id.rb2);
        rbimsi = v.findViewById(R.id.rbimsi);
        btn_done_on_next_of_kin = v.findViewById(R.id.btn_done_on_next_of_kin);
        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage("Please wait...");
        progressDialog.setCancelable(false);

        radioGroupDetails.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                String identifier = SharedPrefs.read(SharedPrefs.IDENTIFIER_FOR_UPDATING, null);
                if (checkedId == R.id.rbbanksaccodetails) {
                    SharedPrefs.write(SharedPrefs.IMSIORNOT, "0");
                    accountDetails.setVisibility(View.VISIBLE);
                    nextofkinDetails.setVisibility(View.GONE);
                    puDetails.setVisibility(View.GONE);
                    rb1.setChecked(false);

                } else if (checkedId == R.id.rbnextofkin) {
                    SharedPrefs.write(SharedPrefs.IMSIORNOT, "0");
                    details_progressbar.setVisibility(View.VISIBLE);
                    requestGetAccountDetails(identifier);
                    accountDetails.setVisibility(View.GONE);//
                    nextofkinDetails.setVisibility(View.VISIBLE);
                    puDetails.setVisibility(View.GONE);
                    rb1.setChecked(false);

                    relSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                            relitem= String.valueOf(relSpinner.getSelectedItemPosition());
                            SharedPrefs.write(SharedPrefs.REG_RELATIONSHIP, relitem);

                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> parent) {

                        }
                    });

                    btn_done_on_next_of_kin.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            SharedPrefs.write(SharedPrefs.REG_FULL_NAME, full_names.getText().toString());
                            SharedPrefs.write(SharedPrefs.REG_KIN_ID, et_kin_id.getText().toString());
                            nextofkinDetails.setVisibility(View.GONE);
                        }
                    });
                } else if (checkedId == R.id.rbpuregistration) {

                    SharedPrefs.write(SharedPrefs.IMSIORNOT, "0");
                    puDetails.setVisibility(View.VISIBLE);
                    accountDetails.setVisibility(View.GONE);
                    nextofkinDetails.setVisibility(View.GONE);

                } else if (checkedId == R.id.rbmpesachange) {
                    accountDetails.setVisibility(View.GONE);
                    nextofkinDetails.setVisibility(View.GONE);
                    puDetails.setVisibility(View.GONE);
                    SharedPrefs.write(SharedPrefs.IMSIORNOT, "0");
                    startActivity(new Intent(getActivity(), MpesaChange.class));
                    rb1.setChecked(false);

                }
                else if (checkedId == R.id.rbimsi) {
                    accountDetails.setVisibility(View.GONE);
                    nextofkinDetails.setVisibility(View.GONE);
                    puDetails.setVisibility(View.GONE);
                    rb1.setChecked(false);

                    SharedPrefs.write(SharedPrefs.REG_FULL_NAME, "null");
                    SharedPrefs.write(SharedPrefs.REG_RELATIONSHIP, "null");
                    SharedPrefs.write(SharedPrefs.UPDATE_PU_REGISTERED, "false");
                    SharedPrefs.write(SharedPrefs.REG_BANK_DETAILS, "null");
                    SharedPrefs.write(SharedPrefs.REG_ACCOUNT_NO, "null");
                    SharedPrefs.write(SharedPrefs.REG_ATM_CARD, "null");
                    SharedPrefs.write(SharedPrefs.REG_SACCO_AC_NO, "null");
                    SharedPrefs.write(SharedPrefs.REG_SACCO_CODE, "null");
                    SharedPrefs.write(SharedPrefs.REG_SACOO_CARD, "null");
                    SharedPrefs.write(SharedPrefs.REG_KIN_ID, "null");
                    SharedPrefs.write(SharedPrefs.IMSIORNOT, "1");


                    progressDialog.show();
                     requestFingerPrints();
                    //submitUpdate();
                }
            }
        });

        radioGroupAccountDetails.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (checkedId == R.id.rbBank) {
                    startActivity(new Intent(getContext(), BankUpdateDialog.class));
                    puDetails.setVisibility(View.GONE);
                } else if (checkedId == R.id.rbSacco) {
                    startActivity(new Intent(getContext(), SaccoUpdateDialog.class));
                    puDetails.setVisibility(View.GONE);
                }
            }
        });

        radioGroupPU.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (checkedId == R.id.rb1) {
                    SharedPrefs.write(SharedPrefs.UPDATE_PU_REGISTERED, "true");
                }else if (checkedId == R.id.rb2){
                    SharedPrefs.write(SharedPrefs.UPDATE_PU_REGISTERED, "false");
                }
            }
        });

        return v;
    }

    @Override
    public void onNextClicked(StepperLayout.OnNextClickedCallback callback) {

    }


    @Override
    public void onCompleteClicked(StepperLayout.OnCompleteClickedCallback callback) {

        if (rbimsi.isChecked()){
            Toast.makeText(getActivity(), "Please wait", Toast.LENGTH_SHORT).show();
        }else {
            if (rb1.isChecked() || rb2.isChecked()) {
                progressDialog.show();
                requestFingerPrints();
                callback.complete();
            } else {
                Toast.makeText(getActivity(), "please select PU option", Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public void onBackClicked(StepperLayout.OnBackClickedCallback callback) {

        SharedPrefs.write(SharedPrefs.REG_IDNUMBER, "null");
        SharedPrefs.write(SharedPrefs.REG_GROWER_NO, "null");
        SharedPrefs.write(SharedPrefs.REG_FULL_NAME, "null");
        SharedPrefs.write(SharedPrefs.REG_RELATIONSHIP, "null");
        SharedPrefs.write(SharedPrefs.UPDATE_PU_REGISTERED, "false");
        SharedPrefs.write(SharedPrefs.REG_BANK_DETAILS, "null");
        SharedPrefs.write(SharedPrefs.REG_ACCOUNT_NO, "null");
        SharedPrefs.write(SharedPrefs.REG_ATM_CARD, "null");
        SharedPrefs.write(SharedPrefs.REG_SACCO_AC_NO, "null");
        SharedPrefs.write(SharedPrefs.REG_SACCO_CODE, "null");
        SharedPrefs.write(SharedPrefs.REG_SACOO_CARD, "null");
        SharedPrefs.write(SharedPrefs.REG_KIN_ID, "null");

        callback.goToPrevStep();
    }

    @Override
    public VerificationError verifyStep() {
        return null;
    }

    @Override
    public void onSelected() {

    }

    @Override
    public void onError(@NonNull VerificationError error) {

    }

    void requestFingerPrints() {
        final String TAG = "GET FINGERPRINTS";
        String URL = GFL.MSACCO_AGENT + GFL.GetFourFingerPrint;
        final FingerPrintRequest fingerPrintRequest = new FingerPrintRequest();
        fingerPrintRequest.accountidentifier = SharedPrefs.read(SharedPrefs.IDENTIFIER_FOR_UPDATING, null);
        fingerPrintRequest.accountidentifiercode = "1";
        fingerPrintRequest.agentid = SharedPrefs.read(SharedPrefs.AGENT_ID_1, null);
        fingerPrintRequest.terminalid =MainGFLDashboard.imei;
        fingerPrintRequest.longitude = getLong(getContext());
        fingerPrintRequest.latitude = getLat(getContext());
        fingerPrintRequest.date = getFormattedDate();

        Log.e(TAG, fingerPrintRequest.getBody().toString());

        GFL.instance(getContext()).request(URL, fingerPrintRequest, new GFL.RequestListener() {
            @Override
            public void onSuccess(String response) {
                FingerPrintResponse fingerPrintResponse = GFL.instance(getActivity())
                        .mGson.fromJson(response, FingerPrintResponse.class);
                if (fingerPrintResponse.is_successful) {
                    try {
                        JSONObject jsonObject = new JSONObject(response);

                        String leftRing = jsonObject.getString("leftring");
                        String leftLittle = jsonObject.getString("leftlittle");
                        String rightRing = jsonObject.getString("rightring");
                        String rightLittle = jsonObject.getString("rightlittle");

                        SharedPrefs.write(SharedPrefs.LEFT_RING, leftRing);
                        SharedPrefs.write(SharedPrefs.LEFT_LITTLE, leftLittle);
                        SharedPrefs.write(SharedPrefs.RIGHT_RING, rightRing);
                        SharedPrefs.write(SharedPrefs.RIGHT_LITTLE, rightLittle);


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    startActivity(new Intent(getActivity(), ValidateTunzhengbigBio.class));
                } else {
                    Toast.makeText(getActivity(), "Could not get customer finger prints!", Toast.LENGTH_SHORT).show();
                }


            }

            @Override
            public void onTokenExpired() {

            }

            @Override
            public void onError(String error) {

            }
        });

    }

    @Override
    public void onResume() {
        SharedPrefs.init(getContext());
        super.onResume();
        String fpBoolean = SharedPrefs.read(SharedPrefs.FP_BOOLEAN, null);

        if (fpBoolean.equals("true")) {
            progressDialog.dismiss();
            submitUpdate();
        } else if (fpBoolean.equals("false")) {
            progressDialog.dismiss();
            toast.swarn("Please authenticate to complete transaction!");
        }
    }

    private void submitUpdate() {

        String URL = GFL.MSACCO_AGENT + GFL.UpdateMemberDetails;
        SimpleDateFormat timeStampFormat = new SimpleDateFormat("yyyy/MM/dd - HH:mm");
        Date myDate = new Date();

        final UpdateDetails updateDetails = new UpdateDetails();
        updateDetails.idnumber = SharedPrefs.read(SharedPrefs.IDENTIFIER_FOR_UPDATING, null);
        updateDetails.bankandbranchcode = SharedPrefs.read(SharedPrefs.REG_BANK_DETAILS, null);
        updateDetails.bankaccountno = SharedPrefs.read(SharedPrefs.REG_ACCOUNT_NO, null);
        updateDetails.bankcardphoto = SharedPrefs.read(SharedPrefs.REG_ATM_CARD, null);
        updateDetails.saccoaccno = SharedPrefs.read(SharedPrefs.REG_SACCO_AC_NO, null);
        updateDetails.saccocode = SharedPrefs.read(SharedPrefs.REG_SACCO_CODE, null);
        updateDetails.saccocardphoto = SharedPrefs.read(SharedPrefs.REG_SACOO_CARD, null);
        updateDetails.nokname = SharedPrefs.read(SharedPrefs.REG_FULL_NAME, null);
        updateDetails.relationship = SharedPrefs.read(SharedPrefs.REG_RELATIONSHIP, null);
        updateDetails.nextofkinid = SharedPrefs.read(SharedPrefs.REG_KIN_ID, null);
        updateDetails.puregister = Boolean.valueOf(SharedPrefs.read(SharedPrefs.UPDATE_PU_REGISTERED,null));
        updateDetails.growerlist = SharedPrefs.read(SharedPrefs.REG_GROWER_NO, null);
        updateDetails.transactiontype = SharedPrefs.read(SharedPrefs.IMSIORNOT, null);
        updateDetails.agentid = SharedPrefs.read(SharedPrefs.AGENT_ID_1, null);
        updateDetails.terminalid = MainGFLDashboard.imei;//SharedPrefs.read(SharedPrefs.DEVICE_ID, null);
        updateDetails.longitude = getLong(getContext());
        updateDetails.latitude = getLat(getContext());
        updateDetails.date = getFormattedDate();
        updateDetails.corporate_no = "CAP016";

        GFL.instance(getContext()).request(URL, updateDetails, new GFL.RequestListener() {
            @Override
            public void onSuccess(String response) {
                updateResponse updateresponse = GFL.instance(getContext())
                        .mGson.fromJson(response, updateResponse.class);

                if (updateresponse.is_successful) {
                    String msg = "Member details updated successfully!";
                    AlertDialog.Builder builder1 = new AlertDialog.Builder(getContext());
                    builder1.setMessage(msg);
                    builder1.setTitle("Success!");
                    builder1.setCancelable(false);

                    builder1.setPositiveButton(
                            "DONE",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                    getActivity().finish();
                                    SharedPrefs.write(SharedPrefs.REG_IDNUMBER, "null");
                                    SharedPrefs.write(SharedPrefs.REG_GROWER_NO, "null");
                                    SharedPrefs.write(SharedPrefs.REG_FULL_NAME, "null");
                                    SharedPrefs.write(SharedPrefs.REG_RELATIONSHIP, "null");
                                    SharedPrefs.write(SharedPrefs.UPDATE_PU_REGISTERED, "false");
                                    SharedPrefs.write(SharedPrefs.REG_BANK_DETAILS, "null");
                                    SharedPrefs.write(SharedPrefs.REG_ACCOUNT_NO, "null");
                                    SharedPrefs.write(SharedPrefs.REG_ATM_CARD, "null");
                                    SharedPrefs.write(SharedPrefs.REG_SACCO_AC_NO, "null");
                                    SharedPrefs.write(SharedPrefs.REG_SACCO_CODE, "null");
                                    SharedPrefs.write(SharedPrefs.REG_SACOO_CARD, "null");
                                    SharedPrefs.write(SharedPrefs.REG_KIN_ID, "null");
                                }
                            });

                    AlertDialog alert11 = builder1.create();
                    alert11.show();

                } else {
                    progressDialog.dismiss();
                    Toast.makeText(getContext(), updateresponse.getError(), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onTokenExpired() {

            }

            @Override
            public void onError(String error) {

            }
        });
       // callback.complete();
    }

    void requestGetAccountDetails(String identifier) {

        final String TAG = "GrowersAccounts";

        String URL = GFL.MSACCO_AGENT + GFL.GetGrowersDetails;

        final GrowersDetailsRequest growersDetails = new GrowersDetailsRequest();
        growersDetails.agentid = SharedPrefs.read(SharedPrefs.AGENT_ID_1, null);
        growersDetails.corporateno = "CAP016";
        growersDetails.accountidentifier = identifier;
        growersDetails.accountidentifiercode = "1";
        growersDetails.transactiontype = "1";
        growersDetails.terminalid = SharedPrefs.read(SharedPrefs.DEVICE_ID, null);
        growersDetails.longitude = getLong(getContext());
        growersDetails.latitude = getLat(getContext());
        growersDetails.date = getFormattedDate();


        GFL.instance(getContext()).request(URL, growersDetails, new GFL.RequestListener() {
            @Override
            public void onSuccess(String response) {
                /*deposit_progressBar.setVisibility(View.GONE);*/

                GrowersDetailsResponse growersDetailsResponse = GFL.instance(getActivity())
                        .mGson.fromJson(response, GrowersDetailsResponse.class);
                if (growersDetailsResponse.is_successful) {
                    List<GrowerDetails> growerDetailsList = new ArrayList<>();

                    try {
                        JSONObject jsonObject = new JSONObject(response);

                        JSONArray jsonArray = jsonObject.getJSONArray("Growers");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                            JSONArray jsonArray1 = jsonObject1.getJSONArray("GrowerDetails");

                            for (int l = 0; l < jsonArray1.length(); l++) {
                                JSONObject jsonObject2 = jsonArray1.getJSONObject(l);
                                details_progressbar.setVisibility(View.GONE);

                                String gNo = jsonObject2.getString("GrowerNo");
                                String isDefaulter = jsonObject2.getString("LoanDefaulter");

                                full_names.setText(jsonObject2.getString("NextofKinFname"));
                            }

                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                } else {
//                    Toast.makeText(ShareDeposit.this, "Accounts Fetching Failed", Toast.LENGTH_LONG).show();
                }

            }

            @Override
            public void onTokenExpired() {

            }

            @Override
            public void onError(String error) {

            }
        });
    }

}
