package com.coretec.agencyApplication.fragments;

import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.coretec.agencyApplication.R;
import com.coretec.agencyApplication.api.Api;
import com.coretec.agencyApplication.api.GFL;
import com.coretec.agencyApplication.api.requests.NameRequest;
import com.coretec.agencyApplication.api.responses.NameResponse;
import com.coretec.agencyApplication.utils.QuickToast;
import com.coretec.agencyApplication.utils.SharedPrefs;
import com.coretec.agencyApplication.utils.Utils;
import com.stepstone.stepper.BlockingStep;
import com.stepstone.stepper.StepperLayout;
import com.stepstone.stepper.VerificationError;

public class AgenctFragment extends Fragment implements BlockingStep {

    private QuickToast toast = new QuickToast(getActivity());
    private EditText agent_code, registration_name,registration_id;
    ProgressBar progressBar,pbnext;
    private TextView status;
    String agentname;
    boolean hasbio=false;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.agentdetails_fragment_bioregistration, container, false);
        SharedPrefs.init(getContext());

        registration_name = v.findViewById(R.id.registration_name);
        registration_id = v.findViewById(R.id.registration_id);

        registration_name.setKeyListener(null);

        progressBar = v.findViewById(R.id.growersPb);
        pbnext = v.findViewById(R.id.pbnext);
        status = v.findViewById(R.id.status);

        agent_code = v.findViewById(R.id.agent_code);

        TextWatcher textWatcherId = new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }


            @Override
            public void afterTextChanged(Editable editable) {
                //here, after we introduced something in the EditText we get the string from it
                final String identifier_number = agent_code.getText().toString();

                if (agent_code.length() >= 7) {
                    progressBar.setVisibility(View.VISIBLE);
                    getAgentName(identifier_number);
                    progressBar.setVisibility(View.GONE);
                }
                if (agent_code.length() >= 5 && agent_code.length() < 7) {

                    status.setVisibility(View.GONE);
                    registration_name.setText(null);
                    progressBar.setVisibility(View.VISIBLE);
                    progressBar.getIndeterminateDrawable().setColorFilter(ContextCompat.getColor(getContext(), R.color.btn_green), PorterDuff.Mode.SRC_IN);
                }
                if (identifier_number.length() > 4) {
                    agent_code.setOnKeyListener(new View.OnKeyListener() {
                        @Override
                        public boolean onKey(View v, int keyCode, KeyEvent event) {
                            //You can identify which key pressed buy checking keyCode value with KeyEvent.KEYCODE_
                            if (keyCode == KeyEvent.KEYCODE_DEL) {
                                //this is for backspace

                            }
                            return false;
                        }
                    });

                }
            }
        };
        agent_code.addTextChangedListener(textWatcherId);

        return  v;
    }
    @Override
    public void onNextClicked(final StepperLayout.OnNextClickedCallback callback) {

        if (agent_code.getText().toString().matches("")) {
            Toast.makeText(getActivity(), "Please enter agent code", Toast.LENGTH_SHORT).show();
        } else if (registration_name.getText().toString().matches("")) {
            Toast.makeText(getActivity(), "Please wait....", Toast.LENGTH_SHORT).show();
        } else if (registration_id.getText().toString().matches("")) {
            Toast.makeText(getActivity(), "Please enter agents ID number", Toast.LENGTH_SHORT).show();
        } else {
            checkHasBio(agent_code.getText().toString(), callback);
        }
    }

    @Override
    public void onCompleteClicked(StepperLayout.OnCompleteClickedCallback callback) {

    }

    @Override
    public void onBackClicked(final StepperLayout.OnBackClickedCallback callback) {
        callback.goToPrevStep();
    }

    @Override
    public VerificationError verifyStep() {
        return null;
    }

    @Override
    public void onSelected() {

    }

    @Override
    public void onError(@NonNull VerificationError error) {

    }
    public void checkHasBio(String agentId, final StepperLayout.OnNextClickedCallback callback){
        String URL = Api.MSACCO_AGENT + GFL.AgentHasBio;
        final NameRequest nameRequest = new NameRequest();
        nameRequest.agentid = agentId;
        nameRequest.terminalid = SharedPrefs.read(SharedPrefs.DEVICE_ID,  null);

        Api.instance(getActivity()).request(URL, nameRequest, new Api.RequestListener() {
            @Override
            public void onSuccess(String response) {
                pbnext.setVisibility(View.VISIBLE);
                NameResponse nameResponse = Api.instance(getActivity()).mGson.fromJson(response, NameResponse.class);
                if (nameResponse.isHasbio()) {
                    pbnext.setVisibility(View.GONE);
                    Utils.showAlertDialog(getActivity(),"Alert!", "Agent is already bio registered");
                } else {
                    pbnext.setVisibility(View.GONE);
                    SharedPrefs.write(SharedPrefs.AGENT_ID_NO, registration_id.getText().toString());
                    SharedPrefs.write(SharedPrefs.BIO_AGENT_CODE, agent_code.getText().toString());
                    callback.goToNextStep();
                }
            }

            @Override
            public void onTokenExpired() {

            }

            @Override
            public void onError(String error) {

            }
        });
    }
    public void getAgentName (String agentId) {
        final String TAG = "AGENT NAME";
        String URL = Api.MSACCO_AGENT + GFL.GetAgentname;
        final NameRequest nameRequest = new NameRequest();
        nameRequest.agentid = agentId;
        nameRequest.terminalid = SharedPrefs.read(SharedPrefs.DEVICE_ID,  null);

        Log.e(TAG, nameRequest.getBody().toString());

        Api.instance(getActivity()).request(URL, nameRequest, new Api.RequestListener() {
            @Override
            public void onSuccess(String response) {

                NameResponse nameResponse = Api.instance(getActivity()).mGson.fromJson(response, NameResponse.class);
                if (nameResponse.is_successful) {
                    agentname=nameResponse.getAgent_name();
                    registration_name.setText(agentname);
                } else {
                    toast.swarn("Could not get agent name!");
                }
            }

            @Override
            public void onTokenExpired() {

            }

            @Override
            public void onError(String error) {

            }
        });

    }
}
