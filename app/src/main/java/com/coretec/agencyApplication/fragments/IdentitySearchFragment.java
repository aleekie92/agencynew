package com.coretec.agencyApplication.fragments;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.cloudpos.jniinterface.PrinterInterface;
import com.coretec.agencyApplication.R;
import com.coretec.agencyApplication.activities.gfl.BalanceInquiry;
import com.coretec.agencyApplication.activities.newmodules.ValidateTunzhengbigBio;
import com.coretec.agencyApplication.api.GFL;
import com.coretec.agencyApplication.api.requests.FingerPrintRequest;
import com.coretec.agencyApplication.api.requests.GrowersDetailsRequest;
import com.coretec.agencyApplication.api.responses.FingerPrintResponse;
import com.coretec.agencyApplication.api.responses.GrowersDetailsResponse;
import com.coretec.agencyApplication.model.GrowerDetails;
import com.coretec.agencyApplication.utils.QuickToast;
import com.coretec.agencyApplication.utils.SharedPrefs;
import com.stepstone.stepper.BlockingStep;
import com.stepstone.stepper.StepperLayout;
import com.stepstone.stepper.VerificationError;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import static com.coretec.agencyApplication.utils.Utils.getFormattedDate;
import static com.coretec.agencyApplication.utils.Utils.getLat;
import static com.coretec.agencyApplication.utils.Utils.getLong;

public class IdentitySearchFragment extends Fragment implements BlockingStep {
    private EditText registration_name, registration_id;
    ProgressBar searchPb;
    String customername;
    private LinearLayout layoutgrowers;
    private Spinner growerSpinner;
    private EditText grower_numbers;
    String item, text, identifiercode;

    private ArrayList<String> arrayList = new ArrayList<>();
    private ArrayAdapter<GrowerDetails> adapter;
    private String canregister, rejectionreason, bssregistered, statuss, blocked;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.gfl_identity_search_for_update, container, false);
        SharedPrefs.init(getContext());
        SharedPrefs.write(SharedPrefs.FP_BOOLEAN, "");

        registration_id = v.findViewById(R.id.id_number);
        registration_name = v.findViewById(R.id.registration_name);
        registration_name.setKeyListener(null);
        registration_id.setInputType(InputType.TYPE_NULL);
        searchPb = v.findViewById(R.id.growersPb);
        layoutgrowers = v.findViewById(R.id.layoutgrowers);
        growerSpinner = v.findViewById(R.id.growerSpinner);
        grower_numbers = v.findViewById(R.id.grower_numbers);

        bssregistered="";

        TextWatcher txtwatcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                final String identifier_number = registration_id.getText().toString();

                if (registration_id.length() >= 7) {
                    searchPb.setVisibility(View.VISIBLE);
                    identifiercode = "1";
                    requestGetAccountDetails(identifier_number);
                    searchPb.setVisibility(View.GONE);
                }
                if (registration_id.length() >= 5 && registration_id.length() < 7) {
                    registration_name.setText(null);
                    grower_numbers.setText(null);
                    searchPb.setVisibility(View.VISIBLE);

                    arrayList.clear();
                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, arrayList);
                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    growerSpinner.setAdapter(null);

                    searchPb.getIndeterminateDrawable().setColorFilter(ContextCompat.getColor(getContext(), R.color.btn_green), PorterDuff.Mode.SRC_IN);

                }
                if (identifier_number.length() > 4) {
                    registration_id.setOnKeyListener(new View.OnKeyListener() {
                        @Override
                        public boolean onKey(View v, int keyCode, KeyEvent event) {
                            //You can identify which key pressed buy checking keyCode value with KeyEvent.KEYCODE_
                            if (keyCode == KeyEvent.KEYCODE_DEL) {
                                //this is for backspace

                            }
                            return false;
                        }
                    });

                }
            }
        };

        registration_id.addTextChangedListener(txtwatcher);

        adapter = new ArrayAdapter<GrowerDetails>(getActivity(),
                R.layout.simple_spinner_dropdown_item, new ArrayList<GrowerDetails>());
        adapter.setDropDownViewResource(R.layout.simple_spinner_dropdown_item);
        growerSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                item = parent.getItemAtPosition(position).toString();
                text = growerSpinner.getSelectedItem().toString();
                Log.d("text","text"+text);
                requestCheckStatus(text);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        return v;
    }

    @Override
    public void onNextClicked(StepperLayout.OnNextClickedCallback callback) {
        //Log.d("bssreg","bssreg"+bssregistered);
        switch (bssregistered) {
            case "Yes":
                if (statuss.equals("Active")) {
                    if (registration_id.getText().toString().matches("")) {
                        Toast.makeText(getActivity(), "Please enter Id number", Toast.LENGTH_SHORT).show();
                    } else if (registration_name.getText().toString().matches("")) {
                        Toast.makeText(getActivity(), "Please wait....", Toast.LENGTH_SHORT).show();
                    } else if (text.isEmpty() || text == "-Select-") {
                        Toast.makeText(getActivity(), "please select grower number", Toast.LENGTH_SHORT).show();
                    } else {

                        SharedPrefs.identifierforupdating(registration_id.getText().toString(), getActivity());
                        SharedPrefs.write(SharedPrefs.REG_GROWER_NO, growerSpinner.getSelectedItem().toString());
                        callback.goToNextStep();
                    }
                } else {
                    Toast.makeText(getContext(), "Customer not active", Toast.LENGTH_LONG).show();
                }
                break;

            case "No":
                Toast.makeText(getContext(), "can not update", Toast.LENGTH_LONG).show();
                break;

            default:
                Toast.makeText(getContext(), "Enter a valid ID number to continue", Toast.LENGTH_LONG).show();
                break;
        }
    }

    @Override
    public void onCompleteClicked(StepperLayout.OnCompleteClickedCallback callback) {

    }

    @Override
    public void onBackClicked(StepperLayout.OnBackClickedCallback callback) {
        callback.goToPrevStep();
    }

    @Override
    public VerificationError verifyStep() {
        return null;
    }

    @Override
    public void onSelected() {

    }

    @Override
    public void onError(@NonNull VerificationError error) {

    }

    @Override
    public void onResume() {
        super.onResume();
    }

    void requestGetAccountDetails(String identifier) {
        final String TAG = "GrowersAccounts";
        String URL = GFL.MSACCO_AGENT + GFL.GetGrowersDetails;

        final GrowersDetailsRequest growersDetails = new GrowersDetailsRequest();
        growersDetails.agentid = SharedPrefs.read(SharedPrefs.AGENT_ID_1, null);
        growersDetails.corporateno = "CAP016";
        growersDetails.accountidentifier = identifier;
        growersDetails.accountidentifiercode = identifiercode;// "1";
        growersDetails.transactiontype = "1";
        growersDetails.terminalid = SharedPrefs.read(SharedPrefs.DEVICE_ID, null);
        growersDetails.longitude = getLong(getContext());
        growersDetails.latitude = getLat(getContext());
        growersDetails.date = getFormattedDate();

        Log.e(TAG, growersDetails.getBody().toString());
        arrayList.clear();
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, arrayList);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        growerSpinner.setAdapter(null);

        GFL.instance(getContext()).request(URL, growersDetails, new GFL.RequestListener() {
            @Override
            public void onSuccess(String response) {
                /*deposit_progressBar.setVisibility(View.GONE);*/

                GrowersDetailsResponse growersDetailsResponse = GFL.instance(getActivity())
                        .mGson.fromJson(response, GrowersDetailsResponse.class);
                if (growersDetailsResponse.is_successful) {
                    List<GrowerDetails> growerDetailsList = new ArrayList<>();

                    try {
                        JSONObject jsonObject = new JSONObject(response);

                        JSONArray jsonArray = jsonObject.getJSONArray("Growers");
                        arrayList.clear();
                        arrayList.add("-Select-");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                            JSONArray jsonArray1 = jsonObject1.getJSONArray("GrowerDetails");
                            for (int l = 0; l < jsonArray1.length(); l++) {
                                JSONObject jsonObject2 = jsonArray1.getJSONObject(l);
                                searchPb.setVisibility(View.GONE);
                                arrayList.add(String.valueOf(jsonObject2.getString("GrowerNo")));
                                registration_name.setText(jsonObject2.getString("GrowerName"));
                            }

                        }
                        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, arrayList);
                        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        growerSpinner.setAdapter(adapter);


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                } else {
                     Toast.makeText(getActivity(), "Accounts Fetching Failed", Toast.LENGTH_LONG).show();
                }

            }

            @Override
            public void onTokenExpired() {

            }

            @Override
            public void onError(String error) {

            }
        });
    }

    void requestCheckStatus(String identifier) {
        final String TAG = "GrowersAccounts";
        String URL = GFL.MSACCO_AGENT + GFL.GetGrowersDetails;

        final GrowersDetailsRequest growersDetails = new GrowersDetailsRequest();
        growersDetails.agentid = SharedPrefs.read(SharedPrefs.AGENT_ID_1, null);
        growersDetails.corporateno = "CAP016";
        growersDetails.accountidentifier = identifier;
        growersDetails.accountidentifiercode = "2";// "1";
        growersDetails.transactiontype = "1";
        growersDetails.terminalid = SharedPrefs.read(SharedPrefs.DEVICE_ID, null);
        growersDetails.longitude = getLong(getContext());
        growersDetails.latitude = getLat(getContext());
        growersDetails.date = getFormattedDate();

        GFL.instance(getContext()).request(URL, growersDetails, new GFL.RequestListener() {
            @Override
            public void onSuccess(String response) {
                /*deposit_progressBar.setVisibility(View.GONE);*/

                GrowersDetailsResponse growersDetailsResponse = GFL.instance(getActivity())
                        .mGson.fromJson(response, GrowersDetailsResponse.class);
                if (growersDetailsResponse.is_successful) {

                    try {
                        JSONObject jsonObject = new JSONObject(response);

                        JSONArray jsonArray = jsonObject.getJSONArray("Growers");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                            JSONArray jsonArray1 = jsonObject1.getJSONArray("GrowerDetails");
                            for (int l = 0; l < jsonArray1.length(); l++) {
                                JSONObject jsonObject2 = jsonArray1.getJSONObject(l);

                                searchPb.setVisibility(View.GONE);
                                //canregister = jsonObject2.getString("isPendingReg");
                                //rejectionreason = jsonObject2.getString("RegrejectReason");
                                bssregistered = jsonObject2.getString("BssRegistered");
                                statuss = jsonObject2.getString("Status");
                                blocked = jsonObject2.getString("Blocked");
                                Log.d("seee","seee---->"+bssregistered);

                            }

                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                } else {
//                    Toast.makeText(ShareDeposit.this, "Accounts Fetching Failed", Toast.LENGTH_LONG).show();
                }

            }

            @Override
            public void onTokenExpired() {

            }

            @Override
            public void onError(String error) {

            }
        });
    }
}
