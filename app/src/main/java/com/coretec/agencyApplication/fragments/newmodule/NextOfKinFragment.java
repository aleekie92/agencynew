package com.coretec.agencyApplication.fragments.newmodule;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.cloudpos.jniinterface.PrinterInterface;
import com.coretec.agencyApplication.R;
import com.coretec.agencyApplication.activities.LoginActivity;
import com.coretec.agencyApplication.activities.MiniStatement;
import com.coretec.agencyApplication.activities.newmodules.MainDashboardActivity;
import com.coretec.agencyApplication.utils.PreferenceFileKeys;
import com.coretec.agencyApplication.utils.SharedPrefs;
import com.coretec.agencyApplication.utils.Utils;
import com.coretec.agencyApplication.wizarpos.function.printer.PrinterCommand;
import com.stepstone.stepper.BlockingStep;
import com.stepstone.stepper.StepperLayout;
import com.stepstone.stepper.VerificationError;

import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class NextOfKinFragment extends Fragment implements BlockingStep {
    LinearLayout bankAtm;
    //LinearLayout layout_languages;
    private static final int CAMERA_REQUEST = 1888;
    private ImageView identity_imageView;
    private EditText fullNames, kin_id,kin_phone;
    private Spinner spinner,kingenderspinner;
    private RadioGroup radioGroup, radioGroupAccount;
    private RadioButton yesBtn, noBtn;
    private  LinearLayout nextofkinpage;
    boolean puRegistered = false;
    TextView date,coplete;
    SharedPreferences sharedPreferences;
    DatePickerDialog datePickerDialog;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.activity_fragment_next_ofkin, container, false);
        SharedPrefs.init(getContext());
        sharedPreferences = getActivity().getSharedPreferences(PreferenceFileKeys.PREFS_FILE, Context.MODE_PRIVATE);
        nextofkinpage = v.findViewById(R.id.nextofkinpage);
        coplete = v.findViewById(R.id.coplete);

        if (MainDashboardActivity.corporateno.equals("CAP021")){
            nextofkinpage.setVisibility(View.GONE);
            SharedPrefs.rship("Relative",getActivity());
            SharedPrefs.kindob("01/01/2000",getActivity());
            SharedPrefs.kingender("Gender",getActivity());
            SharedPrefs.msaccoreg(false,getActivity());
            coplete.setVisibility(View.VISIBLE);
        }else {
            nextofkinpage.setVisibility(View.VISIBLE);
            coplete.setVisibility(View.GONE);
        }

        fullNames = v.findViewById(R.id.full_names);
        kin_id = v.findViewById(R.id.et_kin_id);
        kin_phone = v.findViewById(R.id.kin_phone);


        date =  v.findViewById(R.id.kindob);
        date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // calender class's instance and get current date , month and year from calender
                final Calendar c = Calendar.getInstance();
                int mYear = c.get(Calendar.YEAR); // current year
                int mMonth = c.get(Calendar.MONTH); // current month
                int mDay = c.get(Calendar.DAY_OF_MONTH); // current day
                // date picker dialog
                datePickerDialog = new DatePickerDialog(getContext(),
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {
                                // set day of month , month and year value in the edit text
                                date.setText(dayOfMonth + "/"
                                        + (monthOfYear + 1) + "/" + year);
                                String dateSelected = date.getText().toString();
                                if (MainDashboardActivity.corporateno.equals("CAP021")){
                                    SharedPrefs.kindob("01/01/2000",getActivity());
                                }else {
                                    SharedPrefs.kindob(dateSelected,getActivity());
                                }
                            }
                        }, mYear, mMonth, mDay);
                datePickerDialog.show();
                datePickerDialog.updateDate(2001, 1, 1);
            }
        });

        kingenderspinner = v.findViewById(R.id.kingenderspinner);
        kingenderspinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                String text = kingenderspinner.getSelectedItem().toString();

                if (MainDashboardActivity.corporateno.equals("CAP021")){
                    SharedPrefs.kingender("Gender",getActivity());
                }else {
                    SharedPrefs.kingender(text,getActivity());
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        kin_id.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // showMyDialog();
            }
        });

        kin_id.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    // showMyDialog();
                }
            }
        });

        kin_phone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // showMyDialog();
            }
        });

        kin_phone.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    // showMyDialog();
                }
            }
        });
        kin_phone.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    // showMyDialog();
                }
            }
        });

        final TextWatcher textWatcher1 = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                String inputPhone = kin_phone.getText().toString();

                if (inputPhone.length() == 1) {
                    if (inputPhone.equals("0") || inputPhone.equals("7") || inputPhone.equals("+")) {
                        kin_phone.setText(SharedPrefs.read(SharedPrefs.VR_CITIZEN, null));
                        kin_phone.setSelection(kin_phone.getText().length());
                        Toast.makeText(getContext(), "Correct format auto completed. Please proceed!", Toast.LENGTH_SHORT).show();
                    } else {
                        kin_phone.setText(null);
                        Toast.makeText(getContext(), "Please enter correct phone number format!", Toast.LENGTH_SHORT).show();
                    }
                }

            }
        };
        kin_phone.addTextChangedListener(textWatcher1);

        //layout_languages = v.findViewById(R.id.linearLanguages);
        radioGroup = v.findViewById(R.id.radioGroup);
        radioGroupAccount = v.findViewById(R.id.radioGroupAccount);
        yesBtn = v.findViewById(R.id.rb1);
        noBtn = v.findViewById(R.id.rb2);

        spinner = v.findViewById(R.id.relSpinner);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String item = parent.getItemAtPosition(position).toString();
                String text = spinner.getSelectedItem().toString();

                if (MainDashboardActivity.corporateno.equals("CAP021")){
                    SharedPrefs.rship("Relative",getActivity());
                }else {
                    if (text.equals("relationship")) {
                        SharedPrefs.rship("",getActivity());
                    } else {
                        SharedPrefs.rship(text,getActivity());
                    }
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (MainDashboardActivity.corporateno.equals("CAP021")){
                    SharedPrefs.msaccoreg(false,getActivity());
                }else {
                    if (checkedId == R.id.rb1) {
                        SharedPrefs.msaccoreg(true,getActivity());
                    } else if (checkedId == R.id.rb2) {
                        SharedPrefs.msaccoreg(false,getActivity());
                    }
                }

            }
        });
        return v;
    }


    @Override
    public void onNextClicked(final StepperLayout.OnNextClickedCallback callback) {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                final String idOfKin = kin_id.getText().toString();
                final String phoneOfKin = kin_phone.getText().toString();

                String s_output="";
                try {
                    if (phoneOfKin.length() == 13) {
                        s_output = phoneOfKin.substring(4, 13);
                    }
                }catch (Exception e){

                }

                if (MainDashboardActivity.corporateno.equals("CAP021")){
                    SharedPrefs.nextofkin("KIN NAME", "11223344", "722222222", getActivity());
                    String msg = "Do you want to print a receipt with member registration details?";
                    android.app.AlertDialog.Builder builder1 = new android.app.AlertDialog.Builder(getContext());
                    builder1.setMessage(msg);
                    builder1.setTitle("Alert!");
                    builder1.setCancelable(false);

                    builder1.setPositiveButton(
                            "YES",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    PrinterInterface.open();
                                    writetest();
                                    PrinterInterface.close();
                                    callback.goToNextStep();
                                }

                            });

                    builder1.setNegativeButton(
                            "no",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                    callback.goToNextStep();
                                }
                            });

                    android.app.AlertDialog alert11 = builder1.create();
                    alert11.show();
                }else {
                    if (fullNames.getText().toString().matches("")) {
                        Toast.makeText(getContext(), "Please fill in the next of kin name!", Toast.LENGTH_SHORT).show();
                    } else if (kingenderspinner.getSelectedItem().toString().trim().equals("SELECT GENDER")) {
                        Toast.makeText(getActivity(), "Please select Gender", Toast.LENGTH_LONG).show();
                    }else if (date.getText().toString().trim().equals("Select Date")) {
                        Toast.makeText(getActivity(), "Please select next of kin date of birth", Toast.LENGTH_LONG).show();
                    }else if (kin_phone.getText().toString().matches("")) {
                        Toast.makeText(getActivity(), "Please select next of kin phone number", Toast.LENGTH_LONG).show();
                    }else if (spinner.getSelectedItem().toString().trim().equals("relationship")) {
                        Toast.makeText(getActivity(), "Please select Relationship", Toast.LENGTH_LONG).show();
                    }
                    else {
                        SharedPrefs.nextofkin(fullNames.getText().toString(), idOfKin, s_output, getActivity());
                        String msg = "Do you want to print a receipt with member registration details?";
                        android.app.AlertDialog.Builder builder1 = new android.app.AlertDialog.Builder(getContext());
                        builder1.setMessage(msg);
                        builder1.setTitle("Alert!");
                        builder1.setCancelable(false);

                        builder1.setPositiveButton(
                                "YES",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        PrinterInterface.open();
                                        writetest();
                                        PrinterInterface.close();
                                        callback.goToNextStep();
                                    }

                                });

                        builder1.setNegativeButton(
                                "no",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();
                                        callback.goToNextStep();
                                    }
                                });

                        android.app.AlertDialog alert11 = builder1.create();
                        alert11.show();
                    }
                }
            }
        }, 0L);
    }

    @Override
    public void onCompleteClicked(StepperLayout.OnCompleteClickedCallback callback) {

    }

    @Override
    public void onBackClicked(final StepperLayout.OnBackClickedCallback callback) {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                callback.goToPrevStep();
            }
        }, 0L);
    }

    @Override
    public VerificationError verifyStep() {
        return null;
    }

    @Override
    public void onSelected() {
    }

    @Override
    public void onError(@NonNull VerificationError error) {
    }

    public void writetest() {
        try {
            byte[] start = null;
            byte[] saccoName = null;
            byte[] terminalNo = null;//
            byte[] type = null;
            byte[] gno = null;
            byte[] accName = null;
            byte[] idNumber = null;
            byte[] phoneNumber = null;

            byte[] genderr = null;//
            byte[] localityy = null;//
            byte[] nearestbranch = null;//
            byte[] nearestmarket = null;//

            byte[] nok = null;
            byte[] relationship = null;

            byte[] nokid = null;//

            byte[] puReg = null;

            byte[] time = null;
            byte[] nameAgent = null;
            byte[] motto = null;

            SimpleDateFormat timeStampFormat = new SimpleDateFormat("yyyy/MM/dd - HH:mm");
            Date myDate = new Date();
            String timeCurrent = timeStampFormat.format(myDate);
            String aName = SharedPrefs.read(SharedPrefs.AGENT_NAME, null);
            boolean puRegistered = SharedPrefs.read("PU_REGISTERED", SharedPrefs.VR_PU_REGISTERED);
            String saccode="";
            String sacco_acc="";
            String bank_details="";
            String bankAcc="";

            try {
                start = String.valueOf("              ").getBytes("GB2312");
                saccoName = String.valueOf(SharedPrefs.read(SharedPrefs.NEW_SACCO_NAME, null) + " : REGISTRATION.").getBytes("GB2312");
                terminalNo = String.valueOf(" Terminal ID  " + Utils.getIMEI(getActivity())).getBytes("GB2312");
                type = String.valueOf("     REGISTRATION RECEIPT     ").getBytes("GB2312");
                accName = String.valueOf("Account Name : " + SharedPrefs.read(SharedPrefs.VR_NAMES, null)).getBytes("GB2312");
                idNumber = String.valueOf("ID Number : " + SharedPrefs.read(SharedPrefs.VR_ID, null)).getBytes("GB2312");
                phoneNumber = String.valueOf("Phone Number : " + SharedPrefs.read(SharedPrefs.VR_PHONE, null)).getBytes("GB2312");

                genderr = String.valueOf("Gender : " + SharedPrefs.read(SharedPrefs.VR_GENDER, null)).getBytes("GB2312");
                localityy = String.valueOf("Locality : " + SharedPrefs.read(SharedPrefs.VR_LOCALITY, null)).getBytes("GB2312");
                nearestbranch = String.valueOf("Nearest Branch : " + SharedPrefs.read(SharedPrefs.VR_BRANCH, null)).getBytes("GB2312");
                nearestmarket = String.valueOf("Nearest Market : " + SharedPrefs.read(SharedPrefs.VR_NEARESTMARKET, null)).getBytes("GB2312");

                nokid = String.valueOf("Next of Kin ID : " + SharedPrefs.read(SharedPrefs.VR_REG_FULL_ID, null)).getBytes("GB2312");
                nok = String.valueOf("Next Of Kin : " + SharedPrefs.read(SharedPrefs.VR_REG_FULL_NAME, null)).getBytes("GB2312");
                relationship = String.valueOf("Relationship : " + SharedPrefs.read(SharedPrefs.VR_REG_RELATIONSHIP, null)).getBytes("GB2312");
                puReg = String.valueOf("Msacco Registered : " + SharedPrefs.read("VR_PU_REGISTERED", SharedPrefs.VR_PU_REGISTERED)).getBytes("GB2312");
                time = String.valueOf("   Date : " + String.valueOf(timeCurrent)).getBytes("GB2312");
                nameAgent = String.valueOf("You were served by " + sharedPreferences.getString(PreferenceFileKeys.AGENT_ACCOUNT_NAME, "")).getBytes("GB2312");
                motto = String.valueOf("  ").getBytes("GB2312");

            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            PrinterInterface.begin();
            byte[] cmd;
            cmd = new byte[]{(byte) 0x1B, (byte) 0x40};

            cmd = new byte[]{
                    (byte) 0x1B, (byte) 0x45, (byte) 0};

            write(cmd);
            writeLineBreak(2);
            write(start);
            writeLineBreak(2);
            write(saccoName);
            writeLineBreak(2);
            write(terminalNo);
            writeLineBreak(2);
            write(type);
            writeLineBreak(2);
            write(accName);
            writeLineBreak(2);
            write(idNumber);
            writeLineBreak(2);
            write(phoneNumber);
            writeLineBreak(2);

            write(genderr);
            writeLineBreak(2);
            write(localityy);
            writeLineBreak(2);
            write(nearestbranch);
            writeLineBreak(2);

            if (!MainDashboardActivity.corporateno.equals("CAP021")) {
                write(nearestmarket);
                writeLineBreak(2);
                write(nokid);
                writeLineBreak(2);
                write(nok);
                writeLineBreak(2);
                write(relationship);
                writeLineBreak(2);

                if (puRegistered) {
                    write(puReg);
                    writeLineBreak(1);
                }
            }


            write(time);
            writeLineBreak(2);
            write(nameAgent);
            writeLineBreak(1);
            write(motto);
            // print line break
            writeLineBreak(4);
            PrinterInterface.end();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void write(final byte[] arryData) {
        int result = PrinterInterface.write(arryData, arryData.length);
    }
    // print line break
    private void writeLineBreak(int lineNumber) {
        // write(new byte[]{'\n'});
        write(PrinterCommand.getCmdEscDN(lineNumber));
    }

}
