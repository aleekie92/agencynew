package com.coretec.agencyApplication.fragments.newmodule;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.coretec.agencyApplication.R;
import com.coretec.agencyApplication.utils.SharedPrefs;
import com.stepstone.stepper.BlockingStep;
import com.stepstone.stepper.StepperLayout;
import com.stepstone.stepper.VerificationError;

import java.io.ByteArrayOutputStream;
public class LoanFormFrontPhotoFragment extends Fragment implements BlockingStep {

    private CardView card_loanformfront_photo;
    private static final int CAMERA_REQUEST = 1888;
    private ImageView loanformfront_imageView;
    private Button submitPassportPhoto;
    private  Button btnAdd;
    ProgressDialog progressDialog;
    private int countclicks=0;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.loanformfront, container, false);
        SharedPrefs.init(getContext());

        loanformfront_imageView =  v.findViewById(R.id.imageview_lformfront);

        card_loanformfront_photo =  v.findViewById(R.id.card_lformfront_photo);
        btnAdd =  v.findViewById(R.id.btnAdd);

        card_loanformfront_photo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(cameraIntent, CAMERA_REQUEST);
            }
        });

        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                countclicks=countclicks+1;
                Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(cameraIntent, CAMERA_REQUEST);
            }
        });
        return  v;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case CAMERA_REQUEST:
                if (resultCode == Activity.RESULT_OK) {

                    try {
                        if (countclicks==0) {
                            Bitmap photo = (Bitmap) data.getExtras().get("data");
                            loanformfront_imageView.setImageBitmap(photo);

                            String defaultp = "iVBORw0KGgoAAAANSUhEUgAAASkAAAF/CAIAAACaLKg/AAAAA3NCSVQICAjb4U/gAAAgAElEQ";
                            Bitmap img = ((BitmapDrawable) loanformfront_imageView.getDrawable()).getBitmap();
                            final String encodedImage = bitmapToBase64(img);
                            String testImage = encodedImage.substring(0,73);

                            if (testImage.equals(defaultp)) {
                                Toast.makeText(getContext(), "Click to capture filled front page photo!", Toast.LENGTH_SHORT).show();
                            } else {
                                SharedPrefs.loanleavephoto1(encodedImage, getActivity());
                            }

                        }

                        else if (countclicks==1) {
                            Bitmap photo = (Bitmap) data.getExtras().get("data");
                            loanformfront_imageView.setImageBitmap(photo);

                            String defaultp = "iVBORw0KGgoAAAANSUhEUgAAASkAAAF/CAIAAACaLKg/AAAAA3NCSVQICAjb4U/gAAAgAElEQ";
                            Bitmap img = ((BitmapDrawable) loanformfront_imageView.getDrawable()).getBitmap();
                            final String encodedImage = bitmapToBase64(img);
                            String testImage = encodedImage.substring(0,73);

                            if (testImage.equals(defaultp)) {
                                Toast.makeText(getContext(), "Click to capture filled front page photo!", Toast.LENGTH_SHORT).show();
                            } else {
                                SharedPrefs.loanleavephoto2(encodedImage, getActivity());
                            }

                        }

                        else if (countclicks==2) {
                            Bitmap photo = (Bitmap) data.getExtras().get("data");
                            loanformfront_imageView.setImageBitmap(photo);

                            String defaultp = "iVBORw0KGgoAAAANSUhEUgAAASkAAAF/CAIAAACaLKg/AAAAA3NCSVQICAjb4U/gAAAgAElEQ";
                            Bitmap img = ((BitmapDrawable) loanformfront_imageView.getDrawable()).getBitmap();
                            final String encodedImage = bitmapToBase64(img);
                            String testImage = encodedImage.substring(0,73);

                            if (testImage.equals(defaultp)) {
                                Toast.makeText(getContext(), "Click to capture filled front page photo!", Toast.LENGTH_SHORT).show();
                            } else {
                                SharedPrefs.loanleavephoto3(encodedImage, getActivity());
                            }
                        }
                        else if (countclicks==3) {
                            Bitmap photo = (Bitmap) data.getExtras().get("data");
                            loanformfront_imageView.setImageBitmap(photo);

                            String defaultp = "iVBORw0KGgoAAAANSUhEUgAAASkAAAF/CAIAAACaLKg/AAAAA3NCSVQICAjb4U/gAAAgAElEQ";
                            Bitmap img = ((BitmapDrawable) loanformfront_imageView.getDrawable()).getBitmap();
                            final String encodedImage = bitmapToBase64(img);
                            String testImage = encodedImage.substring(0,73);

                            if (testImage.equals(defaultp)) {
                                Toast.makeText(getContext(), "Click to capture filled front page photo!", Toast.LENGTH_SHORT).show();
                            } else {
                                SharedPrefs.loanleavephoto4(encodedImage, getActivity());
                            }
                        }
                        else if (countclicks==4) {
                            Bitmap photo = (Bitmap) data.getExtras().get("data");
                            loanformfront_imageView.setImageBitmap(photo);

                            String defaultp = "iVBORw0KGgoAAAANSUhEUgAAASkAAAF/CAIAAACaLKg/AAAAA3NCSVQICAjb4U/gAAAgAElEQ";
                            Bitmap img = ((BitmapDrawable) loanformfront_imageView.getDrawable()).getBitmap();
                            final String encodedImage = bitmapToBase64(img);
                            String testImage = encodedImage.substring(0,73);

                            if (testImage.equals(defaultp)) {
                                Toast.makeText(getContext(), "Click to capture filled front page photo!", Toast.LENGTH_SHORT).show();
                            } else {
                                SharedPrefs.loanleavephoto5(encodedImage, getActivity());
                            }
                        }
                        else if (countclicks==5) {
                            Bitmap photo = (Bitmap) data.getExtras().get("data");
                            loanformfront_imageView.setImageBitmap(photo);

                            String defaultp = "iVBORw0KGgoAAAANSUhEUgAAASkAAAF/CAIAAACaLKg/AAAAA3NCSVQICAjb4U/gAAAgAElEQ";
                            Bitmap img = ((BitmapDrawable) loanformfront_imageView.getDrawable()).getBitmap();
                            final String encodedImage = bitmapToBase64(img);
                            String testImage = encodedImage.substring(0,73);

                            if (testImage.equals(defaultp)) {
                                Toast.makeText(getContext(), "Click to capture filled front page photo!", Toast.LENGTH_SHORT).show();
                            } else {
                                SharedPrefs.loanleavephoto6(encodedImage, getActivity());
                            }
                        }
                        else if (countclicks==6) {
                            Bitmap photo = (Bitmap) data.getExtras().get("data");
                            loanformfront_imageView.setImageBitmap(photo);

                            String defaultp = "iVBORw0KGgoAAAANSUhEUgAAASkAAAF/CAIAAACaLKg/AAAAA3NCSVQICAjb4U/gAAAgAElEQ";
                            Bitmap img = ((BitmapDrawable) loanformfront_imageView.getDrawable()).getBitmap();
                            final String encodedImage = bitmapToBase64(img);
                            String testImage = encodedImage.substring(0,73);

                            if (testImage.equals(defaultp)) {
                                Toast.makeText(getContext(), "Click to capture filled front page photo!", Toast.LENGTH_SHORT).show();
                            } else {
                                SharedPrefs.loanleavephoto7(encodedImage, getActivity());
                            }
                        }
                        else if (countclicks==7) {
                            Bitmap photo = (Bitmap) data.getExtras().get("data");
                            loanformfront_imageView.setImageBitmap(photo);

                            String defaultp = "iVBORw0KGgoAAAANSUhEUgAAASkAAAF/CAIAAACaLKg/AAAAA3NCSVQICAjb4U/gAAAgAElEQ";
                            Bitmap img = ((BitmapDrawable) loanformfront_imageView.getDrawable()).getBitmap();
                            final String encodedImage = bitmapToBase64(img);
                            String testImage = encodedImage.substring(0,73);

                            if (testImage.equals(defaultp)) {
                                Toast.makeText(getContext(), "Click to capture filled front page photo!", Toast.LENGTH_SHORT).show();
                            } else {
                                SharedPrefs.loanleavephoto8(encodedImage, getActivity());
                            }
                        }
                        else if (countclicks==8) {
                            Bitmap photo = (Bitmap) data.getExtras().get("data");
                            loanformfront_imageView.setImageBitmap(photo);

                            String defaultp = "iVBORw0KGgoAAAANSUhEUgAAASkAAAF/CAIAAACaLKg/AAAAA3NCSVQICAjb4U/gAAAgAElEQ";
                            Bitmap img = ((BitmapDrawable) loanformfront_imageView.getDrawable()).getBitmap();
                            final String encodedImage = bitmapToBase64(img);
                            String testImage = encodedImage.substring(0,73);

                            if (testImage.equals(defaultp)) {
                                Toast.makeText(getContext(), "Click to capture filled front page photo!", Toast.LENGTH_SHORT).show();
                            } else {
                                SharedPrefs.loanleavephoto9(encodedImage, getActivity());
                            }
                        }
                        else if (countclicks==9) {
                            Bitmap photo = (Bitmap) data.getExtras().get("data");
                            loanformfront_imageView.setImageBitmap(photo);

                            String defaultp = "iVBORw0KGgoAAAANSUhEUgAAASkAAAF/CAIAAACaLKg/AAAAA3NCSVQICAjb4U/gAAAgAElEQ";
                            Bitmap img = ((BitmapDrawable) loanformfront_imageView.getDrawable()).getBitmap();
                            final String encodedImage = bitmapToBase64(img);
                            String testImage = encodedImage.substring(0,73);

                            if (testImage.equals(defaultp)) {
                                Toast.makeText(getContext(), "Click to capture filled front page photo!", Toast.LENGTH_SHORT).show();
                            } else {
                                SharedPrefs.loanleavephoto10(encodedImage, getActivity());
                            }

                        }
                        else {
                            Bitmap photo = (Bitmap) data.getExtras().get("data");
                            loanformfront_imageView.setImageBitmap(photo);

                            String defaultp = "iVBORw0KGgoAAAANSUhEUgAAASkAAAF/CAIAAACaLKg/AAAAA3NCSVQICAjb4U/gAAAgAElEQ";
                            Bitmap img = ((BitmapDrawable) loanformfront_imageView.getDrawable()).getBitmap();
                            final String encodedImage = bitmapToBase64(img);
                            String testImage = encodedImage.substring(0,73);

                            if (testImage.equals(defaultp)) {
                                Toast.makeText(getContext(), "Click to capture filled front page photo!", Toast.LENGTH_SHORT).show();
                            } else {
                                SharedPrefs.loanleavephoto1(encodedImage, getActivity());
                            }
                        }


                    } catch (Exception e) {
                        Toast.makeText(getContext(), "Failed to load", Toast.LENGTH_SHORT)
                                .show();
                        Log.e("Camera", e.toString());
                    }
                }
        }
    }

    private String bitmapToBase64(Bitmap bitmap) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
        byte[] byteArray = byteArrayOutputStream.toByteArray();
        return Base64.encodeToString(byteArray, Base64.DEFAULT);

    }

    @Override
    public void onNextClicked(final StepperLayout.OnNextClickedCallback callback) {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                String defaultp = "iVBORw0KGgoAAAANSUhEUgAAASkAAAF/CAIAAACaLKg/AAAAA3NCSVQICAjb4U/gAAAgAElEQ";
                Bitmap img = ((BitmapDrawable) loanformfront_imageView.getDrawable()).getBitmap();
                final String encodedImage = bitmapToBase64(img);
                String testImage = encodedImage.substring(0,73);

                if (testImage.equals(defaultp)) {
                    Toast.makeText(getContext(), "Click to capture filled front page photo!", Toast.LENGTH_SHORT).show();
                } else {
                    if (countclicks<1)
                    {Toast.makeText(getContext(), "Click on + button to add atleast two form pages.", Toast.LENGTH_SHORT).show();}
                    else
                        {
                            callback.goToNextStep();
                        }
                }
            }
        }, 0L);
    }

    @Override
    public void onCompleteClicked(StepperLayout.OnCompleteClickedCallback callback) {
   //on complete action...
    }

    @Override
    public void onBackClicked(final StepperLayout.OnBackClickedCallback callback) {
        callback.goToPrevStep();
    }

    @Override
    public VerificationError verifyStep() {
        return null;
    }

    @Override
    public void onSelected() {

    }

    @Override
    public void onError(@NonNull VerificationError error) {

    }
}
