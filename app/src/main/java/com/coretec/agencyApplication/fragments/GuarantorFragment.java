package com.coretec.agencyApplication.fragments;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.cloudpos.fingerprint.FingerprintDevice;
import com.cloudpos.jniinterface.PrinterInterface;
import com.coretec.agencyApplication.R;
import com.coretec.agencyApplication.activities.gfl.MainGFLDashboard;
import com.coretec.agencyApplication.activities.newmodules.ValidateTunzhengbigBio;
import com.coretec.agencyApplication.activities.gfl.Loans;
import com.coretec.agencyApplication.api.GFL;
import com.coretec.agencyApplication.api.requests.FingerPrintRequest;
import com.coretec.agencyApplication.api.requests.GetAvailableKgsGuarantorsRequest;
import com.coretec.agencyApplication.api.requests.GrowersDetailsRequest;
import com.coretec.agencyApplication.api.requests.GuarantorRegistration_v2;
import com.coretec.agencyApplication.api.responses.FingerPrintResponse;
import com.coretec.agencyApplication.api.responses.GetAvailableKgsGuarantorsResponse;
import com.coretec.agencyApplication.api.responses.GrowersDetailsResponse;
import com.coretec.agencyApplication.api.responses.GuarantorRegistrationResponse;
import com.coretec.agencyApplication.model.GrowerDetails;
import com.coretec.agencyApplication.model.GuarantorObject;
import com.coretec.agencyApplication.utils.SharedPrefs;
import com.coretec.agencyApplication.utils.Utils;
import com.coretec.agencyApplication.wizarpos.function.printer.PrinterCommand;
import com.stepstone.stepper.BlockingStep;
import com.stepstone.stepper.StepperLayout;
import com.stepstone.stepper.VerificationError;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import static com.coretec.agencyApplication.utils.Utils.getFormattedDate;
import static com.coretec.agencyApplication.utils.Utils.getLat;
import static com.coretec.agencyApplication.utils.Utils.getLong;
public class GuarantorFragment extends Fragment implements BlockingStep {
    Button btnAdd;
    AlertDialog guarantorDialog;
    private List<String> guarantorList = new ArrayList();
    private List<GuarantorObject> guarantorObjectList = new ArrayList<>();
    private List<String> guarantorList1 = new ArrayList();
    private LinearLayout guarantorsLayout;
    private TextView  requiredKgs1;
    private Button saveBtn, submitLoanApplication;
    ProgressDialog progressDialog;
    private EditText identifier_deposit;
    Button submitBtn;
    double qualified,selfg,req;

    private String item;
    String available_kgs;
    String identifier_id;

    AlertDialog dialogFingerP;
    android.app.AlertDialog fingerpDialog;
    private Handler handler;
    private FingerprintDevice fingerprintDevice;
    private static final int MSGID_SHOW_MESSAGE = 0;
    protected Handler mHandler = null;


    private static final int SHOW_FAIL_MESSAGE = 2;
    private static final int SHOW_BTN = 3;
    private static final int HIDE_BTN = 4;

    private SharedPreferences preferences;
    SharedPreferences sharedPreferences;
    String text1;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.gfl_fragment_guarantor, container, false);
        guarantorsLayout = v.findViewById(R.id.linear_guarantors);
        requiredKgs1 = v.findViewById(R.id.requiredKgs1);
        saveBtn = v.findViewById(R.id.btnSave);
        submitLoanApplication = v.findViewById(R.id.submitLoan);
        progressDialog = new ProgressDialog(getActivity());
        submitLoanApplication.setVisibility(View.VISIBLE);
        submitLoanApplication.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String msg = "Are you sure you want to complete loan application?";
                android.app.AlertDialog.Builder builder1 = new android.app.AlertDialog.Builder(getContext());
                builder1.setMessage(msg);
                builder1.setTitle("Alert!");
                builder1.setCancelable(false);

                builder1.setPositiveButton(
                        "Yes",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                                /* progress dialog */
                                progressDialog = new ProgressDialog(getContext());
                                progressDialog.setMessage("Submitting, please wait...");
                                progressDialog.setCancelable(false);
                                progressDialog.show();
                                SubmitLoanApplication();
                                submitLoanApplication.setClickable(false);
                            }
                        });

                builder1.setNegativeButton(
                        "Cancel",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });

                android.app.AlertDialog alert11 = builder1.create();
                alert11.show();
            }
        });

        String kilos = SharedPrefs.read(SharedPrefs.LOAN_QUALIFIED_KGS, null);
        btnAdd = v.findViewById(R.id.btnAdd);
        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                createDialog();
            }
        });

        return v;
    }

    private Context mContext;

    @Override
    public void onAttach(final Activity activity) {
        super.onAttach(activity);
        mContext = activity;
    }

    private void createDialog() {

        LayoutInflater myInflater = LayoutInflater.from(mContext);
        View guarantorView = myInflater.inflate(R.layout.gfl_dialog_guarantor, null);

        final ArrayList<String> arrayList = new ArrayList<>();
        ArrayAdapter<GrowerDetails> adapter;
        String item;

        submitBtn = guarantorView.findViewById(R.id.btnDiagSubmit);
        final Button cancelBtn = guarantorView.findViewById(R.id.btnDiagCancel);
        identifier_deposit = guarantorView.findViewById(R.id.id_number);
        final Spinner spinner = guarantorView.findViewById(R.id.growerSpinner);
        final ProgressBar progressBar = guarantorView.findViewById(R.id.growersPb);
        final TextView growerName = guarantorView.findViewById(R.id.tvfNames);
        final TextView warNing = guarantorView.findViewById(R.id.warning);
        final TextView totalKilos = guarantorView.findViewById(R.id.totalKgs);
        final TextView kiloTakikana = guarantorView.findViewById(R.id.takikana);
        final TextView availKgs = guarantorView.findViewById(R.id.availableKgs);
        final ProgressBar loading = guarantorView.findViewById(R.id.loadingPb);
        final TextView remarks = guarantorView.findViewById(R.id.tvRemarks);

        kiloTakikana.setText(SharedPrefs.read(SharedPrefs.LOAN_QUALIFIED_KGS, null));
        adapter = new ArrayAdapter<GrowerDetails>(getContext(),
                R.layout.simple_spinner_dropdown_item, new ArrayList<GrowerDetails>());
        warNing.setVisibility(View.GONE);
        loading.setVisibility(View.GONE);
        identifier_deposit.setInputType(InputType.TYPE_NULL);
        identifier_deposit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // showMyDialog();
            }
        });

        identifier_deposit.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    // showMyDialog();
                }
            }
        });
        TextWatcher textWatcherId = new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                final String identifier_number = identifier_deposit.getText().toString();
                /*if (idNumber.equals(identifier_number)) {
                    warNing.setVisibility(View.VISIBLE);
                    progressBar.setVisibility(View.GONE);
                    growerName.setText("");
                    arrayList.clear();

                } else if (!(idNumber.equals(identifier_number))){*/
                    if (identifier_deposit.length() >= 5 && identifier_deposit.length() <= 8) {
                        warNing.setVisibility(View.GONE);
                        submitBtn.setVisibility(View.GONE);
                        arrayList.clear();
                        progressBar.setVisibility(View.VISIBLE);
                        progressBar.getIndeterminateDrawable().setColorFilter(ContextCompat.getColor(getContext(), R.color.btn_green), PorterDuff.Mode.SRC_IN);
                    }

                    boolean existsAlready = false;
                    for (GuarantorObject guarantorObject : guarantorObjectList) {
                        if (guarantorObject.getPassport().equals(identifier_deposit.getText().toString())) {
                            existsAlready = true;
                            break;
                        }
                    }

                    if (existsAlready) {
                        Toast.makeText(getActivity(), "Guarantor already added", Toast.LENGTH_SHORT).show();
                        return;
                    }

                    if (identifier_deposit.length() >= 7) {
                        final String TAG = "GrowersAccounts";
                        String URL = GFL.MSACCO_AGENT + GFL.GetGrowersDetails;
                        final GrowersDetailsRequest growersDetails = new GrowersDetailsRequest();
                        growersDetails.agentid = SharedPrefs.read(SharedPrefs.AGENT_ID_1, null);
                        growersDetails.corporateno = "CAP016";
                        growersDetails.accountidentifier = identifier_number;
                        growersDetails.accountidentifiercode = "1";
                        growersDetails.transactiontype = "1";
                        growersDetails.terminalid = SharedPrefs.read(SharedPrefs.DEVICE_ID, null);
                        growersDetails.longitude = getLong(getContext());
                        growersDetails.latitude = getLat(getContext());
                        growersDetails.date = getFormattedDate();

                        Log.e(TAG, growersDetails.getBody().toString());

                        GFL.instance(getContext()).request(URL, growersDetails, new GFL.RequestListener() {
                            @Override
                            public void onSuccess(String response) {
                                GrowersDetailsResponse growersDetailsResponse = GFL.instance(getContext())
                                        .mGson.fromJson(response, GrowersDetailsResponse.class);
                                if (growersDetailsResponse.is_successful) {
                                    List<GrowerDetails> growerDetailsList = new ArrayList<>();
                                    warNing.setVisibility(View.GONE);
                                    try {
                                        JSONObject jsonObject = new JSONObject(response);

                                        JSONArray jsonArray = jsonObject.getJSONArray("Growers");
                                        for (int i = 0; i < jsonArray.length(); i++) {
                                            JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                                            JSONArray jsonArray1 = jsonObject1.getJSONArray("GrowerDetails");
                                            for (int l = 0; l < jsonArray1.length(); l++) {
                                                JSONObject jsonObject2 = jsonArray1.getJSONObject(l);
                                                progressBar.setVisibility(View.GONE);
                                                arrayList.add(String.valueOf(jsonObject2.getString("GrowerNo")));
                                                String gName = jsonObject2.getString("GrowerName");
                                                String gNo = jsonObject2.getString("GrowerNo");
                                                String totalKgs = jsonObject2.getString("CurrentKG");
                                                String isDefaulter = jsonObject2.getString("LoanDefaulter");

                                                SharedPrefs.write(SharedPrefs.IS_DEFAULTER, isDefaulter);
                                                if (isDefaulter.equals("Yes")) {
                                                    String msg = "The grower selected has defaulted one or more loans. Please choose another one.";
                                                    android.app.AlertDialog.Builder builder1 = new android.app.AlertDialog.Builder(getContext());
                                                    builder1.setMessage(msg);
                                                    builder1.setTitle("Defaulter Alert!");
                                                    builder1.setCancelable(false);

                                                    builder1.setPositiveButton(
                                                            "OK",
                                                            new DialogInterface.OnClickListener() {
                                                                public void onClick(DialogInterface dialog, int id) {
                                                                    dialog.cancel();
                                                                    guarantorDialog.dismiss();
                                                                }
                                                            });

                                                    android.app.AlertDialog alert11 = builder1.create();
                                                    alert11.show();
                                                }
                                                    growerName.setText(gName);
                                                    totalKilos.setText(totalKgs);

                                            }
                                        }
                                        String idNumber = SharedPrefs.read(SharedPrefs.LOAN_IDENTITY_NO, null);
                                        if (idNumber.equals(identifier_number))
                                        {
                                            arrayList.clear();
                                            arrayList.add(SharedPrefs.read(SharedPrefs.SELECTEDGROWER, null));
                                            ArrayAdapter<String> adapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_spinner_item, arrayList);
                                            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                            spinner.setAdapter(adapter);
                                        }else {
                                            ArrayAdapter<String> adapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_spinner_item, arrayList);
                                            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                            spinner.setAdapter(adapter);
                                        }
                                        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                            @Override
                                            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                                String item = parent.getItemAtPosition(position).toString();
                                                text1 = spinner.getSelectedItem().toString();
                                                SharedPrefs.write(SharedPrefs.DROP_DOWN, text1);
                                                loading.setVisibility(View.VISIBLE);
                                                loading.getIndeterminateDrawable().setColorFilter(ContextCompat.getColor(getContext(), R.color.white), PorterDuff.Mode.SRC_IN);
                                                final String TAG = "AvailableKGS";

                                                String URL = GFL.MSACCO_AGENT + GFL.GetAvailableKgsGuarantors;
                                                final GetAvailableKgsGuarantorsRequest availableKgs = new GetAvailableKgsGuarantorsRequest();
                                                availableKgs.agentid = SharedPrefs.read(SharedPrefs.AGENT_ID_1, null);
                                                availableKgs.corporateno = "CAP016";
                                                availableKgs.growerno = text1;
                                                availableKgs.transactiontype = "1";
                                                availableKgs.terminalid = SharedPrefs.read(SharedPrefs.DEVICE_ID, null);
                                                availableKgs.longitude = getLong(getContext());
                                                availableKgs.latitude = getLat(getContext());
                                                availableKgs.date = getFormattedDate();

                                                GFL.instance(getContext()).request(URL, availableKgs, new GFL.RequestListener() {
                                                    @Override
                                                    public void onSuccess(String response) {
                                                        GetAvailableKgsGuarantorsResponse availableKgsResponse = GFL.instance(getContext())
                                                                .mGson.fromJson(response, GetAvailableKgsGuarantorsResponse.class);
                                                        if (availableKgsResponse.is_successful) {
                                                            List<GrowerDetails> growerDetailsList = new ArrayList<>();
                                                            try {
                                                                JSONObject jsonObject = new JSONObject(response);
                                                                String availableKgs = jsonObject.getString("availablekgs");
                                                                availKgs.setText(availableKgs);
                                                                double kilos = Double.parseDouble(availableKgs);
                                                                if (kilos < 100.00 || kilos < 0.00) {
                                                                    cancelBtn.setVisibility(View.VISIBLE);
                                                                    loading.setVisibility(View.GONE);
                                                                    remarks.setText("Grower has over guaranteed");
                                                                    remarks.setTextColor(getResources().getColor(R.color.red));
                                                                    submitBtn.setVisibility(View.GONE);
                                                                } else {
                                                                    String idNumber = SharedPrefs.read(SharedPrefs.LOAN_IDENTITY_NO, null);
                                                                    final String identifier_number = identifier_deposit.getText().toString();
                                                                    if (idNumber.equals(identifier_number)) {
                                                                        requestGetAccountDetailsForSpinerLoad(identifier_number);
                                                                        //submitBtn.setVisibility(View.VISIBLE);
                                                                    } else{// if (!(idNumber.equals(identifier_number)))
                                                                        //submitBtn.setVisibility(View.GONE);
                                                                        requestFingerPrints();
                                                                    }
                                                                    submitBtn.setVisibility(View.GONE);
                                                                    cancelBtn.setVisibility(View.GONE);
                                                                    loading.setVisibility(View.GONE);
                                                                    remarks.setText("OK");
                                                                    remarks.setTextColor(getResources().getColor(R.color.btn_green));
                                                                }

                                                            } catch (JSONException e) {
                                                                e.printStackTrace();
                                                            }

                                                        } else {
                                                            //do something here
                                                        }

                                                    }

                                                    @Override
                                                    public void onTokenExpired() {

                                                    }

                                                    @Override
                                                    public void onError(String error) {

                                                    }
                                                });
                                                Log.e("LOAN CODE", text1);
                                            }

                                            @Override
                                            public void onNothingSelected(AdapterView<?> parent) {

                                            }
                                        });

                                    } catch (JSONException e) {
                                        e.printStackTrace();
                                    }

                                } else {
                                }

                            }

                            @Override
                            public void onTokenExpired() {

                            }

                            @Override
                            public void onError(String error) {

                            }
                        });
                    }

               // }

            }
        };
        identifier_deposit.addTextChangedListener(textWatcherId);
        submitBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                progressDialog = new ProgressDialog(getContext());
                progressDialog.setCancelable(false);
                progressDialog.show();
                available_kgs = availKgs.getText().toString();
                GuarantorObject guarantorObject = new GuarantorObject();
                guarantorObject.setAvailableKgs(availKgs.getText().toString());
                guarantorObject.setGrowerNumber(SharedPrefs.read(SharedPrefs.DROP_DOWN, null));
                guarantorObject.setPassport(identifier_deposit.getText().toString());
                guarantorObjectList.add(guarantorObject);

                addGuarantorToList();
                guarantorDialog.dismiss();
                progressDialog.dismiss();
            }
        });
        cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                guarantorsLayout.removeAllViews();
                guarantorDialog.dismiss();
            }
        });

        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        builder.setView(guarantorView);
        guarantorDialog = builder.create();
        guarantorDialog.show();

    }

    @Override
    public void onNextClicked(final StepperLayout.OnNextClickedCallback callback) {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                SharedPrefs.write(SharedPrefs.FP_BOOLEAN, "false");
                callback.goToNextStep();
            }
        }, 0L);// delay open another fragment,
    }

    @Override
    public void onCompleteClicked(StepperLayout.OnCompleteClickedCallback callback) {

        if (guarantorObjectList.size() < 3) {
            Toast.makeText(getActivity(), "Minimum of three guarantors required", Toast.LENGTH_SHORT).show();
            return;
        }

    }

    @Override
    public void onBackClicked(final StepperLayout.OnBackClickedCallback callback) {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                Toast.makeText(mContext, "Please add guarantors to save application!", Toast.LENGTH_SHORT).show();
            }
        }, 0L);
    }

    @Override
    public VerificationError verifyStep() {
        return null;
    }

    @Override
    public void onSelected() {
            qualified= Double.parseDouble(SharedPrefs.read(SharedPrefs.LOAN_QUALIFIED_KGS, null));
            requiredKgs1.setText(String.valueOf(qualified));
            saveBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final String TAG = "GuarantorRegistration";
                progressDialog = new ProgressDialog(getContext());
                progressDialog.setMessage("Loading please wait...");
                progressDialog.setCancelable(false);
                progressDialog.show();
                String URL = GFL.MSACCO_AGENT + GFL.GuarantorRegistration;
                final GuarantorRegistration_v2 grRequest = new GuarantorRegistration_v2();
                grRequest.setApplidno(SharedPrefs.read(SharedPrefs.LOAN_IDENTITY_NO, null));
                grRequest.setNewAppl(true);
                grRequest.setApplgrowerno(SharedPrefs.read(SharedPrefs.LOAN_APP_GROWER, null));
                grRequest.setApplreqkgs(SharedPrefs.read(SharedPrefs.LOAN_QUALIFIED_KGS, null));
                grRequest.setApplptype(SharedPrefs.read(SharedPrefs.LOAN_PT_CODE, null));
                grRequest.setApplamount(SharedPrefs.read(SharedPrefs.LOAN_ENTER_AMT, null));
                grRequest.setDisbursmentmethod(SharedPrefs.read(SharedPrefs.LOAN_SPINNER, null));
                grRequest.setApprovalstatus("4");
                grRequest.setGuarantorlist(guarantorObjectList);
                grRequest.setTransactiontype("1");
                grRequest.setAgentid(SharedPrefs.read(SharedPrefs.AGENT_ID_1, null));
                grRequest.setTerminalid(SharedPrefs.read(SharedPrefs.DEVICE_ID, null));
                grRequest.setLongitude(getLong(getContext()));
                grRequest.setLatitude(getLat(getContext()));
                grRequest.setDate(getFormattedDate());
                grRequest.setCorporate_no("CAP016");

                Log.e(TAG, grRequest.toString());

                GFL.instance(getContext()).request(URL, grRequest, new GFL.RequestListener() {
                    @Override
                    public void onSuccess(String response) {
                        GuarantorRegistrationResponse grResponse = GFL.instance(getContext())
                                .mGson.fromJson(response, GuarantorRegistrationResponse.class);
                        if (grResponse.is_successful) {
                            try {
                                JSONObject jsonObject = new JSONObject(response);

                                String receiptNo = jsonObject.getString("receiptNo");
                                Toast.makeText(getContext(), receiptNo, Toast.LENGTH_LONG).show();

                                progressDialog.dismiss();
                                Toast.makeText(mContext, "Successfully submitted", Toast.LENGTH_SHORT).show();
                                getActivity().finish();

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        } else {

                        }

                    }

                    @Override
                    public void onTokenExpired() {

                    }

                    @Override
                    public void onError(String error) {

                    }
                });
            }
        });

    }

    @Override
    public void onError(@NonNull VerificationError error) {
    }

    private void addGuarantorToList() {

        if (guarantorObjectList.size() >= 1 && guarantorObjectList.size() < 3) {
            submitLoanApplication.setVisibility(View.VISIBLE);
        }
        if (guarantorObjectList.size() < 1) {
            saveBtn.setVisibility(View.GONE);
            submitLoanApplication.setVisibility(View.GONE);
        }
        if (guarantorObjectList.size() >= 3) {
            saveBtn.setVisibility(View.GONE);
            submitLoanApplication.setVisibility(View.VISIBLE);
        }
        guarantorsLayout.removeAllViews();
        for (final GuarantorObject guarantorObject : guarantorObjectList) {
            View v = LayoutInflater.from(getContext()).inflate(R.layout.item_guarantor, null);
            TextView tvName = v.findViewById(R.id.tv_guarantors_name);
            TextView tvPass = v.findViewById(R.id.tv_guarantors_id);
            TextView tvKg = v.findViewById(R.id.tv_guarantors_kgs);
            ImageButton removeImageBtn = v.findViewById(R.id.imageButton_close);

            tvKg.setText(guarantorObject.getAvailableKgs() + " kgs");
            tvName.setText(guarantorObject.getGrowerNumber());
            tvPass.setText(guarantorObject.getPassport());

            removeImageBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    String msg = "Are you sure you want to remove guarantor?";
                    android.app.AlertDialog.Builder builder1 = new android.app.AlertDialog.Builder(getContext());
                    builder1.setMessage(msg);
                    builder1.setTitle("Alert!");
                    builder1.setCancelable(false);

                    builder1.setPositiveButton(
                            "Yes",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                    guarantorObjectList.remove(guarantorObject);
                                    addGuarantorToList();
                                }
                            });

                    builder1.setNegativeButton(
                            "Cancel",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                }
                            });

                    android.app.AlertDialog alert11 = builder1.create();
                    alert11.show();

                }
            });
            guarantorsLayout.addView(v);
        }
    }

    void SubmitLoanApplication() {
        final String TAG = "GuarantorRegistration";
        String URL = GFL.MSACCO_AGENT + GFL.GuarantorRegistration;

        final GuarantorRegistration_v2 grRequest = new GuarantorRegistration_v2();
        grRequest.setApplidno(SharedPrefs.read(SharedPrefs.LOAN_IDENTITY_NO, null));
        grRequest.setApplgrowerno(SharedPrefs.read(SharedPrefs.LOAN_APP_GROWER, null));
        grRequest.setApplreqkgs(SharedPrefs.read(SharedPrefs.LOAN_QUALIFIED_KGS, null));
        grRequest.setApplptype(SharedPrefs.read(SharedPrefs.LOAN_PT_CODE, null));
        grRequest.setApplamount(SharedPrefs.read(SharedPrefs.LOAN_ENTER_AMT, null));
        grRequest.setDisbursmentmethod(SharedPrefs.read(SharedPrefs.LOAN_SPINNER, null));
        grRequest.setApprovalstatus("5");
        grRequest.setGuarantorlist(guarantorObjectList);
        grRequest.setTransactiontype("1");
        grRequest.setAgentid(SharedPrefs.read(SharedPrefs.AGENT_ID_1, null));
        grRequest.setTerminalid(SharedPrefs.read(SharedPrefs.DEVICE_ID, null));
        grRequest.setLongitude(getLong(getContext()));
        grRequest.setLatitude(getLat(getContext()));
        grRequest.setDate(getFormattedDate());
        grRequest.setCorporate_no("CAP016");

        Log.e(TAG, grRequest.toString());

        GFL.instance(getContext()).request(URL, grRequest, new GFL.RequestListener() {
            @Override
            public void onSuccess(String response) {

                progressDialog.dismiss();
                GuarantorRegistrationResponse grResponse = GFL.instance(getContext())
                        .mGson.fromJson(response, GuarantorRegistrationResponse.class);
                if (grResponse.is_successful) {
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        String agent =SharedPrefs.read(SharedPrefs.AGENT_ID_1, null);// sharedPreferences.getString(PreferenceFileKeys.AGENT_ACCOUNT_NAME, "");
                        String growerno = grRequest.getApplgrowerno();
                        String qualifiedkgs = grRequest.getApplreqkgs();
                        String loancode = grRequest.getApplptype();
                        String appliedamt = grRequest.getApplamount();
                        String disbursmentmethod = grRequest.getDisbursmentmethod();
                        String saccoName = "GREENLAND FEDHA LTD";

                        PrinterInterface.open();

                        writetest(agent, growerno,qualifiedkgs, loancode, appliedamt,
                                disbursmentmethod, saccoName,grResponse.getReceiptNo(),
                                grResponse.getTransactiondate(),"Loan Application");
                        PrinterInterface.close();

                        String msg = "Loan application submitted successfully!";
                        SharedPrefs.write(SharedPrefs.LOAN_IDENTITY_NO, "null");
                        SharedPrefs.write(SharedPrefs.LOAN_APP_GROWER, "null");
                        SharedPrefs.write(SharedPrefs.LOAN_QUALIFIED_KGS, "null");
                        SharedPrefs.write(SharedPrefs.LOAN_PT_CODE, "null");
                        SharedPrefs.write(SharedPrefs.LOAN_ENTER_AMT, "null");
                        SharedPrefs.write(SharedPrefs.VR_NAMES, "null");
                        SharedPrefs.write(SharedPrefs.VR_NAMES, "null");

                        android.app.AlertDialog.Builder builder1 = new android.app.AlertDialog.Builder(getContext());
                        builder1.setMessage(msg);
                        builder1.setTitle("Success!");
                        builder1.setCancelable(false);

                        builder1.setPositiveButton(
                                "Done",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();
                                        SharedPrefs.write(SharedPrefs.LOAN_IDENTITY_NO, "null");
                                        SharedPrefs.write(SharedPrefs.LOAN_ENTER_AMT, "null");
                                        startActivity(new Intent(getContext(), Loans.class));
                                    }
                                });

                        android.app.AlertDialog alert11 = builder1.create();
                        alert11.show();

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                } else {
                    //do something here
                }

            }

            @Override
            public void onTokenExpired() {

            }

            @Override
            public void onError(String error) {

            }
        });
    }

    void requestFingerPrints() {
        final String TAG = "GET FINGERPRINTS";
        String URL = GFL.MSACCO_AGENT + GFL.GetFourFingerPrint;
        identifier_id = identifier_deposit.getText().toString();
        final FingerPrintRequest fingerPrintRequest = new FingerPrintRequest();
        fingerPrintRequest.accountidentifier = identifier_deposit.getText().toString();
        fingerPrintRequest.accountidentifiercode = "1";
        fingerPrintRequest.agentid = SharedPrefs.read(SharedPrefs.AGENT_ID_1, null);
        fingerPrintRequest.terminalid = MainGFLDashboard.imei;//SharedPrefs.read(SharedPrefs.DEVICE_ID, null);
        fingerPrintRequest.longitude = getLong(getActivity());
        fingerPrintRequest.latitude = getLat(getActivity());
        fingerPrintRequest.date = getFormattedDate();

        Log.e(TAG, fingerPrintRequest.getBody().toString());
        GFL.instance(getActivity()).request(URL, fingerPrintRequest, new GFL.RequestListener() {
            @Override
            public void onSuccess(String response) {
                FingerPrintResponse fingerPrintResponse = GFL.instance(getActivity())
                        .mGson.fromJson(response, FingerPrintResponse.class);
                if (fingerPrintResponse.is_successful){
                    try {
                        JSONObject jsonObject = new JSONObject(response);

                        String leftRing = jsonObject.getString("leftring");
                        String leftLittle = jsonObject.getString("leftlittle");
                        String rightRing = jsonObject.getString("rightring");
                        String rightLittle = jsonObject.getString("rightlittle");

                        SharedPrefs.write(SharedPrefs.LEFT_RING, leftRing);
                        SharedPrefs.write(SharedPrefs.LEFT_LITTLE, leftLittle);
                        SharedPrefs.write(SharedPrefs.RIGHT_RING, rightRing);
                        SharedPrefs.write(SharedPrefs.RIGHT_LITTLE, rightLittle);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    submitBtn.setVisibility(View.VISIBLE);
                    SharedPrefs.transactionstypelogs(13, getContext());
                    SharedPrefs.growerslogs(text1, getContext());
                    startActivity(new Intent(getActivity(), ValidateTunzhengbigBio.class));

                } else {
                    progressDialog.dismiss();
                    Toast.makeText(getActivity(), "Could not get fingerprints", Toast.LENGTH_SHORT).show();
                    submitBtn.setVisibility(View.GONE);
                }

            }

            @Override
            public void onTokenExpired() {

            }

            @Override
            public void onError(String error) {

            }
        });

    }


    @Override
    public void onResume() {
        SharedPrefs.init(getContext());
        super.onResume();
        String fpBoolean = SharedPrefs.read(SharedPrefs.FP_BOOLEAN, null);
        if (fpBoolean.equals("true")) {
            //requestGetAccountDetailsForSpinerLoad(identifier_deposit.getText().toString());
            Toast.makeText(mContext, "proceed", Toast.LENGTH_SHORT).show();
        } else if (fpBoolean.equals("false")){
            guarantorDialog.dismiss();
        }
    }


    public void writetest(String agentName, String growerno, String qualifiedkgs, String loancode,
                          String appliedamt, String disbursmentmethod,  String saccoame,
                          String receiptNumber, String dateed, String transactionType) {
        try {

            byte[] arrySaccoName = null;

            byte[] arrygrowerno = null;
            byte[] arryqualifiedkgs = null;
            byte[] arryloancode = null;
            byte[] aarryppliedamt = null;
            byte[] arrydisbursmentmethod = null;

            byte[] arryWithdrawAmt = null;
            byte[] arryAmt = null;

            byte[] arryMotto = null;
            byte[] terminalNo = null;
           // byte[] receiptNo = null;
            byte[] date = null;
            byte[] transactionTYpe = null;
            byte[] servedBy = null;
            byte[] signature = null;


            try {
                arrySaccoName = String.valueOf("       " + String.valueOf(saccoame)).getBytes("GB2312");
                terminalNo = String.valueOf(" Terminal ID :" + Utils.getIMEI(getContext())).getBytes("GB2312");
               // receiptNo = String.valueOf(" Copy Receipt Number  : " + receiptNumber).getBytes("GB2312");
                date = String.valueOf("Date : " + dateed).getBytes("GB2312");
                transactionTYpe = String.valueOf("      " + transactionType).getBytes("GB2312");

                arrygrowerno = String.valueOf(" Grower Number  :" + growerno).getBytes("GB2312");
                arryqualifiedkgs = String.valueOf(String.valueOf("   Qualified Kgs " + qualifiedkgs)).getBytes("GB2312");
                arryloancode = String.valueOf(String.valueOf("  Loan Code   :" + loancode)).getBytes("GB2312");
                aarryppliedamt = String.valueOf(String.valueOf("   Applied Amount  :" + appliedamt)).getBytes("GB2312");
                arrydisbursmentmethod = String.valueOf(String.valueOf("  Disbursement Method   :" + disbursmentmethod)).getBytes("GB2312");

                servedBy = String.valueOf(" You were Served By " + agentName).getBytes("GB2312");
                signature = String.valueOf("Signature_______________________").getBytes("GB2312");

                arryMotto = "         PESAULIPO:  Wakati Wowote, Popote.      ".getBytes("GB2312");

            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            PrinterInterface.begin();

            byte[] cmd;
            cmd = new byte[]{(byte) 0x1B, (byte) 0x40};

            cmd = new byte[]{
                    (byte) 0x1B, (byte) 0x45, (byte) 0};

            write(cmd);
            write(arrySaccoName);
            writeLineBreak(1);
            write(terminalNo);
            writeLineBreak(1);
           /* write(receiptNo);
            writeLineBreak(1);*/
            write(date);
            writeLineBreak(1);
            write(transactionTYpe);
            writeLineBreak(3);

            write(arrygrowerno);
            writeLineBreak(1);
            write(arryqualifiedkgs);
            writeLineBreak(1);
            write(arryloancode);
            writeLineBreak(1);
            write(aarryppliedamt);
            writeLineBreak(1);
            write(arrydisbursmentmethod);
            writeLineBreak(3);

            write(servedBy);
            // print line break
            writeLineBreak(3);
            write(signature);
            writeLineBreak(5);
            write(arryMotto);
            writeLineBreak(4);
            writeLineBreak(5);
            PrinterInterface.end();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void write(final byte[] arryData) {
        int result = PrinterInterface.write(arryData, arryData.length);
    }
    private void writeLineBreak(int lineNumber) {
        write(PrinterCommand.getCmdEscDN(lineNumber));
    }



    void requestGetAccountDetailsForSpinerLoad(String identifier) {
        final String TAG = "GrowersAccounts";
        String URL = GFL.MSACCO_AGENT + GFL.GetGrowersDetails;
        final GrowersDetailsRequest growersDetails = new GrowersDetailsRequest();
        growersDetails.agentid = SharedPrefs.read(SharedPrefs.AGENT_ID_1, null);
        growersDetails.corporateno = "CAP016";
        growersDetails.accountidentifier = identifier;
        growersDetails.accountidentifiercode = "1";
        growersDetails.transactiontype = "1";
        growersDetails.terminalid = SharedPrefs.read(SharedPrefs.DEVICE_ID, null);
        growersDetails.longitude = getLong(getContext());
        growersDetails.latitude = getLat(getContext());
        growersDetails.date = getFormattedDate();

        Log.e(TAG, growersDetails.getBody().toString());

        GFL.instance(getContext()).request(URL, growersDetails, new GFL.RequestListener() {
            @Override
            public void onSuccess(String response) {
                GrowersDetailsResponse growersDetailsResponse = GFL.instance(getContext())
                        .mGson.fromJson(response, GrowersDetailsResponse.class);
                if (growersDetailsResponse.is_successful) {
                    List<GrowerDetails> growerDetailsList = new ArrayList<>();
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        JSONArray jsonArray = jsonObject.getJSONArray("Growers");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                            JSONArray jsonArray1 = jsonObject1.getJSONArray("GrowerDetails");
                            for (int l = 0; l < jsonArray1.length(); l++) {
                                JSONObject jsonObject2 = jsonArray1.getJSONObject(l);
                                if (jsonObject2.getString("BssRegistered").equals("Yes") && jsonObject2.getString("Status").equals("Active")){
                                    String idNumber = SharedPrefs.read(SharedPrefs.LOAN_IDENTITY_NO, null);
                                    final String identifier_number = identifier_deposit.getText().toString();
                                    if (idNumber.equals(identifier_number)) {
                                        submitBtn.setVisibility(View.VISIBLE);
                                    } else{// if (!(idNumber.equals(identifier_number)))
                                        submitBtn.setVisibility(View.GONE);
                                        //requestFingerPrints();
                                    }

                                }if (jsonObject2.getString("BssRegistered").equals("No")){
                                    Toast.makeText(getContext(), "Guarantor is not BSS registered", Toast.LENGTH_SHORT).show();
                                    submitBtn.setVisibility(View.GONE);
                                }if (!jsonObject2.getString("Status").equals("Active")){
                                    Toast.makeText(getContext(), "Guarantor is not active", Toast.LENGTH_SHORT).show();
                                    submitBtn.setVisibility(View.GONE);
                                }
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                }

            }

            @Override
            public void onTokenExpired() {

            }

            @Override
            public void onError(String error) {

            }
        });
    }
}
