package com.coretec.agencyApplication.fragments.newmodule;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.cloudpos.jniinterface.PrinterInterface;
import com.coretec.agencyApplication.R;
import com.coretec.agencyApplication.activities.newmodules.MainDashboardActivity;
import com.coretec.agencyApplication.api.Api;
import com.coretec.agencyApplication.api.requests.RequestMemberName;
import com.coretec.agencyApplication.api.requests.newmodulesrequests.GetMLoanProductsRequest;
import com.coretec.agencyApplication.api.requests.newmodulesrequests.LApplicationRequest;
import com.coretec.agencyApplication.api.requests.newmodulesrequests.RequestQualifiedAmount;
import com.coretec.agencyApplication.api.responses.MemberNameResponse;
import com.coretec.agencyApplication.api.responses.newmoduleresponses.ApplicationSubmissionResponse;
import com.coretec.agencyApplication.api.responses.newmoduleresponses.GetMLoanProductsResponse;
import com.coretec.agencyApplication.utils.PreferenceFileKeys;
import com.coretec.agencyApplication.utils.QuickToast;
import com.coretec.agencyApplication.utils.SharedPrefs;
import com.coretec.agencyApplication.utils.Utils;
import com.coretec.agencyApplication.wizarpos.function.printer.PrinterCommand;
import com.stepstone.stepper.BlockingStep;
import com.stepstone.stepper.StepperLayout;
import com.stepstone.stepper.VerificationError;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

import static com.coretec.agencyApplication.activities.BaseActivity.requestFingerPrints;
import static com.coretec.agencyApplication.utils.Utils.getFormattedDate;
import static com.coretec.agencyApplication.utils.Utils.getLat;
import static com.coretec.agencyApplication.utils.Utils.getLong;

public class NewLoanApplicationFragment extends Fragment implements BlockingStep, AdapterView.OnItemSelectedListener {

    private EditText identifier_deposit, id_amount;
    private TextView reqamout, namee, phone, loading, minamout,crd_score;
    private ArrayList<String> arrayList = new ArrayList<>();
    private Spinner productType;
    ProgressBar qualifiedPb, minimumPb, loanPb;
    ProgressDialog progressDialog;
    private String code, desc, productcode, itemLoan;
    private ArrayList<String> loanDetailsList = new ArrayList<>();
    private QuickToast toast = new QuickToast(getActivity());
    SharedPreferences sharedPreferences;
    Button btn_account_search;
    public RadioButton rbpin, rbbio;
    private static String autmode;
    private RadioGroup radioGroupAutharization;

    private Context globalContext = null;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.loan_application_frag, container, false);
        SharedPrefs.init(getContext());
        identifier_deposit = v.findViewById(R.id.id_number);
        reqamout = v.findViewById(R.id.reqamout);
        minamout = v.findViewById(R.id.minamout);
        crd_score = v.findViewById(R.id.crd_score);

        namee = v.findViewById(R.id.namee);
        phone = v.findViewById(R.id.phone);
        loading = v.findViewById(R.id.loading);
        identifier_deposit = v.findViewById(R.id.id_number);
        productType = v.findViewById(R.id.productType);
        loanPb = v.findViewById(R.id.loanPb);

        btn_account_search = v.findViewById(R.id.btn_account_search);

        globalContext = this.getActivity();

        sharedPreferences = getActivity().getSharedPreferences(PreferenceFileKeys.PREFS_FILE, Context.MODE_PRIVATE);
        SharedPrefs.write(SharedPrefs.FP_BOOLEAN, "false");


        btn_account_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String identifier_number = identifier_deposit.getText().toString();
                if (identifier_deposit.getText().toString().matches("")) {
                    //toast.swarn("Please enter ID number to proceed!");
                    Toast.makeText(getActivity(), "Please enter ID number to proceed!", Toast.LENGTH_LONG).show();
                } else if (identifier_number.length() < 7) {
                    Toast.makeText(getActivity(), "Please enter a complete ID number!", Toast.LENGTH_LONG).show();
                } else {
                    loanDetailsList.clear();
                    loanDetailsList.add("Loans - Products");
                    requestMemberDetails(identifier_number);
                }
            }
        });

        radioGroupAutharization = v.findViewById(R.id.radioGroupAutharization);
        radioGroupAutharization.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (checkedId == R.id.rbpin) {
                    autmode = "1";
                }
                if (checkedId == R.id.rbbio) {
                    autmode = "2";
                }
            }
        });

        productType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                itemLoan = parent.getItemAtPosition(position).toString();
                String text = productType.getSelectedItem().toString();
                String result = text.substring(0, text.lastIndexOf("-"));


                if (text.equals("Loans - Products") || text.equals("Savings - Products")) {
                    qualifiedPb.setVisibility(View.GONE);
                    minimumPb.setVisibility(View.GONE);
                } else {
                    qualifiedPb.setVisibility(View.VISIBLE);
                    minimumPb.setVisibility(View.VISIBLE);
                    SharedPrefs.productcode(result, getActivity());
                    if (text.length() >= 1) {
                        requestQualifiedAmount(result);
                    }
                }
            }


            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        identifier_deposit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // showMyDialog();
                identifier_deposit.post(new Runnable() {
                    @Override
                    public void run() {
                        identifier_deposit.setSelection(identifier_deposit.getText().toString().length());
                    }
                });
            }
        });

        identifier_deposit.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    // showMyDialog();
                    identifier_deposit.post(new Runnable() {
                        @Override
                        public void run() {
                            identifier_deposit.setSelection(identifier_deposit.getText().toString().length());
                        }
                    });
                }
            }
        });

        qualifiedPb = v.findViewById(R.id.qualifiedPb);
        minimumPb = v.findViewById(R.id.minimumPb);
        id_amount = v.findViewById(R.id.id_amount);


        TextWatcher textWatcherId = new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {


            }

            @Override
            public void afterTextChanged(Editable editable) {

                reqamout.setText("******");
                minamout.setText("******");
                namee.setText("*************");
                crd_score.setText(" ");
                phone.setText("+254********");
                loanDetailsList.clear();
                productType.clearFocus();
            }
        };
        identifier_deposit.addTextChangedListener(textWatcherId);
        rbpin = v.findViewById(R.id.rbpin);
        rbbio = v.findViewById(R.id.rbbio);
        return v;
    }

    @Override
    public void onNextClicked(StepperLayout.OnNextClickedCallback callback) {

    }

    @Override
    public void onCompleteClicked(StepperLayout.OnCompleteClickedCallback callback) {

        if (identifier_deposit.getText().toString().matches("")) {
            Toast.makeText(getActivity(), "Please enter ID to continue", Toast.LENGTH_LONG).show();
        } else if (identifier_deposit.getText().toString().length() < 8) {
            Toast.makeText(getActivity(), "Please enter a complete ID to continue", Toast.LENGTH_LONG).show();
        } else if (namee.getText().toString().matches("") ||
                namee.getText().toString().equals("*************") ||
                phone.getText().toString().equals("+254********") ||
                phone.getText().toString().matches("")) {
            Toast.makeText(getActivity(), "Please confirm the ID number is correct!", Toast.LENGTH_LONG).show();
        }

        else if (reqamout.getText().toString().equals("******")) {
            Toast.makeText(getActivity(), "Please select product", Toast.LENGTH_LONG).show();
        } else if (minamout.getText().toString().equals("******")) {
            Toast.makeText(getActivity(), "Please select product", Toast.LENGTH_LONG).show();
        } else if (reqamout.getText().toString().equals("0") || (reqamout.getText().toString().equals("******"))) {
            Toast.makeText(getActivity(), "You do not qualify for this product, select a different product", Toast.LENGTH_LONG).show();
        } else if (minamout.getText().toString().equals("0") || (minamout.getText().toString().equals("******"))) {
            Toast.makeText(getActivity(), "You do not qualify for this product, select a different product", Toast.LENGTH_LONG).show();
        } else if (minamout.getText().toString().equals("null") || (reqamout.getText().toString().equals("null"))) {
            Toast.makeText(getActivity(), "You do not qualify for loan application", Toast.LENGTH_LONG).show();
        }

        else if (id_amount.getText().toString().matches("")) {
            id_amount.setError("Enter amount");
        }


        else if(Integer.parseInt(id_amount.getText().toString()) < Integer.parseInt(minamout.getText().toString().replace(",",""))
            || Integer.parseInt(id_amount.getText().toString()) > Integer.parseInt(reqamout.getText().toString().replace(",",""))){
            id_amount.setError("Enter the correct amount");
        }

        else {
            loanPb.setVisibility(View.VISIBLE);
            progressDialog = new ProgressDialog(getContext());
            progressDialog.setMessage("Submitting, please wait...");
            progressDialog.setCancelable(false);

            String msg = "Are you sure you want to submit loan application?";
            AlertDialog.Builder builder1 = new AlertDialog.Builder(getContext());
            builder1.setMessage(msg);
            builder1.setTitle("Confirm!");
            builder1.setCancelable(false);

            if (rbpin.isChecked() || rbbio.isChecked()) {

                switch (autmode) {
                    case "1":

                        builder1.setPositiveButton(
                                "YES",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();
                                        progressDialog.show();
                                        submitPinAuthenticatedLoanApplication();
                                    }
                                });

                        builder1.setNegativeButton(
                                "Cancel",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();
                                    }
                                });

                        AlertDialog alert11 = builder1.create();
                        alert11.show();
                        break;
                    case "2":

                        builder1.setPositiveButton(
                                "YES",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();
                                        progressDialog.show();
                                        String identity = identifier_deposit.getText().toString();
                                        requestFingerPrints(getActivity(), identity, loanPb);
                                    }
                                });

                        builder1.setNegativeButton(
                                "Cancel",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();
                                    }
                                });

                        AlertDialog alert12 = builder1.create();
                        alert12.show();
                        break;
                    default:
                        //Toast.makeText(getActivity(), "", Toast.LENGTH_SHORT).show();
                }
            } else {
                loanPb.setVisibility(View.GONE);
                Toast.makeText(getActivity(), "Please select authentication method", Toast.LENGTH_SHORT).show();
            }
         }

    }

    @Override
    public void onBackClicked(final StepperLayout.OnBackClickedCallback callback) {

        callback.goToPrevStep();
    }

    @Override
    public VerificationError verifyStep() {
        return null;
    }

    @Override
    public void onSelected() {

    }

    @Override
    public void onError(@NonNull VerificationError error) {

    }

    void requestQualifiedAmount(final String productLoan) {
        final String phonee = phone.getText().toString();
        String s_output = "";
        try {
            if (phonee.length() == 13) {
                s_output = phonee.substring(1, 13);
            }
        } catch (Exception e) {

        }
        RequestQualifiedAmount requestQualified = new RequestQualifiedAmount();
        requestQualified.idnumber = identifier_deposit.getText().toString();//s_output;
        requestQualified.productcode = productLoan;

        requestQualified.corporateno = MainDashboardActivity.corporateno;
        requestQualified.agentid = SharedPrefs.read(SharedPrefs.AGENT_ID_1, null);
        requestQualified.terminalid = MainDashboardActivity.imei;
        requestQualified.longitude = getLong(getContext());
        requestQualified.latitude = getLat(getContext());
        requestQualified.date = getFormattedDate();
        qualifiedPb.setVisibility(View.VISIBLE);
        minimumPb.setVisibility(View.VISIBLE);
        Api.instance(getActivity()).request(Api.MSACCO_AGENT + Api.GetQualifiedAmount, requestQualified, new Api.RequestListener() {

            @Override
            public void onSuccess(String response) {
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    JSONObject jsonArray = jsonObject.getJSONObject("dcs");
                    String minimum = jsonArray.getString("minimumamount");
                    String maximum = jsonArray.getString("maximumamount");
                    String crdr_score = jsonArray.getString("scoringresponse");
                    minamout.setText(minimum);
                    reqamout.setText(maximum);
                    crd_score.setText(crdr_score);

                    qualifiedPb.setVisibility(View.GONE);
                    minimumPb.setVisibility(View.GONE);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onTokenExpired() {
                qualifiedPb.setVisibility(View.GONE);
                minimumPb.setVisibility(View.GONE);
            }

            @Override
            public void onError(String error) {
                Toast.makeText(getActivity(), "Failed to load", Toast.LENGTH_SHORT).show();
            }
        });

    }

    void submitPinAuthenticatedLoanApplication() {
        String URL = Api.MSACCO_AGENT + Api.mloanApplication;
        final LApplicationRequest reprequest = new LApplicationRequest();
        reprequest.corporate_no = MainDashboardActivity.corporateno;
        reprequest.agentid = SharedPrefs.read(SharedPrefs.AGENT_ID_1, null);
        reprequest.idno = identifier_deposit.getText().toString();
        reprequest.loantype = SharedPrefs.read(SharedPrefs.VR_LOAN_PT_CODE, null);
        reprequest.amount = Double.valueOf(id_amount.getText().toString());
        reprequest.loanapplicationformfrontphoto = SharedPrefs.read(SharedPrefs.VR_LOAN_LEAVE_PHOTO10, null);
        reprequest.loanapplicationformbackphoto =SharedPrefs.read(SharedPrefs.VR_LOAN_BACK_PHOTO, null);
        reprequest.phonenumber = phone.getText().toString();
        reprequest.terminalid = MainDashboardActivity.imei;
        reprequest.longitude = getLong(getContext());
        reprequest.latitude = getLat(getContext());
        reprequest.date = getFormattedDate();

         Api.instance(getActivity()).request(URL, reprequest, new Api.RequestListener() {
            @Override
            public void onSuccess(String response) {

                loanPb.setVisibility(View.GONE);
                final ApplicationSubmissionResponse repaymentResponse = Api.instance(getContext())
                        .mGson.fromJson(response, ApplicationSubmissionResponse.class);

                if (repaymentResponse.operationsuccess) {
                    progressDialog.dismiss();
                    final String agentName = sharedPreferences.getString(PreferenceFileKeys.AGENT_ACCOUNT_NAME, "");
                    final String saccoMotto = sharedPreferences.getString(PreferenceFileKeys.AGENT_SACCO_MOTTO, "");
                    final String saccoName = sharedPreferences.getString(PreferenceFileKeys.AGENT_SACCO_NAME, "");
                    final String loan = productType.getSelectedItem().toString();
                    final String loanresult = loan.substring(loan.lastIndexOf("-") + 1);
                    Utils.showAlertDialog(getActivity(), "Loan application", "Loan application successfully submited", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            PrinterInterface.open();
                            writetest(agentName, Double.valueOf(id_amount.getText().toString()), loanresult, saccoName, saccoMotto, repaymentResponse.getReceiptNo(), repaymentResponse.getTransactiondate(), "Loan Application");
                            //PrinterInterface.close();
                            progressDialog.dismiss();

                            Utils.showAlertDialog(getActivity(), "Loan application", " Press Ok to print another Receipt ",
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            PrinterInterface.open();
                                            writetest1(agentName, Double.valueOf(id_amount.getText().toString()), loanresult, saccoName, saccoMotto, repaymentResponse.getReceiptNo(), repaymentResponse.getTransactiondate(), "Loan Application");


                                            SharedPrefs.write(SharedPrefs.VR_LOAN_PT_CODE, "null");
                                            SharedPrefs.write(SharedPrefs.VR_LOAN_LEAVE_PHOTO10, "null");
                                            SharedPrefs.write(SharedPrefs.VR_LOAN_BACK_PHOTO, "null");

                                            getActivity().finish();
                                            Intent i = new Intent(getActivity(), MainDashboardActivity.class);
                                            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                                            i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                            startActivity(i);
                                        }
                                    });
                        }
                    });
                } else {
                    progressDialog.dismiss();
                    Toast.makeText(getContext(), "Loan application failed. Try again!", Toast.LENGTH_LONG).show();
                }

            }

            @Override
            public void onTokenExpired() {

            }

            @Override
            public void onError(String error) {

            }
        });
    }

    void submitBioAuthenticatedLoanApplication() {
        String URL = Api.MSACCO_AGENT + Api.mloanApplication;
        final LApplicationRequest reprequest = new LApplicationRequest();
        reprequest.corporate_no = MainDashboardActivity.corporateno;
        reprequest.agentid = SharedPrefs.read(SharedPrefs.AGENT_ID_1, null);
        reprequest.idno = identifier_deposit.getText().toString();
        reprequest.loantype = SharedPrefs.read(SharedPrefs.VR_LOAN_PT_CODE, null);
        reprequest.amount = Double.valueOf(id_amount.getText().toString());
        reprequest.loanapplicationformfrontphoto =SharedPrefs.read(SharedPrefs.VR_LOAN_LEAVE_PHOTO10, null);
        reprequest.loanapplicationformbackphoto = SharedPrefs.read(SharedPrefs.VR_LOAN_BACK_PHOTO, null);
        reprequest.phonenumber = phone.getText().toString();
        reprequest.terminalid = MainDashboardActivity.imei;
        reprequest.longitude = getLong(getContext());
        reprequest.latitude = getLat(getContext());
        reprequest.date = getFormattedDate();
        Api.instance(getContext()).bioauthrequest(URL, reprequest, new Api.RequestListener() {
            @Override
            public void onSuccess(String response) {

                loanPb.setVisibility(View.GONE);
                final ApplicationSubmissionResponse repaymentResponse = Api.instance(getContext())
                        .mGson.fromJson(response, ApplicationSubmissionResponse.class);

                if (repaymentResponse.operationsuccess) {
                    progressDialog.dismiss();
                    final String agentName = sharedPreferences.getString(PreferenceFileKeys.AGENT_ACCOUNT_NAME, "");
                    final String saccoMotto = sharedPreferences.getString(PreferenceFileKeys.AGENT_SACCO_MOTTO, "");
                    final String saccoName = sharedPreferences.getString(PreferenceFileKeys.AGENT_SACCO_NAME, "");
                    final String loan = productType.getSelectedItem().toString();
                    final String loanresult = loan.substring(loan.lastIndexOf("-") + 1);

                    Utils.showAlertDialog(getActivity(), "Loan application", "Loan application successfully submited", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            PrinterInterface.open();
                            writetest(agentName, Double.valueOf(id_amount.getText().toString()), loanresult, saccoName, saccoMotto, repaymentResponse.getReceiptNo(), repaymentResponse.getTransactiondate(), "Loan Application");
                            //PrinterInterface.close();
                            progressDialog.dismiss();

                            Utils.showAlertDialog(getActivity(), "Loan application", " Press Ok to print another Receipt ",
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            PrinterInterface.open();
                                            writetest1(agentName, Double.valueOf(id_amount.getText().toString()), loanresult, saccoName, saccoMotto, repaymentResponse.getReceiptNo(), repaymentResponse.getTransactiondate(), "Loan Application");


                                            SharedPrefs.write(SharedPrefs.VR_LOAN_PT_CODE, "null");
                                            SharedPrefs.write(SharedPrefs.VR_LOAN_LEAVE_PHOTO10, "null");
                                            SharedPrefs.write(SharedPrefs.VR_LOAN_BACK_PHOTO, "null");

                                            getActivity().finish();
                                            Intent i = new Intent(getActivity(), MainDashboardActivity.class);
                                            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                                            i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                            startActivity(i);
                                        }
                                    });
                        }
                    });

                } else {
                    progressDialog.dismiss();
                    Toast.makeText(getContext(), "Loan application failed. Try again!", Toast.LENGTH_LONG).show();
                }

            }

            @Override
            public void onTokenExpired() {

            }

            @Override
            public void onError(String error) {

            }
        });
    }

    void requestMemberDetails(String id) {
        RequestMemberName requestMemberName = new RequestMemberName();
        requestMemberName.corporateno = MainDashboardActivity.corporateno;
        requestMemberName.accountidentifier = id;
        requestMemberName.accountcode = "0";
        requestMemberName.agentid = SharedPrefs.read(SharedPrefs.AGENT_ID_1, null);////
        requestMemberName.terminalid = MainDashboardActivity.imei;
        loading.setVisibility(View.VISIBLE);

        Api.instance(getActivity()).request(Api.MSACCO_AGENT + Api.GetMembername, requestMemberName, new Api.RequestListener() {
            @Override
            public void onSuccess(String response) {
                loading.setVisibility(View.GONE);
                qualifiedPb.setVisibility(View.GONE);
                minimumPb.setVisibility(View.GONE);

                MemberNameResponse memberNameResponse = Api.instance(getContext()).mGson.fromJson(response, MemberNameResponse.class);
                if (memberNameResponse.is_successful) {
                    namee.setText(memberNameResponse.getCustomer_name());
                    phone.setText(memberNameResponse.getPhoneno());

                    //checkDefaulter(text);
                    requestLoanProducts();
                } else {
                    loading.setVisibility(View.VISIBLE);
                    loading.setText("Failed, input the correct ID number");
                }

            }

            @Override
            public void onTokenExpired() {
                loading.setVisibility(View.GONE);
            }

            @Override
            public void onError(String error) {
                loading.setText("failed to load..");
            }
        });
    }


    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    void requestLoanProducts() {
        String URL = Api.MSACCO_AGENT + Api.GetmLoanProducts;
        final GetMLoanProductsRequest loansProducts = new GetMLoanProductsRequest();
        loansProducts.corporateno = MainDashboardActivity.corporateno;

        loansProducts.productioncode = "2";
        loansProducts.idnumber = identifier_deposit.getText().toString();
        loansProducts.agentid = SharedPrefs.read(SharedPrefs.AGENT_ID_1, null);
        loansProducts.terminalid = MainDashboardActivity.imei;
        loansProducts.longitude = getLong(getContext());
        loansProducts.latitude = getLat(getContext());

        Api.instance(getContext()).request(URL, loansProducts, new Api.RequestListener() {
            @Override
            public void onSuccess(String response) {

                GetMLoanProductsResponse loanProductsResponse = Api.instance(getContext())
                        .mGson.fromJson(response, GetMLoanProductsResponse.class);
                if (loanProductsResponse.is_successful) {
                    qualifiedPb.setVisibility(View.GONE);
                    minimumPb.setVisibility(View.GONE);
                    try {
                        // tvLoadProducts.setVisibility(View.GONE);
                        JSONObject jsonObject = new JSONObject(response);
                        JSONArray jsonArray = jsonObject.getJSONArray("product");

                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject loanObjects = jsonArray.getJSONObject(i);
                            code = loanObjects.getString("productid");
                            desc = loanObjects.getString("description");

                            loanDetailsList.add(code + "-  " + desc);

                        }
                        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_spinner_item, loanDetailsList);
                        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                        productType.setAdapter(adapter);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                } else {

                }

            }

            @Override
            public void onTokenExpired() {

            }

            @Override
            public void onError(String error) {
                //loadProductsError();
            }
        });
    }

    public void writetest(String agentName, double loanAmt, String loanAccNo, String saccoName, String saccoMotto, String receiptNum, String transactionDate, String transactionType) {
        try {

            byte[] arryBeginText = null;
            // byte[] arryAccType = null;
            byte[] arryAccNo = null;
            byte[] arryAmt = null;
            byte[] arryMotto = null;
            byte[] terminalNo = null;
            //byte[] receiptNo = null;
            byte[] date = null;
            byte[] transactionTYpe = null;
            byte[] servedBy = null;

            byte[] signature = null;
            byte[] id = null;

            byte[] customerReceipt = null;

            try {
                arryBeginText = String.valueOf("     " + saccoName).getBytes("GB2312");
                terminalNo = String.valueOf("Terminal ID" + Utils.getIMEI(getActivity())).getBytes("GB2312");
                //receiptNo = String.valueOf(" Receipt Number : " + receiptNum).getBytes("GB2312");
                date = String.valueOf("Date : " + transactionDate).getBytes("GB2312");
                transactionTYpe = String.valueOf("    " + transactionType).getBytes("GB2312");
                //arryAccType = "Your Loan Has been submitted successfully ".getBytes("GB2312");
                arryAccNo = String.valueOf("Loan Type :" + loanAccNo).getBytes("GB2312");
                arryAmt = String.valueOf("Amount" + SharedPrefs.read(SharedPrefs.CURRENCY, null) + " :" + loanAmt).getBytes("GB2312");

                id = String.valueOf("ID :_______________________").getBytes("GB2312");
                signature = String.valueOf("SIGNATURE : _____________").getBytes("GB2312");

                servedBy = String.valueOf("You were Served By : " + agentName).getBytes("GB2312");
                arryMotto = String.valueOf("     " + saccoMotto).getBytes("GB2312");

                customerReceipt = String.valueOf("         CUSTOMER COPY         ").getBytes("GB2312");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            PrinterInterface.begin();

            byte[] cmd;
            cmd = new byte[]{(byte) 0x1B, (byte) 0x40};

            cmd = new byte[]{
                    (byte) 0x1B, (byte) 0x45, (byte) 0};

            write(cmd);
            write(arryBeginText);
            writeLineBreak(2);
            write(terminalNo);
            writeLineBreak(1);
            //write(receiptNo);
            writeLineBreak(1);
            write(date);
            writeLineBreak(1);
            write(transactionTYpe);
            // print line break
            writeLineBreak(2);

            // print line break
            writeLineBreak(1);
            // print text
            // write(arryAccType);
            write(arryAccNo);
            writeLineBreak(1);
            write(arryAmt);
            // print line break
            writeLineBreak(2);

            write(id);
            writeLineBreak(2);
            write(signature);
            writeLineBreak(2);
            write(servedBy);
            writeLineBreak(2);
            write(arryMotto);
            writeLineBreak(3);
            write(customerReceipt);
            writeLineBreak(4);
            PrinterInterface.end();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public void writetest1(String agentName, double loanAmt, String loanAccNo, String saccoName, String saccoMotto, String receiptNum, String transactionDate, String transactionType) {
        try {

            byte[] arryBeginText = null;
            // byte[] arryAccType = null;
            byte[] arryAccNo = null;
            byte[] arryAmt = null;
            byte[] arryMotto = null;
            byte[] terminalNo = null;
            //byte[] receiptNo = null;
            byte[] date = null;
            byte[] transactionTYpe = null;
            byte[] servedBy = null;

            byte[] signature = null;
            byte[] id = null;

            byte[] customerReceipt = null;

            try {
                arryBeginText = String.valueOf("     " + saccoName).getBytes("GB2312");
                terminalNo = String.valueOf("Terminal ID" + Utils.getIMEI(getActivity())).getBytes("GB2312");
                //receiptNo = String.valueOf(" Receipt Number : " + receiptNum).getBytes("GB2312");
                date = String.valueOf("Date : " + transactionDate).getBytes("GB2312");
                transactionTYpe = String.valueOf("    " + transactionType).getBytes("GB2312");
                //arryAccType = "Your Loan Has been submitted successfully ".getBytes("GB2312");
                arryAccNo = String.valueOf("Loan Type :" + loanAccNo).getBytes("GB2312");
                arryAmt = String.valueOf("Amount" + SharedPrefs.read(SharedPrefs.CURRENCY, null) + " :" + loanAmt).getBytes("GB2312");

                id = String.valueOf("ID :_______________________").getBytes("GB2312");
                signature = String.valueOf("SIGNATURE : _____________").getBytes("GB2312");

                servedBy = String.valueOf("You were Served By : " + agentName).getBytes("GB2312");
                arryMotto = String.valueOf("     " + saccoMotto).getBytes("GB2312");

                customerReceipt = String.valueOf("         AGENT COPY         ").getBytes("GB2312");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            PrinterInterface.begin();

            byte[] cmd;
            cmd = new byte[]{(byte) 0x1B, (byte) 0x40};

            cmd = new byte[]{
                    (byte) 0x1B, (byte) 0x45, (byte) 0};

            write(cmd);
            write(arryBeginText);
            writeLineBreak(2);
            write(terminalNo);
            writeLineBreak(1);
            //write(receiptNo);
            writeLineBreak(1);
            write(date);
            writeLineBreak(1);
            write(transactionTYpe);
            // print line break
            writeLineBreak(2);

            writeLineBreak(1);
            // print text
            // write(arryAccType);
            write(arryAccNo);
            writeLineBreak(1);
            write(arryAmt);
            // print line break
            writeLineBreak(2);

            write(id);
            writeLineBreak(2);
            write(signature);
            writeLineBreak(2);
            write(servedBy);
            writeLineBreak(2);
            write(arryMotto);
            writeLineBreak(3);
            write(customerReceipt);
            writeLineBreak(4);
            PrinterInterface.end();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    private void write(final byte[] arryData) {
        int result = PrinterInterface.write(arryData, arryData.length);
    }
    private void writeLineBreak(int lineNumber) {
        write(PrinterCommand.getCmdEscDN(lineNumber));
    }

    @Override
    public void onResume() {
        SharedPrefs.init(getContext());
        super.onResume();
        String fpBoolean = SharedPrefs.read(SharedPrefs.FP_BOOLEAN, null);

        if (fpBoolean.equals("true")) {
            submitBioAuthenticatedLoanApplication();
        } else if (fpBoolean.equals("false")) {
           // Toast.makeText(getContext(), "Biometrics Authentication shall requitre Customer's fingerprints!", Toast.LENGTH_SHORT).show();
        }

    }

}
