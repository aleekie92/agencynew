package com.coretec.agencyApplication.fragments.newmodule;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.cloudpos.DeviceException;
import com.cloudpos.OperationResult;
import com.cloudpos.POSTerminal;
import com.cloudpos.TimeConstants;
import com.cloudpos.fingerprint.Fingerprint;
import com.cloudpos.fingerprint.FingerprintDevice;
import com.cloudpos.fingerprint.FingerprintOperationResult;
import com.coretec.agencyApplication.R;
import com.coretec.agencyApplication.activities.LoginActivity;
import com.coretec.agencyApplication.activities.gfl.MainGFLDashboard;
import com.coretec.agencyApplication.activities.newmodules.MainDashboardActivity;
import com.coretec.agencyApplication.activities.newmodules.ValidateTunzhengbigBio;
import com.coretec.agencyApplication.api.Api;
import com.coretec.agencyApplication.api.requests.FlexibleMenuRequest;
import com.coretec.agencyApplication.api.requests.newmodulesrequests.GlobalVRequest;
import com.coretec.agencyApplication.api.responses.FlexibleMenuResponse;
import com.coretec.agencyApplication.api.responses.newmoduleresponses.GlobalVResponse;
import com.coretec.agencyApplication.utils.ByteConvertStringUtil;
import com.coretec.agencyApplication.utils.LogHelper;
import com.coretec.agencyApplication.utils.SharedPrefs;
import com.coretec.agencyApplication.utils.Utils;
import com.stepstone.stepper.BlockingStep;
import com.stepstone.stepper.StepperLayout;
import com.stepstone.stepper.VerificationError;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;

import static com.coretec.agencyApplication.utils.Utils.getFormattedDate;
import static com.coretec.agencyApplication.utils.Utils.getLat;
import static com.coretec.agencyApplication.utils.Utils.getLong;

public class TunzhengbigBioEnrollment extends Fragment implements BlockingStep, View.OnClickListener {

    Button leftRing, leftLittle, rightRing, rightLittle;
    Button delete;
    Button clear;
    Button quit;
    TextView enrollText;

    ProgressDialog progressDialog;
    protected TextView log_text,coplete;
    protected Handler mHandler = null;
    protected Runnable runnable = null;
    private LinearLayout enrollButtons;

    private RelativeLayout RelativeLayout02;
    private TextView show;

    private int userID = 0;
    private int timeout = 10 * 1000;
   // private FingerprintDevice fingerprintDevice = null;

    private SharedPreferences preferences;
    private SharedPreferences.Editor editor;

    private static final String FINGERINDEX1 = "Finger1";
    private static final String FINGERINDEX2 = "Finger2";
    private static final String FINGERINDEX3 = "Finger3";
    private static final String FINGERINDEX4 = "Finger4";

    private Handler handler;
    private static final int SHOW_NORMAL_MESSAGE = 0;
    private static final int SHOW_SUCCESS_MESSAGE = 1;
    private static final int SHOW_FAIL_MESSAGE = 2;
    private static final int SHOW_BTN = 3;
    private static final int HIDE_BTN = 4;
    Thread th = null;
    SharedPreferences sharedPreferences;

    FingerprintDevice fingerprintDevice = null;
    @SuppressLint("HandlerLeak")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        SharedPrefs.init(getContext());
        View v = inflater.inflate(R.layout.final_fingerprint, container, false);

        fingerprintDevice=(FingerprintDevice) POSTerminal.getInstance(getActivity()).getDevice("cloudpos.device.fingerprint");
        leftRing = v.findViewById(R.id.btn_left_ring);
        leftLittle = v.findViewById(R.id.btn_left_little);
        rightRing = v.findViewById(R.id.btn_right_ring);
        rightLittle = v.findViewById(R.id.btn_right_little);
        delete = v.findViewById(R.id.ButtonDelete);
        clear = v.findViewById(R.id.ButtonClear);
        show = v.findViewById(R.id.EnrollmentTextView);
        enrollButtons = v.findViewById(R.id.enrollButtons);
        RelativeLayout02 = v.findViewById(R.id.RelativeLayout02);
        coplete = v.findViewById(R.id.coplete);
        dynamicMenu();

        leftRing.setOnClickListener(this);
        leftLittle.setOnClickListener(this);
        rightRing.setOnClickListener(this);
        rightLittle.setOnClickListener(this);
        delete.setOnClickListener(this);
        clear.setOnClickListener(this);


        fingerprintDevice = (FingerprintDevice) POSTerminal.getInstance(getActivity()).getDevice("cloudpos.device.fingerprint");
        preferences = this.getActivity().getSharedPreferences("userFinger", Context.MODE_PRIVATE);

        editor = preferences.edit();
        editor.putString(FINGERINDEX1, "0");
        editor.putString(FINGERINDEX2, "0");
        editor.putString(FINGERINDEX3, "0");
        editor.putString(FINGERINDEX4, "0");
        editor.apply();

        show.setMovementMethod(ScrollingMovementMethod.getInstance());

        handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                super.handleMessage(msg);
                switch (msg.what) {
                    case SHOW_NORMAL_MESSAGE:
                        sendMsg((String) msg.obj);
                        scrollLogView();
                        break;
                    case SHOW_SUCCESS_MESSAGE:
                        sendSuccessMsg((String) msg.obj);
                        scrollLogView();
                        break;
                    case SHOW_FAIL_MESSAGE:
                        sendFailMsg((String) msg.obj);
                        scrollLogView();
                        break;

                    case SHOW_BTN:

                        break;
                    case HIDE_BTN:

                        break;
                }
            }
        };

        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    int result = fingerprintDevice.delAllFingers();
                    if (result >= 0) {
                        handler.obtainMessage(SHOW_SUCCESS_MESSAGE, getStr(R.string.Cleared)).sendToTarget();
                    }
                } catch (DeviceException e) {
                    e.printStackTrace();
                    handler.obtainMessage(SHOW_FAIL_MESSAGE, getStr(R.string.MatchFailed)).sendToTarget();
                }
                Toast.makeText(getActivity(), getActivity().getText(R.string.Cleared), Toast.LENGTH_SHORT).show();
            }
        });
        clear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                show.setText("");
            }
        });

        return v;
    }

    private void fingerPut(String fingerIndex) {
        preferences = this.getActivity().getSharedPreferences("userFinger", Context.MODE_PRIVATE);
        editor = preferences.edit();
        Fingerprint fingerprint = getFingerprint();
        if (fingerprint != null) {
            byte[] buffer1 = fingerprint.getFeature();
            Log.e("FINGERPRINT_DEMO", buffer1.length + "");
            editor.putString(fingerIndex, ByteConvertStringUtil.bytesToHexString(buffer1));
            editor.apply();
        }
        handler.obtainMessage(SHOW_SUCCESS_MESSAGE, this.getActivity().getString(R.string.entry) + fingerIndex).sendToTarget();
    }

    private Fingerprint getFingerprint() {
        Fingerprint fingerprint = null;
        try {
            FingerprintOperationResult operationResult = fingerprintDevice.waitForFingerprint(TimeConstants.FOREVER);
            if (operationResult.getResultCode() == OperationResult.SUCCESS) {
                fingerprint = operationResult.getFingerprint(0, 0);
                Log.e("FINGERPRINT_DEMO", "operationResult SUCCESS!" + fingerprint.getFeature().length);
                //handler.obtainMessage(SHOW_SUCCESS_MESSAGE, mContext.getString(R.string.SUCCESSINFO)).sendToTarget();
            } else {
                handler.obtainMessage(SHOW_FAIL_MESSAGE, this.getActivity().getString(R.string.FAILEDINFO)).sendToTarget();
            }
        } catch (DeviceException e) {
            e.printStackTrace();
            handler.obtainMessage(SHOW_FAIL_MESSAGE, this.getActivity().getString(R.string.DEVICEFAILED)).sendToTarget();
        }
        return fingerprint;
    }

    private void sendSuccessMsg(String msg) {
        LogHelper.infoAppendMsgForSuccess(msg + "\n", show);
        scrollLogView();
    }

    private void sendFailMsg(String msg) {
        LogHelper.infoAppendMsgForFailed(msg + "\n", show);
        scrollLogView();
    }

    private void sendMsg(String msg) {
        LogHelper.infoAppendMsg(msg + "\n", show);
        scrollLogView();
    }

    public void scrollLogView() {
        int offset = show.getLineCount() * show.getLineHeight();
        if (offset > show.getHeight()) {
            show.scrollTo(0, offset - show.getHeight());
        }
    }

    private String getStr(int strId) {
        return getResources().getString(strId);
    }

    @Override
    public void onClick(View v) {

        if (v.getId() != R.id.ButtonDelete) {
            sendMsg(getStr(R.string.MESSAGE));
        }
        if (v.getId() == R.id.ButtonDelete || v.getId() == R.id.ButtonClear) {
            switch (v.getId()) {
                case R.id.ButtonDelete:
                    try {
                        int result = fingerprintDevice.delAllFingers();
                        if (result >= 0) {
                            handler.obtainMessage(SHOW_SUCCESS_MESSAGE, getStr(R.string.Cleared)).sendToTarget();
                        }
                    } catch (DeviceException e) {
                        e.printStackTrace();
                        handler.obtainMessage(SHOW_FAIL_MESSAGE, getStr(R.string.MatchFailed)).sendToTarget();
                    }

                    break;
                case R.id.ButtonClear:
                    show.setText("");
                    break;
            }
        } else {
            if (th == null || th.getState() == Thread.State.TERMINATED) {
                switch (v.getId()) {
                    case R.id.btn_left_ring:
                        th = new Thread() {
                            @Override
                            public void run() {
                                // enroll();
                                fingerPut(FINGERINDEX1);
                            }
                        };

                        break;
                    case R.id.btn_left_little:
                        th = new Thread() {
                            @Override
                            public void run() {
                                // enroll();
                                fingerPut(FINGERINDEX2);
                            }
                        };
                        break;
                    case R.id.btn_right_ring:
                        th = new Thread() {
                            @Override
                            public void run() {
                                // enroll();
                                fingerPut(FINGERINDEX3);
                            }
                        };
                        break;
                    case R.id.btn_right_little:
                        th = new Thread() {
                            @Override
                            public void run() {
                                //enroll();
                                fingerPut(FINGERINDEX4);
                            }
                        };
                        break;

                }
                th.start();
            }
        }

    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onNextClicked(final StepperLayout.OnNextClickedCallback callback) {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                callback.goToNextStep();
            }
        }, 0L);// delay open another fragment,
    }

    @Override
    public void onCompleteClicked(StepperLayout.OnCompleteClickedCallback callback) {

        String msg = "Are you sure you want to submit customer registration?";
        //final String qualKgs = SharedPrefs.read(SharedPrefs.LOAN_QUALIFIED_KGS, null);
        AlertDialog.Builder builder1 = new AlertDialog.Builder(getContext());
        builder1.setMessage(msg);
        builder1.setTitle("Confirm!");
        builder1.setCancelable(false);

        builder1.setPositiveButton(
                "YES",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                        progressfordynamicMenu();
                    }
                });

        builder1.setNegativeButton(
                "Cancel",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

        AlertDialog alert11 = builder1.create();
        alert11.show();

    }

    @Override
    public void onBackClicked(final StepperLayout.OnBackClickedCallback callback) {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                callback.goToPrevStep();
            }
        }, 0L);// delay open another fragment,
    }

    @Override
    public VerificationError verifyStep() {
        return null;
    }

    @Override
    public void onSelected() {

    }

    @Override
    public void onError(@NonNull VerificationError error) {

    }

    void submitCustomerRegistration() {
        SharedPreferences prefs = getActivity().getSharedPreferences("userFinger", Context.MODE_PRIVATE);
        String finger1 = prefs.getString("Finger1", null);
        String finger2 = prefs.getString("Finger2", null);
        String finger3 = prefs.getString("Finger3", null);
        String finger4 = prefs.getString("Finger4", null);

        String URL = Api.MSACCO_AGENT + Api.CreateVirtualRegistration;
        SimpleDateFormat timeStampFormat = new SimpleDateFormat("yyyy/MM/dd - HH:mm");
        final GlobalVRequest globalVRequest = new GlobalVRequest();
        globalVRequest.name = SharedPrefs.read(SharedPrefs.VR_NAMES, null);
        globalVRequest.idno = SharedPrefs.read(SharedPrefs.VR_ID, null);
        globalVRequest.phoneno = SharedPrefs.read(SharedPrefs.VR_PHONE, null);
        globalVRequest.gender = SharedPrefs.read(SharedPrefs.VR_GENDER, null);
        globalVRequest.amount = SharedPrefs.read(SharedPrefs.VR_AMOUNT, null);
        globalVRequest.dob =  SharedPrefs.read(SharedPrefs.VR_DOB, null);//myDate;
        globalVRequest.passportphoto = SharedPrefs.read(SharedPrefs.VR_PHOTO, null);
        globalVRequest.maritalstatus = SharedPrefs.read(SharedPrefs.VR_STATUS, null);
        globalVRequest.citizenship = SharedPrefs.read(SharedPrefs.VR_CITIZEN, null);
        globalVRequest.email = SharedPrefs.read(SharedPrefs.VR_EMAIL, null);
        globalVRequest.corporate_no = MainDashboardActivity.corporateno;
        globalVRequest.idfrontphoto = SharedPrefs.read(SharedPrefs.VR_FRONT_ID, null);
        globalVRequest.idbackphoto = SharedPrefs.read(SharedPrefs.VR_BACK_ID, null);
        globalVRequest.signphoto = SharedPrefs.read(SharedPrefs.VR_SIGNATURE, null);
        globalVRequest.nextofkin = SharedPrefs.read(SharedPrefs.VR_REG_FULL_NAME, null);
        globalVRequest.nextofkinid = SharedPrefs.read(SharedPrefs.VR_REG_FULL_ID, null);
        globalVRequest.nextofkinphone = SharedPrefs.read(SharedPrefs.VR_REG_FULL_PHONE, null);
        globalVRequest.relationship = SharedPrefs.read(SharedPrefs.VR_REG_RELATIONSHIP, null);
        globalVRequest.msaccoregister = SharedPrefs.read("VR_PU_REGISTERED", SharedPrefs.VR_PU_REGISTERED);
        globalVRequest.leftring = finger1;
        globalVRequest.leftlittle = finger2;
        globalVRequest.rightring = finger3;
        globalVRequest.rightlittle = finger4;
        globalVRequest.transactiontype = "1";
        globalVRequest.agentid = SharedPrefs.read(SharedPrefs.AGENT_ID_1, null);
        globalVRequest.terminalid = MainDashboardActivity.imei;
        globalVRequest.longitude = getLong(getContext());
        globalVRequest.latitude = getLat(getContext());
        globalVRequest.date = getFormattedDate();
        globalVRequest.salutation = SharedPrefs.read(SharedPrefs.VR_SALUTATION, null);
        globalVRequest.nearestbranch = SharedPrefs.read(SharedPrefs.VR_BRANCH, null);
        globalVRequest.locality = SharedPrefs.read(SharedPrefs.VR_LOCALITY, null);
        globalVRequest.nearestmarket = SharedPrefs.read(SharedPrefs.VR_NEARESTMARKET, null);
        globalVRequest.applformfront = SharedPrefs.read(SharedPrefs.VR_REG_FORMFRONT, null);
        globalVRequest.applformback = SharedPrefs.read(SharedPrefs.VR_REG_FORMBACK, null);
        globalVRequest.employeecode = SharedPrefs.read(SharedPrefs.VR_EMPLOYER, null);
        globalVRequest.payrollno = SharedPrefs.read(SharedPrefs.VR_EMPLOYER_NUMBER, null);
        globalVRequest.nextofkindob = SharedPrefs.read(SharedPrefs.VR_KIN_DOB, null);
        globalVRequest.nextofkingender = SharedPrefs.read(SharedPrefs.VR_KIN_GENDER, null);
        Api.instance(getContext()).request(URL, globalVRequest, new Api.RequestListener() {
            @Override
            public void onSuccess(String response) {

                GlobalVResponse globalVResponse = Api.instance(getContext())
                        .mGson.fromJson(response, GlobalVResponse.class);

                if (globalVResponse.operationsuccess){
                    progressDialog.dismiss();
                    String msg = "Customer registration submitted successfully!";
                    AlertDialog.Builder builder1 = new AlertDialog.Builder(getContext());
                    builder1.setMessage(msg);
                    builder1.setTitle("Success!");
                    builder1.setCancelable(false);
                    builder1.setPositiveButton(
                            "DONE",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();

                                    SharedPrefs.write(SharedPrefs.VR_NAMES, "null");
                                    SharedPrefs.write(SharedPrefs.VR_ID, "null");
                                    SharedPrefs.write(SharedPrefs.VR_PHONE, "null");
                                    SharedPrefs.write(SharedPrefs.VR_GENDER, "null");
                                    SharedPrefs.write(SharedPrefs.VR_AMOUNT, "null");
                                    SharedPrefs.write(SharedPrefs.VR_DOB, "null");
                                    SharedPrefs.write(SharedPrefs.VR_PHOTO, "null");
                                    SharedPrefs.write(SharedPrefs.VR_STATUS, "null");
                                    SharedPrefs.write(SharedPrefs.VR_CITIZEN, "null");
                                    SharedPrefs.write(SharedPrefs.VR_EMAIL, "null");
                                    //SharedPrefs.write("PU_REGISTERED", SharedPrefs.PU_REGISTERED);
                                    SharedPrefs.write(SharedPrefs.VR_FRONT_ID, "null");
                                    SharedPrefs.write(SharedPrefs.VR_BACK_ID, "null");
                                    SharedPrefs.write(SharedPrefs.VR_SIGNATURE, "null");

                                    SharedPrefs.write(SharedPrefs.VR_REG_FULL_NAME, "null");
                                    SharedPrefs.write(SharedPrefs.VR_REG_FULL_ID, "null");
                                    SharedPrefs.write(SharedPrefs.VR_REG_FULL_PHONE, "null");
                                    SharedPrefs.write(SharedPrefs.VR_REG_RELATIONSHIP, "null");

                                    SharedPrefs.write(SharedPrefs.VR_SALUTATION, "null");
                                    SharedPrefs.write(SharedPrefs.VR_BRANCH, "null");
                                    SharedPrefs.write(SharedPrefs.VR_LOCALITY, "null");
                                    SharedPrefs.write(SharedPrefs.VR_NEARESTMARKET, "null");
                                    SharedPrefs.write(SharedPrefs.VR_REG_FORMFRONT, "null");
                                    SharedPrefs.write(SharedPrefs.VR_REG_FORMBACK, "null");
                                    SharedPrefs.write(SharedPrefs.VR_EMPLOYER, "null");

                                    SharedPrefs.write(SharedPrefs.VR_EMPLOYER_NUMBER, "null");
                                    SharedPrefs.write(SharedPrefs.VR_KIN_DOB, "null");
                                    SharedPrefs.write(SharedPrefs.VR_KIN_GENDER, "null");

                                    getActivity().finish();
                                    Intent i=new Intent();
                                    if (SharedPrefs.read(SharedPrefs.COPORATE, null).equals("gfl")){
                                        i = new Intent(getActivity(), MainGFLDashboard.class);
                                    }if(SharedPrefs.read(SharedPrefs.COPORATE, null).equals("notgfl")) {
                                        i = new Intent(getActivity(), MainDashboardActivity.class);
                                    }
                                    i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                                    i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                    startActivity(i);
                                }
                            });

                    AlertDialog alert11 = builder1.create();
                    alert11.show();
                } else {
                    progressDialog.dismiss();
                    Utils.showAlertDialog(getActivity(),"Failed", globalVResponse.getError());
                    //Toast.makeText(getContext(), "Customer registration failed. Try again later!" +globalVResponse.getError(), Toast.LENGTH_LONG).show();
                }

            }

            @Override
            public void onTokenExpired() {

            }

            @Override
            public void onError(String error) {
                progressDialog.dismiss();
                Utils.showAlertDialog(getActivity(),"Failed",error);
            }
        });

    }

    public void dynamicMenu(){
        final FlexibleMenuRequest dynamicMenu = new FlexibleMenuRequest();
        dynamicMenu.agentid = SharedPrefs.read(SharedPrefs.AGENT_ID_1, null);
        dynamicMenu.terminalid = MainDashboardActivity.imei;
        dynamicMenu.longitude = getLong(getActivity());
        dynamicMenu.latitude = getLat(getActivity());
        dynamicMenu.date = getFormattedDate();
        dynamicMenu.corporate_no = MainDashboardActivity.corporateno;

        Api.instance(getActivity()).request(Api.MSACCO_AGENT + Api.DynamicMenu, dynamicMenu, new Api.RequestListener() {
            @Override
            public void onSuccess(String response) {
                FlexibleMenuResponse dynamicmenuresponse = Api.instance(getActivity()).mGson.fromJson(response, FlexibleMenuResponse.class);
                if (dynamicmenuresponse.operationsuccess) {
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        String usesbio = jsonObject.getString("usesbioauthentication");
                        if (usesbio=="true"){
                            enrollButtons.setVisibility(View.VISIBLE);
                            RelativeLayout02.setVisibility(View.VISIBLE);
                            coplete.setVisibility(View.GONE);
                        }else {
                            enrollButtons.setVisibility(View.GONE);
                            RelativeLayout02.setVisibility(View.GONE);
                            coplete.setVisibility(View.VISIBLE);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    Utils.showAlertDialog(getActivity(), "Failed", dynamicmenuresponse.getError());
                }

            }

            @Override
            public void onTokenExpired() {

            }

            @Override
            public void onError(String error) {
                Utils.showAlertDialog(getActivity(), "Failed", error);
            }
        });
    }

    public void progressfordynamicMenu(){
        final FlexibleMenuRequest dynamicMenu = new FlexibleMenuRequest();
        dynamicMenu.agentid = SharedPrefs.read(SharedPrefs.AGENT_ID_1, null);
        dynamicMenu.terminalid = MainDashboardActivity.imei;
        dynamicMenu.longitude = getLong(getActivity());
        dynamicMenu.latitude = getLat(getActivity());
        dynamicMenu.date = getFormattedDate();
        dynamicMenu.corporate_no = MainDashboardActivity.corporateno;

        Api.instance(getActivity()).request(Api.MSACCO_AGENT + Api.DynamicMenu, dynamicMenu, new Api.RequestListener() {
            @Override
            public void onSuccess(String response) {
                FlexibleMenuResponse dynamicmenuresponse = Api.instance(getActivity()).mGson.fromJson(response, FlexibleMenuResponse.class);
                if (dynamicmenuresponse.operationsuccess) {
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        String usesbio = jsonObject.getString("usesbioauthentication");
                        if(usesbio=="false"){
                            progressDialog = new ProgressDialog(getContext());
                            progressDialog.setMessage("Submitting customer registration details...");
                            progressDialog.setCancelable(false);
                            progressDialog.show();
                            submitCustomerRegistration();
                        }else {
                            SharedPreferences prefs = getActivity().getSharedPreferences("userFinger", Context.MODE_PRIVATE);
                            String finger1 = prefs.getString("Finger1", null);
                            String finger2 = prefs.getString("Finger2", null);
                            String finger3 = prefs.getString("Finger3", null);
                            String finger4 = prefs.getString("Finger4", null);
                            assert finger1 != null;
                            assert finger2 != null;
                            assert finger3 != null;
                            assert finger4 != null;

                            if (finger1.equals("0") || finger2.equals("0") || finger3.equals("0") || finger4.equals("0")) {
                                Toast.makeText(getContext(), "Please input user fingerprints to proceed!", Toast.LENGTH_SHORT).show();
                            } else {
                                progressDialog = new ProgressDialog(getContext());
                                progressDialog.setMessage("Submitting customer registration details...");
                                progressDialog.setCancelable(false);
                                progressDialog.show();
                                submitCustomerRegistration();

                            }
                        }

                        if (usesbio=="true"){
                            enrollButtons.setVisibility(View.VISIBLE);
                            RelativeLayout02.setVisibility(View.VISIBLE);
                            coplete.setVisibility(View.GONE);
                        }else {
                            enrollButtons.setVisibility(View.GONE);
                            RelativeLayout02.setVisibility(View.GONE);
                            coplete.setVisibility(View.VISIBLE);
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    Utils.showAlertDialog(getActivity(), "Failed", dynamicmenuresponse.getError());
                }

            }

            @Override
            public void onTokenExpired() {

            }

            @Override
            public void onError(String error) {
                Utils.showAlertDialog(getActivity(), "Failed", error);
            }
        });
    }

    void  openDevice(){
        try {
            fingerprintDevice.open(1);
        } catch (DeviceException e) {
            e.printStackTrace();
        }
    }


    @Override
    public void onResume() {
        super.onResume();
        openDevice();
    }
}
