package com.coretec.agencyApplication.fragments.newmodule;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.cloudpos.DeviceException;
import com.cloudpos.OperationResult;
import com.cloudpos.POSTerminal;
import com.cloudpos.TimeConstants;
import com.cloudpos.fingerprint.Fingerprint;
import com.cloudpos.fingerprint.FingerprintDevice;
import com.cloudpos.fingerprint.FingerprintOperationResult;
import com.coretec.agencyApplication.R;
import com.coretec.agencyApplication.activities.newmodules.MainDashboardActivity;
import com.coretec.agencyApplication.api.Api;
import com.coretec.agencyApplication.api.requests.newmodulesrequests.DetailsUpdating;
import com.coretec.agencyApplication.api.requests.newmodulesrequests.GlobalVRequest;
import com.coretec.agencyApplication.api.responses.newmoduleresponses.GlobalVResponse;
import com.coretec.agencyApplication.api.responses.newmoduleresponses.GlobalVUpdateResponse;
import com.coretec.agencyApplication.fingerprint.bfp.demo.HexString;
import com.coretec.agencyApplication.fingerprint.bfp.mgr.FPMgrImpl;
import com.coretec.agencyApplication.fingerprint.bfp.mgr.IFPMgr;
import com.coretec.agencyApplication.fingerprint.util.TextViewUtil;
import com.coretec.agencyApplication.utils.ByteConvertStringUtil;
import com.coretec.agencyApplication.utils.LogHelper;
import com.coretec.agencyApplication.utils.SharedPrefs;
import com.digitalpersona.uareu.Engine;
import com.digitalpersona.uareu.Fmd;
import com.digitalpersona.uareu.Importer;
import com.digitalpersona.uareu.UareUException;
import com.digitalpersona.uareu.UareUGlobal;
import com.stepstone.stepper.BlockingStep;
import com.stepstone.stepper.StepperLayout;
import com.stepstone.stepper.VerificationError;
import com.upek.android.ptapi.PtConstants;
import com.upek.android.ptapi.PtException;
import com.upek.android.ptapi.struct.PtInputBir;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static com.coretec.agencyApplication.utils.Utils.getFormattedDate;
import static com.coretec.agencyApplication.utils.Utils.getLat;
import static com.coretec.agencyApplication.utils.Utils.getLong;

public class UpdateCrossmatchBio extends Fragment implements BlockingStep {

    //Button enroll;
    Button leftRing, leftLittle, rightRing, rightLittle;
    Button delete;
    Button clear;
    Button quit;
    TextView enrollText;

    ProgressDialog progressDialog;

    //crossmatch fingerprint declarations
    protected TextView log_text;
    protected Handler mHandler = null;
    protected Runnable runnable = null;
    public Engine.Candidate[] results;
    Importer im = null;
    Engine m_engine = null;

    //Fingerprint Declarations
    private TextView show;
    //private Context context = getActivity();
    private Handler handler;
    private FingerprintDevice fingerprintDevice;
    private static final int MSGID_SHOW_MESSAGE = 0;
    private SharedPreferences preferences;
    private SharedPreferences.Editor editor;
    private static final String FINGERINDEX1 = "Finger1";
    private static final String FINGERINDEX2 = "Finger2";
    private static final String FINGERINDEX3 = "Finger3";
    private static final String FINGERINDEX4 = "Finger4";

    private String  valdataf1,valdataf2,valdataf3,valdataf4;
    byte[] image1,image2,image3,image4;
    Fmd fm1,fm2;

    @SuppressLint("HandlerLeak")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        SharedPrefs.init(getContext());
        View v = inflater.inflate(R.layout.final_fingerprint_update, container, false);

        //initialize your UI
        leftRing = v.findViewById(R.id.btn_left_ring);
        leftLittle = v.findViewById(R.id.btn_left_little);
        rightRing = v.findViewById(R.id.btn_right_ring);
        rightLittle = v.findViewById(R.id.btn_right_little);
        delete = v.findViewById(R.id.ButtonDelete);
        clear = v.findViewById(R.id.ButtonClear);

        show = v.findViewById(R.id.EnrollmentTextView);

        preferences = getActivity().getSharedPreferences("userFinger", Context.MODE_PRIVATE);
        editor = preferences.edit();
        editor.putString(FINGERINDEX1, "0");
        editor.putString(FINGERINDEX2, "0");
        editor.putString(FINGERINDEX3, "0");
        editor.putString(FINGERINDEX4, "0");
        editor.commit();

        //log_text = this.findViewById(R.id.text_result);
        show.setMovementMethod(ScrollingMovementMethod.getInstance());
        try {
            im = UareUGlobal.GetImporter();
            m_engine = UareUGlobal.GetEngine();
        } catch (Exception e) {
            e.printStackTrace();
        }
        mHandler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                String str = msg.obj + "\n";
                switch (msg.what) {
                    case R.id.log_default:
                        show.setText(str);
                        break;
                    case R.id.log_success:
                        // String str = msg.obj + "\n";
                        TextViewUtil.infoBlueTextView(show, str);
                        break;
                    case R.id.log_failed:
                        // String str = msg.obj + "\n";
                        TextViewUtil.infoRedTextView(show, str);
                        break;
                    case R.id.log_clean:
                        show.setText("");
                        break;
                }
            }
        };

        leftRing.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                enrollLeftRing();
            }
        });

        leftLittle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                enrollLeftLittle();
            }
        });

        rightRing.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                enrollRightRing();
            }
        });

        rightLittle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                enrollRightLittle();
            }
        });

        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                IFPMgr fpMgr = FPMgrImpl.getInstance();
                fpMgr.close();
                preferences = getActivity().getSharedPreferences("userFinger", Context.MODE_PRIVATE);
                editor = preferences.edit();
                editor.putString(FINGERINDEX1, "");
                editor.putString(FINGERINDEX2, "");
                editor.putString(FINGERINDEX3, "");
                editor.putString(FINGERINDEX4, "");
                editor.commit();
                Toast.makeText(getActivity(), getActivity().getText(R.string.Cleared), Toast.LENGTH_SHORT).show();
            }
        });
        clear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                IFPMgr fpMgr = FPMgrImpl.getInstance();
                fpMgr.close();
                show.setText("");
            }
        });

        return v;
    }

    private void writerLogInTextview(String log, int id) {
        Message msg = new Message();
        msg.what = id;
        msg.obj = log;
        mHandler.sendMessage(msg);
    }

    Thread th = null;

    public void enrollLeftRing() {
        if (th == null || th.getState() == Thread.State.TERMINATED) {
            th = new Thread() {
                @SuppressWarnings("unused")
                public void run() {
                    IFPMgr fpMgr = FPMgrImpl.getInstance();
                    try {
                        fpMgr.open(getContext());

                        fpMgr.deleteAll(getContext());

                        List<PtInputBir> birList = new ArrayList<PtInputBir>();
                        writerLogInTextview("Please put Left Ring finger on the scanner", R.id.log_success);
                        try
                        {
                            image1 = fpMgr
                                    .GrabImage(PtConstants.PT_GRAB_TYPE_508_508_8_SCAN508_508_8);
                            int iWidth = fpMgr.getImagewidth();
                            fm1 = ConvertImgToIsoTemplate(image1, iWidth);



                            if (fm1 != null) {
                                valdataf1 = HexString.bufferToHex(fm1.getData());
                                preferences = getActivity().getSharedPreferences("userFinger", Context.MODE_PRIVATE);
                                editor = preferences.edit();
                                Log.d("kidole", "hiikidole1"+valdataf1);

                                editor.putString(FINGERINDEX1, valdataf1);

                                editor.commit();
                                writerLogInTextview("Left Ring Enrolled successfully", R.id.log_success);
                            } else {
                                Log.e("fm", "fm is null");
                                writerLogInTextview("Please retry again!", R.id.log_failed);
                            }

                        }
                        catch (PtException e)
                        {
                            writerLogInTextview("exception " + e.getMessage(), R.id.log_success);
                        }

                    } finally {
                        fpMgr.close();
                    }
                }
            };
            th.start();
        }
    }

    public void enrollLeftLittle() {
        if (th == null || th.getState() == Thread.State.TERMINATED) {
            th = new Thread() {
                @SuppressWarnings("unused")
                public void run() {
                    IFPMgr fpMgr = FPMgrImpl.getInstance();
                    try {

                        fpMgr.open(getContext());

                        fpMgr.deleteAll(getContext());
                        List<PtInputBir> birList = new ArrayList<PtInputBir>();
                        writerLogInTextview("Please put Left Little finger on the scanner", R.id.log_success);

                        try
                        {
                            image2 = fpMgr
                                    .GrabImage(PtConstants.PT_GRAB_TYPE_508_508_8_SCAN508_508_8);
                            int iWidth = fpMgr.getImagewidth();
                            fm2 = ConvertImgToIsoTemplate(image2, iWidth);

                            if (fm2 != null) {
                                if (fm1==fm2){
                                    Toast.makeText(getActivity(), "please use a different finger", Toast.LENGTH_SHORT).show();
                                }else {
                                    valdataf2 = HexString.bufferToHex(fm2.getData());
                                    preferences = getActivity().getSharedPreferences("userFinger", Context.MODE_PRIVATE);
                                    editor = preferences.edit();
                                    Log.d("kidole", "hiikidole2"+valdataf2);
                                    editor.putString(FINGERINDEX2, valdataf2);
                                    editor.commit();
                                    writerLogInTextview("Left Little Enrolled successfully", R.id.log_success);
                                }

                            } else {
                                Log.e("fm", "fm is null");
                                writerLogInTextview("Please retry again!", R.id.log_failed);
                            }
                        }
                        catch (PtException e)
                        {
                            writerLogInTextview("exception " + e.getMessage(), R.id.log_success);
                        }

                    } finally {
                        fpMgr.close();
                    }
                }
            };
            th.start();
        }
    }

    public void enrollRightRing() {
        if (th == null || th.getState() == Thread.State.TERMINATED) {
            th = new Thread() {
                @SuppressWarnings("unused")
                public void run() {
                    IFPMgr fpMgr = FPMgrImpl.getInstance();
                    try {

                        fpMgr.open(getContext());

                        fpMgr.deleteAll(getContext());

                        List<PtInputBir> birList = new ArrayList<PtInputBir>();
                        // fpMgr.test(FpActivity.this, FingerId.NONE);
                        writerLogInTextview("Please put Right Ring finger on the scanner", R.id.log_success);
                        try
                        {
                            byte[] image = fpMgr
                                    .GrabImage(PtConstants.PT_GRAB_TYPE_508_508_8_SCAN508_508_8);
                            int iWidth = fpMgr.getImagewidth();
                            Fmd fm = ConvertImgToIsoTemplate(image, iWidth);

                            if (fm != null) {
                                valdataf3 = HexString.bufferToHex(fm.getData());

                                preferences = getActivity().getSharedPreferences("userFinger", Context.MODE_PRIVATE);
                                editor = preferences.edit();

                                editor.putString(FINGERINDEX3, valdataf3);
                                editor.commit();
                                writerLogInTextview("Right Ring Enrolled successfully", R.id.log_success);

                            } else {
                                Log.e("fm", "fm is null");
                                writerLogInTextview("Please retry again!", R.id.log_failed);
                            }

                        }
                        catch (PtException e)
                        {
                            writerLogInTextview("exception " + e.getMessage(), R.id.log_success);
                        }

                    } finally {
                        fpMgr.close();
                    }
                }
            };
            th.start();
        }
    }

    public void enrollRightLittle() {
        if (th == null || th.getState() == Thread.State.TERMINATED) {
            th = new Thread() {
                @SuppressWarnings("unused")
                public void run() {
                    IFPMgr fpMgr = FPMgrImpl.getInstance();
                    try {

                        fpMgr.open(getContext());

                        fpMgr.deleteAll(getContext());

                        List<PtInputBir> birList = new ArrayList<PtInputBir>();
                        // fpMgr.test(FpActivity.this, FingerId.NONE);
                        writerLogInTextview("Please put Right Little finger on the scanner", R.id.log_success);
                        // PtInputBir template = null;

                        // template = fpMgr.enrollFp(FpActivity.this,
                        // FingerId.NONE);
                        try
                        {
                            byte[] image = fpMgr
                                    .GrabImage(PtConstants.PT_GRAB_TYPE_508_508_8_SCAN508_508_8);
                            int iWidth = fpMgr.getImagewidth();
                            Fmd fm = ConvertImgToIsoTemplate(image, iWidth);

                            if (fm != null) {
                                valdataf4 = HexString.bufferToHex(fm.getData());

                                preferences = getActivity().getSharedPreferences("userFinger", Context.MODE_PRIVATE);
                                editor = preferences.edit();

                                editor.putString(FINGERINDEX4, valdataf4);
                                editor.commit();
                                writerLogInTextview("Right Little Enrolled successfully", R.id.log_success);
                            } else {
                                Log.e("fm", "fm is null");
                                writerLogInTextview("Please retry again!", R.id.log_failed);
                            }

                        }
                        catch (PtException e)
                        {
                            writerLogInTextview("exception " + e.getMessage(), R.id.log_success);
                        }

                    } finally {
                        fpMgr.close();
                    }
                }
            };
            th.start();
        }
    }

    private Fmd ConvertImgToIsoTemplate(byte[] aImage, int iWidth)
    {

        if (aImage == null) {
            return null;
        }

        int iLength = aImage.length;
        int iHeight = aImage.length / iWidth;

        Importer importer = UareUGlobal.GetImporter();
        try {
            // Fid fid = importer.ImportRaw(aImage, iWidth, iHeight, 500, 0, 0,
            // Fid.Format.ISO_19794_4_2005, 500, false);
            Engine engine = UareUGlobal.GetEngine();
            Fmd fmd = engine.CreateFmd(aImage, iWidth, iHeight, 500, 0, 0, Fmd.Format.ISO_19794_2_2005);
            // int score = engine.Compare(fmd,0,fmd,0);

            Log.i("BasicSample", "Import a Fmd from a raw image OK");
            // Log.i("BasicSample", String.format("score %d" , score ));
            writerLogInTextview("Scan Complete", R.id.log_success);
            return fmd;
        } catch (UareUException e)
        {
            Log.d("BasicSample", "Import Raw Image Fail", e);
            writerLogInTextview("Scan Failed", R.id.log_success);
            return null;
        }

    }

    @Override
    public void onNextClicked(final StepperLayout.OnNextClickedCallback callback) {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                //you can do anythings you want
                callback.goToNextStep();
            }
        }, 0L);// delay open another fragment,
    }

    @Override
    public void onCompleteClicked(StepperLayout.OnCompleteClickedCallback callback) {
        /*Toast.makeText(getActivity(), "Registration Successful", Toast.LENGTH_SHORT).show();
        startActivity(new Intent(getActivity(), Dashboard.class));
        getActivity().finish();*/

        String msg = "Are you sure you want to submit customer biometric details?";
        //final String qualKgs = SharedPrefs.read(SharedPrefs.LOAN_QUALIFIED_KGS, null);
        AlertDialog.Builder builder1 = new AlertDialog.Builder(getContext());
        builder1.setMessage(msg);
        builder1.setTitle("Confirm!");
        builder1.setCancelable(false);

        builder1.setPositiveButton(
                "YES",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();

                        SharedPreferences prefs = getActivity().getSharedPreferences("userFinger", Context.MODE_PRIVATE);
                        String finger1 = prefs.getString("Finger1", null);
                        String finger2 = prefs.getString("Finger2", null);
                        String finger3 = prefs.getString("Finger3", null);
                        String finger4 = prefs.getString("Finger4", null);

                        assert finger1 != null;
                        assert finger2 != null;
                        assert finger3 != null;
                        assert finger4 != null;
                        if (finger1.equals("0") || finger2.equals("0") || finger3.equals("0") || finger4.equals("0")) {
                            Toast.makeText(getContext(), "Please input user fingerprints to proceed!", Toast.LENGTH_SHORT).show();
                        } else {

                            progressDialog = new ProgressDialog(getContext());
                            progressDialog.setMessage("Submitting customer registration details...");
                            progressDialog.setCancelable(false);
                            progressDialog.show();
                            submitCustomerBiometricDetails();

                        }
                    }
                });

        builder1.setNegativeButton(
                "Cancel",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();
                    }
                });

        AlertDialog alert11 = builder1.create();
        alert11.show();

    }

    @Override
    public void onBackClicked(final StepperLayout.OnBackClickedCallback callback) {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                //you can do anythings you want
                /*callback.goToNextStep();*/
                callback.goToPrevStep();
            }
        }, 0L);// delay open another fragment,
    }

    @Override
    public VerificationError verifyStep() {
        return null;
    }

    @Override
    public void onSelected() {

    }

    @Override
    public void onError(@NonNull VerificationError error) {

    }

    void submitCustomerBiometricDetails() {
        SharedPreferences prefs = getActivity().getSharedPreferences("userFinger", Context.MODE_PRIVATE);
        String finger1 = prefs.getString("Finger1", null);
        String finger2 = prefs.getString("Finger2", null);
        String finger3 = prefs.getString("Finger3", null);
        String finger4 = prefs.getString("Finger4", null);

        final String TAG = "customerupdate";

        String URL = Api.MSACCO_AGENT + Api.BioRegistration;
        SimpleDateFormat timeStampFormat = new SimpleDateFormat("yyyy/MM/dd - HH:mm");
        Date myDate = new Date();

        final DetailsUpdating globalVRequest = new DetailsUpdating();
        globalVRequest.idno = SharedPrefs.read(SharedPrefs.VR_BIO_ID, null);
        globalVRequest.leftring = finger1;
        globalVRequest.leftlittle = finger2;
        globalVRequest.rightring = finger3;
        globalVRequest.rightlittle = finger4;
        globalVRequest.transactiontype = "1";
        globalVRequest.agentid = SharedPrefs.read(SharedPrefs.AGENT_ID_1, null);////
        globalVRequest.terminalid = MainDashboardActivity.imei;
        globalVRequest.longitude = getLong(getContext());
        globalVRequest.latitude = getLat(getContext());
        globalVRequest.date = getFormattedDate();
        globalVRequest.corporate_no = MainDashboardActivity.corporateno;

        Api.instance(getContext()).request(URL, globalVRequest, new Api.RequestListener() {
            @Override
            public void onSuccess(String response) {
                GlobalVUpdateResponse globalVResponse = Api.instance(getContext())
                        .mGson.fromJson(response, GlobalVUpdateResponse.class);

                if (globalVResponse.operationsuccess) {
                    progressDialog.dismiss();
                    String msg = "Biometric details updated successfully!";
                    AlertDialog.Builder builder1 = new AlertDialog.Builder(getContext());
                    builder1.setMessage(msg);
                    builder1.setTitle("Success!");
                    builder1.setCancelable(false);

                    builder1.setPositiveButton(
                            "DONE",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                    getActivity().finish();
                                    Intent i = new Intent(getActivity(), MainDashboardActivity.class);
                                    i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                                    //i.addFlags(IntentCompat.FLAG_ACTIVITY_CLEAR_TASK);
                                    i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                    startActivity(i);
                                }
                            });

                    AlertDialog alert11 = builder1.create();
                    alert11.show();
                } else {
                    progressDialog.dismiss();
                    Toast.makeText(getContext(), "failed. Try again later!", Toast.LENGTH_LONG).show();
                }

            }

            @Override
            public void onTokenExpired() {

            }

            @Override
            public void onError(String error) {
                progressDialog.dismiss();
            }
        });
    }

}
