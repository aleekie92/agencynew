package com.coretec.agencyApplication.fragments.newmodule;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.coretec.agencyApplication.R;
import com.coretec.agencyApplication.activities.newmodules.MainDashboardActivity;
import com.coretec.agencyApplication.api.Api;
import com.coretec.agencyApplication.api.requests.FingerPrintRequest;
import com.coretec.agencyApplication.api.requests.RequestMemberName;
import com.coretec.agencyApplication.api.responses.FingerPrintResponse;
import com.coretec.agencyApplication.api.responses.MemberNameResponse;
import com.coretec.agencyApplication.utils.SharedPrefs;
import com.stepstone.stepper.BlockingStep;
import com.stepstone.stepper.StepperLayout;
import com.stepstone.stepper.VerificationError;

import org.json.JSONException;
import org.json.JSONObject;

import static com.coretec.agencyApplication.utils.Utils.getFormattedDate;
import static com.coretec.agencyApplication.utils.Utils.getLat;
import static com.coretec.agencyApplication.utils.Utils.getLong;

public class UpdateBiometrics extends Fragment implements BlockingStep {

    private EditText registration_id, registration_name;
    private Button btn_account_search;
    private TextView status;

    private ProgressBar bioregistration_progressBar;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.updatebio, container, false);
        SharedPrefs.init(getContext());

        registration_id = v.findViewById(R.id.registration_id);
        registration_name = v.findViewById(R.id.registration_name);
        bioregistration_progressBar = v.findViewById(R.id.bioregistration_progressBar);
        btn_account_search = v.findViewById(R.id.btn_account_search);
        status = v.findViewById(R.id.status);
        registration_name.setFocusable(false);

        btn_account_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String identity = registration_id.getText().toString();

                if (identity.matches("")) {
                    Toast.makeText(getActivity(), "Please Enter ID number to proceed!", Toast.LENGTH_SHORT).show();
                } else {
                    bioregistration_progressBar.setVisibility(View.VISIBLE);
                    requestMemberDetails(identity);
                }
            }
        });
        return v;
    }

    @Override
    public void onNextClicked(final StepperLayout.OnNextClickedCallback callback) {
        if (registration_id.getText().toString().matches("")) {
            Toast.makeText(getActivity(), "Please Enter ID number to proceed!", Toast.LENGTH_SHORT).show();
        } else if (registration_name.getText().toString().matches("")) {
            Toast.makeText(getActivity(), "No name!! Input the correct ID and search again", Toast.LENGTH_LONG).show();
        } else {
            status.setVisibility(View.VISIBLE);
            requestFingerPrints(callback);
        }
    }

    @Override
    public void onCompleteClicked(StepperLayout.OnCompleteClickedCallback callback) {

    }

    @Override
    public void onBackClicked(final StepperLayout.OnBackClickedCallback callback) {

        callback.goToPrevStep();
    }

    @Override
    public VerificationError verifyStep() {
        return null;
    }

    @Override
    public void onSelected() {

    }

    @Override
    public void onError(@NonNull VerificationError error) {

    }

    void requestMemberDetails(String id) {
        RequestMemberName requestMemberName = new RequestMemberName();
        requestMemberName.corporateno = MainDashboardActivity.corporateno;
        requestMemberName.accountidentifier = id;
        requestMemberName.accountcode = "0";
        requestMemberName.agentid = SharedPrefs.read(SharedPrefs.AGENT_ID_1, null);////
        requestMemberName.terminalid = MainDashboardActivity.imei;
        Api.instance(getActivity()).request(Api.MSACCO_AGENT + Api.GetMembername, requestMemberName, new Api.RequestListener() {
            @Override
            public void onSuccess(String response) {
                bioregistration_progressBar.setVisibility(View.GONE);
                MemberNameResponse memberNameResponse = Api.instance(getContext()).mGson.fromJson(response, MemberNameResponse.class);
                if (memberNameResponse.is_successful) {
                    registration_name.setText(memberNameResponse.getCustomer_name());
                } else {
                    Toast.makeText(getActivity(), "No account details found, input the correct ID number and try again", Toast.LENGTH_LONG).show();
                }

            }

            @Override
            public void onTokenExpired() {
                bioregistration_progressBar.setVisibility(View.GONE);

            }

            @Override
            public void onError(String error) {
                Toast.makeText(getActivity(), "failed to load, try again later", Toast.LENGTH_SHORT).show();

            }
        });
    }

    void requestFingerPrints(final StepperLayout.OnNextClickedCallback callback) {
        final String TAG = "GET FINGERPRINTS";

        String URL = Api.MSACCO_AGENT + Api.GetFingerPrint;

        final FingerPrintRequest fingerPrintRequest = new FingerPrintRequest();

        fingerPrintRequest.accountidentifier = registration_id.getText().toString();
        fingerPrintRequest.accountidentifiercode = "1";
        fingerPrintRequest.agentid = SharedPrefs.read(SharedPrefs.AGENT_ID_1, null);
        fingerPrintRequest.terminalid = MainDashboardActivity.imei;
        fingerPrintRequest.longitude = getLong(getActivity());
        fingerPrintRequest.latitude = getLat(getActivity());
        fingerPrintRequest.date = getFormattedDate();

        Log.e(TAG, fingerPrintRequest.getBody().toString());
        Api.instance(getActivity()).request(URL, fingerPrintRequest, new Api.RequestListener() {
            @Override
            public void onSuccess(String response) {

                FingerPrintResponse fingerPrintResponse = Api.instance(getContext())
                        .mGson.fromJson(response, FingerPrintResponse.class);
                if (fingerPrintResponse.is_successful) {
                    status.setVisibility(View.GONE);
                    try {
                        JSONObject jsonObject = new JSONObject(response);

                            String msg = "Biometric details Already exists!";
                            AlertDialog.Builder builder1 = new AlertDialog.Builder(getContext());
                            builder1.setMessage(msg);
                            builder1.setTitle("Alert!");
                            builder1.setCancelable(false);

                            builder1.setPositiveButton(
                                    "DONE",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            dialog.cancel();
                                            getActivity().finish();
                                            Intent i = new Intent(getActivity(), MainDashboardActivity.class);
                                            i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                                            i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                            startActivity(i);
                                        }
                                    });

                            AlertDialog alert11 = builder1.create();
                            alert11.show();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }else {
                    SharedPrefs.bioupdateid(registration_id.getText().toString(), getActivity());
                    callback.goToNextStep();
                }

            }

            @Override
            public void onTokenExpired() {

            }

            @Override
            public void onError(String error) {

            }
        });

    }
}
