package com.coretec.agencyApplication.fragments;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.os.Handler;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.coretec.agencyApplication.R;
import com.coretec.agencyApplication.activities.LoginActivity;
import com.coretec.agencyApplication.activities.gfl.MainGFLDashboard;
import com.coretec.agencyApplication.activities.newmodules.ValidateTunzhengbigBio;
import com.coretec.agencyApplication.api.GFL;
import com.coretec.agencyApplication.api.requests.FingerPrintRequest;
import com.coretec.agencyApplication.api.requests.GetGrowersQualifiedLoansRequest;
import com.coretec.agencyApplication.api.requests.GetLoanProductsRequest;
import com.coretec.agencyApplication.api.requests.GetLoansInfoRequest;
import com.coretec.agencyApplication.api.requests.GrowersDetailsRequest;
import com.coretec.agencyApplication.api.requests.LoanExistRequest;
import com.coretec.agencyApplication.api.responses.FingerPrintResponse;
import com.coretec.agencyApplication.api.responses.GetGrowersQualifiedLoansResponse;
import com.coretec.agencyApplication.api.responses.GetLoanProductsResponse;
import com.coretec.agencyApplication.api.responses.GetLoansInfoResponse;
import com.coretec.agencyApplication.api.responses.GrowersDetailsResponse;
import com.coretec.agencyApplication.api.responses.LoanExistResponse;
import com.coretec.agencyApplication.model.GrowerDetails;
import com.coretec.agencyApplication.model.LoanDetails;
import com.coretec.agencyApplication.utils.LogOutTimerUtil;
import com.coretec.agencyApplication.utils.SharedPrefs;
import com.stepstone.stepper.BlockingStep;
import com.stepstone.stepper.StepperLayout;
import com.stepstone.stepper.VerificationError;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.TimeZone;

import static com.coretec.agencyApplication.utils.Utils.getFormattedDate;
import static com.coretec.agencyApplication.utils.Utils.getLat;
import static com.coretec.agencyApplication.utils.Utils.getLong;
public class LoanFragment extends Fragment implements BlockingStep {

    private TextInputLayout input_layout_id_number;
    private EditText identifier_deposit;
    private Spinner spinner;
    private TextView minimum, maximum;

    private ArrayList<String> arrayList = new ArrayList<>();
    private ArrayAdapter<GrowerDetails> adapter;
    private String item, itemLoan;

    private Spinner spinnerLoan;
    private ArrayList<String> arrayListLoan = new ArrayList<>();
    private ArrayAdapter<LoanDetails> adapterLoan;

    ProgressBar progressBar, loanBar;
    ProgressBar miniPB, maxiPB;
    LinearLayout loan_exists;
    //ProgressDialog progressDialog;
    Button submit;
    String identifier_number;
    String text;
    int agee;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.gfl_fragment_loan, container, false);
        SharedPrefs.init(getContext());

        input_layout_id_number = v.findViewById(R.id.input_layout_id_number);
        identifier_deposit = v.findViewById(R.id.id_number);
        minimum = v.findViewById(R.id.tvMin);
        maximum = v.findViewById(R.id.tvMax);
        loan_exists = v.findViewById(R.id.layout_loan_exists);

        identifier_deposit.setInputType(InputType.TYPE_NULL);
        identifier_deposit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                identifier_deposit.post(new Runnable() {
                    @Override
                    public void run() {
                        identifier_deposit.setSelection(identifier_deposit.getText().toString().length());
                    }
                });
            }
        });

        identifier_deposit.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    identifier_deposit.post(new Runnable() {
                        @Override
                        public void run() {
                            identifier_deposit.setSelection(identifier_deposit.getText().toString().length());
                        }
                    });
                }
            }
        });

        spinner = v.findViewById(R.id.growerSpinner);
        progressBar = v.findViewById(R.id.growersPb);

        loanBar = v.findViewById(R.id.loanPb);
        miniPB = v.findViewById(R.id.minPb);
        maxiPB = v.findViewById(R.id.maxPb);
        //growers_number = findViewById(R.id.growers_number);
        spinnerLoan = v.findViewById(R.id.productType);

      /*  progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage("Please wait...");
        progressDialog.setCancelable(false);
*/
        TextWatcher textWatcherId = new TextWatcher() {

            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                identifier_number = identifier_deposit.getText().toString();
                SharedPrefs.identifiernumber(identifier_number,getActivity());
                if (identifier_deposit.length() >= 7) {
                    requestGetAccountDetailsForSpinerLoad(identifier_number);
                    requestGetAccountDetails();
                }
                if (identifier_deposit.length() <= 7) {
                    minimum.setText("xxxxx");
                    maximum.setText("xxxxx");
                    arrayListLoan.clear();
                    ArrayAdapter<String> adapter1 = new ArrayAdapter<String>(getContext(), android.R.layout.simple_spinner_item, arrayListLoan);
                    adapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    spinnerLoan.setAdapter(null);

                    progressBar.setVisibility(View.GONE);
                    arrayList.clear();
                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_spinner_item, arrayList);
                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    spinner.setAdapter(null);
                }
                if (identifier_deposit.length() >= 5 && identifier_deposit.length() <= 8) {

                    minimum.setText("xxxxx");
                    maximum.setText("xxxxx");
                    arrayList.clear();
                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_spinner_item, arrayList);
                    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    spinner.setAdapter(null);

                    arrayListLoan.clear();
                    ArrayAdapter<String> adapter1 = new ArrayAdapter<String>(getContext(), android.R.layout.simple_spinner_item, arrayListLoan);
                    adapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                    spinnerLoan.setAdapter(null);

                    progressBar.setVisibility(View.VISIBLE);
                    progressBar.getIndeterminateDrawable().setColorFilter(ContextCompat.getColor(getContext(), R.color.btn_green), PorterDuff.Mode.SRC_IN );
                }

            }
        };

        adapter = new ArrayAdapter<GrowerDetails>(getContext(),
                R.layout.simple_spinner_dropdown_item, new ArrayList<GrowerDetails>());
        adapter.setDropDownViewResource(R.layout.simple_spinner_dropdown_item);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                item = parent.getItemAtPosition(position).toString();
                 text = spinner.getSelectedItem().toString();
                if (!text.isEmpty()) {
                    SharedPrefs.loanappgrower(text,getActivity());
                    if (text.length() >= 7) {
                        arrayListLoan.clear();
                        checkDefaulter(text);
                        requestLoanProducts(text);
                        requestFingerPrints();
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        adapterLoan = new ArrayAdapter<LoanDetails>(getContext(),
                R.layout.simple_spinner_dropdown_item, new ArrayList<LoanDetails>());
        spinnerLoan.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                itemLoan = parent.getItemAtPosition(position).toString();
                String text1 = spinnerLoan.getSelectedItem().toString();
                String code1 = text1.substring(0,text1.indexOf("-"));
                if (!text1.equals("product-type")) {
                    miniPB.setVisibility(View.VISIBLE);
                    maxiPB.setVisibility(View.VISIBLE);
                    SharedPrefs.write(SharedPrefs.LOAN_PT_CODE, code1);
                    if (text1.length()>=1){
                        requestQualifiedAmount(text1);
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        identifier_deposit.addTextChangedListener(textWatcherId);

        return v;
    }

    @Override
    public void onNextClicked(final StepperLayout.OnNextClickedCallback callback) {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (identifier_deposit.getText().toString().length() <= 6){
                    Toast.makeText(getContext(), "Fill in all details to continue!", Toast.LENGTH_SHORT).show();
                } else {
                    loan_exists.setVisibility(View.VISIBLE);
                     loanExists(callback);
                }
            }
        }, 0L);// delay open another fragment,
    }
    @Override
    public void onCompleteClicked(StepperLayout.OnCompleteClickedCallback callback) {
    }
    @Override
    public void onBackClicked(final StepperLayout.OnBackClickedCallback callback) {
        callback.goToPrevStep();
    }
    @Override
    public VerificationError verifyStep() {
        return null;
    }
    @Override
    public void onSelected() {

        minimum.setVisibility(View.VISIBLE);
        maximum.setVisibility(View.VISIBLE);
        miniPB.setVisibility(View.GONE);
        maxiPB.setVisibility(View.GONE);

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getContext());
        if (!prefs.getBoolean("first_time_loan", false)) {
            SharedPreferences.Editor editor = prefs.edit();
            editor.putBoolean("first_time_loan", true);
            editor.commit();
            if (identifier_deposit.getText().toString().isEmpty()) {
                SharedPrefs.write(SharedPrefs.LOAN_IDENTITY_NO, "null");
            }
        } else {
            String id_number = SharedPrefs.read(SharedPrefs.LOAN_IDENTITY_NO, null);

            if (!id_number.equals("null")) {
                //identifier_deposit.setText(id_number);
            }
        }

    }
    @Override
    public void onError(@NonNull VerificationError error) {
    }

    void loanExists(final StepperLayout.OnNextClickedCallback callback){
        final String TAG = "LoanExists";
        String URL = GFL.MSACCO_AGENT + GFL.LoanExist;
        final LoanExistRequest loanExistRequest = new LoanExistRequest();
        loanExistRequest.AgentCode = SharedPrefs.read(SharedPrefs.AGENT_ID_1, null);
        loanExistRequest.GrowerNo = SharedPrefs.read(SharedPrefs.LOAN_APP_GROWER, null);
        loanExistRequest.LoanProduct = SharedPrefs.read(SharedPrefs.LOAN_PT_CODE, null);

        Log.e(TAG, loanExistRequest.getBody().toString());

        GFL.instance(getContext()).request(URL, loanExistRequest, new GFL.RequestListener() {
            @Override
            public void onSuccess(String response) {
                LoanExistResponse loanExistResponse = GFL.instance(getContext())
                        .mGson.fromJson(response, LoanExistResponse.class);
                if (loanExistResponse.is_successful) {
                    loan_exists.setVisibility(View.GONE);
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        boolean loanExists = jsonObject.getBoolean("Exist");
                        if (loanExists) {
                            String msg = "Loan application cannot continue. This client has an active loan application for the selected product type";
                            AlertDialog.Builder builder1 = new AlertDialog.Builder(getContext());
                            builder1.setMessage(msg);
                            builder1.setTitle("Alert!");
                            builder1.setCancelable(false);

                            builder1.setPositiveButton(
                                    "OK",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            dialog.cancel();
                                            getActivity().finish();
                                        }
                                    });

                            builder1.setNegativeButton(
                                    "Cancel",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            dialog.cancel();
                                        }
                                    });

                            AlertDialog alert11 = builder1.create();
                            alert11.show();
                        } else {

                            String min = SharedPrefs.read(SharedPrefs.LOAN_MINIMUM, null);
                            String max = SharedPrefs.read(SharedPrefs.LOAN_MAXIMUM, null);

                            if(min==null && max==null){
                                Toast.makeText(getActivity(), "Select A loan product that you qualify for", Toast.LENGTH_SHORT).show();
                            }else {
                                String fpBoolean = SharedPrefs.read(SharedPrefs.FP_BOOLEAN, null);
                               /* progressDialog = new ProgressDialog(getActivity());
                                progressDialog.setMessage("Please wait...");
                                progressDialog.setCancelable(false);*/
                                if (fpBoolean.equals("true")) {
                                    //progressDialog.dismiss();
                                    callback.goToNextStep();
                                    SharedPrefs.write(SharedPrefs.FP_BOOLEAN, "false");
                                    SharedPrefs.write(SharedPrefs.SELECTEDGROWER, spinner.getSelectedItem().toString());
                                } else if (fpBoolean.equals("false")){
                                    //progressDialog.dismiss();
                                    Toast.makeText(getActivity(), "Please authenticate first!", Toast.LENGTH_SHORT).show();
                                }
                            }
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                } else {
                    loan_exists.setVisibility(View.GONE);
                    Toast.makeText(getContext(), "Could not retrieve information. Please try again!", Toast.LENGTH_LONG).show();
                }

            }

            @Override
            public void onTokenExpired() {

            }

            @Override
            public void onError(String error) {

            }
        });
    }

    void requestQualifiedAmount(final String productLoan){
        final String TAG = "QualifiedAmount";
        String URL = GFL.MSACCO_AGENT + GFL.GetGrowersQualifiedLoans;
        String growerNo = SharedPrefs.read(SharedPrefs.LOAN_APP_GROWER, null);
        String productCode = SharedPrefs.read(SharedPrefs.LOAN_PT_CODE, null);

        final GetGrowersQualifiedLoansRequest qualifiedLoansRequest = new GetGrowersQualifiedLoansRequest();
        qualifiedLoansRequest.corporateno = "CAP016";
        qualifiedLoansRequest.growersno = growerNo;
        qualifiedLoansRequest.loanproductcode = productCode;
        qualifiedLoansRequest.transactiontype = "";
        qualifiedLoansRequest.agentid = SharedPrefs.read(SharedPrefs.AGENT_ID_1, null);
        qualifiedLoansRequest.terminalid = SharedPrefs.read(SharedPrefs.DEVICE_ID, null);
        qualifiedLoansRequest.longitude = getLong(getContext());
        qualifiedLoansRequest.latitude = getLat(getContext());
        qualifiedLoansRequest.date = getFormattedDate();

        Log.e(TAG, qualifiedLoansRequest.getBody().toString());

        GFL.instance(getContext()).request(URL, qualifiedLoansRequest, new GFL.RequestListener() {
            @Override
            public void onSuccess(String response) {
                GetGrowersQualifiedLoansResponse qualifiedLoansResponse = GFL.instance(getContext())
                        .mGson.fromJson(response, GetGrowersQualifiedLoansResponse.class);
                if (qualifiedLoansResponse.is_successful) {
                    miniPB.setVisibility(View.GONE);
                    maxiPB.setVisibility(View.GONE);
                    minimum.setVisibility(View.VISIBLE);
                    maximum.setVisibility(View.VISIBLE);
                    try {
                        JSONObject jsonObject = new JSONObject(response);

                        JSONArray jsonArray = jsonObject.getJSONArray("GetGrowersQualifiedLoans");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                            JSONArray jsonArray1 = jsonObject1.getJSONArray("QualifiedLoans");
                            for (int l = 0; l < jsonArray1.length(); l++) {
                                JSONObject jsonObject2 = jsonArray1.getJSONObject(l);
                                String min = jsonObject2.getString("MinimumBalance");
                                String max = jsonObject2.getString("Amount");
                                String loanValidation = jsonObject2.getString("LoanValidation");
                                String topUpValidation = jsonObject2.getString("LoanTopUpValidation");

                                if (loanValidation.equals("Yes") && topUpValidation.equals("Yes")) {
                                    minimum.setText(min);
                                    maximum.setText(max);
                                    SharedPrefs.write(SharedPrefs.LOAN_MINIMUM, min);
                                    SharedPrefs.write(SharedPrefs.LOAN_MAXIMUM, max);
                                } else if (loanValidation.equals("No")) {
                                    String msg = "You do not qualify for " +productLoan+" at the moment!";
                                    AlertDialog.Builder builder1 = new AlertDialog.Builder(getContext());
                                    builder1.setMessage(msg);
                                    builder1.setTitle("Alert!");
                                    builder1.setCancelable(false);

                                    builder1.setPositiveButton(
                                            "Cancel Application",
                                            new DialogInterface.OnClickListener() {
                                                public void onClick(DialogInterface dialog, int id) {
                                                    getActivity().finish();
                                                }
                                            });
                                    builder1.setNegativeButton(
                                            "Select Another Loan",
                                            new DialogInterface.OnClickListener() {
                                                public void onClick(DialogInterface dialog, int id) {
                                                    dialog.cancel();
                                                }
                                            });

                                    AlertDialog alert11 = builder1.create();
                                    alert11.show();
                                } else if (topUpValidation.equals("No")){
                                    String msg = productLoan+" does not allow for top-ups!";
                                    AlertDialog.Builder builder1 = new AlertDialog.Builder(getContext());
                                    builder1.setMessage(msg);
                                    builder1.setTitle("Alert!");
                                    builder1.setCancelable(false);
                                    builder1.setPositiveButton(
                                            "Cancel Application",
                                            new DialogInterface.OnClickListener() {
                                                public void onClick(DialogInterface dialog, int id) {
                                                    getActivity().finish();
                                                }
                                            });
                                    builder1.setNegativeButton(
                                            "Select Another Loan",
                                            new DialogInterface.OnClickListener() {
                                                public void onClick(DialogInterface dialog, int id) {
                                                    dialog.cancel();
                                                }
                                            });

                                    AlertDialog alert11 = builder1.create();
                                    alert11.show();
                                } else {
                                    String msg = "You do not qualify for "+productLoan;
                                    AlertDialog.Builder builder1 = new AlertDialog.Builder(getContext());
                                    builder1.setMessage(msg);
                                    builder1.setTitle("Alert!");
                                    builder1.setCancelable(false);

                                    builder1.setPositiveButton(
                                            "Cancel Application",
                                            new DialogInterface.OnClickListener() {
                                                public void onClick(DialogInterface dialog, int id) {
                                                    getActivity().finish();
                                                }
                                            });
                                    builder1.setNegativeButton(
                                            "Select Another Loan",
                                            new DialogInterface.OnClickListener() {
                                                public void onClick(DialogInterface dialog, int id) {
                                                    dialog.cancel();
                                                }
                                            });

                                    AlertDialog alert11 = builder1.create();
                                    alert11.show();
                                }
                            }

                        }


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                } else {
//                    Toast.makeText(ShareDeposit.this, "Accounts Fetching Failed", Toast.LENGTH_LONG).show();
                }

            }

            @Override
            public void onTokenExpired() {

            }

            @Override
            public void onError(String error) {

            }
        });
    }

    void checkDefaulter(final String identifier){
        final String TAG = "LoanProducts";
        String URL = GFL.MSACCO_AGENT + GFL.GetLoansInfo;
        final GetLoansInfoRequest loansInfoRequest = new GetLoansInfoRequest();
        loansInfoRequest.corporateno = "CAP016";
        loansInfoRequest.growersno = identifier;
        loansInfoRequest.transactiontype = "1";
        loansInfoRequest.agentid = SharedPrefs.read(SharedPrefs.AGENT_ID_1, null);
        loansInfoRequest.terminalid = SharedPrefs.read(SharedPrefs.DEVICE_ID, null);
        loansInfoRequest.longitude = getLong(getContext());
        loansInfoRequest.latitude = getLat(getContext());
        loansInfoRequest.date = getFormattedDate();

        Log.e(TAG, loansInfoRequest.getBody().toString());

        GFL.instance(getContext()).request(URL, loansInfoRequest, new GFL.RequestListener() {
            @Override
            public void onSuccess(String response) {
                GetLoansInfoResponse loansInfoResponse = GFL.instance(getContext())
                        .mGson.fromJson(response, GetLoansInfoResponse.class);
                if (loansInfoResponse.is_successful) {
                    List<LoanDetails> loanDetailsList = new ArrayList<>();

                    try {
                        JSONObject jsonObject = new JSONObject(response);

                        JSONArray jsonArray = jsonObject.getJSONArray("GetLoansInfo");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                            JSONArray jsonArray1 = jsonObject1.getJSONArray("LoanDetails");
                            for (int l = 0; l < jsonArray1.length(); l++) {
                                JSONObject jsonObject2 = jsonArray1.getJSONObject(l);
                                String code = jsonObject2.getString("IsDefaulter");
                                if (code.matches("Yes")){
                                    String msg = "Loan application cannot continue. This client has defaulted one or more loans";
                                    AlertDialog.Builder builder1 = new AlertDialog.Builder(getContext());
                                    builder1.setMessage(msg);
                                    builder1.setTitle("Defaulter Alert!");
                                    builder1.setCancelable(false);

                                    builder1.setPositiveButton(
                                            "DONE",
                                            new DialogInterface.OnClickListener() {
                                                public void onClick(DialogInterface dialog, int id) {
                                                    dialog.cancel();
                                                    //System.exit(0);
                                                    getActivity().finish();
                                                }
                                            });

                                    AlertDialog alert11 = builder1.create();
                                    alert11.show();
                                }
                            }

                        }
                        try {
                            ArrayAdapter<String> adapter1 = new ArrayAdapter<String>(getContext(), android.R.layout.simple_spinner_item, arrayListLoan);
                            adapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                            spinnerLoan.setAdapter(adapter1);
                        }catch (Exception r){
                            r.printStackTrace();
                        }


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                } else {
                }

            }

            @Override
            public void onTokenExpired() {

            }

            @Override
            public void onError(String error) {

            }
        });
    }

    void requestLoanProducts(String identifier){
        final String TAG = "LoanProducts";

        String URL = GFL.MSACCO_AGENT + GFL.GetLoanProducts;

        final GetLoanProductsRequest loansProducts = new GetLoanProductsRequest();
        loansProducts.corporateno = "CAP016";
        loansProducts.transactiontype = "1";
        loansProducts.agentid = SharedPrefs.read(SharedPrefs.AGENT_ID_1, null);
        loansProducts.terminalid = SharedPrefs.read(SharedPrefs.DEVICE_ID, null);
        loansProducts.longitude = getLong(getContext());
        loansProducts.latitude = getLat(getContext());
        loansProducts.date = getFormattedDate();

        Log.e(TAG, loansProducts.getBody().toString());
        arrayListLoan.clear();
        ArrayAdapter<String> adapter1 = new ArrayAdapter<String>(getContext(), android.R.layout.simple_spinner_item, arrayListLoan);
        adapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerLoan.setAdapter(null);

        GFL.instance(getContext()).request(URL, loansProducts, new GFL.RequestListener() {
            @Override
            public void onSuccess(String response) {
                GetLoanProductsResponse loanProductsResponse = GFL.instance(getContext())
                        .mGson.fromJson(response, GetLoanProductsResponse.class);
                if (loanProductsResponse.is_successful) {
                    List<LoanDetails> loanDetailsList = new ArrayList<>();
                    arrayListLoan.clear();
                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        JSONArray jsonArray = jsonObject.getJSONArray("GetLoanProducts");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                            JSONArray jsonArray1 = jsonObject1.getJSONArray("LoanProducts");
                            for (int l = 0; l < jsonArray1.length(); l++) {
                                JSONObject jsonObject2 = jsonArray1.getJSONObject(l);
                                loanBar.setVisibility(View.GONE);
                                String code = jsonObject2.getString("LoanProductCode");
                                String desc = jsonObject2.getString("ProductDescription");
                                String code1 = jsonObject2.getString("MaxLoanAmount");
                                String dropDown1 = code + "-" + desc;
                                arrayListLoan.add(String.valueOf(dropDown1));
                            }

                        }

                        try {
                            ArrayAdapter<String> adapter1 = new ArrayAdapter<String>(getContext(), android.R.layout.simple_spinner_item, arrayListLoan);
                            adapter1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                            spinnerLoan.setAdapter(adapter1);
                        }catch (Exception e){
                            e.printStackTrace();
                        }


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                } else {
                    Toast.makeText(getActivity(), loanProductsResponse.getError(), Toast.LENGTH_LONG).show();
                }

            }

            @Override
            public void onTokenExpired() {

            }

            @Override
            public void onError(String error) {

            }
        });
    }

    void requestGetAccountDetailsForSpinerLoad(String identifier) {
        SharedPrefs.write(SharedPrefs.LOAN_IDENTITY_NO, identifier);
        final String TAG = "GrowersAccounts";
        String URL = GFL.MSACCO_AGENT + GFL.GetGrowersDetails;
        final GrowersDetailsRequest growersDetails = new GrowersDetailsRequest();
        growersDetails.agentid = SharedPrefs.read(SharedPrefs.AGENT_ID_1, null);
        growersDetails.corporateno = "CAP016";
        growersDetails.accountidentifier = identifier;
        growersDetails.accountidentifiercode = "1";
        growersDetails.transactiontype = "1";
        growersDetails.terminalid = SharedPrefs.read(SharedPrefs.DEVICE_ID, null);
        growersDetails.longitude = getLong(getContext());
        growersDetails.latitude = getLat(getContext());
        growersDetails.date = getFormattedDate();

        Log.e(TAG, growersDetails.getBody().toString());

        GFL.instance(getContext()).request(URL, growersDetails, new GFL.RequestListener() {
            @Override
            public void onSuccess(String response) {
                GrowersDetailsResponse growersDetailsResponse = GFL.instance(getContext())
                        .mGson.fromJson(response, GrowersDetailsResponse.class);
                if (growersDetailsResponse.is_successful) {
                    List<GrowerDetails> growerDetailsList = new ArrayList<>();

                    try {
                        JSONObject jsonObject = new JSONObject(response);
                        JSONArray jsonArray = jsonObject.getJSONArray("Growers");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                            JSONArray jsonArray1 = jsonObject1.getJSONArray("GrowerDetails");
                            for (int l = 0; l < jsonArray1.length(); l++) {
                                JSONObject jsonObject2 = jsonArray1.getJSONObject(l);
                                progressBar.setVisibility(View.GONE);

                                if (jsonObject2.getString("BssRegistered").equals("Yes") && jsonObject2.getString("Status").equals("Active")){
                                    arrayList.add(String.valueOf(jsonObject2.getString("GrowerNo")));
                                }if (jsonObject2.getString("BssRegistered").equals("No")){
                                    Toast.makeText(getContext(), "Grower number is not BSS registered", Toast.LENGTH_SHORT).show();
                                }if (!jsonObject2.getString("Status").equals("Active")){
                                    Toast.makeText(getContext(), "Grower number is not active", Toast.LENGTH_SHORT).show();
                                }

                                String gNo = jsonObject2.getString("GrowerNo");
                                String bankExists = jsonObject2.getString("BankExists");
                                String growerName = jsonObject2.getString("GrowerName");
                                String mpesaloanlimit = jsonObject2.getString("Mpesaloanlimit");
                                String dob=jsonObject2.getString("Dob");

                                String correct_year = null;
                                  if(dob.equals("N/A")){
                                      Calendar calendarr = Calendar.getInstance();
                                      SimpleDateFormat sdff = new SimpleDateFormat("yyyy");
                                      correct_year = sdff.format(calendarr.getTime());
                                      Toast.makeText(getActivity(), "invalid DOB", Toast.LENGTH_SHORT).show();
                                    }else {
                                      if (dob.length() == 8) {
                                          String wrong_year = dob.substring(6, 8);
                                          correct_year = "19" + wrong_year;
                                      } else {
                                              DateFormat formatter = new SimpleDateFormat("yyyy");
                                              correct_year = formatter.format(Date.parse(dob));
                                      }
                                  }
                                Calendar calendar = Calendar.getInstance();
                                SimpleDateFormat sdf = new SimpleDateFormat("yyyy");
                                String currentDate = sdf.format(calendar.getTime());
                                agee = Integer.parseInt(currentDate) - Integer.parseInt(correct_year);

                                SharedPrefs.write(SharedPrefs.GROWERS_AGE, String.valueOf(agee));
                                SharedPrefs.write(SharedPrefs.MPESA_LIMIT, mpesaloanlimit);

                                SharedPrefs.write(SharedPrefs.LOAN_APP_G_NAME, growerName);
                                loanBar = getActivity().findViewById(R.id.loanPb);
                                loanBar.setVisibility(View.VISIBLE);
                                loanBar.getIndeterminateDrawable().setColorFilter(ContextCompat.getColor(getContext(), R.color.btn_green), PorterDuff.Mode.SRC_IN );

                            }
                        }

                        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_spinner_item, arrayList);
                        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                        spinner.setAdapter(adapter);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                }

            }

            @Override
            public void onTokenExpired() {

            }

            @Override
            public void onError(String error) {

            }
        });
    }



    void requestFingerPrints() {
        final String TAG = "GET FINGERPRINTS";
        String URL = GFL.MSACCO_AGENT + GFL.GetFourFingerPrint;

        final FingerPrintRequest fingerPrintRequest = new FingerPrintRequest();
        fingerPrintRequest.accountidentifier = identifier_deposit.getText().toString();
        fingerPrintRequest.accountidentifiercode = "1";
        fingerPrintRequest.agentid = SharedPrefs.read(SharedPrefs.AGENT_ID_1, null);
        fingerPrintRequest.terminalid = MainGFLDashboard.imei;//SharedPrefs.read(SharedPrefs.DEVICE_ID, null);
        fingerPrintRequest.longitude = getLong(getActivity());
        fingerPrintRequest.latitude = getLat(getActivity());
        fingerPrintRequest.date = getFormattedDate();

        Log.e(TAG, fingerPrintRequest.getBody().toString());

        GFL.instance(getActivity()).request(URL, fingerPrintRequest, new GFL.RequestListener() {
            @Override
            public void onSuccess(String response) {
                FingerPrintResponse fingerPrintResponse = GFL.instance(getActivity())
                        .mGson.fromJson(response, FingerPrintResponse.class);
                if (fingerPrintResponse.is_successful){
                    try {
                        JSONObject jsonObject = new JSONObject(response);

                        String leftRing = jsonObject.getString("leftring");
                        String leftLittle = jsonObject.getString("leftlittle");
                        String rightRing = jsonObject.getString("rightring");
                        String rightLittle = jsonObject.getString("rightlittle");

                        SharedPrefs.write(SharedPrefs.LEFT_RING, leftRing);
                        SharedPrefs.write(SharedPrefs.LEFT_LITTLE, leftLittle);
                        SharedPrefs.write(SharedPrefs.RIGHT_RING, rightRing);
                        SharedPrefs.write(SharedPrefs.RIGHT_LITTLE, rightLittle);
                        //progressDialog.dismiss();

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    SharedPrefs.transactionstypelogs(6, getContext());
                    SharedPrefs.growerslogs(text, getContext());
                    startActivity(new Intent(getActivity(), ValidateTunzhengbigBio.class));

                }

            }

            @Override
            public void onTokenExpired() {

            }

            @Override
            public void onError(String error) {

            }
        });

    }
    @Override
    public void onResume() {
        SharedPrefs.init(getContext());
        super.onResume();
        String fpBoolean = SharedPrefs.read(SharedPrefs.FP_BOOLEAN, null);
       /* progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage("Please wait...");
        progressDialog.setCancelable(false);
        if (fpBoolean.equals("true")) {
            progressDialog.dismiss();
            //requestGetAccountDetails();
        } else if (fpBoolean.equals("false")){
            progressDialog.show();
            Toast.makeText(getActivity(), "Please authenticate to complete transaction!", Toast.LENGTH_SHORT).show();
        }*/
    }

    void requestGetAccountDetails() {
        final String TAG = "GrowersAccounts";

        String URL = GFL.MSACCO_AGENT + GFL.GetGrowersDetails;
        final GrowersDetailsRequest growersDetails = new GrowersDetailsRequest();
        growersDetails.agentid = SharedPrefs.read(SharedPrefs.AGENT_ID_1, null);
        growersDetails.corporateno = "CAP016";
        growersDetails.accountidentifier = SharedPrefs.read(SharedPrefs.LOAN_APP_GROWER, null);;
        growersDetails.accountidentifiercode = "2";
        growersDetails.transactiontype = "1";
        growersDetails.terminalid = SharedPrefs.read(SharedPrefs.DEVICE_ID, null);
        growersDetails.longitude = getLong(getContext());
        growersDetails.latitude = getLat(getContext());
        growersDetails.date = getFormattedDate();

        Log.e(TAG, growersDetails.getBody().toString());

        GFL.instance(getContext()).request(URL, growersDetails, new GFL.RequestListener() {
            @Override
            public void onSuccess(String response) {
                GrowersDetailsResponse growersDetailsResponse = GFL.instance(getContext())
                        .mGson.fromJson(response, GrowersDetailsResponse.class);
                if (growersDetailsResponse.is_successful) {
                    //progressDialog.dismiss();
                    List<GrowerDetails> growerDetailsList = new ArrayList<>();

                    try {
                        JSONObject jsonObject = new JSONObject(response);

                        JSONArray jsonArray = jsonObject.getJSONArray("Growers");
                        for (int i = 0; i < jsonArray.length(); i++) {
                            JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                            JSONArray jsonArray1 = jsonObject1.getJSONArray("GrowerDetails");
                            for (int l = 0; l < jsonArray1.length(); l++) {
                                JSONObject jsonObject2 = jsonArray1.getJSONObject(l);
                                progressBar.setVisibility(View.GONE);
                                String bankExists = jsonObject2.getString("BankExists");
                                String saccoExists = jsonObject2.getString("SaccoExists");
                                String currentkgs =  jsonObject2.getString("CurrentKG");
                                SharedPrefs.bankexists(bankExists,getActivity());
                                SharedPrefs.currentkgs(currentkgs,getActivity());
                                SharedPrefs.saccoexists(saccoExists,getActivity());
                            }
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                } else {
                    Toast.makeText(getActivity(), "Accounts Fetching Failed", Toast.LENGTH_LONG).show();
                }

            }

            @Override
            public void onTokenExpired() {

            }

            @Override
            public void onError(String error) {

            }
        });
    }
}
