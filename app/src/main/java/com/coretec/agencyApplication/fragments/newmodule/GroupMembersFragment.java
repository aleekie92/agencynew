package com.coretec.agencyApplication.fragments.newmodule;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.InputType;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.cloudpos.jniinterface.PrinterInterface;
import com.coretec.agencyApplication.R;
import com.coretec.agencyApplication.activities.newmodules.MainDashboardActivity;
import com.coretec.agencyApplication.adapters.newmodule.NewMemberAdapter;
import com.coretec.agencyApplication.api.Api;
import com.coretec.agencyApplication.api.requests.newmodulesrequests.GetMicrogroupRequest;
import com.coretec.agencyApplication.api.responses.newmoduleresponses.GetGroupResponse;
import com.coretec.agencyApplication.model.newmodulemodels.GroupObject;
import com.coretec.agencyApplication.model.newmodulemodels.PaymentObject;
import com.coretec.agencyApplication.utils.PreferenceFileKeys;
import com.coretec.agencyApplication.utils.SharedPrefs;
import com.coretec.agencyApplication.wizarpos.function.printer.PrinterCommand;
import com.stepstone.stepper.BlockingStep;
import com.stepstone.stepper.StepperLayout;
import com.stepstone.stepper.VerificationError;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static com.coretec.agencyApplication.utils.Utils.getFormattedDate;
import static com.coretec.agencyApplication.utils.Utils.getLat;
import static com.coretec.agencyApplication.utils.Utils.getLong;
import static com.coretec.agencyApplication.utils.Utils.snackMessage;

public class GroupMembersFragment extends Fragment implements BlockingStep, View.OnClickListener,NewMemberAdapter.testClick {
    private EditText identifier;
    public RecyclerView mRecyclerView;
    RecyclerView.Adapter mAdapter;

    private Button searchmembers;
    private ProgressDialog progressDialog;
    private List<PaymentObject> paymentObjectList = new ArrayList<>();
    private LinearLayout addPaymentToList;
    SharedPreferences sharedPreferences;
    double totalL , totalS;

    private String Groupcode;
    String mnumber,accountname ,expectedamt, groupname;
    double totalexpectedamt,count ,totalpaid,unpaid;
    AlertDialog paymentdialog;
    List<GroupObject> miniModelList;
    private  TextView gname;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.adapter_member_details, container, false);

        sharedPreferences = getActivity().getSharedPreferences(PreferenceFileKeys.PREFS_FILE, Context.MODE_PRIVATE);
        identifier =v.findViewById(R.id.identifier);
        gname =v.findViewById(R.id.gname);
        searchmembers = v.findViewById(R.id.button_search);
        addPaymentToList = v.findViewById(R.id.linear_payment);
        mRecyclerView = v.findViewById(R.id.recycler_view);
        mRecyclerView.setOnClickListener(this);

        searchmembers.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Groupcode = identifier.getText().toString();

                if (Groupcode.isEmpty()) {
                    identifier.setError("Please input group number!");
                } else {
                    progressDialog = new ProgressDialog(getActivity());
                    progressDialog.setMessage("Fetching group details, please wait...");
                    progressDialog.setCancelable(false);
                    progressDialog.show();
                    requestMembers();

                    SharedPrefs.groupcode(Groupcode, getActivity());

                }
            }
        });
        return v;
    }

    @Override
    public void onNextClicked(final StepperLayout.OnNextClickedCallback callback) {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                if(identifier.getText().toString().matches("")){
                    Toast.makeText(getActivity(), "Enter group code and click on search", Toast.LENGTH_SHORT).show();
                }else if (paymentObjectList.size() <= 0 ){
                    Toast.makeText(getActivity(), "Create Contributions", Toast.LENGTH_SHORT).show();
                } else {

                    double tpresentmembers= paymentObjectList.size();
                    double tabsentmembers = count - tpresentmembers;

                    String res1, res2;

                    if(tpresentmembers == (int) tpresentmembers){
                        res1= String.format("%d",(int)tpresentmembers);
                    }
                    else {
                        res1= String.format("%s", tpresentmembers);
                    }

                    if(tabsentmembers == (int) tabsentmembers){
                        res2= String.format("%d",(int)tabsentmembers);
                    }
                    else {
                        res2= String.format("%s", tabsentmembers);
                    }

                    for (int i = 0; i < paymentObjectList.size(); i++) {
                        totalL += Double.parseDouble(paymentObjectList.get(i).getLoanAmount());//Integer.parseInt(loanamount.getText().toString());
                        totalS += Double.parseDouble(paymentObjectList.get(i).getSavingsAmount());// Integer.parseInt(savingsamount.getText().toString());
                    }

                    totalpaid=totalL+totalS;
                    unpaid=totalexpectedamt-totalpaid;
                    SharedPrefs.membersdetails(res1,res2,getActivity());
                    SharedPrefs.microrepayment(String.valueOf(totalL), String.valueOf(totalS), String.valueOf(unpaid), String.valueOf(totalpaid), getActivity());


                    PrinterInterface.open();
                    header(groupname);
                    writetestpaymentprint();
                    footer();
                    PrinterInterface.close();

                    callback.goToNextStep();
                    paymentObjectList.clear();
                    addPaymentToList.removeAllViews();
                }

            }
        },0L);
    }

    @Override
    public void onCompleteClicked(StepperLayout.OnCompleteClickedCallback callback) {

    }

    @Override
    public void onBackClicked(final StepperLayout.OnBackClickedCallback callback) {

    }

    @Override
    public VerificationError verifyStep() {
        return null;
    }

    @Override
    public void onSelected() {

    }

    @Override
    public void onError(@NonNull VerificationError error) {

    }
    public void setuprecyclerview(List<GroupObject> modList) {

        mRecyclerView.setLayoutManager(new LinearLayoutManager(
                getActivity()));
        //mRecyclerView.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));
        mRecyclerView.addItemDecoration(new DividerItemDecoration(mRecyclerView.getContext(), DividerItemDecoration.VERTICAL));

        mAdapter = new NewMemberAdapter(getContext(), modList,this);
        mRecyclerView.setAdapter(mAdapter);
    }


     void requestMembers(){
         final String TAG = "GetMicroGroups";
         String URL = Api.MSACCO_AGENT + Api.GetMicroGroups;

         final GetMicrogroupRequest microgroupRequest = new GetMicrogroupRequest();
         microgroupRequest.agentid = SharedPrefs.read(SharedPrefs.AGENT_ID_1, null);////
         microgroupRequest.corporate_no = MainDashboardActivity.corporateno;
         microgroupRequest.groupcode =identifier.getText().toString();
         microgroupRequest.terminalid = MainDashboardActivity.imei;
         microgroupRequest.longitude = getLong(getContext());
         microgroupRequest.latitude = getLat(getContext());
         microgroupRequest.date = getFormattedDate();
         Api.instance(getContext()).request(URL, microgroupRequest, new Api.RequestListener() {
             @Override
             public void onSuccess(String response) {

                 GetGroupResponse getGroupResponse = Api.instance(getActivity())
                         .mGson.fromJson(response, GetGroupResponse.class);

                 if (getGroupResponse.is_successful) {
                     progressDialog.dismiss();
                     try {
                         JSONObject jsonObject = new JSONObject(response);

                         miniModelList= new ArrayList<>();
                         JSONArray grlist = jsonObject.getJSONArray("microgroups");

                         for (int i = 0; i < grlist.length(); i++) {
                             JSONObject jsonObject2 = grlist.getJSONObject(i);


                              mnumber = jsonObject2.getString("memberno");
                              accountname = jsonObject2.getString("accountname");
                              expectedamt = jsonObject2.getString("expectedamount");
                              totalexpectedamt = jsonObject2.getDouble("totalexpected");
                              count = jsonObject2.getDouble("count");
                             groupname = jsonObject2.getString("goupname");

                             GroupObject newMod = new GroupObject(mnumber,accountname,expectedamt,totalexpectedamt,count);
                             miniModelList.add(newMod);
                         }
                         gname.setText(groupname);
                         PrinterInterface.open();
                         header(groupname);
                         writetest();
                         footer();
                         PrinterInterface.close();
                         setuprecyclerview(miniModelList);

                     } catch (JSONException e) {
                         e.printStackTrace();
                     }

                 } else {
                     progressDialog.dismiss();
                     Toast.makeText(getActivity(), "Could not find group details!", Toast.LENGTH_SHORT).show();
                 }

             }

             @Override
             public void onTokenExpired() {

             }

             @Override
             public void onError(String error) {

             }
         });
     }

    public void header(String gname){

        try {
            byte[] start = null;
            byte[] saccoName = null;
            byte[] type = null;

            try {
                start = String.valueOf("            ").getBytes("GB2312");
                saccoName = String.valueOf("     "+gname).getBytes("GB2312");
                type = String.valueOf("      GROUP REPAYMENT     ").getBytes("GB2312");

            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            PrinterInterface.begin();
            byte[] cmd;
            cmd = new byte[]{(byte) 0x1B, (byte) 0x40};

            cmd = new byte[]{
                    (byte) 0x1B, (byte) 0x45, (byte) 0};

            write(cmd);
            write(start);
            writeLineBreak(1);
            write(saccoName);
            writeLineBreak(2);
            write(type);
            writeLineBreak(1);
            PrinterInterface.end();

        }catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void footer(){
        try {
            byte[] time = null;
            byte[] nameAgent = null;
            byte[] motto = null;

            SimpleDateFormat timeStampFormat = new SimpleDateFormat("dd/MM/yyyy - HH:mm");
            Date myDate = new Date();
            String timeCurrent = timeStampFormat.format(myDate);
            String aName = sharedPreferences.getString(PreferenceFileKeys.AGENT_ACCOUNT_NAME, "");

            try {
                time = String.valueOf("Date : " + String.valueOf(timeCurrent)).getBytes("GB2312");
                nameAgent = String.valueOf("SERVED BY " + String.valueOf(aName)).getBytes("GB2312");
                motto = String.valueOf("       "+sharedPreferences.getString(PreferenceFileKeys.AGENT_SACCO_MOTTO, "")).getBytes("GB2312");
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            PrinterInterface.begin();
            byte[] cmd;
            cmd = new byte[]{(byte) 0x1B, (byte) 0x40};

            cmd = new byte[]{
                    (byte) 0x1B, (byte) 0x45, (byte) 0};

            write(cmd);
            write(time);
            writeLineBreak(1);
            write(nameAgent);
            writeLineBreak(2);
            write(motto);
            // print line break
            writeLineBreak(4);
            PrinterInterface.end();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    private void write(final byte[] arryData) {
        int result = PrinterInterface.write(arryData, arryData.length);
    }

    // print line break
    private void writeLineBreak(int lineNumber) {
        // write(new byte[]{'\n'});
        write(PrinterCommand.getCmdEscDN(lineNumber));
    }

    //public void writetest(String mnumber, String accname, String expectedamt) {
    public void writetest() {
        try {
            byte[] titles = null;
            byte[] mno = null;
            byte[] mname = null;
            byte[] expamount = null;

            try {

                for (int i = 0; i < miniModelList.size(); i++) {
                    mno = String.valueOf(miniModelList.get(i).getMemberno()).getBytes("GB2312");
                    write(mno);

                    mname = String.valueOf(": " + miniModelList.get(i).getAccountname()).getBytes("GB2312");
                    write(mname);
                    writeLineBreak(1);

                    expamount = String.valueOf("Expected Amount : " + miniModelList.get(i).getExpectedamount()).getBytes("GB2312");
                    write(expamount);
                    writeLineBreak(2);

                }
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            PrinterInterface.begin();

            byte[] cmd;
            cmd = new byte[]{(byte) 0x1B, (byte) 0x40};

            cmd = new byte[]{
                    (byte) 0x1B, (byte) 0x45, (byte) 0};
            write(cmd);

            PrinterInterface.end();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void writetestpaymentprint() {
        try {
            byte[] titles = null;
            byte[] mno = null;
            byte[] lamount = null;
            byte[] samount = null;

            try {

                for (int i = 0; i < paymentObjectList.size(); i++) {
                    mno = String.valueOf("Member No : " + paymentObjectList.get(i).getMemberNo()).getBytes("GB2312");
                    write(mno);

                    lamount = String.valueOf("Loan Amount : " + paymentObjectList.get(i).getLoanAmount()).getBytes("GB2312");
                    write(lamount);
                    writeLineBreak(1);

                    samount = String.valueOf("Expected Amount : " + paymentObjectList.get(i).getSavingsAmount()).getBytes("GB2312");
                    write(samount);
                    writeLineBreak(2);

                }
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
            PrinterInterface.begin();

            byte[] cmd;
            cmd = new byte[]{(byte) 0x1B, (byte) 0x40};

            cmd = new byte[]{
                    (byte) 0x1B, (byte) 0x45, (byte) 0};
            write(cmd);

            PrinterInterface.end();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void addPaymentToList(){
        addPaymentToList.removeAllViews();
        TextView mnumber,loanamount,savingsamount;
        for (final PaymentObject grObject : paymentObjectList) {
            // get g view
            View v = LayoutInflater.from(getActivity()).inflate(R.layout.item_payment, null);

            // get views
             mnumber = v.findViewById(R.id.tv_member_no);
             loanamount = v.findViewById(R.id.tv_loan_amount);
             savingsamount = v.findViewById(R.id.tv_savings_amout);
             ImageButton removeImageBtn = v.findViewById(R.id.imageButton_close);


            mnumber.setText(grObject.getMemberNo());
            loanamount.setText(String.valueOf(grObject.getLoanAmount()));
            savingsamount.setText(String.valueOf(grObject.getSavingsAmount()));
            removeImageBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    String msg = "Are you sure you want to remove contribution?";
                    android.app.AlertDialog.Builder builder1 = new android.app.AlertDialog.Builder(getContext());
                    builder1.setMessage(msg);
                    builder1.setTitle("Alert!");
                    builder1.setCancelable(false);

                    builder1.setPositiveButton(
                            "Yes",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                    paymentObjectList.remove(grObject);
                                    addPaymentToList();
                                }
                            });

                    builder1.setNegativeButton(
                            "Cancel",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
                                    dialog.cancel();
                                }
                            });

                    android.app.AlertDialog alert11 = builder1.create();
                    alert11.show();

                }
            });

            addPaymentToList.addView(v);

        }

    }


    private void createPaymentDialog(final String data) {

        LayoutInflater myInflater = LayoutInflater.from(getActivity());
        View v = myInflater.inflate(R.layout.payment_dialog_group, null);

        final Button submitBtn = v.findViewById(R.id.btn_submitt);
        final Button cancelBtn = v.findViewById(R.id.btnDiagCancel);
        final ProgressBar progressBar = v.findViewById(R.id.deposit_progressBar);

        final EditText mnumber = v.findViewById(R.id.txt_memberno);
        final EditText loanAmount = v.findViewById(R.id.loan_amt);
        final EditText savingsAmount = v.findViewById(R.id.savings_amt);
        mnumber.setText(data);
        mnumber.setFocusable(false);
        submitBtn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                String identifier = mnumber.getText().toString();
                String loan = loanAmount.getText().toString();
                String savings = savingsAmount.getText().toString();

               if (identifier.isEmpty()) {
                   Toast.makeText(getActivity(), "Enter member numnber", Toast.LENGTH_SHORT).show();
                }else if (loan.isEmpty()) {
                   Toast.makeText(getActivity(), "Enter Member's Loan amount", Toast.LENGTH_SHORT).show();
                } else if (savings.isEmpty()) {
                   Toast.makeText(getActivity(), "Enter Member's Savings amount", Toast.LENGTH_SHORT).show();
                } else {

                   String existingmnumber=null;
                   for (final PaymentObject grObject : paymentObjectList) {
                       existingmnumber=grObject.getMemberNo();
                   }

                   PaymentObject paymentObject = new PaymentObject();

                   if (identifier.equals(existingmnumber)){
                       Toast.makeText(getActivity(), "Member has already contributed", Toast.LENGTH_SHORT).show();
                   }else {
                       paymentObject.setMemberNo(identifier);
                       paymentObject.setLoanAmount(loan);
                       paymentObject.setSavingsAmount(savings);

                       paymentObjectList.add(paymentObject);

                       addPaymentToList();
                       paymentdialog.dismiss();
                   }
                }

            }
        });

        cancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                paymentdialog.dismiss();
            }
        });

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setView(v);
        paymentdialog = builder.create();
        paymentdialog.show();
        paymentdialog.setCancelable(false);
    }

    @Override
    public void onClick(View v) {

    }

    @Override
    public void testClicking(String data) {
        createPaymentDialog(data);
    }

    @Override
    public void onResume() {
        totalL=0.00;
        totalS=0.00;
        super.onResume();
    }
}
