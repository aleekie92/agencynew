package com.coretec.agencyApplication.fragments.newmodule;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.coretec.agencyApplication.R;
import com.coretec.agencyApplication.utils.SharedPrefs;
import com.stepstone.stepper.BlockingStep;
import com.stepstone.stepper.StepperLayout;
import com.stepstone.stepper.VerificationError;

import java.io.ByteArrayOutputStream;

public class ApplicationFormBack extends Fragment implements BlockingStep {

    private CardView card_appformback_photo;
    private static final int CAMERA_REQUEST = 1888;
    private ImageView imageview_formback;

    private Button submitPassportPhoto;
    ProgressDialog progressDialog;
    SharedPreferences sharedPreferences;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.activity_vr_adapter_applicationformback_photo, container, false);
        SharedPrefs.init(getContext());

        //sharedPreferences = getActivity().getSharedPreferences(PreferenceFileKeys.PREFS_FILE, Context.MODE_PRIVATE);
        //initialize your UI
        imageview_formback =  v.findViewById(R.id.imageview_formback);
        card_appformback_photo =  v.findViewById(R.id.card_appformback_photo);

        card_appformback_photo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(cameraIntent, CAMERA_REQUEST);
            }
        });

        return v;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case CAMERA_REQUEST:
                if (resultCode == Activity.RESULT_OK) {
                    try {
                        Bitmap photo = (Bitmap) data.getExtras().get("data");
                        imageview_formback.setImageBitmap(photo);

                    } catch (Exception e) {
                        Toast.makeText(getContext(), "Failed to load", Toast.LENGTH_SHORT)
                                .show();
                        Log.e("Camera", e.toString());
                    }
                }
        }
    }

    private String bitmapToBase64(Bitmap bitmap) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
        byte[] byteArray = byteArrayOutputStream.toByteArray();
        return Base64.encodeToString(byteArray, Base64.DEFAULT);

        //suding this function
        /*Bitmap img = ((BitmapDrawable) passport_imageView.getDrawable()).getBitmap();
        String encodedImage = bitmapToBase64(img);*/
    }

    @Override
    public void onNextClicked(final StepperLayout.OnNextClickedCallback callback) {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                String defaultb = "iVBORw0KGgoAAAANSUhEUgAAAVMAAAFPCAIAAAB/PbrDAAAAA3NCSVQICAjb4U/gAAAgAElEQV";

                Bitmap img = ((BitmapDrawable) imageview_formback.getDrawable()).getBitmap();
                final String encodedImage = bitmapToBase64(img);
                String testImage = encodedImage.substring(0,74);

                if (testImage.equals(defaultb)) {
                    Toast.makeText(getContext(), "Click to capture filled back page photo!", Toast.LENGTH_SHORT).show();
                } else {
                    SharedPrefs.appback(encodedImage,getActivity());
                    callback.goToNextStep();
                }
                //callback.goToNextStep();

            }
        }, 0L);// delay open another fragment,
    }
    @Override
    public void onCompleteClicked(StepperLayout.OnCompleteClickedCallback callback) {

    }
    @Override
    public void onBackClicked(final StepperLayout.OnBackClickedCallback callback) {
        callback.goToPrevStep();
    }
    @Override
    public VerificationError verifyStep() {
        return null;
    }
    @Override
    public void onSelected() {
    }
    @Override
    public void onError(@NonNull VerificationError error) {
    }



}
