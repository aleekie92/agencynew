package com.coretec.agencyApplication.utils;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.text.TextUtils;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.coretec.agencyApplication.R;
import com.coretec.agencyApplication.api.Api;
import com.coretec.agencyApplication.wizarpos.manager.ActionContainerImpl;
import com.coretec.agencyApplication.wizarpos.mvc.base.ActionManager;

public class AppController extends Application {

    private static final String TAG = AppController.class.getSimpleName();
    public static AppController mInstance;
    private RequestQueue mRequestQueue;
    private static Context mContext;
    public static String model = "";
    public static boolean isHand = false;
    public static boolean isQ1 = false;

    // SharedPreference Keys
    public static final String SECURITY = "security";
    public static final String AGENT_PHONE_NUMBER = "agent_phone_number";

    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = this;
        mContext = getApplicationContext();

        // Common.getModel();
        model = "actions";
        ActionManager.initActionContainer(new ActionContainerImpl(this, model));
    }

    public static synchronized AppController getInstance() {
        return mInstance;
    }

    private RequestQueue getRequestQueue() {
        if (mRequestQueue == null) {
            mRequestQueue = Volley.newRequestQueue(getApplicationContext());
        }
        return mRequestQueue;
    }

    public <T> void addToRequestQueue(Request req, String tag) {
        req.setTag(TextUtils.isEmpty(tag) ? TAG : tag);
        getRequestQueue().add(req);
    }

    public <T> void addToRequestQueue(Request req) {
        req.setTag(TAG);
        getRequestQueue().add(req);
    }

    public void cancelPendingRequest(Object tag) {
        getRequestQueue().cancelAll(tag);
    }

    public static Context getAppContext() {
        return AppController.mContext;
    }


    public void setConnectivityReceiver(ConnectivityReceiver.ConnectivityReceiverListener listener) {
        ConnectivityReceiver.connectivityReceiverListener = listener;
    }
}
