package com.coretec.agencyApplication.utils;

/**
 * Created by Baliat on 17/10/10.
 */

public class Const {
    private String loginToken = "";
    private String requestToken = "";
    private String apiKey = "";

    private Const() {

    }
    private static Const instance;

    public static Const getInstance() {
        if (instance == null) {
            instance = new Const();
        }
        return instance;
    }

    public String getLoginToken() {
        return loginToken;
    }

    public void setLoginToken(String loginToken) {
        this.loginToken = loginToken;
    }

    public String getRequestToken() {
        return requestToken;
    }

    public void setRequestToken(String requestToken) {
        this.requestToken = requestToken;
    }

    public String getApiKey() {
        return apiKey;
    }

    public void setApiKey(String apiKey) {
        this.apiKey = apiKey;
    }
}
