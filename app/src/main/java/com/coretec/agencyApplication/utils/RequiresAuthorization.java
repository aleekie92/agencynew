package com.coretec.agencyApplication.utils;

/**
 * Created by kelvinoff on 17/11/21.
 */

public interface RequiresAuthorization {
    String getPhoneNumber();
}
