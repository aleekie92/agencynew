package com.coretec.agencyApplication.utils;

import android.app.KeyguardManager;
import android.app.admin.DevicePolicyManager;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Handler;
import android.os.UserManager;
import android.support.annotation.RequiresApi;
import android.util.Log;
import android.widget.Toast;

import com.coretec.agencyApplication.activities.SplashActivity;
import com.coretec.agencyApplication.api.Api;
import com.coretec.agencyApplication.api.Server;
import com.coretec.agencyApplication.api.responses.GenerateTokenResponse;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class BootReceiver extends BroadcastReceiver{

    Context cxt;
    @Override
    public void onReceive(Context context, Intent intent) {

        if (intent.getAction().equals(Intent.ACTION_BOOT_COMPLETED)) {
            autoLaunch(context);
            //autodataON(context);
        }
    }

    public void autoLaunch(final Context context){
        switch (Api.MSACCO_AGENT) {
            case "http://172.16.11.145:3052/api/AgencyBanking/":
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Intent i = new Intent(context, SplashActivity.class);
                        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(i);
                    }
                }, 20000);
                break;
            case "http://172.16.11.145:3051/api/AgencyBanking/":
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Intent i = new Intent(context, SplashActivity.class);
                        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                        context.startActivity(i);
                    }
                }, 20000);
                break;

            default:
                Toast.makeText(context, "success", Toast.LENGTH_SHORT).show();
        }
    }

    public void autodataON(Context context){
        ConnectivityManager dataManager;
        dataManager  = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        Method dataMtd = null;
        try {
            dataMtd = ConnectivityManager.class.getDeclaredMethod("setMobileDataEnabled", boolean.class);
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }
        dataMtd.setAccessible(true);
        try {
            dataMtd.invoke(dataManager, true);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }

        WifiManager wifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
        //wifiManager.setWifiEnabled(true);
        wifiManager.setWifiEnabled(false);
    }

}
