package com.coretec.agencyApplication.utils;


public class PreferenceFileKeys {
    public static final String PREFS_FILE = "com.prefs";


    public static final String IS_LOGGED_IN = "is_logged_in";
    public static final String ACCESS_TOKEN = "is_logged_in";

    public static final String AGENT_ID = "agent_id";
    public static final String AGENT_SACCO_NAME = "sacconame";
    public static final String AGENT_SACCO_MOTTO = "saccomotto";
    public static final String AGENT_ACCOUNT_NAME = "accountname";
    public static final String AGENT_BUSINESS_NAME = "proposedname";
    public static final String AGENT_ACCOUNT_NO = "agentaccno";
    public static final String AGENT_LOCATION = "location";
    public static final String AGENT_ACCOUNT_BALANCE = "accountbalance";
    public static final String AGENT_COMMISSION_BALANCE = "accountcommissionbalance";
}
