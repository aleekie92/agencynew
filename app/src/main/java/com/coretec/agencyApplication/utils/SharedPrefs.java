package com.coretec.agencyApplication.utils;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;

import com.coretec.agencyApplication.R;
import com.coretec.agencyApplication.api.Api;

public class SharedPrefs {

    private static SharedPreferences mSharedPref;
    public static final String DEVICE_ID = "DEVICE_ID";
    public static final String AGENT_NAME = "AGENT_NAME";
    public static final String GFL_AGENT_NAME = "GFL_AGENT_NAME";
    public static final String NEW_SACCO_NAME = "NEW_SACCO_NAME";
    public static final String AGE = "AGE";
    public static final String IS_SELECT = "IS_SELECT";
    public static final String PHONE_NUMBER = "PHONE_NUMBER";
    public static final String LOAN_CODE = "LOAN_CODE";

    public static final String LOAN_APP_CODE = "LOAN_APP_CODE";
    public static final String PU_ACTIVATION_G_NUMBER = "PU_ACTIVATION_G_NUMBER";
    public static final String PU_REGISTRATION_G_NUMBER = "PU_REGISTRATION_G_NUMBER";
    public static final String PIN_RESET_G_NO = "PIN_RESET_G_NO";
    public static final String LOAN_HEAD_G_NUMBER = "LOAN_HEAD_G_NUMBER";
    public static final String LOAN_CALC_G_NUMBER = "LOAN_CALC_G_NUMBER";
    public static final String BALANCE_G_NUMBER = "BALANCE_G_NUMBER";
    public static final String LOAN_APP_G_NUMBER = "LOAN_APP_G_NUMBER";
    public static final String MINI_G_NUMBER = "MINI_G_NUMBER";
    public static final String OLD_PHONE = "OLD_PHONE";
    public static final String MPESA_G_NUMBER = "MPESA_G_NUMBER";
    public static final String PU_G_NUMBER = "PU_G_NUMBER";

    public static final String PU_BLOCK_P_NUMBER = "PU_BLOCK_P_NUMBER";
    public static final String FP_BOOLEAN = "FP_BOOLEAN";
    public static final String SELECTEDGROWER = "SELECTEDGROWER";
    public static final String PBC_PHONE = "PBC_PHONE";
    public static final String PBC_AMOUNT = "PBC_AMOUNT";

    //Loan application
    public static final String LOAN_PT_CODE = "LOAN_PT_CODE";
    public static final String LOAN_ENTER_AMT = "LOAN_ENTER_AMT";
    public static final String LOAN_APP_GROWER = "LOAN_APP_GROWER";
    public static final String LOAN_MINIMUM = "LOAN_MINIMUM";
    public static final String LOAN_MAXIMUM = "LOAN_MAXIMUM";
    public static final String LOAN_BANK_EXISTS ="LOAN_BANK_EXISTS";
    public static final String GROWERS_AGE ="GROWERS_AGE";
    public static final String MPESA_LIMIT ="MPESA_LIMIT";
    public static final String MENU_SEPERATOR ="MENU_SEPERATOR";
    public static final String COPORATE ="COPORATE";
    public static final String LOAN_SACCO_EXISTS ="LOAN_SACCO_EXISTS";
    public static final String LOAN_CURRENT_KGS ="LOAN_CURRENT_KGS";
    public static final String LOAN_QUALIFIED_KGS = "LOAN_QUALIFIED_KGS";
    public static final String LOAN_IDENTITY_NO = "LOAN_IDENTITY_NO";
    public static final String LOAN_APP_G_NAME = "LOAN_APP_G_NAME";
    public static final String LOAN_SPINNER = "LOAN_SPINNER";
    public static final String LOAN_NEXT_STATUS = "LOAN_NEXT_STATUS";
    public static final String LOAN_NEW_MINIMUM = "LOAN_NEW_MINIMUM";
    public static final String LOAN_NEW_MAXIMUM = "LOAN_NEW_MAXIMUM";
    public static final String DROP_DOWN = "DROP_DOWN";
    public static final String NET_AMOUNT = "NET_AMOUNT";
    public static final String IS_DEFAULTER = "IS_DEFAULTER";
    public static final String APPLICANT_ID = "APPLICANT_ID";

    //Loan Calculator
    public static final String LOAN_G_NO = "LOAN_G_NO";
    public static final String LOAN_F_N = "LOAN_F_N";
    public static final String LOAN_ID = "LOAN_ID";
    public static final String LOAN_G_ST = "LOAN_G_ST";
    public static final String LOAN_PU_REG = "LOAN_PU_REG";
    public static final String LOAN_PR_TY = "LOAN_PR_TY";
    public static final String LOAN_PAY_KEY = "LOAN_PAY_KEY";
    public static final String LOAN_PRO_NAME = "LOAN_PRO_NAME";
    public static final String LOAN_PREV_YR = "LOAN_PREV_YR";
    public static final String LOAN_CURR_KG = "LOAN_CURR_KG";
    public static final String LOAN_TOT_CURR = "LOAN_TOT_CURR";
    public static final String LOAN_RATE_PKG = "LOAN_RATE_PKG";
    public static final String LOAN_FACT = "LOAN_FACT";
    public static final String LOAN_BONUS_RT = "LOAN_BONUS_RT";
    public static final String LOAN_PER_OF_PRE = "LOAN_PER_OF_PRE";
    public static final String LOAN_PRO_RATE = "LOAN_PRO_RATE";
    public static final String LOAN_PROR_RL = "LOAN_PROR_RL";
    public static final String LOAN_QUAL_AMT = "LOAN_QUAL_AMT";
    public static final String LOAN_OUT_BAL = "LOAN_OUT_BAL";
    public static final String LOAN_NET_AMT = "LOAN_NET_AMT";

    //Balance Inquiry
    public static final String BAL_CLIENT_NAME = "BAL_CLIENT_NAME";
    public static final String CUST_CODE = "CUST_CODE";
    public static final String LOAN_TYPE = "LOAN_TYPE";
    public static final String LOAN_TYPE_NAME = "LOAN_TYPE_NAME";
    public static final String LOAN_NO = "LOAN_NO";
    public static final String INT_BAL = "INT_BAL";
    public static final String LOAN_BAL = "LOAN_BAL";
    public static final String TOTAL_BAL = "TOTAL_BAL";

    //Customer Registration Details
    public static final String REG_GROWER_NO = "REG_GROWER_NO";
    public static final String REG_ACC_NAME = "REG_ACC_NAME";
    public static final String REG_IDNUMBER = "REG_IDNUMBER";
    public static final String REG_PHONE = "REG_PHONE";
    public static final String REG_GENDER = "REG_GENDER";
    public static final String REG_DOB = "REG_DOB";
    public static final String REG_PASSPORT = "REG_PASSPORT";
    public static final String REG_FRONT_ID = "REG_FRONT_ID";
    public static final String REG_BACK_ID = "REG_BACK_ID";
    public static final String REG_FULL_NAME = "REG_FULL_NAME";
    public static final String REG_BANK_DETAILS = "REG_BANK_DETAILS";

    public static final String REG_ACCOUNT_NO = "REG_ACCOUNT_NO";
    public static final String REG_KIN_ID = "REG_KIN_ID";
    public static final String IDENTIFIER_DIP_NUMBER = "IDENTIFIER_DIP_NUMBER";
    public static final String IDENTIFIER_FOR_UPDATING = "IDENTIFIER_FOR_UPDATING";
    public static final String REG_ATM_CARD = "REG_ATM_CARD";
    public static final String REG_RELATIONSHIP = "REG_RELATIONSHIP";
    public static final boolean PU_REGISTERED = true;

    public static final String REG_SACCO_CODE = "REG_SACCO_CODE";
    public static final String REG_SACCO_AC_NO = "REG_SACCO_AC_NO";
    public static final String REG_SACOO_CARD = "REG_SACOO_CARD";
    public static final String REG_LANG_SPINNER = "REG_LANG_SPINNER";

    public static final String LEFT_RING = "LEFT_RING";
    public static final String LEFT_LITTLE = "LEFT_LITTLE";
    public static final String RIGHT_RING = "RIGHT_RING";
    public static final String RIGHT_LITTLE = "RIGHT_LITTLE";

    public static final String AGENT_LEFT_RING = "AGENT_LEFT_RING";
    public static final String AGENT_LEFT_LITTLE = "AGENT_LEFT_LITTLE";
    public static final String AGENT_RIGHT_RING = "AGENT_RIGHT_RING";
    public static final String AGENT_RIGHT_LITTLE = "AGENT_RIGHT_LITTLE";

    public static final String  AGENT_LEFT_MIDDLE="AGENT_LEFT_MIDDLE";
    public static final String AGENT_RIGHT_MIDDLE="AGENT_RIGHT_MIDDLE";
    public static final String AGENT_LEFT_INDEX="AGENT_LEFT_INDEX";
    public static final String AGENT_RIGHT_INDEX="AGENT_RIGHT_INDEX";
    public static final String AGENT_LEFT_THUMB="AGENT_LEFT_THUMB";
    public static final String AGENT_RIGHT_THUMB="AGENT_RIGHT_THUMB";

    //MiniStatement
    public static final String MINI_POSTING_DATE = "MINI_POSTING_DATE";
    public static final String MINI_DESCRIPTION = "MINI_DESCRIPTION";
    public static final String MINI_AMOUNT = "MINI_AMOUNT";

    //Virtual Registration
    public static final String VR_PASSPORT = "VR_PASSPORT";
    public static final String VR_DOB = "VR_DOB";
    public static final String VR_KIN_DOB = "VR_KIN_DOB";
    public static final String VR_GENDER = "VR_GENDER";
    public static final String VR_KIN_GENDER = "VR_KIN_GENDER";
    public static final String VR_NAMES = "VR_NAMES";
    public static final String VR_ID = "VR_ID";
    public static final String VR_PHONE = "VR_PHONE";
    public static final String VR_STATUS = "VR_STATUS";
    public static final String VR_CITIZEN = "VR_CITIZEN";
    public static final String VR_EMAIL = "VR_EMAIL";
    public static final String VR_EMPLOYER = "VR_EMPLOYER";
    public static final String VR_FRONT_ID = "VR_FRONT_ID";
    public static final String VR_BACK_ID = "VR_BACK_ID";
    public static final String VR_SIGNATURE = "VR_SIGNATURE";
    public static final String VR_BIO_ID = "VR_BIO_ID";
    public static final String VR_GROUP_PHOTO = "VR_GROUP_PHOTO";
    public static final String FINGERMODULE = "FINGERMODULE";
    public static final String VR_PHOTO = "VR_PHOTO";

    public static final String SMS_CODE = "SMS_CODE";
    public static final String AGENT_ID_1 = "AGENT_ID_1";
    public static final String DEVICE_TERMINAL = "DEVICE_TERMINAL";

    //STK Push
    public static final String STK_LOAN_KEY = "STK_LOAN_KEY";
    public static final String STK_APP_GROWER = "STK_APP_GROWER";

    //BENEVOLENT and RSF ACCOUNT CODE
    public static final String ACCOUNT_CODE = "ACCOUNT_CODE";

    //new modules
    public static final String VR_SALUTATION = "VR_SALUTATION";
    public static final String CURRENCY = "CURRENCY";
    public static final String IMSIORNOT = "IMSIORNOT";
    public static final String VR_LOCALITY = "VR_LOCALITY";
    public static final String VR_NEARESTMARKET = "VR_NEARESTMARKET";
    public static final String VR_BRANCH = "VR_BRANCH";
    public static final String VR_REG_FORMFRONT = "VR_REG_FORMFRONT";
    public static final String VR_REG_FORMBACK = "VR_REG_FORMBACK";
    public static final String VR_LOAN_PT_CODE = "VR_LOAN_PT_CODE";

    public static final String VR_LOAN_LEAVE_PHOTO1 = "VR_LOAN_LEAVE_PHOTO1";
    public static final String VR_LOAN_LEAVE_PHOTO2 = "VR_LOAN_LEAVE_PHOTO2";
    public static final String VR_LOAN_LEAVE_PHOTO3 = "VR_LOAN_LEAVE_PHOTO3";
    public static final String VR_LOAN_LEAVE_PHOTO4 = "VR_LOAN_LEAVE_PHOTO4";
    public static final String VR_LOAN_LEAVE_PHOTO5 = "VR_LOAN_LEAVE_PHOTO5";
    public static final String VR_LOAN_LEAVE_PHOTO6 = "VR_LOAN_LEAVE_PHOTO6";
    public static final String VR_LOAN_LEAVE_PHOTO7 = "VR_LOAN_LEAVE_PHOTO7";
    public static final String VR_LOAN_LEAVE_PHOTO8 = "VR_LOAN_LEAVE_PHOTO8";
    public static final String VR_LOAN_LEAVE_PHOTO9 = "VR_LOAN_LEAVE_PHOTO9";
    public static final String VR_LOAN_LEAVE_PHOTO10 = "VR_LOAN_LEAVE_PHOTO10";

    public static final String VR_LOAN_BACK_PHOTO = "VR_LOAN_FRONT_PHOTO";

    //GROUP LOAN PAYMENTS DETAILS
    public static final String VR_TOTAL_LOANS = "VR_TOTAL_LOANS";
    public static final String VR_GROUP_CODE = "VR_GROUP_CODE";
    public static final String VR_TOTAL_SAVINGS = "VR_TOTAL_SAVINGS";
    public static final String VR_TOTAL_UNPAID = "VR_TOTAL_UNPAID";
    public static final String VR_TOTAL_SAVING_AND_LOAN_PAID = "VR_TOTAL_SAVING_AND_LOAN_PAID";

    public static final String VR_GROUP_PAYMENT_VERIFIED_BY = "VR_GROUP_PAYMENT_VERIFIED_BY";
    public static final String VR_GROUP_PAYMENT_VERIFIER_PHONE = "VR_GROUP_PAYMENT_VERIFIER_PHONE";
    public static final String VR_TOTAL_PRESENT_MEMBERS = "VR_TOTAL_PRESENT_MEMBERS";
    public static final String VR_TOTAL_ABSENT_MEMBERS = "VR_TOTAL_ABSENT_MEMBERS";

    //public static final String VR_TOTAL_MEMBERS = "VR_TOTAL_MEMBERS";
    // public static final String VR_GROUP_IMAGE = "VR_GROUP_IMAGE";
    public static final String VR_GROUP_REMARKS = "VR_GROUP_REMARKS";
    public static final String VR_AMOUNT = "VR_AMOUNT";
    public static final String VR_EMPLOYER_NUMBER = "VR_EMPLOYER_NUMBER";

    public static final String VR_REG_FULL_NAME = "VR_REG_FULL_NAME";
    public static final String VR_REG_FULL_ID = "VR_REG_FULL_ID";
    public static final String VR_REG_FULL_PHONE = "VR_REG_FULL_PHONE";
    public static final String VR_REG_RELATIONSHIP = "VR_REG_RELATIONSHIP";
    public static final String VR_COOPORATE_NO = "VR_COOPORATE_NO";
    public static final String VR_CLOSING_FLOAT = "VR_CLOSING_FLOAT";


    public static final boolean VR_PU_REGISTERED = true;
    //public static final boolean UPDATE_PU_REGISTERED = false;
    public static final String UPDATE_PU_REGISTERED = "true";

    //agent bioregistration
    public static final String AGENT_PASS_PHOTO = "AGENT_PASS_PHOTO";
    public static final String AGENT_ID_NO = "AGENT_ID_NO";
    public static final String BIO_AGENT_CODE = "BIO_AGENT_CODE";

    public static final String DATA_STORE = "DATA_STORE";

    //BIOMETRICS LOGS
    public static final String GROWERS_NUMBER_BIOLOGS = "GROWERS_NUMBER_BIOLOGS";
    public static final int TRANS_TYPE_BIOLOGS = 1;

    public static final int VR_THEME = 0;
    public static final String VR_APP_NAME = "VR_APP_NAME";

    private SharedPrefs()
    {

    }

    public static  void growerslogs(String glogs, Context context){
        SharedPreferences.Editor editor=mSharedPref.edit();
        editor.putString(GROWERS_NUMBER_BIOLOGS, glogs);
        editor.apply();
    }

    public static  void transactionstypelogs(int translogs, Context context){
        SharedPreferences.Editor editor=mSharedPref.edit();
        editor.putInt(String.valueOf(TRANS_TYPE_BIOLOGS), translogs);
        editor.apply();
    }


    public static  void kinidnumber(String kinid, Context context){
        SharedPreferences.Editor editor=mSharedPref.edit();
        editor.putString(REG_KIN_ID, kinid);
        editor.apply();
    }

    public static  void bankexists(String bankk, Context context){
        SharedPreferences.Editor editor=mSharedPref.edit();
        editor.putString(LOAN_BANK_EXISTS, bankk);
        editor.apply();
    }

    public static  void dashboard(String dashboardtype, Context context){
        SharedPreferences.Editor editor=mSharedPref.edit();
        editor.putString(MENU_SEPERATOR, dashboardtype);
        editor.apply();
    }

    public static  void coporate(String corporate, Context context){
        SharedPreferences.Editor editor=mSharedPref.edit();
        editor.putString(COPORATE, corporate);
        editor.apply();
    }

    public static  void currentkgs(String currentkgss, Context context){
        SharedPreferences.Editor editor=mSharedPref.edit();
        editor.putString(LOAN_CURRENT_KGS, currentkgss);
        editor.apply();
    }

    public static  void saccoexists(String saccoExists, Context context){
        SharedPreferences.Editor editor=mSharedPref.edit();
        editor.putString(LOAN_SACCO_EXISTS, saccoExists);
        editor.apply();
    }

    public static  void loanappgrower(String grwer, Context context){
        SharedPreferences.Editor editor=mSharedPref.edit();
        editor.putString(LOAN_APP_GROWER, grwer);
        editor.apply();
    }



    public static  void identifiernumber(String ide, Context context){
        SharedPreferences.Editor editor=mSharedPref.edit();
        editor.putString(IDENTIFIER_DIP_NUMBER, ide);
        editor.apply();
    }

    public static  void identifierforupdating(String idforupdating, Context context){
        SharedPreferences.Editor editor=mSharedPref.edit();
        editor.putString(IDENTIFIER_FOR_UPDATING, idforupdating);
        editor.apply();
    }



    public static  void putname(String name,String id,String phone, Context context){
        SharedPreferences.Editor editor=mSharedPref.edit();
        editor.putString(VR_NAMES, name);
        editor.putString(VR_ID, id);
        editor.putString(VR_PHONE, phone);
        editor.apply();
    }

    public static  void moredetails(String name,String phone,String remarkss, Context context){
        SharedPreferences.Editor editor=mSharedPref.edit();
        editor.putString(VR_GROUP_PAYMENT_VERIFIED_BY, name);
        editor.putString(VR_GROUP_PAYMENT_VERIFIER_PHONE, phone);
        editor.putString(VR_GROUP_REMARKS, remarkss);
        editor.apply();
    }

    public static  void membersdetails(String totalp,String totala, Context context){
        SharedPreferences.Editor editor=mSharedPref.edit();
        editor.putString(VR_TOTAL_PRESENT_MEMBERS, totalp);
        editor.putString(VR_TOTAL_ABSENT_MEMBERS, totala);
        editor.apply();
    }

    public static  void status(String status, Context context){
        SharedPreferences.Editor editor=mSharedPref.edit();
        editor.putString(VR_STATUS, status);
        editor.apply();
    }
    public static  void agentidd(String agentt, Context context){
        SharedPreferences.Editor editor=mSharedPref.edit();
        editor.putString(AGENT_ID_1, agentt);
        editor.apply();
    }


    public static  void salutation(String sal, Context context){
        SharedPreferences.Editor editor=mSharedPref.edit();
        editor.putString(VR_SALUTATION, sal);
        editor.apply();
    }
    public static  void gender(String gender, Context context){
        SharedPreferences.Editor editor=mSharedPref.edit();
        editor.putString(VR_GENDER, gender);
        editor.apply();
    }

    public static  void kingender(String gender, Context context){
        SharedPreferences.Editor editor=mSharedPref.edit();
        editor.putString(VR_KIN_GENDER, gender);
        editor.apply();
    }

    public static  void amount(String amount, Context context){
        SharedPreferences.Editor editor=mSharedPref.edit();
        editor.putString(VR_AMOUNT, amount);
        editor.apply();
    }

    public static  void employernumber(String emplono, Context context){
        SharedPreferences.Editor editor=mSharedPref.edit();
        editor.putString(VR_EMPLOYER_NUMBER, emplono);
        editor.apply();
    }

    public static  void citizenship(String citi, Context context){
        SharedPreferences.Editor editor=mSharedPref.edit();
        editor.putString(VR_CITIZEN, citi);
        editor.apply();
    }

    public static  void branch(String bra, Context context){
        SharedPreferences.Editor editor=mSharedPref.edit();
        editor.putString(VR_BRANCH, bra);
        editor.apply();
    }

    public static  void productcode(String procode, Context context){
        SharedPreferences.Editor editor=mSharedPref.edit();
        editor.putString(VR_LOAN_PT_CODE, procode);
        editor.apply();
    }

    public static  void loanleavephoto1(String loanleave1, Context context){
        SharedPreferences.Editor editor=mSharedPref.edit();
        editor.putString(VR_LOAN_LEAVE_PHOTO1, loanleave1);
        editor.apply();
    }
    public static  void loanleavephoto2(String loanleave2, Context context){
        SharedPreferences.Editor editor=mSharedPref.edit();
        editor.putString(VR_LOAN_LEAVE_PHOTO2, loanleave2);
        editor.apply();
    }
    public static  void loanleavephoto3(String loanleave3, Context context){
        SharedPreferences.Editor editor=mSharedPref.edit();
        editor.putString(VR_LOAN_LEAVE_PHOTO3, loanleave3);
        editor.apply();
    }
    public static  void loanleavephoto4(String loanleave4, Context context){
        SharedPreferences.Editor editor=mSharedPref.edit();
        editor.putString(VR_LOAN_LEAVE_PHOTO4, loanleave4);
        editor.apply();
    }
    public static  void loanleavephoto5(String loanleave5, Context context){
        SharedPreferences.Editor editor=mSharedPref.edit();
        editor.putString(VR_LOAN_LEAVE_PHOTO5, loanleave5);
        editor.apply();
    }
    public static  void loanleavephoto6(String loanleave6, Context context){
        SharedPreferences.Editor editor=mSharedPref.edit();
        editor.putString(VR_LOAN_LEAVE_PHOTO6, loanleave6);
        editor.apply();
    }
    public static  void loanleavephoto7(String loanleave7, Context context){
        SharedPreferences.Editor editor=mSharedPref.edit();
        editor.putString(VR_LOAN_LEAVE_PHOTO7, loanleave7);
        editor.apply();
    }
    public static  void loanleavephoto8(String loanleave8, Context context){
        SharedPreferences.Editor editor=mSharedPref.edit();
        editor.putString(VR_LOAN_LEAVE_PHOTO8, loanleave8);
        editor.apply();
    }
    public static  void loanleavephoto9(String loanleave9, Context context){
        SharedPreferences.Editor editor=mSharedPref.edit();
        editor.putString(VR_LOAN_LEAVE_PHOTO9, loanleave9);
        editor.apply();
    }
    public static  void loanleavephoto10(String loanleave10, Context context){
        SharedPreferences.Editor editor=mSharedPref.edit();
        editor.putString(VR_LOAN_LEAVE_PHOTO10, loanleave10);
        editor.apply();
    }

    public static  void loanformbackphoto(String loanbackfront, Context context){
        SharedPreferences.Editor editor=mSharedPref.edit();
        editor.putString(VR_LOAN_BACK_PHOTO, loanbackfront);
        editor.apply();
    }


    public static  void employer(String employer, Context context){
        SharedPreferences.Editor editor=mSharedPref.edit();
        editor.putString(VR_EMPLOYER, employer);
        editor.apply();
    }

    public static  void contactss(String email, String locality, String nearestmarket, Context context){
        SharedPreferences.Editor editor=mSharedPref.edit();
        editor.putString(VR_EMAIL,email);
        editor.putString(VR_LOCALITY,locality);
        editor.putString(VR_NEARESTMARKET,nearestmarket);
        editor.apply();
    }


    public static  void passphoto(String passp, Context context){
        SharedPreferences.Editor editor=mSharedPref.edit();
        editor.putString(VR_PHOTO, passp);
        editor.apply();
    }

    public static  void dob(String dob, Context context){
        SharedPreferences.Editor editor=mSharedPref.edit();
        editor.putString(VR_DOB, dob);
        editor.apply();
    }

    public static  void kindob(String kindob, Context context){
        SharedPreferences.Editor editor=mSharedPref.edit();
        editor.putString(VR_KIN_DOB, kindob);
        editor.apply();
    }


    public static  void frontid(String frontid, Context context){
        SharedPreferences.Editor editor=mSharedPref.edit();
        editor.putString(VR_FRONT_ID, frontid);
        editor.apply();
    }

    public static  void backid(String backid, Context context){
        SharedPreferences.Editor editor=mSharedPref.edit();
        editor.putString(VR_BACK_ID, backid);
        editor.apply();
    }

    public static  void signature(String sigh, Context context){
        SharedPreferences.Editor editor=mSharedPref.edit();
        editor.putString(VR_SIGNATURE, sigh);
        editor.apply();
    }

    public static  void currency(String currency, Context context){
        SharedPreferences.Editor editor=mSharedPref.edit();
        editor.putString(CURRENCY, currency);
        editor.apply();
    }

    public static  void bioupdateid(String bioupdatingid, Context context){
        SharedPreferences.Editor editor=mSharedPref.edit();
        editor.putString(VR_BIO_ID, bioupdatingid);
        editor.apply();
    }


    public static  void groupphoto(String gphoto, Context context){
        SharedPreferences.Editor editor=mSharedPref.edit();
        editor.putString(VR_GROUP_PHOTO, gphoto);
        editor.apply();
    }
    public static  void fingerprintmodule(String module, Context context){
        SharedPreferences.Editor editor=mSharedPref.edit();
        editor.putString(FINGERMODULE, module);
        editor.apply();
    }

    public static  void appfront(String appfront, Context context){
        SharedPreferences.Editor editor=mSharedPref.edit();
        editor.putString(VR_REG_FORMFRONT, appfront);
        editor.apply();
    }

    public static  void appback(String appback, Context context){
        SharedPreferences.Editor editor=mSharedPref.edit();
        editor.putString(VR_REG_FORMBACK, appback);
        editor.apply();
    }

    public static  void nextofkin(String name,String idno, String phoneno, Context context){
        SharedPreferences.Editor editor=mSharedPref.edit();
        editor.putString(VR_REG_FULL_NAME, name);
        editor.putString(VR_REG_FULL_ID, idno);
        editor.putString(VR_REG_FULL_PHONE, phoneno);
        editor.apply();
    }


    public static  void rship(String rship, Context context){
        SharedPreferences.Editor editor=mSharedPref.edit();
        editor.putString(VR_REG_RELATIONSHIP, rship);
        editor.apply();
    }


    public static  void cooporateno(String coopno, Context context){
        SharedPreferences.Editor editor=mSharedPref.edit();
        editor.putString(VR_COOPORATE_NO, coopno);
        editor.apply();
    }

    public static  void msaccoreg(boolean msaccco, Context context){
        SharedPreferences.Editor editor=mSharedPref.edit();
        editor.putBoolean(String.valueOf(VR_PU_REGISTERED), msaccco);
        editor.apply();
    }


    public static  void microrepayment(String tloan,String tsave,String totalunpaid, String totalpaid, Context context){
        SharedPreferences.Editor editor=mSharedPref.edit();
        editor.putString(VR_TOTAL_LOANS, tloan);
        editor.putString(VR_TOTAL_SAVINGS, tsave);
        editor.putString(VR_TOTAL_UNPAID, totalunpaid);
        editor.putString(VR_TOTAL_SAVING_AND_LOAN_PAID, totalpaid);
        editor.apply();
    }

    public static  void groupcode(String groupcode, Context context){
        SharedPreferences.Editor editor=mSharedPref.edit();
        editor.putString(VR_GROUP_CODE, groupcode);
        editor.apply();
    }

    public static void init(Context context)
    {
        if(mSharedPref == null)
           // mSharedPref = context.getSharedPreferences(context.getPackageName(), 0);
            mSharedPref = context.getSharedPreferences(context.getPackageName(), Activity.MODE_PRIVATE);
            //mSharedPref = context.getSharedPreferences(context.getPackageName(), Activity.MODE_WORLD_READABLE);
    }

    public static String read(String key, String defValue) {
        return mSharedPref.getString(key, defValue);
    }

    public static void write(String key, String value) {
        SharedPreferences.Editor prefsEditor = mSharedPref.edit();
        prefsEditor.putString(key, value);
        prefsEditor.apply();
    }

    public static boolean read(String key, boolean defValue) {
        return mSharedPref.getBoolean(key, defValue);
    }

    public static void write(String key, boolean value) {
        SharedPreferences.Editor prefsEditor = mSharedPref.edit();
        prefsEditor.putBoolean(key, value);
        prefsEditor.apply();
    }

    public static Integer read(String key, int defValue) {
        return mSharedPref.getInt(key, defValue);
    }

    public static void write(String key, Integer value) {
        SharedPreferences.Editor prefsEditor = mSharedPref.edit();
        prefsEditor.putInt(key, value).apply();
    }

}
