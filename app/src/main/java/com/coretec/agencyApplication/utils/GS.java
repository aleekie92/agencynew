package com.coretec.agencyApplication.utils;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * Created by Alex on 1/19/2016.
 */
public class GS {


    private static Gson gson = new GsonBuilder()
            .setDateFormat("yyyyMMddHHmmss")
            .create();
    private static Gson gsonPretty = new GsonBuilder()
            .setPrettyPrinting()
            .setDateFormat("yyyyMMddHHmmss")
            .create();
    private static Gson gsonConst = new GsonBuilder()
            .setPrettyPrinting()
            .setDateFormat("yyyyMMddHHmmss")
            .excludeFieldsWithoutExposeAnnotation()
            .create();

    private static Gson prettyConstant = new GsonBuilder()
            .setPrettyPrinting()
            .setDateFormat("yyyyMMddHHmmss")
            .excludeFieldsWithoutExposeAnnotation()
            .setPrettyPrinting()
            .create();

    public static Gson gson(){
        return gson;
    }

    public static Gson pretty(){
        return gsonPretty;
    }

    public static Gson constant(){
        return gsonConst;
    }

    public static Gson prettyConstant(){  return prettyConstant; }

}
